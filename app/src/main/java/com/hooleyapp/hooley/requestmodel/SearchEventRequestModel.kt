package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SearchEventRequestModel {

    @SerializedName("searchString")
    @Expose
    var searchString: String = ""
    @SerializedName("radius")
    @Expose
    var radius: Double = 0.toDouble()
    @SerializedName("eventTypes")
    @Expose
    var eventTypes: List<Int>? = null
    @SerializedName("isPaid")
    @Expose
    var isPaid: Boolean = false
    @SerializedName("isFree")
    @Expose
    var isFree: Boolean = false
    @SerializedName("latitude")
    @Expose
    var latitude: Double = 0.toDouble()
    @SerializedName("longitude")
    @Expose
    var longitude: Double = 0.toDouble()
    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("searchDate")
    @Expose
    var searchDate: String = ""
    @SerializedName("isGuest")
    @Expose
    var isGuest: Boolean = false


    /**
     * No args constructor for use in serialization
     *
     */
    constructor()

    /**
     *
     * @param isFree
     * @param searchString
     * @param userId
     * @param searchDate
     * @param longitude
     * @param radius
     * @param latitude
     * @param isPaid
     * @param eventTypes
     */
    constructor(searchString: String, radius: Double, eventTypes: List<Int>, isPaid: Boolean, isFree: Boolean, latitude: Double, longitude: Double, userId: String, searchDate: String, isGuest_: Boolean) : super() {
        this.searchString = searchString
        this.radius = radius
        this.eventTypes = eventTypes
        this.isPaid = isPaid
        this.isFree = isFree
        this.latitude = latitude
        this.longitude = longitude
        this.userId = userId
        this.searchDate = searchDate
        this.isGuest = isGuest_
    }

}