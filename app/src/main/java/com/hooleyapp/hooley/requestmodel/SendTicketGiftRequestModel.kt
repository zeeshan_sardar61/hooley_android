package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class SendTicketGiftRequestModel {

    @SerializedName("friendId")
    @Expose
    var friendId: Int = 0
    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("buyTicketId")
    @Expose
    var buyTicketId: ArrayList<Int>? = null

    /**
     * No args constructor for use in serialization
     *
     */
    constructor()

    /**
     *
     * @param userId
     * @param friendId
     * @param buyTicketId
     */
    constructor(friendId: Int, userId: String, buyTicketId: ArrayList<Int>) : super() {
        this.friendId = friendId
        this.userId = userId
        this.buyTicketId = buyTicketId
    }


}