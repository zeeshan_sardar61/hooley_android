package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SetupProfileRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("birthday")
    @Expose
    var birthday: String = ""
    @SerializedName("phoneNo")
    @Expose
    var phoneNo: String = ""
    @SerializedName("countryCode")
    @Expose
    var countryCode: String = ""
    @SerializedName("countryISO")
    @Expose
    var countryISO: String = ""
    @SerializedName("countryId")
    @Expose
    var countryId: String = ""
    @SerializedName("stateId")
    @Expose
    var stateId: String = ""
    @SerializedName("cityId")
    @Expose
    var cityId: String = ""
    @SerializedName("msisdn")
    @Expose
    var msisdn: String = ""
    @SerializedName("genderId")
    @Expose
    var gender: Int = 0

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param countryId
     * @param phoneNo
     * @param birthday
     * @param countryISO
     * @param cityId
     * @param stateId
     * @param userId
     * @param countryCode
     */
    constructor(userId: String, birthday: String, phoneNo: String, countryCode: String, countryISO: String, countryId: String, stateId: String, cityId: String, msisdn: String, gender: Int) : super() {
        this.userId = userId
        this.birthday = birthday
        this.phoneNo = phoneNo
        this.countryCode = countryCode
        this.countryISO = countryISO
        this.countryId = countryId
        this.stateId = stateId
        this.cityId = cityId
        this.msisdn = msisdn
        this.gender = gender
    }

}
