package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.SerializedName
import com.hooleyapp.hooley.model.PersonalProfileModel
import java.util.*

class UpdateGeoFenceRequestModel {

    @SerializedName("eventId")
    var eventId: String? = null

    @SerializedName("userId")
    var userId: String? = null

    @SerializedName("geofence")
    var eventGeofenceList = ArrayList<PersonalProfileModel.GeoFence>()

    constructor(eventId: String?, userId: String?, eventGeofenceList: ArrayList<PersonalProfileModel.GeoFence>) {
        this.eventId = eventId
        this.userId = userId
        this.eventGeofenceList = eventGeofenceList
    }
}