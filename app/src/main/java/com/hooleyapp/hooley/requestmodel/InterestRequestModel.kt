package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class InterestRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("name")
    @Expose
    var name: String = ""

    /**
     * No args constructor for use in serialization
     *
     */
    constructor()

    /**
     *
     * @param name
     * @param userId
     */
    constructor(userId: String, name: String) : super() {
        this.userId = userId
        this.name = name
    }

}