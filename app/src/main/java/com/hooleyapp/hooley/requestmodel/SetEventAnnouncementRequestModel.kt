package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SetEventAnnouncementRequestModel {
    @SerializedName("id")
    @Expose
    var id: Int = 0
    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("eventId")
    @Expose
    var eventId: Int = 0
    @SerializedName("announcement")
    @Expose
    var announcement: String = ""
    @SerializedName("broadcastAll")
    @Expose
    var broadcastAll: Boolean = false
    @SerializedName("broadcastHereNow")
    @Expose
    var broadcastHereNow: Boolean = false

    /**
     * No args constructor for use in serialization
     *
     */
    constructor()

    /**
     *
     * @param announcement
     * @param eventId
     * @param broadcastAll
     * @param userId
     * @param broadcastHereNow
     */

    constructor(id: Int, userId: String, eventId: Int, announcement: String, broadcastAll: Boolean, broadcastHereNow: Boolean) : super() {
        this.id = id
        this.userId = userId
        this.eventId = eventId
        this.announcement = announcement
        this.broadcastAll = broadcastAll
        this.broadcastHereNow = broadcastHereNow
    }

}