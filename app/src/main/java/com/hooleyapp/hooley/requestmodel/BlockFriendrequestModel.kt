package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BlockFriendrequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("friendId")
    @Expose
    var friendId: String = ""
    @SerializedName("isBlock")
    @Expose
    var isBlock: Boolean = true

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param userId
     * @param friendId
     */
    constructor(userId: String, friendId: String, block: Boolean) : super() {
        this.userId = userId
        this.friendId = friendId
        this.isBlock = block
    }

}
