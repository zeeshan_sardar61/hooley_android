package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by adilmalik on 20/10/2018.
 */

class UpdateLatLongRequestModel
/**
 * @param userId
 * @param longitude
 * @param latitude
 */
(@field:SerializedName("userId")
 @field:Expose
 var userId: String, @field:SerializedName("latitude")
 @field:Expose
 var latitude: String, @field:SerializedName("longitude")
 @field:Expose
 var longitude: String)
