package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class InvitePastGuestListRequestModel
/**
 * @param eventId
 * @param userId
 * @param eventName
 * @param eventList
 */
(@field:SerializedName("userId")
 @field:Expose
 var userId: String, @field:SerializedName("eventId")
 @field:Expose
 var eventId: String, @field:SerializedName("eventName")
 @field:Expose
 var eventName: String, eventList: ArrayList<EventList>) {

    @SerializedName("eventList")
    @Expose
    var eventList: ArrayList<EventList>? = null

    init {
        this.eventList = eventList
    }

    class EventList
    /**
     * @param eventId
     * @param userId
     */
    (@field:SerializedName("eventId")
     @field:Expose
     var eventId: String,
     @field:SerializedName("userId")
     @field:Expose
     var userId: ArrayList<Int>)
}
