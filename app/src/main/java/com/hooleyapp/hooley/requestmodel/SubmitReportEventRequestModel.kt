package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by adilmalik on 20/10/2018.
 */

class SubmitReportEventRequestModel
/**
 * @param reasonId
 * @param eventId
 * @param userId
 * @param reportDetails
 */
(@field:SerializedName("eventId")
 @field:Expose
 var eventId: String, @field:SerializedName("reasonId")
 @field:Expose
 var reasonId: String, @field:SerializedName("userId")
 @field:Expose
 var userId: String, @field:SerializedName("reportDetails")
 @field:Expose
 var reportDetails: String)
