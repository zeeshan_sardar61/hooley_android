package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BuyTicketRequestModel {


    @SerializedName("userId")
    @Expose
    var userId: String = ""

    @SerializedName("eventId")
    @Expose
    var eventId: String = ""
    @SerializedName("totalTickets")
    @Expose
    var totalTickets: Int = 0
    @SerializedName("token")
    @Expose
    var token: String = ""
    @SerializedName("eventTicketId")
    @Expose
    var eventTicketId: String = ""

    @SerializedName("type")
    @Expose
    var type: String = ""

    @SerializedName("donateAmount")
    @Expose
    var donateAmount: Int = 0

    /**
     * No args constructor for use in serialization
     */

    /**
     * @param eventId
     * @param eventTicketId
     * @param token
     * @param userId
     * @param totalTickets
     */
    constructor(userId: String, eventId: String, totalTickets: Int, token: String, eventTicketId: String, type: String, donateAmount: Int) : super() {
        this.userId = userId
        this.eventId = eventId
        this.totalTickets = totalTickets
        this.token = token
        this.eventTicketId = eventTicketId
        this.type = type
        this.donateAmount = donateAmount
    }


}
