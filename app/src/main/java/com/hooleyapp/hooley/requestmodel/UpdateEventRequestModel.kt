package com.hooleyapp.hooley.requestmodel


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by adilmalik on 29/10/2018.
 */

class UpdateEventRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""

    @SerializedName("eventId")
    @Expose
    var eventId: String = ""

    @SerializedName("eventName")
    @Expose
    var eventName: String = ""
    @SerializedName("eventDetails")
    @Expose
    var eventDetails: String = ""
    @SerializedName("startTime")
    @Expose
    var startTime: String = ""
    @SerializedName("endTime")
    @Expose
    var endTime: String = ""
    @SerializedName("minimumAgeId")
    @Expose
    var minimumAgeId: String = ""
    @SerializedName("isPublic")
    @Expose
    var isPublic: Boolean? = null
    @SerializedName("isPublish")
    @Expose
    var isPublish: Boolean? = null
    @SerializedName("coHostEvent")
    @Expose
    var coHostEvent: ArrayList<String>? = null
    @SerializedName("categoryEvent")
    @Expose
    var categoryEvent: ArrayList<String>? = null
    @SerializedName("optionalCategoryEvent")
    @Expose
    var optionalCategoryEvent: ArrayList<String>? = null
    @SerializedName("eventCovers")
    @Expose
    var eventCovers: ArrayList<EventCover>? = null
    @SerializedName("geofence")
    @Expose
    var geofence: ArrayList<Geofence>? = null

    @SerializedName("saveTicket")
    @Expose
    var saveTicket: SaveTicketRequestModel.TicketObj? = null

    @SerializedName("eventType")
    @Expose
    var eventType: String = ""

    @SerializedName("eventHashTags")
    @Expose
    var eventHashTags: ArrayList<String>? = null


    /**
     * No args constructor for use in serialization
     */

    /**
     * @param userId
     * @param eventId
     * @param eventName
     * @param eventDetails
     * @param startTime
     * @param endTime
     * @param isPrivate
     * @param isPublic
     * @param isFree
     * @param isPaid
     * @param inviteFriends
     * @param isPublish
     * @param hostEvent
     * @param coHostEvent
     * @param categoryEvent
     * @param optionalCategoryEvent
     * @param eventCovers
     * @param geofence
     */
    constructor(userId: String, eventId: String, eventName: String, eventType: String, eventDetails: String, eventHashs: ArrayList<String>, startTime: String, endTime: String, isPublic: Boolean?, isPublish: Boolean?, coHostEvent: ArrayList<String>, categoryEvent: ArrayList<String>, optionalCategoryEvent: ArrayList<String>, eventCovers: ArrayList<EventCover>, geofence: ArrayList<Geofence>, ageId: String) : super() {
        this.userId = userId
        this.eventId = eventId
        this.eventName = eventName
        this.eventType = eventType
        this.eventDetails = eventDetails
        this.eventHashTags = eventHashs
        this.startTime = startTime
        this.endTime = endTime
        this.isPublic = isPublic
        this.isPublish = isPublish
        this.coHostEvent = coHostEvent
        this.categoryEvent = categoryEvent
        this.optionalCategoryEvent = optionalCategoryEvent
        this.eventCovers = eventCovers
        this.geofence = geofence
        this.minimumAgeId = ageId
    }

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param eventAttribute
     */
//    constructor(eventAttribute: EventAttribute) : super() {
//        this.eventAttribute = eventAttribute
//    }
//
//    class EventAttribute {
//
//
//
//    }

    class EventCover {

        @SerializedName("photo_url")
        @Expose
        var photoUrl: String = ""

        /**
         * No args constructor for use in serialization
         */
        constructor()

        /**
         * @param photoUrl
         */
        constructor(photoUrl: String) : super() {
            this.photoUrl = photoUrl
        }

    }


    class Geofence {

        @SerializedName("latitude")
        @Expose
        var latitude: String = ""
        @SerializedName("longitude")
        @Expose
        var longitude: String = ""
        @SerializedName("radius")
        @Expose
        var radius: String = ""
        @SerializedName("address")
        @Expose
        var address: String = ""

        /**
         * No args constructor for use in serialization
         */
        constructor()

        /**
         * @param eventAddress
         * @param radius
         * @param longitude
         * @param latitude
         */
        constructor(latitude: String, longitude: String, radius: String, eventAddress: String) : super() {
            this.latitude = latitude
            this.longitude = longitude
            this.radius = radius
            this.address = eventAddress
        }

    }

}