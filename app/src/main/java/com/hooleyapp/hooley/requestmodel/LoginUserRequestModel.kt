package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginUserRequestModel
/**
 * @param email
 * @param deviceType
 * @param deviceToken
 * @param password
 */
(@field:SerializedName("email")
 @field:Expose
 var email: String, @field:SerializedName("password")
 @field:Expose
 var password: String, @field:SerializedName("deviceToken")
 @field:Expose
 var deviceToken: String, @field:SerializedName("deviceType")
 @field:Expose
 var deviceType: String)
