package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VerifyNumberRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""

    @SerializedName("phoneNo")
    @Expose
    var phoneNo: String = ""

    @SerializedName("countryCode")
    @Expose
    var countryCode: String = ""

    @SerializedName("msisdn")
    @Expose
    var msisdn: String = ""

    /**
     * No args constructor for use in serialization
     *
     */
    constructor()

    /**
     *
     * @param phoneNo
     * @param userId
     */
    constructor(userId: String, phoneNo: String, countryCode: String, msisdn: String) : super() {
        this.userId = userId
        this.phoneNo = phoneNo
        this.countryCode = countryCode
        this.msisdn = msisdn
    }

}