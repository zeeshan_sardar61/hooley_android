package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RefundTicketModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("eventId")
    @Expose
    var eventId: String = ""
    @SerializedName("buyTicketId")
    @Expose
    var buyTicketId: String = ""

    constructor(userId: String, eventId: String, buyTicketId: String) {
        this.userId = userId
        this.eventId = eventId
        this.buyTicketId = buyTicketId
    }
}
