package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LogoutRequestModel(@field:SerializedName("userId")
                         @field:Expose
                         var userId: String)

