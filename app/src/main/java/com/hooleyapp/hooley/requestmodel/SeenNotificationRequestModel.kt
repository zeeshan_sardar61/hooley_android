package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SeenNotificationRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("type")
    @Expose
    var type: String = ""

    /**
     * No args constructor for use in serialization
     *
     */
    constructor()

    /**
     *
     * @param userId
     * @param type
     */
    constructor(userId: String, type: String) : super() {
        this.userId = userId
        this.type = type
    }

}