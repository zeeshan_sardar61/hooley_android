package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AddCardRequest {

    @SerializedName("userId")
    @Expose
    var userId: String = ""

    @SerializedName("token")
    @Expose
    var token: String = ""

    constructor(userId: String, token: String) {
        this.userId = userId
        this.token = token
    }
}