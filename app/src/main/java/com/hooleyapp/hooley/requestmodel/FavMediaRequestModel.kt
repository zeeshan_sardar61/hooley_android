package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by adilmalik on 20/10/2018.
 */

class FavMediaRequestModel
/**
 * @param eventId
 * @param isFavorite
 * @param userId
 * @param postId
 */
(@field:SerializedName("userId")
 @field:Expose
 var userId: String, @field:SerializedName("eventId")
 @field:Expose
 var eventId: String, @field:SerializedName("postId")
 @field:Expose
 var postId: String, @field:SerializedName("isFavorite")
 @field:Expose
 var isFavorite: Boolean?)
