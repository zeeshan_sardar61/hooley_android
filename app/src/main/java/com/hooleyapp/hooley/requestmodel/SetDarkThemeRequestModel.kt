package com.hooleyapp.hooley.requestmodel

data class SetDarkThemeRequestModel(var userId: String, var isDark: Boolean)