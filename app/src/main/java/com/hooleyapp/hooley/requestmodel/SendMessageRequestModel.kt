package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SendMessageRequestModel {

    @SerializedName("threadId")
    @Expose
    var threadId: String = ""

    @SerializedName("userId")
    @Expose
    var userId: String = ""

    @SerializedName("messageText")
    @Expose
    var messageText: String = ""

    @SerializedName("type")
    @Expose
    var type: String = ""

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param messageText
     * @param threadId
     * @param userId
     * @param type
     */
    constructor(userId: String, threadId: String, messageText: String, type: String) : super() {
        this.threadId = threadId
        this.userId = userId
        this.messageText = messageText
        this.type = type
    }

}