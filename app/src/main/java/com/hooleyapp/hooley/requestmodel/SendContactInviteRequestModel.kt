package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class SendContactInviteRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("eventId")
    @Expose
    var eventId: String = ""

    @SerializedName("invitedContacts")
    @Expose
    var invitedContacts: ArrayList<myObject>? = null

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param eventId
     * @param userId
     * @param invitedContacts
     */
    constructor(userId: String, eventId: String, invitedContacts: ArrayList<myObject>) : super() {
        this.userId = userId
        this.eventId = eventId
        this.invitedContacts = invitedContacts
    }


    class myObject(@field:SerializedName("contactName")
                   var contactName: String, @field:SerializedName("contactNumbers")
                   var contactNumbers: String)
}