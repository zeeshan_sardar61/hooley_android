package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SendStreamMessageRequestModel
/**
 * @param messageText
 * @param eventId
 * @param userId
 */
(@field:SerializedName("userId")
 @field:Expose
 var userId: String, @field:SerializedName("eventId")
 @field:Expose
 var eventId: String, @field:SerializedName("messageText")
 @field:Expose
 var messageText: String)