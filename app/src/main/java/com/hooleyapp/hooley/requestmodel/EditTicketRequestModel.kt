package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class EditTicketRequestModel {

    @SerializedName("eventId")
    @Expose
    var eventId: String = ""
    @SerializedName("startDate")
    @Expose
    var startDate: String = ""
    @SerializedName("endDate")
    @Expose
    var endDate: String = ""
    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("ticketList")
    @Expose
    var ticketList: ArrayList<TicketList>? = null

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param startDate
     * @param eventId
     * @param ticketList
     * @param userId
     * @param endDate
     */
    constructor(eventId: String, startDate: String, endDate: String, userId: String, ticketList: ArrayList<TicketList>) : super() {
        this.eventId = eventId
        this.startDate = startDate
        this.endDate = endDate
        this.userId = userId
        this.ticketList = ticketList
    }

    class TicketList {

        @SerializedName("ticketId")
        @Expose
        var ticketId: String = ""

        @SerializedName("quantity")
        @Expose
        var quantity: Int = 0

        @SerializedName("price")
        @Expose
        var price: Int = 0

        /**
         * No args constructor for use in serialization
         */
        constructor()

        /**
         * @param price
         * @param ticketId
         * @param quantity
         */
        constructor(ticketId: String, quantity: Int, price: Int) : super() {
            this.ticketId = ticketId
            this.quantity = quantity
            this.price = price
        }

    }

}
