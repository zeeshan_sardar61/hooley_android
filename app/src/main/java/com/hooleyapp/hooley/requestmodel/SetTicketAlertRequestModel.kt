package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SetTicketAlertRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("buyTicketId")
    @Expose
    var buyTicketId: Int = 0
    @SerializedName("eventId")
    @Expose
    var eventId: Int = 0
    @SerializedName("setTime")
    @Expose
    var setTime: String = ""
    @SerializedName("isAlertOn")
    @Expose
    var isAlertOn: Boolean = false

    /**
     * No args constructor for use in serialization
     *
     */
    constructor()

    /**
     *
     * @param setTime
     * @param eventId
     * @param isAlertOn
     * @param userId
     * @param buyTicketId
     */
    constructor(userId: String, buyTicketId: Int, eventId: Int, setTime: String, isAlertOn: Boolean) : super() {
        this.userId = userId
        this.buyTicketId = buyTicketId
        this.eventId = eventId
        this.setTime = setTime
        this.isAlertOn = isAlertOn
    }

}