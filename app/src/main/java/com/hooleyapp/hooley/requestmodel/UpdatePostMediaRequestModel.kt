package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UpdatePostMediaRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: Int = 0

    @SerializedName("postId")
    @Expose
    var postId: String = ""

    @SerializedName("eventId")
    @Expose
    var eventId: String = ""

    @SerializedName("caption")
    @Expose
    var caption: String = ""

    @SerializedName("imageUrls")
    @Expose
    var imageUrlList: List<ImageUrlItem>? = null

    @SerializedName("userTagsList")
    @Expose
    var userTagsList: List<TagUserItem>? = null

    constructor(uId: Int, eId: String, pId: String, cap: String, imageList: ArrayList<ImageUrlItem>, tagusers: ArrayList<TagUserItem>) {
        this.userId = uId
        this.eventId = eId
        this.postId = pId
        this.caption = cap
        this.imageUrlList = imageList
        this.userTagsList = tagusers
    }

    class ImageUrlItem {
        @SerializedName("url")
        @Expose
        var url: String = ""

        @SerializedName("isVideo")
        @Expose
        var isVideo: Boolean = false

        constructor(mediaUrl: String, type: Boolean) {
            this.url = mediaUrl
            this.isVideo = type
        }

    }

    class TagUserItem {
        @SerializedName("userId")
        @Expose
        var userId: Int = 0

        @SerializedName("status")
        @Expose
        var status: Int = 0

        constructor(id: Int, stat: Int) {
            this.userId = id
            this.status = stat
        }

    }

}