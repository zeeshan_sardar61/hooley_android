package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class EventImageShareRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("eventId")
    @Expose
    var eventId: String = ""
    @SerializedName("postId")
    @Expose
    var postId: String = ""
    @SerializedName("shareMedia")
    @Expose
    var shareMedia: String = ""

    /**
     * No args constructor for use in serialization
     *
     */
    constructor()

    /**
     *
     * @param eventId
     * @param shareMedia
     * @param userId
     * @param postId
     */
    constructor(userId: String, eventId: String, postId: String, shareMedia: String) : super() {
        this.userId = userId
        this.eventId = eventId
        this.postId = postId
        this.shareMedia = shareMedia
    }

}