package com.hooleyapp.hooley.requestmodel


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AddSearchBookmarkRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""

    @SerializedName("miles")
    @Expose
    var miles: Float = 0.toFloat()

    @SerializedName("isPaid")
    @Expose
    var isPaid: Boolean? = null
    @SerializedName("isFree")
    @Expose
    var isFree: Boolean? = null
    @SerializedName("eventType")
    @Expose
    var eventType: List<EventType>? = null

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param isFree
     * @param userId
     * @param eventType
     * @param miles
     * @param isPaid
     */
    constructor(userId: String, miles: Float, isPaid: Boolean?, isFree: Boolean?, eventType: List<EventType>) : super() {
        this.userId = userId
        this.miles = miles
        this.isPaid = isPaid
        this.isFree = isFree
        this.eventType = eventType
    }

    class EventType {
        @SerializedName("typeId")
        @Expose
        var typeId: String = ""

        /**
         * No args constructor for use in serialization
         */
        constructor()

        /**
         * @param typeId
         */
        constructor(typeId: String) : super() {
            this.typeId = typeId
        }

    }
}