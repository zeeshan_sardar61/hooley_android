package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AddFriendRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("addFriends")
    @Expose
    var addFriends: List<AddFriend>? = null

    /**
     * No args constructor for use in serialization
     *
     */
    constructor()

    /**
     *
     * @param userId
     * @param addFriends
     */
    constructor(userId: String, addFriends: List<AddFriend>) : super() {
        this.userId = userId
        this.addFriends = addFriends
    }

    class AddFriend {

        @SerializedName("friendId")
        @Expose
        var friendId: Int = 0

        /**
         * No args constructor for use in serialization
         *
         */
        constructor()

        /**
         *
         * @param friendId
         */
        constructor(friendId: Int) : super() {
            this.friendId = friendId
        }

    }
}