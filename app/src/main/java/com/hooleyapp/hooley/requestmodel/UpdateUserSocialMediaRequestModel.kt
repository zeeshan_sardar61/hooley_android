package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UpdateUserSocialMediaRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("socialMediaId")
    @Expose
    var socialMediaId: String = ""
    @SerializedName("socialUrl")
    @Expose
    var socialUrl: String = ""
    @SerializedName("isConnected")
    @Expose
    var isConnected: Boolean? = null

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param socialMediaId
     * @param isConnected
     * @param userId
     * @param socialUrl
     */
    constructor(userId: String, socialMediaId: String, socialUrl: String, isConnected: Boolean?) : super() {
        this.userId = userId
        this.socialMediaId = socialMediaId
        this.socialUrl = socialUrl
        this.isConnected = isConnected
    }

}