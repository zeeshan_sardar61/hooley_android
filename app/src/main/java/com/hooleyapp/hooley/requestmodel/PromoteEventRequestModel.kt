package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class PromoteEventRequestModel {

    @SerializedName("promoteRegions")
    @Expose
    var promoteRegions: ArrayList<PromoteRegion>? = null
    @SerializedName("promoteAudiences")
    @Expose
    var promoteAudiences: ArrayList<PromoteAudience>? = null
    @SerializedName("amount")
    @Expose
    var amount: Int = 0
    @SerializedName("startTime")
    @Expose
    var startTime: String = ""
    @SerializedName("endTime")
    @Expose
    var endTime: String = ""
    @SerializedName("eventId")
    @Expose
    var eventId: String = ""
    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("ageFrom")
    @Expose
    var ageFrom: String = ""
    @SerializedName("ageTo")
    @Expose
    var ageTo: String = ""
    @SerializedName("stripeToken")
    @Expose
    var stripeToken: String = ""

    /**
     * No args constructor for use in serialization
     */
    constructor(ageFrom: String) {
        this.ageFrom = ageFrom
    }

    /**
     * @param startTime
     * @param amount
     * @param ageTo
     * @param promoteAudiences
     * @param ageFrom
     * @param eventId
     * @param stripeToken
     * @param userId
     * @param endTime
     * @param promoteRegions
     */
    constructor(promoteRegions: ArrayList<PromoteRegion>, promoteAudiences: ArrayList<PromoteAudience>,
                amount: Int, startTime: String, endTime: String, eventId: String, userId: String, ageFrom: String, ageTo: String, stripeToken: String) : super() {
        this.promoteRegions = promoteRegions
        this.promoteAudiences = promoteAudiences
        this.amount = amount
        this.startTime = startTime
        this.endTime = endTime
        this.eventId = eventId
        this.userId = userId
        this.ageFrom = ageFrom
        this.ageTo = ageTo
        this.stripeToken = stripeToken
    }

    class PromoteAudience {

        @SerializedName("id")
        @Expose
        var id: String = ""
        @SerializedName("name")
        @Expose
        var name: String = ""

        /**
         * No args constructor for use in serialization
         */
        constructor()

        /**
         * @param id
         * @param name
         */
        constructor(id: String, name: String) : super() {
            this.id = id
            this.name = name
        }

    }


    class PromoteRegion {

        @SerializedName("id")
        @Expose
        var id: String = ""
        @SerializedName("promoteRegionsCities")
        @Expose
        var promoteRegionsCities: ArrayList<PromoteRegionsCity>? = null

        /**
         * No args constructor for use in serialization
         */
        constructor()

        /**
         * @param id
         * @param promoteRegionsCities
         */
        constructor(id: String, promoteRegionsCities: ArrayList<PromoteRegionsCity>) : super() {
            this.id = id
            this.promoteRegionsCities = promoteRegionsCities
        }

    }


    class PromoteRegionsCity {

        @SerializedName("id")
        @Expose
        var id: String = ""

        /**
         * No args constructor for use in serialization
         */
        constructor()

        /**
         * @param id
         */
        constructor(id: String) : super() {
            this.id = id
        }

    }

}
