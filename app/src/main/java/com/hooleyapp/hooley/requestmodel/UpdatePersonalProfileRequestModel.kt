package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UpdatePersonalProfileRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""

    @SerializedName("profilePic")
    @Expose
    var profilePic: String = ""
    @SerializedName("firstName")
    @Expose
    var firstName: String = ""
    @SerializedName("lastName")
    @Expose
    var lastName: String = ""
    @SerializedName("aboutMe")
    @Expose
    var aboutMe: String = ""
    @SerializedName("phoneNo")
    @Expose
    var phoneNo: String = ""
    @SerializedName("homeTown")
    @Expose
    var homeTown: String = ""
    @SerializedName("email")
    @Expose
    var email: String = ""
    @SerializedName("website")
    @Expose
    var website: String = ""
    @SerializedName("genderId")
    @Expose
    var genderId: String = ""
    @SerializedName("birthday")
    @Expose
    var birthday: String
    @SerializedName("relationshipId")
    @Expose
    var relationshipId: String = ""
    @SerializedName("religionId")
    @Expose
    var religionId: String = ""
    @SerializedName("hobbies")
    @Expose
    var hobbies: String = ""
    @SerializedName("studentOf")
    @Expose
    var studentOf: String = ""
    @SerializedName("politicalViewId")
    @Expose
    var politicalViewId: String = ""
    @SerializedName("languagesId")
    @Expose
    var languagesId: String = ""
    @SerializedName("ethinicBackground")
    @Expose
    var ethinicBackground: String = ""
    @SerializedName("userPersonalGeofence")
    @Expose
    var userPersonalGeofence: List<UserPersonalGeofence>? = null
    @SerializedName("astrologicalSignId")
    @Expose
    var astrologicalSignId: String = ""
    @SerializedName("collageMajor")
    @Expose
    var collageMajor: String = ""
    @SerializedName("highSchool")
    @Expose
    var highSchool: String = ""
    @SerializedName("cityId")
    @Expose
    var cityId: String = ""
    @SerializedName("employer")
    @Expose
    var employer: String = ""
    @SerializedName("organization")
    @Expose
    var organization: String = ""
    @SerializedName("occupationId")
    @Expose
    var occupationId: String = ""
    @SerializedName("countryCode")
    @Expose
    var countryCode: String = ""
    @SerializedName("countryISO")
    @Expose
    var countryISO: String = ""
    @SerializedName("countryId")
    @Expose
    var countryId: String = ""
    @SerializedName("stateId")
    @Expose
    var stateId: String = ""

    /**
     * No args constructor for use in serialization
     */
    constructor(birthday: String) {
        this.birthday = birthday
    }

    /**
     * @param birthday
     * @param countryISO
     * @param cityId
     * @param employer
     * @param aboutMe
     * @param studentOf
     * @param organization
     * @param religionId
     * @param userId
     * @param politicalViewId
     * @param firstName
     * @param countryId
     * @param lastName
     * @param ethinicBackground
     * @param website
     * @param collageMajor
     * @param countryCode
     * @param genderId
     * @param hobbies
     * @param astrologicalSignId
     * @param occupationId
     * @param phoneNo
     * @param highSchool
     * @param languagesId
     * @param relationshipId
     * @param email
     * @param stateId
     * @param homeTown
     * @param profilePic
     */
    constructor(userId: String, profilePic: String, firstName: String, lastName: String,
                aboutMe: String, phoneNo: String, homeTown: String, email: String, website: String,
                genderId: String, birthday: String, relationshipId: String, religionId: String, hobbies: String, studentOf: String,
                politicalViewId: String, languagesId: String, ethinicBackground: String,
                astrologicalSignId: String, collageMajor: String, highSchool: String, cityId: String, employer: String, organization: String,
                occupationId: String, countryCode: String, countryISO: String, countryId: String, stateId: String) : super() {
        this.userId = userId
        this.profilePic = profilePic
        this.firstName = firstName
        this.lastName = lastName
        this.aboutMe = aboutMe
        this.phoneNo = phoneNo
        this.homeTown = homeTown
        this.email = email
        this.website = website
        this.genderId = genderId
        this.birthday = birthday
        this.relationshipId = relationshipId
        this.religionId = religionId
        this.hobbies = hobbies
        this.studentOf = studentOf
        this.politicalViewId = politicalViewId
        this.languagesId = languagesId
        this.ethinicBackground = ethinicBackground
        this.userPersonalGeofence = userPersonalGeofence
        this.astrologicalSignId = astrologicalSignId
        this.collageMajor = collageMajor
        this.highSchool = highSchool
        this.cityId = cityId
        this.employer = employer
        this.organization = organization
        this.occupationId = occupationId
        this.countryCode = countryCode
        this.countryISO = countryISO
        this.countryId = countryId
        this.stateId = stateId
    }

    class UserPersonalGeofence {

        @SerializedName("userId")
        @Expose
        var userId: String? = null
        @SerializedName("latitude")
        @Expose
        var latitude: Double = 0.toDouble()
        @SerializedName("longitude")
        @Expose
        var longitude: Double = 0.toDouble()
        @SerializedName("radius")
        @Expose
        var radius: String? = null
        @SerializedName("addressLocation")
        @Expose
        var addressLocation: String? = null


    }


}