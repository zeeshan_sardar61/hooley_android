package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RemoveAllSearchBookMarkRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param userId
     */
    constructor(userId: String) : super() {
        this.userId = userId
    }

}