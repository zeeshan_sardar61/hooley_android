package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UpdateEventActionRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("eventId")
    @Expose
    var eventId: String = ""
    @SerializedName("updateActionId")
    @Expose
    var updateActionId: Int = 0
    @SerializedName("startTime")
    @Expose
    var startTime: String? = null
    @SerializedName("endTime")
    @Expose
    var endTime: String? = null
    @SerializedName("isAlertEmail")
    @Expose
    var isAlertEmail: Boolean? = null
    @SerializedName("isTicketrefund")
    @Expose
    var isTicketrefund: Boolean? = null
    @SerializedName("reasonText")
    @Expose
    var reasonText: String = ""

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param startTime
     * @param isAlertEmail
     * @param eventId
     * @param isTicketrefund
     * @param userId
     * @param reasonText
     * @param endTime
     * @param updateActionId
     */
    constructor(userId: String, eventId: String, updateActionId: Int, startTime: String?, endTime: String?, isAlertEmail: Boolean?, isTicketrefund: Boolean?, reasonText: String) : super() {
        this.userId = userId
        this.eventId = eventId
        this.updateActionId = updateActionId
        this.startTime = startTime
        this.endTime = endTime
        this.isAlertEmail = isAlertEmail
        this.isTicketrefund = isTicketrefund
        this.reasonText = reasonText
    }

}
