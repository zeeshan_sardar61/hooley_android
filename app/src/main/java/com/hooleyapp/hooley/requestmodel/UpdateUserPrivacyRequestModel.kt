package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UpdateUserPrivacyRequestModel {

    @SerializedName("id")
    @Expose
    var id: String = ""
    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("privacyId")
    @Expose
    var privacyId: String = ""
    @SerializedName("isActive")
    @Expose
    var isActive: Boolean = false
    @SerializedName("accessPrivacyId")
    @Expose
    var accessPrivacyId: String = "0"

    /**
     * No args constructor for use in serialization
     *
     */
    constructor()

    /**
     *
     * @param isActive
     * @param id
     * @param privacyId
     * @param userId
     */
    constructor(id: String, userId: String, privacyId: String, isActive: Boolean) : super() {
        this.id = id
        this.userId = userId
        this.privacyId = privacyId
        this.isActive = isActive
    }

    constructor(userId: String, privacyId: String, isActive: Boolean, accessPrivacyId: String) : super() {
        this.userId = userId
        this.privacyId = privacyId
        this.isActive = isActive
        this.accessPrivacyId = accessPrivacyId
    }


}