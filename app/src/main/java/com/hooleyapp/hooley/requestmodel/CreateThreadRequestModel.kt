package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CreateThreadRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("friendId")
    @Expose
    var friendId: String = ""

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param userId
     * @param friendId
     */
    constructor(userId: String, friendId: String) : super() {
        this.userId = userId
        this.friendId = friendId
    }

}
