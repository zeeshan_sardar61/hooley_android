package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by adilmalik on 20/10/2018.
 */

class LikeEventRequestModel(@field:SerializedName("eventId")
                            @field:Expose
                            private val eventId: String, @field:SerializedName("userId")
                            @field:Expose
                            private val userId: String)
