package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UpdateBusinessProfileRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("businessName")
    @Expose
    var businessName: String = ""
    @SerializedName("profilePic")
    @Expose
    var profilePic: String = ""
    @SerializedName("aboutBusiness")
    @Expose
    var aboutBusiness: String = ""
    @SerializedName("businessPhone")
    @Expose
    var businessPhone: String = ""
    @SerializedName("businessEmail")
    @Expose
    var businessEmail: String = ""
    @SerializedName("businessWebsite")
    @Expose
    var businessWebsite: String = ""
    @SerializedName("contactName")
    @Expose
    var contactName: String = ""
    @SerializedName("companyTypeId")
    @Expose
    var companyTypeId: String = ""
    @SerializedName("industryId")
    @Expose
    var industryId: String = ""
    @SerializedName("address")
    @Expose
    var address: String = ""
    @SerializedName("cityId")
    @Expose
    var cityId: String = ""
    @SerializedName("stateId")
    @Expose
    var stateId: String = ""
    @SerializedName("zipCode")
    @Expose
    var zipCode: String = ""
    @SerializedName("countryId")
    @Expose
    var countryId: String = ""
    @SerializedName("countryCode")
    @Expose
    var countryCode: String = ""
    @SerializedName("countryISO")
    @Expose
    var countryISO: String = ""
    @SerializedName("userSocial")
    @Expose
    var userSocial: List<UserSocial>? = null

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param countryId
     * @param companyTypeId
     * @param businessName
     * @param userSocial
     * @param countryISO
     * @param businessPhone
     * @param cityId
     * @param countryCode
     * @param aboutBusiness
     * @param address
     * @param stateId
     * @param businessWebsite
     * @param contactName
     * @param zipCode
     * @param userId
     * @param profilePic
     * @param businessEmail
     * @param industryId
     */
    constructor(userId: String, businessName: String, profilePic: String, aboutBusiness: String, businessPhone: String, businessEmail: String, businessWebsite: String, contactName: String, companyTypeId: String, industryId: String, address: String, cityId: String, stateId: String, zipCode: String, countryId: String, countryCode: String, countryISO: String, userSocial: List<UserSocial>) : super() {
        this.userId = userId
        this.businessName = businessName
        this.profilePic = profilePic
        this.aboutBusiness = aboutBusiness
        this.businessPhone = businessPhone
        this.businessEmail = businessEmail
        this.businessWebsite = businessWebsite
        this.contactName = contactName
        this.companyTypeId = companyTypeId
        this.industryId = industryId
        this.address = address
        this.cityId = cityId
        this.stateId = stateId
        this.zipCode = zipCode
        this.countryId = countryId
        this.countryCode = countryCode
        this.countryISO = countryISO
        this.userSocial = userSocial
    }

    class UserSocial {

        @SerializedName("id")
        @Expose
        var id: Int = 0
        @SerializedName("userId")
        @Expose
        var userId: String = ""
        @SerializedName("socialUrl")
        @Expose
        var socialUrl: String = ""
        @SerializedName("socialName")
        @Expose
        var socialName: String = ""

        /**
         * No args constructor for use in serialization
         */
        constructor()

        /**
         * @param id
         * @param userId
         * @param socialName
         * @param socialUrl
         */
        constructor(id: Int, userId: String, socialUrl: String, socialName: String) : super() {
            this.id = id
            this.userId = userId
            this.socialUrl = socialUrl
            this.socialName = socialName
        }

    }


}