package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ConnectStripeRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("code")
    @Expose
    var code: String = ""

    /**
     * No args constructor for use in serialization
     *
     */
    constructor()

    /**
     *
     * @param userId
     * @param code
     */
    constructor(userId: String, code: String) : super() {
        this.userId = userId
        this.code = code
    }

}