package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ForgetPasswordRequestModel(@field:SerializedName("email")
                                 @field:Expose
                                 var email: String)

