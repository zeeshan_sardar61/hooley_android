package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RemoveThreadRequestModel {

    @SerializedName("threadId")
    @Expose
    var threadId: String = ""
    @SerializedName("userId")
    @Expose
    var userId: String = ""

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param threadId
     * @param userId
     */
    constructor(threadId: String, userId: String) : super() {
        this.threadId = threadId
        this.userId = userId
    }

}