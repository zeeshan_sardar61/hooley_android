package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class SeeAllNearByEventMapViewPolygonFeedsModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""

    @SerializedName("geoFenceList")
    @Expose
    var geoFenceList: ArrayList<GeoFenceList>? = null

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param geoFenceList
     * @param userId
     */
    constructor(userId: String, geoFenceList: ArrayList<GeoFenceList>) : super() {
        this.userId = userId
        this.geoFenceList = geoFenceList
    }

    class GeoFenceList {

        @SerializedName("latitude")
        @Expose
        var latitude: Double? = null
        @SerializedName("longitude")
        @Expose
        var longitude: Double? = null

        /**
         * No args constructor for use in serialization
         */
        constructor()

        /**
         * @param longitude
         * @param latitude
         */
        constructor(latitude: Double?, longitude: Double?) : super() {
            this.latitude = latitude
            this.longitude = longitude
        }

    }

}
