package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class InvitePeopleNearByMeRequestModel
/**
 * @param eventId
 * @param userId
 * @param eventName
 * @param userslist
 */
(@field:SerializedName("userId")
 @field:Expose
 var userId: String, @field:SerializedName("eventId")
 @field:Expose
 var eventId: String, @field:SerializedName("eventName")
 @field:Expose
 var eventName: String, userslist: ArrayList<Userslist>) {
    @SerializedName("userslist")
    @Expose
    var userslist: ArrayList<Userslist>? = null


    init {
        this.userslist = userslist
    }

    class Userslist
    /**
     * @param userId
     */
    (@field:SerializedName("userId")
     @field:Expose
     var userId: String)
}
