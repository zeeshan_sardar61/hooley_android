package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RemoveSingleSearchBookMarkRequestModel {

    @SerializedName("id")
    @Expose
    var id: String = ""
    @SerializedName("userId")
    @Expose
    var userId: String = ""

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param id
     * @param userId
     */
    constructor(id: String, userId: String) : super() {
        this.id = id
        this.userId = userId
    }

}