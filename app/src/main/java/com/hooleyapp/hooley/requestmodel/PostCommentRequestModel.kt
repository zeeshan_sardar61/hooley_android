package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by adilmalik on 20/10/2018.
 */

class PostCommentRequestModel
/**
 *
 * @param commentText
 * @param eventId
 * @param userId
 * @param postId
 */
(@field:SerializedName("userId")
 @field:Expose
 var userId: String, @field:SerializedName("eventId")
 @field:Expose
 var eventId: String, @field:SerializedName("postId")
 @field:Expose
 var postId: String, @field:SerializedName("commentText")
 @field:Expose
 var commentText: String)
