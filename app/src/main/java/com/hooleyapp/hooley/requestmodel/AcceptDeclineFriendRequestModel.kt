package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AcceptDeclineFriendRequestModel {

    @SerializedName("friendId")
    @Expose
    var friendId: Int = 0
    @SerializedName("userId")
    @Expose
    lateinit var userId: String
    @SerializedName("isAccept")
    @Expose
    var isAccept: Boolean? = null

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param userId
     * @param isAccept
     * @param friendId
     */
    constructor(userId: String, friendId: Int, isAccept: Boolean?) : super() {
        this.friendId = friendId
        this.userId = userId
        this.isAccept = isAccept
    }

}