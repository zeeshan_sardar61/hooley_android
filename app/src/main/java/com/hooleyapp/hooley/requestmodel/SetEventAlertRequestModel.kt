package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SetEventAlertRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("eventId")
    @Expose
    var eventId: Int = 0
    @SerializedName("dateTime")
    @Expose
    var dateTime: String = ""
    @SerializedName("isAlertOn")
    @Expose
    var isAlertOn: Boolean = false

    /**
     * No args constructor for use in serialization
     *
     */
    constructor()

    /**
     *
     * @param dateTime
     * @param eventId
     * @param isAlertOn
     * @param userId
     */
    constructor(userId: String, eventId: Int, dateTime: String, isAlertOn: Boolean) : super() {
        this.userId = userId
        this.eventId = eventId
        this.dateTime = dateTime
        this.isAlertOn = isAlertOn
    }

}