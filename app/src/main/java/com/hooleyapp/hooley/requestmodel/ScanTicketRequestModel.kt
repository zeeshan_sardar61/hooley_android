package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ScanTicketRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("eventId")
    @Expose
    var eventId: String = ""
    @SerializedName("qrScanInfo")
    @Expose
    lateinit var qrScanInfo: QrScanInfo

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param qrScanInfo
     * @param eventId
     * @param userId
     */
    constructor(userId: String, eventId: String, qrScanInfo: QrScanInfo) : super() {
        this.userId = userId
        this.eventId = eventId
        this.qrScanInfo = qrScanInfo
    }

    class QrScanInfo {

        @SerializedName("eventName")
        @Expose
        var eventName: String = ""
        @SerializedName("price")
        @Expose
        var price: String = ""
        @SerializedName("startTime")
        @Expose
        var startTime: String = ""
        @SerializedName("endTime")
        @Expose
        var endTime: String = ""
        @SerializedName("ticketId")
        @Expose
        var ticketId: String = ""

        /**
         * No args constructor for use in serialization
         */
        constructor()

        /**
         * @param startTime
         * @param ticketId
         * @param price
         * @param eventName
         * @param endTime
         */
        constructor(eventName: String, price: String, startTime: String, endTime: String, ticketId: String) : super() {
            this.eventName = eventName
            this.price = price
            this.startTime = startTime
            this.endTime = endTime
            this.ticketId = ticketId
        }

    }

}