package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SocialLoginRequestModel
/**
 * @param lastName
 * @param email
 * @param deviceType
 * @param deviceToken
 * @param socialId
 * @param loginMedia
 * @param profilePic
 * @param gender
 * @param firstName
 */
(@field:SerializedName("email")
 @field:Expose
 private val email: String, @field:SerializedName("socialId")
 @field:Expose
 private val socialId: String, @field:SerializedName("deviceToken")
 @field:Expose
 private val deviceToken: String, @field:SerializedName("firstName")
 @field:Expose
 private val firstName: String, @field:SerializedName("lastName")
 @field:Expose
 private val lastName: String,
 @field:SerializedName("profilePic")
 @field:Expose
 private val profilePic: String,
 @field:SerializedName("deviceType")
 @field:Expose
 private val deviceType: String, @field:SerializedName("loginMedia")
 @field:Expose
 private val loginMedia: String, @field:SerializedName("socialToken")
 @field:Expose
 private val socialToken: String, @field:SerializedName("isParamCompleted")
 @field:Expose
 private val isParamCompleted: Boolean)