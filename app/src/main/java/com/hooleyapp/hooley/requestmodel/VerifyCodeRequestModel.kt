package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VerifyCodeRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("apiMessageId")
    @Expose
    lateinit var apiMessageId: String
    @SerializedName("code")
    @Expose
    lateinit var code: String
    @SerializedName("isBusinessProfile")
    @Expose
    var isBusinessProfile: Boolean = false

    /**
     * No args constructor for use in serialization
     *
     */
    constructor()

    /**
     *
     * @param apiMessageId
     * @param userId
     * @param code
     * @param isBusinessProfile
     */
    constructor(userId: String, apiMessageId: String, code: String, isBusinessProfile: Boolean) : super() {
        this.userId = userId
        this.apiMessageId = apiMessageId
        this.code = code
        this.isBusinessProfile = isBusinessProfile
    }

}