package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class UpdatePersonalGeofenceRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("myGeofence")
    @Expose
    var myGeofence: ArrayList<MyGeofence>? = null

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param userId
     * @param myGeofence
     */
    constructor(userId: String, myGeofence: ArrayList<MyGeofence>) : super() {
        this.userId = userId
        this.myGeofence = myGeofence
    }


    class MyGeofence {

        @SerializedName("latitude")
        @Expose
        var latitude: Double? = null
        @SerializedName("longitude")
        @Expose
        var longitude: Double? = null
        @SerializedName("radius")
        @Expose
        var radius: String = ""
        @SerializedName("address")
        @Expose
        var addressLocation: String = ""

        /**
         * @param addressLocation
         * @param radius
         * @param longitude
         * @param latitude
         */
        constructor(latitude: Double?, longitude: Double?, radius: String, addressLocation: String) : super() {
            this.latitude = latitude
            this.longitude = longitude
            this.radius = radius
            this.addressLocation = addressLocation
        }

        /**
         * No args constructor for use in serialization
         */
        constructor()
    }
}