package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by adilmalik on 20/10/2018.
 */

class ShareEventRequestModel
/**
 * @param eventId
 * @param shareMedia
 * @param userId
 */
(@field:SerializedName("userId")
 @field:Expose
 var userId: String, @field:SerializedName("eventId")
 @field:Expose
 var eventId: String, @field:SerializedName("shareMedia")
 @field:Expose
 var shareMedia: String)
