package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class ConstactSyncRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("userContacts")
    @Expose
    var userContacts: ArrayList<UserContact>? = null

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param userContacts
     * @param userId
     */
    constructor(userId: String, userContacts: ArrayList<UserContact>) : super() {
        this.userId = userId
        this.userContacts = userContacts
    }


    class UserContact {

        @SerializedName("name")
        @Expose
        var name: String = ""
        @SerializedName("contactNumber")
        @Expose
        var contactNumber: String = ""

        /**
         * No args constructor for use in serialization
         */
        constructor()

        /**
         * @param contactNumber
         * @param name
         */
        constructor(name: String, contactNumber: String) : super() {
            this.name = name
            this.contactNumber = contactNumber
        }

    }


}