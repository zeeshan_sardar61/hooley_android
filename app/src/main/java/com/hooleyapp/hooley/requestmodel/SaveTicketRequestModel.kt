package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*


class SaveTicketRequestModel {


    @SerializedName("saveTicket")
    @Expose
    lateinit var ticketObj: TicketObj


    constructor(obj: TicketObj) {
        this.ticketObj = obj
    }

    class TicketObj {
        @SerializedName("ticketInfo")
        @Expose
        var ticketInfo: ArrayList<TicketInfo>? = null
        @SerializedName("incomeShare")
        @Expose
        var incomeShare: ArrayList<IncomeShare>? = null

        @SerializedName("isRefundAllow")
        @Expose
        var isRefundAllow: Boolean = false

        @SerializedName("charityTarget")
        @Expose
        var charityTarget: Int = 0

        @SerializedName("payPerViewPrice")
        @Expose
        var payPerViewPrice: Int = 0


        /**
         * No args constructor for use in serialization
         */
        constructor()

        /**
         * @param incomeShare
         * @param onlineSaleEnd
         * @param eventId
         * @param onlineSaleStart
         * @param ticketInfo
         */


        constructor(isRefundAllow_: Boolean, charityTarget_: Int, payperView_: Int, ticketInfo: ArrayList<TicketInfo>?, incomeShare: ArrayList<IncomeShare>?) : super() {
            this.ticketInfo = ticketInfo
            this.incomeShare = incomeShare
            this.isRefundAllow = isRefundAllow_
            this.charityTarget = charityTarget_
            this.payPerViewPrice = payperView_
        }


        class IncomeShare {

            @SerializedName("userId")
            @Expose
            var userId: String = ""
            @SerializedName("percentage")
            @Expose
            var percentage: Int = 0

            /**
             * No args constructor for use in serialization
             */
            constructor()

            /**
             * @param percentage
             * @param userId
             */
            constructor(userId: String, percentage: Int) : super() {
                this.userId = userId
                this.percentage = percentage
            }

        }


        class TicketInfo {

            @SerializedName("ticketName")
            @Expose
            var ticketName: String = ""
            @SerializedName("quantity")
            @Expose
            var quantity: String = ""
            @SerializedName("price")
            @Expose
            var price: String = ""

            /**
             * No args constructor for use in serialization
             */
            constructor()

            /**
             * @param price
             * @param ticketName
             * @param quantity
             */
            constructor(ticketName: String, quantity: String, price: String) : super() {
                this.ticketName = ticketName
                this.quantity = quantity
                this.price = price
            }

        }


    }
}