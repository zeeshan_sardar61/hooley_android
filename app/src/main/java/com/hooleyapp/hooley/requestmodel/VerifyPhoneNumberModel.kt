package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.SerializedName

/**
 * Created by Nauman on 1/30/2018.
 */

class VerifyPhoneNumberModel {
    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("apiMessageId")
    var apiMessageId: String? = null


    @SerializedName("userInfo")
    var userInfo: UserInfoo? = null

    inner class UserInfoo {
        @SerializedName("email")
        var email: String? = null

        @SerializedName("fullName")
        var fullName: String? = null

        @SerializedName("profilePic")
        var profilePic: String? = null
    }

}


