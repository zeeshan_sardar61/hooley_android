package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by adilmalik on 20/10/2018.
 */

class DeleMediaRequestModel
/**
 * @param mediaId
 * @param eventId
 * @param userId
 */
(@field:SerializedName("mediaId")
 @field:Expose
 var mediaId: String, @field:SerializedName("userId")
 @field:Expose
 var userId: String, @field:SerializedName("eventId")
 @field:Expose
 var eventId: String)
