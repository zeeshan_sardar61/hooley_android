package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FeedBackRequestModel {

    @SerializedName("userId")
    @Expose
    var userId: String = ""
    @SerializedName("feedbackMessage")
    @Expose
    var feedbackMessage: String = ""
    @SerializedName("rateCount")
    @Expose
    var rateCount: String = ""
    @SerializedName("isGuest")
    @Expose
    var isGuest: Boolean = false

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param rateCount
     * @param userId
     * @param feedbackMessage
     */
    constructor(userId: String, feedbackMessage: String, rateCount: String, isGuest_: Boolean) : super() {
        this.userId = userId
        this.feedbackMessage = feedbackMessage
        this.rateCount = rateCount
        this.isGuest = isGuest_
    }

}