package com.hooleyapp.hooley.requestmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RegisterRequestModel {

    @SerializedName("firstName")
    @Expose
    var firstName: String = ""
    @SerializedName("lastName")
    @Expose
    var lastName: String = ""
    @SerializedName("email")
    @Expose
    var email: String = ""
    @SerializedName("password")
    @Expose
    var password: String = ""
    @SerializedName("gender")
    @Expose
    var gender: Int = 0
    @SerializedName("profilePic")
    @Expose
    var profilePic: String = ""
    @SerializedName("loginMedia")
    @Expose
    var loginMedia: String = ""
    @SerializedName("isEmailFound")
    @Expose
    var isEmailFound: Boolean = false
    @SerializedName("socialId")
    @Expose
    var socialId: String = ""
    @SerializedName("socialToken")
    @Expose
    var socialToken: String = ""

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param lastName
     * @param email
     * @param loginMedia
     * @param profilePic
     * @param gender
     * @param firstName
     * @param password   "isEmailFound": true,
     * "socialId": "string",
     * "socialToken": "string"
     */
    constructor(firstName: String, lastName: String, email: String, password: String, profilePic: String, loginMedia: String, isEmailFound: Boolean, socialId: String, socialToken: String) : super() {
        this.firstName = firstName
        this.lastName = lastName
        this.email = email
        this.password = password
        this.profilePic = profilePic
        this.loginMedia = loginMedia
        this.isEmailFound = isEmailFound
        this.socialId = socialId
        this.socialToken = socialToken
    }

}
