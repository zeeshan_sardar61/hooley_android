package com.hooleyapp.hooley.Bubbles;


import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.hooleyapp.hooley.HooleyApp;
import com.hooleyapp.hooley.R;
import com.hooleyapp.hooley.log.Logger;
import com.hooleyapp.hooley.others.Constants;

import java.util.Calendar;

public class BubbleLayout extends BubbleBaseLayout {
    static final int MIN_DISTANCE = 20;
    static final int MAX_SWIPE_TIME = 500;
    private static final int TOUCH_TIME_THRESHOLD = 150;
    private static final int LONG_TOUCH_TIME_THRESHOLD = 650;
    private float initialTouchX;
    private float initialTouchY;
    private int initialX;
    private int initialY;
    private OnBubbleRemoveListener onBubbleRemoveListener;
    private OnBubbleClickListener onBubbleClickListener;
    private onBubbleLongClickListener onBubbleLongClickListener;
    private long lastTouchDown;
    private MoveAnimator animator;
    private int width;
    private WindowManager windowManager;
    private boolean shouldStickToWall = true;
    private boolean isBubbleUse = true;
    private float x1, x2;
    private long startClickTime;

    public BubbleLayout(Context context) {
        super(context);
        animator = new MoveAnimator();
        windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        initializeView();
    }

    public BubbleLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        animator = new MoveAnimator();
        windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        initializeView();
    }

    public BubbleLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        animator = new MoveAnimator();
        windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        initializeView();
    }

    public void setOnBubbleRemoveListener(OnBubbleRemoveListener listener) {
        onBubbleRemoveListener = listener;
    }

    public void setOnBubbleClickListener(OnBubbleClickListener listener) {
        onBubbleClickListener = listener;
    }

    public void setOnBubbleLongClickListener(onBubbleLongClickListener listener) {
        onBubbleLongClickListener = listener;
    }

    public void setShouldStickToWall(boolean shouldStick) {
        this.shouldStickToWall = shouldStick;
    }

    public void isBubbleUse(boolean bubbleUse) {
        this.isBubbleUse = bubbleUse;
    }

    void notifyBubbleRemoved() {
        if (onBubbleRemoveListener != null) {
            onBubbleRemoveListener.onBubbleRemoved(this);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initializeView() {
        setClickable(true);
        this.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        startClickTime = Calendar.getInstance().getTimeInMillis();
                        x1 = event.getX();
                        break;
                    case MotionEvent.ACTION_UP:
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        x2 = event.getX();
                        float deltaX = x2 - x1;
                        if (Math.abs(deltaX) > MIN_DISTANCE && clickDuration < MAX_SWIPE_TIME) {
                            HooleyApp.Companion.getDb().putBoolean(Constants.BUBBLE_REMOVE_BY_USER, true);
                            notifyBubbleRemoved();
                        } else if (deltaX > MIN_DISTANCE && clickDuration < MAX_SWIPE_TIME) {
                            HooleyApp.Companion.getDb().putBoolean(Constants.BUBBLE_REMOVE_BY_USER, true);
                            notifyBubbleRemoved();
                        } else {
                            // consider as something else - a screen tap for example

                        }
                        break;
                }
                return false;
            }
        });
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        playAnimation();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event != null) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (!HooleyApp.Companion.getDb().getBoolean(Constants.IS_BUBBLE_CLICKABLE))
                        return true;
                    initialX = getViewParams().x;
                    initialY = getViewParams().y;
                    initialTouchX = event.getRawX();
                    initialTouchY = event.getRawY();
//                    playAnimationClickDown();
                    lastTouchDown = System.currentTimeMillis();
//                    animator.stop();
                    updateSize();
                    Logger.INSTANCE.e("Motion event ACTION_DOWN ==> " + initialX + "/" + initialX);
                    Logger.INSTANCE.e("Motion Initial touch ACTION_DOWN ==> " + initialTouchX + "/" + initialTouchY);
                    break;
                case MotionEvent.ACTION_MOVE:
                    int x = initialX + (int) (event.getRawX() - initialTouchX);
                    int y = initialY + (int) (event.getRawY() - initialTouchY);
                    getViewParams().x = x;
                    getViewParams().y = y;
                    getWindowManager().updateViewLayout(this, getViewParams());
                    if (getLayoutCoordinator() != null) {
                        if (isBubbleUse)
                            getLayoutCoordinator().notifyBubblePositionChanged(this, x, y);
                    }
                    if (System.currentTimeMillis() - lastTouchDown > 200) {
                        if (isBubbleUse) {
                            if (Math.abs(initialTouchX - event.getRawX()) < 5 && Math.abs(initialTouchY - event.getRawY()) < 5) {
                                if (!HooleyApp.Companion.getDb().getBoolean(Constants.BUBBLE_REMOVE_BY_USER)) {
                                    if (getLayoutCoordinator() != null) {
                                        getLayoutCoordinator().notifyBubbleRelease(this);
                                    }
                                    if (onBubbleLongClickListener != null) {
                                        onBubbleLongClickListener.onBubbleLongClickListener();
                                        isBubbleUse = false;
                                    }
                                }
                            }
                        }
                    } else {

                    }
                    Logger.INSTANCE.e("Motion event ACTION_MOVE ==> " + x + "/" + y);
                    Logger.INSTANCE.e("Motion Initial ACTION_MOVE ==> " + initialX + "/" + initialY);
                    Logger.INSTANCE.e("Motion Initial touch ACTION_MOVE ==> " + event.getRawX() + "/" + event.getRawY());
                    break;
                case MotionEvent.ACTION_UP:

//                    if (getLayoutCoordinator() != null) {
//                        getLayoutCoordinator().notifyBubbleRelease(this);
//                        playAnimationClickUp();
//                    }

                    if (System.currentTimeMillis() - lastTouchDown < TOUCH_TIME_THRESHOLD) {
                        if (onBubbleClickListener != null) {
                            onBubbleClickListener.onBubbleClick(this);
                            isBubbleUse = false;
                        }
                    } else {

                    }
                    goToWall();
                    Logger.INSTANCE.e("Motion event ACTION_UP ==> " + getViewParams().x + "/" + getViewParams().y);

                    break;
                case MotionEvent.ACTION_CANCEL:
                    Log.e("Cancel", "Cancel");
                    break;
            }
        }
        return super.

                onTouchEvent(event);
    }

    private void playAnimation() {
        if (!isInEditMode()) {
            AnimatorSet animator = (AnimatorSet) AnimatorInflater
                    .loadAnimator(getContext(), R.animator.bubble_shown_animator);
            animator.setTarget(this);
            animator.start();
        }
    }

    private void playAnimationClickDown() {
        if (!isInEditMode()) {
            AnimatorSet animator = (AnimatorSet) AnimatorInflater
                    .loadAnimator(getContext(), R.animator.bubble_down_click_animator);
            animator.setTarget(this);
            animator.start();
        }
    }

    private void playAnimationClickUp() {
        if (!isInEditMode()) {
            AnimatorSet animator = (AnimatorSet) AnimatorInflater
                    .loadAnimator(getContext(), R.animator.bubble_up_click_animator);
            animator.setTarget(this);
            animator.start();
        }
    }

    private void updateSize() {
        DisplayMetrics metrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(metrics);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = (size.x - this.getWidth());

    }

    public void goToWall() {
        if (shouldStickToWall) {
            int middle = width / 2;
            float nearestXWall = getViewParams().x >= middle ? width : 0;
            animator.start(nearestXWall, getViewParams().y);
        }
    }

    private void move(float deltaX, float deltaY) {
        getViewParams().x += deltaX;
        getViewParams().y += deltaY;
        windowManager.updateViewLayout(this, getViewParams());
    }

    public interface OnBubbleRemoveListener {
        void onBubbleRemoved(BubbleLayout bubble);
    }

    public interface OnBubbleClickListener {
        void onBubbleClick(BubbleLayout bubble);
    }

    public interface onBubbleLongClickListener {
        void onBubbleLongClickListener();
    }

    private class MoveAnimator implements Runnable {
        private Handler handler = new Handler(Looper.getMainLooper());
        private float destinationX;
        private float destinationY;
        private long startingTime;

        private void start(float x, float y) {
            this.destinationX = x;
            this.destinationY = y;
            startingTime = System.currentTimeMillis();
            handler.post(this);
        }

        @Override
        public void run() {
            if (getRootView() != null && getRootView().getParent() != null) {
                float progress = Math.min(1, (System.currentTimeMillis() - startingTime) / 400f);
                float deltaX = (destinationX - getViewParams().x) * progress;
                float deltaY = (destinationY - getViewParams().y) * progress;
                move(deltaX, deltaY);
                if (progress < 1) {
                    handler.post(this);
                }
            }
        }

        private void stop() {
            handler.removeCallbacks(this);
        }
    }
}