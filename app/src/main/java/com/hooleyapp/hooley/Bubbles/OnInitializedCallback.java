package com.hooleyapp.hooley.Bubbles;

public interface OnInitializedCallback {
    void onInitialized();
}