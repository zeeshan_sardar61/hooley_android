package com.hooleyapp.hooley.fragments.event

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragementCreateEventOptionsBinding
import com.hooleyapp.hooley.fragments.BaseFragment

class CreateEventOptionsFragment : BaseFragment() {

    lateinit var binding: FragementCreateEventOptionsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragement_create_event_options, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.rlCreateEvent.setOnClickListener {
            (HooleyMain.activity as ActivityBase).callFragment(R.id.container, CreateEventFragment.newInstance("1"), "CreateEventFragment")
        }

        binding.rlPaidEvent.setOnClickListener {
            (HooleyMain.activity as ActivityBase).callFragment(R.id.container, CreateEventFragment.newInstance("2"), "CreatePaidEventFragment")

        }

        binding.rlCharityEvent.setOnClickListener {
            (HooleyMain.activity as ActivityBase).callFragment(R.id.container, CreateEventFragment.newInstance("3"), "CreateCharityEventFragment")

        }
    }
}