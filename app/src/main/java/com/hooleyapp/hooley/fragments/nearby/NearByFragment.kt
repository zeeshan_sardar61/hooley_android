package com.hooleyapp.hooley.fragments.nearby

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.app.Dialog
import android.app.PendingIntent
import android.app.UiModeManager.MODE_NIGHT_YES
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.content.res.Resources
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Point
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.*
import com.facebook.CallbackManager
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterNearByEvents
import com.hooleyapp.hooley.app.view.ui.fragments.event.EventDetailAnnouncementFragment
import com.hooleyapp.hooley.databinding.FragmentNearByBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.event.EventPagerFragment
import com.hooleyapp.hooley.fragments.event.LiveEventFragment
import com.hooleyapp.hooley.fragments.event.ViewSalesFragment
import com.hooleyapp.hooley.fragments.media.AllFeedsSinglePostCommentFragment
import com.hooleyapp.hooley.fragments.messaging.ChatWindowFragment
import com.hooleyapp.hooley.fragments.notification.NotificationMainFragment
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.fragments.search.SearchFilterFragment
import com.hooleyapp.hooley.fragments.ticket.MyTicketFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.INearByEventClickListener
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.*
import com.hooleyapp.hooley.others.*
import com.hooleyapp.hooley.services.EventWebService
import com.hooleyapp.hooley.services.GeoFenceWebService
import com.hooleyapp.hooley.social.SocialClass
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.text.DecimalFormat
import java.util.*

/**
 * Created by Nauman on 12/11/2017.
 */

open class NearByFragment : BaseFragment(),
        RadioGroup.OnCheckedChangeListener,
        GoogleApiClient.ConnectionCallbacks,
        OnMapReadyCallback,
        SeekBar.OnSeekBarChangeListener,
        View.OnClickListener,
        GoogleMap.OnMarkerClickListener,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        ResultCallback<LocationSettingsResult>,
        INearByEventClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        GoogleMap.OnMapLongClickListener,
        View.OnTouchListener {
    private var list = listOf<Place.Field>()
    private val AUTOCOMPLETE_REQUEST_CODE: Int = 506
    var binding: FragmentNearByBinding? = null
    var mGoogleApiClient: GoogleApiClient? = null
    lateinit var mLocationRequest: LocationRequest
    lateinit var mLocationSettingsRequest: LocationSettingsRequest
    var mCurrentLocation: Location? = null
    lateinit var objectResponse: NearByEventListFeedModel
    lateinit var objectMapResponse: NearByEventMapFeedModel
    internal var mList: ArrayList<NearByEventsFinalModel>? = ArrayList()
    internal var isSwiped = false
    lateinit var adapter: AdapterNearByEvents
    var mRequestingLocationUpdates: Boolean? = true
    internal var mGooglemap: GoogleMap? = null
    lateinit var locationManager: LocationManager
    lateinit var mapFragment: SupportMapFragment
    var ct = "CT_" + javaClass.simpleName
    var mapFinalList: ArrayList<MapListFinalModel>? = ArrayList()
    lateinit var nearByEvents: NearByEventsFinalModel
    internal var bold: Typeface? = null
    internal var regular: Typeface? = null
    private var mapCircle: Circle? = null
    private var markerOption: MarkerOptions? = null
    private var position = 0
    private var showInfoDialog: Dialog? = null
    private val socailClass = SocialClass()
    private var callbackManager: CallbackManager? = null
    //    private var twitterAuthClient: TwitterAuthClient? = null
    private val mLatlngs = ArrayList<LatLng>()
    private var circleLat: Double = 0.toDouble()
    private var circleLong: Double = 0.toDouble()
    private var mGestureDetector: GestureDetector? = null
    private var mPolylineOptions: PolylineOptions? = null
    private var mPolygonOptions: PolygonOptions? = null
    private var mDrawFinished = false
    private var isCircle = true
    private var eventService: EventWebService? = EventWebService()
    private var currentProgress = 8046.72f
    private val geoFenceWebService = GeoFenceWebService()
    private var displayInfoDialog = true
    private val gson = Gson()
    var intent: Intent? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_near_by, container, false)
        Util.hideKeyboard(HooleyMain.activity!!)
        callbackManager = socailClass.initFacebookSdk()
//        twitterAuthClient = socailClass.initTwitter()
        bold = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)
        regular = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)
        retainInstance = true

        if (HooleyApp.db.getString(Constants.PERSONAL_RADIUS).isEmpty()) {
            DashBoardFragment.fragmentDashboardBinding!!.sbGeofence.progress = currentProgress.toInt()
            DashBoardFragment.fragmentDashboardBinding!!.tvSbProgress.text = "5 miles"
        } else {
            val f = DecimalFormat("##.00")
            currentProgress = (HooleyApp.db.getString(Constants.PERSONAL_RADIUS).toFloat() * 1609.34).toFloat()
            DashBoardFragment.fragmentDashboardBinding!!.sbGeofence.progress = currentProgress.toInt()
            if (currentProgress < 1609.344) {
                if (currentProgress <= 100) {
                    currentProgress = 100.0f
                    DashBoardFragment.fragmentDashboardBinding!!.sbGeofence.progress = currentProgress.toInt()
                    DashBoardFragment.fragmentDashboardBinding!!.tvSbProgress.text = 100.toString() + " meters"
                } else {
                    DashBoardFragment.fragmentDashboardBinding!!.tvSbProgress.text = f.format(currentProgress) + " meters"
                }
            } else {
                DashBoardFragment.fragmentDashboardBinding!!.tvSbProgress.text = "${f.format(currentProgress / 1609.344)} miles"
            }
        }

        initMap()
        initPlaces()
        setListener()
        getNearByEventsListFeeds()
        DashBoardFragment.fragmentDashboardBinding!!.rbListView.isChecked = true
        DashBoardFragment.fragmentDashboardBinding!!.rbListView.typeface = bold
        DashBoardFragment.fragmentDashboardBinding!!.rbMapView.typeface = regular
        return binding!!.root
    }

    private fun setSavedData() = if (HooleyApp.db.getString(Constants.EVENT_FEEDS).isNotEmpty()) {
        if (isSwiped) {
            binding!!.sLRecycler.isRefreshing = false
            isSwiped = false
        }

        objectResponse = gson.fromJson(HooleyApp.db.getString(Constants.EVENT_FEEDS), NearByEventListFeedModel::class.java)
        addAllDataInList()
        binding!!.tvNoData.visibility = View.GONE
        binding!!.pbNearBy.visibility = View.GONE
    } else {
        Toast.makeText(HooleyMain.activity, "No Internet Connection Available", Toast.LENGTH_SHORT).show()
    }

    private fun setListener() {
        DashBoardFragment.fragmentDashboardBinding!!.nearByTab.setOnCheckedChangeListener(this)
        DashBoardFragment.fragmentDashboardBinding!!.sbGeofence.setOnSeekBarChangeListener(this)
        DashBoardFragment.fragmentDashboardBinding!!.rgEventType.setOnCheckedChangeListener(this)
        DashBoardFragment.fragmentDashboardBinding!!.ivMyLocation.setOnClickListener(this)
        DashBoardFragment.fragmentDashboardBinding!!.ivFullScreenMap.setOnClickListener(this)
        DashBoardFragment.fragmentDashboardBinding!!.ivSmallScrenMap.setOnClickListener(this)
        mGestureDetector = GestureDetector(context, GestureListener())
        binding!!.drawerView.setOnTouchListener(this)
        binding!!.sLRecycler.setOnRefreshListener(this)
        binding!!.ivMapInfo.setOnClickListener(this)
        DashBoardFragment.fragmentDashboardBinding!!.ivMinus.setOnClickListener(this)
        DashBoardFragment.fragmentDashboardBinding!!.ivPlus.setOnClickListener(this)
        DashBoardFragment.fragmentDashboardBinding!!.ivMinus.setOnLongClickListener {
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    if (DashBoardFragment.fragmentDashboardBinding!!.ivMinus.isPressed) {
                        if (currentProgress >= 100.0f) {
                            var temp = currentProgress
                            temp -= 100f
                            if (temp <= 100.0f) {
                                currentProgress = 100.0f
                            } else {
                                currentProgress -= 100f
                            }
                            DashBoardFragment.fragmentDashboardBinding!!.sbGeofence.progress = currentProgress.toInt()
                        }
                    } else {
                        timer.cancel()
                        updateUI()
                    }
                }
            }, 100, 200)
            true
        }
        DashBoardFragment.fragmentDashboardBinding!!.ivPlus.setOnLongClickListener {
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    if (DashBoardFragment.fragmentDashboardBinding!!.ivPlus.isPressed) {
                        if (currentProgress < 64373.0f) {
                            currentProgress += 100f
                            DashBoardFragment.fragmentDashboardBinding!!.sbGeofence.progress = currentProgress.toInt()
                        }
                    } else {
                        timer.cancel()
                        updateUI()
                    }
                }
            }, 100, 200)

            true
        }
    }

    private fun updateUI() {
        Handler(Looper.getMainLooper()).post {
            if (mapCircle != null) {
                if (currentProgress > 30) {
                    mapCircle!!.radius = currentProgress.toDouble()
                    addCircle(circleLat, circleLong)
                    val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
                    val animateZomm = currentZoomLevel + 5
                    mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat, circleLong), animateZomm))
                    mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
                    getNearByEventsMapFeeds(circleLat, circleLong, currentProgress / 1609.344f)
                }
            }
        }
    }

    private fun initPlaces() {
        list = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)
        binding!!.tvLocationName.setOnClickListener {
            try {
                intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, list).build(HooleyMain.activity!!)
                HooleyMain.activity!!.startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }

        }
    }

    private fun setRecyclerView() {
        DashBoardFragment.fragmentDashboardBinding!!.rbListView.isChecked = true
        DashBoardFragment.fragmentDashboardBinding!!.rbListView.typeface = bold
        DashBoardFragment.fragmentDashboardBinding!!.rbMapView.typeface = regular
        binding!!.rvNearBy.visibility = View.VISIBLE
        binding!!.sLRecycler.visibility = View.VISIBLE
        binding!!.rlMapView.visibility = View.GONE
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_from_right)
        binding!!.rvNearBy.layoutAnimation = animation
        binding!!.rvNearBy.layoutManager = manager
        binding!!.rvNearBy.setEmptyView(binding!!.tvNoData)
        adapter = AdapterNearByEvents(HooleyMain.activity!!, mList!!)
        binding!!.rvNearBy.adapter = adapter
        adapter.setListener(this)

    }

    private fun getNearByEventsListFeeds() {
        if (NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            if (binding != null && !isSwiped)
                binding!!.pbNearBy.visibility = View.VISIBLE
            eventService!!.seeAllNearByEventListViewFeeds(object : IWebServiceCallback<NearByEventListFeedModel> {
                override fun success(result: NearByEventListFeedModel) {
                    if (isSwiped) {
                        binding!!.sLRecycler.isRefreshing = false
                        isSwiped = false
                    }
                    objectResponse = result

                    val data = gson.toJson(objectResponse)
                    HooleyApp.db.putString(Constants.EVENT_FEEDS, data)

                    addAllDataInList()
                    binding!!.tvNoData.visibility = View.GONE
                    binding!!.pbNearBy.visibility = View.GONE
                }

                override fun failure(message: String) {
                    try {
                        if (isSwiped) {
                            binding!!.sLRecycler.isRefreshing = false
                            isSwiped = false
                        }
                        binding!!.rlMapView.visibility = View.GONE
                        binding!!.tvNoData.visibility = View.VISIBLE
                        //                        userSearchbinding.rvNearBy.setVisibility(View.GONE);
                        binding!!.pbNearBy.visibility = View.GONE
                        binding!!.tvNoData.text = message
                    } catch (ex: NullPointerException) {
                        ex.printStackTrace()
                    }

                }

                override fun onTokenExpire() {
                    onTokenExpireLogOut()
                }
            })
        } else {
            setSavedData()
        }

    }

    private fun addAllDataInList() {
        if (mList != null)
            mList!!.clear()
        if (objectResponse.featuredEventsList.size > 0)
            mList!!.add(NearByEventsFinalModel(Constants.TYPE_FEATURE_EVENT, objectResponse.featuredEventsList))
        if (objectResponse.todayEventsList.size > 0)
            mList!!.add(NearByEventsFinalModel(Constants.TYPE_TODAY_EVENT, objectResponse.todayEventsList))
        if (objectResponse.nextWeekEventsList.size > 0)
            mList!!.add(NearByEventsFinalModel(Constants.TYPE_NEXT_WEEK_EVENT, objectResponse.nextWeekEventsList))
        if (objectResponse.commingEventsList.size > 0)
            mList!!.add(NearByEventsFinalModel(Constants.TYPE_COMMING_EVENT, objectResponse.commingEventsList))
        setRecyclerView()
        setNotificationFragment()
    }

    private fun setNotificationFragment() {
        val from_Notification = HooleyApp.db.getBoolean(Constants.IS_FROM_NOTIFICATION)
        HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, false)
        if (from_Notification) {
            HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, false)
            when (HooleyApp.db.getString(Constants.TYPE_NOTIFICATION)) {
                Constants.TYPE_MESSAGES_ALERT -> (HooleyMain.activity as ActivityBase).callFragment(R.id.container, ChatWindowFragment(HooleyApp.db.getString(Constants.THREAD_ID), HooleyApp.db.getString(Constants.FRIEND_ID), HooleyApp.db.getString(Constants.FRIEND_NAME)), "ChatWindowFragment")
                Constants.TYPE_EVENT_ALERTS -> (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment.newInstance(HooleyApp.db.getString(Constants.EVENT_ID)), "LiveEventFragment")
                Constants.TYPE_FRIENDS_ALERT -> {
                    var avatarfragment = ViewAvatarFragment(HooleyApp.db.getString(Constants.FRIEND_UID).toInt())
                    avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
                }
                Constants.TYPE_MEDIA_ALERT -> (HooleyMain.activity as ActivityBase).callFragment(R.id.container, AllFeedsSinglePostCommentFragment.newInstance(HooleyApp.db.getString(Constants.EVENT_ID), HooleyApp.db.getString(Constants.MEDIA_ID)), "AllFeedsSinglePostCommentFragment")
                Constants.TYPE_TICKET_ALERT -> {
                    (HooleyMain.activity as ActivityBase).callFragment(R.id.container, MyTicketFragment(), "MyTicketFragment")
                }
                Constants.TYPE_TICKETSALE_ALERT -> {
                    (HooleyMain.activity as ActivityBase).callFragment(R.id.container, ViewSalesFragment.newInstance(HooleyApp.db.getString(Constants.EVENT_ID)), "ViewSalesFragment")
                }
                Constants.TYPE_UPDATE_ANNOUNCEMENTS_ALERT -> {
                    (HooleyMain.activity!! as ActivityBase).callFragment(R.id.container, EventDetailAnnouncementFragment(HooleyApp.db.getString(Constants.EVENT_ID), false), "EventDetailAnnouncementFragment")
                }
                Constants.TYPE_ANNOUNCEMENTS_ALERT -> {
                    HooleyMain.activity!!.showAnnouncementDialog(HooleyApp.db.getString(Constants.EVENT_ANNOUNCEMENT_MSG),
                            HooleyApp.db.getString(Constants.EVENT_ANNOUNCEMENT_IMGURL),
                            HooleyApp.db.getString(Constants.EVENT_NAME),
                            HooleyApp.db.getString(Constants.EVENT_START_TIME),
                            HooleyApp.db.getString(Constants.EVENT_END_TIME),
                            HooleyApp.db.getString(Constants.EVENT_ADDRESS),
                            object : onClickDialog {
                                override fun onClickYes() {
                                    (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment.newInstance(HooleyApp.db.getString(Constants.EVENT_ID)), "LiveEventFragment")
                                }
                            })
                }
            }
        }

        val from_Deeplink = HooleyApp.db.getBoolean(Constants.IS_DEEPLINK)

        if (from_Deeplink) {
            HooleyApp.db.putBoolean(Constants.IS_DEEPLINK, false)
            gotoEventScreen(HooleyApp.db.getString(Constants.DEEPLINK_URI))
        }
    }

    private fun gotoEventScreen(uri: String) {
        val urlparts = uri.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        var id = ""
        if (urlparts[0].contains("EventPreview")) {
            id = urlparts[1]
            (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment.newInstance(id), "NotificationMainFragment")
        } else {
            val ba = urlparts[1].split("&".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            id = ba[0]
            (HooleyMain.activity as ActivityBase).callFragment(R.id.container, AllFeedsSinglePostCommentFragment.newInstance(urlparts[2], id), "AllFeedsSinglePostCommentFragment")
        }
        hideDrawer(HooleyMain.activity!!)
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(context!!)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            settingsRequest()
        } else {
            startLocationUpdates()
            createLocationRequest()
            buildLocationSettingsRequest()
            checkLocationSettings()
        }
    }

    fun settingsRequest() {
        try {
            mLocationRequest = LocationRequest.create()
            mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            mLocationRequest.interval = (30 * 1000).toLong()
            mLocationRequest.fastestInterval = (5 * 1000).toLong()
            val builder = LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest)
            mLocationSettingsRequest = builder.build()
            builder.setAlwaysShow(true) //this is the key ingredient
            val result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, mLocationSettingsRequest)
            result.setResultCallback { result ->
                val status = result.status
                val state = result.locationSettingsStates
                when (status.statusCode) {
                    LocationSettingsStatusCodes.SUCCESS -> {
                        startLocationUpdates()
                        createLocationRequest()
                        buildLocationSettingsRequest()
                        checkLocationSettings()
                    }
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                        status.startResolutionForResult(HooleyMain.activity, REQUEST_CHECK_SETTINGS)
                    } catch (e: IntentSender.SendIntentException) {
                    }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                    }
                }
            }
        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
        }

    }

    private fun initMap() {
        locationManager = HooleyMain.activity!!.getSystemService(LOCATION_SERVICE) as LocationManager
        if (!googleServicesAvailable()) {
            Toast.makeText(HooleyMain.activity, "google service not available", Toast.LENGTH_SHORT).show()
            return
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission()
        } else {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (mGoogleApiClient != null) {
                    settingsRequest()
                } else {
                    buildGoogleApiClient()
                }
            }
        }
        mapFragment = childFragmentManager.findFragmentById(R.id.mpGeofence) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    fun googleServicesAvailable(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val isAvailable = apiAvailability.isGooglePlayServicesAvailable(context!!)
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true
        } else if (apiAvailability.isUserResolvableError(isAvailable)) {
            val dialog = apiAvailability.getErrorDialog(HooleyMain.activity, isAvailable, 0)
            dialog.show()
        } else {
            Toast.makeText(HooleyMain.activity, "Cant connect ", Toast.LENGTH_SHORT).show()
        }
        return false
    }

    fun checkLocationPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(HooleyMain.activity!!, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(HooleyMain.activity!!, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(HooleyMain.activity!!, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS_REQUEST_LOCATION)
            } else {
                ActivityCompat.requestPermissions(HooleyMain.activity!!, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS_REQUEST_LOCATION)
            }
            return false
        } else {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (mGoogleApiClient != null) {
                    settingsRequest()
                } else {
                    buildGoogleApiClient()
                }
            }
            return true
        }
    }

    private fun startLocationUpdates() {
        try {
            if (ActivityCompat.checkSelfPermission(HooleyMain.activity!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return
            } else {
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        mGoogleApiClient,
                        mLocationRequest,
                        this
                ).setResultCallback { status ->
                    mRequestingLocationUpdates = true
                    if (status.isSuccess) {
                        Log.i(ct, "Status true! Going to load!")
                    } else {
                        Log.e(ct, "There is a problem")
                    }
                    getLastLocation()
                }
            }
        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
        }

    }

    private fun stopLocationUpdates() {
        try {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient,
                    this
            ).setResultCallback { status ->
                mRequestingLocationUpdates = false
                if (status.isSuccess) {
                    Log.i(ct, "stopLocationUpdates-> Status true! Going to load!")
                } else {
                    Log.e(ct, "stopLocationUpdates-> There is a problem")
                }
            }
        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
        }

    }

    fun getLastLocation() {
        if (mRequestingLocationUpdates!!) {
            if (mCurrentLocation != null) {
                Log.e(ct, "getLastLocation-> Now fetched!")
                Log.d(ct, "getLastLocation-> Lat is: " + mCurrentLocation!!.latitude)
                Log.d(ct, "getLastLocation-> Lon is: " + mCurrentLocation!!.longitude)
                stopLocationUpdates()
            } else {
                Log.i(ct, "getLastLocation-> Cannot fetch! (mCurrentLocation)")
            }
        } else {
            Log.i(ct, "getLastLocation-> Cannot fetch! (mRequestingLocationUpdates)")
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu!!.clear()
        inflater!!.inflate(R.menu.menu_main, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.ic_search -> {
                HooleyMain.activity!!.hideKeyboard()
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, SearchFilterFragment.newInstance(), "SearchFilterFragment")
                hideDrawer(HooleyMain.activity!!)
                return true
            }
            R.id.ic_notification -> {
                HooleyMain.activity!!.hideKeyboard()
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, NotificationMainFragment.newInstance(), "NotificationMainFragment")
                hideDrawer(HooleyMain.activity!!)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }


    override fun onConnected(bundle: Bundle?) {
        if (mGooglemap != null)
            mGooglemap!!.uiSettings.isMyLocationButtonEnabled = false
    }

    override fun onConnectionSuspended(i: Int) {}

    override fun onMapReady(googleMap: GoogleMap) {
        mGooglemap = googleMap
        mGooglemap!!.uiSettings.isMapToolbarEnabled = false
        mGooglemap!!.uiSettings.isZoomControlsEnabled = false
        mGooglemap!!.setOnMapLongClickListener(this)

        if (HooleyApp.db.getInt(Constants.TYPE_THEME) == MODE_NIGHT_YES) {
            try {
                // Customise the styling of the base map using a JSON object defined
                // in a raw resource file.
                val success = mGooglemap!!.setMapStyle(MapStyleOptions.loadRawResourceStyle(HooleyMain.activity!!, R.raw.black_map))
                if (!success) {
                    Log.e("", "Style parsing failed.")
                }
            } catch (e: Resources.NotFoundException) {
                Log.e("", "Can't find style. Error: ", e)
            }

        } else {
            try {
                // Customise the styling of the base map using a JSON object defined
                // in a raw resource file.
                val success = mGooglemap!!.setMapStyle(MapStyleOptions.loadRawResourceStyle(HooleyMain.activity!!, R.raw.white_map))
                if (!success) {
                    Log.e("", "Style parsing failed.")
                }
            } catch (e: Resources.NotFoundException) {
                Log.e("", "Can't find style. Error: ", e)
            }
        }
        val personalLatitude = HooleyApp.db.getDouble(Constants.PERSONAL_LAT, 0.0)
        val personalLongitude = HooleyApp.db.getDouble(Constants.PERSONAL_LANG, 0.0)

        if (personalLatitude != 0.0 || personalLongitude != 0.0) {
            circleLat = personalLatitude
            circleLong = personalLongitude
        } else {
            circleLat = HooleyApp.db.getDouble(Constants.CURRENT_LAT, 0.0)
            circleLong = HooleyApp.db.getDouble(Constants.CURRENT_LANG, 0.0)
        }
        moveToLockLocation()
        try {
            binding!!.tvLocationName.text = (HooleyMain.activity as ActivityBase).getCompleteAddressString(circleLat, circleLong)
        } catch (e: KotlinNullPointerException) {
        }
        addCircle(circleLat, circleLong)
        if (mapCircle != null) {
            mapCircle!!.radius = currentProgress.toDouble()
            val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
            val animateZomm = currentZoomLevel + 5
            mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat, circleLong), animateZomm))
            mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
        }
        getNearByEventsMapFeeds(circleLat, circleLong, currentProgress / 1609.344f)
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

    }

    override fun onLocationChanged(location: Location) {
        mCurrentLocation = location
        getLastLocation()
    }

    protected fun checkLocationSettings() {
        try {
            val result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, mLocationSettingsRequest)
            result.setResultCallback(this)
        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(HooleyMain.activity!!, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        initMap()
                    }
                } else {
                    Toast.makeText(HooleyMain.activity, "permission denied", Toast.LENGTH_LONG).show()
                }
                return
            }
        }
    }

    fun moveToCurrentLocation() {
        try {
            if (mCurrentLocation == null) {
                if (HooleyMain.activity != null) {
                    if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    }
                }
                if (mGoogleApiClient != null)
                    mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
                if (mCurrentLocation != null) {
                    val loc = LatLng(mCurrentLocation!!.latitude, mCurrentLocation!!.longitude)
                    if (mGooglemap != null) {
                        mGooglemap!!.clear()
                        val markerOption = MarkerOptions().position(loc).flat(true).draggable(false)
                        markerOption.icon(bitmapDescriptorFromVector(HooleyMain.activity, R.drawable.ic_map_cirlce_centre))
                        mapCircle!!.radius = currentProgress.toDouble()
                        val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
                        val animateZomm = currentZoomLevel + 5
                        mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat, circleLong), animateZomm))
                        mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
                        mGooglemap!!.addMarker(markerOption)
                    }
                    /*                mGooglemap.clear();
                addCircle(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
                getNearByEventsMapFeeds(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), Integer.toString(userSearchbinding.sbGeofence.getProgress() + 5));*/

                }
            } else {
                circleLat = mCurrentLocation!!.latitude
                circleLong = mCurrentLocation!!.longitude
                val loc = LatLng(mCurrentLocation!!.latitude, mCurrentLocation!!.longitude)
                if (mGooglemap != null) {
                    mGooglemap!!.clear()
                    addCircle(circleLat, circleLong)
                    val markerOption = MarkerOptions().position(loc).flat(true).draggable(false)
                    markerOption.icon(bitmapDescriptorFromVector(HooleyMain.activity, R.drawable.ic_map_cirlce_centre))
                    mGooglemap!!.addMarker(markerOption)
                    mapCircle!!.radius = currentProgress.toDouble()
                    val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
                    val animateZomm = currentZoomLevel + 5
                    mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat, circleLong), animateZomm))
                    mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
                }
                binding!!.tvLocationName.text = (HooleyMain.activity as ActivityBase).getCompleteAddressString(mCurrentLocation!!.latitude, mCurrentLocation!!.longitude)
                /*            mGooglemap.clear();
            addCircle(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            getNearByEventsMapFeeds(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), Integer.toString(userSearchbinding.sbGeofence.getProgress() + 5));*/

            }

        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        if (!event.fromNotification)
            shareEvent(event.message)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                7 -> shareEvent("")
                AUTOCOMPLETE_REQUEST_CODE -> {
                    if (resultCode == RESULT_OK) {
                        val place: Place = Autocomplete.getPlaceFromIntent(data!!)
                        Log.i("TAG", "Place: " + place.name + ", " + place.id)
                        circleLat = place.latLng!!.latitude
                        circleLong = place.latLng!!.longitude
                        binding!!.tvLocationName.text = (HooleyMain.activity as ActivityBase).getCompleteAddressString(circleLat, circleLong)
                        val loc = LatLng(circleLat, circleLong)
                        if (mGooglemap != null) {
                            mGooglemap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 11.5f))
                            mGooglemap!!.clear()
                            addCircle(circleLat, circleLong)
                            getNearByEventsMapFeeds(circleLat, circleLong, currentProgress / 1609.344f)
                        }
                    } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                        // TODO: Handle the error.
                        val status: Status = Autocomplete.getStatusFromIntent(data!!)
                        Log.i("TAG", status.statusMessage)
                    } else if (resultCode == RESULT_CANCELED) {
                        // The user canceled the operation.
                    }

                }
                REQUEST_CHECK_SETTINGS -> when (resultCode) {
                    RESULT_OK -> {
                        Log.i(ct, "User agreed to make required location settings changes.")
                        startLocationUpdates()
                    }
                    Activity.RESULT_CANCELED -> {
                        Toast.makeText(context, "Please allow your location!", Toast.LENGTH_SHORT).show()
                        Log.i(ct, "User chose not to make required location settings changes.")
                    }
                }
            }
        }
        Log.d(ct, "onActivityResult-> $resultCode")
    }

    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun buildLocationSettingsRequest() {
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        mLocationSettingsRequest = builder.build()
    }

    override fun onResult(locationSettingsResult: LocationSettingsResult) {
        val status = locationSettingsResult.status
        Log.d(ct, "onResult-> " + status.statusCode)
        Log.i(ct, "onResult-> " + status.statusMessage!!)
        when (status.statusCode) {
            LocationSettingsStatusCodes.CANCELED -> Log.i(ct, "Location handling cancelled!")
            LocationSettingsStatusCodes.SUCCESS -> {
                Log.i(ct, "All location settings are satisfied.")
                startLocationUpdates()
            }
            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                Log.i(ct, "Location settings are not satisfied. Show the user a dialog to" + "upgrade location settings ")

                try {
                    status.startResolutionForResult(HooleyMain.activity, REQUEST_CHECK_SETTINGS)
                } catch (e: IntentSender.SendIntentException) {
                    Log.i(ct, "PendingIntent unable to execute request.")
                }

            }
            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(ct, "Location settings are inadequate, and cannot be fixed here. Dialog " + "not created.")
        }

    }

    fun addCircle(cLat: Double, cLong: Double) {
        isCircle = true
        mGooglemap!!.uiSettings.isZoomGesturesEnabled = true
        mGooglemap!!.uiSettings.setAllGesturesEnabled(true)
        if (mapCircle != null) {
            mapCircle!!.remove()
        }

        mapCircle = mGooglemap!!.addCircle(CircleOptions()
                .center(LatLng(cLat, cLong))
                .radius(currentProgress.toDouble())
                .fillColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_light_color))
                .strokeColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                .strokeWidth(2f)
                .clickable(true))
        mapCircle!!.isVisible = true

        val markerOption = MarkerOptions().position(LatLng(cLat, cLong)).flat(true).draggable(false)
        markerOption.icon(bitmapDescriptorFromVector(HooleyMain.activity, R.drawable.ic_map_cirlce_centre))
        mGooglemap!!.addMarker(markerOption)
        binding!!.drawerView.visibility = View.GONE
        DashBoardFragment.fragmentDashboardBinding!!.ivDrawCircle.isChecked = true
        DashBoardFragment.fragmentDashboardBinding!!.ivDrawPolygon.isChecked = false
    }

    @SuppressLint("MissingPermission")
    private fun moveToLockLocation() {
        if (mGooglemap != null) {
            mGooglemap!!.isMyLocationEnabled = false
            mGooglemap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat, circleLong), 11.5f))
        }
    }

    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
        if (mapCircle != null) {
            mapCircle!!.radius = progress.toDouble()
        }
        val f = DecimalFormat("##.00")
        currentProgress = seekBar.progress.toFloat()
        try {
            if (currentProgress < 1609.344) {
                if (progress <= 100) {
                    currentProgress = 100.0f
                    DashBoardFragment.fragmentDashboardBinding!!.tvSbProgress.text = 100.toString() + " meters"
                } else {
                    DashBoardFragment.fragmentDashboardBinding!!.tvSbProgress.text = f.format(currentProgress) + " meters"
                }
            } else {
                DashBoardFragment.fragmentDashboardBinding!!.tvSbProgress.text = "${f.format(currentProgress / 1609.344)} miles"
            }
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar) {
        mGooglemap!!.clear()
        addCircle(circleLat, circleLong)
        val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
        val animateZomm = currentZoomLevel + 5
        mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat, circleLong), animateZomm))
        mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
        getNearByEventsMapFeeds(circleLat, circleLong, currentProgress / 1609.344f)
    }

    private fun getNearByEventsMapFeeds(lat: Double, lang: Double, radius: Float) {
        geoFenceWebService.seeAllNearByEventMapViewFeeds(lat, lang, radius, object : IWebServiceCallback<NearByEventMapFeedModel> {
            override fun success(result: NearByEventMapFeedModel) {
                objectMapResponse = result
                if (DashBoardFragment.fragmentDashboardBinding!!.rbPaidEvent.isChecked) {
                    addFreePaidEventInFinalList(objectMapResponse.paidEventsList, Constants.TYPE_PAID_EVENT)
                    addPaidEventMarker()
                }
                if (DashBoardFragment.fragmentDashboardBinding!!.rbFreeEvent.isChecked) {
                    addFreePaidEventInFinalList(objectMapResponse.freeEventsList, Constants.TYPE_FREE_EVENT)
                    addFreeEventsMarker()
                }
                mGooglemap!!.setOnMarkerClickListener(this@NearByFragment)
            }

            override fun failure(message: String) {
                mGooglemap!!.clear()
                addCircle(lat, lang)
                try {
                    //                    Util.showToastMessage(HooleyMain.Companion.getActivity(), message);
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    private fun addFreeEventsMarker() {
        if (mapFinalList != null && mapFinalList!!.size > 0) {
            for (i in mapFinalList!!.indices) {
                if (mapFinalList!![i].type == Constants.TYPE_FREE_EVENT) {
                    val loc = LatLng(mapFinalList!![i].eventDetail.eventLat!!, mapFinalList!![i].eventDetail.eventLong!!)
                    markerOption = MarkerOptions().position(loc).flat(false).draggable(false)
                    markerOption!!.icon(bitmapDescriptorFromVector(HooleyMain.activity, R.drawable.ic_free_event))
                    val marker = mGooglemap!!.addMarker(markerOption)
                    marker.tag = i
                }

            }
        } else {
            mGooglemap!!.clear()
            addCircle(circleLat, circleLong)
        }
    }

    private fun addPaidEventMarker() {
        if (mapFinalList != null && mapFinalList!!.size > 0) {
            for (i in mapFinalList!!.indices) {
                if (mapFinalList!![i].type == Constants.TYPE_PAID_EVENT) {
                    val loc = LatLng(mapFinalList!![i].eventDetail.eventLat!!, mapFinalList!![i].eventDetail.eventLong!!)
                    markerOption = MarkerOptions().position(loc).flat(false).draggable(false)
                    markerOption!!.icon(bitmapDescriptorFromVector(HooleyMain.activity, R.drawable.ic_paid_event))
                    val marker = mGooglemap!!.addMarker(markerOption)
                    marker.tag = i
                }
            }
        } else {
            mGooglemap!!.clear()
            addCircle(circleLat, circleLong)
        }
    }

    private fun addFreePaidEventInFinalList(list: ArrayList<NearByEventListFeedModel.EventDetail>, type: Int) {
        if (mapFinalList != null || mapFinalList!!.size > 0)
            mapFinalList!!.clear()
        if (list.size > 0) {
            for (i in list.indices) {
                mapFinalList!!.add(MapListFinalModel(list[i], type))
            }
        }
    }

    private fun bitmapDescriptorFromVector(context: Context?, vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context!!, vectorResId)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    override fun onResume() {
        super.onResume()
        if (mGoogleApiClient != null)
            if (mGoogleApiClient!!.isConnected) {
                startLocationUpdates()
            }
        if (isVisible) {
            if (mGooglemap != null)
                mGooglemap!!.clear()
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivFullScreenMap -> {
                DashBoardFragment.fragmentDashboardBinding!!.llTopTabs.visibility = View.GONE
                //                DashBoardFragment.Companion.getFragmentDashboardBinding().rlTabs.setVisibility(View.GONE);
                DashBoardFragment.fragmentDashboardBinding!!.ivSmallScrenMap.visibility = View.VISIBLE
                DashBoardFragment.fragmentDashboardBinding!!.ivFullScreenMap.visibility = View.GONE
            }

            R.id.ivSmallScrenMap -> {
                DashBoardFragment.fragmentDashboardBinding!!.llTopTabs.visibility = View.VISIBLE
                //                DashBoardFragment.Companion.getFragmentDashboardBinding().rlTabs.setVisibility(View.VISIBLE);
                DashBoardFragment.fragmentDashboardBinding!!.ivSmallScrenMap.visibility = View.GONE
                DashBoardFragment.fragmentDashboardBinding!!.ivFullScreenMap.visibility = View.VISIBLE
            }

            R.id.ivMyLocation -> {
                DashBoardFragment.fragmentDashboardBinding!!.rlTop.visibility = View.VISIBLE
                moveToCurrentLocation()
            }

            R.id.ivDrawPolygon -> {
                drawZone()
                DashBoardFragment.fragmentDashboardBinding!!.rlTop.visibility = View.GONE
            }

            R.id.ivDrawCircle -> {
                DashBoardFragment.fragmentDashboardBinding!!.rlTop.visibility = View.VISIBLE
                mGooglemap!!.clear()
                addCircle(circleLat, circleLong)
                val loc = LatLng(circleLat, circleLong)
                if (mGooglemap != null) {
                    mGooglemap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, mGooglemap!!.cameraPosition.zoom))
                }
                getNearByEventsMapFeeds(circleLat, circleLong, currentProgress / 1609.344f)
            }
            R.id.ivMapInfo -> showInfoDialog()

            R.id.ivMinus -> if (currentProgress >= 100.0f) {
                var temp = currentProgress
                temp -= 100f
                if (temp <= 100.0f) {
                    currentProgress = 100.0f
                } else {
                    currentProgress -= 100f
                }
                DashBoardFragment.fragmentDashboardBinding!!.sbGeofence.progress = (currentProgress).toInt()
                updateUI()
            }
            R.id.ivPlus -> if (currentProgress < 64373.0f) {
                currentProgress += 100f
                DashBoardFragment.fragmentDashboardBinding!!.sbGeofence.progress = (currentProgress).toInt()
                updateUI()
            }
        }
    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        when (checkedId) {
            R.id.rbListView -> {
                if (!(HooleyMain.activity!! as HooleyMain).isBubbleShowing) {
                    (HooleyMain.activity!! as HooleyMain).addNewChatHead()
                }
                DashBoardFragment.fragmentDashboardBinding!!.llTopTabs.visibility = View.VISIBLE
                DashBoardFragment.fragmentDashboardBinding!!.rlBottomViews.visibility = View.GONE
                binding!!.sLRecycler.visibility = View.VISIBLE
                DashBoardFragment.fragmentDashboardBinding!!.rbListView.typeface = bold
                DashBoardFragment.fragmentDashboardBinding!!.rbMapView.typeface = regular
                DashBoardFragment.fragmentDashboardBinding!!.rbListView.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                DashBoardFragment.fragmentDashboardBinding!!.rbListView.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_left_radio)
                DashBoardFragment.fragmentDashboardBinding!!.rbMapView.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                DashBoardFragment.fragmentDashboardBinding!!.rbMapView.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding!!.rvNearBy.visibility = View.VISIBLE
                binding!!.rlMapView.visibility = View.GONE
            }
            R.id.rbMapView -> {
                binding!!.sLRecycler.visibility = View.GONE
                DashBoardFragment.fragmentDashboardBinding!!.rlBottomViews.visibility = View.VISIBLE
                DashBoardFragment.fragmentDashboardBinding!!.rbListView.typeface = regular
                DashBoardFragment.fragmentDashboardBinding!!.rbMapView.typeface = bold
                DashBoardFragment.fragmentDashboardBinding!!.rbMapView.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                DashBoardFragment.fragmentDashboardBinding!!.rbMapView.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_right_radio)
                DashBoardFragment.fragmentDashboardBinding!!.rbListView.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                DashBoardFragment.fragmentDashboardBinding!!.rbListView.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding!!.rvNearBy.visibility = View.GONE
                binding!!.rlMapView.visibility = View.VISIBLE
                binding!!.tvNoData.visibility = View.GONE
                if (displayInfoDialog) {
                    displayInfoDialog = false
                    showInfoDialog()
                }
            }
            R.id.rbPaidEvent -> {
                DashBoardFragment.fragmentDashboardBinding!!.rbPaidEvent.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white))
                DashBoardFragment.fragmentDashboardBinding!!.rbPaidEvent.typeface = bold
                DashBoardFragment.fragmentDashboardBinding!!.rbFreeEvent.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                DashBoardFragment.fragmentDashboardBinding!!.rbFreeEvent.typeface = regular
                DashBoardFragment.fragmentDashboardBinding!!.rbFreeEvent.isChecked = false
                mGooglemap!!.clear()
                if (isCircle) {
                    addCircle(circleLat, circleLong)
                } else {
                    addPolygonForFreePaid()
                }
                if (DashBoardFragment.fragmentDashboardBinding!!.rbFreeEvent.isChecked) {
                    addFreeEventsMarker()
                } else {
                    addPaidEventMarker()
                }
                getNearByEventsMapFeeds(circleLat, circleLong, currentProgress / 1609.344f)

            }
            R.id.rbFreeEvent -> {
                DashBoardFragment.fragmentDashboardBinding!!.rbFreeEvent.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white))
                DashBoardFragment.fragmentDashboardBinding!!.rbFreeEvent.typeface = bold
                DashBoardFragment.fragmentDashboardBinding!!.rbPaidEvent.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                DashBoardFragment.fragmentDashboardBinding!!.rbPaidEvent.typeface = regular
                DashBoardFragment.fragmentDashboardBinding!!.rbPaidEvent.isChecked = false
                mGooglemap!!.clear()
                if (isCircle) {
                    addCircle(circleLat, circleLong)
                } else {
                    addPolygonForFreePaid()
                }

                if (DashBoardFragment.fragmentDashboardBinding!!.rbPaidEvent.isChecked) {
                    addPaidEventMarker()
                } else {
                    addFreeEventsMarker()
                }
                getNearByEventsMapFeeds(circleLat, circleLong, currentProgress / 1609.344f)

            }
        }
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        var pos = 0
        if (marker.tag == null)
            pos = 0
        else
            pos = marker.tag as Int
//        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, EventPagerFragment.newInstance(mapFinalList!!, pos), "EventPagerFragment")
        (HooleyMain.activity!! as HooleyMain).removeBubbleView()
        var eventsCardFragment = EventPagerFragment.newInstance(mapFinalList!!, pos)
        eventsCardFragment.show(HooleyMain.activity!!.supportFragmentManager)
//        hideDrawer(HooleyMain.activity!!)
        return false
    }

    override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null)
            mGoogleApiClient!!.connect()
        EventBus.getDefault().register(this)

    }

    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient != null)
            if (mGoogleApiClient!!.isConnected) {
                mGoogleApiClient!!.disconnect()
            }
        EventBus.getDefault().unregister(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onClickLike(nearByEventsFinalModel: NearByEventsFinalModel, pos: Int, listPosition: Int) {
        nearByEvents = nearByEventsFinalModel
        position = pos
        try {
            likeEvent(nearByEvents.eventDetailList[pos].eventId, listPosition)
        } catch (ex: IndexOutOfBoundsException) {
            ex.printStackTrace()
        }

    }

    override fun onClickComment() {}

    override fun onClickFollow(nearByEventsFinalModel: NearByEventsFinalModel, pos: Int, listPosition: Int) {
        nearByEvents = nearByEventsFinalModel
        position = pos
        try {
            followEvent(nearByEvents.eventDetailList[pos].eventId, listPosition)
        } catch (ex: IndexOutOfBoundsException) {
            ex.printStackTrace()
        }

    }

    override fun onClickShare(nearByEventsFinalModel: NearByEventsFinalModel, pos: Int) {
        nearByEvents = nearByEventsFinalModel
        position = pos
        object : GetBitmapTask() {
            override fun onPostExecute(bmp: Bitmap) {
                super.onPostExecute(bmp)
                //                HooleyMain.removeDialog();
//                showShareEventDialog(bmp)
                (HooleyMain.activity as HooleyMain).shareWithImage(bmp, nearByEvents.eventDetailList[position].eventName!!, Constants.EVENT_SHARE + nearByEvents.eventDetailList[position].eventId, DateUtils.formatDateNew(nearByEvents.eventDetailList[position].startTime!!))

            }
        }.execute(nearByEvents.eventDetailList[position].coverPhoto)


    }

    private fun likeEvent(eventId: String?, listPosition: Int) {
        if (binding != null)
            binding!!.pbNearBy.visibility = View.VISIBLE
        if (eventService == null)
            eventService = EventWebService()
        eventService!!.likeEvent(eventId!!, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                binding!!.pbNearBy.visibility = View.GONE
                try {

                    when (nearByEvents.type) {
                        Constants.TYPE_FEATURE_EVENT -> if (mList!![listPosition].eventDetailList[position].isLiked) {
                            mList!![listPosition].eventDetailList[position].isLiked = false
                            mList!![listPosition].eventDetailList[position].likesCount = mList!![listPosition].eventDetailList[position].likesCount - 1
                        } else {
                            mList!![listPosition].eventDetailList[position].isLiked = true
                            mList!![listPosition].eventDetailList[position].likesCount = mList!![listPosition].eventDetailList[position].likesCount + 1
                        }
                        Constants.TYPE_TODAY_EVENT ->

                            if (mList!![listPosition].eventDetailList[position].isLiked) {
                                mList!![listPosition].eventDetailList[position].isLiked = false
                                mList!![listPosition].eventDetailList[position].likesCount = mList!![listPosition].eventDetailList[position].likesCount - 1
                            } else {
                                mList!![listPosition].eventDetailList[position].isLiked = true
                                mList!![listPosition].eventDetailList[position].likesCount = mList!![listPosition].eventDetailList[position].likesCount + 1
                            }
                        Constants.TYPE_NEXT_WEEK_EVENT ->

                            if (mList!![listPosition].eventDetailList[position].isLiked) {
                                mList!![listPosition].eventDetailList[position].isLiked = false
                                mList!![listPosition].eventDetailList[position].likesCount = mList!![listPosition].eventDetailList[position].likesCount - 1
                            } else {
                                mList!![listPosition].eventDetailList[position].isLiked = true
                                mList!![listPosition].eventDetailList[position].likesCount = mList!![listPosition].eventDetailList[position].likesCount + 1
                            }
                        Constants.TYPE_COMMING_EVENT ->

                            if (mList!![listPosition].eventDetailList[position].isLiked) {
                                mList!![listPosition].eventDetailList[position].isLiked = false
                                mList!![listPosition].eventDetailList[position].likesCount = mList!![listPosition].eventDetailList[position].likesCount - 1
                            } else {
                                mList!![listPosition].eventDetailList[position].isLiked = true
                                mList!![listPosition].eventDetailList[position].likesCount = mList!![listPosition].eventDetailList[position].likesCount + 1
                            }
                    }
                } catch (ex: IndexOutOfBoundsException) {
                    ex.printStackTrace()
                }

            }

            override fun failure(message: String) {
                binding!!.pbNearBy.visibility = View.GONE
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })

    }

    private fun followEvent(eventId: String?, listPosition: Int) {
        if (binding != null)
            binding!!.pbNearBy.visibility = View.VISIBLE
        if (eventService == null)
            eventService = EventWebService()
        eventService!!.followEvent(eventId!!, !mList!![listPosition].eventDetailList[position].isFollowed, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                binding!!.pbNearBy.visibility = View.GONE
                try {

                    when (nearByEvents.type) {
                        Constants.TYPE_FEATURE_EVENT -> if (mList!![listPosition].eventDetailList[position].isFollowed) {
                            mList!![listPosition].eventDetailList[position].isFollowed = false
                            mList!![listPosition].eventDetailList[position].followersCount = mList!![listPosition].eventDetailList[position].followersCount - 1
                        } else {
                            mList!![listPosition].eventDetailList[position].isFollowed = true
                            mList!![listPosition].eventDetailList[position].followersCount = mList!![listPosition].eventDetailList[position].followersCount + 1
                            adapter.notifyDataSetChanged()
                        }
                        Constants.TYPE_TODAY_EVENT ->

                            if (mList!![listPosition].eventDetailList[position].isFollowed) {
                                mList!![listPosition].eventDetailList[position].isFollowed = false
                                mList!![listPosition].eventDetailList[position].followersCount = mList!![listPosition].eventDetailList[position].followersCount - 1
                            } else {
                                mList!![listPosition].eventDetailList[position].isFollowed = true
                                mList!![listPosition].eventDetailList[position].followersCount = mList!![listPosition].eventDetailList[position].followersCount + 1
                            }
                        Constants.TYPE_NEXT_WEEK_EVENT ->

                            if (mList!![listPosition].eventDetailList[position].isFollowed) {
                                mList!![listPosition].eventDetailList[position].isFollowed = false
                                mList!![listPosition].eventDetailList[position].followersCount = mList!![listPosition].eventDetailList[position].followersCount - 1
                            } else {
                                mList!![listPosition].eventDetailList[position].isFollowed = true
                                mList!![listPosition].eventDetailList[position].followersCount = mList!![listPosition].eventDetailList[position].followersCount + 1
                            }
                        Constants.TYPE_COMMING_EVENT ->

                            if (mList!![listPosition].eventDetailList[position].isFollowed) {
                                mList!![listPosition].eventDetailList[position].isFollowed = false
                                mList!![listPosition].eventDetailList[position].followersCount = mList!![listPosition].eventDetailList[position].followersCount - 1
                            } else {
                                mList!![listPosition].eventDetailList[position].isFollowed = true
                                mList!![listPosition].eventDetailList[position].followersCount = mList!![listPosition].eventDetailList[position].followersCount + 1
                            }
                    }
                } catch (ex: IndexOutOfBoundsException) {
                    ex.printStackTrace()
                }

            }

            override fun failure(message: String) {
                binding!!.pbNearBy.visibility = View.GONE
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })

    }

    private fun showInfoDialog() {
        showInfoDialog = Dialog(HooleyMain.activity!!)
        showInfoDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        showInfoDialog!!.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        showInfoDialog!!.setContentView(R.layout.dialog_map_info)
        val ivClose = showInfoDialog!!.findViewById<ImageView>(R.id.ivClose)
        val tvText = showInfoDialog!!.findViewById<TextView>(R.id.tvText)
        tvText.text = HooleyMain.activity!!.getString(R.string.str_map_near_by)
        ivClose.setOnClickListener { showInfoDialog!!.dismiss() }
        showInfoDialog!!.show()
    }

    private fun showShareEventDialog(bitmap: Bitmap) {
        val bitmapPath = MediaStore.Images.Media.insertImage(HooleyMain.activity!!.contentResolver, bitmap, "Thumb", null)
        val bitmapUri = Uri.parse(bitmapPath)
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "image/png"
        intent.type = "text/plain"
        val receiver = Intent(HooleyMain.activity, ApplicationSelectorReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(HooleyMain.activity, 0, receiver, PendingIntent.FLAG_UPDATE_CURRENT)
        intent.putExtra(Intent.EXTRA_STREAM, bitmapUri)
        intent.putExtra(Intent.EXTRA_TEXT,
                Constants.EVENT_SHARE + nearByEvents.eventDetailList[position].eventId
                        + "\n" + nearByEvents.eventDetailList[position].eventName
                        + "\nStart Date = " + DateUtils.formatDateNew(nearByEvents.eventDetailList[position].startTime!!))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            startActivityForResult(Intent.createChooser(intent, null, pendingIntent.intentSender), 7)
        } else {
            startActivityForResult(Intent.createChooser(intent, "Share"), 7)
        }
    }

    private fun shareEvent(shareMedia: String) {
        if (binding != null)
            binding!!.pbNearBy.visibility = View.VISIBLE

        if (eventService == null)
            eventService = EventWebService()
        eventService!!.shareEvent(nearByEvents.eventDetailList[position].eventId!!, shareMedia, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                binding!!.pbNearBy.visibility = View.GONE
                when (nearByEvents.type) {
                    Constants.TYPE_FEATURE_EVENT -> {

                        mList!![0].eventDetailList[position].sharesCount = mList!![0].eventDetailList[position].sharesCount + 1
                        adapter.notifyDataSetChanged()
                    }
                    Constants.TYPE_TODAY_EVENT -> {
                        if (mList!!.size != 1) {
                            mList!![1].eventDetailList[position].sharesCount = mList!![1].eventDetailList[position].sharesCount + 1
                        } else {
                            mList!![0].eventDetailList[position].sharesCount = mList!![0].eventDetailList[position].sharesCount + 1
                        }
                        adapter.notifyDataSetChanged()
                    }
                    Constants.TYPE_NEXT_WEEK_EVENT -> {
                        if (mList!!.size != 1) {
                            mList!![2].eventDetailList[position].sharesCount = mList!![2].eventDetailList[position].sharesCount + 1
                        } else {
                            mList!![0].eventDetailList[position].sharesCount = mList!![0].eventDetailList[position].sharesCount + 1
                        }
                        adapter.notifyDataSetChanged()
                    }
                    Constants.TYPE_COMMING_EVENT -> {
                        if (mList!!.size != 1) {
                            mList!![3].eventDetailList[position].sharesCount = mList!![3].eventDetailList[position].sharesCount + 1
                        } else {
                            mList!![0].eventDetailList[position].sharesCount = mList!![0].eventDetailList[position].sharesCount + 1
                        }
                        adapter.notifyDataSetChanged()
                    }
                }
            }

            override fun failure(message: String) {
                binding!!.pbNearBy.visibility = View.GONE
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    override fun onUpComingEvent() {
        // unused

    }

    override fun onPastEvent() {
        // unused

    }

    override fun onMapLongClick(latLng: LatLng) {
        circleLat = latLng.latitude
        circleLong = latLng.longitude
        mGooglemap!!.clear()
        addCircle(circleLat, circleLong)
        mGooglemap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11.5f))
        getNearByEventsMapFeeds(circleLat, circleLong, currentProgress / 1609.344f)
        binding!!.tvLocationName.text = ((HooleyMain.activity as ActivityBase).getCompleteAddressString(circleLat, circleLong))
    }

    fun drawZone() {
        isCircle = false
        mGooglemap!!.clear()
        mLatlngs.clear()
        mPolylineOptions = null
        mPolygonOptions = null
        mDrawFinished = true
        binding!!.drawerView.visibility = View.VISIBLE
        mGooglemap!!.uiSettings.isScrollGesturesEnabled = false
        if (mapCircle != null) {
            mapCircle!!.isVisible = false
            mapCircle = null
        }
        DashBoardFragment.fragmentDashboardBinding!!.ivDrawCircle.isChecked = false
        DashBoardFragment.fragmentDashboardBinding!!.ivDrawPolygon.isChecked = true
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        var X1 = event.x.toInt()
        var Y1 = event.y.toInt()
        var point = Point()
        point.x = X1
        point.y = Y1
        val firstGeoPoint = mGooglemap!!.projection.fromScreenLocation(
                point)
        when (event.action) {

            MotionEvent.ACTION_DOWN -> {
            }

            MotionEvent.ACTION_MOVE -> if (mDrawFinished) {
                X1 = event.x.toInt()
                Y1 = event.y.toInt()
                point = Point()
                point.x = X1
                point.y = Y1
                val geoPoint = mGooglemap!!.projection
                        .fromScreenLocation(point)
                mLatlngs.add(geoPoint)
                mPolylineOptions = PolylineOptions()
                mPolylineOptions!!.color(resources.getColor(R.color.app_purple_color))
                mPolylineOptions!!.width(5f)
                mPolylineOptions!!.addAll(mLatlngs)
                mGooglemap!!.addPolyline(mPolylineOptions)

            }
            MotionEvent.ACTION_UP -> {
                Log.d(ct, "Poinnts array size " + mLatlngs.size)
                mLatlngs.add(firstGeoPoint)
                mGooglemap!!.clear()
                mPolylineOptions = null
                binding!!.drawerView.visibility = View.GONE
                mGooglemap!!.uiSettings.isZoomGesturesEnabled = true
                mGooglemap!!.uiSettings.setAllGesturesEnabled(true)
                mPolygonOptions = PolygonOptions()
                mPolygonOptions!!.fillColor(resources.getColor(R.color.app_purple_light_color))
                mPolygonOptions!!.strokeColor(resources.getColor(R.color.app_purple_color))
                mPolygonOptions!!.strokeWidth(3f)
                mPolygonOptions!!.addAll(mLatlngs)
                mGooglemap!!.addPolygon(mPolygonOptions)
                mPolygonOptions!!.zIndex
                mDrawFinished = false
                val latLng = (HooleyMain.activity as ActivityBase).computeCentroid(mLatlngs)
                mLatlngs.add(mLatlngs[0])
                getNearByEventsMapFeedsPolygon()

                AsyncTask.execute { binding!!.tvLocationName.text = (HooleyMain.activity as ActivityBase).getCompleteAddressString(latLng.latitude, latLng.latitude) }
            }
        }//((EditText) autocompleteFragment.getView().findViewById(R.id.places_autocomplete_search_input)).setText(((ActivityBase) HooleyMain.Companion.getActivity()).getCompleteAddressString(latLng.latitude, latLng.latitude));


        return mGestureDetector!!.onTouchEvent(event)
    }

    private fun getNearByEventsMapFeedsPolygon() {

        geoFenceWebService.seeAllNearByEventMapViewPolygonFeeds(mLatlngs, object : GenericCallback<NearByEventMapFeedModel> {
            override fun success(result: NearByEventMapFeedModel) {
                objectMapResponse = result
                if (DashBoardFragment.fragmentDashboardBinding!!.rbPaidEvent.isChecked) {
                    addPaidEventMarker()
                    addFreePaidEventInFinalList(objectMapResponse.paidEventsList, Constants.TYPE_PAID_EVENT)
                }
                if (DashBoardFragment.fragmentDashboardBinding!!.rbFreeEvent.isChecked) {
                    addFreeEventsMarker()
                    addFreePaidEventInFinalList(objectMapResponse.freeEventsList, Constants.TYPE_FREE_EVENT)
                }
                mGooglemap!!.setOnMarkerClickListener(this@NearByFragment)
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)

                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun addPolygonForFreePaid() {
        mPolylineOptions = null
        binding!!.drawerView.visibility = View.GONE
        mGooglemap!!.uiSettings.isZoomGesturesEnabled = true
        mGooglemap!!.uiSettings.setAllGesturesEnabled(true)
        mPolygonOptions = PolygonOptions()
        mPolygonOptions!!.fillColor(resources.getColor(R.color.app_purple_light_color))
        mPolygonOptions!!.strokeColor(resources.getColor(R.color.app_purple_color))
        mPolygonOptions!!.strokeWidth(3f)
        mPolygonOptions!!.addAll(mLatlngs)
        mGooglemap!!.addPolygon(mPolygonOptions)
        mPolygonOptions!!.zIndex
        mDrawFinished = false
    }

    override fun onRefresh() {
        if (binding!!.sLRecycler != null) {
            binding!!.sLRecycler.isRefreshing = true
            binding!!.pbNearBy.visibility = View.GONE
        }
        isSwiped = true
        getNearByEventsListFeeds()
    }

    private inner class GestureListener : GestureDetector.SimpleOnGestureListener() {
        override fun onDown(e: MotionEvent): Boolean {
            return true
        }

        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            return false
        }

    }

    companion object {
        val MY_PERMISSIONS_REQUEST_LOCATION = 99
        val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 1000
        val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2
        protected val REQUEST_CHECK_SETTINGS = 0x1
    }
}
