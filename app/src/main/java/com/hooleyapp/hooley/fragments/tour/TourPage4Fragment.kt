package com.hooleyapp.hooley.fragments.tour

import android.animation.Animator
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.Splash
import com.hooleyapp.hooley.databinding.FragmentCollectTourBinding
import com.hooleyapp.hooley.fragments.BaseFragment

/**
 * Created by Nauman on 2/28/2018.
 */

class TourPage4Fragment : BaseFragment(), Animator.AnimatorListener {

    internal var binding: FragmentCollectTourBinding? = null

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            if (view != null) {
                binding!!.lavCollectLoop.visibility = View.GONE
                binding!!.lavCollect.clearAnimation()
                binding!!.lavCollect.setAnimation(Splash.activity.resources.getIdentifier("collect", "raw", Splash.activity.packageName))
                binding!!.lavCollect.playAnimation()
            }
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_collect_tour, container, false)
        setListener()
        return binding!!.root
    }

    private fun setListener() {
        binding!!.lavCollect.addAnimatorListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onAnimationStart(animator: Animator) {

    }

    override fun onAnimationEnd(animator: Animator) {
        binding!!.lavCollectLoop.visibility = View.VISIBLE
    }

    override fun onAnimationCancel(animator: Animator) {

    }

    override fun onAnimationRepeat(animator: Animator) {

    }
}
