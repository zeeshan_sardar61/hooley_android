package com.hooleyapp.hooley.fragments.search

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterAddFriendHooley
import com.hooleyapp.hooley.adapters.AdapterRecentSearches
import com.hooleyapp.hooley.databinding.FragmentUserSearchBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.fragments.search.SearchFilterFragment.Companion.mainBinding
import com.hooleyapp.hooley.helper.hideKeyboard
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IFriendContactClickListener
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GetUserSearchesHistory
import com.hooleyapp.hooley.model.friends.HooleyUserModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

class UserSearchFragment : BaseFragment(), View.OnClickListener, AdapterRecentSearches.SearchItemClick, ViewAvatarFragment.CardCallBackListener, IFriendContactClickListener {

    lateinit var userSearchbinding: FragmentUserSearchBinding
    var mList: ArrayList<FriendsTable> = ArrayList()
    private lateinit var adapter: AdapterAddFriendHooley
    private lateinit var recentSearchesAdapter: AdapterRecentSearches
    private lateinit var usersObjectResponse: HooleyUserModel
    var friendService = FriendWebService()
    private var recentSearchList: ArrayList<String>? = null

    fun searchFriend(searchString: String) {
        userSearchbinding.rvItems.visibility = View.GONE
        userSearchbinding.llRecentSearchView.visibility = View.GONE
        userSearchbinding.rvHooleyUsers.visibility = View.VISIBLE
        friendService.getMoreInvitePeople(searchString, object : GenericCallback<HooleyUserModel> {
            override fun success(result: HooleyUserModel) {
                HooleyMain.activity!!.removeDialog()
                usersObjectResponse = result
                if (mList.size > 0)
                    mList.clear()
                mList.addAll(usersObjectResponse.inviteList)
                setUsersAdapter()

            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                HooleyMain.activity!!.showSuccessDialog(message)
                getRecentSearches()
                mList.clear()

            }
        })
    }

    private fun setUsersAdapter() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        userSearchbinding.rvHooleyUsers.layoutManager = manager
        userSearchbinding.rvHooleyUsers.layoutAnimation = animation
        userSearchbinding.rvHooleyUsers.setEmptyView(userSearchbinding.tvNoData)
        adapter = AdapterAddFriendHooley(mList)
        adapter.mListener = this
        userSearchbinding.rvHooleyUsers.adapter = adapter

    }

    private fun clearAllSearch() {
        userSearchbinding.rvItems.visibility = View.VISIBLE
        userSearchbinding.rvHooleyUsers.visibility = View.GONE
        HooleyMain.activity!!.showDialog()
        friendService.removeAllUserSearches(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                if (recentSearchList != null) {
                    recentSearchList!!.clear()
                }
                if (recentSearchesAdapter != null)
                    recentSearchesAdapter.notifyDataSetChanged()

            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                HooleyMain.activity!!.showSuccessDialog(message)
            }
        })
    }

    fun getRecentSearches() {
        userSearchbinding.rvItems.visibility = View.VISIBLE
        userSearchbinding.llRecentSearchView.visibility = View.VISIBLE
        userSearchbinding.rvHooleyUsers.visibility = View.GONE
        HooleyMain.activity!!.showDialog()
        friendService.getUserSearchHistory(object : GenericCallback<GetUserSearchesHistory> {
            override fun success(result: GetUserSearchesHistory) {
                HooleyMain.activity!!.removeDialog()
                setRecentSearchesAdapter(result)

            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                HooleyMain.activity!!.showSuccessDialog(message)
            }

        })
    }


    fun setRecentSearchesAdapter(result: GetUserSearchesHistory) {
        if (result.searchList.size > 0) {
            recentSearchList = result.searchList
            val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity!!, R.anim.layout_animation_fall_down)
            val manager = LinearLayoutManager(HooleyMain.activity!!, LinearLayoutManager.VERTICAL, false)
            userSearchbinding.rvItems.layoutManager = manager
            userSearchbinding.rvItems.layoutAnimation = animation
            userSearchbinding.rvItems.setEmptyView(userSearchbinding.tvNoData)
            recentSearchesAdapter = AdapterRecentSearches(HooleyMain.activity!!, recentSearchList!!)
            recentSearchesAdapter.mListener = this
            userSearchbinding.rvItems.adapter = recentSearchesAdapter
        }

    }

    override fun reloadFragment(flag: Boolean) {
        if (flag)
            searchFriend(mainBinding.edtSearch.text.toString())
    }

    private fun sendFriendRequest(position: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        val friendList = java.util.ArrayList<Int>()
        friendList.add(mList[position].userId.toInt())
        friendService.sendFriendRequest(friendList, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                Util.showToastMessage(HooleyMain.activity!!, "Friend Request Sent")
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun acceptFriendRequest(position: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        friendService.acceptIgnoreFriendRequest(mList[position].userId.toInt(), true, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                mList[position].status = Constants.TYPE_FRIEND
                if (adapter != null)
                    adapter.notifyDataSetChanged()
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.showSuccessDialog(message)
            }
        })

    }


    override fun onClickAddFriend(position: Int) {
        mList[position].status = Constants.TYPE_PENDING_SENT
        adapter.notifyDataSetChanged()
        sendFriendRequest(position)
    }

    override fun onClickAcceptFriend(position: Int) {
        acceptFriendRequest(position)
    }

    override fun onClickItem(position: Int) {
        Util.hideKeyboard(HooleyMain.activity!!)
        val avatarfragment = ViewAvatarFragment(mList[position].userId)
        avatarfragment.mListener = this
        avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
    }


    override fun onSearchClick(name: String) {
        HooleyMain.activity!!.showDialog()
        searchFriend(name)
    }

    fun addStringToSearch(name: String) {
        HooleyMain.activity!!.showDialog()
        friendService.addUserSearchHistory(name, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                searchFriend(name)
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
            }
        })
    }


    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser && isResumed) {
            getRecentSearches()
            setEditListener()
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tvClearAll -> {
                clearAllSearch()
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        userSearchbinding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_search, container, false)
        setListener()
        if (userVisibleHint) {
            getRecentSearches()
            setEditListener()
        }
        return userSearchbinding.root
    }

    private fun setListener() {
        userSearchbinding.tvClearAll.setOnClickListener(this)
    }

    fun setEditListener() {
        mainBinding.edtSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (!mainBinding.edtSearch.text!!.isEmpty()) {
                    HooleyMain.activity!!.hideKeyboard()
                    addStringToSearch(mainBinding.edtSearch.text.toString())

                }
            }
            true
        }
        mainBinding.ivClose.setOnClickListener {
            if (mainBinding.rbHooleyUsers.isChecked) {
                mainBinding.edtSearch.setText("")
                getRecentSearches()
            } else {
                mainBinding.edtSearch.setText("")
            }
        }
    }

}
