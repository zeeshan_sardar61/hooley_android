package com.hooleyapp.hooley.fragments.tour

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.FragmentPromoteTourBinding
import com.hooleyapp.hooley.fragments.BaseFragment

/**
 * Created by Nauman on 2/28/2018.
 */

class TourPage3Fragment : BaseFragment() {

    internal var binding: FragmentPromoteTourBinding? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_promote_tour, container, false)
        return binding!!.root
    }


    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }
}
