package com.hooleyapp.hooley.fragments.profile

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentPersonalProfileBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.fragments.stripe.StripeConnectFragment
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GetUserProfilesModel
import com.hooleyapp.hooley.model.staticData.StaticDataModel
import com.hooleyapp.hooley.others.*
import com.hooleyapp.hooley.services.ProfileWebService
import org.greenrobot.eventbus.EventBus

/**
 * Created by Nauman on 12/12/2017.
 */

class PersonalProfileFragment : BaseFragment(), View.OnClickListener {

    var binding: FragmentPersonalProfileBinding? = null
    var enable = false
    internal var objectResponse: GetUserProfilesModel? = null
    private var stripeConnectFragment: StripeConnectFragment? = null
    private val REQUEST_CODE_STRIPE = 900
    private var code: String? = null
    private val service = ProfileWebService()
    private var stripeDialog: Dialog? = null
    private val gson = Gson()
    var model = StaticDataModel()

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {

        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            try {
                getPersonalProfile()
            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            }

        } else {
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_personal_profile, container, false)
        setListener()
        setStaticData()
        return binding!!.root
    }

    private fun setStaticData() {
        model = gson.fromJson(HooleyApp.db.getString(Constants.STATIC_MODEL), StaticDataModel::class.java)
    }

    private fun setupHolleyWalletDialog(heading: String, content: String) {
        stripeDialog = Dialog(HooleyMain.activity!!)
        stripeDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        stripeDialog!!.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        stripeDialog!!.setContentView(R.layout.dialog_hooley_wallet)
        val tvHeading = stripeDialog!!.findViewById<TextView>(R.id.tvHeading)
        val tvContent = stripeDialog!!.findViewById<TextView>(R.id.tvContent)
        val ivClose = stripeDialog!!.findViewById<ImageView>(R.id.ivClose)
        tvHeading.text = heading
        tvContent.text = content
        ivClose.setOnClickListener { stripeDialog!!.dismiss() }

    }

    private fun setListener() {
        binding!!.btnEditPrfle.setOnClickListener(this)
        binding!!.btnEditProfile.setOnClickListener(this)
        binding!!.ivFaceBook.setOnClickListener(this)
        binding!!.ivTwitter.setOnClickListener(this)
        binding!!.ivInstagram.setOnClickListener(this)
        binding!!.ivLinkedIn.setOnClickListener(this)
        binding!!.ivFlicker.setOnClickListener(this)
        binding!!.ivAvatar.setOnClickListener(this)
    }


    private fun getPersonalProfile() {
        if (binding != null)
            binding!!.pbPersonalProfile.visibility = View.VISIBLE

        service.getUserProfile(object : GenericCallback<GetUserProfilesModel> {
            override fun success(result: GetUserProfilesModel) {
                binding!!.pbPersonalProfile.visibility = View.GONE
                objectResponse = result
                AppAnimationUtil.expandView(binding!!.scroll)
                setData()
            }

            override fun failure(message: String) {
                binding!!.pbPersonalProfile.visibility = View.GONE
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }

    override fun onPause() {
        super.onPause()
        enable = false
    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
//        if (TextUtils.isEmpty(objectResponse!!.generalProfile!!.userInfo.phoneNo)) {
//            (HooleyMain.activity as ActivityBase).callFragment(R.id.container, EditPersonalProfileFragment.newInstance(objectResponse!!), "EditPersonalProfileFragment")
//            return
//        }
        if (!TextUtils.isEmpty(objectResponse!!.generalProfile!!.userInfo.firstName) && !TextUtils.isEmpty(objectResponse!!.generalProfile!!.userInfo.lastName)) {
            binding!!.tvUserName.text = objectResponse!!.generalProfile!!.userInfo.firstName + " " + objectResponse!!.generalProfile!!.userInfo.lastName
            HooleyApp.db.putString(Constants.FULL_NAME, objectResponse!!.generalProfile!!.userInfo.firstName + " " + objectResponse!!.generalProfile!!.userInfo.lastName)

        } else {
            binding!!.tvUserName.text = objectResponse!!.generalProfile!!.userInfo.firstName + " " + objectResponse!!.generalProfile!!.userInfo.lastName
            HooleyApp.db.putString(Constants.FULL_NAME, objectResponse!!.generalProfile!!.userInfo.firstName + " " + objectResponse!!.generalProfile!!.userInfo.lastName)
        }
        if (objectResponse!!.generalProfile!!.userGeofence.size > 0)
            binding!!.tvUserAdress.text = objectResponse!!.generalProfile!!.userGeofence[0].address
        else
            binding!!.tvUserAdress.visibility = View.GONE

        HooleyApp.db.putBoolean(Constants.IS_PHONE_VERIFIED, objectResponse!!.generalProfile!!.userInfo.isPhoneVerified)
        HooleyApp.db.putString(Constants.VERIFIED_PHONE_NUMBER, objectResponse!!.generalProfile!!.userInfo.countryCode + objectResponse!!.generalProfile!!.userInfo.phoneNo)
        HooleyApp.db.putString(Constants.COUNTRY_CODE, objectResponse!!.generalProfile!!.userInfo.countryCode!!)
        if (!TextUtils.isEmpty(objectResponse!!.generalProfile!!.userInfo.aboutMe))
            binding!!.tvAbout.text = objectResponse!!.generalProfile!!.userInfo.aboutMe
        if (!TextUtils.isEmpty(objectResponse!!.generalProfile!!.userInfo.phoneNo))
            binding!!.tvPhoneNumber.text = objectResponse!!.generalProfile!!.userInfo.phoneNo
        if (!TextUtils.isEmpty(objectResponse!!.generalProfile!!.userInfo.website))
            binding!!.tvWebsite.text = objectResponse!!.generalProfile!!.userInfo.website
        if (!TextUtils.isEmpty(objectResponse!!.generalProfile!!.userInfo.email))
            binding!!.tvEmail.text = objectResponse!!.generalProfile!!.userInfo.email
        if (!TextUtils.isEmpty(objectResponse!!.generalProfile!!.userInfo.hobbies))
            binding!!.tvHobbies.text = objectResponse!!.generalProfile!!.userInfo.hobbies
        if (!TextUtils.isEmpty(objectResponse!!.generalProfile!!.userInfo.studentOf))
            binding!!.tvCollege.text = objectResponse!!.generalProfile!!.userInfo.studentOf
        if (!TextUtils.isEmpty(objectResponse!!.generalProfile!!.userInfo.homeTown))
            binding!!.tvHometown.text = objectResponse!!.generalProfile!!.userInfo.homeTown
        if (!TextUtils.isEmpty(objectResponse!!.generalProfile!!.userInfo.highSchool))
            binding!!.tvHighSchool.text = objectResponse!!.generalProfile!!.userInfo.highSchool
        if (!TextUtils.isEmpty(objectResponse!!.generalProfile!!.userInfo.currentCity))
            binding!!.tvCurrentCity.text = objectResponse!!.generalProfile!!.userInfo.currentCity + ", " + objectResponse!!.generalProfile!!.userInfo.state + ", " + objectResponse!!.generalProfile!!.userInfo.country
        if (!TextUtils.isEmpty(objectResponse!!.generalProfile!!.userInfo.employer))
            binding!!.tvEmployer.text = objectResponse!!.generalProfile!!.userInfo.employer
        if (!TextUtils.isEmpty(objectResponse!!.generalProfile!!.userInfo.organization))
            binding!!.tvOrganization.text = objectResponse!!.generalProfile!!.userInfo.organization
        if (!TextUtils.isEmpty(objectResponse!!.generalProfile!!.userInfo.birthday) && !objectResponse!!.generalProfile!!.userInfo.birthday!!.startsWith("000")) {
            val birthday: Array<String>
            if (objectResponse!!.generalProfile!!.userInfo.birthday!!.contains("T")) {
                birthday = objectResponse!!.generalProfile!!.userInfo.birthday!!.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                binding!!.tvDob.text = DateUtils.getBirthdayDate(birthday[0])
            } else
                binding!!.tvDob.text = DateUtils.getBirthdayDate(objectResponse!!.generalProfile!!.userInfo.birthday!!)
        }
        if (!TextUtils.isEmpty(objectResponse!!.generalProfile!!.userInfo.profilePicture)) {
            HooleyApp.db.putString(Constants.USER_AVATAR, objectResponse!!.generalProfile!!.userInfo.profilePicture!!)
            EventBus.getDefault().post(MessageEvent(objectResponse!!.generalProfile!!.userInfo.profilePicture!!, "DP"))
            Glide.with(HooleyMain.activity!!).load(objectResponse!!.generalProfile!!.userInfo.profilePicture).into(binding!!.ivAvatar)
//            Glide.with(HooleyMain.activity!!).load(objectResponse!!.generalProfile!!.userInfo.profilePicture).into((HooleyMain.activity!! as Activity))
        }

        if (objectResponse!!.generalProfile!!.userInfo.isPhoneVerified) {
            binding!!.btnVerified.visibility = View.VISIBLE
            binding!!.btnNotVerified.visibility = View.GONE
        } else {
            binding!!.btnVerified.visibility = View.GONE
            binding!!.btnNotVerified.visibility = View.VISIBLE
        }
        setGender()
        setRelationShip()
        setPolicitcalView()
        setReligion()

        setLanguages()
        setEthinic()
        setAstrologicalSign()
        setGeofence()
        setInterstRv()
        setOccupation()
        setCollegeMajor()
        setUserSocial()
    }

    private fun setGeofence() {
        if (objectResponse!!.generalProfile!!.userGeofence.size > 0) {
            binding!!.tvGeofence.text = objectResponse!!.generalProfile!!.userGeofence[0].address
        }
    }


    @SuppressLint("SetTextI18n")
    private fun setGender() {
        if (!TextUtils.isEmpty(objectResponse!!.generalProfile!!.userInfo.gender))
            when {
                objectResponse!!.generalProfile!!.userInfo.gender.equals("1", ignoreCase = true) -> binding!!.tvGender.text = "Male"
                objectResponse!!.generalProfile!!.userInfo.gender.equals("2", ignoreCase = true) -> binding!!.tvGender.text = "Female"
                else -> binding!!.tvGender.text = "Other"
            }
    }

    private fun setCollegeMajor() {
        if (!objectResponse!!.generalProfile!!.userInfo.collageMajor.equals("0", ignoreCase = true)) {
            for (i in model.collageMajorList!!.indices) {
                if (model.collageMajorList!![i].id.equals(objectResponse!!.generalProfile!!.userInfo.collageMajor, ignoreCase = true)) {
                    binding!!.tvCollegeMajor.text = model.collageMajorList!![i].name
                }
            }
        }
    }

    private fun setRelationShip() {
        if (!objectResponse!!.generalProfile!!.userInfo.relationshipId.equals("0", ignoreCase = true)) {
            for (i in model.relationshipList!!.indices) {
                if (model.relationshipList!![i].id.equals(objectResponse!!.generalProfile!!.userInfo.relationshipId, ignoreCase = true)) {
                    binding!!.tvRelationShip.text = model.relationshipList!![i].name
                }
            }
        }
    }

    private fun setPolicitcalView() {
        if (!objectResponse!!.generalProfile!!.userInfo.politicalViewId.equals("0", ignoreCase = true)) {
            for (i in model.politicalViewsList!!.indices) {
                if (model.politicalViewsList!![i].id.equals(objectResponse!!.generalProfile!!.userInfo.politicalViewId, ignoreCase = true)) {
                    binding!!.tvPoliticalView.text = model.politicalViewsList!![i].name
                }
            }
        }
    }

    private fun setReligion() {
        if (!objectResponse!!.generalProfile!!.userInfo.religionId.equals("0", ignoreCase = true)) {
            for (i in model.religionList!!.indices) {
                if (model.religionList!![i].id.equals(objectResponse!!.generalProfile!!.userInfo.religionId, ignoreCase = true)) {
                    binding!!.tvReligion.text = model.religionList!![i].name

                }
            }
        }
    }

    private fun setLanguages() {
        if (!objectResponse!!.generalProfile!!.userInfo.languageId.equals("0", ignoreCase = true)) {
            for (i in model.languagesList!!.indices) {
                if (model.languagesList!![i].id.equals(objectResponse!!.generalProfile!!.userInfo.languageId, ignoreCase = true)) {
                    binding!!.tvLanguage.text = model.languagesList!![i].name
                }
            }
        }
    }

    private fun setOccupation() {
        if (!objectResponse!!.generalProfile!!.userInfo.occupationId.equals("0", ignoreCase = true)) {
            for (i in model.userOccupation!!.indices) {
                if (model.userOccupation!![i].id.equals(objectResponse!!.generalProfile!!.userInfo.occupationId, ignoreCase = true)) {
                    binding!!.tvOccupation.text = model.userOccupation!![i].name
                }
            }
        }
    }

    private fun setEthinic() {

        if (objectResponse!!.generalProfile!!.userInfo.ethnicBackgroundId != null) {
            if (!objectResponse!!.generalProfile!!.userInfo.ethnicBackgroundId.equals("0", ignoreCase = true)) {
                for (i in model.ethnicBackgroundList!!.indices) {
                    if (model.ethnicBackgroundList!![i].id.equals(objectResponse!!.generalProfile!!.userInfo.ethnicBackgroundId, ignoreCase = true)) {
                        binding!!.tvEthinic.text = model.ethnicBackgroundList!![i].name
                    }
                }
            }
        }
    }

    private fun setAstrologicalSign() {
        if (!objectResponse!!.generalProfile!!.userInfo.astrologicalSignId.equals("0", ignoreCase = true)) {
            for (i in model.astrologicalSigns!!.indices) {
                if (model.astrologicalSigns!![i].id.equals(objectResponse!!.generalProfile!!.userInfo.astrologicalSignId, ignoreCase = true)) {
                    binding!!.tvAstrologicalSign.text = model.astrologicalSigns!![i].name
                }
            }
        }
    }

    private fun setInterstRv() {
        val builder = StringBuilder()

        if (objectResponse!!.generalProfile!!.userInterestList.size > 0) {
            for (i in objectResponse!!.generalProfile!!.userInterestList.indices) {
                builder.append(objectResponse!!.generalProfile!!.userInterestList[i].name + ", ")
            }

            binding!!.tvInterests.text = builder.toString().replace(", $".toRegex(), "")
        } else {
            binding!!.tvInterests.text = "--"
        }


    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnEditPrfle, R.id.btnEditProfile -> if (objectResponse != null)
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, EditPersonalProfileFragment.newInstance(objectResponse!!), "EditPersonalProfileFragment")
            R.id.rlStripe -> {
                stripeConnectFragment = StripeConnectFragment.newInstance()
                stripeConnectFragment!!.setTargetFragment(ProfileMainFragment.instance, REQUEST_CODE_STRIPE)
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, stripeConnectFragment!!, "stripeConnectFragment")
            }

            R.id.ivFaceBook -> onSocialClick(Constants.SOCIAL_FB)
            R.id.ivTwitter -> onSocialClick(Constants.SOCIAL_TW)
            R.id.ivLinkedIn -> onSocialClick(Constants.SOCIAL_LI)
            R.id.ivInstagram -> onSocialClick(Constants.SOCIAL_INSTA)
            R.id.ivFlicker -> onSocialClick(Constants.SOCIAL_FLKR)
            R.id.ivAvatar -> {
                val avatarfragment = ViewAvatarFragment(HooleyApp.db.getString(Constants.USER_ID).toInt())
                avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
//                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ViewAvatarFragment.newInstance(HooleyApp.db.getString(Constants.USER_ID)), "ViewAvatarFragment")
            }
            R.id.ivInfoHooleyWallet -> {
                setupHolleyWalletDialog("Setup Hooley Wallet", getString(R.string.str_setup_holley_wallet))
                stripeDialog!!.show()
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_STRIPE && resultCode == Activity.RESULT_OK) {
            HooleyApp.db.putBoolean(Constants.IS_STRIPE_CONNECTED, true)
            code = data!!.getStringExtra("code")
            connectStripe()
            Log.e("StripeCode", " " + data.getStringExtra("code"))
        }
    }


    private fun connectStripe() {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        HooleyMain.activity!!.showDialog()
        service.connectStripe(code!!, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                Util.showToastMessage(HooleyMain.activity!!, "Stripe Connect Successfully")
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }


    private fun setUserSocial() {
        if (objectResponse!!.generalProfile!!.socialMediasList.size > 0) {
            for (i in objectResponse!!.generalProfile!!.socialMediasList.indices) {
                when (objectResponse!!.generalProfile!!.socialMediasList[i].name) {
                    Constants.SOCIAL_FB -> if (objectResponse!!.generalProfile!!.socialMediasList[i].isConnected) {
                        binding!!.ivFaceBook.setBackgroundResource(R.drawable.ic_socail_fb)
                        HooleyApp.db.putString(Constants.SOCIAL_USER_ID, objectResponse!!.generalProfile!!.socialMediasList[i].socialUrl!!)
                    } else {
                        binding!!.ivFaceBook.setBackgroundResource(R.drawable.ic_social_fb_dissconnect)
                    }
                    Constants.SOCIAL_TW -> if (objectResponse!!.generalProfile!!.socialMediasList[i].isConnected) {
                        binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_socail_twitter)
                        HooleyApp.db.putString(Constants.TWITTER_USER_ID, objectResponse!!.generalProfile!!.socialMediasList[i].socialUrl!!)
                    } else {
                        binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_social_twitter_dissconnect)
                    }
                    Constants.SOCIAL_LI -> if (objectResponse!!.generalProfile!!.socialMediasList[i].isConnected) {
                        binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_socail_linkedin)
                        HooleyApp.db.putString(Constants.LINKEDIN_USER_ID, objectResponse!!.generalProfile!!.socialMediasList[i].socialUrl!!)
                    } else {
                        binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_social_linkedin_dissconnected)
                    }
                    Constants.SOCIAL_INSTA -> if (objectResponse!!.generalProfile!!.socialMediasList[i].isConnected) {
                        binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram)
                        HooleyApp.db.putString(Constants.INSTAGRAM_USER_ID, objectResponse!!.generalProfile!!.socialMediasList[i].socialUrl!!)
                    } else {
                        binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram_dissconnect)
                    }
                    Constants.SOCIAL_FLKR -> if (objectResponse!!.generalProfile!!.socialMediasList[i].isConnected) {
                        binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_socail_filker)
                        HooleyApp.db.putString(Constants.FLICKER_USER_ID, objectResponse!!.generalProfile!!.socialMediasList[i].socialUrl!!)
                    } else {
                        binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_social_filker_dissconnect)
                    }
                }
            }
        }
    }


    private fun onSocialClick(type: String) {
        if (objectResponse!!.generalProfile!!.socialMediasList.size > 0) {
            for (i in objectResponse!!.generalProfile!!.socialMediasList.indices) {
                if (objectResponse!!.generalProfile!!.socialMediasList[i].name.equals(type, ignoreCase = true)) {
                    if (!TextUtils.isEmpty(objectResponse!!.generalProfile!!.socialMediasList[i].socialUrl)) {
                        try {
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(objectResponse!!.generalProfile!!.socialMediasList[i].socialUrl)))
                            break
                        } catch (ex: ActivityNotFoundException) {
                            ex.printStackTrace()
                            Util.showToastMessage(HooleyMain.activity!!, "Invalid url")
                            break
                        }

                    }
                }
            }
        }
    }
}
