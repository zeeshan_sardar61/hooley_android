package com.hooleyapp.hooley.fragments.ticket

import android.app.Activity
import android.app.Dialog
import android.app.DialogFragment
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.RadioGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterMoreMediaDialoge
import com.hooleyapp.hooley.adapters.AdapterMyTickets
import com.hooleyapp.hooley.adapters.AdapterSendGiftTicket
import com.hooleyapp.hooley.databinding.FragmentMyTicketsBinding
import com.hooleyapp.hooley.db.DbManager
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.friends.SelectFriendFragment
import com.hooleyapp.hooley.fragments.others.MoreMediaDialogFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.FriendsModel
import com.hooleyapp.hooley.model.MyTicketModel
import com.hooleyapp.hooley.model.SendGiftModel
import com.hooleyapp.hooley.model.SetAlertModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.others.MessageEvent
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.services.TicketWebService
import com.hooleyapp.hooley.tables.FriendsTable
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*

/**
 * Created by Nauman on 5/7/2018.
 */

class MyTicketFragment : BaseFragment(), AdapterMyTickets.IMyTicketClickListener, RadioGroup.OnCheckedChangeListener, AdapterSendGiftTicket.ISendGiftTicketClickListener, TicketPagerFragment.GiftClickListener, SwipeRefreshLayout.OnRefreshListener {

    var binding: FragmentMyTicketsBinding? = null
    private var objectResponseMyTicket: MyTicketModel? = null
    private val objectResponseSendGiftTicket: SendGiftModel? = null
    private var adapterMyTickets: AdapterMyTickets? = null
    private var adapterSendGiftTicket: AdapterSendGiftTicket? = null
    private val previousSeletedDate: Date? = null
    private var itemposition = -1
    var mService = TicketWebService()
    private var isSwiped = false
    internal var isMyTicket = false
    internal var isGifted = false
    internal var webService = TicketWebService()
    internal var font1 = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)
    internal var font2 = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)
    private var customDialog: CustomDateTimePicker? = null
    private var dateText = ""
    internal val filterList = ArrayList<AdapterMoreMediaDialoge.MoreItem>()
    private var moreFilterDialogeFragment: MoreMediaDialogFragment? = null
    private var selectFriendFragment: SelectFriendFragment? = null
    private val REQUEST_CODE = 963
    private var service = FriendWebService()
    private var friendList: MutableList<FriendsTable>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_tickets, container, false)
        HooleyMain.activity!!.supportActionBar!!.show()
        hideDrawer(HooleyMain.activity!!)
        setListener()
        setMoreAlbumCallerBackListener()
        binding!!.rbMyTicket.isChecked = true
        getMyTickets()
        //        getSendGiftTickets();
        getFriendList()
        return binding!!.root
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        if (event.message == "ticketRefresh")
            getMyTickets()
    }


    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    private fun getFriendList() {
        if (friendList == null)
            friendList = ArrayList()
        if (friendList!!.size > 0)
            friendList!!.clear()
        friendList = DbManager.getInstance().friendList
        if (friendList!!.size > 0) {
            //set
            getFriends(false)
        } else {
            getFriends(false)
        }
    }

    private fun getFriends(showDialog: Boolean) {
        if (showDialog) {
            HooleyMain.activity!!.showDialog()
        }
        service = FriendWebService()
        service.getMyFriendsList(object : GenericCallback<FriendsModel> {
            override fun success(result: FriendsModel) {
                HooleyMain.activity!!.removeDialog()
                if (showDialog) {
                    if (friendList == null)
                        friendList = ArrayList()
                    friendList!!.addAll(result.friendsList)
                } else {
                    if (friendList!!.size != result.friendsList.size) {
                        friendList!!.clear()
                        friendList!!.addAll(result.friendsList)
                        DbManager.getInstance().friendList = friendList
                    } else {
                        if (friendList == null)
                            friendList = ArrayList()
                        friendList!!.clear()
                        friendList!!.addAll(result.friendsList)
                    }
                }
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    Log.e("ERROR", message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val friendsTable = getSelectedFriend(data!!.getParcelableArrayListExtra("friendListIds"))
            (HooleyMain.activity as ActivityBase).addFragment(R.id.container, GiftTicketFragment.newInstance(friendsTable, ticketListItem, itemposition), "ConfirmFeedbackFragment")
        }

    }

    private fun getSelectedFriend(friendListIds: ArrayList<FriendsTable>): FriendsTable {
        var friendsTable = FriendsTable()
        if (friendList == null) {
            friendList = DbManager.getInstance().friendList
        }
        for (i in friendList!!.indices) {
            for (j in friendListIds.indices) {
                if (friendList!![i].userId == friendListIds[j].userId) {
                    friendsTable = friendList!![i]
                    break
                }
            }
        }
        return friendsTable
    }


    private fun setListener() {
        binding!!.slTickets.setOnRefreshListener(this)
        binding!!.myTicketTab.setOnCheckedChangeListener(this)
    }

    private fun setRecyclerView() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding!!.rvMyTickets.layoutManager = manager
        binding!!.rvMyTickets.layoutAnimation = animation
        adapterMyTickets = AdapterMyTickets(HooleyMain.activity!!, objectResponseMyTicket!!.myTicketsList)
        adapterMyTickets!!.setListener(this)
        binding!!.rvMyTickets.adapter = adapterMyTickets
    }

    private fun setGiftTicketRecyclerView() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding!!.rvGiftedTickets.layoutManager = manager
        binding!!.rvGiftedTickets.layoutAnimation = animation
        adapterSendGiftTicket = AdapterSendGiftTicket(HooleyMain.activity!!, objectResponseMyTicket!!.giftedTicketsList)
        adapterSendGiftTicket!!.setListener(this)
        binding!!.rvGiftedTickets.adapter = adapterSendGiftTicket
    }

    private fun getMyTickets() {
        if (binding != null && !isSwiped) {
            binding!!.pbMyTickets.visibility = View.VISIBLE
        }
        mService.getMyTickets(object : GenericCallback<MyTicketModel> {
            override fun success(result: MyTicketModel) {
                binding!!.pbMyTickets.visibility = View.GONE
                if (isSwiped) {
                    binding!!.slTickets.isRefreshing = false
                    isSwiped = false
                }
                objectResponseMyTicket = result
                if (result.myTicketsList != null && result.myTicketsList!!.size > 0) {
                    for (i in result.myTicketsList!!.indices) {
                        for (j in result.myTicketsList!![i].ticketList.indices)
                            if (!result.myTicketsList!![i].ticketList[j].qrCodeUrl.isEmpty()) {
                                result.myTicketsList!![i].ticketList[j].qrBM = Util.createImageFromString(HooleyMain.activity!!, result.myTicketsList!![i].ticketList[j].qrCodeUrl)
                            }
                    }
                    setRecyclerView()
                }
                if (result.giftedTicketsList != null && result.giftedTicketsList!!.size > 0) {
                    setGiftTicketRecyclerView()
                }
                if (isMyTicket && result.myTicketsList!!.size == 0) {
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.tvNoGData.visibility = View.GONE
                    binding!!.tvNoData.text = "No tckets found"
                }
                if (isGifted && result.giftedTicketsList!!.size == 0) {
                    binding!!.tvNoData.visibility = View.GONE
                    binding!!.tvNoGData.visibility = View.VISIBLE
                    binding!!.tvNoGData.text = "No gifted tickets found"

                }

                if (isMyTicket && result.myTicketsList!!.size == 0 && result.giftedTicketsList!!.size == 0) {
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.tvNoGData.visibility = View.GONE
                    binding!!.tvNoData.text = "No Tickets found"
                }
                if (isGifted && result.myTicketsList!!.size == 0 && result.giftedTicketsList!!.size == 0) {
                    binding!!.tvNoData.visibility = View.GONE
                    binding!!.tvNoGData.visibility = View.VISIBLE
                    binding!!.tvNoGData.text = "No gifted tickets found"
                }

            }

            override fun failure(message: String) {
                if (isSwiped) {
                    binding!!.slTickets.isRefreshing = false
                    isSwiped = false
                }
                binding!!.pbMyTickets.visibility = View.GONE
                try {
                    if (isMyTicket) {
                        binding!!.rvMyTickets.visibility = View.GONE
                        binding!!.tvNoData.text = message
                        binding!!.tvNoData.visibility = View.VISIBLE
                        binding!!.tvNoGData.visibility = View.GONE
                    }
                    if (isGifted) {
                        binding!!.rvGiftedTickets.visibility = View.GONE
                        binding!!.tvNoGData.visibility = View.VISIBLE
                        binding!!.tvNoData.visibility = View.GONE
                        binding!!.tvNoGData.text = message
                    }
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }


    private fun setTicketAlert(alarmPosition: Int, date: String) {
        binding!!.pbMyTickets.visibility = View.VISIBLE
        webService.setTicketAlert(objectResponseMyTicket!!.myTicketsList!![alarmPosition].ticketList[0].id!!, objectResponseMyTicket!!.myTicketsList!![alarmPosition].eventId!!, date, true, object : GenericCallback<SetAlertModel> {
            override fun success(result: SetAlertModel) {
                binding!!.pbMyTickets.visibility = View.GONE
                objectResponseMyTicket!!.myTicketsList!![alarmPosition].alertOn = !(objectResponseMyTicket!!.myTicketsList!![alarmPosition].alertOn)!!
                adapterMyTickets!!.notifyDataSetChanged()
            }

            override fun failure(message: String) {
                binding!!.pbMyTickets.visibility = View.GONE
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }

    fun showCustomDialog(startTime: String, endTime: String, alertTime: String) {
        var setTime = ""
        customDialog = CustomDateTimePicker(HooleyMain.activity!!,
                object : CustomDateTimePicker.ICustomDateTimeListener {
                    override fun onSet(dialog: Dialog, calendarSelected: Calendar,
                                       dateSelected: Date, year: Int, monthFullName: String,
                                       monthShortName: String, monthNumber: Int, date: Int,
                                       weekDayFullName: String, weekDayShortName: String,
                                       hour24: Int, hour12: Int, min: Int, sec: Int,
                                       AM_PM: String) {

                        if (dateSelected.before(Calendar.getInstance().time)) {
                            Util.showToastMessage(HooleyMain.activity!!, "Invalid date, Please select future date.")
                            return
                        }
                        if (dateSelected.after(DateUtils.stringToDate(endTime))) {
                            Util.showToastMessage(HooleyMain.activity!!, "Invalid date, date/time is greater than event end date and time")
                            return
                        }

                        setTime = (String.format("%04d-%02d-%02d", year, monthNumber + 1, calendarSelected.get(Calendar.DAY_OF_MONTH))
                                + " " + String.format("%02d:%02d", hour12, min) +
                                " " + AM_PM)

                        setTicketAlert(itemposition, setTime)

                    }

                    override fun onCancel() {

                    }
                })
        /**
         * Pass Directly current time format it will return AM and PM if you set
         * false
         */
        customDialog!!.set24HourFormat(false)
        /**
         * Pass Directly current data and time to show when it pop up
         */
        if (TextUtils.isEmpty(alertTime)) {
            customDialog!!.setDate(Calendar.getInstance())
        } else {
            customDialog!!.setDate(DateUtils.stringToDate(DateUtils.getLocalDate(alertTime)))
        }
        customDialog!!.showDialog()
    }

    override fun onClickItem(position: Int) {
        itemposition = position
//        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, TicketPagerFragment.newInstance(objectResponseMyTicket!!.myTicketsList!![itemposition], objectResponseMyTicket!!.myTicketsList!![itemposition].ticketList, 1), "TicketPagerFragment")
        var ticketFragment: DialogFragment? = null
        ticketFragment = TicketPagerFragment.newInstance(objectResponseMyTicket!!.myTicketsList!![itemposition], objectResponseMyTicket!!.myTicketsList!![itemposition].ticketList, 1)
        ticketFragment.mListener = this
        ticketFragment.show(HooleyMain.activity!!.fragmentManager, "dialog")

    }

    override fun setAlert(position: Int) {
        itemposition = position
        if ((objectResponseMyTicket!!.myTicketsList!![position].alertOn!!)) {
            showCustomDialog(objectResponseMyTicket!!.myTicketsList!![position].startTime, objectResponseMyTicket!!.myTicketsList!![position].endTime, objectResponseMyTicket!!.myTicketsList!![position].alertDate)
        } else {
//            objectResponseMyTicket!!.myTicketsList!![position].alertOn = !objectResponseMyTicket!!.myTicketsList!![position].alertOn!!
            showCustomDialog(objectResponseMyTicket!!.myTicketsList!![position].startTime, objectResponseMyTicket!!.myTicketsList!![position].endTime, "")
        }
        /*
        if ((objectResponseMyTicket!!.myTicketsList!![position].alertOn)!!)
            showCustomDialog()
        else
          setTicketAlert(itemposition, dateText)
          */
    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {

        when (checkedId) {
            R.id.rbMyTicket -> {

                binding!!.rbMyTicket.typeface = font1
                binding!!.rbGiftedTicket.typeface = font2
                binding!!.rbMyTicket.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding!!.rbMyTicket.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_left_radio)
                binding!!.rbGiftedTicket.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding!!.rbGiftedTicket.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                if (objectResponseMyTicket != null)
                    if (objectResponseMyTicket!!.myTicketsList!!.size > 0) {
                        if (binding!!.rvMyTickets.visibility == View.GONE)
                            binding!!.rvMyTickets.visibility = View.VISIBLE
                        binding!!.rvGiftedTickets.visibility = View.GONE
                        binding!!.tvNoGData.visibility = View.GONE
                    } else {
                        binding!!.rvGiftedTickets.visibility = View.GONE
                        binding!!.tvNoGData.visibility = View.VISIBLE
                    }
                isMyTicket = true
                isGifted = false
            }
            R.id.rbGiftedTicket -> {
                binding!!.rbMyTicket.typeface = font2
                binding!!.rbGiftedTicket.typeface = font1
                binding!!.rbGiftedTicket.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding!!.rbGiftedTicket.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_right_radio)
                binding!!.rbMyTicket.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding!!.rbMyTicket.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                if (objectResponseMyTicket != null)
                    if (objectResponseMyTicket!!.giftedTicketsList!!.size > 0) {
                        binding!!.rvMyTickets.visibility = View.GONE
                        binding!!.rvGiftedTickets.visibility = View.VISIBLE
                        binding!!.tvNoData.visibility = View.GONE
                    } else {
                        binding!!.rvMyTickets.visibility = View.GONE
                        binding!!.tvNoData.visibility = View.VISIBLE
                    }
                isMyTicket = false
                isGifted = true
            }
        }

    }

    override fun onLongClick(position: Int) {
        itemposition = position
        if (!moreFilterDialogeFragment!!.isAdded)
            moreFilterDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)

    }


    override fun onClickGiftItem(position: Int) {
        itemposition = position
//        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, TicketPagerFragment.newInstance(objectResponseMyTicket!!.giftedTicketsList!![itemposition], objectResponseMyTicket!!.giftedTicketsList!![itemposition].ticketList, 2), "TicketPagerFragment")
        var ticketFragment: DialogFragment? = null
        ticketFragment = TicketPagerFragment.newInstance(objectResponseMyTicket!!.giftedTicketsList!![itemposition], objectResponseMyTicket!!.giftedTicketsList!![itemposition].ticketList, 2)
        ticketFragment.mListener = this
        ticketFragment.show(HooleyMain.activity!!.fragmentManager, "dialog")
    }

    private fun setMoreAlbumCallerBackListener() {
        if (filterList.size > 0) {
            filterList.clear()
        }
        filterList.add(AdapterMoreMediaDialoge.MoreItem("Schedule Alerts", false))
//        filterList.add(AdapterMoreMediaDialoge.MoreItem("Refund Tickets", false))

        moreFilterDialogeFragment = MoreMediaDialogFragment.Builder(HooleyMain.activity!!).setTitle("More").setType(Constants.TYPE_MORE_80).setList(filterList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    moreFilterDialogeFragment!!.dismiss()
                    if ((objectResponseMyTicket!!.myTicketsList!![position].alertOn!!)) {
                        showCustomDialog(objectResponseMyTicket!!.myTicketsList!![itemposition].startTime, objectResponseMyTicket!!.myTicketsList!![itemposition].endTime, objectResponseMyTicket!!.myTicketsList!![itemposition].alertDate)
                    } else {
//                        objectResponseMyTicket!!.myTicketsList!![itemposition].alertOn = !objectResponseMyTicket!!.myTicketsList!![itemposition].alertOn!!
                        showCustomDialog(objectResponseMyTicket!!.myTicketsList!![itemposition].startTime, objectResponseMyTicket!!.myTicketsList!![itemposition].endTime, "")
                    }
                }
//                1 -> {
//                    moreFilterDialogeFragment!!.dismiss()
//                    (HooleyMain.activity as ActivityBase).addFragment(R.id.container, MyTicketRefundFragment.newInstance(objectResponseMyTicket!!.myTicketsList!![itemposition]), "TicketRefundFragment")
//                }

            }
        }.create()


    }

    private lateinit var ticketListItem: MyTicketModel.MyTicketsList

    override fun onClickTicketRefund(position: Int, item: MyTicketModel.MyTicketsList) {
        itemposition = position
        ticketListItem = item
        selectFriendFragment = SelectFriendFragment.newInstance()
        selectFriendFragment!!.setTargetFragment(HooleyMain.activity!!.getRootParentFragment(this@MyTicketFragment), REQUEST_CODE)
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, selectFriendFragment!!, "selectFriendFragment")

    }


//    override fun onClickRefund(position: Int) {
//        itemposition = position
//        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, MyTicketRefundFragment.newInstance(objectResponseMyTicket!!.myTicketsList!![itemposition]), "TicketRefundFragment")
//    }


    override fun onRefresh() {
        if (binding!!.slTickets != null) {
            binding!!.slTickets.isRefreshing = true
            binding!!.pbMyTickets.visibility = View.GONE
        }
        isSwiped = true
        getMyTickets()
    }

    companion object {

        lateinit var instance: MyTicketFragment

        fun newInstance(): MyTicketFragment {
            instance = MyTicketFragment()
            return instance
        }
    }
}
