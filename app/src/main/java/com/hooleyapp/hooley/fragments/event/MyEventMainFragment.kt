package com.hooleyapp.hooley.fragments.event

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.VPDashBoardAdapter
import com.hooleyapp.hooley.databinding.FragmentMyEventMainBinding
import com.hooleyapp.hooley.fragments.BaseFragment

/**
 * Created by Nauman on 1/17/2018.
 */

class MyEventMainFragment : BaseFragment(), View.OnClickListener {
    var adapter: VPDashBoardAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentMyEventMainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_event_main, container, false)
        setListener()
        setupViewPager()
        //        setHasOptionsMenu(true);
        setSelectedBackground(R.id.llHosting)
        return fragmentMyEventMainBinding.root
    }


    private fun seCurrentPage() {
        fragmentMyEventMainBinding.vpMyEvent.currentItem = 0
        fragmentMyEventMainBinding.rlHosting.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_gray_color))
    }

    private fun setListener() {
        fragmentMyEventMainBinding.llHosting.setOnClickListener(this)
        fragmentMyEventMainBinding.llAttending.setOnClickListener(this)
        fragmentMyEventMainBinding.llFollowing.setOnClickListener(this)
        fragmentMyEventMainBinding.llEventInvites.setOnClickListener(this)
        fragmentMyEventMainBinding.llPast.setOnClickListener(this)

    }

    private fun setupViewPager() {
        adapter = VPDashBoardAdapter(childFragmentManager)
        adapter!!.addFragment(MyHostEventFragment(), "MyHostEventFragment")
        adapter!!.addFragment(MyAttendingEventFragment(), "MyAttendingEventFragment")
        adapter!!.addFragment(MyFollowingEventFragment(), "MyFollowingEventFragment")
        adapter!!.addFragment(MyEventInviteFragment(), "MyEventInviteFragment")
        adapter!!.addFragment(MyPastEventFragment(), "MyPastEventFragment")
        fragmentMyEventMainBinding.vpMyEvent.adapter = adapter
    }


    override fun onDestroy() {
        super.onDestroy()
        if (fragmentMyEventMainBinding != null)
            fragmentMyEventMainBinding.unbind()
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.llHosting -> {
                setSelectedBackground(R.id.llHosting)
                fragmentMyEventMainBinding.vpMyEvent.setCurrentItem(0, true)
            }
            R.id.llAttending -> {
                setSelectedBackground(R.id.llAttending)
                fragmentMyEventMainBinding.vpMyEvent.setCurrentItem(1, true)
            }
            R.id.llFollowing -> {
                setSelectedBackground(R.id.llFollowing)
                fragmentMyEventMainBinding.vpMyEvent.setCurrentItem(2, true)
            }
            R.id.llEventInvites -> {
                fragmentMyEventMainBinding.vpMyEvent.setCurrentItem(3, true)
                setSelectedBackground(R.id.llEventInvites)
            }
            R.id.llPast -> {
                fragmentMyEventMainBinding.vpMyEvent.setCurrentItem(4, true)
                setSelectedBackground(R.id.llPast)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when (fragmentMyEventMainBinding.vpMyEvent.currentItem) {
            0 -> setSelectedBackground(R.id.llHosting)
            1 -> setSelectedBackground(R.id.llAttending)
            2 -> setSelectedBackground(R.id.llFollowing)
            3 -> setSelectedBackground(R.id.llEventInvites)
            4 -> setSelectedBackground(R.id.llPast)
        }
    }

    private fun setSelectedBackground(id: Int) {
        when (id) {
            R.id.llHosting -> {
                fragmentMyEventMainBinding.llHosting.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                fragmentMyEventMainBinding.llAttending.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentMyEventMainBinding.llFollowing.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentMyEventMainBinding.llEventInvites.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentMyEventMainBinding.llPast.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
            R.id.llAttending -> {
                fragmentMyEventMainBinding.llHosting.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentMyEventMainBinding.llAttending.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                fragmentMyEventMainBinding.llFollowing.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentMyEventMainBinding.llEventInvites.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentMyEventMainBinding.llPast.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
            R.id.llFollowing -> {
                fragmentMyEventMainBinding.llHosting.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentMyEventMainBinding.llAttending.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentMyEventMainBinding.llFollowing.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                fragmentMyEventMainBinding.llEventInvites.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentMyEventMainBinding.llPast.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
            R.id.llEventInvites -> {
                fragmentMyEventMainBinding.llHosting.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentMyEventMainBinding.llAttending.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentMyEventMainBinding.llFollowing.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentMyEventMainBinding.llEventInvites.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                fragmentMyEventMainBinding.llPast.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
            R.id.llPast -> {
                fragmentMyEventMainBinding.llHosting.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentMyEventMainBinding.llAttending.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentMyEventMainBinding.llFollowing.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentMyEventMainBinding.llEventInvites.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentMyEventMainBinding.llPast.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
            }
        }
    }

    companion object {

        lateinit var instance: MyEventMainFragment

        internal lateinit var fragmentMyEventMainBinding: FragmentMyEventMainBinding

        fun newInstance(): MyEventMainFragment {
            instance = MyEventMainFragment()
            return instance
        }
    }
}