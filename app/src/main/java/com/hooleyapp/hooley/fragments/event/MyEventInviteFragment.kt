package com.hooleyapp.hooley.fragments.event

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterOtherEvent
import com.hooleyapp.hooley.databinding.FragmentOtherEventBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.MyHostEventModel
import com.hooleyapp.hooley.model.MyInviteEventModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.services.EventWebService
import java.util.*

/**
 * Created by Nauman on 4/23/2018.
 */

class MyEventInviteFragment : BaseFragment(), AdapterOtherEvent.IMyEventsClickListeners, SwipeRefreshLayout.OnRefreshListener {


    internal var binding: FragmentOtherEventBinding? = null
    private var adapterOtherEvent: AdapterOtherEvent? = null
    private var list = ArrayList<MyHostEventModel.HostEvents>()
    private val gson = Gson()
    private val myInviteEventModel: MyInviteEventModel? = null
    private var responseObj: MyInviteEventModel? = null
    private val service = EventWebService()
    internal var isSwiped = false

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {

        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            try {
                getMyFollowingEvent()
            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            }

        } else {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_other_event, container, false)
        setListener()
        return binding!!.root
    }


    private fun setListener() {
        binding!!.slAttend.setOnRefreshListener(this)
    }

    private fun getMyFollowingEvent() {
        if (binding != null && isSwiped == false)
            binding!!.pbOther.visibility = View.VISIBLE

        service.getMyEventInvite(object : IWebServiceCallback<MyInviteEventModel> {
            override fun success(result: MyInviteEventModel) {
                binding!!.tvNoData.visibility = View.GONE
                binding!!.pbOther.visibility = View.GONE
                responseObj = result
                if (isSwiped == true) {
                    binding!!.slAttend.isRefreshing = false
                    isSwiped = false
                }
                list = result.inviteEvents
                setRecyclerView()

            }

            override fun failure(message: String) {
                binding!!.rvOtherEvent.visibility = View.GONE
                if (isSwiped == true) {
                    binding!!.slAttend.isRefreshing = false
                    isSwiped = false
                }
                binding!!.pbOther.visibility = View.GONE
                try {
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.tvNoData.text = message
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })

    }


    private fun setRecyclerView() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        binding!!.rvOtherEvent.layoutAnimation = animation
        binding!!.rvOtherEvent.layoutManager = manager
        binding!!.rvOtherEvent.setEmptyView(binding!!.tvNoData)
        adapterOtherEvent = AdapterOtherEvent(HooleyMain.activity!!, list, false)
        adapterOtherEvent!!.mListener = this
        binding!!.rvOtherEvent.adapter = adapterOtherEvent
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null) {
            binding!!.unbind()
        }
    }

    override fun onRefresh() {
        if (binding!!.slAttend != null) {
            binding!!.slAttend.isRefreshing = true
            binding!!.pbOther.visibility = View.GONE
        }
        isSwiped = true
        getMyFollowingEvent()
    }

    override fun onClickItem(position: Int) {
        HooleyApp.db.putBoolean(Constants.IS_COMMENT_CLICKED, false)
        (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment
                .newInstance(list[position].id!!), "LiveEventFragment")
    }

    override fun onClickAlert(position: Int) {
    }

    override fun onLongClick(position: Int) {
    }
}
