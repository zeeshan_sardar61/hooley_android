package com.hooleyapp.hooley.fragments.event

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterPromotionStats
import com.hooleyapp.hooley.databinding.FragmentPromotionStatsBinding
import com.hooleyapp.hooley.databinding.PromotionStatsHeaderBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.load
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.EventPromotionStatsModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.others.Util
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PromotionStatsFragment : BaseFragment(), View.OnClickListener, AdapterPromotionStats.IPromotionStatsClickListener {
    lateinit var binding: FragmentPromotionStatsBinding
    lateinit var promotionStatsHeaderBinding: PromotionStatsHeaderBinding
    private var objectResponsePromotionData: Response<EventPromotionStatsModel>? = null
    private var adapterPromotionStat: AdapterPromotionStats? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_promotion_stats, container, false)
        getPromotionStatsData()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
    }


    private fun setListener() {}


    override fun onClick(v: View) {

    }

    // TODO MOVE TO SERVICE
    fun getPromotionStatsData() {
        HooleyMain.activity!!.showDialog()
        val call = HooleyApp.apiService.getEventPromotionStats(HooleyApp.db.getString(Constants.USER_ID), eventId)
        call.enqueue(object : Callback<EventPromotionStatsModel> {
            override fun onResponse(call: Call<EventPromotionStatsModel>, response: Response<EventPromotionStatsModel>) {
                HooleyMain.activity!!.removeDialog()
                if (response.body() == null) {
                    Util.showToastMessage(HooleyMain.activity!!, "Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    objectResponsePromotionData = response
                    Logger.ex("objectResponsePromotionData:" + Gson().toJson(objectResponsePromotionData))
                    setData()
                } else {
                    Util.showToastMessage(HooleyMain.activity!!, response.body()!!.exception)
                }
            }

            override fun onFailure(call: Call<EventPromotionStatsModel>, t: Throwable) {
                Logger.ex("ERROR:" + t.message)
                Util.showToastMessage(HooleyMain.activity!!, "Server not responding")
            }
        })
    }

    private fun setData() {
        promotionStatsHeaderBinding = DataBindingUtil.inflate(LayoutInflater.from(binding.elvPromotionStats.context), R.layout.promotion_stats_header, binding.elvPromotionStats, false)
        binding.elvPromotionStats.addHeaderView(promotionStatsHeaderBinding.root)
        assert(objectResponsePromotionData!!.body() != null)
        Glide.with(HooleyMain.activity!!).load(objectResponsePromotionData!!.body()!!.promotionStats!!.coverPhoto).into(promotionStatsHeaderBinding.ivEventCover)
        promotionStatsHeaderBinding.tvEventName.text = objectResponsePromotionData!!.body()!!.promotionStats!!.eventName
        promotionStatsHeaderBinding.tvHostName.text = "Hosted by - " + objectResponsePromotionData!!.body()!!.promotionStats!!.fullName
        if (objectResponsePromotionData!!.body()!!.promotionStats!!.profilePic!!.isNotEmpty())
            promotionStatsHeaderBinding.ivAvatar.load(objectResponsePromotionData!!.body()!!.promotionStats!!.profilePic!!)

        promotionStatsHeaderBinding.tvEventAddress.text = objectResponsePromotionData!!.body()!!.promotionStats!!.address
        promotionStatsHeaderBinding.tvStartPromotion.text = DateUtils.getDate(DateUtils.getLocalDate(objectResponsePromotionData!!.body()!!.promotionStats!!.startTime!!))
//        var dateDifference = DateUtils.getMinutesDifference(DateUtils.getCurrentDate()!!, DateUtils.getLocalDate(objectResponsePromotionData!!.body()!!.promotionStats!!.endTime!!))
//        if (dateDifference > 0) {
//            promotionStatsHeaderBinding.tvEndPromotion.text = DateUtils.getDateDaysHoursDifference(objectResponsePromotionData!!.body()!!.promotionStats!!.endTime!!)
//        } else {
        promotionStatsHeaderBinding.tvEndPromotion.text = DateUtils.getDate(DateUtils.getLocalDate(objectResponsePromotionData!!.body()!!.promotionStats!!.endTime!!))
//        }
        promotionStatsHeaderBinding.tvEventViews.text = Integer.toString(objectResponsePromotionData!!.body()!!.promotionStats!!.totalViewCount)
        binding.elvPromotionStats.setGroupIndicator(null)
        binding.elvPromotionStats.isClickable = true
        adapterPromotionStat = AdapterPromotionStats(binding.elvPromotionStats, HooleyMain.activity!!, objectResponsePromotionData!!.body()!!.promotionStats!!.statesList)
        adapterPromotionStat!!.setListener(this)
        binding.elvPromotionStats.setAdapter(adapterPromotionStat)

    }

    override fun onClickParent(isExpanded: Boolean, position: Int) {
        if (isExpanded)
            binding.elvPromotionStats.collapseGroup(position)
        else
            binding.elvPromotionStats.expandGroup(position)
    }

    companion object {

        lateinit var instance: PromotionStatsFragment
        private var eventId: String? = null

        fun newInstance(id: String): PromotionStatsFragment {
            eventId = id
            instance = PromotionStatsFragment()
            return instance
        }
    }
}
