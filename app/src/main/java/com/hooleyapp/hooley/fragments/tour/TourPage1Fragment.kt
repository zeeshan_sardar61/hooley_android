package com.hooleyapp.hooley.fragments.tour

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.ActivityDecisionBinding
import com.hooleyapp.hooley.fragments.BaseFragment

/**
 * Created by Nauman on 2/28/2018.
 */

class TourPage1Fragment : BaseFragment() {

    internal var binding: ActivityDecisionBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_decision, container, false)

        return binding!!.root
    }


    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    companion object {

        var instance: TourPage1Fragment? = null
    }
}
