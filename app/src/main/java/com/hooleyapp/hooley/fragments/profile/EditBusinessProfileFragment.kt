package com.hooleyapp.hooley.fragments.profile

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.text.TextUtils
import android.view.*
import android.widget.*
import com.alimuzaffar.lib.pin.PinEntryEditText
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterCountries
import com.hooleyapp.hooley.adapters.AdapterSpinner
import com.hooleyapp.hooley.databinding.FragmentEditBusinessProfileBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.ICallBackUri
import com.hooleyapp.hooley.model.*
import com.hooleyapp.hooley.model.staticData.StaticDataModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.requestmodel.VerifyPhoneNumberModel
import com.hooleyapp.hooley.services.ProfileWebService
import com.rilixtech.Country
import com.rilixtech.CountryCodePicker
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Nauman on 12/13/2017.
 */

class EditBusinessProfileFragment : BaseFragment(), AdapterView.OnItemSelectedListener, View.OnClickListener, CountryCodePicker.OnCountryChangeListener, View.OnFocusChangeListener {
    internal var binding: FragmentEditBusinessProfileBinding? = null
    private var adapterSpinner: AdapterCountries? = null
    private var countryList: ArrayList<PersonalProfileModel.myObject>? = null
    private var stateList: ArrayList<PersonalProfileModel.myObject>? = null
    private var citiesList: ArrayList<PersonalProfileModel.myObject>? = null
    private var strCountry: String? = null
    private var strCountryId: String? = null
    private var strState: String? = null
    private var strStateId: String? = null
    private var strCity: String? = null
    private var strCityId: String? = null
    private var spinnerAdapter: AdapterSpinner? = null
    private var businessImageUrl = ""
    private var companyTypePosition: Int = 0
    private var industryPosition: Int = 0
    private var cc = ""
    private var ccIso = ""
    private var media: BusinessProfileModel.SocialMedia? = null
    private var messageId: String? = ""
    private var verifyNumberDialog: Dialog? = null
    private var countDownTimer: CountDownTimer? = null
    internal var isValid = true
    internal var service = ProfileWebService()
    private var responseCountry = CountryStateCitiesModel()
    private var mediaUri: Uri? = null
    var model = StaticDataModel()
    var gson = Gson()
    private var moreDialogeFragment: MoreDialogeFragment? = null


    private val socailList: ArrayList<BusinessProfileModel.SocialMedia>
        get() {
            if (responseObject!!.businessProfile!!.socialMediaArrayList.size > 0) {
                for (i in responseObject!!.businessProfile!!.socialMediaArrayList.indices) {
                    when (responseObject!!.businessProfile!!.socialMediaArrayList[i].socialName!!.toLowerCase()) {
                        Constants.SOCIAL_FB.toLowerCase() -> if (!TextUtils.isEmpty(binding!!.edFaceBook.text.toString()))
                            responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl = binding!!.edFaceBook.text.toString()
                        else
                            responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl = ""
                        Constants.SOCIAL_TW.toLowerCase() -> if (!TextUtils.isEmpty(binding!!.edTwitter.text.toString()))
                            responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl = binding!!.edTwitter.text.toString()
                        else
                            responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl = ""
                        Constants.SOCIAL_LI.toLowerCase() -> if (!TextUtils.isEmpty(binding!!.edLinkedIn.text.toString()))
                            responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl = binding!!.edLinkedIn.text.toString()
                        else
                            responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl = ""
                        Constants.SOCIAL_INSTA.toLowerCase() -> if (!TextUtils.isEmpty(binding!!.edInstagram.text.toString()))
                            responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl = binding!!.edInstagram.text.toString()
                        else
                            responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl = ""
                        Constants.SOCIAL_FLKR.toLowerCase() -> if (!TextUtils.isEmpty(binding!!.edFlicker.text.toString()))
                            responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl = binding!!.edFlicker.text.toString()
                        else
                            responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl = ""
                    }
                }
                return responseObject!!.businessProfile!!.socialMediaArrayList
            } else {
                val firstScialList = ArrayList<BusinessProfileModel.SocialMedia>()
                media = BusinessProfileModel.SocialMedia()
                media!!.id = 0
                media!!.socialName = Constants.SOCIAL_FB.toLowerCase()
                if (!TextUtils.isEmpty(binding!!.edFaceBook.text.toString()))
                    media!!.socialUrl = binding!!.edFaceBook.text.toString()
                else
                    media!!.socialUrl = ""
                firstScialList.add(media!!)
                media = BusinessProfileModel.SocialMedia()
                media!!.id = 0
                media!!.socialName = Constants.SOCIAL_TW.toLowerCase()
                if (!TextUtils.isEmpty(binding!!.edTwitter.text.toString()))
                    media!!.socialUrl = binding!!.edTwitter.text.toString()
                else
                    media!!.socialUrl = ""
                firstScialList.add(media!!)
                media = BusinessProfileModel.SocialMedia()
                media!!.id = 0
                media!!.socialName = Constants.SOCIAL_LI.toLowerCase()
                if (!TextUtils.isEmpty(binding!!.edLinkedIn.text.toString()))
                    media!!.socialUrl = binding!!.edLinkedIn.text.toString()
                else
                    media!!.socialUrl = ""
                firstScialList.add(media!!)
                media = BusinessProfileModel.SocialMedia()
                media!!.id = 0
                media!!.socialName = Constants.SOCIAL_FLKR.toLowerCase()
                if (!TextUtils.isEmpty(binding!!.edFlicker.text.toString()))
                    media!!.socialUrl = binding!!.edFlicker.text.toString()
                else
                    media!!.socialUrl = ""
                firstScialList.add(media!!)
                media = BusinessProfileModel.SocialMedia()
                media!!.id = 0
                media!!.socialName = Constants.SOCIAL_INSTA.toLowerCase()
                if (!TextUtils.isEmpty(binding!!.edInstagram.text.toString()))
                    media!!.socialUrl = binding!!.edInstagram.text.toString()
                else
                    media!!.socialUrl = ""
                firstScialList.add(media!!)
                return firstScialList
            }
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_business_profile, container, false)
        (HooleyMain.activity as HooleyMain).removeBubbleView()
        setToolbar()
        setListener()
        model = gson.fromJson(HooleyApp.db.getString(Constants.STATIC_MODEL), StaticDataModel::class.java)

        getCountries("country", "0", "0", Constants.TYPE_COUNTRIES)
        //checking either its a create event or edit event call
        if (responseObject != null) {
            setData()
        }
        setHasOptionsMenu(true)
        return binding!!.root
    }

    private fun setToolbar() {
        HooleyMain.activity!!.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        HooleyMain.activity!!.supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
    }

    private fun setMoreCallerBackListener() {
        val arrayList = ArrayList<String>()
        arrayList.add("Photo")
        arrayList.add("Gallery")
        moreDialogeFragment = MoreDialogeFragment.Builder(activity!!).setTitle("Select").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, _ ->
            when (position) {
                0 -> {
                    moreDialogeFragment!!.dismiss()
                    HooleyMain.activity!!.startCamera(Constants.CAMERA_RQ)
                }
                1 -> {
                    moreDialogeFragment!!.dismiss()
                    HooleyMain.activity!!.showGallery(Constants.IMAGE_PICKER_SELECT)
                }
            }
        }.create()



        if (!moreDialogeFragment!!.isAdded)
            moreDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)

    }

    private fun setListener() {
        binding!!.btnSaveProfile.setOnClickListener(this)
        binding!!.btnNotVerified.setOnClickListener(this)
        binding!!.ivAddPhoto.setOnClickListener(this)
        binding!!.ccp.setOnCountryChangeListener(this)
        binding!!.edFaceBook.onFocusChangeListener = this
        binding!!.edFlicker.onFocusChangeListener = this
        binding!!.edInstagram.onFocusChangeListener = this
        binding!!.edLinkedIn.onFocusChangeListener = this
        binding!!.edTwitter.onFocusChangeListener = this

    }

    private fun setData() {
        try {
            if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.businessName))
                binding!!.edtBusinessName.setText(responseObject!!.businessProfile!!.businessInfo.businessName)
            if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.aboutBusiness))
                binding!!.edtAboutBusiness.setText(responseObject!!.businessProfile!!.businessInfo.aboutBusiness)
            if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.businessEmail))
                binding!!.edtBusinessEmail.setText(responseObject!!.businessProfile!!.businessInfo.businessEmail)
            if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.website))
                binding!!.edtBusinessWebsite.setText(responseObject!!.businessProfile!!.businessInfo.website)
            if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.businessPhone))
                binding!!.edtBusinessPhone.setText(responseObject!!.businessProfile!!.businessInfo.businessPhone)
            if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.address))
                binding!!.edtBusinessAddress.setText(responseObject!!.businessProfile!!.businessInfo.address)
            if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.zipCode))
                binding!!.tvPostalCode.setText(responseObject!!.businessProfile!!.businessInfo.zipCode)
            if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.contactName))
                binding!!.edtContactName.setText(responseObject!!.businessProfile!!.businessInfo.contactName)
            if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.country))
                binding!!.tvCountry.text = responseObject!!.businessProfile!!.businessInfo.country
            if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.city))
                binding!!.tvCity.text = responseObject!!.businessProfile!!.businessInfo.city
            if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.state))
                binding!!.tvState.text = responseObject!!.businessProfile!!.businessInfo.state
            if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.businessProfilePicture)) {
                Glide.with(HooleyMain.activity!!).load(responseObject!!.businessProfile!!.businessInfo.businessProfilePicture).into(binding!!.ivAvatar)
                businessImageUrl = responseObject!!.businessProfile!!.businessInfo.businessProfilePicture!!
            }

            if (responseObject!!.businessProfile!!.businessInfo.isPhoneVerified) {
                binding!!.btnVerified.visibility = View.VISIBLE
                binding!!.btnNotVerified.visibility = View.GONE
            } else {
                binding!!.btnVerified.visibility = View.GONE
                binding!!.btnNotVerified.visibility = View.VISIBLE
            }

            if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.countryCode)) {
                binding!!.ccp.setCountryForNameCode(responseObject!!.businessProfile!!.businessInfo.countryISO)
            }


        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

        setCompanyTypeSpinner()
        setIndustrySpinner()
        setBusinessSocial()

    }

    private fun setBusinessSocial() {
        var socialMedia = ""
        if (responseObject!!.businessProfile!!.socialMediaArrayList.size > 0) {
            for (i in responseObject!!.businessProfile!!.socialMediaArrayList.indices) {
                socialMedia = responseObject!!.businessProfile!!.socialMediaArrayList[i].socialName!!
                if (socialMedia.equals(responseObject!!.businessProfile!!.socialMediaArrayList[i].socialName!!, ignoreCase = true)) {
                    when (responseObject!!.businessProfile!!.socialMediaArrayList[i].socialName!!) {
                        Constants.SOCIAL_FB.toLowerCase() -> if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl)) {
                            binding!!.edFaceBook.setText(responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl)
                            binding!!.ivFacebook.setBackgroundResource(R.drawable.ic_socail_fb)
                        } else {
                            binding!!.ivFacebook.setBackgroundResource(R.drawable.ic_social_fb_dissconnect)
                        }
                        Constants.SOCIAL_TW.toLowerCase() -> if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl)) {
                            binding!!.edTwitter.setText(responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl)
                            binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_socail_twitter)
                        } else {
                            binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_social_twitter_dissconnect)
                        }
                        Constants.SOCIAL_LI.toLowerCase() -> if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl)) {
                            binding!!.edLinkedIn.setText(responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl)
                            binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_socail_linkedin)
                        } else {
                            binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_social_linkedin_dissconnected)
                        }
                        Constants.SOCIAL_INSTA.toLowerCase() -> if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl)) {
                            binding!!.edInstagram.setText(responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl)
                            binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram)
                        } else {
                            binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram_dissconnect)
                        }
                        Constants.SOCIAL_FLKR.toLowerCase() -> if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl)) {
                            binding!!.edFlicker.setText(responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl)
                            binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_socail_filker)
                        } else {
                            binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_social_filker_dissconnect)
                        }
                    }
                }
            }
        }
    }

    private fun setCompanyTypeSpinner() {
        spinnerAdapter = AdapterSpinner(HooleyMain.activity, R.layout.sp_gender_group_item, model.companyTypeList!!)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spCompanyType.adapter = spinnerAdapter
        binding!!.spCompanyType.onItemSelectedListener = this
        if (!responseObject!!.businessProfile!!.businessInfo.companyTypeId.equals("0", ignoreCase = true)) {
            for (i in responseObject!!.businessProfile!!.companyTypeList.indices) {
                if (responseObject!!.businessProfile!!.companyTypeList[i].id.equals(responseObject!!.businessProfile!!.businessInfo.companyTypeId, ignoreCase = true)) {
                    binding!!.spCompanyType.setSelection(i)
                }
            }
        }
    }

    private fun setIndustrySpinner() {
        spinnerAdapter = AdapterSpinner(HooleyMain.activity, R.layout.sp_gender_group_item, model.industryList!!)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spIndustry.adapter = spinnerAdapter
        binding!!.spIndustry.onItemSelectedListener = this
        if (!responseObject!!.businessProfile!!.businessInfo.industryId.equals("0", ignoreCase = true)) {
            for (i in responseObject!!.businessProfile!!.industryList.indices) {
                if (responseObject!!.businessProfile!!.industryList[i].id.equals(responseObject!!.businessProfile!!.businessInfo.industryId, ignoreCase = true)) {
                    binding!!.spIndustry.setSelection(i)
                }
            }
        }

    }

    private fun setCountrySpinner(response: CountryStateCitiesModel) {
        if (countryList != null && countryList!!.size > 0)
            countryList!!.clear()
        countryList = response.countryStateCitiesList
        adapterSpinner = AdapterCountries(HooleyMain.activity!!, R.layout.sp_gender_group_item, countryList)
        adapterSpinner!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spCountry.adapter = adapterSpinner
        binding!!.spCountry.onItemSelectedListener = this
        if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.country)) {
            for (loop in countryList!!.indices) {
                if (countryList!![loop].name.equals(responseObject!!.businessProfile!!.businessInfo.country, ignoreCase = true)) {
                    binding!!.spCountry.setSelection(loop)
                    binding!!.tvCountry.visibility = View.GONE
                }
            }
        }
    }

    private fun setStateSpinner(response: CountryStateCitiesModel) {
        if (stateList != null && stateList!!.size > 0)
            stateList!!.clear()
        stateList = response.countryStateCitiesList
        adapterSpinner = AdapterCountries(HooleyMain.activity!!, R.layout.sp_gender_group_item, stateList)
        adapterSpinner!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spState.adapter = adapterSpinner
        binding!!.spState.onItemSelectedListener = this
        if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.state)) {
            for (loop in stateList!!.indices) {
                if (stateList!![loop].name.equals(responseObject!!.businessProfile!!.businessInfo.state, ignoreCase = true)) {
                    binding!!.spState.setSelection(loop)
                    binding!!.tvState.visibility = View.GONE
                }
            }
        }
    }

    private fun setCitySpinner(response: CountryStateCitiesModel) {
        if (citiesList != null && citiesList!!.size > 0)
            citiesList!!.clear()
        citiesList = response.countryStateCitiesList
        adapterSpinner = AdapterCountries(HooleyMain.activity!!, R.layout.sp_gender_group_item, citiesList)
        adapterSpinner!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spCity.adapter = adapterSpinner
        binding!!.spCity.onItemSelectedListener = this
        if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.city)) {
            for (loop in citiesList!!.indices) {
                if (citiesList!![loop].name.equals(responseObject!!.businessProfile!!.businessInfo.city, ignoreCase = true)) {
                    binding!!.tvCity.visibility = View.GONE
                    binding!!.spCity.setSelection(loop)
                }
            }
        }
    }

    private fun VerifyNumberDialog(number: String) {
        isValid = true
        verifyNumberDialog = Dialog(HooleyMain.activity!!)
        verifyNumberDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        verifyNumberDialog!!.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        verifyNumberDialog!!.setContentView(R.layout.dialog_verify_number)
        val btnVerifyCode = verifyNumberDialog!!.findViewById<Button>(R.id.btnVerifyCode)
        val ivClose = verifyNumberDialog!!.findViewById<ImageView>(R.id.ivClose)
        val tvPhoneNumber = verifyNumberDialog!!.findViewById<TextView>(R.id.tvPhoneNumber)
        val tvResendCode = verifyNumberDialog!!.findViewById<TextView>(R.id.tvResendCode)
        val tvTimer = verifyNumberDialog!!.findViewById<TextView>(R.id.tvTimer)
        val rlResendCode = verifyNumberDialog!!.findViewById<RelativeLayout>(R.id.rlResendCode)
        tvPhoneNumber.text = number
        tvResendCode.setOnClickListener { verifyNumber() }
        val pincode = verifyNumberDialog!!.findViewById<PinEntryEditText>(R.id.pinCode)

        ivClose.setOnClickListener { verifyNumberDialog!!.dismiss() }
        btnVerifyCode.setOnClickListener(View.OnClickListener {
            if (Util.isContentNull(pincode.text.toString())) {
                pincode.requestFocus()
                pincode.error = getString(R.string.required)
                return@OnClickListener
            }

            if (TextUtils.isEmpty(messageId)) {
                Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_server_not_responding))
                return@OnClickListener
            }
            if (isValid) {
                verifyPinCode(pincode.text.toString())
                verifyNumberDialog!!.dismiss()
            } else {
                Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_invalid_code))
            }
        })

        val currentDateTimeString = SimpleDateFormat("HH:mm:ss").format(Date())
        val format = SimpleDateFormat("HH:mm:ss")
        var date2: Date? = null
        try {
            date2 = format.parse(currentDateTimeString)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        countDownTimer = object : CountDownTimer((60 * 3 * 1000).toLong(), 100) {
            override fun onTick(l: Long) {
                val seconds = l / 1000
                val currentMinute = seconds / 60
                tvTimer.text = String.format("%02d", seconds / 60) + ":" + String.format("%02d", seconds % 60)
                if (currentMinute == 0L && seconds == 0L) {
                    rlResendCode.visibility = View.VISIBLE
                    isValid = false
                    tvTimer.text = "00:00"
                    if (countDownTimer != null) {
                        countDownTimer!!.cancel()
                    }
                } else {

                }
            }

            override fun onFinish() {
                rlResendCode.visibility = View.VISIBLE
                tvTimer.text = "00:00"
                isValid = false
                if (countDownTimer != null) {
                    countDownTimer!!.cancel()
                }
            }
        }.start()
        verifyNumberDialog!!.show()
    }

    private fun validateInput(): Boolean {
        if (Util.isContentNull(binding!!.edtBusinessPhone.text.toString())) {
            binding!!.edtBusinessPhone.requestFocus()
            binding!!.edtBusinessPhone.error = getString(R.string.ic_add_number)
            return false
        } else if (Util.isContentNull(binding!!.edtBusinessEmail.text.toString())) {
            binding!!.edtBusinessEmail.requestFocus()
            binding!!.edtBusinessEmail.error = getString(R.string.str_error_email)
            return false
        } else if (!Util.isContentNull(binding!!.edtBusinessEmail.text.toString()) && !android.util.Patterns.EMAIL_ADDRESS.matcher(binding!!.edtBusinessEmail.text.toString()).matches()) {
            binding!!.edtBusinessEmail.requestFocus()
            binding!!.edtBusinessEmail.error = getString(R.string.str_invalid_email)
            return false
        } else if (Util.isContentNull(binding!!.edtBusinessName.text.toString())) {
            binding!!.edtBusinessName.requestFocus()
            binding!!.edtBusinessName.error = getString(R.string.required)
            return false
        } else if (Util.isContentNull(binding!!.edtAboutBusiness.text.toString())) {
            binding!!.edtAboutBusiness.requestFocus()
            binding!!.edtBusinessName.error = getString(R.string.abt_required)
            return false
        } else if (TextUtils.isEmpty(businessImageUrl) && mediaUri == null) {
            Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_upload_cover))
            return false
        } else if (!Util.isContentNull(binding!!.edtBusinessWebsite.text.toString()) && !android.util.Patterns.WEB_URL.matcher(binding!!.edtBusinessWebsite.text).matches()) {
            binding!!.edtBusinessWebsite.requestFocus()
            binding!!.edtBusinessWebsite.error = getString(R.string.str_url)
            return false
        } else if (!Util.isContentNull(binding!!.edFaceBook.text.toString()) && !android.util.Patterns.WEB_URL.matcher(binding!!.edFaceBook.text).matches()) {
            binding!!.edFaceBook.requestFocus()
            binding!!.edFaceBook.error = getString(R.string.str_url)
            return false
        } else if (!Util.isContentNull(binding!!.edInstagram.text.toString()) && !android.util.Patterns.WEB_URL.matcher(binding!!.edInstagram.text).matches()) {
            binding!!.edInstagram.requestFocus()
            binding!!.edInstagram.error = getString(R.string.str_url)
            return false
        } else if (!Util.isContentNull(binding!!.edTwitter.text.toString()) && !android.util.Patterns.WEB_URL.matcher(binding!!.edTwitter.text).matches()) {
            binding!!.edTwitter.requestFocus()
            binding!!.edTwitter.error = getString(R.string.str_url)
            return false
        } else if (!Util.isContentNull(binding!!.edFlicker.text.toString()) && !android.util.Patterns.WEB_URL.matcher(binding!!.edFlicker.text).matches()) {
            binding!!.edFlicker.requestFocus()
            binding!!.edFlicker.error = getString(R.string.str_url)
            return false
        } else if (!Util.isContentNull(binding!!.edLinkedIn.text.toString()) && !android.util.Patterns.WEB_URL.matcher(binding!!.edLinkedIn.text).matches()) {
            binding!!.edLinkedIn.requestFocus()
            binding!!.edLinkedIn.error = getString(R.string.str_url)
            return false
        } else return strCity != null
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        when (parent.id) {
            R.id.spCountry -> {
                strCountry = countryList!![position].name
                strCountryId = countryList!![position].id
                getCountries("state", strCountryId, "0", Constants.TYPE_STATE)
            }
            R.id.spState -> {
                strState = stateList!![position].name
                strStateId = stateList!![position].id
                getCountries("city", strCountryId, strStateId, Constants.TYPE_CITY)
            }
            R.id.spCity -> {
                strCity = citiesList!![position].name
                strCityId = citiesList!![position].id
            }

            R.id.spIndustry -> industryPosition = position
            R.id.spCompanyType -> companyTypePosition = position
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnSaveProfile -> if (validateInput())
                saveBusinessProfile()
            R.id.ivAddPhoto -> setMoreCallerBackListener()

            R.id.btnNotVerified -> {
                if (TextUtils.isEmpty(binding!!.edtBusinessPhone.text.toString())) {
                    binding!!.edtBusinessPhone.requestFocus()
                    binding!!.edtBusinessPhone.error = getString(R.string.required)
                    return
                }
                if (TextUtils.isEmpty(cc)) {
                    Util.showToastMessage(HooleyMain.activity!!, getString(R.string.ic_add_cc))
                    return
                }
                verifyNumber()
                VerifyNumberDialog(cc + binding!!.edtBusinessPhone.text.toString())
            }
        }//                Util.hidekeyPad(HooleyMain.activity, view);
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
        inflater!!.inflate(R.menu.gallery_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.save -> {
                if (validateInput()) {
                    saveBusinessProfile()
                    return false
                }
                return super.onOptionsItemSelected(item)
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
        //        HooleyMain.activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);
        Util.hideKeyboard(HooleyMain.activity!!)

    }

    override fun onCountrySelected(country: Country) {
        cc = country.phoneCode
        ccIso = country.iso
    }

    //Web Services

    /**
     * @param area
     * @param countryId
     * @param stateId
     * @param state
     */
    fun getCountries(area: String, countryId: String?, stateId: String?, state: Int) {
        when (state) {
            Constants.TYPE_COUNTRIES -> binding!!.pbCountries.visibility = View.VISIBLE
            Constants.TYPE_STATE -> binding!!.pbState.visibility = View.VISIBLE
            Constants.TYPE_CITY -> binding!!.pbCity.visibility = View.VISIBLE
        }


        service.getCountries(area, countryId!!, stateId!!, object : GenericCallback<CountryStateCitiesModel> {
            override fun success(result: CountryStateCitiesModel) {
                responseCountry = result
                when (state) {
                    Constants.TYPE_COUNTRIES -> {
                        binding!!.pbCountries.visibility = View.INVISIBLE
                        setCountrySpinner(responseCountry)
                    }
                    Constants.TYPE_STATE -> {
                        binding!!.pbState.visibility = View.INVISIBLE
                        binding!!.tvState.visibility = View.GONE
                        binding!!.spState.visibility = View.VISIBLE
                        setStateSpinner(responseCountry)
                    }
                    Constants.TYPE_CITY -> {
                        binding!!.pbCity.visibility = View.INVISIBLE
                        setCitySpinner(responseCountry)
                    }
                }
            }

            override fun failure(message: String) {
                binding!!.pbCountries.visibility = View.INVISIBLE
                try {
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constants.CAMERA_RQ) {
                HooleyMain.activity!!.processCapturedPhoto(object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        mediaUri = HooleyMain.activity!!.compressFile(result!!)
                        binding!!.ivAvatar.setImageBitmap(HooleyMain.activity!!.handleSamplingAndRotationBitmap(result))
                    }

                })
            } else if (requestCode == Constants.IMAGE_PICKER_SELECT) {
                HooleyMain.activity!!.processGalleryPhoto(data!!, object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        mediaUri = HooleyMain.activity!!.compressFile(result!!)
                        binding!!.ivAvatar.setImageBitmap(HooleyMain.activity!!.handleSamplingAndRotationBitmap(result))
                    }

                })
            }
        } else {
            Toast.makeText(HooleyMain.activity, "Action Cancelled", Toast.LENGTH_SHORT).show()
        }
    }


    private fun verifyNumber() {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        HooleyMain.activity!!.showDialog()
        val data = cc + binding!!.edtBusinessPhone.text.toString()

        service.verifyNumber(binding!!.edtBusinessPhone.text.toString(), cc, data, object : GenericCallback<VerifyPhoneNumberModel> {
            override fun success(result: VerifyPhoneNumberModel) {
                HooleyMain.activity!!.removeDialog()
                messageId = result.apiMessageId
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                verifyNumberDialog!!.dismiss()
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    /**
     * @param code
     */
    private fun verifyPinCode(code: String) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        HooleyMain.activity!!.showDialog()
        service.verifyPinCode(messageId!!, code, true, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                binding!!.btnNotVerified.visibility = View.GONE
                binding!!.btnVerified.visibility = View.VISIBLE
                if (countDownTimer != null) {
                    countDownTimer!!.cancel()
                }
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }

    private fun saveBusinessProfile() {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }

        HooleyMain.activity!!.showDialog()
        socailList
        service.updateBusinessProfile(mediaUri, businessImageUrl, binding!!.edtBusinessName.text.toString(), binding!!.edtAboutBusiness.text.toString(),
                cc, ccIso, binding!!.edtBusinessPhone.text.toString(), false, binding!!.edtBusinessEmail.text.toString(), binding!!.edtBusinessWebsite.text.toString(), binding!!.edtContactName.text.toString(),
                model.companyTypeList!![companyTypePosition].id!!, model.industryList!![industryPosition].id!!,
                binding!!.edtBusinessAddress.text.toString(), strCityId!!, strStateId!!, strCountryId!!, binding!!.tvPostalCode.text.toString(), socailList, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                HooleyApp.db.putBoolean(Constants.IS_BUSINESS_PROFILE_CREATED, true)
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)

                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }


    override fun onFocusChange(view: View, b: Boolean) {
        when (view.id) {
            R.id.edFaceBook -> {
                binding!!.ivFacebook.setBackgroundResource(R.drawable.ic_socail_fb)
                if (TextUtils.isEmpty(binding!!.edFlicker.text.toString())) {
                    binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_social_filker_dissconnect)
                }
                if (TextUtils.isEmpty(binding!!.edTwitter.text.toString())) {
                    binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_social_twitter_dissconnect)
                }
                if (TextUtils.isEmpty(binding!!.edLinkedIn.text.toString())) {
                    binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_social_linkedin_dissconnected)
                }
                if (TextUtils.isEmpty(binding!!.edInstagram.text.toString())) {
                    binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram_dissconnect)
                }
            }

            R.id.edTwitter -> {
                binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_socail_twitter)
                if (TextUtils.isEmpty(binding!!.edFlicker.text.toString())) {
                    binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_social_filker_dissconnect)
                }
                if (TextUtils.isEmpty(binding!!.edFaceBook.text.toString())) {
                    binding!!.ivFacebook.setBackgroundResource(R.drawable.ic_social_fb_dissconnect)
                }
                if (TextUtils.isEmpty(binding!!.edLinkedIn.text.toString())) {
                    binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_social_linkedin_dissconnected)
                }
                if (TextUtils.isEmpty(binding!!.edInstagram.text.toString())) {
                    binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram_dissconnect)
                }
            }

            R.id.edFlicker -> {
                binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_socail_filker)
                if (TextUtils.isEmpty(binding!!.edTwitter.text.toString())) {
                    binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_social_twitter_dissconnect)
                }
                if (TextUtils.isEmpty(binding!!.edFaceBook.text.toString())) {
                    binding!!.ivFacebook.setBackgroundResource(R.drawable.ic_social_fb_dissconnect)
                }
                if (TextUtils.isEmpty(binding!!.edLinkedIn.text.toString())) {
                    binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_social_linkedin_dissconnected)
                }
                if (TextUtils.isEmpty(binding!!.edInstagram.text.toString())) {
                    binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram_dissconnect)
                }
            }
            R.id.edInstagram -> {
                binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram)
                if (TextUtils.isEmpty(binding!!.edTwitter.text.toString())) {
                    binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_social_twitter_dissconnect)
                }
                if (TextUtils.isEmpty(binding!!.edFaceBook.text.toString())) {
                    binding!!.ivFacebook.setBackgroundResource(R.drawable.ic_social_fb_dissconnect)
                }
                if (TextUtils.isEmpty(binding!!.edLinkedIn.text.toString())) {
                    binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_social_linkedin_dissconnected)
                }
                if (TextUtils.isEmpty(binding!!.edFlicker.text.toString())) {
                    binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_social_filker_dissconnect)
                }
            }
            R.id.edLinkedIn -> {
                binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_socail_linkedin)
                if (TextUtils.isEmpty(binding!!.edTwitter.text.toString())) {
                    binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_social_twitter_dissconnect)
                }
                if (TextUtils.isEmpty(binding!!.edFaceBook.text.toString())) {
                    binding!!.ivFacebook.setBackgroundResource(R.drawable.ic_social_fb_dissconnect)
                }
                if (TextUtils.isEmpty(binding!!.edInstagram.text.toString())) {
                    binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram_dissconnect)
                }
                if (TextUtils.isEmpty(binding!!.edFlicker.text.toString())) {
                    binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_social_filker_dissconnect)
                }
            }
        }
    }

    companion object {

        private var responseObject: GetUserProfilesModel? = null

        lateinit var instance: EditBusinessProfileFragment

        fun newInstance(response: GetUserProfilesModel): EditBusinessProfileFragment {
            responseObject = response
            instance = EditBusinessProfileFragment()
            return instance
        }
    }
}
