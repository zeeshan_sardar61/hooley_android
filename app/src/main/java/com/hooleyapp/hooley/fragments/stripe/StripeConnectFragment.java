package com.hooleyapp.hooley.fragments.stripe;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.hooleyapp.hooley.R;
import com.hooleyapp.hooley.activites.HooleyMain;
import com.hooleyapp.hooley.databinding.FragmentStripeConnectBinding;
import com.hooleyapp.hooley.fragments.BaseFragment;
import com.hooleyapp.hooley.log.Logger;

/**
 * Created by Zeeshan on 06-Mar-18.
 */

public class StripeConnectFragment extends BaseFragment {

    private static final String PROTECTED_RESOURCE_URL = "https://www.messagemuse.com/";
    public static StripeConnectFragment instance;
    //    private static String connectUrl = "https://connect.stripe.com/express/oauth/authorize?client_id=";
    private static String connectUrl = "https://connect.stripe.com/oauth/authorize?response_type=code&client_id=" + HooleyMain.Companion.getActivity().getString(R.string.stripe_client_id) + "&scope=read_write";
    public Intent intent;
    FragmentStripeConnectBinding binding;

    public static StripeConnectFragment newInstance() {
        instance = new StripeConnectFragment();
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_stripe_connect, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setToolbar();
        setWebView();

    }

    private void setToolbar() {
        HooleyMain.Companion.getActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        HooleyMain.Companion.getActivity().getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
    }

    public void setWebView() {

        binding.wvStripe.getSettings().setJavaScriptEnabled(true);
        binding.wvStripe.getSettings().setLoadWithOverviewMode(true);
        binding.wvStripe.getSettings().setUseWideViewPort(true);
        binding.wvStripe.getSettings().setBuiltInZoomControls(true);
        binding.wvStripe.loadUrl(connectUrl);
        binding.wvStripe.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (url.indexOf("code=") > -1) {
                    Logger.INSTANCE.e("hi" + " / code = " + url.split("=")[1]);
                    intent = new Intent();
                    intent.putExtra("code", url.split("=")[2]);
                    HooleyMain.Companion.getActivity().getSupportFragmentManager().popBackStackImmediate();
                    getTargetFragment().onActivityResult(
                            getTargetRequestCode(),
                            Activity.RESULT_OK,
                            intent
                    );
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (url.contains(PROTECTED_RESOURCE_URL) && !url.contains("code=")) {
                    HooleyMain.Companion.getActivity().getSupportFragmentManager().popBackStackImmediate();
                } else if (!url.contains("code=")) {
                    Logger.INSTANCE.e("killProgressDialog");
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (binding != null)
            binding.unbind();
//        HooleyMain.activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);

    }
}
