package com.hooleyapp.hooley.fragments.invites

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterPastGuestList
import com.hooleyapp.hooley.databinding.FragmentPastGuestListBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.PastGuestListModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.InviteWebService


/**
 * Created by Nauman on 12/29/2017.
 */

class PastGuestListFragment : BaseFragment(), View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, AdapterPastGuestList.IPastGuestListListeners {

    internal lateinit var binding: FragmentPastGuestListBinding
    private lateinit var adapter: AdapterPastGuestList
    lateinit var responseObj: PastGuestListModel
    var service = InviteWebService()
    internal var isSwiped = false
    private val REQUEST_CODE = 963
    private lateinit var pastGuestList: PastGuestListFriendsFragment
    private var clickPosition: Int = -1
    private var isSelectedAll: Boolean = false

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser && isResumed) {
            try {
                getPastGuestList()
            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            }

        } else {

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_past_guest_list, container, false)
        if (userVisibleHint)
            getPastGuestList()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
    }

    private fun setListener() {
        binding.slPastEvents.setOnRefreshListener(this)
        binding.btnSendInvite.setOnClickListener(this)
    }

    private fun setRecyclerView() {
        binding.tvGuestCount.text = "(" + responseObj.guestArrayList.size + ")"
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        binding.rvPastGuestList.layoutManager = manager
        binding.rvPastGuestList.layoutAnimation = animation
        adapter = AdapterPastGuestList(HooleyMain.activity!!, responseObj.guestArrayList)
        adapter.mListener = this
        binding.rvPastGuestList.adapter = adapter
    }

    private fun getPastGuestList() {
        if (!isSwiped && ::binding.isInitialized)
            binding.pbPastGuestList.visibility = View.VISIBLE
        service.getPastGuestList(object : GenericCallback<PastGuestListModel> {
            override fun success(result: PastGuestListModel) {
                binding.btnSendInvite.visibility = View.VISIBLE
                if (isSwiped) {
                    binding.slPastEvents.isRefreshing = false
                    isSwiped = false
                }
                binding.pbPastGuestList.visibility = View.GONE
                binding.tvNoData.visibility = View.GONE
                responseObj = result
                setRecyclerView()
            }

            override fun failure(message: String) {
                if (isSwiped) {
                    binding.slPastEvents.isRefreshing = false
                    isSwiped = false
                }
                binding.pbPastGuestList.visibility = View.GONE
                try {
                    binding.tvNoData.visibility = View.VISIBLE
                    binding.tvNoData.text = message
                    binding.tvGuestCount.visibility = View.GONE
                    binding.tvTitle.visibility = View.GONE
                    binding.btnSendInvite.visibility = View.GONE
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnSendInvite -> if (validInput())
                invitePastGuestList()
            else
                Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_selection))
        }
    }

    private fun validInput(): Boolean {
        for (i in responseObj.guestArrayList.indices) {
            if (responseObj.guestArrayList[i].isChecked)
                return true
        }
        return false
    }

    private fun invitePastGuestList() {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            HooleyMain.activity!!.showSuccessDialog(Constants.NO_NETWORK)
            return
        }
        HooleyMain.activity!!.showDialog()
        service.invitePastGuestList(InviteFragment.createdEventId, InviteFragment.createdEventName, responseObj.guestArrayList, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                HooleyMain.activity!!.showSuccessDialog(getString(R.string.str_send_invite))
                getPastGuestList()
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                    HooleyMain.activity!!.removeDialog()
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }
            }
        })
    }


    override fun onClickItem(position: Int) {
        clickPosition = position
        pastGuestList = PastGuestListFriendsFragment(responseObj.guestArrayList[position], responseObj.guestArrayList[position].isSelectedAll)
        pastGuestList.setTargetFragment(InviteFragment.instance, REQUEST_CODE)
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, pastGuestList, "selectGuestsFragment")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            val guestIds = data!!.getIntegerArrayListExtra("guestListIds")
            addingGuests(guestIds)
//            HooleyMain.activity!!.showSuccessDialog(data.toString())
        } else if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_CANCELED) {
            if (adapter != null)
                adapter.notifyDataSetChanged()
        }
    }

    private fun addingGuests(guestIds: ArrayList<Int>?) {
        if (clickPosition != -1) {
            responseObj.guestArrayList[clickPosition].isChecked = true
            responseObj.guestArrayList[clickPosition].userIdList = ArrayList()
            responseObj.guestArrayList[clickPosition].userIdList.addAll(guestIds!!)
            val parts = responseObj.guestArrayList[clickPosition].invitGuestCount.split(" ")
            if (Integer.parseInt(parts[0]) == guestIds.size) {
                responseObj.guestArrayList[clickPosition].isSelectedAll = true
                responseObj.guestArrayList[clickPosition].selection = 1
            } else {
                responseObj.guestArrayList[clickPosition].isSelectedAll = false
                responseObj.guestArrayList[clickPosition].selection = 2
            }
            adapter.notifyDataSetChanged()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding.unbind()
    }

    override fun onRefresh() {
        if (binding.slPastEvents != null) {
            binding.slPastEvents.isRefreshing = true
        }
        isSwiped = true
        getPastGuestList()
    }

    companion object {
        lateinit var instance: PastGuestListFragment

        fun newInstance(): PastGuestListFragment {
            instance = PastGuestListFragment()
            return instance
        }
    }

}
