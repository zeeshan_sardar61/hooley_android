package com.hooleyapp.hooley.fragments.event

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.Transformation
import android.widget.AdapterView
import android.widget.RadioGroup
import android.widget.TextView
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.*
import com.hooleyapp.hooley.databinding.FragmentCreateEventBinding
import com.hooleyapp.hooley.db.DbManager
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.friends.SelectFriendFragment
import com.hooleyapp.hooley.fragments.nearby.DashBoardFragment
import com.hooleyapp.hooley.fragments.nearby.GeofenceFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.ICallBackUri
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.*
import com.hooleyapp.hooley.model.staticData.Category
import com.hooleyapp.hooley.model.staticData.StaticDataModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.EventWebService
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 * Use the [CreateEventFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@Suppress("SENSELESS_COMPARISON")
class CreateEventFragment : BaseFragment(), AdapterView.OnItemClickListener, View.OnClickListener, RadioGroup.OnCheckedChangeListener, View.OnTouchListener, TextWatcher, View.OnFocusChangeListener, AdapterAddCoHost.onListItemRemovedListener {

    override fun onListSizeChange() {
        binding.btnAddCoHost.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_add)
        binding.btnAddCoHost.isEnabled = true
    }

    private var oldHashtagLenth: Int = 0
    lateinit var responseObject: CreateEventModel
    var hostList: ArrayList<FriendsTable> = ArrayList()
    var coHostList: ArrayList<FriendsTable> = ArrayList()
    var optionalCatList = ArrayList<String>()
    var geofenceList = ArrayList<GeoFenceModel>()
    var isPaid = false
    var isPublic = true
    var eventWallpapers: ArrayList<ItemCreateEventWallpaper> = ArrayList()
    lateinit var binding: FragmentCreateEventBinding
    internal var gson = Gson()
    internal var handler = Handler()
    internal var startDate = ""
    internal var endDate = ""
    private var adapterCategory: AdapterEventCategory? = null
    private var adapterAge: AdapterEventAges? = null
    private var list: ArrayList<Category> = ArrayList()
    private var customDialog: CustomDateTimePicker? = null
    private var friendList: MutableList<FriendsTable>? = null
    private var galleryFragment: HooleyGalleryFragment? = null
    private val REQUEST_CODE = 963
    private val REQUEST_CODE_GEOFENCE = 964
    private val REQUEST_CODE_ADD_HOST = 965
    private val REQUEST_CODE_ADD_CO_HOST = 966
    private var adapterOptCategory: AdapterOptionCategory? = null
    private var geoFenceFragment: GeofenceFragment? = null
    private var listLatLong: ArrayList<LatLng>? = null
    private var centerLatLngObj: LatLng? = null
    private var previousSelectedDate: Date? = null
    private var adapter: AdapterWallpaperPager? = null
    private var isEdit = false
    private var service: FriendWebService? = null
    private val webService = EventWebService()
    private var imagesArray: ArrayList<String>? = null
    private var selectFriendFragment: SelectFriendFragment? = null
    var model = StaticDataModel()
    private var moreDialogFragment: MoreDialogeFragment? = null
    var listAge: ArrayList<PersonalProfileModel.myObject> = ArrayList()
    var friendListIds: ArrayList<FriendsTable> = ArrayList()
    private var eventHashes = ArrayList<String>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_event, container, false)
        (HooleyMain.activity as HooleyMain).removeBubbleView()
        setHasOptionsMenu(true)
        when {
            eventType.equals("1", true) -> {
                binding.rlAgeContainer.visibility = VISIBLE
                isPaid = false
                (HooleyMain.activity!! as HooleyMain).setToolbarTitle("Create Free Event")

            }
            eventType.equals("2", true) -> {
                binding.rlAgeContainer.visibility = VISIBLE
                isPaid = true
                (HooleyMain.activity!! as HooleyMain).setToolbarTitle("Create Paid Event")
            }
            else -> {
                eventType = "3"
                isPublic = true
                (HooleyMain.activity!! as HooleyMain).setToolbarTitle("Create Charity Event")
                binding.evisibility.visibility = GONE
                binding.rgEventVisibility.visibility = GONE
                for (i in listAge.indices) {
                    if (listAge[i].name.equals("All Ages")) {
                        ageId = listAge[i].id!!
                        selectedAge = listAge[i].name!!
                        listAge[i].isActive = true
                        break
                    }
                }
            }
        }

        setListener()
        setMoreCallerBackListener()
        getEventData()
        getFriendList()
        setOptEvent()
        if (eventWallpapers.size == 0)
            setupViewPager()
        return binding.root
    }


    fun setToolbar() {
        when {
            eventType.equals("1", true) -> {
                (HooleyMain.activity!! as HooleyMain).setToolbarTitle("Create Free Event")
            }
            eventType.equals("2", true) -> {
                (HooleyMain.activity!! as HooleyMain).setToolbarTitle("Create Paid Event")
            }
            else -> {
                (HooleyMain.activity!! as HooleyMain).setToolbarTitle("Create Charity Event")
            }
        }
    }

    private fun setListener() {
        binding.llStartDate.setOnClickListener(this)
        binding.ivUploadPic.setOnClickListener(this)
        binding.llEndDate.setOnClickListener(this)
        binding.ivEventCover.setOnClickListener(this)
        binding.ivDelete.setOnClickListener(this)
        binding.ivEdit.setOnClickListener(this)
        binding.btnEventPreview.setOnClickListener(this)
        binding.ivCollapseEvent.setOnClickListener(this)
        binding.ivExpandEvent.setOnClickListener(this)
        binding.ivCollapseAge.setOnClickListener(this)
        binding.ivExpandAge.setOnClickListener(this)
        binding.btnAddCoHost.setOnClickListener(this)
        binding.rlLocation.setOnClickListener(this)
        binding.btnAddCategory.setOnClickListener(this)
        binding.rgEventVisibility.setOnCheckedChangeListener(this)
        binding.btnSaveUnPublish.setOnClickListener(this)
        binding.rbPublic.setOnTouchListener(this)
        binding.rbPrivate.setOnTouchListener(this)
        binding.btnAddCoHost.setOnTouchListener(this)
        binding.btnSaveUnPublish.setOnTouchListener(this)
        binding.btnAddCategory.setOnTouchListener(this)
        binding.edEventHashes.addTextChangedListener(this)
        binding.edEventHashes.onFocusChangeListener = this
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
    }

    override fun afterTextChanged(p0: Editable?) {
        if (binding.edEventHashes.text!!.isNotEmpty()) {

            val lastChar = binding.edEventHashes.text.toString().substring(binding.edEventHashes.text!!.length - 1)
            if (lastChar == " ") {

                if (oldHashtagLenth < binding.edEventHashes.text.toString().length) {
                    binding.edEventHashes.text = Editable.Factory.getInstance().newEditable(binding.edEventHashes.text.toString() + "#")
                    binding.edEventHashes.setSelection(binding.edEventHashes.text.toString().length)
                }

            }
        }
    }

    override fun beforeTextChanged(sequence: CharSequence?, start: Int, count: Int, after: Int) {
        if (binding.edEventHashes.hasFocus()) {
            oldHashtagLenth = binding.edEventHashes.text!!.length
        }
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        if (binding.edEventHashes.hasFocus()) {
            if (binding.edEventHashes.text!!.isEmpty()) {
                binding.edEventHashes.text = Editable.Factory.getInstance().newEditable(binding.edEventHashes.text.toString() + "#")
                binding.edEventHashes.setSelection(binding.edEventHashes.text.toString().length)
            }
        }

    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        when (v!!.id) {
            R.id.edEventHashes ->
                if (hasFocus) {
                    if (TextUtils.isEmpty(binding.edEventHashes.text)) {
                        binding.edEventHashes.setText("#")
                    }
                } else {
                    if (binding.edEventHashes.text.toString() == "#") {
                        binding.edEventHashes.setText("")
                    }
                }
        }

    }

    fun showCustomDialog(view: TextView, isStartDate: Boolean) {
        customDialog = CustomDateTimePicker(HooleyMain.activity!!,
                object : CustomDateTimePicker.ICustomDateTimeListener {
                    override fun onSet(dialog: Dialog, calendarSelected: Calendar,
                                       dateSelected: Date, year: Int, monthFullName: String,
                                       monthShortName: String, monthNumber: Int, date: Int,
                                       weekDayFullName: String, weekDayShortName: String,
                                       hour24: Int, hour12: Int, min: Int, sec: Int,
                                       AM_PM: String) {
                        if (view.id == binding.tvEventEndDate.id) {
                            if (previousSelectedDate != null) {
                                if (dateSelected.before(previousSelectedDate))
                                    if (TimeUnit.MILLISECONDS.toHours(dateSelected.time - previousSelectedDate!!.time) < 1) {
                                        Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_invalid_select_date))
                                        return
                                    }
                                if (dateSelected.before(Calendar.getInstance().time)) {
                                    Util.showToastMessage(HooleyMain.activity!!, "Date should be after start date")
                                    return
                                }
                            } else {
                                Util.showToastMessage(HooleyMain.activity!!, "Please select start date")
                                return
                            }
                        } else {
                            if (dateSelected.before(Calendar.getInstance().time)) {
                                Util.showToastMessage(HooleyMain.activity!!, "Date should be before end date")
                                return
                            }
                            previousSelectedDate = dateSelected
                        }
                        view.text = ""
                        if (isStartDate) {
                            startDate = (String.format("%04d-%02d-%02d", year, monthNumber + 1, calendarSelected.get(Calendar.DAY_OF_MONTH))
                                    + " " + String.format("%02d:%02d", hour12, min) +
                                    " " + AM_PM)
                        } else {
                            endDate = (String.format("%04d-%02d-%02d", year, monthNumber + 1, calendarSelected.get(Calendar.DAY_OF_MONTH))
                                    + " " + String.format("%02d:%02d", hour12, min) +
                                    " " + AM_PM)
                        }
                        view.text = DateUtils.formatDate(String.format("%04d-%02d-%02d", year, monthNumber + 1, calendarSelected.get(Calendar.DAY_OF_MONTH))
                                + " " + String.format("%02d:%02d", hour12, min) +
                                " " + AM_PM, false)

                    }

                    override fun onCancel() {

                    }
                })

        customDialog!!.set24HourFormat(false)
        if (TextUtils.isEmpty(view.text.toString())) {
            var calendar: Calendar = Calendar.getInstance()
            customDialog!!.setMinDate(calendar.timeInMillis)
            customDialog!!.setDate(calendar)
        } else {
            if (isStartDate) {
                customDialog!!.setDate(DateUtils.stringToDate(DateUtils.SendDateToDate(startDate)))
            } else {
                customDialog!!.setDate(DateUtils.stringToDate(DateUtils.SendDateToDate(endDate)))
            }
        }
        customDialog!!.showDialog()
    }

    private fun setMoreCallerBackListener() {
        val arrayList = java.util.ArrayList<String>()
        arrayList.add("Camera")
        arrayList.add("Photo Library")
        arrayList.add("Wallpaper")
        moreDialogFragment = MoreDialogeFragment.Builder(activity!!).setTitle("Select").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, _ ->
            when (position) {
                0 -> {
                    moreDialogFragment!!.dismiss()
                    HooleyMain.activity!!.startCamera(Constants.CAMERA_RQ)
                }
                1 -> {
                    moreDialogFragment!!.dismiss()
                    HooleyMain.activity!!.showGalleryImage(Constants.IMAGE_PICKER_SELECT)
                }
                2 -> {
                    moreDialogFragment!!.dismiss()
                    galleryFragment = HooleyGalleryFragment.newInstance(isEdit)
                    galleryFragment!!.setTargetFragment(this@CreateEventFragment, REQUEST_CODE)
                    (HooleyMain.activity as ActivityBase).addFragment(R.id.container, galleryFragment!!, "HooleyGalleryFragment")
                }
            }
        }.create()

    }

    private fun getEventData() {
        model = gson.fromJson(HooleyApp.db.getString(Constants.STATIC_MODEL), StaticDataModel::class.java)
        if (list == null)
            list = ArrayList()
        list.addAll(model.categories!!)
        list.sortBy { it.name }
        setGridEvent()
        setAgeGridEvent()
    }

    private fun getFriendList() {
        if (friendList == null)
            friendList = ArrayList()
        if (friendList!!.size > 0)
            friendList!!.clear()
        friendList = DbManager.getInstance().friendList

        if (friendList!!.size > 0) {
            getFriends(false)
        } else {
            getFriends(false)
        }
    }

    private fun setGridEvent() {
        adapterCategory = AdapterEventCategory(HooleyMain.activity!!, list)
        binding.gvEventType.adapter = adapterCategory
        binding.gvEventType.onItemClickListener = this
    }

    fun getAgeList(): ArrayList<PersonalProfileModel.myObject> {
        listAge.addAll(model.minimumAgeList!!)
        if (eventType == "3") {
            listAge[3].isActive = true
        } else {
            listAge[0].isActive = true
        }
        return listAge
    }

    private fun setAgeGridEvent() {
        adapterAge = AdapterEventAges(HooleyMain.activity!!, getAgeList())
        binding.gvAge.adapter = adapterAge
    }

    private fun setOptEvent() {
        adapterOptCategory = AdapterOptionCategory(HooleyMain.activity!!, optionalCatList)
        binding.gvEventOptCat.adapter = adapterOptCategory
        binding.gvEventOptCat.onItemClickListener = this
    }

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        list[position].isActive = !list[position].isActive
        adapterCategory!!.notifyDataSetChanged()
    }

    fun getHashes(): ArrayList<String> {
        if (binding.edEventHashes.text.toString() != "#" && !binding.edEventHashes.text.toString().isEmpty()) {
            val splited = binding.edEventHashes.text.toString().split(" ")

            for (i in 0 until splited.size) {
                eventHashes.add(splited[i].substring(1))
            }
        }
        return eventHashes
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.llStartDate -> showCustomDialog(binding.tvEventStartDate, true)
            R.id.llEndDate -> showCustomDialog(binding.tvEventEndDate, false)
            R.id.ivEventCover -> {
                isEdit = false
                if (!moreDialogFragment!!.isAdded)
                    moreDialogFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
            }
            R.id.btnSaveUnPublish -> if (validateInput()) {
                createEvent()
            }
            R.id.rlLocation -> if (validateInput()) {
                geoFenceFragment = GeofenceFragment.newInstance(isPaid)
                geoFenceFragment!!.setTargetFragment(this@CreateEventFragment, REQUEST_CODE_GEOFENCE)
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, geoFenceFragment!!, "GeofenceFragment")
            }
            R.id.btnEventPreview -> {
                if (binding.tvLocationName.text != "Select Location") {
                    HooleyMain.activity!!.hideKeyboard()
                    binding.root.clearAllFocus()
                    HooleyApp.db.putBoolean(Constants.IS_PUBLISH_EVENT, false)
                    (HooleyMain.activity as ActivityBase).addFragment(R.id.container, PreviewEventFragment.newInstance(eventWallpapers,
                            binding.edEventName.text.toString(),
                            eventType,
                            binding.edEventDetails.text.toString(),
                            getHashes(),
                            binding.edEventHashes.text.toString(),
                            startDate,
                            endDate,
                            isPublic,
                            true,
                            optionalCatList,
                            coHostList,
                            list,
                            geofenceList,
                            ageId,
                            selectedAge,
                            "false"), "PreviewEventFragment")
                }
            }
            R.id.ivCollapseEvent -> {
                collapse(binding.rlSelectEvent)
                binding.ivCollapseEvent.visibility = View.GONE
                binding.ivExpandEvent.visibility = View.VISIBLE
            }
            R.id.ivExpandEvent -> {
                expand(binding.rlSelectEvent)
                binding.ivCollapseEvent.visibility = View.VISIBLE
                binding.ivExpandEvent.visibility = View.GONE
            }
            R.id.ivCollapseAge -> {
                collapseAge(binding.rlSelectAge)
                binding.ivCollapseAge.visibility = View.GONE
                binding.ivExpandAge.visibility = View.VISIBLE
            }
            R.id.ivExpandAge -> {
                expandAge(binding.rlSelectAge)
                binding.ivCollapseAge.visibility = View.VISIBLE
                binding.ivExpandAge.visibility = View.GONE
            }
            R.id.btnAddCoHost -> {
                HooleyApp.db.putString(Constants.CALL_TYPE, "ADD_CO_HOST")
                if (friendList == null) {
                    Util.showToastMessage(HooleyMain.activity!!, getString(R.string.no_friends))
                    return
                }
                if (friendList != null && friendList!!.size == 0) {
                    Util.showToastMessage(HooleyMain.activity!!, getString(R.string.no_friends))
                    return
                }
                if (coHostList.size == 3) {
                    Util.showToastMessage(HooleyMain.activity!!, getString(R.string.up_to_3))
                    return
                }
                Util.hideKeyboard(HooleyMain.activity!!)
                selectFriendFragment = SelectFriendFragment.newInstance(coHostList)
                selectFriendFragment!!.setTargetFragment(this@CreateEventFragment, REQUEST_CODE_ADD_CO_HOST)
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, selectFriendFragment!!, "selectFriendFragment")
            }
            R.id.btnAddCategory -> {
                if (TextUtils.isEmpty(binding.edtEventCategory.text.toString())) {
                    return
                }
                for (loop in optionalCatList.indices) {
                    if (binding.edtEventCategory.text.toString().equals(optionalCatList[loop], ignoreCase = true)) {
                        return
                    }
                }
                optionalCatList.add(binding.edtEventCategory.text.toString())
                adapterOptCategory!!.notifyDataSetChanged()
                binding.edtEventCategory.setText("")
            }
            R.id.ivEdit -> {
                isEdit = true
                if (!moreDialogFragment!!.isAdded)
                    moreDialogFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
            }

            R.id.ivUploadPic -> {
                isEdit = false
                if (!moreDialogFragment!!.isAdded)
                    moreDialogFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
            }
            R.id.ivDelete -> removeView(binding.vpWallPaper.currentItem)
        }

    }

    private fun createEvent() {
        HooleyMain.activity!!.showDialog()
        webService.createEvent(
                eventWallpapers,
                binding.edEventName.text.toString(),
                eventType,
                binding.edEventDetails.text.toString(),
                getHashes(),
                startDate,
                endDate,
                isPublic,
                false,
                optionalCatList,
                coHostList,
                list,
                geofenceList,
                ageId,
                "false"
                , object : IWebServiceCallback<SaveEventModel> {
            override fun success(result: SaveEventModel) {
                HooleyMain.activity!!.removeDialog()
                if (result.success) {
                    Logger.ex("publishEvent:$result")
                    HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                    (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, DashBoardFragment(), null)

                } else {
                    Util.showToastMessage(HooleyMain.activity!!, result.exception)
                }
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    private fun validateInput(): Boolean {
        if (Util.isContentNull(binding.edEventName.text.toString())) {
            binding.edEventName.requestFocus()
            HooleyMain.activity!!.showSuccessDialog(getString(R.string.str_name_missing))
            return false
        } else if (Util.isContentNull(binding.edEventDetails.text.toString())) {
            binding.edEventDetails.requestFocus()
            HooleyMain.activity!!.showSuccessDialog(getString(R.string.str_detail_missing))
            return false
        } else if (Util.isContentNull(binding.edEventHashes.text.toString()) || binding.edEventHashes.text!!.length < 2) {
            binding.edEventHashes.requestFocus()
            HooleyMain.activity!!.showSuccessDialog(getString(R.string.str_hash_missing))
            return false
        } else if (Util.isContentNull(binding.tvEventStartDate.text.toString())) {
            binding.tvEventStartDate.requestFocus()
            HooleyMain.activity!!.showSuccessDialog(getString(R.string.str_start_date_missing))
            return false
        } else if (Util.isContentNull(binding.tvEventEndDate.text.toString())) {
            binding.tvEventEndDate.requestFocus()
            HooleyMain.activity!!.showSuccessDialog(getString(R.string.str_end_date_missing))
            return false
        } else if (eventWallpapers.size == 0) {
            Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_upload_cover))
            return false
        } else if (!Util.checkIfName(binding.edEventName.text.toString())) {
            Util.showToastMessage(HooleyMain.activity!!, getString(R.string.invalid_event_name))
            return false
        } else if (!agesSelected()) {
            Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_select_age))
            if (binding.rlSelectAge.visibility != View.VISIBLE)
                binding.ivExpandAge.performClick()
            return false

        } else {
            for (i in list.indices) {
                if (list[i].isActive) {
                    return true
                }
            }
            Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_select_cat))
            if (binding.rlSelectEvent.visibility != View.VISIBLE)
                binding.ivExpandEvent.performClick()
            return false
        }
    }

    private var ageId: String = ""
    private var selectedAge: String = ""

    private fun agesSelected(): Boolean {
        for (i in listAge.indices) {
            if (listAge[i].isActive) {
                ageId = listAge[i].id!!
                selectedAge = listAge[i].name!!
                return true
            }
        }
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                Constants.CAMERA_RQ -> HooleyMain.activity!!.processCapturedPhoto(object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        //                        mediaArraylist.add(UpdatePostMediaItem("", result, Constants.TYPE_URI, false))
                        Log.e("Image uti", "" + result)
                        binding.rlCover.visibility = View.GONE
                        if (eventWallpapers.size < 4) {
                            addWallpaper(ItemCreateEventWallpaper("", HooleyMain.activity!!.compressFile(result!!), Constants.TYPE_URI))
                        }
                    }
                })
                Constants.IMAGE_PICKER_SELECT -> HooleyMain.activity!!.processGalleryPhoto(data!!, object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        //                        mediaArraylist.add(UpdatePostMediaItem("", result, Constants.TYPE_URI, false))
                        Log.e("Image uti", "" + result)
                        binding.rlCover.visibility = View.GONE
                        if (eventWallpapers.size < 4) {
                            val uru: Uri = HooleyMain.activity!!.compressFile(result!!)
                            if (uru != null) {
                                addWallpaper(ItemCreateEventWallpaper("", uru, Constants.TYPE_URI))
                            } else {
                                Util.showToastMessage(HooleyMain.activity!!, "Image is not valid")
                            }
                        }
                    }
                })
                REQUEST_CODE -> {
                    val isUri = data!!.getBooleanExtra("isUri", false)
                    if (isUri) {
                        //                uploadImage((Uri) data.getParcelableExtra("Image"));
                    } else {
                        binding.rlCover.visibility = View.GONE
                        imagesArray = data.getStringArrayListExtra("Image")
                        if (imagesArray != null && imagesArray!!.size > 0) {
                            for (i in imagesArray!!.indices) {
                                addWallpaper(ItemCreateEventWallpaper(imagesArray!![i], null, Constants.TYPE_URL))
                            }
                        }
                    }
                }
                REQUEST_CODE_GEOFENCE -> {
                    val isCircle = data!!.getBooleanExtra("isCircle", false)
                    if (isCircle) {
                        if (geofenceList.size > 0)
                            geofenceList.clear()
                        val model = GeoFenceModel()
                        model.placeLat = data.getDoubleExtra("Lat", 0.0)
                        model.placeLong = data.getDoubleExtra("Long", 0.0)
                        model.raduis = data.getFloatExtra("radius", 30f).toString()
                        model.address = (HooleyMain.activity as ActivityBase).getCompleteAddressString(data.getDoubleExtra("Lat", 0.0), data.getDoubleExtra("Long", 0.0))
                        geofenceList.add(model)
                        binding.tvLocationName.text = model.address
                    } else {
                        listLatLong = data.getParcelableArrayListExtra("mLatlngs")
                        centerLatLngObj = (HooleyMain.activity as ActivityBase).computeCentroid(listLatLong!!)
                        if (geofenceList.size > 0)
                            geofenceList.clear()
                        for (loop in listLatLong!!.indices) {
                            val model = GeoFenceModel()
                            model.placeLat = listLatLong!![loop].latitude
                            model.placeLong = listLatLong!![loop].longitude
                            model.raduis = (30 / 1609.34).toString()
                            model.address = data.getStringExtra("address")
                            geofenceList.add(model)
                            binding.tvLocationName.text = model.address
                        }
                    }
                    binding.btnEventPreview.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_signup)

                }
                REQUEST_CODE_ADD_CO_HOST -> {
                    coHostList = data!!.getParcelableArrayListExtra("friendListIds")
                    if (coHostList.size == 3) {
                        binding.btnAddCoHost.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_disable_btn)
                        binding.btnAddCoHost.isEnabled = false
                    }
                    addingCoHost(coHostList)
                }
            }
        } else {
            Util.showToastMessage(HooleyMain.activity!!, "Action Cancelled")
        }

    }

    private fun addingCoHost(coHosts: ArrayList<FriendsTable>) {
//        binding.llCoHost.removeAllViews()
//        for (j in coHosts.indices) {
//            addCoHostChild(j)
//        }
        var coHostAdapter = AdapterAddCoHost(HooleyMain.activity!!, coHosts)
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding.rvCoHost.layoutAnimation = animation
        binding.rvCoHost.layoutManager = manager
        coHostAdapter.mListener = this
        binding.rvCoHost.adapter = coHostAdapter
    }

    private fun setupViewPager() {
        adapter = AdapterWallpaperPager()
        binding.vpWallPaper.adapter = adapter
        binding.tabEventWallpaper.setupWithViewPager(binding.vpWallPaper, true)
    }

    fun addView(item: ItemCreateEventWallpaper) {
        val pageIndex = adapter!!.addView(item)
        adapter!!.notifyDataSetChanged()
        binding.vpWallPaper.setCurrentItem(pageIndex, true)
        if (eventWallpapers.size == 4) {
            binding.ivUploadPic.visibility = View.GONE
        }
    }

    fun editView(position: Int, item: ItemCreateEventWallpaper) {
        val pageIndex = adapter!!.editView(position, item)
        adapter!!.notifyDataSetChanged()
        binding.vpWallPaper.setCurrentItem(pageIndex, true)
    }

    fun removeView(position: Int) {

        var pageIndex = adapter!!.removeView(binding.vpWallPaper, position)
        eventWallpapers.removeAt(pageIndex)
        adapter!!.notifyDataSetChanged()
        if (pageIndex == adapter!!.count)
            pageIndex--
        binding.vpWallPaper.currentItem = pageIndex
        if (eventWallpapers.size in 1..3) {
            binding.ivUploadPic.visibility = View.VISIBLE
            binding.ivDelete.visibility = View.VISIBLE
            binding.ivEdit.visibility = View.VISIBLE
        } else if (eventWallpapers.size == 0) {
            binding.ivEdit.visibility = View.GONE
            binding.ivDelete.visibility = View.GONE
            binding.ivUploadPic.visibility = View.GONE
            binding.ivEventCover.visibility = View.VISIBLE
            binding.rlCover.visibility = View.VISIBLE
        }

    }

    private fun addWallpaper(imageUrl: ItemCreateEventWallpaper) {
        if (isEdit) {
            eventWallpapers[binding.vpWallPaper.currentItem] = imageUrl
            editView(binding.vpWallPaper.currentItem, imageUrl)
        } else {
            if (eventWallpapers.size < 4) {
                eventWallpapers.add(imageUrl)
                addView(imageUrl)
            } else {
                binding.ivUploadPic.visibility = View.GONE
            }
        }
        if (eventWallpapers.size in 1..3) {
            binding.ivEdit.visibility = View.VISIBLE
            binding.ivDelete.visibility = View.VISIBLE
            binding.ivUploadPic.visibility = View.VISIBLE
            binding.ivEventCover.visibility = View.GONE
        }
    }

    private fun getFriends(showDialog: Boolean) {
        service = FriendWebService()
        service!!.getMyFriendsList(object : GenericCallback<FriendsModel> {
            override fun success(result: FriendsModel) {
                if (showDialog) {
                    if (friendList == null)
                        friendList = ArrayList()
                    friendList!!.addAll(result.friendsList)

                } else {
                    if (friendList!!.size != result.friendsList.size) {
                        friendList!!.clear()
                        friendList!!.addAll(result.friendsList)
                        DbManager.getInstance().friendList = friendList
                        val friendsTable = FriendsTable()
                        friendsTable.userId = Integer.parseInt(HooleyApp.db.getString(Constants.USER_ID))
                        friendsTable.fullName = HooleyApp.db.getString(Constants.FULL_NAME)
                        friendsTable.isChecked = false
                        friendsTable.profilePic = HooleyApp.db.getString(Constants.USER_AVATAR)
                        friendList!!.add(0, friendsTable)
                        hostList.add(friendList!![0])
                        friendList!![0].isChecked = false
                    } else {
                        val friendsTable = FriendsTable()
                        friendsTable.userId = Integer.parseInt(HooleyApp.db.getString(Constants.USER_ID))
                        friendsTable.fullName = HooleyApp.db.getString(Constants.FULL_NAME)
                        friendsTable.isChecked = false
                        friendsTable.profilePic = HooleyApp.db.getString(Constants.USER_AVATAR)
                        friendList!!.add(0, friendsTable)
                        hostList.add(friendList!![0])
                        friendList!![0].isChecked = true
                    }

                }
            }

            override fun failure(message: String) {
                try {
                    val friendsTable = FriendsTable()
                    friendsTable.userId = Integer.parseInt(HooleyApp.db.getString(Constants.USER_ID))
                    friendsTable.fullName = HooleyApp.db.getString(Constants.FULL_NAME)
                    friendsTable.isChecked = true
                    friendsTable.profilePic = HooleyApp.db.getString(Constants.USER_AVATAR)
                    friendList!!.add(0, friendsTable)
                    hostList.add(friendList!![0])
                    friendList!![0].isChecked = true
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding.unbind()
    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        when (group.id) {
            R.id.rgEventVisibility -> when (checkedId) {
                R.id.rbPublic -> {
                    isPublic = true
                }
                R.id.rbPrivate -> {
                    isPublic = false
                }
            }

        }
    }


    fun expand(v: View) {
        v.measure(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        val targetHeight = v.measuredHeight

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.layoutParams.height = 1
        v.visibility = View.VISIBLE
        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                v.layoutParams.height = if (interpolatedTime == 1f)
                    WindowManager.LayoutParams.WRAP_CONTENT
                else
                    (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // 1dp/ms
        a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
        binding.tvEventCategory.visibility = View.GONE
    }

    fun collapse(v: View) {
        val initialHeight = v.measuredHeight

        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // 1dp/ms
        a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
        setEventCategoryLabel()
    }

    private fun expandAge(v: View) {
        v.measure(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        val targetHeight = v.measuredHeight

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.layoutParams.height = 1
        v.visibility = View.VISIBLE
        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                v.layoutParams.height = if (interpolatedTime == 1f)
                    WindowManager.LayoutParams.WRAP_CONTENT
                else
                    (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // 1dp/ms
        a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
        binding.tvMinimumAge.visibility = View.GONE
        binding.viewLine.visibility = VISIBLE

    }

    private fun collapseAge(v: View) {
        val initialHeight = v.measuredHeight

        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // 1dp/ms
        a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
        setEventAgeLabel()
        binding.viewLine.visibility = GONE
    }


    private fun setEventAgeLabel() {
        var categoryName: String? = null
        for (i in listAge.indices) {
            if (listAge[i].isActive) {
                if (categoryName == null) {
                    categoryName = listAge[i].name
                }
            }
        }
        binding.tvMinimumAge.text = categoryName
        if (categoryName != null)
            binding.tvMinimumAge.visibility = View.VISIBLE
    }


    private fun setEventCategoryLabel() {
        var categoryName: String? = null
        for (i in list.indices) {
            if (list[i].isActive) {
                categoryName = if (categoryName == null) {
                    list[i].name
                } else {
                    categoryName + ", " + list[i].name
                }
            }
        }
        binding.tvEventCategory.text = categoryName
        if (categoryName != null)
            binding.tvEventCategory.visibility = View.VISIBLE
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_UP) {
            Util.hideKeyboard(HooleyMain.activity)
        }
        return false
    }

    companion object {

        private var instance: CreateEventFragment? = null
        var eventType: String = ""
        fun newInstance(event: String): CreateEventFragment {
            instance = CreateEventFragment()
            this.eventType = event
            return instance!!
        }
    }
}
