package com.hooleyapp.hooley.fragments.friends

import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.RadioGroup
import com.google.gson.Gson
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterMoreMediaDialoge
import com.hooleyapp.hooley.adapters.AdapterMyFriends
import com.hooleyapp.hooley.adapters.AdapterPastEventList
import com.hooleyapp.hooley.adapters.AdapterPendingFriendRequest
import com.hooleyapp.hooley.databinding.FragmentFriendsBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.event.PastEventListFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.fragments.others.MoreMediaDialogFragment
import com.hooleyapp.hooley.fragments.profile.ProfileMainFragment.Companion.fragmentProfileMainBinding
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.FriendsModel
import com.hooleyapp.hooley.model.PastGuestListModel
import com.hooleyapp.hooley.model.PendingFriendRequestModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.services.InviteWebService
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Nauman on 3/2/2018.
 */

class FriendsFragment : BaseFragment(), RadioGroup.OnCheckedChangeListener, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, TextWatcher {


    var binding: FragmentFriendsBinding? = null
    private val myInviteEventModel: PastGuestListModel? = null
    private var adapterPastEvent: AdapterPastEventList? = null
    private var moreFriendsDialoge: MoreMediaDialogFragment? = null
    private var list = ArrayList<PastGuestListModel.Guest>()
    private val gson = Gson()
    private val moreDialogeFragment: MoreDialogeFragment? = null
    private var objectResponseFriends: FriendsModel? = null
    private var mFriendListFilter: ArrayList<FriendsTable>? = null
    private var adapterMyFriends: AdapterMyFriends? = null
    private var mListSearch: ArrayList<FriendsTable> = ArrayList()
    private var objectResponsePendingFriendRequest: PendingFriendRequestModel? = null
    private var mListFilterPendingRequest: ArrayList<FriendsTable> = ArrayList()
    private var adapterPendingFriendRequest: AdapterPendingFriendRequest? = null
    var isMaleChecked = false
    var isFemaleChecked = false
    var isOtherChecked = false
    private var service: FriendWebService? = null
    internal var isSwiped = false
    private var inviteWebService: InviteWebService? = null
    internal val arrayList = ArrayList<AdapterMoreMediaDialoge.MoreItem>()
    internal var font1 = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)
    internal var font2 = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)
    internal var count = 0
    internal var isFriend = true

    private val otherFilterList: ArrayList<FriendsTable>
        get() {
            val tempArray = ArrayList<FriendsTable>()
            when (binding!!.rgFriends.checkedRadioButtonId) {
                R.id.rbMyFriends -> {
                    if (objectResponseFriends != null && objectResponseFriends!!.friendsList.size > 0) {
                        for (i in objectResponseFriends!!.friendsList.indices) {
                            if (objectResponseFriends!!.friendsList[i].gender == Constants.GENDER_OTHER)
                                tempArray.add(objectResponseFriends!!.friendsList[i])
                        }
                        return tempArray
                    }
                    if (objectResponsePendingFriendRequest != null && objectResponsePendingFriendRequest!!.friendsList.size > 0) {
                        for (i in objectResponsePendingFriendRequest!!.friendsList.indices) {
                            if (objectResponsePendingFriendRequest!!.friendsList[i].gender == Constants.GENDER_OTHER)
                                tempArray.add(objectResponsePendingFriendRequest!!.friendsList[i])
                        }
                        return tempArray
                    }
                    return tempArray
                }
                R.id.rbFriendRequests -> {
                    if (objectResponsePendingFriendRequest != null && objectResponsePendingFriendRequest!!.friendsList.size > 0) {
                        for (i in objectResponsePendingFriendRequest!!.friendsList.indices) {
                            if (objectResponsePendingFriendRequest!!.friendsList[i].gender == Constants.GENDER_OTHER)
                                tempArray.add(objectResponsePendingFriendRequest!!.friendsList[i])
                        }
                        return tempArray
                    }
                    return tempArray
                }
                else -> return tempArray
            }
        }

    private val femaleFilterList: ArrayList<FriendsTable>
        get() {
            val tempArray = ArrayList<FriendsTable>()
            when (binding!!.rgFriends.checkedRadioButtonId) {
                R.id.rbMyFriends -> {
                    if (objectResponseFriends != null && objectResponseFriends!!.friendsList.size > 0) {
                        for (i in objectResponseFriends!!.friendsList.indices) {
                            if (objectResponseFriends!!.friendsList[i].gender == Constants.GENDER_FEMALE)
                                tempArray.add(objectResponseFriends!!.friendsList[i])
                        }
                        return tempArray
                    }
                    if (objectResponsePendingFriendRequest != null && objectResponsePendingFriendRequest!!.friendsList.size > 0) {
                        for (i in objectResponsePendingFriendRequest!!.friendsList.indices) {
                            if (objectResponsePendingFriendRequest!!.friendsList[i].gender == Constants.GENDER_FEMALE)
                                tempArray.add(objectResponsePendingFriendRequest!!.friendsList[i])
                        }
                        return tempArray
                    }
                    return tempArray
                }
                R.id.rbFriendRequests -> {
                    if (objectResponsePendingFriendRequest != null && objectResponsePendingFriendRequest!!.friendsList.size > 0) {
                        for (i in objectResponsePendingFriendRequest!!.friendsList.indices) {
                            if (objectResponsePendingFriendRequest!!.friendsList[i].gender == Constants.GENDER_FEMALE)
                                tempArray.add(objectResponsePendingFriendRequest!!.friendsList[i])
                        }
                        return tempArray
                    }
                    return tempArray
                }
                else -> return tempArray
            }
        }

    private val maleFilterList: ArrayList<FriendsTable>
        get() {
            val tempArray = ArrayList<FriendsTable>()
            when (binding!!.rgFriends.checkedRadioButtonId) {
                R.id.rbMyFriends -> {
                    if (objectResponseFriends != null && objectResponseFriends!!.friendsList.size > 0) {
                        for (i in objectResponseFriends!!.friendsList.indices) {
                            if (objectResponseFriends!!.friendsList[i].gender == Constants.GENDER_MALE)
                                tempArray.add(objectResponseFriends!!.friendsList[i])
                        }

                        return tempArray
                    }
                    if (objectResponsePendingFriendRequest != null && objectResponsePendingFriendRequest!!.friendsList.size > 0) {
                        for (i in objectResponsePendingFriendRequest!!.friendsList.indices) {
                            if (objectResponsePendingFriendRequest!!.friendsList[i].gender == Constants.GENDER_MALE)
                                tempArray.add(objectResponsePendingFriendRequest!!.friendsList[i])
                        }
                        return tempArray
                    }
                    return tempArray
                }
                R.id.rbFriendRequests -> {
                    if (objectResponsePendingFriendRequest != null && objectResponsePendingFriendRequest!!.friendsList.size > 0) {
                        for (i in objectResponsePendingFriendRequest!!.friendsList.indices) {
                            if (objectResponsePendingFriendRequest!!.friendsList[i].gender == Constants.GENDER_MALE)
                                tempArray.add(objectResponsePendingFriendRequest!!.friendsList[i])
                        }
                        return tempArray
                    }
                    return tempArray
                }
                else -> return tempArray
            }
        }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        HooleyMain.activity!!.removeDialog()
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_friends, container, false)
        setMoreCallerBackListener()
        setListener()
        if (count == 0 && isFriend) {
            binding!!.rbMyFriends.isChecked = true
            count++
        }
        fragmentProfileMainBinding!!.ivbtnBack.visibility = GONE
        return binding!!.root
    }

    override fun onResume() {
        super.onResume()
        when (binding!!.rgFriends.checkedRadioButtonId) {
            R.id.rbMyFriends -> getMyFriendsList()
            R.id.rbFriendRequests -> getMyPendingFriendRequest()
            else -> binding!!.rbAddFriend.isChecked = true
        }
    }

    private fun setListener() {
        binding!!.rgFriends.setOnCheckedChangeListener(this)
        binding!!.ivFilter.setOnClickListener(this)
        binding!!.rbMale.setOnClickListener(this)
        binding!!.slFriends.setOnRefreshListener(this)
        binding!!.rbFemale.setOnClickListener(this)
        binding!!.rbOther.setOnClickListener(this)
        binding!!.llContact.setOnClickListener(this)
        binding!!.llPastGuestList.setOnClickListener(this)
        binding!!.llHooley.setOnClickListener(this)
        binding!!.edtSearch.addTextChangedListener(this)
        binding!!.ivClose.setOnClickListener(this)
    }


    private fun setMoreCallerBackListener() {

        if (arrayList.size > 0)
            arrayList.clear()
        val itemAtoZ = AdapterMoreMediaDialoge.MoreItem("A - Z", true)
        val itemCity = AdapterMoreMediaDialoge.MoreItem("Current City", false)
        val itemShareInterest = AdapterMoreMediaDialoge.MoreItem("Shared Interests", false)
        val itemTime = AdapterMoreMediaDialoge.MoreItem("Time", false)

        arrayList.add(itemAtoZ)
        arrayList.add(itemCity)
        arrayList.add(itemShareInterest)
        arrayList.add(itemTime)


        moreFriendsDialoge = MoreMediaDialogFragment.Builder(HooleyMain.activity!!).setTitle("Sort by").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, _ ->
            when (position) {
                0 -> {
                    sortByAlphabetical()
                    arrayList[0].isCheck = true
                    arrayList[1].isCheck = false
                    arrayList[2].isCheck = false
                    arrayList[3].isCheck = false
                    moreFriendsDialoge!!.dismiss()
                    sortByAlphabetical()
                }
                1 -> {
                    arrayList[0].isCheck = false
                    arrayList[1].isCheck = true
                    arrayList[2].isCheck = false
                    arrayList[3].isCheck = false
                    moreFriendsDialoge!!.dismiss()
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        sortCityName()
                    }
                }
                2 -> {
                    arrayList[0].isCheck = false
                    arrayList[1].isCheck = false
                    arrayList[2].isCheck = true
                    arrayList[3].isCheck = false
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        sortByShareInterest()
                    }
                    moreFriendsDialoge!!.dismiss()
                }
                3 -> {
                    arrayList[0].isCheck = false
                    arrayList[1].isCheck = false
                    arrayList[2].isCheck = false
                    arrayList[3].isCheck = true
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        sortByTime()
                    }
                    moreFriendsDialoge!!.dismiss()
                }
            }
        }.create()


    }


    private fun sortCityName() {
        when (binding!!.rgFriends.checkedRadioButtonId) {
            R.id.rbMyFriends -> {
                if (mFriendListFilter == null)
                    return
                Collections.sort(mFriendListFilter!!, FriendsTable.sortByCityComparator)
                adapterMyFriends!!.notifyDataSetChanged()
            }
            R.id.rbFriendRequests -> {
                Collections.sort(mListFilterPendingRequest, FriendsTable.sortByCityComparator)
                adapterPendingFriendRequest!!.notifyDataSetChanged()
            }
        }
    }

    private fun sortByAlphabetical() {
        when (binding!!.rgFriends.checkedRadioButtonId) {
            R.id.rbMyFriends -> {
                if (mFriendListFilter == null)
                    return
                Collections.sort(mFriendListFilter!!, FriendsTable.sortByNameComparator)
                adapterMyFriends!!.notifyDataSetChanged()
            }
            R.id.rbFriendRequests -> {
                Collections.sort(mListFilterPendingRequest, FriendsTable.sortByNameComparator)
                adapterPendingFriendRequest!!.notifyDataSetChanged()
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun sortByShareInterest() {
        when (binding!!.rgFriends.checkedRadioButtonId) {
            R.id.rbMyFriends -> {
                if (mFriendListFilter == null)
                    return
                Collections.sort(mFriendListFilter!!, FriendsTable.sortByShareInterest)
                adapterMyFriends!!.notifyDataSetChanged()
            }
            R.id.rbFriendRequests -> {
                Collections.sort(mListFilterPendingRequest, FriendsTable.sortByShareInterest)
                adapterPendingFriendRequest!!.notifyDataSetChanged()
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun sortByTime() {
        when (binding!!.rgFriends.checkedRadioButtonId) {
            R.id.rbMyFriends -> {
                if (mFriendListFilter == null)
                    return
                Collections.sort(mFriendListFilter!!, FriendsTable.sortByTimeComparator)
                adapterMyFriends!!.notifyDataSetChanged()
            }
            R.id.rbFriendRequests -> {
                Collections.sort(mListFilterPendingRequest, FriendsTable.sortByTimeComparator)
                adapterPendingFriendRequest!!.notifyDataSetChanged()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        when (checkedId) {
            R.id.rbMyFriends -> {
                binding!!.edtSearch.setText("")
                binding!!.rlSearchBar.visibility = VISIBLE
                binding!!.vUp.visibility = VISIBLE
                count = 0
                isFriend = true
                binding!!.slFriends.visibility = VISIBLE
                binding!!.tvNoData.visibility = GONE
                binding!!.rbMyFriends.typeface = font1
                binding!!.rbAddFriend.typeface = font2
                binding!!.rbFriendRequests.typeface = font2
                binding!!.rvMyFriends.visibility = GONE
                binding!!.rvFriendsRequests.visibility = GONE
                binding!!.rbMyFriends.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding!!.rbMyFriends.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_left_radio)
                binding!!.rbFriendRequests.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding!!.rbFriendRequests.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding!!.rbAddFriend.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding!!.rbAddFriend.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding!!.llAddFriends.visibility = GONE
                binding!!.rvEventList.visibility = GONE
                binding!!.tvTitle.visibility = GONE
                getMyFriendsList()
            }
            R.id.rbFriendRequests -> {
                binding!!.edtSearch.setText("")
                binding!!.rlSearchBar.visibility = VISIBLE
                binding!!.vUp.visibility = VISIBLE
                count++
                isFriend = false
                arrayList[0].isCheck = true
                arrayList[1].isCheck = false
                arrayList[2].isCheck = false
                binding!!.slFriends.visibility = VISIBLE
                binding!!.tvNoData.visibility = GONE
                binding!!.rbMyFriends.typeface = font2
                binding!!.rbAddFriend.typeface = font2
                binding!!.rbFriendRequests.typeface = font1
                binding!!.rvMyFriends.visibility = GONE
                binding!!.rvFriendsRequests.visibility = GONE
                binding!!.rbMyFriends.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding!!.rbMyFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding!!.rbFriendRequests.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding!!.rbFriendRequests.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_purple_gradiant)
                binding!!.rbAddFriend.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding!!.rbAddFriend.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding!!.rvEventList.visibility = GONE
                binding!!.llAddFriends.visibility = GONE
                getMyPendingFriendRequest()
            }

            R.id.rbAddFriend -> {
                binding!!.rlSearchBar.visibility = GONE
                binding!!.vUp.visibility = GONE
                count++
                isFriend = false
                binding!!.slFriends.visibility = GONE
                binding!!.tvNoData.visibility = GONE
                binding!!.rbMyFriends.typeface = font2
                binding!!.rbAddFriend.typeface = font1
                binding!!.rbFriendRequests.typeface = font2
                binding!!.rvMyFriends.visibility = GONE
                binding!!.rvFriendsRequests.visibility = GONE
                binding!!.rbMyFriends.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding!!.rbMyFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding!!.rbFriendRequests.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding!!.rbFriendRequests.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding!!.rbAddFriend.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding!!.rbAddFriend.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_right_radio)
                binding!!.rvMyFriends.visibility = GONE
                binding!!.rvFriendsRequests.visibility = GONE
                binding!!.rlActionTabs.visibility = GONE
                binding!!.llAddFriends.visibility = VISIBLE
                binding!!.tvTitle.visibility = GONE
                binding!!.rvEventList.visibility = VISIBLE
                binding!!.tvTitle.visibility = VISIBLE
//                getPastGuestList()
            }
        }
    }

    private fun getMyPendingFriendRequest() {
        if (!isSwiped)
            binding!!.pbFriendsRequests.visibility = VISIBLE
        if (service != null) {
            service!!.getMyPendingFriendRequest(object : GenericCallback<PendingFriendRequestModel> {
                override fun success(result: PendingFriendRequestModel) {
                    binding!!.rlActionTabs.visibility = VISIBLE

                    if (isSwiped) {
                        binding!!.slFriends.isRefreshing = false
                        isSwiped = false
                    }
                    if (binding!!.rgFriends.checkedRadioButtonId == R.id.rbFriendRequests) {
                        binding!!.rlActionTabs.visibility = VISIBLE
                        binding!!.llAddFriends.visibility = GONE
                        binding!!.pbFriendsRequests.visibility = GONE
                        binding!!.rvMyFriends.visibility = GONE
                        binding!!.rvEventList.visibility = GONE
                        binding!!.rvFriendsRequests.visibility = VISIBLE
                        objectResponsePendingFriendRequest = result
                        setPendingFriendRequestRv()
                    }
                }

                override fun failure(message: String) {
                    if (isSwiped) {
                        binding!!.slFriends.isRefreshing = false
                        isSwiped = false
                    }
                    try {
                        binding!!.rvMyFriends.visibility = GONE
                        binding!!.rvFriendsRequests.visibility = GONE
                        binding!!.rvEventList.visibility = GONE
                        binding!!.rlActionTabs.visibility = GONE
                        binding!!.tvPeopleCount.text = "0 People"
                        binding!!.tvNoData.visibility = VISIBLE
                        binding!!.tvNoData.text = message
                        binding!!.pbFriendsRequests.visibility = GONE
                    } catch (ex: NullPointerException) {
                        binding!!.pbFriendsRequests.visibility = GONE
                        binding!!.tvNoData.text = message
                    }

                }
            })
        }
    }

    private fun getMyFriendsList() {
        if (!isSwiped)
            binding!!.pbMyFriends.visibility = VISIBLE
        service = FriendWebService()
        service!!.getMyFriendsList(object : GenericCallback<FriendsModel> {
            override fun success(result: FriendsModel) {
                binding!!.rlActionTabs.visibility = VISIBLE
                if (isSwiped) {
                    binding!!.slFriends.isRefreshing = false
                    isSwiped = false
                }
                if (binding!!.rgFriends.checkedRadioButtonId == R.id.rbMyFriends) {
                    binding!!.llAddFriends.visibility = GONE
                    binding!!.rlActionTabs.visibility = VISIBLE
                    binding!!.pbMyFriends.visibility = GONE
                    binding!!.rvMyFriends.visibility = VISIBLE
                    binding!!.rvFriendsRequests.visibility = GONE
                    binding!!.rvEventList.visibility = GONE
                    binding!!.tvNoData.visibility = GONE
                }
                objectResponseFriends = result
                if (binding!!.rbMyFriends.isChecked)
                    setMyFriendsRv()
            }

            override fun failure(message: String) {
                if (isSwiped) {
                    binding!!.slFriends.isRefreshing = false
                    isSwiped = false
                }
                binding!!.pbMyFriends.visibility = GONE
                try {
                    binding!!.rvFriendsRequests.visibility = GONE
                    binding!!.rvMyFriends.visibility = GONE
                    binding!!.rvEventList.visibility = GONE
                    binding!!.rlActionTabs.visibility = GONE
                    binding!!.tvNoData.visibility = VISIBLE
                    binding!!.tvNoData.text = message
                    binding!!.tvPeopleCount.text = "0 People"
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }
            }
        })
    }

    private fun setMyFriendsRv() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding!!.rvMyFriends.layoutManager = manager
        binding!!.rvMyFriends.layoutAnimation = animation
        if (mFriendListFilter != null) {
            if (mFriendListFilter!!.size > 0)
                mFriendListFilter!!.clear()
        } else {
            mFriendListFilter = ArrayList()
        }
        mFriendListFilter!!.addAll(objectResponseFriends!!.friendsList)
        binding!!.tvPeopleCount.text = mFriendListFilter!!.size.toString() + " Friends"
        adapterMyFriends = AdapterMyFriends(HooleyMain.activity!!, mFriendListFilter)
        binding!!.rvMyFriends.adapter = adapterMyFriends
        sortByAlphabetical()
        binding!!.rvMyFriends.setEmptyView(binding!!.tvNoData)

    }

    private fun setPendingFriendRequestRv() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding!!.rvFriendsRequests.layoutManager = manager
        binding!!.rvFriendsRequests.layoutAnimation = animation
        if (mListFilterPendingRequest.size > 0)
            mListFilterPendingRequest.clear()
        mListFilterPendingRequest.addAll(objectResponsePendingFriendRequest!!.friendsList)
        binding!!.tvPeopleCount.text = if (mListFilterPendingRequest.size == 1) {
            mListFilterPendingRequest.size.toString() + " Person"
        } else {
            mListFilterPendingRequest.size.toString() + " People"
        }
        adapterPendingFriendRequest = AdapterPendingFriendRequest(HooleyMain.activity!!, mListFilterPendingRequest)
        binding!!.rvFriendsRequests.adapter = adapterPendingFriendRequest
        binding!!.rvFriendsRequests.setEmptyView(binding!!.tvNoData)

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivFilter -> {
                if (!moreFriendsDialoge!!.isAdded)
                    moreFriendsDialoge!!.show(HooleyMain.activity!!.supportFragmentManager)
            }
            R.id.rbMale -> {
                isMaleChecked = !isMaleChecked
                if (isMaleChecked) {
                    binding!!.rlMale.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_selected_field)
                    binding!!.rbMale.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_male_white)
                } else {
                    binding!!.rlMale.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                    binding!!.rbMale.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_male)
                }
                applyGenderFilter()
            }
            R.id.rbFemale -> {
                isFemaleChecked = !isFemaleChecked
                if (isFemaleChecked) {
                    binding!!.rlFemale.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_rb_center)
                    binding!!.rbFemale.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_female_white)
                } else {
                    binding!!.rlFemale.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                    binding!!.rbFemale.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_female)
                }
                applyGenderFilter()
            }
            R.id.rbOther -> {
                isOtherChecked = !isOtherChecked
                if (isOtherChecked) {
                    binding!!.rlOther.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_rb_center)
                    binding!!.rbOther.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_other_white)
                } else {
                    binding!!.rlOther.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                    binding!!.rbOther.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_other)
                }
                applyGenderFilter()
            }
            R.id.llContact -> {
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, AddFriendFromContactFragment(), "AddFriendFromContactFragment")
            }
            R.id.llPastGuestList -> {
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, PastEventListFragment(), "PastEventListFragment")
            }
            R.id.llHooley -> {
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, AddFriendFromHooley(), "AddFriendFromHooley")
            }
            R.id.ivClose -> {
                binding!!.edtSearch.setText("")
            }
        }
    }

    private fun getPastGuestList() {
        if (binding != null && !isSwiped)
            binding!!.pbMyFriends.visibility = VISIBLE
        inviteWebService = InviteWebService()
        inviteWebService!!.getPastGuestList(object : GenericCallback<PastGuestListModel> {
            override fun success(result: PastGuestListModel) {
                binding!!.pbMyFriends.visibility = GONE
                if (isSwiped) {
                    binding!!.slFriends.isRefreshing = false
                    isSwiped = false
                }
                binding!!.llAddFriends.visibility = VISIBLE
                binding!!.rlActionTabs.visibility = GONE
                binding!!.rvMyFriends.visibility = GONE
                binding!!.rvFriendsRequests.visibility = GONE
                binding!!.tvTitle.visibility = VISIBLE
                binding!!.tvTitleHead.visibility = VISIBLE
                binding!!.rvEventList.visibility = VISIBLE
                list = result.guestArrayList
                setRecyclerView()

            }

            override fun failure(message: String) {
                if (isSwiped) {
                    binding!!.slFriends.isRefreshing = false
                    isSwiped = false
                }
                try {
                    binding!!.rlActionTabs.visibility = GONE
                    binding!!.rvFriendsRequests.visibility = GONE
                    binding!!.rvMyFriends.visibility = GONE
                    binding!!.rvEventList.visibility = GONE
                    binding!!.tvNoData.visibility = VISIBLE
                    binding!!.rvEventList.visibility = GONE
                    binding!!.tvNoData.text = message
                    binding!!.tvTitle.visibility = GONE
                    binding!!.tvTitleHead.visibility = GONE
                    binding!!.pbMyFriends.visibility = GONE
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }
            }
        })
    }

    private fun setRecyclerView() {
        binding!!.tvTitle.text = "(" + list.size + ")"
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        binding!!.rvEventList.layoutManager = manager
        binding!!.rvEventList.layoutAnimation = animation
        binding!!.rvEventList.setEmptyView(binding!!.tvNoData)
        adapterPastEvent = AdapterPastEventList(HooleyMain.activity!!, list)
        binding!!.rvEventList.adapter = adapterPastEvent
    }

    private fun applyGenderFilter() {
        when (binding!!.rgFriends.checkedRadioButtonId) {
            R.id.rbMyFriends -> {
                if (mFriendListFilter == null) {
                    mFriendListFilter = ArrayList()
                } else {
                    if (mFriendListFilter!!.size > 0)
                        mFriendListFilter!!.clear()
                }
                if (isMaleChecked || isFemaleChecked || isOtherChecked) {
                    mFriendListFilter!!.addAll(if (isMaleChecked) maleFilterList else ArrayList())
                    mFriendListFilter!!.addAll(if (isFemaleChecked) femaleFilterList else ArrayList())
                    mFriendListFilter!!.addAll(if (isOtherChecked) otherFilterList else ArrayList())
                } else {
                    if (objectResponseFriends != null)
                        mFriendListFilter!!.addAll(objectResponseFriends!!.friendsList)
                }
                if (adapterMyFriends != null)
                    adapterMyFriends!!.notifyDataSetChanged()
            }
            R.id.rbFriendRequests -> {
                if (mListFilterPendingRequest.size > 0)
                    mListFilterPendingRequest.clear()
                if (isMaleChecked || isFemaleChecked || isOtherChecked) {
                    mListFilterPendingRequest.addAll(if (isMaleChecked) maleFilterList else ArrayList())
                    mListFilterPendingRequest.addAll(if (isFemaleChecked) femaleFilterList else ArrayList())
                    mListFilterPendingRequest.addAll(if (isOtherChecked) otherFilterList else ArrayList())
                } else {
                    if (objectResponsePendingFriendRequest != null)
                        mListFilterPendingRequest.addAll(objectResponsePendingFriendRequest!!.friendsList)
                }
                if (adapterPendingFriendRequest != null)
                    adapterPendingFriendRequest!!.notifyDataSetChanged()
            }
        }
    }

    private fun showOtherFilterList() {
        when (binding!!.rgFriends.checkedRadioButtonId) {
            R.id.rbMyFriends -> {
                if (mFriendListFilter == null)
                    return
                mFriendListFilter!!.clear()
                for (i in objectResponseFriends!!.friendsList.indices) {
                    if (objectResponseFriends!!.friendsList[i].gender == Constants.GENDER_OTHER) {
                        mFriendListFilter!!.add(objectResponseFriends!!.friendsList[i])
                    }
                }
                adapterMyFriends!!.notifyDataSetChanged()
            }
            R.id.rbFriendRequests -> {
                mListFilterPendingRequest.clear()
                for (i in objectResponsePendingFriendRequest!!.friendsList.indices) {
                    if (objectResponsePendingFriendRequest!!.friendsList[i].gender == Constants.GENDER_OTHER) {
                        mListFilterPendingRequest.add(objectResponsePendingFriendRequest!!.friendsList[i])
                    }
                }
                adapterPendingFriendRequest!!.notifyDataSetChanged()
            }
        }
    }

    private fun showFemaleFilterList() {
        if (objectResponseFriends == null)
            return
        when (binding!!.rgFriends.checkedRadioButtonId) {
            R.id.rbMyFriends -> {
                if (mFriendListFilter == null)
                    return
                mFriendListFilter!!.clear()
                for (i in objectResponseFriends!!.friendsList.indices) {
                    if (objectResponseFriends!!.friendsList[i].gender == Constants.GENDER_FEMALE) {
                        mFriendListFilter!!.add(objectResponseFriends!!.friendsList[i])
                    }
                }
                adapterMyFriends!!.notifyDataSetChanged()
            }
            R.id.rbFriendRequests -> {
                mListFilterPendingRequest.clear()
                for (i in objectResponsePendingFriendRequest!!.friendsList.indices) {
                    if (objectResponsePendingFriendRequest!!.friendsList[i].gender == Constants.GENDER_FEMALE) {
                        mListFilterPendingRequest.add(objectResponsePendingFriendRequest!!.friendsList[i])
                    }
                }
                adapterPendingFriendRequest!!.notifyDataSetChanged()
            }
        }
    }

    private fun showMaleFilterList() {
        if (objectResponseFriends == null)
            return
        when (binding!!.rgFriends.checkedRadioButtonId) {
            R.id.rbMyFriends -> {
                if (mFriendListFilter == null)
                    return
                mFriendListFilter!!.clear()
                for (i in objectResponseFriends!!.friendsList.indices) {
                    if (objectResponseFriends!!.friendsList[i].gender == Constants.GENDER_MALE) {
                        mFriendListFilter!!.add(objectResponseFriends!!.friendsList[i])
                    }
                }
                adapterMyFriends!!.notifyDataSetChanged()
            }
            R.id.rbFriendRequests -> {
                mListFilterPendingRequest.clear()
                for (i in objectResponsePendingFriendRequest!!.friendsList.indices) {
                    if (objectResponsePendingFriendRequest!!.friendsList[i].gender == Constants.GENDER_MALE) {
                        mListFilterPendingRequest.add(objectResponsePendingFriendRequest!!.friendsList[i])
                    }
                }
                adapterPendingFriendRequest!!.notifyDataSetChanged()
            }
        }
    }

    private fun showOriginalList() {
        if (objectResponseFriends == null)
            return
        when (binding!!.rgFriends.checkedRadioButtonId) {
            R.id.rbMyFriends -> {
                if (mFriendListFilter == null) {
                    mFriendListFilter = ArrayList()
                } else {
                    if (mFriendListFilter!!.size > 0)
                        mFriendListFilter!!.clear()
                }
                mFriendListFilter!!.addAll(objectResponseFriends!!.friendsList)
                adapterMyFriends!!.notifyDataSetChanged()
            }
            R.id.rbFriendRequests -> {
                if (mListFilterPendingRequest.size > 0)
                    mListFilterPendingRequest.clear()
                mListFilterPendingRequest.addAll(objectResponsePendingFriendRequest!!.friendsList)
                adapterPendingFriendRequest!!.notifyDataSetChanged()
            }
        }
    }

    override fun onRefresh() {
        binding!!.slFriends.isRefreshing = true
        binding!!.pbMyFriends.visibility = GONE
        binding!!.pbFriendsRequests.visibility = GONE
        isSwiped = true
        binding!!.edtSearch.setText("")
        if (binding!!.rbMyFriends.isChecked)
            getMyFriendsList()
        else if (binding!!.rbFriendRequests.isChecked)
            getMyPendingFriendRequest()
//        else
//            getPastGuestList()
    }

    private fun filter(text: String, isFriend: Boolean) {
        try {
            mListSearch.clear()
            mFriendListFilter!!.clear()
            mListFilterPendingRequest.clear()
            val filteredList = ArrayList<FriendsTable>()
            if (isFriend) {
                mListSearch.addAll(objectResponseFriends!!.friendsList)
            } else {
                mListSearch.addAll(objectResponsePendingFriendRequest!!.friendsList)
            }
            for (item in mListSearch.indices) {
                if (mListSearch[item].fullName.toLowerCase().contains(text.toLowerCase())) {
                    filteredList.add(mListSearch[item])
                }
            }

            if (filteredList.size > 0) {
                if (binding!!.rgFriends.checkedRadioButtonId == R.id.rbMyFriends)
                    adapterMyFriends!!.filterList(filteredList)
                else
                    adapterPendingFriendRequest!!.filterList(filteredList)
                binding!!.tvPeopleCount.text = if (filteredList.size == 1) {
                    filteredList.size.toString() + " Person"
                } else {
                    filteredList.size.toString() + " People"
                }
            } else {
                binding!!.tvPeopleCount.text = "0 People"
                binding!!.tvNoData.text = "No Record Found"
                if (binding!!.rgFriends.checkedRadioButtonId == R.id.rbMyFriends) {
                    adapterMyFriends!!.filterList(filteredList)
                } else {
                    adapterPendingFriendRequest!!.filterList(filteredList)
                }
            }
        } catch (e: KotlinNullPointerException) {
        }
    }


    override fun afterTextChanged(s: Editable?) {
        if (s!!.isNotEmpty()) {
            filter(s.toString(), (binding!!.rgFriends.checkedRadioButtonId == R.id.rbMyFriends))
        } else {
            try {
                if (binding!!.rgFriends.checkedRadioButtonId == R.id.rbMyFriends) {
                    if (mFriendListFilter == null)
                        mFriendListFilter = ArrayList()
                    if (mFriendListFilter!!.size > 0)
                        mFriendListFilter!!.clear()
                    mFriendListFilter!!.addAll(objectResponseFriends!!.friendsList)
                    binding!!.tvPeopleCount.text = if (mFriendListFilter!!.size == 1) {
                        mFriendListFilter!!.size.toString() + " Person"
                    } else {
                        mFriendListFilter!!.size.toString() + " People"
                    }
                    adapterMyFriends!!.filterList(mFriendListFilter!!)
                } else if (binding!!.rgFriends.checkedRadioButtonId == R.id.rbFriendRequests) {
                    mListFilterPendingRequest.clear()
                    mListFilterPendingRequest.addAll(objectResponsePendingFriendRequest!!.friendsList)
                    binding!!.tvPeopleCount.text = if (mFriendListFilter!!.size == 1) {
                        mListFilterPendingRequest.size.toString() + " Person"
                    } else {
                        mListFilterPendingRequest.size.toString() + " People"
                    }
                    adapterPendingFriendRequest!!.filterList(mListFilterPendingRequest)
                }
            } catch (e: Exception) {
            }
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

}
