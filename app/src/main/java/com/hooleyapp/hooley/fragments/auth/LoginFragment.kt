package com.hooleyapp.hooley.fragments.auth

import android.annotation.TargetApi
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatDelegate
import android.text.InputType
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import com.facebook.CallbackManager
import com.facebook.FacebookSdk
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyAuth
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentLoginBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.ISocialCallBackListener
import com.hooleyapp.hooley.model.LoginModel
import com.hooleyapp.hooley.model.staticData.StaticDataModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.AuthWebService
import com.hooleyapp.hooley.social.SocialClass
import java.util.*

/**
 * Created by Nauman on 11/30/2017.
 */

class LoginFragment : BaseFragment(), View.OnClickListener, ISocialCallBackListener, CompoundButton.OnCheckedChangeListener {
    var service = AuthWebService()
    internal var binding: FragmentLoginBinding? = null
    internal var socailClass = SocialClass()
    private var callbackManager: CallbackManager? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var gson = Gson()
    lateinit var notificationManager: NotificationManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        //        callbackManager = socailClass.initFacebookSdk();
        mGoogleApiClient = socailClass.initGooglePlus()
        binding!!.ivBack.setOnClickListener {
            HooleyAuth.activity.onBackPressed()
        }
        setListener()
        return binding!!.root
    }

    private fun setListener() {
        binding!!.rlSignIn.setOnClickListener(this)
        binding!!.btnRegister.setOnClickListener(this)
        binding!!.tvForgetPass.setOnClickListener(this)
        binding!!.rlFacebook.setOnClickListener(this)
        binding!!.cbViewPass.setOnCheckedChangeListener(this)
        binding!!.rlGoogle.setOnClickListener(this)
        binding!!.btnSignIn.setOnClickListener(this)
        socailClass.setMListener(this)

        if (email.isNotEmpty()) {
            binding!!.edEmail.setText(email)
        }
    }

    private fun validateInput(): Boolean {
        if (Util.isContentNull(binding!!.edEmail.text.toString())) {
            binding!!.edEmail.requestFocus()
            binding!!.edEmail.error = getString(R.string.str_error_email)
            return false
        } else if (!Util.isEmailValid(binding!!.edEmail.text.toString().trim())) {
            binding!!.edEmail.requestFocus()
            binding!!.edEmail.error = getString(R.string.invalid_email)
            return false
        } else if (Util.isContentNull(binding!!.edPass.text.toString())) {
            binding!!.edPass.requestFocus()
            binding!!.edPass.error = getString(R.string.str_password_error)
            return false
        } else {
            Util.hideKeyboard(activity!!)
            return true
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rlSignIn ->
                if (validateInput())
                    loginUser()
            R.id.tvForgetPass -> (activity as ActivityBase).callFragmentWithReplace(R.id.authContainer, ForgotPasswordFragment.newInstance(), "ForgotPasswordFragment")
            R.id.btnRegister -> (activity as ActivityBase).callFragmentWithReplace(R.id.authContainer, RegisterFragment.newInstance(), "RegisterFragment")
            R.id.rlFacebook -> {
                callbackManager = socailClass.initFacebookSdk()
                socailClass.loginWithFacebook(HooleyAuth.activity)
            }
            R.id.rlGoogle -> signInWithGoogle()
            R.id.btnSignIn -> if (validateInput())
                loginUser()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }


    private fun loginUser() {
        if (!NetworkUtil.isInternetConnected(HooleyAuth.activity)) {
            HooleyAuth.activity.showSuccessDialog(Constants.NO_NETWORK)
            return
        }
        HooleyAuth.activity.showDialog()
        service.loginUser(binding!!.edEmail.text.toString().trim(), binding!!.edPass.text.toString(), object : GenericCallback<LoginModel> {
            override fun success(result: LoginModel) {
                try {
                    HooleyApp.db.putBoolean(Constants.IS_FIRST_LOGIN, result.isFirstLogin)
                    HooleyApp.db.putBoolean(Constants.IS_PHONE_VERIFIED, result.generalProfile!!.userInfo.isPhoneVerified)
                    HooleyApp.db.putString(Constants.VERIFIED_PHONE_NUMBER, result.generalProfile!!.userInfo.countryCode + result.generalProfile!!.userInfo.phoneNo)
                    HooleyApp.db.putString(Constants.COUNTRY_CODE, result.generalProfile!!.userInfo.countryCode!!)
                    HooleyApp.db.putBoolean(Constants.IS_BUSINESS_PROFILE_CREATED, result.isBusinessProfile)
                    HooleyApp.db.putBoolean(Constants.IS_WALLET_SETUP, result.isWalletSetup)
                    HooleyApp.db.putString(Constants.USER_ID, result.userId!!)
                    HooleyApp.db.putString(Constants.AUTH_TOKKEN, result.userToken!!)
                    HooleyApp.db.putString(Constants.EMAIL_ID, binding!!.edEmail.text.toString())
                    HooleyApp.db.putString(Constants.FULL_NAME, result.generalProfile!!.userInfo.firstName + " " + result.generalProfile!!.userInfo.lastName)
                    if (!TextUtils.isEmpty(result.generalProfile!!.userInfo.profilePicture))
                        HooleyApp.db.putString(Constants.USER_AVATAR, result.generalProfile!!.userInfo.profilePicture!!)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

                if (result.generalProfile!!.userGeofence.size > 0) {
                    if (result.generalProfile!!.userGeofence.size > 1) {
                        val listLatLong = ArrayList<LatLng>()
                        for (i in result.generalProfile!!.userGeofence.indices) {
                            listLatLong.add(LatLng(java.lang.Double.parseDouble(result.generalProfile!!.userGeofence[i].latitude), java.lang.Double.parseDouble(result.generalProfile!!.userGeofence[i].longitude)))
                        }
                        val centerLatLngObj = (HooleyMain.activity as ActivityBase).computeCentroid(listLatLong)
                        HooleyApp.db.putDouble(Constants.PERSONAL_LAT, centerLatLngObj.latitude)
                        HooleyApp.db.putDouble(Constants.PERSONAL_LANG, centerLatLngObj.latitude)
                        HooleyApp.db.putString(Constants.PERSONAL_RADIUS, result.generalProfile!!.userGeofence[0].radius!!)
                    } else {
                        HooleyApp.db.putDouble(Constants.PERSONAL_LAT, java.lang.Double.parseDouble(result.generalProfile!!.userGeofence[0].latitude))
                        HooleyApp.db.putDouble(Constants.PERSONAL_LANG, java.lang.Double.parseDouble(result.generalProfile!!.userGeofence[0].longitude))
                        HooleyApp.db.putString(Constants.PERSONAL_RADIUS, result.generalProfile!!.userGeofence[0].radius!!)
                    }
                }
                if (result.isDark) {
                    HooleyApp.db.putInt(Constants.TYPE_THEME, AppCompatDelegate.MODE_NIGHT_YES)
//                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
//                    HooleyAuth.activity.delegate.setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES)

                } else {
                    HooleyApp.db.putInt(Constants.TYPE_THEME, AppCompatDelegate.MODE_NIGHT_NO)
//                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
//                    HooleyAuth.activity.delegate.setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                }
                getStaticData(result)

            }

            override fun failure(message: String) {
                HooleyAuth.activity.removeDialog()
                try {
                    HooleyAuth.activity.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun getStaticData(result: LoginModel) {
        notificationManager = HooleyAuth.activity.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
        service.getStaticData(result.userId!!, "2", object : GenericCallback<StaticDataModel> {
            override fun success(data: StaticDataModel) {
                HooleyAuth.activity.removeDialog()
                Log.d("okTAG", gson.toJson(data))
                HooleyApp.db.putString(Constants.STATIC_MODEL, gson.toJson(data))
                performAction(result)
            }

            override fun failure(message: String) {
                HooleyAuth.activity.removeDialog()
                try {
                    HooleyAuth.activity.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }
            }
        })
    }


    fun performAction(result: LoginModel) {
        if (!result.isFirstLogin) {
            (activity as ActivityBase).callFragmentWithReplace(R.id.authContainer, ProfileSetupFragment.newInstance(), "ProfileSetupFragment")
        } else {
            val intent = Intent(activity, HooleyMain::class.java)
            startActivity(intent)
            activity!!.finish()
        }
    }


    private fun loginWithSMUser(loginMedia: String) {

        if (!NetworkUtil.isInternetConnected(HooleyAuth.activity)) {
            HooleyAuth.activity.showSuccessDialog(Constants.NO_NETWORK)
            return
        }
        HooleyAuth.activity.showDialog()
        service.loginWithSMUser(loginMedia, object : GenericCallback<LoginModel> {
            override fun success(result: LoginModel) {
                HooleyAuth.activity.removeDialog()
                HooleyApp.db.putBoolean(Constants.IS_FIRST_LOGIN, result.isFirstLogin)
                HooleyApp.db.putBoolean(Constants.IS_BUSINESS_PROFILE_CREATED, result.isBusinessProfile)
                HooleyApp.db.putString(Constants.USER_ID, result.userId!!)
                HooleyApp.db.putString(Constants.EMAIL_ID, result.generalProfile!!.userInfo.email!!)
                HooleyApp.db.putString(Constants.AUTH_TOKKEN, result.userToken!!)
                HooleyApp.db.putBoolean(Constants.IS_WALLET_SETUP, result.isWalletSetup)
                HooleyApp.db.putString(Constants.FULL_NAME, result.generalProfile!!.userInfo.firstName + " " + result.generalProfile!!.userInfo.lastName)
                if (!TextUtils.isEmpty(result.generalProfile!!.userInfo.profilePicture))
                    HooleyApp.db.putString(Constants.USER_AVATAR, result.generalProfile!!.userInfo.profilePicture!!)

                if (result.generalProfile!!.userGeofence.size > 0) {
                    if (result.generalProfile!!.userGeofence.size > 1) {
                        val listLatLong = ArrayList<LatLng>()
                        for (i in result.generalProfile!!.userGeofence.indices) {
                            listLatLong.add(LatLng(java.lang.Double.parseDouble(result.generalProfile!!.userGeofence[i].latitude), java.lang.Double.parseDouble(result.generalProfile!!.userGeofence[i].longitude)))
                        }
                        val centerLatLngObj = (HooleyMain.activity as ActivityBase).computeCentroid(listLatLong)
                        HooleyApp.db.putDouble(Constants.PERSONAL_LAT, centerLatLngObj.latitude)
                        HooleyApp.db.putDouble(Constants.PERSONAL_LANG, centerLatLngObj.latitude)
                        HooleyApp.db.putString(Constants.PERSONAL_RADIUS, "5")
                    } else {
                        HooleyApp.db.putDouble(Constants.PERSONAL_LAT, java.lang.Double.parseDouble(result.generalProfile!!.userGeofence[0].latitude))
                        HooleyApp.db.putDouble(Constants.PERSONAL_LANG, java.lang.Double.parseDouble(result.generalProfile!!.userGeofence[0].longitude))
                        HooleyApp.db.putString(Constants.PERSONAL_RADIUS, "5")
                    }
                }

                if (result.isDark) {
                    HooleyApp.db.putInt(Constants.TYPE_THEME, AppCompatDelegate.MODE_NIGHT_YES)
//                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
//                    HooleyAuth.activity.delegate.setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES)

                } else {
                    HooleyApp.db.putInt(Constants.TYPE_THEME, AppCompatDelegate.MODE_NIGHT_NO)
//                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
//                    HooleyAuth.activity.delegate.setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                }
                getStaticData(result)
            }

            override fun failure(message: String) {
                HooleyAuth.activity.removeDialog()
                try {
                    HooleyAuth.activity.showSuccessDialog(message)
                    if (message == "No user found") {
                        (activity as ActivityBase).callFragmentWithReplace(R.id.authContainer, CompleteRegisterFragment.newInstance(loginMedia), "CompleteRegisterFragment")
                    }
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }


            }
        })
    }

    private fun signInWithGoogle() {
        if (mGoogleApiClient!!.isConnected)
            Auth.GoogleSignInApi.signOut(mGoogleApiClient)
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        HooleyAuth.activity.startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)

            if (result.isSuccess) {
                socailClass.handleSignInResult(result)
            } else {
                socailClass.handleSignInResult(null)
            }
        }
        if (FacebookSdk.isFacebookRequestCode(requestCode)) {
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onCallBackFaceBook(isLogin: Boolean) {
        if (isLogin)
            loginWithSMUser(Constants.SOCIAL_FACE_BOOK)
    }

    override fun onCallBackTwitter(isLogin: Boolean) {

    }

    override fun onCallBackLinkedIn(isLogin: Boolean) {

    }

    override fun onCallBackGooglePlus(isLogin: Boolean) {
        if (isLogin) {
            mGoogleApiClient!!.disconnect()
            loginWithSMUser(Constants.SOCIAL_GOOGLE)
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        if (isChecked) {
            binding!!.cbViewPass.backgroundTintList = ContextCompat.getColorStateList(HooleyAuth.activity, R.color.app_black_color)
            binding!!.edPass.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
        } else {
            binding!!.cbViewPass.backgroundTintList = ContextCompat.getColorStateList(HooleyAuth.activity, R.color.app_gray_color)
            binding!!.edPass.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        }
        binding!!.edPass.setSelection(binding!!.edPass.length())
    }

    companion object {

        private val RC_SIGN_IN = 9001
        lateinit var instance: LoginFragment
        var email: String = ""
        //Why this?
        fun newInstance(): LoginFragment {
            instance = LoginFragment()
            return instance
        }

        fun newInstance(email_: String): LoginFragment {
            instance = LoginFragment()
            email = email_
            return instance
        }
    }
}
