package com.hooleyapp.hooley.fragments.messaging

import android.annotation.SuppressLint
import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.*
import android.widget.Toast
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterChatWindow
import com.hooleyapp.hooley.app.data.model.messages.GetAllMessageModel
import com.hooleyapp.hooley.app.viewmodel.messages.ChatWindowViewModel
import com.hooleyapp.hooley.databinding.FragmentChatWindowBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.notification.NotificationMainFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.fragments.search.SearchFilterFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.ICallBackUri
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DownloadTask
import com.hooleyapp.hooley.others.Util
import java.util.*

/**
 * Created by Nauman on 4/24/2018.
 */

@SuppressLint("ValidFragment")
class ChatWindowFragment constructor(var threadId: String, var friendId: String, var friendName: String) : BaseFragment(), View.OnClickListener, AdapterChatWindow.IChatWindowClickListener {

    lateinit var binding: FragmentChatWindowBinding
    lateinit var chatwindowViewModel: ChatWindowViewModel
    private var adapterChatWindow: AdapterChatWindow? = null
    private var moreDialogFragment: MoreDialogeFragment? = null
    private var moreSaveToGalleryDialogFragment: MoreDialogeFragment? = null
    private var pos = 0
    private val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            try {
                onMessageReceive(intent)
            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        HooleyMain.activity!!.supportActionBar!!.show()
        hideDrawer(HooleyMain.activity!!)
        HooleyApp.db.putString(Constants.RECEIVER_ID, friendId)
        LocalBroadcastManager.getInstance(HooleyMain.activity!!).registerReceiver(mMessageReceiver,
                IntentFilter("Message"))
    }

    fun setToolbar() {
        (HooleyMain.activity as HooleyMain).setToolbarTitle(friendName)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat_window, container, false)
        chatwindowViewModel = ViewModelProviders.of(this).get(ChatWindowViewModel::class.java)
        if (TextUtils.isEmpty(threadId))
            chatwindowViewModel.createThread(friendId)
        else
            chatwindowViewModel.getAllMessages(threadId)
        setHasOptionsMenu(true)
        setUiObserver()
        setListener()
        setMoreCallerBackListener()
        setSaveToGalleryMoreCallerBackListener()
        return binding.root
    }


    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        menu.clear()
        menuInflater.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, menuInflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.ic_search -> {
                HooleyMain.activity!!.hideKeyboard()
                if ((HooleyMain as ActivityBase).supportFragmentManager.findFragmentById(R.id.container) is SearchFilterFragment)
                    return false
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, SearchFilterFragment.newInstance(), "SearchFilterFragment")
                hideDrawer(HooleyMain.activity!!)
                return true
            }
            R.id.ic_notification -> {
                HooleyMain.activity!!.hideKeyboard()
                if ((HooleyMain as ActivityBase).supportFragmentManager.findFragmentById(R.id.container) is NotificationMainFragment)
                    return false
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, NotificationMainFragment.newInstance(), "NotificationMainFragment")
                hideDrawer(HooleyMain.activity!!)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setUiObserver() {
        chatwindowViewModel.canMessage.observe(this, android.arch.lifecycle.Observer {
            it?.let { isBlocked ->
                if (isBlocked)
                    binding.rlChat.visibility = View.GONE
                else
                    binding.rlChat.visibility = View.VISIBLE
            }
        })
        chatwindowViewModel.loading.observe(this, android.arch.lifecycle.Observer {
            it?.let { isLoading ->
                if (isLoading)
                    binding.pbChat.visibility = View.VISIBLE
                else
                    binding.pbChat.visibility = View.GONE
            }
        })
        chatwindowViewModel.threadId.observe(this, android.arch.lifecycle.Observer {
            it?.let { id ->
                threadId = id
                chatwindowViewModel.getAllMessages(threadId)
            }
        })

        chatwindowViewModel.mList.observe(this, android.arch.lifecycle.Observer {
            it?.let { mList ->
                setRecyclerView(mList)
            }
        })
    }

    private fun setListener() {
        binding.btnSendMsg.setOnClickListener(this)
        binding.btnUploadMedia.setOnClickListener(this)
    }


    private fun setRecyclerView(mList: ArrayList<GetAllMessageModel.MessageInfo>) {
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding.rvChat.layoutManager = manager
        adapterChatWindow = AdapterChatWindow(HooleyMain.activity!!, friendId, mList, chatwindowViewModel.canMessage.value!!)
        binding.rvChat.adapter = adapterChatWindow
        adapterChatWindow!!.setListener(this)
        binding.rvChat.scrollToPosition(chatwindowViewModel.mList.value!!.size - 1)
    }

    private fun onMessageReceive(intent: Intent) {
        val isImage = intent.extras!!.getBoolean("isImage")
        val isVideo = intent.extras!!.getBoolean("isVideo")
        val videoUrl = intent.extras!!.getString("mediaUrl")
        val imageUrl = intent.extras!!.getString("thumbnailUrl")
        if (!friendId.equals(intent.extras!!.getString("friendId")!!, ignoreCase = true)) {
            return
        }

        val streamMsg = GetAllMessageModel.MessageInfo()
        streamMsg.profilePic = intent.extras!!.getString("profilePic")
        streamMsg.senderId = intent.extras!!.getString("userId")

        if (isImage) {
            streamMsg.type = Constants.TYPE_CHAT_IMAGE
            streamMsg.mediaUrl = videoUrl
            streamMsg.thumbnailUrl = imageUrl
        } else if (isVideo) {
            streamMsg.type = Constants.TYPE_CHAT_VIDEO
            streamMsg.mediaUrl = videoUrl
        } else {
            streamMsg.type = Constants.TYPE_CHAT_TEXT
            val body = intent.extras!!.getString("body")!!.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (body.size == 1)
                return
            if (body[1] == null)
                return
            streamMsg.messageText = body[1]
        }
        streamMsg.sentTime = "Just Now"
        chatwindowViewModel.mList.value!!.add(chatwindowViewModel.mList.value!!.size, streamMsg)
        adapterChatWindow!!.notifyDataSetChanged()
        binding.rvChat.scrollToPosition(chatwindowViewModel.mList.value!!.size - 1)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
        Util.hideKeyboard(HooleyMain.activity!!)
        HooleyApp.db.putString(Constants.RECEIVER_ID, "")
        LocalBroadcastManager.getInstance(HooleyMain.activity!!).unregisterReceiver(mMessageReceiver)

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnSendMsg -> if (binding.edtMessage.text.toString().trim { it <= ' ' }.isEmpty()) {
                binding.edtMessage.requestFocus()
                binding.edtMessage.error = getString(R.string.required)
                return
            } else {
                val msg = binding.edtMessage.text.toString()
                sendMessageInUI(msg, Constants.TYPE_CHAT_TEXT, null, false)
                binding.edtMessage.setText("")
            }

            R.id.btnUploadMedia -> {
                if (!moreDialogFragment!!.isAdded)
                    moreDialogFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
            }
        }
    }


    fun sendMessageInUI(msg: String?, type: String, uri: Uri?, isVideo: Boolean) {
        val messageInfo = GetAllMessageModel.MessageInfo()
        messageInfo.profilePic = HooleyApp.db.getString(Constants.USER_AVATAR)
        messageInfo.senderId = HooleyApp.db.getString(Constants.USER_ID)
        when (type) {
            Constants.TYPE_CHAT_TEXT -> messageInfo.messageText = msg!!
            Constants.TYPE_CHAT_IMAGE -> messageInfo.mediaUrl = msg!!
            Constants.TYPE_CHAT_VIDEO -> messageInfo.mediaUrl = msg!!
        }

        messageInfo.type = type
        messageInfo.sentTime = "Just Now"
        chatwindowViewModel.mList.value!!.add(chatwindowViewModel.mList.value!!.size, messageInfo)
        adapterChatWindow!!.notifyDataSetChanged()
        binding.rvChat.scrollToPosition(chatwindowViewModel.mList.value!!.size - 1)
        when (type) {
            Constants.TYPE_CHAT_TEXT -> chatwindowViewModel.sendTextMessage(threadId, msg!!, Constants.TYPE_CHAT_TEXT, chatwindowViewModel.mList.value!!.size - 1)
            Constants.TYPE_CHAT_IMAGE -> chatwindowViewModel.sendMediaMessage(uri, isVideo, threadId, chatwindowViewModel.mList.value!!.size - 1)
            Constants.TYPE_CHAT_VIDEO -> chatwindowViewModel.sendMediaMessage(uri, isVideo, threadId, chatwindowViewModel.mList.value!!.size - 1)
        }
    }

    private fun setMoreCallerBackListener() {
        val arrayList = ArrayList<String>()
        arrayList.add("Photo")
        arrayList.add("Video")
        arrayList.add("Gallery")
        moreDialogFragment = MoreDialogeFragment.Builder(activity!!).setTitle("Select").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, _ ->
            when (position) {
                0 -> {
                    moreDialogFragment!!.dismiss()
                    HooleyMain.activity!!.startCamera(Constants.CAMERA_RQ)
                }
                1 -> {
                    moreDialogFragment!!.dismiss()
                    HooleyMain.activity!!.startVideoCamera(Constants.VIDEO_RQ)

                }
                2 -> {
                    moreDialogFragment!!.dismiss()
                    HooleyMain.activity!!.showGallery(Constants.IMAGE_PICKER_SELECT)
                }
            }
        }.create()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {

                Constants.CAMERA_RQ -> HooleyMain.activity!!.processCapturedPhoto(object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        sendMessageInUI(HooleyMain.activity!!.compressFile(result!!).path, Constants.TYPE_CHAT_IMAGE, result, false)
                    }
                })

                Constants.VIDEO_RQ -> HooleyMain.activity!!.processCapturedVideo(object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        sendMessageInUI(result?.path, Constants.TYPE_CHAT_VIDEO, result, true)
                    }
                })

                Constants.IMAGE_PICKER_SELECT -> {
                    HooleyMain.activity!!.processGalleryPhoto(data!!, object : ICallBackUri {
                        override fun imageUri(result: Uri?) {
                            if (result!!.toString().contains("mp4"))
                                sendMessageInUI(result.path, Constants.TYPE_CHAT_VIDEO, result, true)
                            else
                                sendMessageInUI(HooleyMain.activity!!.compressFile(result).path, Constants.TYPE_CHAT_IMAGE, result, false)
                        }
                    })
                }
            }
        } else {
            Toast.makeText(HooleyMain.activity, "Action Cancelled", Toast.LENGTH_SHORT).show()
        }

    }

    private fun setSaveToGalleryMoreCallerBackListener() {
        val arrayList = ArrayList<String>()
        arrayList.add("Save to Gallery")
        moreSaveToGalleryDialogFragment = MoreDialogeFragment.Builder(activity!!).setTitle("Select").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    moreSaveToGalleryDialogFragment!!.dismiss()
                    val downloadTask = DownloadTask(HooleyMain.activity)
                    downloadTask.execute(chatwindowViewModel.mList.value!![pos].mediaUrl)
                }
            }
        }.create()

    }

    override fun onItemLongClick(position: Int) {
        pos = position
        if (!moreSaveToGalleryDialogFragment!!.isAdded)
            moreSaveToGalleryDialogFragment!!.show(HooleyMain.activity!!.supportFragmentManager)


    }

}
