package com.hooleyapp.hooley.fragments.nearby

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.UiModeManager.MODE_NIGHT_YES
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.content.res.Resources
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentMyGeoFenceBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GeoFenceModel
import com.hooleyapp.hooley.model.PersonalGeofenceModel
import com.hooleyapp.hooley.model.PersonalProfileModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.ProfileWebService
import java.text.DecimalFormat
import java.util.*

/**
 * Created by Zeeshan on 15-Jan-18.
 */

class MyGeoFenceFragment : BaseFragment(), OnMapReadyCallback, View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, PlaceSelectionListener, LocationListener, ResultCallback<LocationSettingsResult>, GoogleMap.OnMapLongClickListener {
    var geofenceList: ArrayList<GeoFenceModel>? = ArrayList()
    protected var mGoogleApiClient: GoogleApiClient? = null
    lateinit var mLocationRequest: LocationRequest
    lateinit var mLocationSettingsRequest: LocationSettingsRequest
    protected var mCurrentLocation: Location? = null
    private var binding: FragmentMyGeoFenceBinding? = null
    private var autocompleteFragment: AutocompleteSupportFragment? = null
    private var mRequestingLocationUpdates: Boolean? = null
    private var mGooglemap: GoogleMap? = null
    private var circleLat: Double? = null
    private var circleLon: Double? = null
    private var locationManager: LocationManager? = null
    private var mapFragment: SupportMapFragment? = null
    private var mapCircle: Circle? = null
    private val ct = "CT_" + javaClass.simpleName
    private var objectResponse: PersonalGeofenceModel? = null
    private val mLatlngs = ArrayList<LatLng>()
    private var listLatLong: ArrayList<LatLng>? = null
    private val perMile = 1609.34
    private var mPolygonOptions: PolygonOptions? = null
    private var centerLatLngObj: LatLng? = null
    private var address: String? = null
    private var mGeoFenceList = ArrayList<PersonalProfileModel.GeoFence>()
    private val service = ProfileWebService()
    private var isUnlocked = false
    private var displayInfoDialog = true
    private var currentProgress = 8046.72f
    private var showInfoDialog: Dialog? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_geo_fence, container, false)
        initMap()
        initPlaces()
        createLocationRequest()
        buildLocationSettingsRequest()
        //        checkLocationSettings();
        buildGoogleApiClient()
        return binding!!.root
    }

    protected fun buildLocationSettingsRequest() {
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        mLocationSettingsRequest = builder.build()
    }

    protected fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPlaces()
    }

    private fun setListner() {
        binding!!.rlUnlockLocation.setOnClickListener(this)
        binding!!.rlLockLocation.setOnClickListener(this)
        binding!!.sbGeofence.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (mapCircle != null) {
                    mapCircle!!.radius = progress.toDouble()
                }
                val f = DecimalFormat("##.00")
                currentProgress = seekBar!!.progress.toFloat()
                try {
                    if (currentProgress < 1609.344) {
                        if (progress <= 100) {
                            currentProgress = 100.0f
                            binding!!.tvSbProgress.text = 100.toString() + " meters"
                        } else {
                            binding!!.tvSbProgress.text = f.format(currentProgress) + " meters"
                        }
                    } else {
                        binding!!.tvSbProgress.text = "${f.format(currentProgress / 1609.344)} miles"
                    }
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                mGooglemap!!.clear()
                val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
                val animateZomm = currentZoomLevel - 5
                addCircle(currentProgress.toDouble(), circleLat!!, circleLon!!)
                mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat!!, circleLon!!), animateZomm))
                mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
            }
        })

        autocompleteFragment!!.setOnPlaceSelectedListener(this)
        binding!!.ivMinus.setOnClickListener(this)
        binding!!.ivPlus.setOnClickListener(this)
        binding!!.ivMyLocation.setOnClickListener(this)
        binding!!.ivMapInfo.setOnClickListener(this)
        binding!!.ivMinus.setOnLongClickListener {
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    if (binding!!.ivMinus.isPressed) {
                        if (currentProgress >= 100.0f) {
                            var temp = currentProgress
                            temp -= 100f
                            if (temp <= 100.0f) {
                                currentProgress = 100.0f
                            } else {
                                currentProgress -= 100f
                            }
                            binding!!.sbGeofence.progress = currentProgress.toInt()
                        }
                    } else {
                        timer.cancel()
                        updateUI()
                    }
                }
            }, 100, 200)
            true
        }

        binding!!.ivPlus.setOnLongClickListener {
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    if (binding!!.ivPlus.isPressed) {
                        if (currentProgress < 64373) {
                            currentProgress += 100f
                            binding!!.sbGeofence.progress = currentProgress.toInt()
                        }
                    } else {
                        timer.cancel()
                        updateUI()
                    }
                }
            }, 100, 200)
            true
        }
    }

    private fun updateUI() {
        Handler(Looper.getMainLooper()).post {
            if (mapCircle != null) {
                if (currentProgress > 30) {
                    mapCircle!!.radius = currentProgress.toDouble()
                    val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
                    val animateZomm = currentZoomLevel + 5
                    mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat!!, circleLon!!), animateZomm))
                    mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
                    addCircle(currentProgress.toDouble(), circleLat!!, circleLon!!)
                }
            }
        }
    }

    private fun initPlaces() {
        autocompleteFragment = childFragmentManager.findFragmentById(R.id.place_autocomplete_fragment) as AutocompleteSupportFragment
        autocompleteFragment!!.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS))
        if (autocompleteFragment!!.view == null)
            return
        (autocompleteFragment!!.view!!.findViewById<View>(R.id.places_autocomplete_search_input) as EditText).textSize = 10.0f
        (autocompleteFragment!!.view!!.findViewById<View>(R.id.places_autocomplete_search_input) as EditText).setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.text_color))
        (autocompleteFragment!!.view!!.findViewById<View>(R.id.places_autocomplete_search_input) as EditText).hint = "Enter Country/State/City/Zip Code"
        autocompleteFragment!!.view!!.findViewById<View>(R.id.places_autocomplete_search_button).visibility = View.GONE
        autocompleteFragment!!.view!!.findViewById<View>(R.id.places_autocomplete_clear_button)
                .setOnClickListener { autocompleteFragment!!.setText("") }
    }

    @Synchronized
    protected fun buildGoogleApiClient() {

        mGoogleApiClient = GoogleApiClient.Builder(HooleyMain.activity!!)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        settingsrequest()
    }

    fun settingsrequest() {

        mLocationRequest = LocationRequest.create()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = (30 * 1000).toLong()
        mLocationRequest.fastestInterval = (5 * 1000).toLong()
        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest)
        mLocationSettingsRequest = builder.build()
        builder.setAlwaysShow(true) //this is the key ingredient
        val result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, mLocationSettingsRequest)
        result.setResultCallback { result ->
            val status = result.status
            val state = result.locationSettingsStates
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> startLocationUpdates()
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                    status.startResolutionForResult(HooleyMain.activity, REQUEST_CHECK_SETTINGS)
                } catch (e: IntentSender.SendIntentException) {
                }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }
        }
    }

    private fun initMap() {
        locationManager = HooleyMain.activity!!.getSystemService(LOCATION_SERVICE) as LocationManager
        if (!googleServicesAvailable()) {
            Toast.makeText(HooleyMain.activity, "google service not available", Toast.LENGTH_SHORT).show()
            return
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission()
        } else {
            if (!locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            }
        }
        mapFragment = childFragmentManager.findFragmentById(R.id.mpGeofence) as SupportMapFragment
        mapFragment!!.getMapAsync(this)
    }

    fun googleServicesAvailable(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val isAvailable = apiAvailability.isGooglePlayServicesAvailable(HooleyMain.activity!!)
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true
        } else if (apiAvailability.isUserResolvableError(isAvailable)) {
            val dialog = apiAvailability.getErrorDialog(HooleyMain.activity, isAvailable, 0)
            dialog.show()
        } else {
            Toast.makeText(HooleyMain.activity, "Cant connect ", Toast.LENGTH_SHORT).show()
        }
        return false
    }

    fun checkLocationPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(HooleyMain.activity!!, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(HooleyMain.activity!!, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(HooleyMain.activity!!, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                        MY_PERMISSIONS_REQUEST_LOCATION)
            } else {
                ActivityCompat.requestPermissions(HooleyMain.activity!!, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                        MY_PERMISSIONS_REQUEST_LOCATION)
            }
            return false
        } else {
            if (!locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (mGoogleApiClient != null) {
                    settingsrequest()
                } else {
                    buildGoogleApiClient()
                }
            }
            return true
        }
    }

    //Check Api Response is polygon or circle
    @SuppressLint("MissingPermission")
    private fun setGeoFenceOnMap() {
        if (mGooglemap != null) {
            mGooglemap!!.clear()
        }
        if (geofenceList != null && geofenceList!!.size > 0) {
            if (geofenceList!!.size > 1) {
                // draw poligon
                Logger.v("geoFenceList")
                if (mLatlngs.size > 0)
                    mLatlngs.clear()
                var latLng: LatLng
                for (i in geofenceList!!.indices) {
                    latLng = LatLng(geofenceList!![i].placeLat, geofenceList!![i].placeLong)
                    mLatlngs.add(latLng)
                }
                mPolygonOptions = PolygonOptions()
                mPolygonOptions!!.fillColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_light_color))
                mPolygonOptions!!.strokeColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                mPolygonOptions!!.strokeWidth(3f)
                mPolygonOptions!!.addAll(mLatlngs)
                mGooglemap!!.addPolygon(mPolygonOptions)

                var centerLatLngObj = (HooleyMain.activity as ActivityBase).computeCentroid(mLatlngs)
                if (mGooglemap != null) {
                    mGooglemap!!.isMyLocationEnabled = false
                    mGooglemap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(centerLatLngObj, 11.5f))
                }

                centerLatLngObj = (HooleyMain.activity as ActivityBase).computeCentroid(mLatlngs)
                circleLat = centerLatLngObj.latitude
                circleLon = centerLatLngObj.longitude
                binding!!.sbGeofence.progress = ((geofenceList!![0].raduis).toFloat() * perMile).toInt()
            } else {
                // draw circle
                addCircle(java.lang.Double.parseDouble(geofenceList!![0].raduis) * perMile, geofenceList!![0].placeLat, geofenceList!![0].placeLong)
                circleLat = geofenceList!![0].placeLat
                circleLon = geofenceList!![0].placeLong
                val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
                val animateZomm = currentZoomLevel + 5
                if (mGooglemap != null) {
                    mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(geofenceList!![0].placeLat, geofenceList!![0].placeLong), animateZomm))
                    mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
                    mGooglemap!!.isMyLocationEnabled = false
                }

                binding!!.sbGeofence.progress = ((geofenceList!![0].raduis).toFloat() * perMile).toInt()
            }
        }
    }

    protected fun startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(HooleyMain.activity!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(HooleyMain.activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        } else {
            if (!mGoogleApiClient!!.isConnected)
                return
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    mLocationRequest,
                    this
            ).setResultCallback { status ->
                mRequestingLocationUpdates = true
                if (status.isSuccess) {
                    Log.i(ct, "Status true! GoingRpe to load!")
                } else {
                    Log.e(ct, "There is a problem")
                }
                getLastLocation()
            }
        }
    }

    protected fun stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        ).setResultCallback { status ->
            mRequestingLocationUpdates = false
            if (status.isSuccess) {
                Log.i(ct, "stopLocationUpdates-> Status true! Going to load!")
            } else {
                Log.e(ct, "stopLocationUpdates-> There is a problem")
            }
        }
    }

    fun getLastLocation() {
        if (mRequestingLocationUpdates == null)
            return
        if (mRequestingLocationUpdates!!) {
            if (mCurrentLocation != null) {
                Log.e(ct, "getLastLocation-> Now fetched!")
                Log.d(ct, "getLastLocation-> Lat is: " + mCurrentLocation!!.latitude)
                Log.d(ct, "getLastLocation-> Lon is: " + mCurrentLocation!!.longitude)
                stopLocationUpdates()
            } else {
                Log.i(ct, "getLastLocation-> Cannot fetch! (mCurrentLocation)")
            }
        } else {
            Log.i(ct, "getLastLocation-> Cannot fetch! (mRequestingLocationUpdates)")
        }
    }

    fun addCircle(radius: Double, cLat: Double, cLong: Double) {
        if (mGooglemap != null) {
            mGooglemap!!.clear()
            mLatlngs.clear()
            mPolygonOptions = null
            mGooglemap!!.uiSettings.isZoomGesturesEnabled = true
            mGooglemap!!.uiSettings.setAllGesturesEnabled(true)
        }
        if (mapCircle != null) {
            mapCircle!!.remove()
        }
        mapCircle = mGooglemap!!.addCircle(CircleOptions()
                .center(LatLng(cLat, cLong))
                .radius(radius)
                .fillColor(HooleyMain.activity!!.resources.getColor(R.color.app_purple_light_color))
                .strokeColor(HooleyMain.activity!!.resources.getColor(R.color.app_purple_color))
                .strokeWidth(2f)
                .clickable(true))
        mapCircle!!.isVisible = true

        val markerOption = MarkerOptions().position(LatLng(cLat, cLong)).flat(true).draggable(false)
        markerOption.icon(bitmapDescriptorFromVector(HooleyMain.activity, R.drawable.ic_map_cirlce_centre))
        mGooglemap!!.addMarker(markerOption)
    }

    private fun setData() {
        if (objectResponse!!.myGeofence.size == 1) {
            if (geofenceList!!.size > 0)
                geofenceList!!.clear()
            val model = GeoFenceModel()
            model.placeLat = java.lang.Double.parseDouble(objectResponse!!.myGeofence[0].latitude!!)
            model.placeLong = java.lang.Double.parseDouble(objectResponse!!.myGeofence[0].longitude!!)
            model.raduis = objectResponse!!.myGeofence[0].radius!!
            model.address = (HooleyMain.activity as ActivityBase).getCompleteAddressString(java.lang.Double.parseDouble(objectResponse!!.myGeofence[0].latitude!!), java.lang.Double.parseDouble(objectResponse!!.myGeofence[0].longitude!!))
            geofenceList!!.add(model)
            binding!!.tvHomeAddress.text = model.address
            (autocompleteFragment!!.view!!.findViewById<View>(R.id.places_autocomplete_search_input) as EditText).setText(objectResponse!!.myGeofence[0].address)
            setGeoFenceOnMap()
        } else {
            if (mLatlngs.size > 0)
                mLatlngs.clear()
            var latLng: LatLng
            for (i in objectResponse!!.myGeofence.indices) {
                latLng = LatLng(java.lang.Double.parseDouble(objectResponse!!.myGeofence[i].latitude!!), java.lang.Double.parseDouble(objectResponse!!.myGeofence[i].longitude!!))
                mLatlngs.add(latLng)
            }
            centerLatLngObj = (HooleyMain.activity as ActivityBase).computeCentroid(mLatlngs)
            address = (HooleyMain.activity as ActivityBase).getCompleteAddressString(centerLatLngObj!!.latitude, centerLatLngObj!!.longitude)
            if (geofenceList!!.size > 0)
                geofenceList!!.clear()
            for (loop in mLatlngs.indices) {
                val model = GeoFenceModel()
                model.placeLat = mLatlngs[loop].latitude
                model.placeLong = mLatlngs[loop].longitude
                model.raduis = (currentProgress / perMile).toString()
                model.address = objectResponse!!.myGeofence[0].address!!
                geofenceList!!.add(model)
            }
            binding!!.tvHomeAddress.text = address
            (autocompleteFragment!!.view!!.findViewById<View>(R.id.places_autocomplete_search_input) as EditText).setText(address)
            setGeoFenceOnMap()
        }
        mGooglemap!!.uiSettings.isScrollGesturesEnabled = false
        if (displayInfoDialog) {
            displayInfoDialog = false
            showInfoDialog()
        }

    }

    private fun getPersonalGeofence() {
        if (binding != null)
            binding!!.pbGeoFence.visibility = View.VISIBLE

        service.getPersonalGeoFence(object : GenericCallback<PersonalGeofenceModel> {
            override fun success(result: PersonalGeofenceModel) {
                HooleyMain.activity!!.removeDialog()
                binding!!.pbGeoFence.visibility = View.GONE
                objectResponse = result
                if (mGeoFenceList.size > 0)
                    mGeoFenceList.clear()
                mGeoFenceList = objectResponse!!.myGeofence
                setData()
            }

            override fun failure(message: String) {
                try {
                    binding!!.pbGeoFence.visibility = View.GONE
                    if (message.equals("No Geofence record found"))
                        moveToCurrentLocation()
                    else
                        HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }

    private fun updatePersonalGeofence() {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        HooleyMain.activity!!.showDialog()
        service.updatePersonalGeofence(geofenceList, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                getPersonalGeofence()
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun bitmapDescriptorFromVector(context: Context?, vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context!!, vectorResId)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            when (resultCode) {
                Activity.RESULT_OK -> startLocationUpdates()
                Activity.RESULT_CANCELED -> {
                }
            }

        }

        if (requestCode == REQUEST_CODE_GEOFENCE && resultCode == Activity.RESULT_OK) {

            val isCircle = data!!.getBooleanExtra("isCircle", false)
            if (isCircle) {
                if (geofenceList!!.size > 0)
                    geofenceList!!.clear()
                val model = GeoFenceModel()
                model.placeLat = data.getDoubleExtra("Lat", 0.0)
                model.placeLong = data.getDoubleExtra("Long", 0.0)
                model.raduis = data.getFloatExtra("radius", 0f).toString()
                model.address = (HooleyMain.activity as ActivityBase).getCompleteAddressString(data.getDoubleExtra("Lat", 0.0), data.getDoubleExtra("Long", 0.0))
                geofenceList!!.add(model)
                binding!!.tvHomeAddress.text = model.address
                HooleyApp.db.putDouble(Constants.PERSONAL_LAT, model.placeLat)
                HooleyApp.db.putDouble(Constants.PERSONAL_LANG, model.placeLong)
                HooleyApp.db.putString(Constants.PERSONAL_RADIUS, model.raduis)
            } else {
                listLatLong = data.getParcelableArrayListExtra("mLatlngs")
                centerLatLngObj = (HooleyMain.activity as ActivityBase).computeCentroid(listLatLong!!)
                address = (HooleyMain.activity as ActivityBase).getCompleteAddressString(centerLatLngObj!!.latitude, centerLatLngObj!!.longitude)
                if (geofenceList!!.size > 0)
                    geofenceList!!.clear()
                for (loop in listLatLong!!.indices) {
                    val model = GeoFenceModel()
                    model.placeLat = listLatLong!![loop].latitude
                    model.placeLong = listLatLong!![loop].longitude
                    model.raduis = "0"
                    model.address = address!!
                    geofenceList!!.add(model)
                }
                HooleyApp.db.putDouble(Constants.PERSONAL_LAT, centerLatLngObj!!.latitude)
                HooleyApp.db.putDouble(Constants.PERSONAL_LANG, centerLatLngObj!!.longitude)
                HooleyApp.db.putString(Constants.PERSONAL_RADIUS, "5")
                binding!!.tvHomeAddress.text = address
            }
            setGeoFenceOnMap()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(HooleyMain.activity!!, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        if (!locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                            if (mGoogleApiClient == null) {
                                buildGoogleApiClient()
                            } else {
                                settingsrequest()
                            }
                        }
                    }
                } else {
                    Toast.makeText(HooleyMain.activity, "permission denied", Toast.LENGTH_LONG).show()
                }
                return
            }
        }
    }

    override fun onConnected(bundle: Bundle?) {
        if (mGooglemap != null)
            mGooglemap!!.uiSettings.isMyLocationButtonEnabled = false
    }

    override fun onConnectionSuspended(i: Int) {

    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

    }

    override fun onLocationChanged(location: Location) {
        getLastLocation()
    }

    fun moveToCurrentLocation() {
        try {
            if (mCurrentLocation == null) {
                if (HooleyMain.activity != null) {
                    if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    }
                }
                mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
                if (mCurrentLocation != null) {
                    circleLat = mCurrentLocation!!.latitude
                    circleLon = mCurrentLocation!!.longitude
                    val loc = LatLng(mCurrentLocation!!.latitude, mCurrentLocation!!.longitude)
                    if (mGooglemap != null) {
                        mGooglemap!!.clear()
                        addCircle(currentProgress.toDouble(), circleLat!!, circleLon!!)
                        val markerOption = MarkerOptions().position(loc).flat(true).draggable(false)
                        markerOption.icon(bitmapDescriptorFromVector(HooleyMain.activity, R.drawable.ic_map_cirlce_centre))
                        mGooglemap!!.addMarker(markerOption)
                        mGooglemap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 11.5f))
                    }
                    (autocompleteFragment!!.view!!.findViewById<View>(R.id.places_autocomplete_search_input) as EditText).setText((HooleyMain.activity as ActivityBase).getCompleteAddressString(circleLat!!, circleLon!!))
                } else {
                    circleLat = HooleyApp.db.getDouble(Constants.CURRENT_LAT, 31.470319395591418)
                    circleLon = HooleyApp.db.getDouble(Constants.CURRENT_LANG, 74.36357019476067)
                    val loc = LatLng(circleLat!!, circleLon!!)
                    if (mGooglemap != null) {
                        mGooglemap!!.clear()
                        addCircle(currentProgress.toDouble(), circleLat!!, circleLon!!)
                        val markerOption = MarkerOptions().position(loc).flat(true).draggable(false)
                        markerOption.icon(bitmapDescriptorFromVector(HooleyMain.activity, R.drawable.ic_map_cirlce_centre))
                        mGooglemap!!.addMarker(markerOption)
                        mGooglemap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 11.5f))
                    }
                    (autocompleteFragment!!.view!!.findViewById<View>(R.id.places_autocomplete_search_input) as EditText).setText((HooleyMain.activity as ActivityBase).getCompleteAddressString(circleLat!!, circleLon!!))

                }
            } else {
                circleLat = mCurrentLocation!!.latitude
                circleLon = mCurrentLocation!!.longitude
                val loc = LatLng(mCurrentLocation!!.latitude, mCurrentLocation!!.longitude)
                if (mGooglemap != null) {
                    mGooglemap!!.clear()
                    addCircle(currentProgress.toDouble(), circleLat!!, circleLon!!)
                    val markerOption = MarkerOptions().position(loc).flat(true).draggable(false)
                    markerOption.icon(bitmapDescriptorFromVector(HooleyMain.activity, R.drawable.ic_map_cirlce_centre))
                    mGooglemap!!.addMarker(markerOption)
                    mGooglemap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 11.5f))
                }
                (autocompleteFragment!!.view!!.findViewById<View>(R.id.places_autocomplete_search_input) as EditText).setText((HooleyMain.activity as ActivityBase).getCompleteAddressString(mCurrentLocation!!.latitude, mCurrentLocation!!.longitude))
            }

        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
        }

    }

    override fun onMapReady(googleMap: GoogleMap) {

        mGooglemap = googleMap

        if (HooleyApp.db.getInt(Constants.TYPE_THEME) == MODE_NIGHT_YES) {
            try {
                val success = mGooglemap!!.setMapStyle(MapStyleOptions.loadRawResourceStyle(HooleyMain.activity!!, R.raw.black_map))
                if (!success) {
                    Log.e("", "Style parsing failed.")
                }
            } catch (e: Resources.NotFoundException) {
                Log.e("", "Can't find style. Error: ", e)
            }

        } else {
            try {
                val success = mGooglemap!!.setMapStyle(MapStyleOptions.loadRawResourceStyle(HooleyMain.activity!!, R.raw.white_map))
                if (!success) {
                    Log.e("", "Style parsing failed.")
                }
            } catch (e: Resources.NotFoundException) {
                Log.e("", "Can't find style. Error: ", e)
            }
        }

        setListner()
        getPersonalGeofence()
        mGooglemap!!.uiSettings.isScrollGesturesEnabled = false
        mGooglemap!!.uiSettings.isZoomControlsEnabled = false
    }

    override fun onStart() {
        super.onStart()
        mGoogleApiClient!!.connect()
    }

    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient!!.isConnected) {
            mGoogleApiClient!!.disconnect()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rlUnlockLocation -> {
                mGooglemap!!.setOnMapLongClickListener(this)
                isUnlocked = true
                mGooglemap!!.uiSettings.isScrollGesturesEnabled = true
                binding!!.rlUnlockLocation.visibility = View.GONE
                binding!!.rlLockLocation.visibility = View.VISIBLE
                binding!!.tvSetLocation.visibility = View.VISIBLE
                binding!!.ivMyLocation.visibility = View.VISIBLE
                binding!!.rlTop.visibility = View.VISIBLE
                binding!!.rlPlacePicker.visibility = View.VISIBLE
                if (!TextUtils.isEmpty(binding!!.tvHomeAddress.text.toString()))
                    (autocompleteFragment!!.view!!.findViewById<View>(R.id.places_autocomplete_search_input) as EditText).setText(binding!!.tvHomeAddress.text.toString())
                binding!!.tvHomeAddress.visibility = View.GONE
                binding!!.placePickerView.visibility = View.VISIBLE
                createLocationRequest()
                buildLocationSettingsRequest()
                checkLocationSettings()
            }
            R.id.rlLockLocation -> {
                isUnlocked = false
                mGooglemap!!.uiSettings.isScrollGesturesEnabled = false
                binding!!.rlUnlockLocation.visibility = View.VISIBLE
                binding!!.rlLockLocation.visibility = View.GONE
                binding!!.tvSetLocation.visibility = View.GONE
                binding!!.ivMyLocation.visibility = View.GONE
                binding!!.rlTop.visibility = View.GONE
                binding!!.rlPlacePicker.visibility = View.GONE
                binding!!.tvHomeAddress.visibility = View.VISIBLE
                binding!!.placePickerView.visibility = View.GONE


                if (geofenceList!!.size > 0)
                    geofenceList!!.clear()
                val model = GeoFenceModel()
                model.placeLat = circleLat!!
                model.placeLong = circleLon!!
                model.raduis = (currentProgress / perMile).toString()
                model.address = (HooleyMain.activity as ActivityBase).getCompleteAddressString(circleLat!!, circleLon!!)
                geofenceList!!.add(model)
                binding!!.tvHomeAddress.text = model.address
                HooleyApp.db.putDouble(Constants.PERSONAL_LAT, model.placeLat)
                HooleyApp.db.putDouble(Constants.PERSONAL_LANG, model.placeLong)
                HooleyApp.db.putString(Constants.PERSONAL_RADIUS, model.raduis)

                //setGeoFenceOnMap();
                updatePersonalGeofence()
            }
            R.id.ivMinus -> if (currentProgress >= 100.0f) {
                var temp = currentProgress
                temp -= 100f
                if (temp <= 100.0f) {
                    currentProgress = 100.0f
                } else {
                    currentProgress -= 100f
                }
                binding!!.sbGeofence.progress = currentProgress.toInt()
                updateUI()
            }
            R.id.ivPlus -> if (currentProgress < 64373) {
                currentProgress += 100f
                binding!!.sbGeofence.progress = currentProgress.toInt()
                updateUI()
            }
            R.id.ivMapInfo -> showInfoDialog()
            R.id.ivMyLocation -> moveToCurrentLocation()
        }
    }

    private fun showInfoDialog() {
        showInfoDialog = Dialog(HooleyMain.activity!!)
        showInfoDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        showInfoDialog!!.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        showInfoDialog!!.setContentView(R.layout.dialog_map_info)
        val ivClose = showInfoDialog!!.findViewById<ImageView>(R.id.ivClose)
        val tvText = showInfoDialog!!.findViewById<TextView>(R.id.tvText)
        tvText.text = HooleyMain.activity!!.getString(R.string.str_map_my_geo_fence)
        ivClose.setOnClickListener { showInfoDialog!!.dismiss() }
        showInfoDialog!!.show()
    }

    protected fun checkLocationSettings() {
        val result = LocationServices.SettingsApi.checkLocationSettings(
                mGoogleApiClient,
                mLocationSettingsRequest
        )
        result.setResultCallback(this)
    }

    override fun onPlaceSelected(place: Place) {
        circleLat = place.latLng!!.latitude
        circleLon = place.latLng!!.longitude
        (autocompleteFragment!!.view!!.findViewById<View>(R.id.places_autocomplete_search_input) as EditText).setText(place.address)
        val loc = LatLng(circleLat!!, circleLon!!)
        mGooglemap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 11.5f))
        addCircle(currentProgress.toDouble(), circleLat!!, circleLon!!)
    }

    override fun onError(status: Status) {
    }

    override fun onMapLongClick(latLng: LatLng) {
        if (isUnlocked) {
            circleLat = latLng.latitude
            circleLon = latLng.longitude
            (autocompleteFragment!!.view!!.findViewById<View>(R.id.places_autocomplete_search_input) as EditText).setText((HooleyMain.activity as ActivityBase).getCompleteAddressString(circleLat!!, circleLon!!))
            addCircle(currentProgress.toDouble(), circleLat!!, circleLon!!)
            mGooglemap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11.5f))
        }
    }

    override fun onResult(locationSettingsResult: LocationSettingsResult) {

    }

    override fun onResume() {
        super.onResume()
        if (isVisible) {
            if (mGooglemap != null)
                mGooglemap!!.clear()
            binding!!.sbGeofence.progress = 8046
            binding!!.tvSbProgress.text = "5 miles"
        }
    }

    companion object {

        val MY_PERMISSIONS_REQUEST_LOCATION = 99
        val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 1000
        val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2
        protected val REQUEST_CHECK_SETTINGS = 0x1
        private val REQUEST_CODE_GEOFENCE = 11
        private var instance: MyGeoFenceFragment? = null

        fun newInstance(): MyGeoFenceFragment {
            instance = MyGeoFenceFragment()
            return instance!!
        }
    }
}
