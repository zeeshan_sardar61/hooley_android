package com.hooleyapp.hooley.fragments.friends

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.text.TextUtils
import android.view.*
import android.widget.RadioGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentFriendProfileBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.messaging.ChatWindowFragment
import com.hooleyapp.hooley.fragments.notification.NotificationMainFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.fragments.search.SearchFilterFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.FriendProfileModel
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.FriendWebService
import java.util.*

/**
 * Created by Nauman on 3/6/2018.
 */

class FriendsProfileFragment : BaseFragment(), RadioGroup.OnCheckedChangeListener, View.OnClickListener, onClickDialog {

    internal var bold = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)
    internal var regular = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)
    internal var binding: FragmentFriendProfileBinding? = null
    private var objectResponse: FriendProfileModel? = null
    private var service = FriendWebService()
    private var moreDialogFragment: MoreDialogeFragment? = null

    init {
        setMoreCallerBackListener()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_friend_profile, container, false)
        setHasOptionsMenu(true)
        binding!!.rbPersonalProfile.typeface = bold
        hideDrawer(HooleyMain.activity!!)
        return binding!!.root
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        menu.clear()
        menuInflater.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, menuInflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.ic_search -> {
                HooleyMain.activity!!.hideKeyboard()
                if ((HooleyMain as ActivityBase).supportFragmentManager.findFragmentById(R.id.container) is SearchFilterFragment)
                    return false
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, SearchFilterFragment.newInstance(), "SearchFilterFragment")
                hideDrawer(HooleyMain.activity!!)
                return true
            }
            R.id.ic_notification -> {
                HooleyMain.activity!!.hideKeyboard()
                if ((HooleyMain as ActivityBase).supportFragmentManager.findFragmentById(R.id.container) is NotificationMainFragment)
                    return false
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, NotificationMainFragment.newInstance(), "NotificationMainFragment")
                hideDrawer(HooleyMain.activity!!)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    private fun setListener() {
        binding!!.rgFriendProfile.setOnCheckedChangeListener(this)
        binding!!.ivFaceBook.setOnClickListener(this)
        binding!!.ivTwitter.setOnClickListener(this)
        binding!!.ivInstagram.setOnClickListener(this)
        binding!!.ivLinkedIn.setOnClickListener(this)
        binding!!.ivFlicker.setOnClickListener(this)
        binding!!.rlMore.setOnClickListener(this)
        binding!!.ivMessage.setOnClickListener(this)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getFriendProfile()
    }


    fun setToolbar() {
        (HooleyMain.activity as HooleyMain).setToolbarTitle(friendName!!)
    }


    private fun setMoreCallerBackListener() {
        val arrayList = ArrayList<String>()
        arrayList.add("Block")
        arrayList.add("Unfriend")
        moreDialogFragment = MoreDialogeFragment.Builder(HooleyMain.activity!!).setTitle("More").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, _ ->
            when (position) {
                0 -> {
                    moreDialogFragment!!.dismiss()
                    HooleyMain.activity!!.YesNoDialogBlock("Are you sure you want to block", objectResponse!!.friendGeneralProfile!!.friendBasicInfo!!.fullName!!, object : onClickDialog {
                        override fun onClickYes() {
                            blockFriend(friendId)
                        }
                    })
                }
                1 -> {
                    moreDialogFragment!!.dismiss()
                    HooleyMain.activity!!.YesNoDialogBlock("Are you sure you want to unfriend", objectResponse!!.friendGeneralProfile!!.friendBasicInfo!!.fullName!!, object : onClickDialog {
                        override fun onClickYes() {
                            unFriend(friendId)
                        }
                    })
                }
            }
        }.create()

    }

    private fun setPersonalProfileData() {
        if (objectResponse != null) {
            if (objectResponse!!.friendGeneralProfile != null) {
                if (!objectResponse!!.friendGeneralProfile!!.isMessageAccessible) {
                    binding!!.ivMessage.visibility = View.GONE
                }
                if (objectResponse!!.friendGeneralProfile!!.friendBasicInfo != null) {
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendBasicInfo!!.fullName))
                        binding!!.tvFriendName.text = objectResponse!!.friendGeneralProfile!!.friendBasicInfo!!.fullName
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendBasicInfo!!.homeTown))
                        binding!!.tvFriendCountry.text = objectResponse!!.friendGeneralProfile!!.friendBasicInfo!!.homeTown
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendBasicInfo!!.profilePic))
                        binding!!.ivAvatar.load(objectResponse!!.friendGeneralProfile!!.friendBasicInfo!!.profilePic!!)
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendBasicInfo!!.aboutMe))
                        binding!!.tvPersonalAbout.text = objectResponse!!.friendGeneralProfile!!.friendBasicInfo!!.aboutMe
                }
                if (objectResponse!!.friendGeneralProfile!!.friendContactInfo == null) {
                    binding!!.llPersonalContactContainer.visibility = View.GONE
                    binding!!.rlCenterLock.visibility = View.VISIBLE
                    handleScrollingHeader(true)
                } else {
                    binding!!.llPersonalContactContainer.visibility = View.VISIBLE
                    binding!!.rlCenterLock.visibility = View.GONE
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendContactInfo!!.email))
                        binding!!.tvEmail.text = objectResponse!!.friendGeneralProfile!!.friendContactInfo!!.email
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendContactInfo!!.phoneNo))
                        binding!!.tvPhoneNumber.text = objectResponse!!.friendGeneralProfile!!.friendContactInfo!!.phoneNo
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendContactInfo!!.website))
                        binding!!.tvWebsite.text = objectResponse!!.friendGeneralProfile!!.friendContactInfo!!.website
                    if (objectResponse!!.friendGeneralProfile!!.friendContactInfo!!.isPhoneVerified) {
                        binding!!.ivPersonalVerified.visibility = View.VISIBLE
                        binding!!.btnPersonalNotVerified.visibility = View.GONE
                    } else {
                        binding!!.ivPersonalVerified.visibility = View.GONE
                        binding!!.btnPersonalNotVerified.visibility = View.VISIBLE
                    }
                }
                if (objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo != null) {
                    handleScrollingHeader(false)
                    binding!!.rlPersonalLock.visibility = View.GONE
                    binding!!.llPersonalInfoContainer.visibility = View.VISIBLE
                    if (!TextUtils.isEmpty(objectResponse?.friendGeneralProfile?.friendGeneralProfileInfo?.ageGroup))
                        binding!!.tvDob.text = objectResponse?.friendGeneralProfile?.friendGeneralProfileInfo?.ageGroup
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.gender))
                        binding!!.tvGender.text = objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.gender
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.collage))
                        binding!!.tvCollege.text = objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.collage
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.collageMajor))
                        binding!!.tvCollegeMajor.text = objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.collageMajor
//                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.currentCity))
                    binding!!.tvCurrentCity.text = "${objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.cityName}, ${objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.stateName}, ${objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.countryName}"

                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.zodiacSign))
                        binding!!.tvAstrologicalSign.text = objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.zodiacSign
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.highSchool))
                        binding!!.tvHighSchool.text = objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.highSchool
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.relationship))
                        binding!!.tvRelationShip.text = objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.relationship
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.politicalView))
                        binding!!.tvPoliticalView.text = objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.politicalView
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.religion))
                        binding!!.tvReligion.text = objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.religion
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.ethinicBackground))
                        binding!!.tvEthinic.text = objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.ethinicBackground
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.hobbies))
                        binding!!.tvHobbies.text = objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.hobbies
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.occupation))
                        binding!!.tvOccupation.text = objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.occupation
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.employer))
                        binding!!.tvEmployer.text = objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.employer
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.organization))
                        binding!!.tvOrganization.text = objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.organization
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.languages))
                        binding!!.tvLanguage.text = objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.languages
                    if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.homeTown))
                        binding!!.tvHometown.text = objectResponse!!.friendGeneralProfile!!.friendGeneralProfileInfo!!.homeTown
                } else {
                    handleScrollingHeader(true)
                    binding!!.rlPersonalLock.visibility = View.VISIBLE
                    binding!!.llPersonalInfoContainer.visibility = View.GONE
                }
                setPersonalSocial()
                setInterstRv()
                setListener()

            }
        }
    }

    private fun handleScrollingHeader(isFixed: Boolean) {
        val params = binding!!.clHeader.layoutParams as AppBarLayout.LayoutParams
        if (isFixed) {
            params.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP // list other flags here by |
            binding!!.clHeader.layoutParams = params
        } else {
            params.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS
            binding!!.clHeader.layoutParams = params
        }
    }

    private fun setInterstRv() {
        val interestList = ArrayList<String>()
        if (interestList.size > 0)
            interestList.clear()
        if (objectResponse?.friendGeneralProfile?.friendGeneralProfileInfo?.friendInterestList != null && objectResponse?.friendGeneralProfile?.friendGeneralProfileInfo?.friendInterestList!!.size > 0) {
            for (i in objectResponse?.friendGeneralProfile?.friendGeneralProfileInfo?.friendInterestList!!.indices) {
                interestList.add(objectResponse?.friendGeneralProfile?.friendGeneralProfileInfo?.friendInterestList!![i].name!!)
            }
        }
        var intests = interestList.joinToString()
        if (intests.isNotEmpty())
            binding!!.tvInterest.text = intests
        else
            binding!!.tvInterest.text = ""

    }

    @SuppressLint("SetTextI18n")
    private fun setBusinessProfile() {
        if (objectResponse != null) {
            if (objectResponse!!.isBusinessAvail) {
                handleScrollingHeader(false)
                binding!!.rlBusinessProfile.visibility = View.VISIBLE
                binding!!.rlNoBusinessProfile.visibility = View.GONE
                if (objectResponse!!.friendBusinessProfile != null) {
                    if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.profilePic))
                        Glide.with(HooleyMain.activity!!).load(objectResponse!!.friendBusinessProfile!!.profilePic).into(binding!!.ivAvatar)
                    if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.businessName))
                        binding!!.tvFriendName.text = objectResponse!!.friendBusinessProfile!!.businessName
                    if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.aboutBusiness))
                        binding!!.tvAboutBusiness.text = objectResponse!!.friendBusinessProfile!!.aboutBusiness
//                    if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.countryName))
//                        userSearchbinding!!.tvCountry.text = objectResponse!!.friendBusinessProfile!!.country
                    if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.address))
                        binding!!.tvBAddress.text = objectResponse!!.friendBusinessProfile!!.address + ", ${objectResponse!!.friendBusinessProfile!!.cityName}, ${objectResponse!!.friendBusinessProfile!!.stateName}, ${objectResponse!!.friendBusinessProfile!!.countryName}"

//                    if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.address))
//                        userSearchbinding!!.tvBusinessAddress.text = objectResponse!!.friendBusinessProfile!!.address
                    if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.businessName))
                        binding!!.tvBusinessName.text = objectResponse!!.friendBusinessProfile!!.businessName
                    if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.website))
                        binding!!.tvBusinessWebsite.text = objectResponse!!.friendBusinessProfile!!.website
                    if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.contactName))
                        binding!!.tvBusinessContactName.text = objectResponse!!.friendBusinessProfile!!.contactName
                    if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.businessEmail))
                        binding!!.tvBusinessEmail.text = objectResponse!!.friendBusinessProfile!!.businessEmail
                    if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.businessPhone))
                        binding!!.tvBusinessPhone.text = objectResponse!!.friendBusinessProfile!!.businessPhone
                    if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.companyType))
                        binding!!.tvCompanyType.text = objectResponse!!.friendBusinessProfile!!.companyType
                    if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.industry))
                        binding!!.tvIndustry.text = objectResponse!!.friendBusinessProfile!!.industry
//                    if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.city))
//                        userSearchbinding!!.tvCity.text = objectResponse!!.friendBusinessProfile!!.city
//                    if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.state))
//                        userSearchbinding!!.tvState.text = objectResponse!!.friendBusinessProfile!!.state
                    if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.zipCode))
                        binding!!.tvZipCode.text = objectResponse!!.friendBusinessProfile!!.zipCode
                    try {
                        binding!!.tvBusinessAddress.text = "${objectResponse!!.friendBusinessProfile!!.cityName}, ${objectResponse!!.friendBusinessProfile!!.stateName}, ${objectResponse!!.friendBusinessProfile!!.countryName}"
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    if (objectResponse!!.friendBusinessProfile!!.isPhoneVerified) {
                        binding!!.btnVerified.visibility = View.VISIBLE
                        binding!!.btnNotVerified.visibility = View.GONE
                    } else {
                        binding!!.btnVerified.visibility = View.GONE
                        binding!!.btnNotVerified.visibility = View.VISIBLE
                    }
                    setBusinessSocial()
                }
            } else {
                handleScrollingHeader(true)
                binding!!.rlNoBusinessProfile.visibility = View.VISIBLE
                binding!!.rlBusinessProfile.visibility = View.GONE
            }
        }
    }

    private fun setBusinessSocial() {
        if (objectResponse!!.friendBusinessProfile!!.businessSocialLists.size > 0) {
            for (i in objectResponse!!.friendBusinessProfile!!.businessSocialLists.indices) {
                when (objectResponse!!.friendBusinessProfile!!.businessSocialLists[i].socialName) {
                    Constants.SOCIAL_FB -> if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.businessSocialLists[i].socialUrl)) {
                        binding!!.ivFaceBook.setBackgroundResource(R.drawable.ic_socail_fb)
                    } else {
                        binding!!.ivFaceBook.setBackgroundResource(R.drawable.ic_social_fb_dissconnect)
                    }
                    Constants.SOCIAL_TW -> if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.businessSocialLists[i].socialUrl)) {
                        binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_socail_twitter)
                    } else {
                        binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_social_twitter_dissconnect)
                    }
                    Constants.SOCIAL_LI -> if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.businessSocialLists[i].socialUrl)) {
                        binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_socail_linkedin)
                    } else {
                        binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_social_linkedin_dissconnected)
                    }
                    Constants.SOCIAL_INSTA -> if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.businessSocialLists[i].socialUrl)) {
                        binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram)
                    } else {
                        binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram_dissconnect)
                    }
                    Constants.SOCIAL_FLKR -> if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.businessSocialLists[i].socialUrl)) {
                        binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_socail_filker)
                    } else {
                        binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_social_filker_dissconnect)
                    }
                }
            }
        } else {
            binding!!.ivFaceBook.setBackgroundResource(R.drawable.ic_social_fb_dissconnect)
            binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_social_twitter_dissconnect)
            binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_social_linkedin_dissconnected)
            binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram_dissconnect)
            binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_social_filker_dissconnect)
        }
    }

    private fun setPersonalSocial() {
        if (objectResponse!!.friendGeneralProfile!!.userSocialList != null && objectResponse!!.friendGeneralProfile!!.userSocialList.size > 0) {
            for (i in objectResponse!!.friendGeneralProfile!!.userSocialList.indices) {
                when (objectResponse!!.friendGeneralProfile!!.userSocialList[i].socialName) {
                    Constants.SOCIAL_FB -> if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.userSocialList[i].socialUrl)) {
                        binding!!.ivFaceBook.setBackgroundResource(R.drawable.ic_socail_fb)
                    } else {
                        binding!!.ivFaceBook.setBackgroundResource(R.drawable.ic_social_fb_dissconnect)
                    }
                    Constants.SOCIAL_TW -> if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.userSocialList[i].socialUrl)) {
                        binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_socail_twitter)
                    } else {
                        binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_social_twitter_dissconnect)
                    }
                    Constants.SOCIAL_LI -> if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.userSocialList[i].socialUrl)) {
                        binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_socail_linkedin)
                    } else {
                        binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_social_linkedin_dissconnected)
                    }
                    Constants.SOCIAL_INSTA -> if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.userSocialList[i].socialUrl)) {
                        binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram)
                    } else {
                        binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram_dissconnect)
                    }
                    Constants.SOCIAL_FLKR -> if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.userSocialList[i].socialUrl)) {
                        binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_socail_filker)
                    } else {
                        binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_social_filker_dissconnect)
                    }
                }
            }
        } else {
            binding!!.ivFaceBook.setBackgroundResource(R.drawable.ic_social_fb_dissconnect)
            binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_social_twitter_dissconnect)
            binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_social_linkedin_dissconnected)
            binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram_dissconnect)
            binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_social_filker_dissconnect)
        }
    }

    private fun onSocialItemClick(type: String) {
        when (binding!!.rgFriendProfile.checkedRadioButtonId) {
            R.id.rbPersonalProfile -> onPersonalSocialClick(type)
            R.id.rbBusinessProfile -> onSocialClick(type)
        }
    }

    private fun onSocialClick(type: String) {
        if (objectResponse!!.friendBusinessProfile != null) {
            @Suppress("SENSELESS_COMPARISON")
            if (objectResponse!!.friendBusinessProfile!!.businessSocialLists != null && objectResponse!!.friendBusinessProfile!!.businessSocialLists.size > 0) {
                for (i in objectResponse!!.friendBusinessProfile!!.businessSocialLists.indices) {
                    if (objectResponse!!.friendBusinessProfile!!.businessSocialLists[i].socialName!!.equals(type, ignoreCase = true)) {
                        if (!TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.businessSocialLists[i].socialUrl)) {
                            try {
                                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(objectResponse!!.friendBusinessProfile!!.businessSocialLists[i].socialUrl)))
                                break
                            } catch (ex: ActivityNotFoundException) {
                                ex.printStackTrace()
                                Util.showToastMessage(HooleyMain.activity!!, "Invalid url")
                                break
                            }

                        }
                    }
                }
            }
        }
    }

    private fun onPersonalSocialClick(type: String) {
        if (objectResponse!!.friendGeneralProfile != null) {
            if (objectResponse!!.friendGeneralProfile!!.userSocialList != null && objectResponse!!.friendGeneralProfile!!.userSocialList.size > 0) {
                for (i in objectResponse!!.friendGeneralProfile!!.userSocialList.indices) {
                    if (objectResponse!!.friendGeneralProfile!!.userSocialList[i].socialName!!.equals(type, ignoreCase = true)) {
                        if (!TextUtils.isEmpty(objectResponse!!.friendGeneralProfile!!.userSocialList[i].socialUrl)) {
                            try {
                                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(objectResponse!!.friendGeneralProfile!!.userSocialList[i].socialUrl)))
                                break
                            } catch (ex: ActivityNotFoundException) {
                                ex.printStackTrace()
                                Util.showToastMessage(HooleyMain.activity!!, "Invalid url")
                                break
                            }

                        }
                    }
                }
            }
        }
    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        when (checkedId) {
            R.id.rbPersonalProfile -> {
                setPersonalProfileData()
                binding!!.rbPersonalProfile.typeface = bold
                binding!!.rbBusinessProfile.typeface = regular
                binding!!.rlPersonalProfile.visibility = View.VISIBLE
                binding!!.rlBusinessProfile.visibility = View.GONE
                binding!!.rlNoBusinessProfile.visibility = View.GONE
                binding!!.rlAddress.visibility = View.GONE
                binding!!.rbPersonalProfile.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding!!.rbPersonalProfile.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_rb_personal_profile_selected)
                binding!!.rbBusinessProfile.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding!!.rbBusinessProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
            }
            R.id.rbBusinessProfile -> {
                setBusinessProfile()
                binding!!.rbPersonalProfile.typeface = regular
                binding!!.rbBusinessProfile.typeface = bold
                binding!!.rbPersonalProfile.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding!!.rbPersonalProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding!!.rbBusinessProfile.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding!!.rbBusinessProfile.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_rb_business_profile_selected)
                binding!!.rlPersonalProfile.visibility = View.GONE
                if (objectResponse != null && objectResponse!!.isBusinessAvail && !TextUtils.isEmpty(objectResponse!!.friendBusinessProfile!!.address))
                    binding!!.rlAddress.visibility = View.GONE
            }
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivFaceBook -> onSocialItemClick(Constants.SOCIAL_FB)
            R.id.ivTwitter -> onSocialItemClick(Constants.SOCIAL_TW)
            R.id.ivLinkedIn -> onSocialItemClick(Constants.SOCIAL_LI)
            R.id.ivInstagram -> onSocialItemClick(Constants.SOCIAL_INSTA)
            R.id.ivFlicker -> onSocialItemClick(Constants.SOCIAL_FLKR)
            R.id.rlMore -> {
                if (!moreDialogFragment!!.isAdded) {
                    moreDialogFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
                }
            }
            R.id.ivMessage -> (HooleyMain.activity as ActivityBase).callFragment(R.id.container, ChatWindowFragment("", Integer.toString(friendId), objectResponse!!.friendGeneralProfile!!.friendBasicInfo!!.fullName!!), "ChatWindowFragment")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }


    private fun showUnFriendAlertDialogue() {
//        val builder = AlertDialog.Builder(HooleyMain.activity)
//        builder.setTitle("Are you sure")
//                .setMessage("Unfriend " + objectResponse!!.friendGeneralProfile!!.friendBasicInfo!!.fullName)
//                .setPositiveButton(android.R.string.yes) { dialog, _ ->
//                    unFriend(friendId)
//                    dialog.dismiss()
//                }
//                .setNegativeButton(android.R.string.no) { dialog, _ -> dialog.dismiss() }
//                .show()
    }

    override fun onClickYes() {
        unFriend(friendId)
    }

    // Calling Api
    private fun getFriendProfile() {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        binding!!.pbProfile.visibility = View.VISIBLE
        service.seeFriendProfile(friendId, object : GenericCallback<FriendProfileModel> {
            override fun success(result: FriendProfileModel) {
                binding!!.pbProfile.visibility = View.GONE
                objectResponse = result
                if (objectResponse!!.isBusinessAvail) {
                    binding!!.rbPersonalProfile.isChecked = true
                    binding!!.rlPersonalProfile.visibility = View.VISIBLE
                    binding!!.rgFriendProfile.visibility = View.VISIBLE
                    setPersonalProfileData()
                } else {
                    binding!!.rlPersonalProfile.visibility = View.VISIBLE
                    binding!!.rgFriendProfile.visibility = View.GONE
                    binding!!.rlAddress.visibility = View.GONE
                    setPersonalProfileData()
                }
            }

            override fun failure(message: String) {
                binding!!.pbProfile.visibility = View.GONE
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun unFriend(friendId: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        service = FriendWebService()
        service.unFriend(friendId, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun blockFriend(friendId: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        service = FriendWebService()
        service.blockFriend(friendId, true, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyApp.db.putString("Block", "Block")
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    companion object {

        lateinit var instance: FriendsProfileFragment
        var friendId: Int = 0
        private var friendName: String? = null

        fun newInstance(id: Int, name: String): FriendsProfileFragment {
            friendId = id
            friendName = name
            instance = FriendsProfileFragment()
            return instance
        }
    }
}


