package com.hooleyapp.hooley.fragments.others

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterMoreMediaDialoge
import com.hooleyapp.hooley.adapters.AdapterUserMorePreferences
import com.hooleyapp.hooley.adapters.AdapterUserPreferences
import com.hooleyapp.hooley.app.view.ui.fragments.friend.BlockUserFragment
import com.hooleyapp.hooley.databinding.FragmentPreferencesBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.UserPreferencesModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.ProfileWebService
import java.util.*

/**
 * Created by Nauman on 6/3/2018.
 */

class PreferencesFragment : BaseFragment(), AdapterUserPreferences.ItemClick, AdapterUserMorePreferences.MoreItemClick {

    var binding: FragmentPreferencesBinding? = null
    private var profileService = ProfileWebService()
    private var moreDialogeFragment: MoreMediaDialogFragment? = null
    private lateinit var infoAdapter: AdapterUserPreferences
    private lateinit var alertsAdapter: AdapterUserPreferences
    private lateinit var privacyAdapter: AdapterUserMorePreferences
    private lateinit var infoList: ArrayList<UserPreferencesModel.PrivacyCheckModel>
    private lateinit var alertsList: ArrayList<UserPreferencesModel.PrivacyCheckModel>
    private lateinit var privacyList: ArrayList<UserPreferencesModel.UserAccessPrivacy>
    internal val arrayList = ArrayList<AdapterMoreMediaDialoge.MoreItem>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_preferences, container, false)
        infoList = ArrayList()
        alertsList = ArrayList()
        privacyList = ArrayList()
        getData()
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding!!.rlBlock.setOnClickListener {
            (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, BlockUserFragment(), "BlockUserFragment")
        }
    }

    private fun setMsgMoreCallerBackListener(accessPrivacyName: String) {
        if (arrayList.size > 0) {
            arrayList.clear()
        }
        arrayList.add(AdapterMoreMediaDialoge.MoreItem("Friends and Event Guests", isEquals("Friends and Event Guests", accessPrivacyName)))
        arrayList.add(AdapterMoreMediaDialoge.MoreItem("Friends Only", isEquals("Friends Only", accessPrivacyName)))
        moreDialogeFragment = MoreMediaDialogFragment.Builder(activity!!).setTitle("Select").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    moreDialogeFragment!!.dismiss()
                    updatePreferences(privacyList[itemPosition].privacyId!!, privacyList[itemPosition].isActive!!, Constants.EVERYONE.toString(), true, "Friends and Event Guests")
                }
                1 -> {
                    moreDialogeFragment!!.dismiss()
                    updatePreferences(privacyList[itemPosition].privacyId!!, privacyList[itemPosition].isActive!!, Constants.FRIENDS.toString(), true, "Friends Only")
                }

            }
        }.create()


        if (!moreDialogeFragment!!.isAdded)
            moreDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)


    }

    private fun setMoreCallerBackListener(accessPrivacyName: String) {
        if (arrayList.size > 0) {
            arrayList.clear()
        }
        arrayList.add(AdapterMoreMediaDialoge.MoreItem("Friends and Event Guests", isEquals("Friends and Event Guests", accessPrivacyName)))
        arrayList.add(AdapterMoreMediaDialoge.MoreItem("Friends Only", isEquals("Friends Only", accessPrivacyName)))
//        arrayList.add(AdapterMoreMediaDialoge.MoreItem("Friends of friend", isEquals("Friends of friend", accessPrivacyName)))
        arrayList.add(AdapterMoreMediaDialoge.MoreItem("No one", isEquals("None", accessPrivacyName)))
        moreDialogeFragment = MoreMediaDialogFragment.Builder(activity!!).setTitle("Select").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    moreDialogeFragment!!.dismiss()
                    updatePreferences(privacyList[itemPosition].privacyId!!, privacyList[itemPosition].isActive!!, Constants.EVERYONE.toString(), true, "Friends and Event Guests")
                }
                1 -> {
                    moreDialogeFragment!!.dismiss()
                    updatePreferences(privacyList[itemPosition].privacyId!!, privacyList[itemPosition].isActive!!, Constants.FRIENDS.toString(), true, "Friends Only")
                }
//                2 -> {
//                    moreDialogeFragment!!.dismiss()
//                    updatePreferences(privacyList[itemPosition].privacyId!!, privacyList[itemPosition].isActive!!, Constants.Friends_of_Friend.toString(), true, "Friends of friend")
//                }
                2 -> {
                    moreDialogeFragment!!.dismiss()
                    updatePreferences(privacyList[itemPosition].privacyId!!, privacyList[itemPosition].isActive!!, Constants.NONE.toString(), true, "None")
                }
            }
        }.create()

        if (!moreDialogeFragment!!.isAdded)
            moreDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)


    }

    private fun setMoreWOFriendsCallerBackListener(accessPrivacyName: String) {
        if (arrayList.size > 0) {
            arrayList.clear()
        }
        arrayList.add(AdapterMoreMediaDialoge.MoreItem("Friends and Event Guests", isEquals("Friends and Event Guests", accessPrivacyName)))
        arrayList.add(AdapterMoreMediaDialoge.MoreItem("Event Guests only", isEquals("Event Guests only", accessPrivacyName)))
        arrayList.add(AdapterMoreMediaDialoge.MoreItem("No one", isEquals("No one", accessPrivacyName)))
        moreDialogeFragment = MoreMediaDialogFragment.Builder(activity!!).setTitle("Select").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    moreDialogeFragment!!.dismiss()
                    updatePreferences(privacyList[itemPosition].privacyId!!, privacyList[itemPosition].isActive!!, Constants.EVERYONE.toString(), true, "Friends and Event Guests")
                }
                1 -> {
                    moreDialogeFragment!!.dismiss()
                    updatePreferences(privacyList[itemPosition].privacyId!!, privacyList[itemPosition].isActive!!, Constants.Friends_of_Friend.toString(), true, "Event Guests only")
                }
                2 -> {
                    moreDialogeFragment!!.dismiss()
                    updatePreferences(privacyList[itemPosition].privacyId!!, privacyList[itemPosition].isActive!!, Constants.NONE.toString(), true, "None")
                }
            }
        }.create()

        if (!moreDialogeFragment!!.isAdded)
            moreDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)


    }

    private fun isEquals(s: String, accessPrivacyName: String): Boolean? {
        if (s.equals(accessPrivacyName)) {
            return true
        }
        return false
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {

        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            try {
                getData()
            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            }

        }
    }

    private lateinit var userPreferenceModel: UserPreferencesModel

    private fun getData() {
        binding!!.pbPreferences.visibility = View.VISIBLE
        profileService.getUserPrivacy(object : GenericCallback<UserPreferencesModel> {
            override fun success(result: UserPreferencesModel) {
                binding!!.pbPreferences.visibility = View.GONE
                userPreferenceModel = result
                binding!!.tvBlockCount.text = "Blocked Users (" + userPreferenceModel.blockCount + ")"
                if (userPreferenceModel.userInfoPrivacy!!.size > 0) {
                    binding!!.vInfo.visibility = VISIBLE
                    setInfoAdapter(result)
                }

                if (userPreferenceModel.userAlertsPrivacy!!.size > 0) {
                    binding!!.vAlerts.visibility = VISIBLE
                    setAlertAdapter(result)
                }

                if (userPreferenceModel.userAccessPrivacy!!.size > 0) {
                    setMoreAdapter(result)
                }
            }

            override fun failure(message: String) {
                binding!!.pbPreferences.visibility = View.GONE
                HooleyMain.activity!!.showSuccessDialog(message)
            }

        })
    }

    private fun setInfoAdapter(result: UserPreferencesModel) {
        infoList.addAll(result.userInfoPrivacy!!)
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        binding!!.rluserInfoPrivacy.layoutManager = manager
        binding!!.rluserInfoPrivacy.layoutAnimation = animation
        infoAdapter = AdapterUserPreferences(HooleyMain.activity!!, result.userInfoPrivacy, true)
        infoAdapter.mListener = this
        binding!!.rluserInfoPrivacy.adapter = infoAdapter

    }

    private fun setAlertAdapter(result: UserPreferencesModel) {
        alertsList.addAll(result.userAlertsPrivacy!!)
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        binding!!.rluserAlertsPrivacy.layoutManager = manager
        binding!!.rluserAlertsPrivacy.layoutAnimation = animation
        alertsAdapter = AdapterUserPreferences(HooleyMain.activity!!, result.userAlertsPrivacy, false)
        alertsAdapter.mListener = this
        binding!!.rluserAlertsPrivacy.adapter = alertsAdapter

    }

    private fun setMoreAdapter(result: UserPreferencesModel) {
        privacyList.addAll(result.userAccessPrivacy!!)
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        binding!!.rluserAccessPrivacy.layoutManager = manager
        binding!!.rluserAccessPrivacy.layoutAnimation = animation
        privacyAdapter = AdapterUserMorePreferences(HooleyMain.activity!!, result.userAccessPrivacy)
        privacyAdapter.mListener = this
        binding!!.rluserAccessPrivacy.adapter = privacyAdapter

    }

    private var itemPosition: Int = 0

    private var isFrom: Boolean = true

    override fun onCheckClick(positon: Int, item: UserPreferencesModel.PrivacyCheckModel, fromInfo: Boolean) {
        itemPosition = positon
        isFrom = fromInfo
        updatePreferences(item.privacyId!!, !item.isActive!!, item.accessPrivacyId.toString(), false, "")
    }


    override fun onMoreItemClick(positon: Int, item: UserPreferencesModel.UserAccessPrivacy) {
        itemPosition = positon
        when {
            item.name!!.contains("requests") -> setMoreWOFriendsCallerBackListener(item.accessPrivacyName!!)
            item.name!!.contains("message") -> setMsgMoreCallerBackListener(item.accessPrivacyName!!)
            else -> setMoreCallerBackListener(item.accessPrivacyName!!)
        }
    }

    private fun updatePreferences(privacyId: Integer, isChecked: Boolean, accessPrivacyId: String, hasOptions: Boolean, selectedOption: String) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        HooleyMain.activity!!.showDialog()
        profileService.updateUserPreferences(privacyId.toString(), isChecked, accessPrivacyId, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                if (hasOptions) {
                    privacyList[itemPosition].accessPrivacyName = selectedOption
                    privacyAdapter.notifyDataSetChanged()
                } else {
                    if (isFrom) {
                        infoList[itemPosition].isActive = !infoList[itemPosition].isActive!!
                        infoAdapter.notifyDataSetChanged()
                    } else {
                        alertsList[itemPosition].isActive = !alertsList[itemPosition].isActive!!
                        alertsAdapter.notifyDataSetChanged()
                    }
                }
                Util.showToastMessage(HooleyMain.activity!!, "Updated")
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    Log.d("ERROR", "ERROR:$message")
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }


    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }


}
