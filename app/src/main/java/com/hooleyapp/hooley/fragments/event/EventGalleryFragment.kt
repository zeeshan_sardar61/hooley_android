package com.hooleyapp.hooley.fragments.event

import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.AnimationUtils
import android.widget.*
import com.felipecsl.asymmetricgridview.AsymmetricRecyclerViewAdapter
import com.felipecsl.asymmetricgridview.Utils
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterMoreMediaDialoge
import com.hooleyapp.hooley.adapters.AdapterSpinner
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.app.view.adapter.EventFeedTrendingAdapter
import com.hooleyapp.hooley.app.view.adapter.EventGalleryAdapter
import com.hooleyapp.hooley.app.viewmodel.event.EventGalleryViewModel
import com.hooleyapp.hooley.databinding.FragmentEventGalleryBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.event.LiveEventFragment.Companion.fragmentLiveEventbinding
import com.hooleyapp.hooley.fragments.media.AllFeedsSinglePostCommentFragment
import com.hooleyapp.hooley.fragments.media.ViewPostMediaFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.fragments.others.MoreMediaDialogFragment
import com.hooleyapp.hooley.interfaces.IBuyPPV
import com.hooleyapp.hooley.interfaces.IEventAllFeedsClick
import com.hooleyapp.hooley.model.PersonalProfileModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.GetBitmapTask
import com.hooleyapp.hooley.others.MessageEvent
import com.hooleyapp.hooley.others.SpacesItemDecoration
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * Created by Zeeshan on 18-Jan-18.
 */

class EventGalleryFragment : BaseFragment(), RadioGroup.OnCheckedChangeListener, IEventAllFeedsClick, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    lateinit var binding: FragmentEventGalleryBinding
    lateinit var eventGalleryViewModel: EventGalleryViewModel
    private var adapterAllEventFeed: EventGalleryAdapter? = null
    private var adapterEventFeedTrending: EventFeedTrendingAdapter? = null
    private var moreTrendingDialogeFragment: MoreMediaDialogFragment? = null
    private var morePostDialogeFragment: MoreDialogeFragment? = null
    private var showMediaReportEventDialog: Dialog? = null
    private var spinnerAdapter: AdapterSpinner? = null
    private var reasonMediaItemPosition = 0
    private var allFeedItemPosition = 0
    private var pagerPosition = 0
    private val arrayList = ArrayList<AdapterMoreMediaDialoge.MoreItem>()
    private lateinit var imageId: String
    private var type = Constants.EVENT_GALLERY_ALL
    private var boldFont: Typeface = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)!!
    private var regularFont: Typeface = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)!!
    lateinit var mListener: IBuyPPV
    var list = ArrayList<EventGalleryFeedModel.EventMediaInfo>()

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser && isResumed)
            if (::eventGalleryViewModel.isInitialized)
                eventGalleryViewModel.getEventGalleryFeeds(LiveEventFragment.eventId, Constants.EVENT_GALLERY_ALL)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_event_gallery, container, false)
        eventGalleryViewModel = ViewModelProviders.of(this).get(EventGalleryViewModel::class.java)
        setListener()
        setUiObserver()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setMoreCallerBackListener()
        setAdapter()
        binding.rbAll.typeface = boldFont
        binding.rbAll.isChecked = true

        if (!LiveEventFragment.objectResult.isPpvPurchased && LiveEventFragment.objectResult.ppvPrice > 0.0) {
            if (!LiveEventFragment.objectResult.isMyEvent) {
                binding.llppvView.visibility = View.VISIBLE
                binding.tvEventPrice.text = "$" + LiveEventFragment.objectResult.ppvPrice.toInt().toString()
            } else {
                binding.llppvView.visibility = View.GONE
            }
        } else {
            binding.llppvView.visibility = View.GONE
        }

        binding.btnBuyNow.setOnClickListener {
            mListener.onBuyPPV()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        eventGalleryViewModel.shareEventMedia(LiveEventFragment.eventId, imageId, event.message, allFeedItemPosition)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    fun setUiObserver() {
        eventGalleryViewModel.sessionExpire.observe(viewLifecycleOwner, Observer { onTokenExpireLogOut() })
        eventGalleryViewModel.failureMessage.observe(viewLifecycleOwner, Observer {
            binding.tvNoData.text = it!!
        })
        eventGalleryViewModel.loading.observe(viewLifecycleOwner, Observer {
            if (it!!) binding.pbAll.visibility = View.VISIBLE else binding.pbAll.visibility = View.GONE
        }
        )

        eventGalleryViewModel.mList.observe(viewLifecycleOwner, Observer {
            if (binding.slGallery.isRefreshing)
                binding.slGallery.isRefreshing = false

            it.let { mList ->
                list.clear()
                list.addAll(mList!!)
                if (type == Constants.EVENT_GALLERY_TRENDING) {
                    setTrendingAdapter(list)
                } else {
                    adapterAllEventFeed?.setList(list)
                }
            }
        })
        eventGalleryViewModel.reportEventList.observe(viewLifecycleOwner, Observer {
            it.let { list ->
                showReportEventDialog(ArrayList(list!!), imageId)
            }
        })

    }

    private fun setListener() {
        binding.rankTab.setOnCheckedChangeListener(this)
        binding.slGallery.setOnRefreshListener(this)
        binding.ivFilter.setOnClickListener(this)
    }

    // set More menu with options
    private fun setMoreCallerBackListener() {
        if (arrayList.size > 0)
            arrayList.clear()
        val itemAll = AdapterMoreMediaDialoge.MoreItem("All", true)
        val itemFavorites = AdapterMoreMediaDialoge.MoreItem("Favorites", false)
        val itemPhotos = AdapterMoreMediaDialoge.MoreItem("Photos", false)
        val itemVideos = AdapterMoreMediaDialoge.MoreItem("Videos", false)
        arrayList.add(itemAll)
        arrayList.add(itemFavorites)
        arrayList.add(itemPhotos)
        arrayList.add(itemVideos)
        if (moreTrendingDialogeFragment != null && !moreTrendingDialogeFragment!!.isDetached)
            moreTrendingDialogeFragment!!.onDetach()
        moreTrendingDialogeFragment = MoreMediaDialogFragment.Builder(HooleyMain.activity!!).setTitle("Filter by").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    if (list.size > 0) {
                        eventGalleryViewModel.filterByAll()
                        arrayList[0].isCheck = (true)
                        arrayList[1].isCheck = (false)
                        arrayList[2].isCheck = (false)
                        arrayList[3].isCheck = (false)
                    }
                    moreTrendingDialogeFragment!!.dismiss()
                }
                1 -> {
                    if (list.size > 0) {
                        arrayList[0].isCheck = (false)
                        arrayList[1].isCheck = (true)
                        arrayList[2].isCheck = (false)
                        arrayList[3].isCheck = (false)
                        eventGalleryViewModel.filterByFav()
                    }
                    moreTrendingDialogeFragment!!.dismiss()
                }
                2 -> {
                    if (list.size > 0) {
                        arrayList[0].isCheck = (false)
                        arrayList[1].isCheck = (false)
                        arrayList[2].isCheck = (true)
                        arrayList[3].isCheck = (false)
                        eventGalleryViewModel.filterByPhotos()
                    }
                    moreTrendingDialogeFragment!!.dismiss()
                }
                3 -> {
                    if (list.size > 0) {
                        arrayList[0].isCheck = (false)
                        arrayList[1].isCheck = (false)
                        arrayList[2].isCheck = (false)
                        arrayList[3].isCheck = (true)
                        eventGalleryViewModel.filterByVideo()
                    }
                    moreTrendingDialogeFragment!!.dismiss()
                }
            }
        }.create()
    }

    // Setting Adapters
    fun setAdapter() {
        binding.rvAll.visibility = View.VISIBLE
        binding.rvTrending.visibility = View.GONE
        binding.rvFriends.visibility = View.GONE
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_from_right)
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding.rvAll.layoutAnimation = animation
        binding.rvAll.layoutManager = manager
        binding.rvAll.setEmptyView(binding.tvNoData)
        adapterAllEventFeed = EventGalleryAdapter()
        adapterAllEventFeed!!.mListener = this
        binding.rvAll.adapter = adapterAllEventFeed
    }

    private fun setTrendingAdapter(mList: ArrayList<EventGalleryFeedModel.EventMediaInfo>) {
        binding.rvAll.visibility = View.GONE
        binding.rvTrending.visibility = View.VISIBLE
        binding.rvFriends.visibility = View.GONE
        binding.rvTrending.setRequestedColumnCount(3)
        binding.rvTrending.isDebugging = false
        binding.rvTrending.requestedHorizontalSpacing = Utils.dpToPx(HooleyMain.activity!!, 5f)
        if (adapterEventFeedTrending == null)
            binding.rvTrending.addItemDecoration(SpacesItemDecoration(HooleyMain.activity!!.resources.getDimensionPixelSize(R.dimen.recycler_padding)))
        adapterEventFeedTrending = EventFeedTrendingAdapter(mList)
        adapterEventFeedTrending!!.mListener = this
        binding.rvTrending.adapter = AsymmetricRecyclerViewAdapter<RecyclerView.ViewHolder>(HooleyMain.activity, binding.rvTrending, adapterEventFeedTrending!!)
    }

    fun setTabs() {
        when (type) {
            Constants.EVENT_GALLERY_ALL -> {
                binding.rbAll.typeface = boldFont
                binding.rbFriends.typeface = regularFont
                binding.rbTrending.typeface = regularFont
                binding.rvAll.visibility = View.GONE
                binding.rvTrending.visibility = View.GONE
                binding.rvFriends.visibility = View.GONE
                if (userVisibleHint)
                    eventGalleryViewModel.getEventGalleryFeeds(LiveEventFragment.eventId, type)
                binding.rbAll.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding.rbAll.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_left_radio)
                binding.rbFriends.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding.rbTrending.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbTrending.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
            }

            Constants.EVENT_GALLERY_TRENDING -> {
                setMoreCallerBackListener()
                binding.rbAll.typeface = regularFont
                binding.rbFriends.typeface = regularFont
                binding.rbTrending.typeface = boldFont
                binding.rvAll.visibility = View.GONE
                binding.rvTrending.visibility = View.GONE
                binding.rvFriends.visibility = View.GONE
                eventGalleryViewModel.getEventGalleryFeeds(LiveEventFragment.eventId, type)
                binding.rbTrending.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding.rbTrending.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_rb_default)
                binding.rbAll.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbAll.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding.rbFriends.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
            }

            Constants.EVENT_GALLERY_FRIEND -> {
                setMoreCallerBackListener()
                binding.rbAll.typeface = regularFont
                binding.rbFriends.typeface = boldFont
                binding.rbTrending.typeface = regularFont
                binding.rvAll.visibility = View.GONE
                binding.rvTrending.visibility = View.GONE
                binding.rvFriends.visibility = View.GONE
                eventGalleryViewModel.getEventGalleryFeeds(LiveEventFragment.eventId, type)
                binding.rbFriends.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding.rbFriends.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_right_radio)
                binding.rbAll.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbAll.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding.rbTrending.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbTrending.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
            }

        }
    }

    private fun showShareEventDialog() {
        object : GetBitmapTask() {
            override fun onPostExecute(bmp: Bitmap) {
                super.onPostExecute(bmp)
                (HooleyMain.activity as HooleyMain).shareWithImage(bmp, eventGalleryViewModel.mList.value!![allFeedItemPosition].eventName!!, eventGalleryViewModel.mList.value!![allFeedItemPosition].imageCaption, eventGalleryViewModel.mList.value!![allFeedItemPosition].eventId!!, eventGalleryViewModel.mList.value!![allFeedItemPosition].imageId)
            }
        }.execute(eventGalleryViewModel.mList.value!![allFeedItemPosition].mediaFiles[pagerPosition].thumbnailUrl)
    }

    private fun showReportEventDialog(list: ArrayList<PersonalProfileModel.myObject>, mediaId: String) {
        showMediaReportEventDialog = Dialog(HooleyMain.activity!!)
        showMediaReportEventDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        showMediaReportEventDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        showMediaReportEventDialog!!.setContentView(R.layout.dialog_report_event)
        val btnReport = showMediaReportEventDialog!!.findViewById<TextView>(R.id.btnReport)
        val edtMessage = showMediaReportEventDialog!!.findViewById<EditText>(R.id.edtMessage)
        val spSelectReason = showMediaReportEventDialog!!.findViewById<Spinner>(R.id.spSelectReason)
        spinnerAdapter = AdapterSpinner(activity, R.layout.sp_gender_group_item, list)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spSelectReason.adapter = spinnerAdapter
        spSelectReason.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                reasonMediaItemPosition = position
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
        btnReport.setOnClickListener {
            showMediaReportEventDialog!!.dismiss()
            eventGalleryViewModel.submitReport(LiveEventFragment.eventId, list[reasonMediaItemPosition].id!!, edtMessage.text.toString(), mediaId)
        }
        if (!showMediaReportEventDialog!!.isShowing) {
            showMediaReportEventDialog!!.show()
        }

    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        when (checkedId) {
            R.id.rbAll -> {
                type = Constants.EVENT_GALLERY_ALL
                setTabs()
            }
            R.id.rbFriends -> {
                type = Constants.EVENT_GALLERY_FRIEND
                setTabs()
            }
            R.id.rbTrending -> {
                type = Constants.EVENT_GALLERY_TRENDING
                setTabs()
            }
        }
    }

    override fun onClickPostComment(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, type: Int) {
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, AllFeedsSinglePostCommentFragment.newInstance(LiveEventFragment.eventId, eventMediaInfo.imageId), "AllFeedsSinglePostCommentFragment")
    }

    override fun onClickMore(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, mpos: Int, mlistPosition: Int, mType: Int) {
        val arrayList = ArrayList<String>()
        if (eventMediaInfo.isFavorited)
            arrayList.add("Remove From Favorite")
        else
            arrayList.add("Add to Favorite")

        if (eventMediaInfo.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
            arrayList.add("Edit Photo")

        arrayList.add("Download/Share Photo")

        if (eventMediaInfo.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
            arrayList.add("Delete Photo")
        else
            arrayList.add("Report Photo")
        morePostDialogeFragment = MoreDialogeFragment.Builder(activity!!).setTitle("More").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    eventGalleryViewModel.mediaFav(LiveEventFragment.eventId, eventMediaInfo.imageId, !eventMediaInfo.isFavorited)
                    morePostDialogeFragment!!.dismiss()
                }
                1 -> {
                    morePostDialogeFragment!!.dismiss()
                    if (eventMediaInfo.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
                        (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, UpdatePostFragment(eventMediaInfo.eventId!!, eventMediaInfo.imageId), "UpdatePostFragment")
                    else {
                        morePostDialogeFragment!!.dismiss()
                        onClickShare(eventMediaInfo.imageId, mpos, mlistPosition, mType)
                    }
                }
                2 -> {
                    morePostDialogeFragment!!.dismiss()
                    if (arrayList.size == 3) {
                        imageId = eventMediaInfo.imageId
                        eventGalleryViewModel.getMediaReportEvent()
                    } else {
                        onClickShare(eventMediaInfo.imageId, mpos, mlistPosition, mType)
                    }
                }
                3 -> {
                    morePostDialogeFragment!!.dismiss()
                    if (eventMediaInfo.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
                        eventGalleryViewModel.deleteMedia(eventMediaInfo.imageId, LiveEventFragment.eventId, mpos)
                    else {
                        imageId = eventMediaInfo.imageId
                        eventGalleryViewModel.getMediaReportEvent()
                    }
                }
            }
        }.create()

        if (!morePostDialogeFragment!!.isAdded)
            morePostDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
    }

    override fun onClickImageView(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo) {
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ViewPostMediaFragment.newInstance(eventMediaInfo), "ViewPostMediaFragment")
    }

    override fun onClickLikePost(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, position: Int, type: Int) {
        eventGalleryViewModel.likePost(LiveEventFragment.eventId, eventMediaInfo.imageId)
    }

    override fun onClickFav(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, position: Int, type: Int) {
        eventGalleryViewModel.mediaFav(LiveEventFragment.eventId, eventMediaInfo.imageId, !eventMediaInfo.isFavorited)

    }

    override fun onClickShare(imId: String, pos: Int, pagerPos: Int, type: Int) {
        imageId = imId
        allFeedItemPosition = pos
        pagerPosition = pagerPos
        showShareEventDialog()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivFilter -> {
                if (!moreTrendingDialogeFragment!!.isAdded)
                    moreTrendingDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (::binding.isInitialized) {
            when (type) {
                Constants.EVENT_GALLERY_ALL -> binding.rbAll.isChecked = true
                Constants.EVENT_GALLERY_TRENDING -> binding.rbTrending.isChecked = true
                Constants.EVENT_GALLERY_FRIEND -> binding.rbFriends.isChecked = true
            }
        }
    }

    override fun onRefresh() {
        binding.slGallery.isRefreshing = true
        binding.pbAll.visibility = View.GONE
        binding.pbFriends.visibility = View.GONE
        binding.pbTrending.visibility = View.GONE
        fragmentLiveEventbinding!!.ivGalleryLiveCircle.visibility = View.GONE
        HooleyApp.db.putBoolean(Constants.SHOW_GALLERY_ALERT, false)
        when {
            binding.rbAll.isChecked -> eventGalleryViewModel.getEventGalleryFeeds(LiveEventFragment.eventId, Constants.EVENT_GALLERY_ALL)
            binding.rbTrending.isChecked -> eventGalleryViewModel.getEventGalleryFeeds(LiveEventFragment.eventId, Constants.EVENT_GALLERY_TRENDING)
            else -> eventGalleryViewModel.getEventGalleryFeeds(LiveEventFragment.eventId, Constants.EVENT_GALLERY_FRIEND)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
    }

}
