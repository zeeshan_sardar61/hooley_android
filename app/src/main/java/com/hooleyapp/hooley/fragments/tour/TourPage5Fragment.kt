package com.hooleyapp.hooley.fragments.tour

import android.animation.Animator
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.Splash
import com.hooleyapp.hooley.databinding.FragmentConnectTourBinding
import com.hooleyapp.hooley.fragments.BaseFragment

/**
 * Created by Nauman on 2/28/2018.
 */

class TourPage5Fragment : BaseFragment(), Animator.AnimatorListener {

    internal var binding: FragmentConnectTourBinding? = null

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            if (view != null) {
                binding!!.lavConnectLoop.visibility = View.GONE
                binding!!.lavConnect.visibility = View.VISIBLE
                binding!!.lavConnect.clearAnimation()
                binding!!.lavConnect.setAnimation(Splash.activity.resources.getIdentifier("connect", "raw", Splash.activity.packageName))
                binding!!.lavConnect.playAnimation()
            }
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_connect_tour, container, false)
        setListener()
        return binding!!.root
    }

    private fun setListener() {
        binding!!.lavConnect.addAnimatorListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onAnimationStart(animator: Animator) {

    }

    override fun onAnimationEnd(animator: Animator) {
        binding!!.lavConnect.visibility = View.GONE
        binding!!.lavConnectLoop.visibility = View.VISIBLE

    }

    override fun onAnimationCancel(animator: Animator) {

    }

    override fun onAnimationRepeat(animator: Animator) {

    }
}
