package com.hooleyapp.hooley.fragments.media

import android.app.Dialog
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterPostComment
import com.hooleyapp.hooley.adapters.AdapterSpinner
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventDetailModel
import com.hooleyapp.hooley.databinding.FragmentAllFeedsSinglePostCommentBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.event.UpdatePostFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.helper.hideKeyboard
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GetReportEventModel
import com.hooleyapp.hooley.model.PersonalProfileModel
import com.hooleyapp.hooley.model.PostImageCommentModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.GetBitmapTask
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.MediaWebService
import java.util.*

/**
 * Created by Zeeshan on 22-Jan-18.
 */

class AllFeedsSinglePostCommentFragment : BaseFragment(), View.OnClickListener, AdapterPostComment.CommentClickListener {
    internal var binding: FragmentAllFeedsSinglePostCommentBinding? = null
    internal lateinit var objectResponse: PostImageCommentModel
    private var adapterPostComment: AdapterPostComment? = null
    private var morePostDialogeFragment: MoreDialogeFragment? = null
    private var showMediaReportEventDialog: Dialog? = null
    private var spinnerAdapter: AdapterSpinner? = null
    private var reasonMediaItemPosition = 0
    private var mediaService: MediaWebService? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_all_feeds_single_post_comment, container, false)
        HooleyMain.activity!!.supportActionBar!!.show()
        (HooleyMain.activity as HooleyMain).removeBubbleView()
        //        setHasOptionsMenu(true);
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
        getSinglePostComments(eventId, imageId)
    }

    private fun setListener() {
        binding!!.btnPostComment.setOnClickListener(this)
    }

    private fun setCommentRv() {
        setToolBar()
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding!!.rvComments.layoutManager = manager
        binding!!.rvComments.layoutAnimation = animation
        adapterPostComment = AdapterPostComment(HooleyMain.activity!!, objectResponse.postComments, objectResponse.imageInfo!!, objectResponse.tagFriends)
        adapterPostComment!!.setMListener(this)
        binding!!.rvComments.adapter = adapterPostComment
        binding!!.rvComments.setHasFixedSize(true)
    }

    fun setToolBar() {
        try {
            (HooleyMain.activity as HooleyMain).setToolbarTitle(objectResponse.imageInfo!!.eventName!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    fun addSingleCommentToList(commentText: String, position: Int) {
        val comments = EventDetailModel.Comments()
        comments.commentText = commentText
        comments.commentTime = "Now"
        comments.fullName = HooleyApp.db.getString(Constants.FULL_NAME)
        comments.profilePic = HooleyApp.db.getString(Constants.USER_AVATAR)
        comments.userId = HooleyApp.db.getString(Constants.USER_ID)
        if (objectResponse.postComments != null)
            objectResponse.postComments.add(position, comments)
        else {
            objectResponse.postComments = ArrayList<EventDetailModel.Comments>()
            objectResponse.postComments.add(position, comments)
            adapterPostComment = null
            objectResponse.imageInfo!!.commentsCount = objectResponse.imageInfo!!.commentsCount + 1
            setCommentRv()
            return
        }
        binding!!.rvComments.scrollToPosition(position)
        objectResponse.imageInfo!!.commentsCount = objectResponse.imageInfo!!.commentsCount + 1
        adapterPostComment!!.notifyDataSetChanged()
        Util.hideKeyboard(HooleyMain.activity!!)
    }

    private fun showReportEventDialog(list: ArrayList<PersonalProfileModel.myObject>, mediaId: String) {
        showMediaReportEventDialog = Dialog(HooleyMain.activity!!)
        showMediaReportEventDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        showMediaReportEventDialog!!.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        showMediaReportEventDialog!!.setContentView(R.layout.dialog_report_event)
        val btnReport = showMediaReportEventDialog!!.findViewById<TextView>(R.id.btnReport)
        val edtMessage = showMediaReportEventDialog!!.findViewById<EditText>(R.id.edtMessage)
        val spSelectReason = showMediaReportEventDialog!!.findViewById<Spinner>(R.id.spSelectReason)
        spinnerAdapter = AdapterSpinner(activity, R.layout.sp_gender_group_item, list)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spSelectReason.adapter = spinnerAdapter
        spSelectReason.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                reasonMediaItemPosition = position
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
        btnReport.setOnClickListener {
            showMediaReportEventDialog!!.dismiss()
            SubmitReport(list[reasonMediaItemPosition].id, edtMessage.text.toString(), mediaId)
        }
        showMediaReportEventDialog!!.show()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnPostComment -> if (binding!!.edtComments.text.toString().trim { it <= ' ' }.isEmpty()) {
                binding!!.edtComments.requestFocus()
                HooleyMain.activity!!.showSuccessDialog(getString(R.string.required))
                return
            } else {
                HooleyMain.activity!!.hideKeyboard()
                binding!!.btnPostComment.isClickable = false
                commentOnPost(eventId, objectResponse.imageInfo!!.imageId, binding!!.edtComments.text.toString())
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.root
        Util.hideKeyboard(HooleyMain.activity!!)
    }

    override fun onClickLike() {
        if (objectResponse.imageInfo!!.isLiked) {
            objectResponse.imageInfo!!.isLiked = false
            objectResponse.imageInfo!!.likesCount = objectResponse.imageInfo!!.likesCount - 1
        } else {
            objectResponse.imageInfo!!.isLiked = true
            objectResponse.imageInfo!!.likesCount = objectResponse.imageInfo!!.likesCount + 1
        }
        if (adapterPostComment != null)
            adapterPostComment!!.notifyDataSetChanged()
        likePost(eventId, objectResponse.imageInfo!!.imageId)
    }

    override fun onclickShare() {
        showShareEventDialog()
    }

    override fun onClickMore() {
        if (!morePostDialogeFragment!!.isAdded)
            morePostDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
    }

    fun setMoreCallBackListener() {
        val arrayList = ArrayList<String>()

        if (objectResponse.imageInfo!!.isFavorited)
            arrayList.add("Remove From Favorite")
        else
            arrayList.add("Add to Favorites")

        if (objectResponse.imageInfo!!.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
            arrayList.add("Edit Photo")

        arrayList.add("Download/Share Photo")

        if (objectResponse.imageInfo!!.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
            arrayList.add("Delete Photo")
        else
            arrayList.add("Report Photo")


        morePostDialogeFragment = MoreDialogeFragment.Builder(activity!!).setTitle("More").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, type ->
            when (position) {
                //                        mediaFav(LiveEventFragment.eventId, eventMediaInfo.imageId, !eventMediaInfo.isFavorited, mpos, mType);

                0 -> {
                    mediaFav(objectResponse.imageInfo!!.eventId, objectResponse.imageInfo!!.imageId, !objectResponse.imageInfo!!.isFavorited)

                    if (objectResponse.imageInfo!!.isFavorited) {
                        morePostDialogeFragment!!.dismiss()
                        arrayList[0] = "Add to Favorites"

                    } else {
                        morePostDialogeFragment!!.dismiss()
                        arrayList[0] = "Remove From Favorite"
                    }
                }
                1 -> {
                    morePostDialogeFragment!!.dismiss()
                    if (objectResponse.imageInfo!!.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
                        (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, UpdatePostFragment(eventId, objectResponse.imageInfo!!.imageId), "UpdatePostFragment")
                    else
                        getMediaReportEvent(objectResponse.imageInfo!!.imageId)
                }
                2 -> {
                    morePostDialogeFragment!!.dismiss()
                    showShareDialog()
                }
                3 -> {
                    morePostDialogeFragment!!.dismiss()
                    if (objectResponse.imageInfo!!.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
                        deleteMedia(objectResponse.imageInfo!!.imageId)
                    else
                        getMediaReportEvent(objectResponse.imageInfo!!.imageId)

                }
            }
        }.create()
    }

    private fun showShareDialog() {
        object : GetBitmapTask() {
            override fun onPostExecute(bmp: Bitmap) {
                super.onPostExecute(bmp)
                (HooleyMain.activity as HooleyMain).shareWithImage(bmp, objectResponse.imageInfo!!.eventName!!, objectResponse.imageInfo!!.imageCaption, objectResponse.imageInfo!!.eventId!!, objectResponse.imageInfo!!.imageId)
            }
        }.execute(objectResponse.imageInfo!!.mediaFiles[0].thumbnailUrl)
    }


    private fun deleteMedia(imageId: String) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        mediaService!!.deleteMedia(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
            }

            override fun failure(message: String) {
                try {

                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        }, imageId, objectResponse.imageInfo!!.eventId!!)
    }


    private fun getMediaReportEvent(mediaId: String) {
        mediaService!!.getMediaReportEvent(object : GenericCallback<GetReportEventModel> {
            override fun success(result: GetReportEventModel) {
                showReportEventDialog(result.mediaReportReasonsArrayList, mediaId)
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    override fun onClickFav() {
        if (!objectResponse.imageInfo!!.isFavorited) {
            objectResponse.imageInfo!!.favoriteCount = objectResponse.imageInfo!!.favoriteCount + 1
        } else {
            objectResponse.imageInfo!!.favoriteCount = objectResponse.imageInfo!!.favoriteCount - 1
        }

        mediaFav(eventId, objectResponse.imageInfo!!.imageId, !objectResponse.imageInfo!!.isFavorited)
    }

    override fun onClickImage() {
        Util.hideKeyboard(HooleyMain.activity!!)
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ViewPostMediaFragment.newInstance(objectResponse.imageInfo!!, true), "ViewPostMediaFragment")
    }

    private fun showShareEventDialog() {
        object : GetBitmapTask() {
            override fun onPostExecute(bmp: Bitmap) {
                super.onPostExecute(bmp)
                (HooleyMain.activity as HooleyMain).shareWithImage(bmp, objectResponse.imageInfo!!.eventName!!, objectResponse.imageInfo!!.imageCaption, objectResponse.imageInfo!!.eventId!!, objectResponse.imageInfo!!.imageId)
            }
        }.execute(objectResponse.imageInfo!!.mediaFiles[0].thumbnailUrl)
    }


    private fun likePost(eventId: String, imageId: String) {

        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        if (mediaService == null)
            mediaService = MediaWebService()
        mediaService!!.likePost(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {

            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        }, eventId, imageId)
    }

    private fun mediaFav(eventId: String?, imageId: String, isFav: Boolean) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        if (mediaService == null)
            mediaService = MediaWebService()
        mediaService!!.mediaFav(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                objectResponse.imageInfo!!.isFavorited = isFav
                if (isFav)
                    objectResponse.imageInfo!!.favoriteCount++
                else
                    objectResponse.imageInfo!!.favoriteCount--
                if (adapterPostComment != null)
                    adapterPostComment!!.notifyDataSetChanged()
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        }, eventId!!, imageId, isFav)


    }

    private fun getSinglePostComments(eventId: String, imageId: String) {
        if (binding != null)
            binding!!.pbComment.visibility = View.VISIBLE

        mediaService = MediaWebService()
        mediaService!!.getPostComments(eventId, imageId, object : GenericCallback<PostImageCommentModel> {
            override fun success(result: PostImageCommentModel) {
                binding!!.pbComment.visibility = View.GONE
                objectResponse = result
                try {
                    val comments = EventDetailModel.Comments()
                    if (objectResponse.postComments != null)
                        objectResponse.postComments.add(0, comments)
                    else {
                        objectResponse.postComments = ArrayList<EventDetailModel.Comments>()
                        objectResponse.postComments.add(0, comments)
                    }
                    setCommentRv()
                    setMoreCallBackListener()

                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun failure(message: String) {
                binding!!.pbComment.visibility = View.GONE
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }

    private fun commentOnPost(eventId: String, imageId: String, commentText: String) {
        if (binding != null)
            binding!!.pbComment.visibility = View.VISIBLE

        mediaService = MediaWebService()
        mediaService!!.commentOnPost(eventId, imageId, commentText, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                binding!!.pbComment.visibility = View.GONE
                binding!!.edtComments.setText("")
                addSingleCommentToList(commentText, objectResponse.postComments.size)
                binding!!.tvNoData.visibility = View.GONE
                binding!!.btnPostComment.isClickable = true
            }

            override fun failure(message: String) {
                binding!!.pbComment.visibility = View.GONE
                binding!!.btnPostComment.isClickable = true
                try {
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.rvComments.visibility = View.GONE
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })


    }

    private fun SubmitReport(reasonId: String?, reportMessage: String, mediaId: String) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        if (mediaService == null)
            mediaService = MediaWebService()
        mediaService!!.submitReport(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                Util.showToastMessage(HooleyMain.activity!!, "Reported")
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        }, reasonId!!, reportMessage, mediaId, eventId)
    }

    companion object {


        private lateinit var instance: AllFeedsSinglePostCommentFragment
        private var eventId = ""
        private var imageId = ""

        fun newInstance(evtId: String, imgId: String): AllFeedsSinglePostCommentFragment {
            eventId = evtId
            imageId = imgId
            instance = AllFeedsSinglePostCommentFragment()
            return instance
        }
    }

}
