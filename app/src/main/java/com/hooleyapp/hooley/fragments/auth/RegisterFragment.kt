package com.hooleyapp.hooley.fragments.auth

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyAuth
import com.hooleyapp.hooley.app.view.ui.fragments.others.PrivacyPolicyFragment
import com.hooleyapp.hooley.databinding.FragmentRegisterBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.others.TermsAndConditionFragment
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.AuthWebService

/**
 * Created by Nauman on 11/30/2017.
 */

class RegisterFragment : BaseFragment(), View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    var binding: FragmentRegisterBinding? = null
    var service = AuthWebService()
    internal var profilePic = ""
    internal var socialMediaType = "hooley"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false)
        //        checkType();
        binding!!.ivBack.setOnClickListener {
            HooleyAuth.activity.onBackPressed()
        }
        setListener()
        return binding!!.root
    }


    private fun setListener() {
        binding!!.btnSignUp.setOnClickListener(this)
        binding!!.tvTerms.setOnClickListener(this)
        binding!!.tvPrivacy.setOnClickListener(this)
        binding!!.tvSignIn.setOnClickListener(this)
        binding!!.cbViewPass.setOnCheckedChangeListener(this)
    }


    private fun validateInput(): Boolean {
        if (Util.isContentNull(binding!!.edFirstName.text.toString())) {
            binding!!.edFirstName.requestFocus()
            binding!!.edFirstName.error = getString(R.string.str_first_name_error)
            return false
        } else if (Util.isContentNull(binding!!.edLastName.text.toString())) {
            binding!!.edLastName.requestFocus()
            binding!!.edLastName.error = getString(R.string.str_last_name_error)
            return false
        } else if (Util.isContentNull(binding!!.edEmail.text.toString())) {
            binding!!.edEmail.requestFocus()
            binding!!.edEmail.error = getString(R.string.str_error_email)
            return false
        } else if (!Util.isEmailValid(binding!!.edEmail.text.toString().trim())) {
            binding!!.edEmail.requestFocus()
            binding!!.edEmail.error = getString(R.string.invalid_email)
            return false
        } else if (Util.isContentNull(binding!!.edPass.text.toString())) {
            binding!!.edPass.requestFocus()
            binding!!.edPass.error = getString(R.string.str_password_error)
            return false
        } else if (binding!!.edPass.text.toString().length < 8) {
            binding!!.edPass.requestFocus()
            binding!!.tvPassword.visibility = View.VISIBLE
            return false
        } else if (Util.isContentNull(binding!!.edConfirmPass.text.toString())) {
            binding!!.edConfirmPass.requestFocus()
            binding!!.edConfirmPass.error = getString(R.string.str_password_error)
            return false
        } else if (!binding!!.edPass.text.toString().equals(binding!!.edConfirmPass.text.toString(), ignoreCase = true)) {
            binding!!.edConfirmPass.requestFocus()
            binding!!.edConfirmPass.error = getString(R.string.password_miss_match)
            return false
        } else {
            Util.hideKeyboard(activity!!)
            return true
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnSignUp -> if (validateInput())
                registerUser()
            R.id.tvTerms -> (activity as ActivityBase).callFragmentWithReplace(R.id.authContainer, TermsAndConditionFragment.newInstance(false, true), "TermsAndConditionFragment")
            R.id.tvPrivacy -> (activity as ActivityBase).callFragmentWithReplace(R.id.authContainer, PrivacyPolicyFragment(), "PrivacyPolicyFragment")
            R.id.tvSignIn -> (activity as ActivityBase).callFragment(R.id.authContainer, LoginFragment.newInstance(), "LoginFragment")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    /**
     *
     * Register User
     * Web service request
     */
    fun registerUser() {
        if (!NetworkUtil.isInternetConnected(HooleyAuth.activity)) {
            HooleyAuth.activity.showSuccessDialog(Constants.NO_NETWORK)
            return
        }
        HooleyAuth.activity.showDialog()
        service.registerUser(binding!!.edFirstName.text.toString().trim { it <= ' ' }, binding!!.edLastName.text.toString().trim { it <= ' ' }, binding!!.edEmail.text.toString().trim { it <= ' ' }, binding!!.edPass.text.toString().trim { it <= ' ' }, profilePic, socialMediaType, false, "", "", object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyAuth.activity.removeDialog()

                (activity as ActivityBase).callFragmentWithReplace(R.id.authContainer, ConfirmRegisterFragment.newInstance(binding!!.edEmail.text.toString().trim { it <= ' ' }), "ConfirmRegisterFragment")
                clearFields()

            }

            override fun failure(message: String) {
                HooleyAuth.activity.removeDialog()

                try {
                    HooleyAuth.activity.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    fun clearFields() {
        binding!!.edLastName.setText("")
        binding!!.edFirstName.setText("")
        binding!!.edEmail.setText("")
        binding!!.edConfirmPass.setText("")
        binding!!.edPass.setText("")
    }


    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        if (isChecked) {
            binding!!.edPass.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
        } else {
            binding!!.edPass.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        }
        binding!!.edPass.setSelection(binding!!.edPass.length())
    }

    companion object {
        lateinit var instance: RegisterFragment

        fun newInstance(): RegisterFragment {
            instance = RegisterFragment()
            return instance
        }
    }
}
