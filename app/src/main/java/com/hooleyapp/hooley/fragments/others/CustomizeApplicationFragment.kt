package com.hooleyapp.hooley.fragments.others

import android.app.UiModeManager
import android.app.UiModeManager.MODE_NIGHT_YES
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v7.app.AppCompatDelegate
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentCustomizationBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.profile.ProfileMainFragment.Companion.fragmentProfileMainBinding
import com.hooleyapp.hooley.others.Constants

/**
 * Created by Nauman on 6/3/2018.
 */

class CustomizeApplicationFragment : BaseFragment(), View.OnClickListener {

    var binding: FragmentCustomizationBinding? = null
    internal var umm: UiModeManager = HooleyMain.activity!!.getSystemService(Context.UI_MODE_SERVICE) as UiModeManager
    private var themeType: Int = 0

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            handleScrollingHeader(true)
        } else {
            handleScrollingHeader(false)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_customization, container, false)
        return binding!!.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
        if (HooleyApp.db.getInt(Constants.TYPE_THEME) == MODE_NIGHT_YES) {
            binding!!.rbDarkTheme.isChecked = true
            binding!!.rbLightTheme.isChecked = false
            themeType = AppCompatDelegate.MODE_NIGHT_YES
        } else {
            binding!!.rbLightTheme.isChecked = true
            binding!!.rbDarkTheme.isChecked = false
            themeType = AppCompatDelegate.MODE_NIGHT_NO
        }
    }

    fun handleScrollingHeader(isFixed: Boolean) {
        val params = fragmentProfileMainBinding!!.clHeader.layoutParams as AppBarLayout.LayoutParams
        if (isFixed) {
            params.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP // list other flags here by |
            fragmentProfileMainBinding!!.clHeader.layoutParams = params
        } else {
            params.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS
            fragmentProfileMainBinding!!.clHeader.layoutParams = params
        }
    }


    private fun setListener() {
        binding!!.rbLightTheme.setOnClickListener(this)
        binding!!.rbDarkTheme.setOnClickListener(this)
        binding!!.btnSave.setOnClickListener(this)
    }


    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.rbLightTheme -> {
                binding!!.rbLightTheme.isChecked = true
                binding!!.rbDarkTheme.isChecked = false
                //                umm.setNightMode(UiModeManager.MODE_NIGHT_NO);
//                HooleyApp.db.putInt(Constants.TYPE_THEME, AppCompatDelegate.MODE_NIGHT_NO)
                themeType = AppCompatDelegate.MODE_NIGHT_NO
            }
            R.id.rbDarkTheme -> {
                binding!!.rbDarkTheme.isChecked = true
                binding!!.rbLightTheme.isChecked = false
                //                umm.setNightMode(UiModeManager.MODE_NIGHT_YES);
                themeType = AppCompatDelegate.MODE_NIGHT_YES

//                HooleyApp.db.putInt(Constants.TYPE_THEME, AppCompatDelegate.MODE_NIGHT_YES)
            }
            R.id.btnSave -> {
                umm.nightMode = themeType
                HooleyApp.db.putInt(Constants.TYPE_THEME, themeType)

            }
        }
    }
}
