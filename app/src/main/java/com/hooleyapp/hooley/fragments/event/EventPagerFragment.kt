package com.hooleyapp.hooley.fragments.event

import android.app.Dialog
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyAuth
import com.hooleyapp.hooley.adapters.AdapterEventPager
import com.hooleyapp.hooley.app.view.ui.activites.HooleyGuestUser
import com.hooleyapp.hooley.databinding.FragmentEventPagerBinding
import com.hooleyapp.hooley.helper.DialogGuestUser
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.IEventPagerClick
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.MapListFinalModel
import com.hooleyapp.hooley.others.*
import com.hooleyapp.hooley.services.EventWebService
import com.hooleyapp.hooley.social.SocialClass
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*

/**
 * Created by Zeeshan on 21-Feb-18.
 */

class EventPagerFragment : AppCompatDialogFragment(), IEventPagerClick {


    override fun onClickViewProfile() {
        dismiss()
    }

    var binding: FragmentEventPagerBinding? = null
    private var eventId: String? = null
    internal var adapterEventPager: AdapterEventPager? = null
    private var eventCover: String? = null
    private var eventService: EventWebService? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_event_pager, container, false)
        setListener()
        return binding!!.root
    }

    private fun setListener() {}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (mList != null && mList!!.size > 0) {
            setCardAdapter()
        }
    }

    private fun setCardAdapter() {
        adapterEventPager = AdapterEventPager(ActivityBase.activity, mList!!)
        binding!!.eventPager.adapter = adapterEventPager
        binding!!.eventPager.currentItem = position
        adapterEventPager!!.setListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onClickShare(id: String, cover: String, pos: Int) {
        if (HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)) {
            HooleyGuestUser.activity.DialogGuestUser()
        } else {
            eventId = id
            position = pos
            eventCover = cover
            object : GetBitmapTask() {
                override fun onPostExecute(bmp: Bitmap) {
                    super.onPostExecute(bmp)
                    //                ActivityBase.removeDialog();
                    showShareEventDialog(bmp)
                }
            }.execute(mList!![position].eventDetail.coverPhoto)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        if (HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)) {
            HooleyGuestUser.activity.DialogGuestUser()
        } else {
            shareEvent(event.message)
        }
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onClickFollow(eventId: String, isFollow: Boolean, pos: Int) {
        if (HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)) {
            HooleyGuestUser.activity.DialogGuestUser()

        } else {
            followEvent(eventId, isFollow, pos)
            position = pos
        }
    }


    override fun onClickLiked(eventId: String, isLiked: Boolean, pos: Int) {
        if (HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)) {
            HooleyGuestUser.activity.DialogGuestUser()

        } else {
            likeEvent(eventId, isLiked, pos)
            position = pos
        }
    }

    override fun onClickViewEvent(eventId: String, pos: Int) {
        dismiss()
        if (HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)) {
            ActivityBase.activity.supportActionBar!!.hide()
            (ActivityBase.activity as ActivityBase).addFragment(R.id.container, GuestUserLiveEventFragment
                    .newInstance(mList!![pos].eventDetail.eventId!!), "GuestUserLiveEventFragment")
            hideDrawer(ActivityBase.activity)
        } else {
            ActivityBase.activity.supportFragmentManager.popBackStackImmediate()
            (ActivityBase.activity as ActivityBase).addFragment(R.id.container, LiveEventFragment.newInstance(mList!![pos].eventDetail.eventId!!), "LiveEventFragment")
            hideDrawer(ActivityBase.activity)
        }
    }

    override fun onClickClose() {
        dismiss()
        HooleyApp.db.putBoolean(Constants.OPEN_DRAWER, true)
        try {
            ActivityBase.activity.supportActionBar!!.show()
            ActivityBase.activity.supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_menu)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        ActivityBase.activity.supportFragmentManager.popBackStackImmediate()
    }

    private fun followEvent(eventId: String, isFlow: Boolean, position: Int) {
        if (HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)) {
            HooleyGuestUser.activity.DialogGuestUser()
        } else {
            if (binding != null)
                binding!!.pbEventPager.visibility = View.VISIBLE
            if (eventService == null)
                eventService = EventWebService()
            eventService!!.followEvent(eventId, isFlow, object : IWebServiceCallback<GeneralModel> {
                override fun success(result: GeneralModel) {
                    binding!!.pbEventPager.visibility = View.GONE
                    if (isFlow) {
                        mList!![position].eventDetail.isFollowed = true
                        mList!![position].eventDetail.followersCount = mList!![position].eventDetail.followersCount + 1
                        adapterEventPager = null
                        setCardAdapter()
                    } else {
                        mList!![position].eventDetail.isFollowed = false
                        mList!![position].eventDetail.followersCount = mList!![position].eventDetail.followersCount - 1
                        adapterEventPager = null
                        setCardAdapter()
                    }
                }

                override fun failure(message: String) {
                    binding!!.pbEventPager.visibility = View.GONE

                    try {
                        ActivityBase.activity.showSuccessDialog(message)
                    } catch (ex: NullPointerException) {
                        ex.printStackTrace()
                    }

                }

                override fun onTokenExpire() {
                    onTokenExpireLogOut()
                }
            })
        }
    }

    private lateinit var notificationManager: NotificationManager

    fun onTokenExpireLogOut() {
        notificationManager = ActivityBase.activity.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
        SocialClass.logoutFaceBook()
        val i = Intent(ActivityBase.activity, HooleyAuth::class.java)
        ActivityBase.activity.startActivity(i)
        HooleyApp.db.putBoolean(Constants.IS_LOGIN, false)
        HooleyApp.db.putBoolean(Constants.IS_GUEST_LOGIN, false)
        ActivityBase.activity.finish()
    }


    private fun likeEvent(eventId: String, isLiked: Boolean, position: Int) {
        if (HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)) {
            HooleyGuestUser.activity.DialogGuestUser()

        } else {
            if (binding != null)
                binding!!.pbEventPager.visibility = View.VISIBLE
            if (eventService == null)
                eventService = EventWebService()
            eventService!!.likeEvent(eventId, object : IWebServiceCallback<GeneralModel> {
                override fun success(result: GeneralModel) {
                    binding!!.pbEventPager.visibility = View.GONE
                    if (isLiked) {
                        mList!![position].eventDetail.isLiked = true
                        mList!![position].eventDetail.likesCount = mList!![position].eventDetail.likesCount + 1
                        adapterEventPager = null
                        setCardAdapter()
                    } else {
                        mList!![position].eventDetail.isLiked = false
                        mList!![position].eventDetail.likesCount = mList!![position].eventDetail.likesCount - 1
                        adapterEventPager = null
                        setCardAdapter()
                    }
                }

                override fun failure(message: String) {
                    binding!!.pbEventPager.visibility = View.GONE
                    try {
                        ActivityBase.activity.showSuccessDialog(message)
                    } catch (ex: NullPointerException) {
                        ex.printStackTrace()
                    }

                }

                override fun onTokenExpire() {
                    onTokenExpireLogOut()
                }
            })
        }
    }


    private fun showShareEventDialog(bitmap: Bitmap) {
        val bitmapPath = MediaStore.Images.Media.insertImage(ActivityBase.activity.contentResolver, bitmap, "Thumb", null)
        val bitmapUri = Uri.parse(bitmapPath)
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "image/png"
        intent.type = "text/plain"
        val receiver = Intent(ActivityBase.activity, ApplicationSelectorReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(ActivityBase.activity, 0, receiver, PendingIntent.FLAG_UPDATE_CURRENT)
        intent.putExtra(Intent.EXTRA_STREAM, bitmapUri)
        intent.putExtra(Intent.EXTRA_TEXT,
                Constants.EVENT_SHARE + mList!![position].eventDetail.eventId
                        + "\n" + mList!![position].eventDetail.eventName
                        + "\nStart Date = " + DateUtils.formatDateNew(mList!![position].eventDetail.startTime!!))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            startActivityForResult(Intent.createChooser(intent, null, pendingIntent.intentSender), 7)
        } else {
            startActivityForResult(Intent.createChooser(intent, "Share"), 7)
        }
    }

    private fun shareEvent(shareMedia: String) {

        if (binding != null)
            binding!!.pbEventPager.visibility = View.VISIBLE
        if (eventService == null)
            eventService = EventWebService()

        eventService!!.shareEvent(eventId!!, shareMedia, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                binding!!.pbEventPager.visibility = View.GONE
                mList!![position].eventDetail.sharesCount = mList!![position].eventDetail.sharesCount + 1
                adapterEventPager = null
                setCardAdapter()
            }

            override fun failure(message: String) {
                binding!!.pbEventPager.visibility = View.GONE
                try {
                    ActivityBase.activity.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })


    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window.requestFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        val dialog = dialog
        if (dialog != null) {
            dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    fun show(fragmentManager: FragmentManager) {
        val ft = fragmentManager.beginTransaction()
        ft.add(this, tag)
        ft.commitAllowingStateLoss()
    }


    companion object {


        lateinit var instance: EventPagerFragment
        private var mList: ArrayList<MapListFinalModel>? = null
        private var position: Int = 0

        fun newInstance(list: ArrayList<MapListFinalModel>, pos: Int): EventPagerFragment {
            mList = list
            position = pos
            instance = EventPagerFragment()
            return instance
        }
    }

}