package com.hooleyapp.hooley.fragments.others

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentWalletCardsBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.CardDetailsModel
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.services.TicketWebService


/**
 * Created by Nauman on 6/3/2018.
 */

@SuppressLint("ValidFragment")
class HooleyWalletCardsFragment(var forPayment: Boolean, var title: String) : BaseFragment() {

    lateinit var binding: FragmentWalletCardsBinding
    var service = TicketWebService()


    private val intent: Intent = Intent()

    private lateinit var hooleyWallet: HooleyWalletFragment

    fun setToolbar() {
        if (title.contains("Save")) {
            setToolBarTitle(HooleyMain.activity!!, "Hooley Wallet")
        } else {
            setToolBarTitle(HooleyMain.activity!!, title)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_wallet_cards, container, false)

        getCardDetails()


        if (forPayment) {
            binding.btnDelete.visibility = View.GONE
            binding.tvAddCard.text = "Pay with other Card"
        } else {
            binding.tvAddCard.text = "Update New Card"
        }

        binding.btnDelete.setOnClickListener {
            HooleyMain.activity!!.YesNoDialog("", "Are you sure, You want to delete card?", null, object : onClickDialog {
                override fun onClickYes() {
                    HooleyMain.activity!!.showDialog()
                    service.deleteCard(object : GenericCallback<GeneralModel> {
                        override fun success(result: GeneralModel) {
                            HooleyApp.db.putBoolean(Constants.IS_WALLET_SETUP, false)
                            HooleyMain.activity!!.removeDialog()
                            HooleyMain.activity!!.showSuccessDialog("Card Deleted Successfully")
                            callFragmentWithReplace(R.id.container, HooleyWalletFragment(false, forPayment, title), null)
                        }

                        override fun failure(message: String) {
                            HooleyMain.activity!!.removeDialog()
                            HooleyMain.activity!!.showSuccessDialog(message)
                        }

                    })

                }
            })
        }

        binding.ivCardBg.setOnClickListener {
            if (forPayment) {
                HooleyMain.activity!!.YesNoDialog("", "Are you sure, want to make payment?", null, object : onClickDialog {
                    override fun onClickYes() {
                        intent.putExtra("stripeToken", "")
                        HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                        targetFragment!!.onActivityResult(
                                targetRequestCode,
                                Activity.RESULT_OK,
                                intent
                        )
                    }
                })
            }
        }

        binding.rlAddNewCard.setOnClickListener {
            if (forPayment) {
                hooleyWallet = HooleyWalletFragment(true, forPayment, title)
                hooleyWallet.setTargetFragment(this, BUY_REQUEST_CODE)
                callFragment(R.id.container, hooleyWallet, "HooleyWalletFragment")
            } else {
                callFragment(R.id.container, HooleyWalletFragment(false, forPayment, title), "HooleyWalletFragment")
            }
        }

        return binding.root
    }

    companion object {

        private lateinit var instance: HooleyWalletCardsFragment

        fun newInstance(): HooleyWalletCardsFragment {
            return HooleyWalletCardsFragment.instance
        }

    }

    private fun getCardDetails() {
        HooleyMain.activity!!.showDialog()
        service.getCards(object : GenericCallback<CardDetailsModel> {
            override fun success(result: CardDetailsModel) {
                HooleyMain.activity!!.removeDialog()
                binding.rlCards.visibility = View.VISIBLE
                binding.tvCardholderName.text = result.name
                binding.tvCExpiry.text = result.expMonth + "/${result.expYear}"
                binding.tvCnum.text = result.last4
                binding.ivCardLogo.setImageDrawable(getImage(result.brand!!))

            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                HooleyMain.activity!!.showSuccessDialog(message)

            }
        })
    }

    private fun getImage(brand: String): Drawable {
        return when (brand) {
            "Visa" -> ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_v_isa)!!
            "Diners Club" -> ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_dinners_club)!!
            "JCB" -> ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_j_cb)!!
            "MasterCard" -> ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_m_astercard)!!
            "UnionPay" -> ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_u_nionpay)!!
            "American Express" -> ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_american_express)!!
            "Discover" -> ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_discover_y)!!
            else -> ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.md_transparent)!!
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        binding.unbind()
    }

    private val BUY_REQUEST_CODE: Int = 3000

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == BUY_REQUEST_CODE) {
                intent.putExtra("stripeToken", data.extras.get("stripeToken").toString())
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                targetFragment!!.onActivityResult(
                        targetRequestCode,
                        Activity.RESULT_OK,
                        intent
                )
            }
        }
    }

}
