package com.hooleyapp.hooley.fragments.nearby

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.FacebookSdk
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.VPDashBoardAdapter
import com.hooleyapp.hooley.app.view.ui.fragments.event.PopularInFriendsFragment
import com.hooleyapp.hooley.app.view.ui.fragments.media.MediaStreamMainFragment
import com.hooleyapp.hooley.databinding.FragmentDashboardBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.event.BookmarkEventsFragment
import com.hooleyapp.hooley.fragments.event.CreateEventOptionsFragment
import com.hooleyapp.hooley.fragments.event.FeatureEventFragment
import com.hooleyapp.hooley.fragments.event.MyEventMainFragment
import com.hooleyapp.hooley.fragments.notification.NotificationMainFragment
import com.hooleyapp.hooley.fragments.profile.ProfileMainFragment
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.helper.onClickDialog
import com.hooleyapp.hooley.helper.showUpdateDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.NotificationCountModel
import com.hooleyapp.hooley.others.Constants

/**
 * Created by Nauman on 12/11/2017.
 */

class DashBoardFragment : BaseFragment(), View.OnClickListener {
    private var adapter: VPDashBoardAdapter? = null
    lateinit var objectResponse: NotificationCountModel
    private var broadcaster: LocalBroadcastManager? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentDashboardBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false)
        broadcaster = LocalBroadcastManager.getInstance(HooleyMain.activity!!)
        setListener()
        setupViewPager()
        setToolbar()
        setHeader()
        getNotificationCount()
        return fragmentDashboardBinding!!.root
    }

    private fun setListener() {
        fragmentDashboardBinding!!.rlCreateEvent.setOnClickListener(this)
        fragmentDashboardBinding!!.rlMyEvent.setOnClickListener(this)
        fragmentDashboardBinding!!.rlMyProfile.setOnClickListener(this)
        fragmentDashboardBinding!!.rlMediaStream.setOnClickListener(this)
        fragmentDashboardBinding!!.rlBookMark.setOnClickListener(this)
        fragmentDashboardBinding!!.rlFeatureEvent.setOnClickListener(this)
        fragmentDashboardBinding!!.rlPopular.setOnClickListener(this)
    }

    private fun setHeader() {
        fragmentDashboardBinding!!.llNearBy.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.selected_tabs_color))
    }

    private fun setupViewPager() {
        adapter = VPDashBoardAdapter(childFragmentManager)
        adapter!!.addFragment(NearByFragment(), "NearByFragment")
        fragmentDashboardBinding!!.vpDashBoard.adapter = adapter
    }

    fun setToolbar() {
        HooleyApp.db.putBoolean(Constants.OPEN_DRAWER, true)
        try {
            HooleyMain.activity!!.supportActionBar!!.show()
            HooleyMain.activity!!.supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_menu)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getNotificationCount() {

        NotificationMainFragment.service.getRecentData(object : GenericCallback<NotificationCountModel> {
            override fun success(result: NotificationCountModel) {
                objectResponse = result
                val mIntent = Intent("Notification")
                HooleyApp.db.putInt(Constants.UNREAD_NOTIFICATION_COUNT, (objectResponse.count.eventInvitesCount + objectResponse.count.messagesCount + objectResponse.count.mediaAlertsCount + objectResponse.count.friendRequestCount))
                if (objectResponse.hooleyVersion != null)
                    if (HooleyMain.activity!!.packageManager.getPackageInfo(HooleyMain.activity!!.packageName, 0).versionName.toDouble() < objectResponse.hooleyVersion.versionName!!.toDouble()) {
                        HooleyMain.activity!!.showUpdateDialog(object : onClickDialog {
                            override fun onClickYes() {
                                val appPackageName = HooleyMain.activity!!.packageName // getPackageName() from Context or Activity object
                                try {
                                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
                                } catch (anfe: android.content.ActivityNotFoundException) {
                                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
                                }

                            }
                        })
                    }
                broadcaster!!.sendBroadcast(mIntent)
            }

            override fun failure(message: String) {
                try {
                    //                    Util.showToastMessage(HooleyMain.activity, message);
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.rlCreateEvent -> {
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, CreateEventOptionsFragment(), "CreateEventOptionsFragment")
                hideDrawer(HooleyMain.activity!!)
            }
            R.id.rlMyEvent -> {
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, MyEventMainFragment.newInstance(), "MyEventMainFragment")
                hideDrawer(HooleyMain.activity!!)
            }
            R.id.rlMyProfile -> {
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, ProfileMainFragment.newInstance(false), "ProfileMainFragment")
                hideDrawer(HooleyMain.activity!!)
            }
            R.id.rlMediaStream -> {
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, MediaStreamMainFragment(), "MediaStreamMainFragment")
                hideDrawer(HooleyMain.activity!!)
            }
            R.id.rlBookMark -> {
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, BookmarkEventsFragment.newInstance(), "BookmarkEventsFragment")
                hideDrawer(HooleyMain.activity!!)
            }
            R.id.rlFeatureEvent -> {
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, FeatureEventFragment.newInstance(), "FeatureEventFragment")
                hideDrawer(HooleyMain.activity!!)
            }
            R.id.rlPopular -> {
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, PopularInFriendsFragment(), "PopularInFriendsFragment")
                hideDrawer(HooleyMain.activity!!)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (fragmentDashboardBinding != null) {
            fragmentDashboardBinding!!.unbind()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (FacebookSdk.isFacebookRequestCode(requestCode)) {
//            ||
//        } TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE == requestCode) {
            val fragment = adapter!!.getItem(0)
            fragment.onActivityResult(requestCode, resultCode, data)
        } else if (requestCode == 506 && resultCode == Activity.RESULT_OK) {
            val fragment = adapter!!.getItem(0)
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onResume() {
        super.onResume()
        setToolbar()
    }

    companion object {

        var instance: DashBoardFragment = DashBoardFragment()

        var fragmentDashboardBinding: FragmentDashboardBinding? = null

        fun newInstance(): DashBoardFragment {
            instance = DashBoardFragment()
            return instance
        }
    }
}
