package com.hooleyapp.hooley.fragments.others

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatDialogFragment
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ProgressBar
import com.gigamole.infinitecycleviewpager.HorizontalInfiniteCycleViewPager
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterViewAvatar
import com.hooleyapp.hooley.fragments.friends.FriendsProfileFragment
import com.hooleyapp.hooley.fragments.messaging.ChatWindowFragment
import com.hooleyapp.hooley.fragments.profile.ProfileMainFragment
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.FriendCardClickListener
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GetUserContactCardResponseModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.services.HooleyWebService
import java.util.*


@SuppressLint("ValidFragment")
class ViewAvatarFragment constructor(var friendId: Int) : AppCompatDialogFragment(), FriendCardClickListener {

    var mlist: ArrayList<GetUserContactCardResponseModel.UserContactCardList>? = null
    var service = HooleyWebService()
    var friendWebService = FriendWebService()
    private var adapterCardPager: AdapterViewAvatar? = null
    private lateinit var contentView: View
    private var pbLoading: ProgressBar? = null
    private var cardPager: HorizontalInfiniteCycleViewPager? = null
    lateinit var mListener: CardCallBackListener

    fun show(fragmentManager: FragmentManager) {
        val ft = fragmentManager.beginTransaction()
        ft.add(this, tag)
        ft.commitAllowingStateLoss()
    }


    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        contentView = View.inflate(context, R.layout.card_pager_fragment, null)
        dialog.setContentView(contentView)
        (HooleyMain.activity as HooleyMain).removeBubbleView()
        initViews()
        getContactCard()
    }


    private fun initViews() {
        pbLoading = contentView.findViewById(R.id.pbLoading)
        cardPager = contentView.findViewById(R.id.cardPager)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window.requestFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }


    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    private fun setCardAdapter() {
        adapterCardPager = AdapterViewAvatar(mlist!!)
        adapterCardPager!!.mListener = this
        cardPager!!.adapter = adapterCardPager
    }

    override fun onClickViewProfile(position: Int) {
        dismiss()
        if (::mListener.isInitialized)
            mListener.reloadFragment(true)
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, FriendsProfileFragment.newInstance(mlist!![position].userId!!.toInt(), mlist!![position].fullName!!), "FriendsProfileFragment")
    }

    override fun onClickAddFriend(position: Int) {
        dismiss()
        sendFriendRequest(mlist!![position].userId!!.toInt())
    }

    override fun onClickAcceptRequest(position: Int, value: Boolean) {
        dismiss()
        if (::mListener.isInitialized)
            mListener.reloadFragment(true)
        friendRequest(mlist!![position].userId!!.toInt(), value)
    }

    override fun onClickMessage(position: Int) {
        dismiss()
        if (::mListener.isInitialized)
            mListener.reloadFragment(true)
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ChatWindowFragment("", mlist!![position].userId!!, mlist!![position].fullName!!), "ChatWindowFragment")
    }

    override fun onClickClose() {
        dismiss()
    }

    override fun onClickMyProfile() {
        dismiss()
        if (::mListener.isInitialized)
            mListener.reloadFragment(true)
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ProfileMainFragment.newInstance(false), "ProfileMainFragment")
        hideDrawer(HooleyMain.activity!!)
    }

    // Get User Detail
    private fun getContactCard() {
        pbLoading!!.visibility = View.VISIBLE
        service.getUserContactCard(friendId.toString(), object : GenericCallback<GetUserContactCardResponseModel> {
            override fun success(result: GetUserContactCardResponseModel) {
                pbLoading!!.visibility = View.GONE
                mlist = result.userContactCardList
                setCardAdapter()
            }

            override fun failure(message: String) {
                try {
                    pbLoading!!.visibility = View.GONE
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun sendFriendRequest(friendId: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        val friendList = ArrayList<Int>()
        friendList.add(friendId)
        friendWebService.sendFriendRequest(friendList, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                Util.showToastMessage(HooleyMain.activity!!, "Friend Request Sent")
                dismiss()
                if (::mListener.isInitialized)
                    mListener.reloadFragment(true)
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }


            }
        })
    }

    private fun friendRequest(userId: Int, reply: Boolean) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        friendWebService.acceptIgnoreFriendRequest(userId, reply, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                dismiss()
                if (::mListener.isInitialized)
                    mListener.reloadFragment(true)
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }


    // Interface
    interface CardCallBackListener {
        fun reloadFragment(flag: Boolean)
    }
}
