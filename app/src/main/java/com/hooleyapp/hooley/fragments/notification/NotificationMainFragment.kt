package com.hooleyapp.hooley.fragments.notification

import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.FacebookSdk.getApplicationContext
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.VPDashBoardAdapter
import com.hooleyapp.hooley.databinding.FragmentNotificationBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.hideKeyboard
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.NotificationCountModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.MessageEvent
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.NotificationWebService
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


/**
 * Created by Nauman on 4/10/2018.
 */

class NotificationMainFragment : BaseFragment(), View.OnClickListener {
    private var adapter: VPDashBoardAdapter? = null
    lateinit var objectResponse: NotificationCountModel
    private var broadcaster: LocalBroadcastManager? = null
    val mIntent = Intent("Notification")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentNotificationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification, container, false)
        broadcaster = LocalBroadcastManager.getInstance(HooleyMain.activity!!)
        val notificationManager = getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
        return fragmentNotificationBinding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        HooleyMain.activity!!.hideKeyboard()
        setListener()
        setupViewPager()
//        setToolbar()
        setSelectedBackground(R.id.llEventInvites)
        getNotificationCount()
        checkForSelectedItem()
        HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, false)
        HooleyApp.db.putInt(Constants.UNREAD_NOTIFICATION_COUNT, 0)
        broadcaster!!.sendBroadcast(mIntent)
    }

    override fun onResume() {
        super.onResume()
        when (fragmentNotificationBinding!!.vpNotification.currentItem) {
            0 -> setSelectedBackground(R.id.llMessages)
            1 -> setSelectedBackground(R.id.llEventInvites)
            2 -> setSelectedBackground(R.id.llMediaAlert)
            3 -> setSelectedBackground(R.id.llFriends)
        }

    }

//    fun setToolbar() {
//        (HooleyMain.activity as HooleyMain).setToolbarTitle("Notifications")
//    }

    private fun seCurrentPage() {
        fragmentNotificationBinding!!.vpNotification.currentItem = 0
        fragmentNotificationBinding!!.llEventInvites.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_gray_color))
    }

    private fun setListener() {
        fragmentNotificationBinding!!.llEventInvites.setOnClickListener(this)
        fragmentNotificationBinding!!.llMessages.setOnClickListener(this)
        fragmentNotificationBinding!!.llMediaAlert.setOnClickListener(this)
        fragmentNotificationBinding!!.llFriends.setOnClickListener(this)

    }

    private fun setupViewPager() {
        adapter = VPDashBoardAdapter(childFragmentManager)
        adapter!!.addFragment(MessageAlertNotificationFragment(), "MessageAlertNotificationFragment")
        adapter!!.addFragment(EventInviteNotificationFragment(), "EventInviteNotificationFragment")
        adapter!!.addFragment(MediaAlertsNotificationFragment(), "MediaAlertsNotificationFragment")
        adapter!!.addFragment(FriendRequestNotificationFragment(), "FriendRequestNotificationFragment")
        fragmentNotificationBinding!!.vpNotification.adapter = adapter
    }

    override fun onDestroy() {
        super.onDestroy()
        if (fragmentNotificationBinding != null)
            fragmentNotificationBinding!!.unbind()
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.llMessages -> {
                setSelectedBackground(R.id.llMessages)
                fragmentNotificationBinding!!.vpNotification.setCurrentItem(0, true)
            }
            R.id.llEventInvites -> {
                setSelectedBackground(R.id.llEventInvites)
                fragmentNotificationBinding!!.vpNotification.setCurrentItem(1, true)
            }
            R.id.llMediaAlert -> {
                setSelectedBackground(R.id.llMediaAlert)
                fragmentNotificationBinding!!.vpNotification.setCurrentItem(2, true)
            }
            R.id.llFriends -> {
                setSelectedBackground(R.id.llFriends)
                fragmentNotificationBinding!!.vpNotification.setCurrentItem(3, true)
            }
        }
    }

    private fun setSelectedBackground(id: Int) {
        when (id) {
            R.id.llEventInvites -> {
                fragmentNotificationBinding!!.llEventInvites.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                fragmentNotificationBinding!!.llMessages.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentNotificationBinding!!.llMediaAlert.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentNotificationBinding!!.llFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
            R.id.llMessages -> {
                fragmentNotificationBinding!!.llEventInvites.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentNotificationBinding!!.llMessages.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                fragmentNotificationBinding!!.llMediaAlert.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentNotificationBinding!!.llFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
            R.id.llMediaAlert -> {
                fragmentNotificationBinding!!.llEventInvites.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentNotificationBinding!!.llMessages.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentNotificationBinding!!.llMediaAlert.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                fragmentNotificationBinding!!.llFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
            R.id.llFriends -> {
                fragmentNotificationBinding!!.llEventInvites.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentNotificationBinding!!.llMessages.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentNotificationBinding!!.llMediaAlert.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentNotificationBinding!!.llFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
            }
        }
    }

    private fun checkForSelectedItem() {
        when (HooleyApp.db.getString(Constants.TYPE_NOTIFICATION)) {
            Constants.TYPE_MESSAGES_ALERT -> {
                setSelectedBackground(R.id.llMessages)
                fragmentNotificationBinding!!.vpNotification.setCurrentItem(0, true)
            }
            Constants.TYPE_EVENT_ALERTS -> {
                setSelectedBackground(R.id.llEventInvites)
                fragmentNotificationBinding!!.vpNotification.setCurrentItem(1, true)
            }
            Constants.TYPE_MEDIA_ALERT -> {
                setSelectedBackground(R.id.llMediaAlert)
                fragmentNotificationBinding!!.vpNotification.setCurrentItem(2, true)
            }
            Constants.TYPE_FRIENDS_ALERT -> {
                setSelectedBackground(R.id.llFriends)
                fragmentNotificationBinding!!.vpNotification.setCurrentItem(3, true)
            }
        }
    }

    fun getNotificationCount() {

        service.getRecentData(object : GenericCallback<NotificationCountModel> {
            override fun success(result: NotificationCountModel) {
                objectResponse = result
                if (objectResponse.count.eventInvitesCount == 0) {
                    fragmentNotificationBinding!!.rlNotificationEvent.visibility = View.GONE
                } else {
                    fragmentNotificationBinding!!.rlNotificationEvent.visibility = View.VISIBLE
                    fragmentNotificationBinding!!.tvInviteCount.text = objectResponse.count.eventInvitesCount.toString()
                }
                if (objectResponse.count.messagesCount == 0) {
                    fragmentNotificationBinding!!.rlNotificationMessage.visibility = View.GONE
                } else {
                    fragmentNotificationBinding!!.rlNotificationMessage.visibility = View.VISIBLE
                    fragmentNotificationBinding!!.tvMessageCount.text = objectResponse.count.messagesCount.toString()
                }
                if (objectResponse.count.mediaAlertsCount == 0) {
                    fragmentNotificationBinding!!.rlNotificationMedia.visibility = View.GONE
                } else {
                    fragmentNotificationBinding!!.rlNotificationMedia.visibility = View.VISIBLE
                    fragmentNotificationBinding!!.tvMediaCount.text = objectResponse.count.mediaAlertsCount.toString()
                }
                if (objectResponse.count.friendRequestCount == 0) {
                    fragmentNotificationBinding!!.rlNotificationFriend.visibility = View.GONE
                } else {
                    fragmentNotificationBinding!!.rlNotificationFriend.visibility = View.VISIBLE
                    fragmentNotificationBinding!!.tvFriendCount.text = objectResponse.count.friendRequestCount.toString()
                }

                setCount()
            }

            override fun failure(message: String) {
                try {
                    //                    Util.showToastMessage(HooleyMain.activity, message);
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }

    companion object {

        lateinit var instance: NotificationMainFragment
        var fragmentNotificationBinding: FragmentNotificationBinding? = null
        var service = NotificationWebService()

        fun newInstance(): NotificationMainFragment {
            instance = NotificationMainFragment()
            return instance
        }

    }

    private fun setCount() {
        HooleyApp.db.putInt(Constants.UNREAD_NOTIFICATION_COUNT, (objectResponse.count.eventInvitesCount + objectResponse.count.messagesCount + objectResponse.count.mediaAlertsCount + objectResponse.count.friendRequestCount))
        broadcaster!!.sendBroadcast(mIntent)

    }

    fun setSeenCountApi(type: String) {

        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }

        service.setSeenCount(type, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                when (type) {
                    Constants.TYPE_EVENT_ALERTS -> {
                        fragmentNotificationBinding!!.rlNotificationEvent.visibility = View.GONE
                        objectResponse.count.eventInvitesCount = 0
                        setCount()
                    }
                    Constants.TYPE_MESSAGES_ALERT -> {
                        fragmentNotificationBinding!!.rlNotificationMessage.visibility = View.GONE
                        objectResponse.count.messagesCount = 0
                        setCount()
                    }
                    Constants.TYPE_MEDIA_ALERT -> {
                        fragmentNotificationBinding!!.rlNotificationMedia.visibility = View.GONE
                        objectResponse.count.mediaAlertsCount = 0
                        setCount()
                    }
                    Constants.TYPE_FRIENDS_ALERT -> {
                        fragmentNotificationBinding!!.rlNotificationFriend.visibility = View.GONE
                        objectResponse.count.friendRequestCount = 0
                        setCount()
                    }
                }
            }

            override fun failure(message: String) {
                try {
                    //                    Util.showToastMessage(HooleyMain.activity, message);
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        setSeenCountApi(event.message)
    }

}
