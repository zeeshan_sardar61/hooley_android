package com.hooleyapp.hooley.fragments.stripe;

import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hooleyapp.hooley.R;

/**
 * Created by Zeeshan on 19-Mar-18.
 */

public class StripeBottomSheetFragment extends BottomSheetDialogFragment {


    public StripeBottomSheetFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_stripe_bottom_sheet, container, false);
    }
}
