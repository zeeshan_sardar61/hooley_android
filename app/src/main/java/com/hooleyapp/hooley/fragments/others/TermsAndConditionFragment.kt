package com.hooleyapp.hooley.fragments.others

import android.annotation.SuppressLint
import android.app.UiModeManager
import android.app.UiModeManager.MODE_NIGHT_YES
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.*
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.databinding.FragmentTermsAndConditionsBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.others.Constants

/**
 * Created by Nauman on 12/4/2017.
 */

class TermsAndConditionFragment : BaseFragment(), View.OnClickListener {

    internal var binding: FragmentTermsAndConditionsBinding? = null
    internal lateinit var umm: UiModeManager
    private var themeType: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_terms_and_conditions, container, false)
        if (isFromMain)
            HooleyApp.db.putBoolean(Constants.OPEN_DRAWER, false)
        ActivityBase.setStatusBarGradiant(0)
        setHasOptionsMenu(true)
        setListener()
        showButton()
        return binding!!.root
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
    }

    private fun showButton() {
        if (isBtnVisible)
            binding!!.btnAccept.visibility = View.VISIBLE
        else
            binding!!.btnAccept.visibility = View.GONE
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setWebView()
    }

    private fun setListener() {
        binding!!.btnAccept.setOnClickListener(this)
    }


    private fun setWebView() {
        binding!!.wvTerms.settings.javaScriptEnabled = true
        var url = ""
        if (HooleyApp.db.getInt(Constants.TYPE_THEME) == MODE_NIGHT_YES) {
            url = Constants.TERMS_CONDITIONS_URL + "true"
        } else {
            url = Constants.TERMS_CONDITIONS_URL + "false"
        }
        binding!!.wvTerms.loadUrl(url)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnAccept -> activity!!.supportFragmentManager.popBackStackImmediate()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
        //        HooleyMain.activity.getSupportActionBar().show();
        /*   setStatusBarColor(R.color.app_purple_color, 0);*/
        activity!!.recreate()
    }

    companion object {

        @SuppressLint("StaticFieldLeak")
        lateinit var instance: TermsAndConditionFragment
        internal var isBtnVisible: Boolean = false
        private var isFromMain: Boolean = false

        fun newInstance(check: Boolean, isVisible: Boolean): TermsAndConditionFragment {
            isFromMain = check
            isBtnVisible = isVisible
            instance = TermsAndConditionFragment()
            return instance
        }
    }
}
