package com.hooleyapp.hooley.fragments.profile

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.VPDashBoardAdapter
import com.hooleyapp.hooley.app.view.ui.fragments.media.MyMediaFragment
import com.hooleyapp.hooley.databinding.FragmentProfileMainBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.friends.FriendsFragment
import com.hooleyapp.hooley.fragments.messaging.MessageHistoryFragment
import com.hooleyapp.hooley.fragments.others.PreferencesFragment
import com.hooleyapp.hooley.fragments.ticket.MyTicketFragment
import com.hooleyapp.hooley.fragments.ticket.ScanTicketFragment
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.others.Constants

/**
 * Created by Nauman on 12/11/2017.
 */

class ProfileMainFragment : BaseFragment(), View.OnClickListener {
    private var adapter: VPDashBoardAdapter? = null
    private val REQUEST_CODE_STRIPE_PERSONAL = 900
    private val REQUEST_CODE_STRIPE_BUSSINESS = 901

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentProfileMainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile_main, container, false)
        setListener()
        setupViewPager()
        hideDrawer(HooleyMain.activity!!)
        HooleyMain.activity!!.supportActionBar!!.show()
        fragmentProfileMainBinding!!.llPersonalProfile.performClick()
        //        seCurrentPage();
        if (isFromSideMenu) {
            if (HooleyApp.db.getBoolean(Constants.IS_FRIEND_CLICK)) {
                fragmentProfileMainBinding!!.llMyFriends.performClick()
            } else {
                fragmentProfileMainBinding!!.llMyTickets.performClick()
            }
        }
        return fragmentProfileMainBinding!!.root
    }

    private fun seCurrentPage() {
        fragmentProfileMainBinding!!.vpProfileMain.currentItem = 0
        fragmentProfileMainBinding!!.rlPersonalProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_gray_color))
    }

    private fun setListener() {
        fragmentProfileMainBinding!!.llPersonalProfile.setOnClickListener(this)
        fragmentProfileMainBinding!!.llBusinessProfile.setOnClickListener(this)
        fragmentProfileMainBinding!!.llCustomApp.setOnClickListener(this)
        fragmentProfileMainBinding!!.llTicketScanning.setOnClickListener(this)
        fragmentProfileMainBinding!!.llMyMedia.setOnClickListener(this)
        fragmentProfileMainBinding!!.llMyFriends.setOnClickListener(this)
        fragmentProfileMainBinding!!.llMyTickets.setOnClickListener(this)
        fragmentProfileMainBinding!!.llMessage.setOnClickListener(this)

    }

    override fun onResume() {
        super.onResume()
        when (fragmentProfileMainBinding!!.vpProfileMain.currentItem) {
            0 -> setSelectedBackground(R.id.llPersonalProfile)
            1 -> setSelectedBackground(R.id.llBusinessProfile)
            2 -> setSelectedBackground(R.id.llCustomApp)
            3 -> setSelectedBackground(R.id.llMyMedia)
            4 -> setSelectedBackground(R.id.llMyFriends)
            5 -> setSelectedBackground(R.id.llMyTickets)
            6 -> setSelectedBackground(R.id.llMessage)
        }
    }

    private fun setupViewPager() {
        adapter = VPDashBoardAdapter(childFragmentManager)
        adapter!!.addFragment(PersonalProfileFragment(), "PersonalProfileFragment")
        adapter!!.addFragment(BusinessProfileFragment(), "BusinessProfileFragment")
//        adapter!!.addFragment(CustomizeApplicationFragment(), "CustomizeApplicationFragment")
        adapter!!.addFragment(PreferencesFragment(), "PreferencesFragment")
        adapter!!.addFragment(MyMediaFragment(), "MyMediaFragment")
        adapter!!.addFragment(FriendsFragment(), "FriendsFragment")
        adapter!!.addFragment(MyTicketFragment(), "MyTicketFragment")
        adapter!!.addFragment(MessageHistoryFragment(), "MessageHistoryFragment")
        fragmentProfileMainBinding!!.vpProfileMain.adapter = adapter
    }


    override fun onDestroy() {
        super.onDestroy()
        if (fragmentProfileMainBinding != null)
            fragmentProfileMainBinding!!.unbind()
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.llPersonalProfile -> {
                setSelectedBackground(R.id.llPersonalProfile)
                fragmentProfileMainBinding!!.vpProfileMain.setCurrentItem(0, true)
            }
            R.id.llBusinessProfile -> {
                setSelectedBackground(R.id.llBusinessProfile)
                fragmentProfileMainBinding!!.vpProfileMain.setCurrentItem(1, true)
            }
            R.id.llCustomApp -> {
                setSelectedBackground(R.id.llCustomApp)
                fragmentProfileMainBinding!!.vpProfileMain.setCurrentItem(2, true)
            }
            R.id.llTicketScanning -> (HooleyMain.activity as ActivityBase).callFragment(R.id.container, ScanTicketFragment.newInstance(), "ScanTicketFragment")
            R.id.llMyMedia -> {
                setSelectedBackground(R.id.llMyMedia)
                fragmentProfileMainBinding!!.vpProfileMain.setCurrentItem(3, true)
            }
            R.id.llMyFriends -> {
                setSelectedBackground(R.id.llMyFriends)
                fragmentProfileMainBinding!!.vpProfileMain.setCurrentItem(4, true)
            }
            R.id.llMyTickets -> {
                setSelectedBackground(R.id.llMyTickets)
                fragmentProfileMainBinding!!.vpProfileMain.setCurrentItem(5, true)
            }
            R.id.llMessage -> {
                setSelectedBackground(R.id.llMessage)
                fragmentProfileMainBinding!!.vpProfileMain.setCurrentItem(6, true)
            }
        }//                setSelectedBackground(R.id.llTicketScanning);
        //                fragmentProfileMainBinding.vpProfileMain.setCurrentItem(3, true);
    }

    private fun setSelectedBackground(id: Int) {
        when (id) {
            R.id.llPersonalProfile -> {
                fragmentProfileMainBinding!!.llPersonalProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                fragmentProfileMainBinding!!.llBusinessProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llCustomApp.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llTicketScanning.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyMedia.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyTickets.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMessage.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
            R.id.llBusinessProfile -> {
                fragmentProfileMainBinding!!.llPersonalProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llBusinessProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                fragmentProfileMainBinding!!.llCustomApp.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llTicketScanning.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyMedia.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyTickets.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMessage.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
            R.id.llCustomApp -> {
                fragmentProfileMainBinding!!.llPersonalProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llBusinessProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llCustomApp.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                fragmentProfileMainBinding!!.llTicketScanning.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyMedia.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyTickets.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMessage.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
            R.id.llTicketScanning -> {
                fragmentProfileMainBinding!!.llPersonalProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llBusinessProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llCustomApp.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llTicketScanning.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                fragmentProfileMainBinding!!.llMyMedia.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyTickets.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMessage.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
            R.id.llMyMedia -> {
                fragmentProfileMainBinding!!.llBusinessProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llTicketScanning.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llCustomApp.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llPersonalProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyMedia.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                fragmentProfileMainBinding!!.llMyFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyTickets.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMessage.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
            R.id.llMyFriends -> {
                fragmentProfileMainBinding!!.llBusinessProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llTicketScanning.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llCustomApp.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llPersonalProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyMedia.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                fragmentProfileMainBinding!!.llMyTickets.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMessage.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
            R.id.llMyTickets -> {
                fragmentProfileMainBinding!!.llBusinessProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llTicketScanning.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llCustomApp.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llPersonalProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyMedia.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyTickets.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                fragmentProfileMainBinding!!.llMessage.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
            R.id.llMessage -> {
                fragmentProfileMainBinding!!.llBusinessProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llTicketScanning.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llCustomApp.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llPersonalProfile.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyMedia.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMyTickets.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                fragmentProfileMainBinding!!.llMessage.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_STRIPE_PERSONAL && resultCode == Activity.RESULT_OK) {
            val fragment = adapter!!.getItem(0)
            fragment.onActivityResult(requestCode, resultCode, data)
        } else if (requestCode == REQUEST_CODE_STRIPE_BUSSINESS && resultCode == Activity.RESULT_OK) {
            val fragment = adapter!!.getItem(1)
            fragment.onActivityResult(requestCode, resultCode, data)
        } else if (requestCode == 963 && resultCode == Activity.RESULT_OK) {
            val fragment = adapter!!.getItem(5)
            fragment.onActivityResult(requestCode, resultCode, data)

        }
    }

    companion object {

        lateinit var instance: ProfileMainFragment

        var fragmentProfileMainBinding: FragmentProfileMainBinding? = null
        private var isFromSideMenu: Boolean = false

        fun newInstance(isFromMenu: Boolean): ProfileMainFragment {
            isFromSideMenu = isFromMenu
            instance = ProfileMainFragment()
            return instance
        }
    }
}
