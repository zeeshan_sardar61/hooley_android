package com.hooleyapp.hooley.fragments.media

import android.app.Dialog
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.activites.VideoPlayerActivity
import com.hooleyapp.hooley.adapters.AdapterSpinner
import com.hooleyapp.hooley.adapters.AdapterViewPostMedia
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.databinding.FragmentViewPostMediaBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.event.UpdatePostFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GetReportEventModel
import com.hooleyapp.hooley.model.PersonalProfileModel
import com.hooleyapp.hooley.others.*
import com.hooleyapp.hooley.services.MediaWebService
import java.util.*

/**
 * Created by adilmalik on 18/10/2018.
 */

class ViewPostMediaFragment : BaseFragment(), AdapterViewPostMedia.IViewPostClickListener, View.OnClickListener {
    var mediaService = MediaWebService()
    internal var binding: FragmentViewPostMediaBinding? = null
    private var adapter: AdapterViewPostMedia? = null
    private var morePostDialogeFragment: MoreDialogeFragment? = null
    private var showMediaReportEventDialog: Dialog? = null
    private var spinnerAdapter: AdapterSpinner? = null
    private var reasonMediaItemPosition = -1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_view_post_media, container, false)
        HooleyMain.activity!!.supportActionBar!!.hide()
        setListener()
        setData()
        initPager()
        setMoreCallBackListener()
        return binding!!.root
    }


    fun setData() {
        try {
            if (!TextUtils.isEmpty(objectItem.profilePic))
                Glide.with(HooleyMain.activity!!).load(objectItem.profilePic).into(binding!!.ivHost)
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }
        binding!!.tvEventName.text = objectItem.eventName
        binding!!.tvHostName.text = objectItem.fullName
        binding!!.tvPostTime.text = DateUtils.convertDateToTimeAgo(DateUtils.getLocalDate(objectItem.postedTime)) + ")"
        binding!!.tvFav.text = objectItem.favoriteCount.toString()
        binding!!.tvLike.text = objectItem.likesCount.toString()
        binding!!.tvShare.text = objectItem.shareCounts.toString()
        binding!!.tvComment.text = objectItem.commentsCount.toString()
        if (objectItem.isLiked) {
            binding!!.ivLike.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_red_heart)
        } else {
            binding!!.ivLike.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_unlike_white)
        }
        if (objectItem.isFavorited) {
            binding!!.ivFav.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_fav)
        } else {
            binding!!.ivFav.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_un_fav_white)
        }
    }

    private fun initPager() {
        adapter = AdapterViewPostMedia(objectItem.mediaFiles)
        adapter!!.setmListener(this)
        binding!!.vpMediaPost.adapter = adapter
        //        userSearchbinding.tlMediaPost.setupWithViewPager(userSearchbinding.vpMediaPost, true);
    }

    fun setListener() {
        binding!!.ivPostMore.setOnClickListener(this)
        binding!!.ivLike.setOnClickListener(this)
        binding!!.ivComment.setOnClickListener(this)
        binding!!.ivFav.setOnClickListener(this)
        binding!!.ivClose.setOnClickListener(this)
        binding!!.ivShare.setOnClickListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null) {
            binding!!.unbind()
        }
        HooleyMain.activity!!.supportActionBar!!.show()
    }

    override fun onClickVideo(position: Int) {
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, VideoPlayerActivity.newInstance(objectItem.mediaFiles[position].mediaUrl!!), "VideoPlayerActivity")
    }

    private fun showReportEventDialog(list: ArrayList<PersonalProfileModel.myObject>, mediaId: String) {
        showMediaReportEventDialog = Dialog(HooleyMain.activity!!)
        showMediaReportEventDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        showMediaReportEventDialog!!.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        showMediaReportEventDialog!!.setContentView(R.layout.dialog_report_event)
        val btnReport = showMediaReportEventDialog!!.findViewById<TextView>(R.id.btnReport)
        val edtMessage = showMediaReportEventDialog!!.findViewById<EditText>(R.id.edtMessage)
        val spSelectReason = showMediaReportEventDialog!!.findViewById<Spinner>(R.id.spSelectReason)
        spinnerAdapter = AdapterSpinner(activity, R.layout.sp_gender_group_item, list)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spSelectReason.adapter = spinnerAdapter
        spSelectReason.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {


            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                reasonMediaItemPosition = position
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
        btnReport.setOnClickListener {
            showMediaReportEventDialog!!.dismiss()
            SubmitReport(list[reasonMediaItemPosition].id, edtMessage.text.toString(), mediaId)
        }
        showMediaReportEventDialog!!.show()
    }

    private fun setMoreCallBackListener() {
        val arrayList = ArrayList<String>()

        if (objectItem.isFavorited)
            arrayList.add("Remove From Favorite")
        else
            arrayList.add("Add to Favorites")

        if (objectItem.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
            arrayList.add("Edit Photo")

        arrayList.add("Download/Share Photo")


        if (objectItem.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
            arrayList.add("Delete Photo")
        else
            arrayList.add("Report Photo")
        morePostDialogeFragment = MoreDialogeFragment.Builder(activity!!).setTitle("More").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    mediaFav(objectItem.eventId, objectItem.imageId, !objectItem.isFavorited)

                    if (objectItem.isFavorited) {
                        var count = Integer.parseInt(binding!!.tvFav.text.toString())
                        count--
                        binding!!.tvFav.text = Integer.toString(count)
                        binding!!.ivFav.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_un_fav_white)
                        morePostDialogeFragment!!.dismiss()
                        arrayList[0] = "Add to Favorites"

                    } else {
                        var count = Integer.parseInt(binding!!.tvFav.text.toString())
                        count++
                        binding!!.tvFav.text = Integer.toString(count)
                        binding!!.ivFav.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_fav)
                        morePostDialogeFragment!!.dismiss()
                        arrayList[0] = "Remove From Favorite"
                    }
                }
                1 -> {
                    morePostDialogeFragment!!.dismiss()
                    if (objectItem.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true)) {
                        HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                        (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, UpdatePostFragment(objectItem.eventId!!, objectItem.imageId), "UpdatePostFragment")
                    } else
                        showShareDialog()

                }
                2 -> {
                    morePostDialogeFragment!!.dismiss()
                    if (arrayList.size == 3) {
                        getMediaReportEvent(objectItem.imageId)
                    } else {
                        showShareDialog()
                    }
                }
                3 -> {
                    morePostDialogeFragment!!.dismiss()
                    if (objectItem.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
                        deleteMedia(objectItem.imageId)
                    else
                        getMediaReportEvent(objectItem.imageId)
                }
            }
        }.create()
    }

    private fun showShareDialog() {
        object : GetBitmapTask() {
            override fun onPostExecute(bmp: Bitmap) {
                super.onPostExecute(bmp)
                (HooleyMain.activity as HooleyMain).shareWithImage(bmp, objectItem.eventName!!, objectItem.imageCaption, objectItem.eventId!!, objectItem.imageId)
            }
        }.execute(objectItem.mediaFiles[binding!!.vpMediaPost.currentItem].thumbnailUrl)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivPostMore -> {
                if (!morePostDialogeFragment!!.isAdded)
                    morePostDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
            }
            R.id.ivLike -> {
                var count = Integer.parseInt(binding!!.tvLike.text.toString())
                if (objectItem.isLiked) {
                    binding!!.ivLike.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_unlike_white)
                    if (count != 0)
                        count--
                    binding!!.tvLike.text = Integer.toString(count)
                } else {
                    binding!!.ivLike.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_red_heart)
                    count++
                    binding!!.tvLike.text = Integer.toString(count)
                }
                likePost(objectItem.eventId, objectItem.imageId)
            }
            R.id.ivFav -> {
                var countFav = Integer.parseInt(binding!!.tvFav.text.toString())
                if (objectItem.isFavorited) {
                    binding!!.ivFav.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_un_fav_white)
                    if (countFav != 0)
                        countFav--
                    binding!!.tvFav.text = Integer.toString(countFav)
                } else {
                    binding!!.ivFav.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_fav)
                    countFav++
                    binding!!.tvFav.text = Integer.toString(countFav)
                }
                mediaFav(objectItem.eventId, objectItem.imageId, !objectItem.isFavorited)
            }
            R.id.ivShare -> showShareDialog()
            R.id.ivComment -> {
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                if (!isFromComment)
                    (HooleyMain.activity as ActivityBase).callFragment(R.id.container, AllFeedsSinglePostCommentFragment.newInstance(objectItem.eventId!!, objectItem.imageId), "AllFeedsSinglePostCommentFragment")
            }
            R.id.ivClose -> HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
        }
    }


    // calling api
    private fun deleteMedia(imageId: String) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        mediaService.deleteMedia(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
            }

            override fun failure(message: String) {
                try {

                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        }, imageId, objectItem.eventId!!)
    }

    private fun getMediaReportEvent(mediaId: String) {
        mediaService.getMediaReportEvent(object : GenericCallback<GetReportEventModel> {
            override fun success(result: GetReportEventModel) {
                showReportEventDialog(result.mediaReportReasonsArrayList, mediaId)
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun SubmitReport(reasonId: String?, reportMessage: String, mediaId: String) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        mediaService.submitReport(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                //                Util.showToastMessage(HooleyMain.activity, "Reported");
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        }, reasonId!!, reportMessage, mediaId, objectItem.eventId!!)
    }

    private fun likePost(eventId: String?, imageId: String) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        mediaService.likePost(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                if (objectItem.isLiked) {
                    objectItem.isLiked = false
                    objectItem.likesCount = objectItem.likesCount - 1
                    binding!!.tvLike.text = objectItem.likesCount.toString()
                } else {
                    objectItem.isLiked = true
                    objectItem.likesCount = objectItem.likesCount + 1
                    binding!!.tvLike.text = objectItem.likesCount.toString()
                }
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        }, eventId!!, imageId)
    }

    private fun mediaFav(eventId: String?, imageId: String, isFav: Boolean) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        mediaService.mediaFav(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                if (objectItem.isFavorited) {
                    objectItem.isFavorited = false
                    objectItem.favoriteCount = objectItem.favoriteCount - 1
                    binding!!.tvFav.text = objectItem.favoriteCount.toString()
                } else {
                    objectItem.isFavorited = true
                    objectItem.favoriteCount = objectItem.favoriteCount + 1
                    binding!!.tvFav.text = objectItem.favoriteCount.toString()
                }
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        }, eventId!!, imageId, isFav)
    }

    companion object {

        lateinit var instance: ViewPostMediaFragment
        lateinit var objectItem: EventGalleryFeedModel.EventMediaInfo
        private var isFromComment = false

        fun newInstance(obj: EventGalleryFeedModel.EventMediaInfo): ViewPostMediaFragment {
            objectItem = obj
            instance = ViewPostMediaFragment()
            return instance
        }

        fun newInstance(obj: EventGalleryFeedModel.EventMediaInfo, isComment: Boolean): ViewPostMediaFragment {
            isFromComment = isComment
            objectItem = obj
            instance = ViewPostMediaFragment()
            return instance

        }
    }

}
