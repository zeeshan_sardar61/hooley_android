package com.hooleyapp.hooley.fragments.ticket

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentGiftTicketBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.MyTicketModel
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.others.MessageEvent
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.TicketWebService
import com.hooleyapp.hooley.tables.FriendsTable
import org.greenrobot.eventbus.EventBus
import java.util.*

/**
 * Created by Nauman on 5/8/2018.
 */

class GiftTicketFragment : BaseFragment(), View.OnClickListener {
    var binding: FragmentGiftTicketBinding? = null
    var webService = TicketWebService()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_gift_ticket, container, false)
        HooleyMain.activity!!.supportActionBar!!.show()
        (HooleyMain.activity as HooleyMain).setToolbarTitle("Send as Gift")
        setListener()
        setViews()
        return binding!!.root
    }

    private fun setViews() {
        if (!TextUtils.isEmpty(friendsTable.profilePic))
            Glide.with(HooleyMain.activity!!).load(friendsTable.profilePic).into(binding!!.ivUserProfile)
        binding!!.tvFriendName.text = friendsTable.fullName
        binding!!.tvAddress.text = friendsTable.cityName + ", " + friendsTable.stateName
        binding!!.tvEventName.text = ticketObj.eventName
        binding!!.tvTimeStamp.text = DateUtils.displayDateOnly(DateUtils.getLocalDate(ticketObj.startTime))
        binding!!.tvEventAddress.text = ticketObj.eventAddress
        binding!!.tvCatName.text = "Category(" + ticketObj.ticketCategoryName + ")"

        if (!TextUtils.isEmpty(ticketObj.ticketList[position].qrCodeUrl)) {
            val bm = Util.createImageFromString(HooleyMain.activity, ticketObj.ticketList[position].qrCodeUrl)
            if (bm != null)
                binding!!.ivQrCode.setImageBitmap(bm)
        }
    }

    private fun setListener() {
        binding!!.btnConfirm.setOnClickListener(this)
    }


    override fun onDestroy() {
        super.onDestroy()
        if (binding != null) {
            binding!!.unbind()
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnConfirm -> giftTicket()
        }
    }

    fun giftTicket() {
        if (binding != null)
            binding!!.pbTicket.visibility = View.VISIBLE
        val idsList = ArrayList<Int>()
        idsList.add(ticketObj.ticketList[position].id!!)
        webService.sendTicketGift(friendsTable.userId, idsList, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                binding!!.pbTicket.visibility = View.GONE
                EventBus.getDefault().post(MessageEvent("ticketRefresh"))
                Util.showToastMessage(HooleyMain.activity!!, "Ticket Gifted")
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
            }

            override fun failure(message: String) {
                binding!!.pbTicket.visibility = View.GONE
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    companion object {


        lateinit var instance: GiftTicketFragment
        lateinit var friendsTable: FriendsTable
        private var position: Int = 0
        lateinit var ticketObj: MyTicketModel.MyTicketsList

        fun newInstance(friendObj: FriendsTable, myObj: MyTicketModel.MyTicketsList, pos: Int): GiftTicketFragment {
            instance = GiftTicketFragment()
            friendsTable = friendObj
            ticketObj = myObj
            position = pos
            return instance
        }
    }

}
