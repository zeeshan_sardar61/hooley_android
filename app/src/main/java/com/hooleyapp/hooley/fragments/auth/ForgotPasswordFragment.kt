package com.hooleyapp.hooley.fragments.auth

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyAuth
import com.hooleyapp.hooley.databinding.FragmentForgotPasswordBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.AuthWebService


/**
 * Created by Nauman on 11/30/2017.
 */

class ForgotPasswordFragment : BaseFragment(), View.OnClickListener {
    var service = AuthWebService()
    internal var binding: FragmentForgotPasswordBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forgot_password, container, false)
        //        HooleyAuth.activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        binding!!.ivBack.setOnClickListener {
            HooleyAuth.activity.onBackPressed()
        }
        setListener()
        return binding!!.root
    }

    private fun setListener() {
        binding!!.btnForgotPass.setOnClickListener(this)
    }

    private fun validInput(): Boolean {
        if (Util.isContentNull(binding!!.edEmail.text.toString())) {
            binding!!.edEmail.requestFocus()
            binding!!.edEmail.error = getString(R.string.required)
            return false
        } else if (!Util.isEmailValid(binding!!.edEmail.text.toString().trim())) {
            binding!!.edEmail.requestFocus()
            binding!!.edEmail.error = getString(R.string.invalid_email)
            return false
        } else {
            Util.hideKeyboard(activity!!)
            return true
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnForgotPass -> if (validInput())
                forgotPassword()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    private fun forgotPassword() {
        if (!NetworkUtil.isInternetConnected(HooleyAuth.activity)) {
            HooleyAuth.activity.showSuccessDialog(Constants.NO_NETWORK)
            return
        }
        HooleyAuth.activity.showDialog()
        service.forgetPassword(binding!!.edEmail.text.toString().trim(), object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyAuth.activity.removeDialog()
                HooleyAuth.activity.supportFragmentManager.popBackStackImmediate()
                (HooleyAuth.activity as ActivityBase).callFragmentWithReplace(R.id.authContainer, ConfirmForgotPasswordFragment.newInstance(binding!!.edEmail.text.toString()), "ConfirmForgotPasswordFragment")

            }

            override fun failure(message: String) {
                HooleyAuth.activity.removeDialog()
                try {
                    HooleyAuth.activity.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    companion object {

        lateinit var instance: ForgotPasswordFragment

        fun newInstance(): ForgotPasswordFragment {
            instance = ForgotPasswordFragment()
            return instance
        }
    }
}
