package com.hooleyapp.hooley.fragments.event

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventAnnouncementModel
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.ICallBackUri
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.Constants.CAMERA_RQ
import com.hooleyapp.hooley.others.Constants.IMAGE_PICKER_SELECT
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.requestmodel.SetEventAnnouncementRequestModel
import com.hooleyapp.hooley.services.EventWebService
import java.util.*

/**
 * Created by Nauman on 2/19/2018.
 */
@SuppressLint("ValidFragment")
class AddEventAnnouncementFragment(var eventId: String, var announcement: EventAnnouncementModel.Announcement?, var isEdit: Boolean) : BaseFragment(), View.OnClickListener {

    internal lateinit var binding: com.hooleyapp.hooley.databinding.FragmentAddAnnouncementBinding
    private var moreDialogeFragment: MoreDialogeFragment? = null
    var eventService = EventWebService()
    private var broadcastAll: Boolean = true
    private var broadcastHereNow: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_announcement, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
        if (isEdit) {
            binding.btnPostAnnouncement.text = "Update Announcement"
            if (!announcement!!.imageUrl!!.isEmpty()) {
                binding.ivBackgroundAnn.load(announcement!!.imageUrl!!)
                binding.ivSelectPicture.load(announcement!!.imageUrl!!)
                binding.ivCameraUpdate.visibility = VISIBLE
            }
            binding.etAnnouncement.setText(announcement!!.description!!)
            if (announcement!!.broadcastAll) {
                broadcastAll = announcement!!.broadcastAll
                broadcastHereNow = announcement!!.broadcastHereNow
                binding.rbBroadcastAll.isChecked = true
                binding.rbBroadcastHereNow.isChecked = false
            } else if (announcement!!.broadcastHereNow) {
                broadcastAll = announcement!!.broadcastAll
                broadcastHereNow = announcement!!.broadcastHereNow
                binding.rbBroadcastAll.isChecked = false
                binding.rbBroadcastHereNow.isChecked = true
            }

        }
    }


    private fun setListener() {
        binding.ivSelectPicture.setOnClickListener(this)
        binding.llBroadcastAll.setOnClickListener(this)
        binding.llBroadcastHereNow.setOnClickListener(this)
        binding.rbBroadcastAll.setOnClickListener(this)
        binding.rbBroadcastHereNow.setOnClickListener(this)
        binding.btnPostAnnouncement.setOnClickListener(this)
    }


    private fun setMoreCallerBackListener() {
        val arrayList = ArrayList<String>()
        arrayList.add("Photo")
        arrayList.add("Gallery")
        moreDialogeFragment = MoreDialogeFragment.Builder(activity!!).setTitle("Select").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, _ ->
            when (position) {
                0 -> {
                    moreDialogeFragment!!.dismiss()
                    HooleyMain.activity!!.startCamera(CAMERA_RQ)
                }
                1 -> {
                    moreDialogeFragment!!.dismiss()
                    HooleyMain.activity!!.showGalleryImage(IMAGE_PICKER_SELECT)
                }
            }
        }.create()

        if (!moreDialogeFragment!!.isAdded)
            moreDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivSelectPicture -> {
                setMoreCallerBackListener()
            }
            R.id.llBroadcastAll -> {
                if (binding.rbBroadcastHereNow.isChecked) {
                    binding.rbBroadcastHereNow.isChecked = false
                    binding.rbBroadcastAll.isChecked = true
                } else if (!binding.rbBroadcastHereNow.isChecked) {
                    binding.rbBroadcastAll.isChecked = true
                } else {
                    binding.rbBroadcastAll.isChecked = true
                }
                broadcastAll = binding.rbBroadcastAll.isChecked
                broadcastHereNow = binding.rbBroadcastHereNow.isChecked
            }
            R.id.llBroadcastHereNow -> {
                if (binding.rbBroadcastAll.isChecked) {
                    binding.rbBroadcastAll.isChecked = false
                    binding.rbBroadcastHereNow.isChecked = true
                } else if (!binding.rbBroadcastAll.isChecked) {
                    binding.rbBroadcastHereNow.isChecked = true
                } else {
                    binding.rbBroadcastHereNow.isChecked = true
                }
                broadcastAll = binding.rbBroadcastAll.isChecked
                broadcastHereNow = binding.rbBroadcastHereNow.isChecked
            }
            R.id.rbBroadcastAll -> {
                if (binding.rbBroadcastHereNow.isChecked) {
                    binding.rbBroadcastHereNow.isChecked = false
                    binding.rbBroadcastAll.isChecked = true
                } else if (!binding.rbBroadcastHereNow.isChecked) {
                    binding.rbBroadcastAll.isChecked = true
                } else {
                    binding.rbBroadcastAll.isChecked = true
                }
                broadcastAll = binding.rbBroadcastAll.isChecked
                broadcastHereNow = binding.rbBroadcastHereNow.isChecked
            }
            R.id.rbBroadcastHereNow -> {
                if (binding.rbBroadcastAll.isChecked) {
                    binding.rbBroadcastAll.isChecked = false
                    binding.rbBroadcastHereNow.isChecked = true
                } else if (!binding.rbBroadcastAll.isChecked) {
                    binding.rbBroadcastHereNow.isChecked = true
                } else {
                    binding.rbBroadcastHereNow.isChecked = true
                }
                broadcastAll = binding.rbBroadcastAll.isChecked
                broadcastHereNow = binding.rbBroadcastHereNow.isChecked
            }
            R.id.btnPostAnnouncement -> {
                if (validInput()) {
                    HooleyMain.activity!!.hideKeyboard()
                    postAnnouncement()
                } else {
                    HooleyMain.activity!!.showSuccessDialog("Please enter the announcement")
                }
            }
        }
    }

    private fun postAnnouncement() {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            HooleyMain.activity!!.showSuccessDialog(Constants.NO_NETWORK)
            return
        }
        HooleyMain.activity!!.showDialog()
        if (isEdit) {
            eventService.addAnnouncement(mediaUri, SetEventAnnouncementRequestModel(announcement!!.id!!, HooleyApp.db.getString(Constants.USER_ID), Integer.parseInt(eventId), binding.etAnnouncement.text.toString(), broadcastAll, broadcastHereNow), object : GenericCallback<GeneralModel> {
                override fun success(result: GeneralModel) {
                    HooleyMain.activity!!.removeDialog()
                    HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                }

                override fun failure(message: String) {
                    HooleyMain.activity!!.removeDialog()
                    HooleyMain.activity!!.showSuccessDialog(message)
                }
            })
        } else {
            eventService.addAnnouncement(mediaUri, SetEventAnnouncementRequestModel(0, HooleyApp.db.getString(Constants.USER_ID), Integer.parseInt(eventId), binding.etAnnouncement.text.toString(), broadcastAll, broadcastHereNow), object : GenericCallback<GeneralModel> {
                override fun success(result: GeneralModel) {
                    HooleyMain.activity!!.removeDialog()
                    HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                }

                override fun failure(message: String) {
                    HooleyMain.activity!!.removeDialog()
                    HooleyMain.activity!!.showSuccessDialog(message)
                }

            })
        }
    }

    private fun validInput(): Boolean {
        if (!Util.checkIfName(binding.etAnnouncement.text.toString())) {
            return false
        } else if (binding.etAnnouncement.text.toString().isNotEmpty()) {
            return true
        }
        return false
    }

    private var mediaUri: Uri? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAMERA_RQ) {
                HooleyMain.activity!!.processCapturedPhoto(object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        mediaUri = HooleyMain.activity!!.compressFile(result!!)
                        binding.ivBackgroundAnn.setImageBitmap(HooleyMain.activity!!.handleSamplingAndRotationBitmap(result))
                        binding.ivSelectPicture.setImageBitmap(HooleyMain.activity!!.handleSamplingAndRotationBitmap(result))
                        binding.ivCameraUpdate.visibility = VISIBLE
                    }

                })
            } else if (requestCode == IMAGE_PICKER_SELECT) {
                HooleyMain.activity!!.processGalleryPhoto(data!!, object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        mediaUri = HooleyMain.activity!!.compressFile(result!!)
                        binding.ivBackgroundAnn.setImageBitmap(HooleyMain.activity!!.handleSamplingAndRotationBitmap(result))
                        binding.ivSelectPicture.setImageBitmap(HooleyMain.activity!!.handleSamplingAndRotationBitmap(result))
                        binding.ivCameraUpdate.visibility = VISIBLE
                    }

                })
            }
        } else {
            Toast.makeText(HooleyMain.activity, "Action Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

}
