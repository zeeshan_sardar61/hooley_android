package com.hooleyapp.hooley.fragments.event

import android.annotation.SuppressLint
import android.app.Dialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.view.*
import android.view.View.GONE
import android.widget.TextView
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentUpdateEventDateBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.others.TermsAndConditionFragment
import com.hooleyapp.hooley.helper.CustomDateTimePicker
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.EventWebService
import java.util.*
import java.util.concurrent.TimeUnit

class UpdateEventDataTimeFragment : BaseFragment() {

    lateinit var binding: FragmentUpdateEventDateBinding
    var mService = EventWebService()
    private var customDialog: CustomDateTimePicker? = null
    private var previousSelectedDate: Date? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_update_event_date, container, false)
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (HooleyMain.activity!! as HooleyMain).setToolbarTitle("Edit Date & Time")
        binding.tvEventStartDate.text = DateUtils.formatDate(DateUtils.getLocalDate(eventStartDate!!), true)
        binding.tvEventEndDate.text = DateUtils.formatDate(DateUtils.getLocalDate(eventEndDate!!), true)
        if (!canUpdate) {
            binding.tvDateTime.text = "You have already update date and time"
            binding.btnUpdateEvent.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_disable_btn)
            binding.btnUpdateEvent.isEnabled = false
            binding.edtReason.setText(cancelReason)
            binding.edtReason.isEnabled = false
            binding.tbTerms.isEnabled = false
            binding.llStartDate.isEnabled = false
            binding.llEndDate.isEnabled = false
            binding.btnUpdateEvent.visibility = GONE
        }

        binding.btnUpdateEvent.setOnClickListener {
            if (validInput()) {
                HooleyMain.activity!!.showDialog()
//                Log.e("TAG",DateUtils.SendDateToDateUpdate(DateUtils.getDateInUtcUpdate(binding.tvEventStartDate.text.toString()))+"\n"+DateUtils.SendDateToDateUpdate(DateUtils.getDateInUtcUpdate(binding.tvEventEndDate.text.toString())))
                mService.setUpdateEventAction(eventId, 1, binding.tvEventStartDate.text.toString(), binding.tvEventEndDate.text.toString(),
                        true, true, binding.edtReason.text.toString(), object : IWebServiceCallback<GeneralModel> {
                    override fun success(result: GeneralModel) {
                        HooleyApp.db.putInt(Constants.SHOW_LIVE_EVENT_TAB, 2)
                        HooleyMain.activity!!.removeDialog()
                        HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                        HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                        HooleyMain.activity!!.showSuccessDialog("Event Time Updated Successfully")
                    }

                    override fun failure(message: String) {
                        HooleyMain.activity!!.removeDialog()
                        try {
                            HooleyMain.activity!!.showSuccessDialog(message)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }

                    override fun onTokenExpire() {
                        HooleyMain.activity!!.removeDialog()
                        onTokenExpireLogOut()
                    }
                })
            }
        }

        binding.llStartDate.setOnClickListener {
            showCustomDialog(binding.tvEventStartDate)
        }

        binding.llEndDate.setOnClickListener {
            showCustomDialog(binding.tvEventEndDate)
        }

        binding.tvTermsConditions.setOnClickListener {
            (activity as ActivityBase).addFragment(R.id.container, TermsAndConditionFragment.newInstance(true, false), "TermsAndConditionFragment")
        }
    }

    private fun validInput(): Boolean {
        if (TextUtils.isEmpty(binding.edtReason.text.toString())) {
            binding.edtReason.requestFocus()
            HooleyMain.activity!!.showSuccessDialog("Please Enter the reason to update the time")
            return false
        } else {
            return true
        }
    }

    fun showCustomDialog(view: TextView) {
        customDialog = CustomDateTimePicker(HooleyMain.activity!!,
                object : CustomDateTimePicker.ICustomDateTimeListener {
                    override fun onSet(dialog: Dialog, calendarSelected: Calendar,
                                       dateSelected: Date, year: Int, monthFullName: String,
                                       monthShortName: String, monthNumber: Int, date: Int,
                                       weekDayFullName: String, weekDayShortName: String,
                                       hour24: Int, hour12: Int, min: Int, sec: Int,
                                       AM_PM: String) {
                        if (view.id == binding.tvEventEndDate.id) {
                            if (previousSelectedDate != null) {
                                if (TimeUnit.MILLISECONDS.toHours(dateSelected.time - previousSelectedDate!!.time) < 1) {
                                    Util.showToastMessage(HooleyMain.activity!!, "End date is smaller then previous date")
                                    return
                                }
                                if (dateSelected.before(Calendar.getInstance().time)) {
                                    Util.showToastMessage(HooleyMain.activity!!, "Invalid date, Please select future date.")
                                    return
                                }
                            }
                        } else {
                            if (dateSelected.before(Calendar.getInstance().time)) {
                                Util.showToastMessage(HooleyMain.activity!!, "Invalid date, Please select future date.")
                                return
                            }
                            previousSelectedDate = dateSelected
                        }
                        view.text = ""
                        view.text = DateUtils.formatDate(String.format("%04d-%02d-%02d", year, monthNumber + 1, calendarSelected.get(Calendar.DAY_OF_MONTH))
                                + " " + String.format("%02d:%02d", hour12, min) +
                                " " + AM_PM, false)

                    }

                    override fun onCancel() {

                    }
                })
        /**
         * Pass Directly current time format it will return AM and PM if you set
         * false
         */
        customDialog!!.set24HourFormat(false)
        /**
         * Pass Directly current data and time to show when it pop up
         */
        if (TextUtils.isEmpty(view.text.toString()))
            customDialog!!.setDate(Calendar.getInstance())
        else {
            customDialog!!.setDate(DateUtils.stringToDate(DateUtils.SendDateToDate(view.text.toString())))
        }
        //        customDialog.setDate(Calendar.getInstance());
        customDialog!!.showDialog()
    }

    companion object {

        @SuppressLint("StaticFieldLeak")
        private var instance: UpdateEventDataTimeFragment? = null
        var eventId: String = ""
        private var eventStartDate: String? = null
        private var eventEndDate: String? = null
        private var canUpdate: Boolean = false
        private var cancelReason: String = ""

        fun newInstance(eventIds: String, startDate: String, endDate: String, canUpdate_: Boolean, cancelReason_: String): UpdateEventDataTimeFragment {
            instance = UpdateEventDataTimeFragment()
            eventId = eventIds
            eventStartDate = startDate
            eventEndDate = endDate
            canUpdate = canUpdate_
            cancelReason = cancelReason_
            return instance!!
        }
    }

}