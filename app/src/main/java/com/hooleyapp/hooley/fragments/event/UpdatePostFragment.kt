package com.hooleyapp.hooley.fragments.event

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapteUpdatePostMedia
import com.hooleyapp.hooley.adapters.AdapterTags
import com.hooleyapp.hooley.adapters.AdapterTagsFriends
import com.hooleyapp.hooley.apis.ApiRequestJson
import com.hooleyapp.hooley.databinding.FragmentLivePostBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.friends.SelectFriendWithListFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.ICallBackUri
import com.hooleyapp.hooley.interfaces.IInterestDeleteClick
import com.hooleyapp.hooley.interfaces.IPostMediaClickListener
import com.hooleyapp.hooley.model.FriendsModel
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.UpdatePostMediaItem
import com.hooleyapp.hooley.model.event.GetEditPostModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.requestmodel.UpdatePostMediaRequestModel
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.services.MediaWebService
import com.hooleyapp.hooley.tables.FriendsTable

@SuppressLint("ValidFragment")
class UpdatePostFragment constructor(var eventId: String, var postId: String) : BaseFragment(), IPostMediaClickListener, IInterestDeleteClick, View.OnClickListener {

    lateinit var binding: FragmentLivePostBinding
    private var mediaService = MediaWebService()
    private var friendService = FriendWebService()
    private lateinit var moreDialogeFragment: MoreDialogeFragment
    private val REQUEST_CODE_TAG_FRIEND = 965
    private var friendsList: ArrayList<FriendsTable> = ArrayList()
    private var tagFriends: ArrayList<FriendsTable> = ArrayList()
    private var mediaArraylist: ArrayList<UpdatePostMediaItem> = ArrayList()
    private lateinit var objectResponse: GetEditPostModel
    private lateinit var adapterUpdatePostMedia: AdapteUpdatePostMedia
    private lateinit var adapterTagsFriends: AdapterTagsFriends
    private lateinit var selectFriendFragment: SelectFriendWithListFragment
    private var isAddedTag = true
    private var adapterTags: AdapterTags? = null


    init {
        setMoreCallerBackListener()
        getMyFriends()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_live_post, container, false)
        setListener()
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
        inflater!!.inflate(R.menu.save_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.save -> {
                if (mediaArraylist.isNotEmpty()) {
                    Util.hideKeyboard(HooleyMain.activity!!)
                    updatePostMedia()
                } else {
                    HooleyMain.activity!!.showSuccessDialog("Add some media")
                }
                return super.onOptionsItemSelected(item)
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun setListener() {
        binding.rlTagsFriends.setOnClickListener(this)
        binding.btnShare.setOnClickListener(this)
    }

    private fun setData() {
        binding.edtCaption.setText(objectResponse.caption ?: "")
        objectResponse.mediaUrlList.let {
            if (it.isNotEmpty()) {
                for (i in 0 until it.size) {
                    mediaArraylist.add(UpdatePostMediaItem(it[i].url
                            ?: "", null, Constants.TYPE_URL, it[i].isVideo))
                }
            }
        }
        objectResponse.tagList.let {
            if (it.isNotEmpty()) {
                for (i in 0 until it.size) {
                    for (j in 0 until friendsList.size) {
                        Log.d("F_SIZE", friendsList.size.toString())
                        if (it[i].userId == friendsList[j].userId) {
                            friendsList[j].isChecked = true
                            tagFriends.add(friendsList[j])
                        }
                    }
                    Log.d("T_SIZE", friendsList.size.toString())
                }
            }
        }
        setMediaAdapter()
        setTagsFriendsAdapter()
    }

    private fun setMediaAdapter() {
        val manager = GridLayoutManager(HooleyMain.activity, 3)
        binding.rvMedia.layoutManager = manager
        binding.rvMedia.setHasFixedSize(true)
        adapterUpdatePostMedia = AdapteUpdatePostMedia(mediaArraylist)
        adapterUpdatePostMedia.mlistener = this
        binding.rvMedia.adapter = adapterUpdatePostMedia
    }

    private fun setTagsFriendsAdapter() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity!!, R.anim.layout_animation_from_right)
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.HORIZONTAL, false)
        binding.rvTags.layoutManager = manager
        binding.rvTags.layoutAnimation = animation
        adapterTags = AdapterTags(HooleyMain.activity!!, tagFriends)
        adapterTags!!.setmListener(this)
        binding.rvTags.adapter = adapterTags

        if (adapterTags != null)
            adapterTags!!.notifyDataSetChanged()
    }

    private fun setMoreCallerBackListener() {
        val arrayList = ArrayList<String>()
        arrayList.add("Photo")
        arrayList.add("Video")
        arrayList.add("Gallery")
        moreDialogeFragment = MoreDialogeFragment.Builder(HooleyMain.activity!!).setTitle("Select").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, _ ->
            when (position) {
                0 -> {
                    moreDialogeFragment.dismiss()
                    HooleyMain.activity!!.startCamera(Constants.CAMERA_RQ)
                }
                1 -> {
                    moreDialogeFragment.dismiss()
                    HooleyMain.activity!!.startVideoCamera(Constants.VIDEO_RQ)
                }
                2 -> {
                    moreDialogeFragment.dismiss()
                    HooleyMain.activity!!.showGallery(Constants.IMAGE_PICKER_SELECT)
                }
            }
        }.create()

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.rlTagsFriends -> {
                if (friendsList.isNotEmpty()) {
                    selectFriendFragment = SelectFriendWithListFragment(friendsList)
                    selectFriendFragment.setTargetFragment(this@UpdatePostFragment, REQUEST_CODE_TAG_FRIEND)
                    (HooleyMain.activity as ActivityBase).addFragment(R.id.container, selectFriendFragment, "SelectFriendWithListFragment")
                }
            }
            R.id.btnShare -> if (mediaArraylist.isNotEmpty()) {
                Util.hideKeyboard(HooleyMain.activity!!)
                updatePostMedia()
            } else {
                HooleyMain.activity!!.showSuccessDialog("Add some media")
            }
        }
    }

    override fun addMedia() {
        if (!moreDialogeFragment.isAdded)
            moreDialogeFragment.show(HooleyMain.activity!!.supportFragmentManager)
    }


    override fun onClickItem(name: String, position: Int) {
        for (loop in friendsList.indices) {
            if (tagFriends[position].userId == friendsList[loop].userId) {
                friendsList[loop].isChecked = false
            }
        }
        if (tagFriends.isNotEmpty())
            tagFriends.removeAt(position)
        adapterTags!!.notifyDataSetChanged()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                Constants.CAMERA_RQ -> HooleyMain.activity!!.processCapturedPhoto(object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        mediaArraylist.add(UpdatePostMediaItem("", result, Constants.TYPE_URI, false))
                        if (::adapterUpdatePostMedia.isInitialized)
                            adapterUpdatePostMedia.notifyDataSetChanged()
                    }
                })
                Constants.VIDEO_RQ -> HooleyMain.activity!!.processCapturedVideo(object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        mediaArraylist.add(UpdatePostMediaItem("", result, Constants.TYPE_URI, true))
                        if (::adapterUpdatePostMedia.isInitialized)
                            adapterUpdatePostMedia.notifyDataSetChanged()

                    }
                })
                Constants.IMAGE_PICKER_SELECT -> {
                    HooleyMain.activity!!.processGalleryPhoto(data!!, object : ICallBackUri {
                        override fun imageUri(result: Uri?) {
                            if (result!!.toString().contains("mp4"))
                                mediaArraylist.add(UpdatePostMediaItem("", result, Constants.TYPE_URI, true))
                            else
                                mediaArraylist.add(UpdatePostMediaItem("", result, Constants.TYPE_URI, false))
                            if (::adapterUpdatePostMedia.isInitialized)
                                adapterUpdatePostMedia.notifyDataSetChanged()
                        }
                    })
                }
                REQUEST_CODE_TAG_FRIEND -> {
                    friendsList = data!!.getParcelableArrayListExtra("list")
                    if (tagFriends.isNotEmpty())
                        tagFriends.clear()
                    for (item in friendsList) {
                        if (item.isChecked)
                            tagFriends.add(item)
                    }
                    if (adapterTags != null)
                        adapterTags!!.notifyDataSetChanged()
                }
            }
        } else if (requestCode == REQUEST_CODE_TAG_FRIEND && resultCode == Activity.RESULT_CANCELED) {
            if (adapterTags != null)
                adapterTags!!.notifyDataSetChanged()
        } else {

            Toast.makeText(HooleyMain.activity, "Action Cancelled", Toast.LENGTH_SHORT).show()
        }
    }


    // API Call
    private fun getMyFriends() {
        friendService.getMyFriendsList(object : GenericCallback<FriendsModel> {
            override fun success(result: FriendsModel) {
                friendsList = result.friendsList
                getEditPost()
            }

            override fun failure(message: String) {
                getEditPost()
            }
        })
    }

    private fun getEditPost() {
        HooleyMain.activity!!.showDialog()
        mediaService.getEditPost(eventId, postId, object : GenericCallback<GetEditPostModel> {
            override fun success(result: GetEditPostModel) {
                HooleyMain.activity!!.removeDialog()
                objectResponse = result
                setData()
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.removeDialog()
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })

    }


    private fun updatePostMedia() {
        if (NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            HooleyMain.activity!!.showDialog()
            mediaService.updatePost(mediaArraylist, ApiRequestJson.getInstance().updatePostMedia(eventId, postId, binding.edtCaption.text.toString(), getTagList(), mediaArraylist), object : GenericCallback<GeneralModel> {
                override fun success(result: GeneralModel) {
                    HooleyMain.activity!!.removeDialog()
                    HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                }

                override fun failure(message: String) {
                    HooleyMain.activity!!.removeDialog()
                    try {
                        HooleyMain.activity!!.showSuccessDialog(message)
                    } catch (ex: NullPointerException) {
                        ex.printStackTrace()
                    }

                }
            })
        } else {
            HooleyMain.activity!!.showSuccessDialog(Constants.NO_NETWORK)

        }
    }

    private fun getTagList(): ArrayList<UpdatePostMediaRequestModel.TagUserItem> {
        var userList: ArrayList<UpdatePostMediaRequestModel.TagUserItem> = arrayListOf()
        var isItemAdded = false
        objectResponse.tagList.let {
            if (it.isNotEmpty()) {
                for (i in 0 until it.size) {
                    for (j in 0 until tagFriends.size) {
                        if (it[i].userId == tagFriends[j].userId) {
                            tagFriends[j].isChecked = false
                            isItemAdded = true
                        }
                    }
                    if (isItemAdded)
                        userList.add(UpdatePostMediaRequestModel.TagUserItem(it[i].userId, Constants.TYPE_TAG_NONE))
                    else
                        userList.add(UpdatePostMediaRequestModel.TagUserItem(it[i].userId, Constants.TYPE_TAG_DELETED))
                    isItemAdded = false
                }
            }
        }
        for (item in tagFriends) {
            if (item.isChecked)
                userList.add(UpdatePostMediaRequestModel.TagUserItem(item.userId, Constants.TYPE_TAG_NEW))
        }
        return userList
    }

}