package com.hooleyapp.hooley.fragments.event

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterPastEventFriendList
import com.hooleyapp.hooley.databinding.PastEventListFriendFragmentBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IFriendContactClickListener
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GetPastEventGuestListModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

/**
 * Created by Nauman on 5/18/2018.
 */

class PastEventListFriendFragment : BaseFragment(), View.OnClickListener, TextWatcher, IFriendContactClickListener, ViewAvatarFragment.CardCallBackListener {
    internal var guestList: MutableList<FriendsTable>? = null
    private val mListSearch = ArrayList<FriendsTable>()
    var binding: PastEventListFriendFragmentBinding? = null
    private var adapterPastEventFriendList: AdapterPastEventFriendList? = null
    private var objectResponse: GetPastEventGuestListModel? = null
    private var service: FriendWebService? = null
    var friendService = FriendWebService()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.past_event_list_friend_fragment, container, false)
        setListener()
        getPastGuestList()
        return binding!!.root
    }

    private fun setListener() {
        binding!!.edtSearch.addTextChangedListener(this)
        binding!!.ivClose.setOnClickListener(this)
    }


    private fun getPastGuestList() {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        HooleyMain.activity!!.showDialog()
        service = FriendWebService()
        service!!.getPastGuestList(eventId!!, object : GenericCallback<GetPastEventGuestListModel> {
            override fun success(result: GetPastEventGuestListModel) {
                HooleyMain.activity!!.removeDialog()
                objectResponse = result
                setRecyclerView()
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    binding!!.rlSearchBar.visibility = View.GONE
                    binding!!.rlAllSelect.visibility = View.GONE
                    binding!!.rvFriends.visibility = View.GONE
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.view.visibility = View.GONE
                    binding!!.tvNoData.text = message
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }


    private fun setRecyclerView() {
        if (objectResponse!!.guestList == null)
            return
        binding!!.tvHooleyFriends.text = eventName
        if (guestList == null)
            guestList = ArrayList()
        guestList!!.clear()
        guestList!!.addAll(objectResponse!!.guestList)
        binding!!.tvPeopleCount.text = if (guestList!!.size == 1) {
            "${guestList!!.size} Person "
        } else {
            "${guestList!!.size} People "
        }
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        binding!!.rvFriends.layoutManager = manager
        binding!!.rvFriends.layoutAnimation = animation
        guestList!!.sortBy { it.fullName?.toString()!!.toLowerCase() }
        adapterPastEventFriendList = AdapterPastEventFriendList(guestList)
        adapterPastEventFriendList!!.mListener = this
        binding!!.rvFriends.adapter = adapterPastEventFriendList
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivClose -> binding!!.edtSearch.setText("")
        }
    }

    override fun reloadFragment(flag: Boolean) {
        if (flag)
            getPastGuestList()
    }

    override fun onClickAddFriend(position: Int) {
        guestList!![position].status = Constants.TYPE_PENDING_SENT
        adapterPastEventFriendList!!.notifyDataSetChanged()
        sendFriendRequest(position)
    }

    override fun onClickAcceptFriend(position: Int) {
        acceptFriendRequest(position)
    }

    private fun sendFriendRequest(position: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        val friendList = ArrayList<Int>()
        friendList.add(guestList!![position].userId)
        friendService.sendFriendRequest(friendList, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                Util.showToastMessage(HooleyMain.activity!!, "Friend Request Sent")
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun acceptFriendRequest(position: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        friendService.acceptIgnoreFriendRequest(guestList!![position].userId, true, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                guestList!![position].status = Constants.TYPE_FRIEND
                if (adapterPastEventFriendList != null)
                    adapterPastEventFriendList!!.notifyDataSetChanged()
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.showSuccessDialog(message)
            }
        })

    }

    override fun onClickItem(position: Int) {
        var avatarfragment = ViewAvatarFragment(guestList!![position].userId)
        avatarfragment.mListener = this
        avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
    }

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

    }

    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

    }

    override fun afterTextChanged(s: Editable) {
        if (s.isNotEmpty()) {
            if (guestList == null)
                return
            guestList!!.clear()
            if (objectResponse != null && objectResponse!!.guestList != null && objectResponse!!.guestList.size > 0) {
                if (mListSearch.size > 0)
                    mListSearch.clear()
                mListSearch.addAll(objectResponse!!.guestList)
            }
            for (i in mListSearch.indices) {
                if (mListSearch[i].fullName!!.toLowerCase().contains(s.toString().toLowerCase())) {
                    guestList!!.add(mListSearch[i])
                }
            }
            if (guestList!!.size == 0) {
                binding!!.tvNoData.visibility = View.VISIBLE
                binding!!.tvNoData.text = "No Record Found"
            } else {
                binding!!.tvNoData.visibility = View.GONE
            }
            binding!!.tvPeopleCount.text = if (guestList!!.size == 1) {
                "${guestList!!.size} Person "
            } else {
                "${guestList!!.size} People "
            }
            if (adapterPastEventFriendList != null)
                adapterPastEventFriendList!!.notifyDataSetChanged()
        } else {
            binding!!.tvNoData.visibility = View.GONE
            if (mListSearch == null)
                return
            if (guestList != null)
                guestList!!.clear()
            guestList!!.addAll(mListSearch)
            binding!!.tvPeopleCount.text = if (guestList!!.size == 1) {
                "${guestList!!.size} Person "
            } else {
                "${guestList!!.size} People "
            }
            if (adapterPastEventFriendList != null)
                adapterPastEventFriendList!!.notifyDataSetChanged()
        }
    }

    companion object {


        lateinit var instance: PastEventListFriendFragment
        private var eventName: String? = null
        private var eventId: String? = null

        fun newInstance(name: String, id: String): PastEventListFriendFragment {
            eventName = name
            eventId = id
            instance = PastEventListFriendFragment()
            return instance
        }
    }
}
