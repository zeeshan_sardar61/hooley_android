package com.hooleyapp.hooley.fragments.event

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.View.GONE
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.Transformation
import android.widget.AdapterView
import android.widget.RadioGroup
import android.widget.TextView
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.*
import com.hooleyapp.hooley.databinding.FragmentUpdateEventBinding
import com.hooleyapp.hooley.db.DbManager
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.friends.SelectFriendFragment
import com.hooleyapp.hooley.fragments.nearby.GeofenceFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.fragments.ticket.EventTicketSetupFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.ICallBackUri
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.*
import com.hooleyapp.hooley.model.staticData.Category
import com.hooleyapp.hooley.model.staticData.StaticDataModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.EventWebService
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.tables.FriendsTable
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


/**
 * Created by Zeeshan on 15-Feb-18.
 */

class UpdateEventFragment : BaseFragment(), View.OnClickListener, RadioGroup.OnCheckedChangeListener, AdapterView.OnItemClickListener, TextWatcher, View.OnFocusChangeListener, AdapterAddCoHost.onListItemRemovedListener {

    override fun onListSizeChange() {
        binding.btnAddCoHost.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_add)
        binding.btnAddCoHost.isEnabled = true
    }

    private var isStartDate: Boolean = false
    private lateinit var binding: FragmentUpdateEventBinding
    private var adapterCategory: AdapterEventCategory? = null
    private var list: ArrayList<Category>? = null
    private val handler = Handler()
    private var friendList: MutableList<FriendsTable>? = null
    var optionalCatList = ArrayList<String>()
    private var galleryFragment: HooleyGalleryFragment? = null
    private val REQUEST_CODE = 963
    private val REQUEST_CODE_GEOFENCE = 964
    private val REQUEST_CODE_ADD_CO_HOST = 966
    private var adapterOptCategory: AdapterOptionCategory? = null
    private var geoFenceFragment: GeofenceFragment? = null
    var isPublic = true
    var inviteFreinds = false
    private var adapter: AdapterWallpaperPager? = null
    private var isEdit = false
    private var objectResponse: Response<EditEventModel>? = null
    private var service: FriendWebService? = null
    var eventWallpapers: ArrayList<ItemCreateEventWallpaper> = ArrayList()
    var eventService = EventWebService()
    private var imagesArray: ArrayList<String>? = null
    private var selectFriendFragment: SelectFriendFragment? = null
    private var listLatLong: ArrayList<LatLng>? = null
    private var centerLatLngObj: LatLng? = null
    private var customDialog: CustomDateTimePicker? = null
    private var previousSeletedDate: Date? = null
    private var moreDialogeFragment: MoreDialogeFragment? = null
    var listAge: ArrayList<PersonalProfileModel.myObject> = ArrayList()
    private var adapterAge: AdapterEventAges? = null
    var model = StaticDataModel()
    internal var gson = Gson()
    private var eventHashes = ArrayList<String>()
    private var oldHashtagLenth: Int = 0
    var isPaid: Boolean = false
    internal var startDate = ""
    internal var endDate = ""
    var coHostList: ArrayList<FriendsTable> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_update_event, container, false)
        (HooleyMain.activity as HooleyMain).removeBubbleView()
        HooleyApp.db.putInt(Constants.SHOW_LIVE_EVENT_TAB, 2)
        getEditEvent()
        setListener()
        HooleyMain.activity!!.title = "Edit Basic info"
//        setImageCallerBackListener()
        getFriendList()
        setOptEvent()
        setMoreCallerBackListener()
        setupViewPager()
        setHasOptionsMenu(true)
        return binding.root
    }


    fun getAgeList(): ArrayList<PersonalProfileModel.myObject> {
        model = gson.fromJson(HooleyApp.db.getString(Constants.STATIC_MODEL), StaticDataModel::class.java)
        for (i in model.minimumAgeList!!.indices) {
            if (model.minimumAgeList!![i].id.equals(objectResponse!!.body()!!.eventBasicInfo!!.minimumAgeId)) {
                model.minimumAgeList!![i].isActive = true
                ageId = objectResponse!!.body()!!.eventBasicInfo!!.minimumAgeId!!
                selectedAge = model.minimumAgeList!![i].name!!
            }
        }
        listAge.addAll(model.minimumAgeList!!)

        return listAge

    }

    fun getHashes(): ArrayList<String> {
        if (binding.edEventHashes.text.toString() != "#" && !binding.edEventHashes.text.toString().isEmpty()) {
            val splited = binding.edEventHashes.text.toString().split(" ")

            for (i in 0 until splited.size) {
                eventHashes.add(splited[i].substring(1))
            }
        }
        return eventHashes
    }


    private fun setAgeGridEvent() {
        binding.tvMinimumAge.visibility = View.VISIBLE
        binding.tvMinimumAge.text = selectedAge
        adapterAge = AdapterEventAges(HooleyMain.activity!!, listAge)
        binding.gvAge.adapter = adapterAge
    }

    private fun setMoreCallerBackListener() {
        val arrayList = java.util.ArrayList<String>()
        arrayList.add("Camera")
        arrayList.add("Photo Library")
        arrayList.add("Wallpaper")
        moreDialogeFragment = MoreDialogeFragment.Builder(activity!!).setTitle("Select").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, _ ->
            when (position) {
                0 -> {
                    moreDialogeFragment!!.dismiss()
//                    newCameraFun()
                    HooleyMain.activity!!.startCamera(Constants.CAMERA_RQ)
                }
                1 -> {
                    moreDialogeFragment!!.dismiss()
//                    showVideoCamera()
                    HooleyMain.activity!!.showGalleryImage(Constants.IMAGE_PICKER_SELECT)
                }
                2 -> {
                    moreDialogeFragment!!.dismiss()
//                    openGalleryForMedia()
                    galleryFragment = HooleyGalleryFragment.newInstance(isEdit)
                    galleryFragment!!.setTargetFragment(this@UpdateEventFragment, REQUEST_CODE)
                    (HooleyMain.activity as ActivityBase).addFragment(R.id.container, galleryFragment!!, "HooleyGalleryFragment")
                }
            }
        }.create()

    }


    private fun setListener() {
        binding.llStartDate.setOnClickListener(this)
        binding.ivUploadPic.setOnClickListener(this)
        binding.llEndDate.setOnClickListener(this)
        binding.ivEventCover.setOnClickListener(this)
        binding.ivDelete.setOnClickListener(this)
        binding.ivEdit.setOnClickListener(this)
        binding.ivCollapseEvent.setOnClickListener(this)
        binding.ivExpandEvent.setOnClickListener(this)
        binding.ivCollapseAge.setOnClickListener(this)
        binding.ivExpandAge.setOnClickListener(this)
        binding.btnAddCoHost.setOnClickListener(this)
        binding.btnAddCategory.setOnClickListener(this)
        binding.rgEventVisibility.setOnCheckedChangeListener(this)
        binding.rgInviteFriend.setOnCheckedChangeListener(this)
        binding.btnSaveChanges.setOnClickListener(this)
        binding.edEventHashes.addTextChangedListener(this)
        binding.edEventHashes.onFocusChangeListener = this
        binding.rlLocation.setOnClickListener(this)
        binding.btnEventPreview.setOnClickListener(this)
        binding.btnSaveUnPublish.setOnClickListener(this)
    }

    override fun afterTextChanged(p0: Editable?) {
        if (binding.edEventHashes.text!!.isNotEmpty()) {

            val lastChar = binding.edEventHashes.text.toString().substring(binding.edEventHashes.text!!.length - 1)
            if (lastChar == " ") {
                if (oldHashtagLenth < binding.edEventHashes.text.toString().length) {
                    binding.edEventHashes.text = Editable.Factory.getInstance().newEditable(binding.edEventHashes.text.toString() + "#")
                    binding.edEventHashes.setSelection(binding.edEventHashes.text.toString().length)
                }
            }
        }
    }

    override fun beforeTextChanged(sequence: CharSequence?, start: Int, count: Int, after: Int) {
        if (binding.edEventHashes.hasFocus()) {
            oldHashtagLenth = binding.edEventHashes.text!!.length

        }
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        if (binding.edEventHashes.hasFocus()) {
            if (binding.edEventHashes.text!!.isEmpty()) {
                binding.edEventHashes.text = Editable.Factory.getInstance().newEditable(binding.edEventHashes.text.toString() + "#")
                binding.edEventHashes.setSelection(binding.edEventHashes.text.toString().length)
            }
        }

    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        when (v!!.id) {
            R.id.edEventHashes ->
                if (hasFocus) {
                    if (TextUtils.isEmpty(binding.edEventHashes.text)) {
                        binding.edEventHashes.setText("#")
                    }
                } else {
                    if (binding.edEventHashes.text.toString() == "#") {
                        binding.edEventHashes.setText("")
                    }
                }
        }

    }

    private fun getFriendList() {
        if (friendList == null)
            friendList = ArrayList()
        if (friendList!!.size > 0)
            friendList!!.clear()
        friendList = DbManager.getInstance().friendList
        if (friendList!!.size > 0) {
            getFriends(false)
        } else {
            getFriends(false)
        }
    }

    private fun setGridEvent() {
        adapterCategory = AdapterEventCategory(HooleyMain.activity!!, list!!)
        binding.gvEventType.adapter = adapterCategory
        binding.gvEventType.onItemClickListener = this
    }

    private fun setOptEvent() {
        adapterOptCategory = AdapterOptionCategory(HooleyMain.activity!!, optionalCatList)
        binding.gvEventOptCat.adapter = adapterOptCategory
        binding.gvEventOptCat.onItemClickListener = this
    }

    fun showCustomDialog(view: TextView) {
        customDialog = CustomDateTimePicker(HooleyMain.activity!!,
                object : CustomDateTimePicker.ICustomDateTimeListener {
                    override fun onSet(dialog: Dialog, calendarSelected: Calendar,
                                       dateSelected: Date, year: Int, monthFullName: String,
                                       monthShortName: String, monthNumber: Int, date: Int,
                                       weekDayFullName: String, weekDayShortName: String,
                                       hour24: Int, hour12: Int, min: Int, sec: Int,
                                       AM_PM: String) {
                        if (view.id == binding.tvEventEndDate.id) {
                            if (previousSeletedDate != null) {
                                if (TimeUnit.MILLISECONDS.toHours(dateSelected.time - previousSeletedDate!!.time) < 1) {
                                    Util.showToastMessage(HooleyMain.activity!!, "End date is smaller then previous date")
                                    return
                                }
                                if (dateSelected.before(Calendar.getInstance().time)) {
                                    Util.showToastMessage(HooleyMain.activity!!, "Invalid date, Please select future date.")
                                    return
                                }
                            }
                        } else {
                            if (dateSelected.before(Calendar.getInstance().time)) {
                                Util.showToastMessage(HooleyMain.activity!!, "Invalid date, Please select future date.")
                                return
                            }
                            previousSeletedDate = dateSelected
                        }
                        if (isStartDate) {
                            startDate = (String.format("%04d-%02d-%02d", year, monthNumber + 1, calendarSelected.get(Calendar.DAY_OF_MONTH))
                                    + " " + String.format("%02d:%02d", hour12, min) +
                                    " " + AM_PM)
                        } else {
                            endDate = (String.format("%04d-%02d-%02d", year, monthNumber + 1, calendarSelected.get(Calendar.DAY_OF_MONTH))
                                    + " " + String.format("%02d:%02d", hour12, min) +
                                    " " + AM_PM)
                        }
                        Log.e("val", startDate + "\n" + endDate)
                        view.text = ""
                        view.text = DateUtils.formatDate(String.format("%04d-%02d-%02d", year, monthNumber + 1, calendarSelected.get(Calendar.DAY_OF_MONTH))
                                + " " + String.format("%02d:%02d", hour12, min) +
                                " " + AM_PM, false)

                    }

                    override fun onCancel() {

                    }
                })
        customDialog!!.set24HourFormat(false)

        if (TextUtils.isEmpty(view.text.toString())) {
            val calendar: Calendar = Calendar.getInstance()
            customDialog!!.setMinDate(calendar.timeInMillis)
            customDialog!!.setDate(calendar)
        } else {
            customDialog!!.setDate(DateUtils.stringToDate(DateUtils.SendDateToDate(view.text.toString())))
        }
        customDialog!!.showDialog()
    }

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        list!![position].isActive = !list!![position].isActive
        objectResponse!!.body()!!.eventCategoryList[position].isAdded = list!![position].isActive
        adapterCategory!!.notifyDataSetChanged()
    }

    private fun expandAge(v: View) {
        v.measure(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        val targetHeight = v.measuredHeight
        v.layoutParams.height = 1
        v.visibility = View.VISIBLE
        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                v.layoutParams.height = if (interpolatedTime == 1f)
                    WindowManager.LayoutParams.WRAP_CONTENT
                else
                    (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
        binding.tvMinimumAge.visibility = View.GONE
        binding.viewLine.visibility = View.VISIBLE

    }

    private fun collapseAge(v: View) {
        val initialHeight = v.measuredHeight

        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
        setEventAgeLabel()
        binding.viewLine.visibility = View.GONE
    }


    private fun setEventAgeLabel() {
        var cattegoryName: String? = null
        for (i in listAge.indices) {
            if (listAge[i].isActive) {
                if (cattegoryName == null) {
                    cattegoryName = listAge[i].name
                }
            }
        }
        binding.tvMinimumAge.text = cattegoryName
        if (cattegoryName != null)
            binding.tvMinimumAge.visibility = View.VISIBLE
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivEventCover -> {
                isEdit = false
                if (!moreDialogeFragment!!.isAdded)
                    moreDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
            }
            R.id.ivCollapseEvent -> {
                collapse(binding.rlSelectEvent)
                binding.ivCollapseEvent.visibility = View.GONE
                binding.ivExpandEvent.visibility = View.VISIBLE
            }
            R.id.ivExpandEvent -> {
                expand(binding.rlSelectEvent)
                binding.ivCollapseEvent.visibility = View.VISIBLE
                binding.ivExpandEvent.visibility = View.GONE
            }
            R.id.ivCollapseAge -> {
                collapseAge(binding.rlSelectAge)
                binding.ivCollapseAge.visibility = View.GONE
                binding.ivExpandAge.visibility = View.VISIBLE
            }
            R.id.ivExpandAge -> {
                expandAge(binding.rlSelectAge)
                binding.ivCollapseAge.visibility = View.VISIBLE
                binding.ivExpandAge.visibility = View.GONE
            }
            R.id.btnAddCoHost -> {
                if (friendList != null && friendList!!.size == 0) {
                    Util.showToastMessage(HooleyMain.activity!!, getString(R.string.no_friends))
                    return
                }
                if (friendList == null) {
                    Util.showToastMessage(HooleyMain.activity!!, getString(R.string.no_friends))
                    return
                }
                if (objectResponse!!.body() != null)
                    if (objectResponse!!.body()!!.eventCoHostProfileList.size > 0) {
                        if (objectResponse!!.body()!!.eventCoHostProfileList.size == 3) {
                            Util.showToastMessage(HooleyMain.activity!!, getString(R.string.up_to_3))
                            return
                        }
                    }
                HooleyApp.db.putString(Constants.CALL_TYPE, "ADD_CO_HOST")
                if (friendList == null) {
                    Util.showToastMessage(HooleyMain.activity!!, getString(R.string.no_friends))
                    return
                }
                Util.hideKeyboard(HooleyMain.activity!!)
                selectFriendFragment = SelectFriendFragment.newInstance()
                selectFriendFragment!!.setTargetFragment(this@UpdateEventFragment, REQUEST_CODE_ADD_CO_HOST)
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, selectFriendFragment!!, "selectFriendFragment")
            }
            R.id.btnAddCategory -> {
                Util.hideKeyboard(HooleyMain.activity!!)
                if (TextUtils.isEmpty(binding.edtEventCategory.text.toString())) {
                    return
                }
                for (loop in objectResponse!!.body()!!.eventOptionalCategoryList.indices) {
                    if (binding.edtEventCategory.text.toString().equals(objectResponse!!.body()!!.eventOptionalCategoryList[loop].categoryName!!, ignoreCase = true)) {
                        Util.showToastMessage(HooleyMain.activity!!, "Category Already Added")
                        return
                    }
                }
                val optionalCategory = EditEventModel.EventOptionalCategory()
                optionalCategory.Id = "0"
                optionalCategory.categoryName = binding.edtEventCategory.text.toString()
                optionalCategory.isDeleted = false
                objectResponse!!.body()!!.eventOptionalCategoryList.add(optionalCategory)
                optionalCatList.add(binding.edtEventCategory.text.toString())
                adapterOptCategory!!.notifyDataSetChanged()
                binding.edtEventCategory.setText("")
            }
            R.id.ivEdit -> {
                isEdit = true
                if (!moreDialogeFragment!!.isAdded)
                    moreDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
            }

            R.id.ivUploadPic -> {
                isEdit = false
                if (!moreDialogeFragment!!.isAdded)
                    moreDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
            }
            R.id.ivDelete -> {
                removeView(binding.vpWallPaper.currentItem)
                Util.hideKeyboard(HooleyMain.activity!!)
            }


            R.id.btnSaveChanges -> {
                Util.hideKeyboard(HooleyMain.activity!!)
                if (HooleyApp.db.getBoolean(Constants.IS_PUBLISH_EVENT)) {
                    updateEditModel()
                    if (objectResponse!!.body()!!.eventBasicInfo!!.isPaid == isPaid) {
                        objectResponse!!.body()!!.eventBasicInfo!!.isPaid = isPaid
                        updateEvent(true)
                    } else {
                        (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, EventTicketSetupFragment.newInstance(eventWallpapers, objectResponse!!.body()!!, eventId!!, "true"), "EventTicketSetupFragment")
                    }
                }
            }
            R.id.llStartDate -> {
                isStartDate = true
                showCustomDialog(binding.tvEventStartDate)
            }
            R.id.llEndDate -> {
                isStartDate = false
                showCustomDialog(binding.tvEventEndDate)
            }
            R.id.rlLocation -> if (validateInput()) {
                Util.hideKeyboard(HooleyMain.activity!!)
                geoFenceFragment = GeofenceFragment.newInstance(objectResponse!!.body()!!.eventGeofenceList)
                geoFenceFragment!!.setTargetFragment(this@UpdateEventFragment, REQUEST_CODE_GEOFENCE)
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, geoFenceFragment!!, "GeofenceFragment")
            }
            R.id.btnEventPreview -> {
                val startDate1 = SimpleDateFormat("MMM dd, yyyy - hh:mm a").parse(binding.tvEventStartDate.text.toString())
                if (startDate1.before(Calendar.getInstance().time)) {
                    HooleyMain.activity!!.showSuccessDialog("Invalid date, Please select future date.")
                } else {
                    val endDate1 = SimpleDateFormat("MMM dd, yyyy - hh:mm a").parse(binding.tvEventEndDate.text.toString())
                    if (endDate1.before(startDate1)) {
                        HooleyMain.activity!!.showSuccessDialog("Date should be after start date")
                    } else {
                        HooleyMain.activity!!.hideKeyboard()
                        updateEditModel()
                        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, PreviewEventFragment.newInstance(eventWallpapers, objectResponse!!.body()!!, eventId!!, "true"), "PreviewEventFragment")
                    }
                }
            }
            R.id.btnSaveUnPublish -> {
                Util.hideKeyboard(HooleyMain.activity!!)
                if (HooleyApp.db.getBoolean(Constants.IS_PUBLISH_EVENT)) {
                    (HooleyMain.activity as ActivityBase).addFragment(R.id.container, CancelEventFragment.newInstance(eventId!!, false, objectResponse!!.body()!!.eventBasicInfo!!.isPaid, Constants.TYPE_UNPUBLISH_EVENT), "CancelEventFragment")
                } else {
                    updateEditModel()
                    objectResponse!!.body()!!.eventBasicInfo!!.isPaid = isPaid
                    updateEvent(false)
                }
            }
        }
    }

    fun expand(v: View) {
        v.measure(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        val targetHeight = v.measuredHeight

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.layoutParams.height = 1
        v.visibility = View.VISIBLE
        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                v.layoutParams.height = if (interpolatedTime == 1f)
                    WindowManager.LayoutParams.WRAP_CONTENT
                else
                    (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }
        a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
        binding.tvEventCategory.visibility = View.GONE
    }

    fun collapse(v: View) {
        val initialHeight = v.measuredHeight

        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // 1dp/ms
        a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
        setEventCategoryLabel()
    }

    private fun setEventCategoryLabel() {
        var categoryName: String? = null
        for (i in list!!.indices) {
            if (list!![i].isActive) {
                categoryName = if (categoryName == null) {
                    list!![i].name
                } else {
                    categoryName + ", " + list!![i].name
                }
            }
        }
        binding.tvEventCategory.text = categoryName
        if (categoryName != null)
            binding.tvEventCategory.visibility = View.VISIBLE
    }

    private fun validateInput(): Boolean {
        if (Util.isContentNull(binding.edEventHashes.text.toString())) {
            binding.edEventHashes.requestFocus()
            HooleyMain.activity!!.showSuccessDialog(getString(R.string.str_hash_missing))
            return false
        } else if (Util.isContentNull(binding.edEventName.text.toString())) {
            binding.edEventName.requestFocus()
            HooleyMain.activity!!.showSuccessDialog(getString(R.string.required))
            return false
        } else if (Util.isContentNull(binding.edEventDetails.text.toString())) {
            binding.edEventDetails.requestFocus()
            HooleyMain.activity!!.showSuccessDialog(getString(R.string.required))
            return false
        } else if (eventWallpapers.size == 0) {
            Util.showToastMessage(HooleyMain.activity!!, "Please upload Cover")
            return false
        } else if (!Util.checkIfName(binding.edEventName.text.toString())) {
            Util.showToastMessage(HooleyMain.activity!!, getString(R.string.invalid_event_name))
            return false
        } else if (Util.isContentNull(binding.edEventHashes.text.toString()) || binding.edEventHashes.text!!.length < 2) {
            binding.edEventHashes.requestFocus()
            HooleyMain.activity!!.showSuccessDialog(getString(R.string.str_hash_missing))
            return false
        } else if (!agesSelected()) {
            Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_select_age))
            if (binding.rlSelectAge.visibility != View.VISIBLE)
                binding.ivExpandAge.performClick()
            return false

        } else {
            if (list != null) {
                for (i in list!!.indices) {
                    if (list!![i].isActive) {
                        return true
                    }
                }

            }
            Util.showToastMessage(HooleyMain.activity!!, "Please select ")
            if (binding.rlSelectEvent.visibility != View.VISIBLE)
                binding.ivExpandEvent.performClick()
            return false
        }
    }

    private var ageId: String = ""
    private var selectedAge: String = ""

    private fun agesSelected(): Boolean {
        for (i in listAge.indices) {
            if (listAge[i].isActive) {
                ageId = listAge[i].id!!
                selectedAge = listAge[i].name!!
                return true
            }
        }
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                Constants.CAMERA_RQ -> HooleyMain.activity!!.processCapturedPhoto(object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        //                        mediaArraylist.add(UpdatePostMediaItem("", result, Constants.TYPE_URI, false))
                        Log.e("Image uti", "" + result)
                        binding.rlCover.visibility = View.GONE
                        if (eventWallpapers.size < 4) {
                            addWallpaper(ItemCreateEventWallpaper("", result, Constants.TYPE_URI))
                        }
                    }
                })
                Constants.IMAGE_PICKER_SELECT -> HooleyMain.activity!!.processGalleryPhoto(data!!, object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        //                        mediaArraylist.add(UpdatePostMediaItem("", result, Constants.TYPE_URI, false))
                        Log.e("Image uti", "" + result)
                        binding.rlCover.visibility = View.GONE
                        if (eventWallpapers.size < 4) {
                            addWallpaper(ItemCreateEventWallpaper("", result, Constants.TYPE_URI))
                        }
                    }
                })
                REQUEST_CODE -> {
                    val isUri = data!!.getBooleanExtra("isUri", false)
                    if (isUri) {
                    } else {
                        binding.rlCover.visibility = View.GONE
                        imagesArray = data.getStringArrayListExtra("Image")
                        if (imagesArray != null && imagesArray!!.size > 0) {
                            for (i in imagesArray!!.indices) {
                                addWallpaper(ItemCreateEventWallpaper(imagesArray!![i], null, Constants.TYPE_URL))
                            }
                        }
                    }
                }
                REQUEST_CODE_GEOFENCE -> {
                    val isCircle = data!!.getBooleanExtra("isCircle", false)
                    if (isCircle) {
                        if (objectResponse!!.body()!!.eventGeofenceList.size > 0)
                            objectResponse!!.body()!!.eventGeofenceList.clear()
                        val model = PersonalProfileModel.GeoFence()
                        model.latitude = java.lang.Double.toString(data.getDoubleExtra("Lat", 0.0))
                        model.longitude = java.lang.Double.toString(data.getDoubleExtra("Long", 0.0))
                        model.radius = data.getFloatExtra("radius", 0f).toString()
                        model.address = (HooleyMain.activity as ActivityBase).getCompleteAddressString(model.latitude!!.toDouble(), model.longitude!!.toDouble())
                        objectResponse!!.body()!!.eventGeofenceList.add(model)
                        binding.tvLocationName.text = model.address
                    } else {
                        listLatLong = data.getParcelableArrayListExtra("mLatlngs")
                        centerLatLngObj = (HooleyMain.activity as ActivityBase).computeCentroid(listLatLong!!)
                        if (objectResponse!!.body()!!.eventGeofenceList.size > 0)
                            objectResponse!!.body()!!.eventGeofenceList.clear()
                        for (loop in listLatLong!!.indices) {
                            val model = PersonalProfileModel.GeoFence()
                            model.latitude = java.lang.Double.toString(listLatLong!![loop].latitude)
                            model.longitude = java.lang.Double.toString(listLatLong!![loop].longitude)
                            model.radius = "0"
                            model.address = (HooleyMain.activity as ActivityBase).getCompleteAddressString(centerLatLngObj!!.latitude, centerLatLngObj!!.longitude)
                            objectResponse!!.body()!!.eventGeofenceList.add(model)
                            binding.tvLocationName.text = model.address
                        }
                    }
                    updateEditModel()
                    objectResponse!!.body()!!.eventBasicInfo!!.isPaid = isPaid

                    binding.btnEventPreview.isEnabled = true
                    binding.btnEventPreview.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_signup)
                }
                REQUEST_CODE_ADD_CO_HOST -> {
                    coHostList = data!!.getParcelableArrayListExtra("friendListIds")
                    if (objectResponse!!.body()!!.eventCoHostProfileList.size > 0)
                        objectResponse!!.body()!!.eventCoHostProfileList.clear()
                    addingCoHost(coHostList)

                }
            }
        } else {
            Util.showToastMessage(HooleyMain.activity!!, "Action Cancelled")
        }

    }

    private fun addingCoHost(coHosts: ArrayList<FriendsTable>) {
        var coHostAdapter = AdapterAddCoHost(HooleyMain.activity!!, coHosts)
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding.rvCoHost.layoutAnimation = animation
        binding.rvCoHost.layoutManager = manager
        coHostAdapter.mListener = this
        binding.rvCoHost.adapter = coHostAdapter
    }

    private fun updateEditModel() {
        objectResponse!!.body()!!.eventBasicInfo!!.eventName = binding.edEventName.text.toString()
        objectResponse!!.body()!!.eventBasicInfo!!.eventDetails = binding.edEventDetails.text.toString()
        objectResponse!!.body()!!.eventBasicInfo!!.minimumAgeId = getAgeId()
        objectResponse!!.body()!!.eventBasicInfo!!.isPrivate = isPublic
        objectResponse!!.body()!!.eventBasicInfo!!.eventStartTime = startDate
        objectResponse!!.body()!!.eventBasicInfo!!.eventEndTime = endDate
        objectResponse!!.body()!!.eventBasicInfo!!.isPublished = HooleyApp.db.getBoolean(Constants.IS_PUBLISH_EVENT)
        objectResponse!!.body()!!.eventBasicInfo!!.eventHashs.clear()
        objectResponse!!.body()!!.eventBasicInfo!!.eventHashs = getHashes()

    }

    private fun getAgeId(): String {
        for (i in listAge) {
            if (i.isActive) {
                return i.id!!
            }
        }
        return ""
    }


    private fun setupViewPager() {
        adapter = AdapterWallpaperPager()
        binding.vpWallPaper.adapter = adapter
        binding.tabEventWallpaper.setupWithViewPager(binding.vpWallPaper, true)
    }

    fun addView(item: ItemCreateEventWallpaper) {
        val pageIndex = adapter!!.addView(item)
        adapter!!.notifyDataSetChanged()
        binding.vpWallPaper.setCurrentItem(pageIndex, true)
        if (eventWallpapers.size == 4) {
            binding.ivUploadPic.visibility = View.GONE
        }
    }


    fun editView(position: Int, item: ItemCreateEventWallpaper) {
        val pageIndex = adapter!!.editView(position, item)
        // You might want to make "newPage" the currently displayed page:
        adapter!!.notifyDataSetChanged()
        binding.vpWallPaper.setCurrentItem(pageIndex, true)
    }

    fun removeView(position: Int) {
        var pageIndex = adapter!!.removeView(binding.vpWallPaper, position)
        eventWallpapers.removeAt(pageIndex)
        adapter!!.notifyDataSetChanged()
        if (pageIndex == adapter!!.count)
            pageIndex--
        binding.vpWallPaper.currentItem = pageIndex
        if (eventWallpapers.size in 1..3) {
            binding.ivUploadPic.visibility = View.VISIBLE
            binding.ivDelete.visibility = View.VISIBLE
            binding.ivEdit.visibility = View.VISIBLE
        } else if (eventWallpapers.size == 0) {
            binding.ivEdit.visibility = View.GONE
            binding.ivDelete.visibility = View.GONE
            binding.ivUploadPic.visibility = View.GONE
            binding.ivEventCover.visibility = View.VISIBLE
            binding.rlCover.visibility = View.VISIBLE
        }
    }

    private fun addWallpaper(imageObj: ItemCreateEventWallpaper) {
        if (isEdit) {
            editView(binding.vpWallPaper.currentItem, imageObj)
            eventWallpapers[binding.vpWallPaper.currentItem] = imageObj
        } else {
            if (eventWallpapers.size < 4) {
                addView(imageObj)
                eventWallpapers.add(imageObj)
            } else {
                binding.ivUploadPic.visibility = View.GONE
            }
        }
        if (eventWallpapers.size in 1..3) {
            binding.ivEdit.visibility = View.VISIBLE
            binding.ivDelete.visibility = View.VISIBLE
            binding.ivUploadPic.visibility = View.VISIBLE
            binding.ivEventCover.visibility = View.GONE
        }

    }


    private fun getFriends(showDialog: Boolean) {
        if (showDialog) {
            HooleyMain.activity!!.showDialog()
        }
        service = FriendWebService()
        service!!.getMyFriendsList(object : GenericCallback<FriendsModel> {
            override fun success(result: FriendsModel) {
                HooleyMain.activity!!.removeDialog()
                if (showDialog) {
                    if (friendList == null)
                        friendList = ArrayList()
                    friendList!!.addAll(result.friendsList)
                } else {
                    if (friendList!!.size != result.friendsList.size) {
                        friendList!!.clear()
                        friendList!!.addAll(result.friendsList)
                        DbManager.getInstance().friendList = friendList
                    }
                }
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    //Util.showToastMessage(HooleyMain.Companion.getActivity(), message);
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.unbind()
    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        when (group.id) {
            R.id.rgEventVisibility -> when (checkedId) {
                R.id.rbPublic -> {
                    isPublic = true
                    objectResponse!!.body()!!.eventBasicInfo!!.isPrivate = false
                }
                R.id.rbPrivate -> {
                    isPublic = false
                    objectResponse!!.body()!!.eventBasicInfo!!.isPrivate = true
                }
            }
//            R.id.rgInviteFriend -> when (checkedId) {
//                R.id.rbYes -> {
//                    inviteFreinds = true
//                    objectResponse!!.body()!!.eventBasicInfo!!.inviteFriend = true
//                }
//                R.id.rbNo -> {
//                    inviteFreinds = true
//                    objectResponse!!.body()!!.eventBasicInfo!!.inviteFriend = false
//                }
//            }
        }
    }


    private fun setData() {
        if (HooleyApp.db.getBoolean(Constants.IS_PUBLISH_EVENT)) {
        } else {
            binding.btnSaveChanges.visibility = View.GONE
            //            if (objectResponse!!.body()!!.eventGeofenceList.size > 0) {
//                binding.btnPublish.visibility = View.VISIBLE
//            } else {
//                binding.btnPublish.visibility = View.GONE
//            }
        }
        if (objectResponse!!.body()!!.eventBasicInfo!!.isPublished) {
            binding.rlLocation.visibility = View.GONE
            binding.btnSaveUnPublish.visibility = View.GONE
            binding.btnEventPreview.visibility = View.GONE
        } else {
            binding.rlLocation.visibility = View.VISIBLE
            binding.rlEventTimings.visibility = View.VISIBLE
            binding.btnSaveUnPublish.visibility = View.VISIBLE
            binding.btnEventPreview.visibility = View.VISIBLE
            binding.btnSaveChanges.visibility = View.GONE
            if (objectResponse!!.body()!!.eventGeofenceList.size > 0)
                binding.tvLocationName.text = objectResponse!!.body()!!.eventGeofenceList[0].address
            else {
                binding.btnEventPreview.isEnabled = false
                binding.btnEventPreview.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_disable_btn)
            }
        }
        if (objectResponse!!.body()!!.eventBasicInfo!!.eventType != null)
            isPaid = when (objectResponse!!.body()!!.eventBasicInfo!!.eventType) {
                "1" -> false
                "2" -> true
                else -> true
            } else
            isPaid = false

        if (objectResponse!!.body()!!.eventBasicInfo!!.eventType == "3") {
            binding.evisibility.visibility = GONE
            binding.rlAgeContainer.visibility = GONE
            binding.viewAge.visibility = GONE
        }

        binding.edEventName.setText(objectResponse!!.body()!!.eventBasicInfo!!.eventName)
        binding.edEventDetails.setText(objectResponse!!.body()!!.eventBasicInfo!!.eventDetails)
        binding.tvEventStartDate.text = DateUtils.formatDate(DateUtils.getLocalDate(objectResponse!!.body()!!.eventBasicInfo!!.eventStartTime!!), true)
        binding.tvEventEndDate.text = DateUtils.formatDate(DateUtils.getLocalDate(objectResponse!!.body()!!.eventBasicInfo!!.eventEndTime!!), true)

        startDate = DateUtils.convertDateForEvent(DateUtils.getLocalDate(objectResponse!!.body()!!.eventBasicInfo!!.eventStartTime!!))
        endDate = DateUtils.convertDateForEvent(DateUtils.getLocalDate(objectResponse!!.body()!!.eventBasicInfo!!.eventEndTime!!))

        Log.e("val", startDate + "\n" + endDate)

        if (objectResponse!!.body()!!.eventBasicInfo!!.isPrivate) {
            binding.rbPrivate.isChecked = true
            binding.rbPublic.isChecked = false
            isPublic = false
        } else {
            binding.rbPrivate.isChecked = false
            binding.rbPublic.isChecked = true
            isPublic = true
        }
        setHashTags()
        setCostHost()
        setEventWallpapers()
        setCategories()
        setOptionalCategory()
    }

    private fun setHashTags() {
        if (objectResponse!!.body()!!.eventBasicInfo!!.eventHashs.size > 0) {
//            for (i in objectResponse!!.body()!!.eventBasicInfo!!.eventHashs.indices) {
//                eventHashes.add("#" + objectResponse!!.body()!!.eventBasicInfo!!.eventHashs[0])
//            }
//        }
//        val builder = StringBuilder()
//        for (hashTag in eventHashes) {
//            builder.append(hashTag)
//            builder.append(" ")
            binding.edEventHashes.setText(objectResponse!!.body()!!.eventBasicInfo!!.eventHashs.joinToString().replace(",", ""))
        }
    }


    private fun setEventWallpapers() {
        if (objectResponse!!.body()!!.eventCoverList.size > 0) {
            binding.rlCover.visibility = View.GONE
            for (i in objectResponse!!.body()!!.eventCoverList.indices) {
                addWallpaper(ItemCreateEventWallpaper(objectResponse!!.body()!!.eventCoverList[i].photo_url!!, null, Constants.TYPE_URL))
            }
            binding.vpWallPaper.currentItem = 0
        }
    }

    private fun setCostHost() {
        if (objectResponse!!.body()!!.eventCoHostProfileList.size > 0) {
            for (i in objectResponse!!.body()!!.eventCoHostProfileList.indices) {
                coHostList.add(objectResponse!!.body()!!.eventCoHostProfileList[i])
            }
            if (coHostList.size == 3) {
                binding.btnAddCoHost.isEnabled = false
                binding.btnAddCoHost.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_disable_btn)
            }
            addingCoHost(coHostList)
        }
    }

    private fun setCategories() {
        if (objectResponse!!.body()!!.eventCategoryList.size > 0) {
            for (i in objectResponse!!.body()!!.eventCategoryList.indices) {
                val category = Category()
                category.id = Integer.parseInt(objectResponse!!.body()!!.eventCategoryList[i].Id!!)
                category.name = objectResponse!!.body()!!.eventCategoryList[i].Name!!
                category.iconUrl = objectResponse!!.body()!!.eventCategoryList[i].icon!!
                category.isActive = objectResponse!!.body()!!.eventCategoryList[i].isAdded
                if (list == null)
                    list = ArrayList<Category>()
                list!!.add(category)
            }
            list!!.sortBy { it.name }
            setGridEvent()
        }
    }


    private fun setOptionalCategory() {
        if (objectResponse!!.body()!!.eventOptionalCategoryList.size > 0) {
            for (i in objectResponse!!.body()!!.eventOptionalCategoryList.indices) {
                optionalCatList.add(objectResponse!!.body()!!.eventOptionalCategoryList[i].categoryName!!)
            }
            adapterOptCategory!!.notifyDataSetChanged()
        }
    }

    private fun getEditEvent() {
        HooleyMain.activity!!.showDialog()
        val call = HooleyApp.apiService.getEditEvent(HooleyApp.db.getString(Constants.USER_ID), eventId)
        call.enqueue(object : Callback<EditEventModel> {
            override fun onResponse(call: Call<EditEventModel>, response: Response<EditEventModel>) {
                HooleyMain.activity!!.removeDialog()
                if (response.body()!!.success) {
                    objectResponse = response
                    Logger.ex("getEditEvent:" + Gson().toJson(response.body()!!.toString()))
                    setData()
                    getAgeList()
                    setAgeGridEvent()
                    return
                } else {
                    Util.showToastMessage(HooleyMain.activity!!, response.body()!!.exception)
                }
            }

            override fun onFailure(call: Call<EditEventModel>, t: Throwable) {
                Log.d("ERROR", "ERROR:" + t.message)
                HooleyMain.activity!!.removeDialog()
                Util.showToastMessage(HooleyMain.activity!!, "Server not responding")
            }
        })
    }

    private fun updateEvent(isPublish: Boolean) {
        HooleyMain.activity!!.showDialog()
        eventService.updateEvent(eventWallpapers, objectResponse!!.body()!!, eventId!!, isPublish, "true", object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                Util.showToastMessage(HooleyMain.activity!!, "Event updated successfully")
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()

            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.removeDialog()
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
    }

    companion object {

        private var instance: UpdateEventFragment? = null
        private var eventId: String? = null

        fun newInstance(id: String): UpdateEventFragment {
            eventId = id
            instance = UpdateEventFragment()
            return instance!!
        }
    }
}
