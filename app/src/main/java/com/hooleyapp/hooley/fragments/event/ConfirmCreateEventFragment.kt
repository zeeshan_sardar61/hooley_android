package com.hooleyapp.hooley.fragments.event

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentConfirmCreateEventBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.invites.InviteFragment
import com.hooleyapp.hooley.helper.setToolBarTitle

/**
 * Created by Nauman on 12/28/2017.
 */

class ConfirmCreateEventFragment : BaseFragment(), View.OnClickListener {
    var binding: FragmentConfirmCreateEventBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_confirm_create_event, container, false)
        // HooleyMain.activity.getSupportActionBar().hide();
        setToolBarTitle(HooleyMain.activity!!, "Hooley")
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setValues()
        setListerner()
    }

    private fun setValues() {
        binding!!.btnSetUpTicket.visibility = View.GONE
        binding!!.tvEventName.text = eventName
    }

    private fun setListerner() {
        binding!!.btnInvite.setOnClickListener(this)
        binding!!.btnSetUpTicket.setOnClickListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
        //        HooleyMain.activity.getSupportActionBar().show();
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnInvite -> (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, InviteFragment.newInstance(eventId!!, eventName!!), null)
//            R.id.btnSetUpTicket -> (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, EventTicketSetupFragment.newInstance(eventObj!!.id!!, eventObj!!.name!!, eventStartTime!!, eventEndTime!!), null)
        }
    }

    companion object {
        lateinit var instance: ConfirmCreateEventFragment
        private var eventId: String? = null
        private var eventName: String? = null

        fun newInstance(eventId_: String, eventName_: String): ConfirmCreateEventFragment {
            eventId = eventId_
            eventName = eventName_
            instance = ConfirmCreateEventFragment()
            return instance
        }
    }
}
