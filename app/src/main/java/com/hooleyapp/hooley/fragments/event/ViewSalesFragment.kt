package com.hooleyapp.hooley.fragments.event

import android.annotation.SuppressLint
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.widget.EditText
import android.widget.TextView
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterShareIncomeTickets
import com.hooleyapp.hooley.databinding.FragmentViewSalesBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.ticket.EditTicketFragment
import com.hooleyapp.hooley.helper.addCommaInCurrency
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.ticketStats.IncomeShareUsersItem
import com.hooleyapp.hooley.model.ticketStats.TicketStatsResponse
import com.hooleyapp.hooley.services.TicketWebService

/**
 * Created by Zeeshan on 07-Mar-18.
 */

class ViewSalesFragment : BaseFragment(), View.OnClickListener {
    lateinit var binding: FragmentViewSalesBinding
    lateinit var objectResponse: TicketStatsResponse
    private var totalQuantity = 0
    private var totalSold = 0
    private var totalAvailable = 0
    private var totalValue = 0
    var ticketService = TicketWebService()
    var adapter: AdapterShareIncomeTickets? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_view_sales, container, false)
        setListener()
        getTicketDetails()
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
    }

    private fun setListener() {
        binding.btnEditTicket.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnEditTicket -> (HooleyMain.activity!! as ActivityBase).callFragmentWithReplace(R.id.container, EditTicketFragment(eventId!!), "EditTicketFragment")
        }
    }

    private fun getTicketDetails() {
        HooleyMain.activity!!.showDialog()
        ticketService.viewTicketSales(eventId!!, object : GenericCallback<TicketStatsResponse> {
            override fun success(result: TicketStatsResponse) {
                HooleyMain.activity!!.removeDialog()
                objectResponse = result
                totalQuantity = 0
                totalSold = 0
                totalAvailable = 0
                totalValue = 0
                setData()
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.removeDialog()
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun setData() {
        if (objectResponse.latestSaleStats != null && objectResponse.latestSaleStats!!.isNotEmpty()) {
            binding.llGeneralTickets.visibility = View.VISIBLE
            binding.llSaleCount.visibility = View.VISIBLE
            addChild()
            binding.rlTotalCount.visibility = View.VISIBLE
            binding.btnEditTicket.visibility = View.VISIBLE
            if (objectResponse.incomeShareUsers != null) {
                setAdapter()
                binding.rlShareIncome.visibility = View.VISIBLE
            }
            setTotalAmounts()
            (HooleyMain.activity!! as HooleyMain).setToolbarTitle("Ticket Sales")
        }
        if (objectResponse.ppvSalesList != null) {
            binding.llPPV.visibility = View.VISIBLE
            binding.llPPVSaleCount.visibility = View.VISIBLE
            if (objectResponse.ppvSalesList!!.isNotEmpty())
                addPPVChild()
            binding.btnEditTicket.visibility = View.VISIBLE
            (HooleyMain.activity!! as HooleyMain).setToolbarTitle("Ticket & PPV Sales ")
        }
        if (objectResponse.charity != null) {
            if (objectResponse.latestSaleStats != null && objectResponse.latestSaleStats!!.isNotEmpty()) {
                binding.llCharityView.visibility = View.VISIBLE
                binding.tvCharityTarget.text = "$" + objectResponse.charity!!.charityTarget!!.toString()
                binding.tvRemoteDonations.text = "$" + objectResponse.charity!!.remoteDonations!!.toString()
                binding.tvDonateAmount.text = "$" + objectResponse.charity!!.remoteDonations!!.toString()
                binding.btnEditTicket.visibility = View.VISIBLE
                (HooleyMain.activity!! as HooleyMain).setToolbarTitle("Donation Stats")
            } else {
                binding.btnEditTicket.visibility = View.GONE
                binding.llSingleCharity.visibility = View.VISIBLE
                binding.tvSingleCharityTarget.text = "$" + objectResponse.charity!!.charityTarget!!.toString()
                binding.tvCollectedAmount.text = "$" + objectResponse.charity!!.remoteDonations!!.toString()
                (HooleyMain.activity!! as HooleyMain).setToolbarTitle("Charity Donation")
            }
        }

    }

    private fun setAdapter() {
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding.rvShareIncome.layoutManager = manager
        adapter = AdapterShareIncomeTickets(objectResponse.incomeShareUsers as List<IncomeShareUsersItem>)
        binding.rvShareIncome.adapter = adapter
    }

    private fun addPPVChild() {
        for (i in objectResponse.ppvSalesList!!.indices) {
            val child = HooleyMain.activity!!.layoutInflater.inflate(R.layout.ppv_sale_count_item, null)
            val value = objectResponse.ppvSalesList!![i]?.revenue
            (child.findViewById<View>(R.id.tvPPVTicketSoldCount) as TextView).text = Integer.toString(objectResponse.ppvSalesList!![i]?.sold!!)
            (child.findViewById<View>(R.id.tvPPVTicketRevenue) as TextView).text = "$$value"
            (child.findViewById<View>(R.id.tvPPVPrice) as TextView).text = "$" + Integer.toString(objectResponse.ppvSalesList!![i]?.price!!)
            totalSold += objectResponse.ppvSalesList!![i]?.sold!!
            totalValue += value!!
            binding.llPPVSaleCount.addView(child)
        }
        binding.tvPPVTotalRevenue.text = "$" + HooleyMain.activity!!.addCommaInCurrency(totalPPV())
    }

    fun totalPPV(): Int {
        var totalPPV = 0
        for (i in objectResponse.ppvSalesList!!.indices) {
            totalPPV += objectResponse.ppvSalesList!![i]?.revenue!!
        }
        return totalPPV
    }

    fun totalSalesRevenue(): Int {
        var totalRev = 0
        for (i in objectResponse.latestSaleStats!!.indices) {
            totalRev += objectResponse.latestSaleStats!![i]?.revenue!!
        }
        return totalRev
    }

    private fun addChild() {
        for (i in objectResponse.latestSaleStats!!.indices) {
            val child = HooleyMain.activity!!.layoutInflater.inflate(R.layout.sale_count_child_item, null)
            val value = objectResponse.latestSaleStats!![i]?.revenue
            (child.findViewById<View>(R.id.tvTicketCategoryName) as TextView).text = objectResponse.latestSaleStats!![i]?.ticketCategory
            (child.findViewById<View>(R.id.tvTicketSoldCount) as TextView).text = Integer.toString(objectResponse.latestSaleStats!![i]?.sold!!)
            (child.findViewById<View>(R.id.tvTicketValueCount) as TextView).text = "$$value"
            (child.findViewById<View>(R.id.tvTicketAvailableCount) as TextView).text = Integer.toString(objectResponse.latestSaleStats!![i]?.available!!)
            (child.findViewById<View>(R.id.edtTicketQuantityCount) as EditText).setText(Integer.toString(objectResponse.latestSaleStats!![i]?.total!!))
            child.findViewById<View>(R.id.edtTicketQuantityCount).isEnabled = false
            child.findViewById<View>(R.id.edtTicketPriceCount).isEnabled = false
            totalQuantity += objectResponse.latestSaleStats!![i]?.total!!
            totalSold += objectResponse.latestSaleStats!![i]?.sold!!
            totalAvailable += objectResponse.latestSaleStats!![i]?.available!!
            totalValue += value!!
            binding.llSaleCount.addView(child)
        }
        binding.tvTicketsTotalRevenue.text = "$" + HooleyMain.activity!!.addCommaInCurrency(totalSalesRevenue())
    }

    @SuppressLint("SetTextI18n")
    private fun setTotalAmounts() {
        binding.tvTotalQuantityCount.text = Integer.toString(totalQuantity)
        binding.tvTotalSoldTicketsCount.text = Integer.toString(totalSold)
        binding.tvTotalAvailableTicketsCount.text = Integer.toString(totalAvailable)
        binding.tvTotalValueTicketCount.text = Integer.toString(totalValue)
    }


    companion object {
        @SuppressLint("StaticFieldLeak")
        private var instance: ViewSalesFragment? = null
        private var eventId: String? = null

        fun newInstance(id: String): ViewSalesFragment {
            eventId = id
            instance = ViewSalesFragment()
            return instance!!
        }
    }
}