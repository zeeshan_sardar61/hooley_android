package com.hooleyapp.hooley.fragments.profile

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.webkit.CookieManager
import android.webkit.CookieSyncManager
import android.widget.*
import com.alimuzaffar.lib.pin.PinEntryEditText
import com.bumptech.glide.Glide
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookSdk
import com.facebook.login.LoginManager
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterAddInterest
import com.hooleyapp.hooley.adapters.AdapterCountries
import com.hooleyapp.hooley.adapters.AdapterSpinner
import com.hooleyapp.hooley.databinding.FragmentEditPersonalProfileBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.auth.FlickrAuthFragment
import com.hooleyapp.hooley.fragments.auth.InstagramAuthenticationFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.ICallBackUri
import com.hooleyapp.hooley.interfaces.IInterestDeleteClick
import com.hooleyapp.hooley.interfaces.ISocialCallBackListener
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.*
import com.hooleyapp.hooley.model.staticData.StaticDataModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.requestmodel.VerifyPhoneNumberModel
import com.hooleyapp.hooley.services.ProfileWebService
import com.hooleyapp.hooley.social.FlickrClient
import com.hooleyapp.hooley.social.SocialClass
import com.linkedin.platform.LISessionManager
import com.rilixtech.Country
import com.rilixtech.CountryCodePicker
import com.twitter.sdk.android.core.TwitterAuthConfig
import com.twitter.sdk.android.core.TwitterCore
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Nauman on 12/12/2017.
 */

class EditPersonalProfileFragment : BaseFragment(), AdapterView.OnItemSelectedListener, View.OnClickListener, IInterestDeleteClick, CountryCodePicker.OnCountryChangeListener, ISocialCallBackListener {
    private var isFlickerConnected: Boolean = false
    private var isFBConnected: Boolean = false
    private var isTwitterConnected: Boolean = false
    private var isInstaConnected: Boolean = false
    private var isLinkedInConnected: Boolean = false

    var relationShipPosition = 0
    var collegeMajorPosition = 0
    var politicalViewPosition = 0
    var religionPosition = 0
    var languagePosition = 0
    var ethnicPosition = 0
    var astrologicalSignId = 0
    var userOccupationPosition = 0
    internal var binding: FragmentEditPersonalProfileBinding? = null
    internal var socialClass = SocialClass()

    internal var isValid = true
    private var spinnerAdapter: AdapterSpinner? = null
    private var personalImageUrl = ""
    private val REQUEST_CODE_FLICKR = 965
    private var adapterAddInterest: AdapterAddInterest? = null
    private var callbackManager: CallbackManager? = null
    private var twitterAuthClient: TwitterAuthClient? = null
    private var verifyNumberDialog: Dialog? = null
    private var messageId: String? = ""
    private var cc = ""
    private var ccIso = ""
    private var flickrAuthFragment: FlickrAuthFragment? = null
    private var countDownTimer: CountDownTimer? = null
    private var isConnected = false
    private var socialId: String? = null
    private var socialUrl: String? = null
    private var instagramAuthenticationFragment: InstagramAuthenticationFragment? = null
    private var intsaAccessToken: String? = null
    private var adapterSpinner: AdapterCountries? = null
    private var countryList: ArrayList<PersonalProfileModel.myObject>? = null
    private var stateList: ArrayList<PersonalProfileModel.myObject>? = null
    private var citiesList: ArrayList<PersonalProfileModel.myObject>? = null
    private var strCountry: String? = null
    private var strCountryId: String? = null
    private var strState: String? = null
    private var strStateId: String? = null
    private var strCity: String? = null
    private var strCityId: String? = null
    private val service = ProfileWebService()
    private var responseCountry = CountryStateCitiesModel()
    private var mediaUri: Uri? = null
    private var isCitySet = false
    var model = StaticDataModel()
    var gson = Gson()
    private var moreDialogeFragment: MoreDialogeFragment? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_personal_profile, container, false)
        (HooleyMain.activity as HooleyMain).removeBubbleView()
        socialClass.setMListener(this)
        callbackManager = socialClass.initFacebookSdk()
        twitterAuthClient = socialClass.initTwitter()
        setHasOptionsMenu(true)
        setToolbar()
        setListener()
        model = gson.fromJson(HooleyApp.db.getString(Constants.STATIC_MODEL), StaticDataModel::class.java)
        setData()
        return binding!!.root
    }

    private fun setToolbar() {
        HooleyMain.activity!!.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        HooleyMain.activity!!.supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
    }

    private fun setListener() {
        binding!!.tvDob.setOnClickListener(this)
        binding!!.spRelationShip.onItemSelectedListener = this
        binding!!.spAstrologicalSign.onItemSelectedListener = this
        binding!!.spReligion.onItemSelectedListener = this
        binding!!.spPoliticalView.onItemSelectedListener = this
        binding!!.spLanguages.onItemSelectedListener = this
        binding!!.spEthnicBg.onItemSelectedListener = this
        binding!!.spCollegeMajor.onItemSelectedListener = this
        binding!!.ivAddPhoto.setOnClickListener(this)
        binding!!.btnSaveProfile.setOnClickListener(this)
        binding!!.btnAddInterest.setOnClickListener(this)
        binding!!.btnNotVerified.setOnClickListener(this)
        binding!!.spOccupation.onItemSelectedListener = this
        binding!!.ccp.setOnCountryChangeListener(this)
        binding!!.btnFacebookConnect.setOnClickListener(this)
        binding!!.btnTwitterConnect.setOnClickListener(this)
        binding!!.btnLinkedInConnect.setOnClickListener(this)
        binding!!.btnInstagramConnect.setOnClickListener(this)
        binding!!.btnFlickerConnect.setOnClickListener(this)

        binding!!.ccp.setPhoneNumberInputValidityListener { _, b ->
            if (!b) {
                binding!!.edtPhone.error = "Please enter valid number"
            }
        }
        binding!!.edtPhone.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (s.toString().equals(responseObject.generalProfile!!.userInfo.phoneNo, ignoreCase = true)) {
                    binding!!.btnVerified.visibility = View.VISIBLE
                    binding!!.btnNotVerified.visibility = View.GONE
                } else {
                    binding!!.btnVerified.visibility = View.GONE
                    binding!!.btnNotVerified.visibility = View.VISIBLE
                }
            }
        })

    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.firstName)) {
            binding!!.edtFirstName.setText(responseObject.generalProfile!!.userInfo.firstName!!.trim())
            if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.lastName)) {
                binding!!.edtFirstName.setText(responseObject.generalProfile!!.userInfo.firstName!!.trim() + " " + responseObject.generalProfile!!.userInfo.lastName!!.trim())
            }
        }
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.aboutMe))
            binding!!.edtAboutMe.setText(responseObject.generalProfile!!.userInfo.aboutMe)
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.phoneNo))
            binding!!.edtPhone.setText(responseObject.generalProfile!!.userInfo.phoneNo)
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.website))
            binding!!.edtWebsite.setText(responseObject.generalProfile!!.userInfo.website)
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.email))
            binding!!.tvEmail.text = responseObject.generalProfile!!.userInfo.email
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.hobbies))
            binding!!.edtHobbies.setText(responseObject.generalProfile!!.userInfo.hobbies)
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.studentOf))
            binding!!.edtCollege.setText(responseObject.generalProfile!!.userInfo.studentOf)
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.highSchool))
            binding!!.edtHighSchool.setText(responseObject.generalProfile!!.userInfo.highSchool)
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.homeTown))
            binding!!.edtHomeTown.setText(responseObject.generalProfile!!.userInfo.homeTown)
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.employer))
            binding!!.edtEmployer.setText(responseObject.generalProfile!!.userInfo.employer)
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.organization))
            binding!!.edtOrganizations.setText(responseObject.generalProfile!!.userInfo.organization)
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.birthday) && !responseObject.generalProfile!!.userInfo.birthday!!.startsWith("000")) {
            val birthday: Array<String>
            if (responseObject.generalProfile!!.userInfo.birthday!!.contains("T")) {
                birthday = responseObject.generalProfile!!.userInfo.birthday!!.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                binding!!.tvDob.text = DateUtils.getBirthdayDate(birthday[0])
            } else
                binding!!.tvDob.text = DateUtils.getBirthdayDate(responseObject.generalProfile!!.userInfo.birthday!!)
        }
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.profilePicture)) {
            Glide.with(HooleyMain.activity!!).load(responseObject.generalProfile!!.userInfo.profilePicture).into(binding!!.ivAvatar)
            personalImageUrl = responseObject.generalProfile!!.userInfo.profilePicture!!
        }

        if (responseObject.generalProfile!!.userInfo.isPhoneVerified) {
            binding!!.ivVerified.visibility = View.VISIBLE
            binding!!.btnNotVerified.visibility = View.GONE
        } else {
            binding!!.ivVerified.visibility = View.GONE
            binding!!.btnNotVerified.visibility = View.VISIBLE
        }

        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.countryCode)) {
            binding!!.ccp.setCountryForNameCode(responseObject.generalProfile!!.userInfo.countryISO)
        }
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.country))
            binding!!.tvCountry.text = responseObject.generalProfile!!.userInfo.country
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.currentCity))
            binding!!.tvCity.text = responseObject.generalProfile!!.userInfo.currentCity
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.state))
            binding!!.tvState.text = responseObject.generalProfile!!.userInfo.state

        setGender()
        setRelationShipSpinner()
        setPolicitcalViewSpinner()
        setReligionSpinner()
        setLanguagesSpinner()
        setCollegeMajorSpinner()
        setAstrologicalSignsSpinner()
        setEthnicSpinner()
        //        setGeofence();
        setAddInterstRv()
        setUserOccupationSpinner()
        setUserSocial()
        getCountries("country", "0", "0", Constants.TYPE_COUNTRIES)


    }

    private fun setUserSocial() {
        if (responseObject.generalProfile!!.socialMediasList[0].isConnected) {
            binding!!.ivFacebook.setBackgroundResource(R.drawable.ic_socail_fb)
            isFBConnected = true
            binding!!.btnFacebookConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_opt_cat)
            binding!!.btnFacebookConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.purple_color))
            binding!!.btnFacebookConnect.text = "Disconnect"
        } else {
            HooleyApp.db.remove(Constants.SOCIAL_USER_ID)
            isFBConnected = false
            binding!!.ivFacebook.setBackgroundResource(R.drawable.ic_social_fb_dissconnect)
            binding!!.btnFacebookConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_add)
            binding!!.btnFacebookConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white))
            binding!!.btnFacebookConnect.text = "Connect"
        }

        if (responseObject.generalProfile!!.socialMediasList[1].isConnected) {
            binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_socail_twitter)
            isTwitterConnected = true
            binding!!.btnTwitterConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_opt_cat)
            binding!!.btnTwitterConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.purple_color))
            binding!!.btnTwitterConnect.text = "Disconnect"
        } else {
            HooleyApp.db.remove(Constants.TWITTER_USER_ID)
            isTwitterConnected = false
            binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_social_twitter_dissconnect)
            binding!!.btnTwitterConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_add)
            binding!!.btnTwitterConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white))
            binding!!.btnTwitterConnect.text = "Connect"
        }

        if (responseObject.generalProfile!!.socialMediasList[2].isConnected) {
            binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_socail_linkedin)
            isLinkedInConnected = true
            binding!!.btnLinkedInConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_opt_cat)
            binding!!.btnLinkedInConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.purple_color))
            binding!!.btnLinkedInConnect.text = "Disconnect"
        } else {
            HooleyApp.db.remove(Constants.LINKEDIN_USER_ID)
            isLinkedInConnected = false
            binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_social_linkedin_dissconnected)
            binding!!.btnLinkedInConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_add)
            binding!!.btnLinkedInConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white))
            binding!!.btnLinkedInConnect.text = "Connect"
        }

        if (responseObject.generalProfile!!.socialMediasList[3].isConnected) {
            binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram)
            isInstaConnected = true
            binding!!.btnInstagramConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_opt_cat)
            binding!!.btnInstagramConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.purple_color))
            binding!!.btnInstagramConnect.text = "Disconnect"
        } else {
            HooleyApp.db.remove(Constants.INSTAGRAM_USER_ID)
            isInstaConnected = false
            binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram_dissconnect)
            binding!!.btnInstagramConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_add)
            binding!!.btnInstagramConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white))
            binding!!.btnInstagramConnect.text = "Connect"
        }

        if (responseObject.generalProfile!!.socialMediasList[4].isConnected) {
            binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_socail_filker)
            isFlickerConnected = true
            binding!!.btnFlickerConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_opt_cat)
            binding!!.btnFlickerConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.purple_color))
            binding!!.btnFlickerConnect.text = "Disconnect"
        } else {
            HooleyApp.db.remove(Constants.FLICKER_USER_ID)
            isFlickerConnected = false
            binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_social_filker_dissconnect)
            binding!!.btnFlickerConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_add)
            binding!!.btnFlickerConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white))
            binding!!.btnFlickerConnect.text = "Connect"
        }

    }

    private fun setAddInterstRv() {
        adapterAddInterest = AdapterAddInterest(HooleyMain.activity!!, responseObject.generalProfile!!.userInterestList)
        adapterAddInterest!!.setmListener(this)
        binding!!.gvInterest.adapter = adapterAddInterest

    }

    fun showDateDialog() {
        val dialogView = View.inflate(HooleyMain.activity, R.layout.date_picker, null)
        val alertDialog = AlertDialog.Builder(HooleyMain.activity!!).create()
        val datePicker = dialogView.findViewById<DatePicker>(R.id.date_picker)
        val c = Calendar.getInstance()
        val mYear: Int
        val mMonth: Int
        val mDay: Int
        if (!TextUtils.isEmpty(binding!!.tvDob.text.toString())) {
            val splitedDate = binding!!.tvDob.text.toString().split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            mYear = Integer.parseInt(splitedDate[2])
            mMonth = Integer.parseInt(splitedDate[0]) - 1
            mDay = Integer.parseInt(splitedDate[1])
        } else {
            mYear = c.get(Calendar.YEAR)
            mMonth = c.get(Calendar.MONTH)
            mDay = c.get(Calendar.DAY_OF_MONTH)
        }
        datePicker.updateDate(mYear, mMonth, mDay)
//        datePicker.maxDate = (System.currentTimeMillis() - 1000.0 * 60.0 * 60.0 * 24.0 * 365.25 * 1.0).toLong()
        datePicker.maxDate = System.currentTimeMillis()
        dialogView.findViewById<View>(R.id.rlSetTimeStamp).setOnClickListener {
            val bDate = (datePicker.month + 1).toString() + "/" + datePicker.dayOfMonth + "/" + datePicker.year
            try {
                if (Util.compareBirthDate(bDate) <= 1) {
                    binding!!.tvDob.text = bDate
                    alertDialog.dismiss()
                } else {
                    Util.showToastMessage(HooleyMain.activity!!, "Birth date not valid")
                }
            } catch (e: ParseException) {
                e.printStackTrace()
            }
        }
        alertDialog.setView(dialogView)
        alertDialog.show()
    }

    private fun setCollegeMajorSpinner() {
        val obj = PersonalProfileModel.myObject()
        obj.id = "0"
        obj.name = "College Major"
        model.collageMajorList!!.add(0, obj)
        spinnerAdapter = AdapterSpinner(HooleyMain.activity, R.layout.sp_gender_group_item, model.collageMajorList)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spCollegeMajor.adapter = spinnerAdapter
        if (model.collageMajorList != null) {
            if (!responseObject.generalProfile!!.userInfo.collageMajor.equals("0", ignoreCase = true)) {
                for (i in model.collageMajorList!!.indices) {
                    if (model.collageMajorList!![i].id.equals(responseObject.generalProfile!!.userInfo.collageMajor, ignoreCase = true)) {
                        binding!!.spCollegeMajor.setSelection(i)
                    }
                }
            }
        }
    }

    private fun setRelationShipSpinner() {
        val obj = PersonalProfileModel.myObject()
        obj.id = "0"
        obj.name = "Status"
        model.relationshipList!!.add(0, obj)
        spinnerAdapter = AdapterSpinner(HooleyMain.activity, R.layout.sp_gender_group_item, model.relationshipList)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spRelationShip.adapter = spinnerAdapter
        if (!responseObject.generalProfile!!.userInfo.relationshipId.equals("0", ignoreCase = true)) {
            for (i in model.relationshipList!!.indices) {
                if (model.relationshipList!![i].id.equals(responseObject.generalProfile!!.userInfo.relationshipId, ignoreCase = true)) {
                    binding!!.spRelationShip.setSelection(i)
                }
            }
        }
    }

    private fun setPolicitcalViewSpinner() {
        val obj = PersonalProfileModel.myObject()
        obj.id = "0"
        obj.name = "Political View"
        model.politicalViewsList!!.add(0, obj)
        spinnerAdapter = AdapterSpinner(HooleyMain.activity, R.layout.sp_gender_group_item, model.politicalViewsList)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spPoliticalView.adapter = spinnerAdapter
        if (!responseObject.generalProfile!!.userInfo.politicalViewId.equals("0", ignoreCase = true)) {
            for (i in model.politicalViewsList!!.indices) {
                if (model.politicalViewsList!![i].id.equals(responseObject.generalProfile!!.userInfo.politicalViewId, ignoreCase = true)) {
                    binding!!.spPoliticalView.setSelection(i)
                }
            }
        }
    }

    private fun setUserOccupationSpinner() {
        val obj = PersonalProfileModel.myObject()
        obj.id = "0"
        obj.name = "Occupation"
        model.userOccupation!!.add(0, obj)
        spinnerAdapter = AdapterSpinner(HooleyMain.activity, R.layout.sp_gender_group_item, model.userOccupation)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spOccupation.adapter = spinnerAdapter
        if (!responseObject.generalProfile!!.userInfo.occupationId.equals("0", ignoreCase = true)) {
            for (i in model.userOccupation!!.indices) {
                if (model.userOccupation!![i].id.equals(responseObject.generalProfile!!.userInfo.occupationId, ignoreCase = true)) {
                    binding!!.spOccupation.setSelection(i)
                }
            }
        }
    }

    private fun setReligionSpinner() {
        val obj = PersonalProfileModel.myObject()
        obj.id = "0"
        obj.name = "Religion"
        model.religionList!!.add(0, obj)
        spinnerAdapter = AdapterSpinner(HooleyMain.activity, R.layout.sp_gender_group_item, model.religionList)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spReligion.adapter = spinnerAdapter
        if (!responseObject.generalProfile!!.userInfo.religionId.equals("0", ignoreCase = true)) {
            for (i in model.religionList!!.indices) {
                if (model.religionList!![i].id.equals(responseObject.generalProfile!!.userInfo.religionId, ignoreCase = true)) {
                    binding!!.spReligion.setSelection(i)
                }
            }
        }
    }

    private fun setLanguagesSpinner() {
        val obj = PersonalProfileModel.myObject()
        obj.id = "0"
        obj.name = "Language"
        model.languagesList!!.add(0, obj)
        spinnerAdapter = AdapterSpinner(HooleyMain.activity, R.layout.sp_gender_group_item, model.languagesList)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spLanguages.adapter = spinnerAdapter
        if (!responseObject.generalProfile!!.userInfo.languageId.equals("0", ignoreCase = true)) {
            for (i in model.languagesList!!.indices) {
                if (model.languagesList!![i].id.equals(responseObject.generalProfile!!.userInfo.languageId, ignoreCase = true)) {
                    binding!!.spLanguages.setSelection(i)
                }
            }
        }
    }

    private fun setEthnicSpinner() {
        val obj = PersonalProfileModel.myObject()
        obj.id = "0"
        obj.name = "Nationality"
        model.ethnicBackgroundList!!.add(0, obj)
        spinnerAdapter = AdapterSpinner(HooleyMain.activity, R.layout.sp_gender_group_item, model.ethnicBackgroundList)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spEthnicBg.adapter = spinnerAdapter
        if (!responseObject.generalProfile!!.userInfo.ethnicBackgroundId.equals("0", ignoreCase = true)) {
            for (i in model.ethnicBackgroundList!!.indices) {
                if (model.ethnicBackgroundList!![i].id.equals(responseObject.generalProfile!!.userInfo.ethnicBackgroundId, ignoreCase = true)) {
                    binding!!.spEthnicBg.setSelection(i)
                }
            }
        }
    }

    private fun setAstrologicalSignsSpinner() {
        val obj = PersonalProfileModel.myObject()
        obj.id = "0"
        obj.name = "Zodiac Sign"
        model.astrologicalSigns!!.add(0, obj)
        spinnerAdapter = AdapterSpinner(HooleyMain.activity, R.layout.sp_gender_group_item, model.astrologicalSigns)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spAstrologicalSign.adapter = spinnerAdapter
        if (!responseObject.generalProfile!!.userInfo.astrologicalSignId.equals("0", ignoreCase = true)) {
            for (i in model.astrologicalSigns!!.indices) {
                if (model.astrologicalSigns!![i].id.equals(responseObject.generalProfile!!.userInfo.astrologicalSignId, ignoreCase = true)) {
                    binding!!.spAstrologicalSign.setSelection(i)
                }
            }
        }
    }

    private fun setGender() {
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.gender))
            if (responseObject.generalProfile!!.userInfo.gender.equals("1", ignoreCase = true))
                binding!!.tvGender.text = "Male"
            else if (responseObject.generalProfile!!.userInfo.gender.equals("2", ignoreCase = true)) {
                binding!!.tvGender.text = "Female"
            } else {
                binding!!.tvGender.text = "Transgender"
            }
    }

    private fun setCountrySpinner(response: CountryStateCitiesModel) {
        if (countryList != null && countryList!!.size > 0)
            countryList!!.clear()
        countryList = response.countryStateCitiesList
        adapterSpinner = AdapterCountries(HooleyMain.activity!!, R.layout.sp_gender_group_item, countryList)
        adapterSpinner!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spCountry.adapter = adapterSpinner
        binding!!.spCountry.onItemSelectedListener = this
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.country)) {
            for (loop in countryList!!.indices) {
                if (countryList!![loop].name.equals(responseObject.generalProfile!!.userInfo.country, ignoreCase = true)) {
                    binding!!.spCountry.setSelection(loop)
                    binding!!.tvCountry.visibility = View.GONE
                }
            }
        }
    }

    private fun setStateSpinner(response: CountryStateCitiesModel) {
        if (stateList != null && stateList!!.size > 0)
            stateList!!.clear()
        stateList = response.countryStateCitiesList
        Collections.sort(stateList!!, PersonalProfileModel.sortByNameComparator)
        adapterSpinner = AdapterCountries(HooleyMain.activity!!, R.layout.sp_gender_group_item, stateList)
        adapterSpinner!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spState.adapter = adapterSpinner
        binding!!.spState.onItemSelectedListener = this
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.state)) {
            for (loop in stateList!!.indices) {
                if (stateList!![loop].name.equals(responseObject.generalProfile!!.userInfo.state, ignoreCase = true)) {
                    binding!!.spState.setSelection(loop)
                    binding!!.tvState.visibility = View.GONE
                }
            }
        }

    }

    private fun setCitySpinner(response: CountryStateCitiesModel) {
        if (citiesList != null && citiesList!!.size > 0)
            citiesList!!.clear()
        citiesList = response.countryStateCitiesList
        Collections.sort(citiesList!!, PersonalProfileModel.sortByNameComparator)
        adapterSpinner = AdapterCountries(HooleyMain.activity!!, R.layout.sp_gender_group_item, citiesList)

        adapterSpinner!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spCity.adapter = adapterSpinner
        binding!!.spCity.onItemSelectedListener = this
        if (!TextUtils.isEmpty(responseObject.generalProfile!!.userInfo.currentCity)) {
            for (loop in citiesList!!.indices) {
                if (citiesList!![loop].name.equals(responseObject.generalProfile!!.userInfo.currentCity, ignoreCase = true)) {
                    binding!!.tvCity.visibility = View.GONE
                    binding!!.spCity.setSelection(loop)
                }
            }
            isCitySet = true
        }
    }

    private fun VerifyNumberDialog(number: String) {
        isValid = true
        verifyNumberDialog = Dialog(HooleyMain.activity!!)
        verifyNumberDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        verifyNumberDialog!!.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        verifyNumberDialog!!.setContentView(R.layout.dialog_verify_number)
        val btnVerifyCode = verifyNumberDialog!!.findViewById<Button>(R.id.btnVerifyCode)
        val ivClose = verifyNumberDialog!!.findViewById<ImageView>(R.id.ivClose)
        val tvPhoneNumber = verifyNumberDialog!!.findViewById<TextView>(R.id.tvPhoneNumber)
        val tvResendCode = verifyNumberDialog!!.findViewById<TextView>(R.id.tvResendCode)
        val tvTimer = verifyNumberDialog!!.findViewById<TextView>(R.id.tvTimer)
        val rlResendCode = verifyNumberDialog!!.findViewById<RelativeLayout>(R.id.rlResendCode)
        tvPhoneNumber.text = number
        tvResendCode.setOnClickListener { verifyNumber() }
        val pincode = verifyNumberDialog!!.findViewById<PinEntryEditText>(R.id.pinCode)
        ivClose.setOnClickListener { verifyNumberDialog!!.dismiss() }

        btnVerifyCode.setOnClickListener(View.OnClickListener {
            if (Util.isContentNull(pincode.text.toString())) {
                pincode.requestFocus()
                pincode.error = getString(R.string.required)
                return@OnClickListener
            }

            if (TextUtils.isEmpty(messageId)) {
                Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_server_not_responding))
                return@OnClickListener
            }
            if (isValid) {
                verifyPinCode(pincode.text.toString())
                verifyNumberDialog!!.dismiss()
            } else {
                Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_invalid_code))
            }
        })

        val currentDateTimeString = SimpleDateFormat("HH:mm:ss").format(Date())
        val format = SimpleDateFormat("HH:mm:ss")
        var date2: Date? = null
        try {
            date2 = format.parse(currentDateTimeString)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        countDownTimer = object : CountDownTimer((60 * 3 * 1000).toLong(), 100) {
            override fun onTick(l: Long) {
                val seconds = l / 1000
                val currentMinute = seconds / 60
                tvTimer.text = String.format("%02d", seconds / 60) + ":" + String.format("%02d", seconds % 60)
                if (currentMinute == 0L && seconds == 0L) {
                    rlResendCode.visibility = View.VISIBLE
                    isValid = false
                    tvTimer.text = "00:00"
                    if (countDownTimer != null) {
                        countDownTimer!!.cancel()
                    }
                } else {

                }
            }

            override fun onFinish() {
                rlResendCode.visibility = View.VISIBLE
                tvTimer.text = "00:00"
                isValid = false
                if (countDownTimer != null) {
                    countDownTimer!!.cancel()
                }
            }
        }.start()
        verifyNumberDialog!!.show()
    }

    private fun validateInput(): Boolean {
        if (Util.isContentNull(binding!!.edtPhone.text.toString())) {
            binding!!.edtPhone.requestFocus()
            binding!!.edtPhone.error = getString(R.string.ic_add_number)
            return false
            //        } else if (!userSearchbinding.ccp.isValid()) {
            //            userSearchbinding.edtPhone.requestFocus();
            //            userSearchbinding.edtPhone.setError("Please enter a valid number");
            //            return false;
//        } else if (Util.isContentNull(binding!!.edtAboutMe.text.toString())) {
//            binding!!.edtAboutMe.requestFocus()
//            binding!!.edtAboutMe.error = getString(R.string.about_me)
//            return false
        } else if (!Util.isContentNull(binding!!.edtWebsite.text.toString()) && !android.util.Patterns.WEB_URL.matcher(binding!!.edtWebsite.text).matches()) {
            binding!!.edtWebsite.requestFocus()
            binding!!.edtWebsite.error = getString(R.string.str_url)
            return false
//        } else if (Util.isContentNull(binding!!.edtHomeTown.text.toString())) {
//            binding!!.edtHomeTown.requestFocus()
//            binding!!.edtHomeTown.error = getString(R.string.hometown_required)
//            return false
//        } else if (Util.isContentNull(userSearchbinding!!.spEthnicBg.selectedItem.toString())) {
//            userSearchbinding!!.spEthnicBg.requestFocus()
//            Util.showToastMessage(HooleyMain.activity!!, getString(R.string.nationality_required))
//            return false
        } else if (binding!!.edtPhone.text.toString().length > 10) {
            binding!!.edtPhone.requestFocus()
            binding!!.edtPhone.error = "Please enter a valid number"
            return false
        } else
            return isCitySet

        /*else if (Util.isContentNull(userSearchbinding.tvDob.getText().toString())) {
            userSearchbinding.edtWebsite.requestFocus();
            Util.showToastMessage(HooleyMain.Companion.getActivity(), getString(R.string.str_dob));
            return false;*/
    }

    private fun setSocailMedia(socialName: String) {
        if (responseObject.generalProfile!!.socialMediasList.size > 0) {
            for (i in responseObject.generalProfile!!.socialMediasList.indices) {
                if (socialName.equals(responseObject.generalProfile!!.socialMediasList[i].name, ignoreCase = true)) {
                    when (responseObject.generalProfile!!.socialMediasList[i].name) {
                        Constants.SOCIAL_FB -> if (isFBConnected) {
                            isConnected = true
                            isFBConnected = true
                            socialId = responseObject.generalProfile!!.socialMediasList[i].id
                            socialUrl = Constants.FB_BASE_URL + HooleyApp.db.getString(Constants.SOCIAL_USER_ID)
                            updateUserSocial()
                        } else {
                            isConnected = false
                            isFBConnected = false
                            socialId = responseObject.generalProfile!!.socialMediasList[i].id
                            socialUrl = ""
                            updateUserSocial()
                        }
                        Constants.SOCIAL_TW -> if (isTwitterConnected) {
                            isConnected = true
                            isTwitterConnected = true
                            socialId = responseObject.generalProfile!!.socialMediasList[i].id
                            socialUrl = Constants.TWITTER_BASE_URL + HooleyApp.db.getString(Constants.TWITTER_USER_NAME)
                            updateUserSocial()
                        } else {
                            isConnected = false
                            isTwitterConnected = false
                            socialId = responseObject.generalProfile!!.socialMediasList[i].id
                            socialUrl = ""
                            updateUserSocial()
                        }

                        Constants.SOCIAL_INSTA -> if (isInstaConnected) {
                            isConnected = true
                            socialId = responseObject.generalProfile!!.socialMediasList[i].id
                            socialUrl = Constants.INSTAGRAM_BASE_URL + HooleyApp.db.getString(Constants.INSTAGRAM_USER_NAME)
                            updateUserSocial()
                        } else {
                            isConnected = false
                            socialId = responseObject.generalProfile!!.socialMediasList[i].id
                            socialUrl = ""
                            updateUserSocial()
                        }

                        Constants.SOCIAL_LI -> if (isLinkedInConnected) {
                            isConnected = true
                            socialId = responseObject.generalProfile!!.socialMediasList[i].id
                            socialUrl = HooleyApp.db.getString(Constants.LINKEDIN_PROFILE_URL)
                            updateUserSocial()
                        } else {
                            isConnected = false
                            socialId = responseObject.generalProfile!!.socialMediasList[i].id
                            socialUrl = ""
                            updateUserSocial()
                        }

                        Constants.SOCIAL_FLKR -> if (isFlickerConnected) {
                            responseObject.generalProfile!!.socialMediasList[i].socialUrl = "Flicker"
                            responseObject.generalProfile!!.socialMediasList[i].isConnected = true
                            socialUrl = Constants.FLICKER_BASE_URL + ""
                            updateUserSocial()
                        } else {
                            isConnected = false
                            socialId = responseObject.generalProfile!!.socialMediasList[i].id
                            socialUrl = ""
                            updateUserSocial()
                        }
                    }

                }

            }
        }
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        when (parent.id) {
            R.id.spRelationShip -> relationShipPosition = position
            R.id.spPoliticalView -> politicalViewPosition = position
            R.id.spReligion -> religionPosition = position
            R.id.spLanguages -> languagePosition = position
            R.id.spEthnicBg -> ethnicPosition = position
            R.id.spAstrologicalSign -> astrologicalSignId = position
            R.id.spCollegeMajor -> collegeMajorPosition = position
            R.id.spOccupation -> userOccupationPosition = position
            R.id.spCountry -> {
                strCountry = countryList!![position].name
                strCountryId = countryList!![position].id
                getCountries("state", strCountryId, "0", Constants.TYPE_STATE)
            }
            R.id.spState -> {
                strState = stateList!![position].name
                strStateId = stateList!![position].id
                getCountries("city", strCountryId, strStateId, Constants.TYPE_CITY)
            }
            R.id.spCity -> {
                strCity = citiesList!![position].name
                strCityId = citiesList!![position].id
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    private fun setMoreCallerBackListener() {
        val arrayList = ArrayList<String>()
        arrayList.add("Photo")
        arrayList.add("Gallery")
        moreDialogeFragment = MoreDialogeFragment.Builder(activity!!).setTitle("Select").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, _ ->
            when (position) {
                0 -> {
                    moreDialogeFragment!!.dismiss()
                    HooleyMain.activity!!.startCamera(Constants.CAMERA_RQ)
                }
                1 -> {
                    moreDialogeFragment!!.dismiss()
                    HooleyMain.activity!!.showGallery(Constants.IMAGE_PICKER_SELECT)
                }
            }
        }.create()

        if (!moreDialogeFragment!!.isAdded)
            moreDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tvDob -> showDateDialog()
            R.id.ivAddPhoto -> setMoreCallerBackListener()

            R.id.btnSaveProfile -> if (validateInput())
                saveProfile()

            R.id.btnAddInterest -> if (TextUtils.isEmpty(binding!!.edtInterest.text.toString().trim { it <= ' ' })) {
                binding!!.edtInterest.requestFocus()
                binding!!.edtInterest.error = getString(R.string.required)
            } else {
                var found = false
                for (loop in responseObject.generalProfile!!.userInterestList.indices) {
                    if (binding!!.edtInterest.text.toString().trim { it <= ' ' }.equals(responseObject.generalProfile!!.userInterestList[loop].name, ignoreCase = true)) {
                        binding!!.edtInterest.requestFocus()
                        binding!!.edtInterest.error = getString(R.string.already_entered)
                        found = true
                    }
                }
                if (!found) {
                    addUserInterests(binding!!.edtInterest.text.toString())
                }

            }

            R.id.btnNotVerified -> {
                if (TextUtils.isEmpty(binding!!.edtPhone.text.toString())) {
                    binding!!.edtPhone.requestFocus()
                    binding!!.edtPhone.error = getString(R.string.required)
                    return
                }
                if (binding!!.edtPhone.text.toString().length > 10) {
                    binding!!.edtPhone.requestFocus()
                    binding!!.edtPhone.error = "Please enter a valid number"
                    return
                }
                if (TextUtils.isEmpty(cc)) {
                    Util.showToastMessage(HooleyMain.activity!!, getString(R.string.ic_add_cc))
                    return
                }
                verifyNumber()
                VerifyNumberDialog("+" + cc + binding!!.edtPhone.text.toString())
            }
            R.id.btnFacebookConnect -> {
                if (!isFBConnected) {
                    AccessToken.setCurrentAccessToken(null)
                    if (AccessToken.getCurrentAccessToken() == null) {
                        socialClass.loginWithFacebook(HooleyMain.activity)
                    }
                } else {
                    isFBConnected = false
                    binding!!.ivFacebook.setBackgroundResource(R.drawable.ic_social_fb_dissconnect)
                    LoginManager.getInstance().logOut()
                    HooleyApp.db.remove(Constants.SOCIAL_USER_ID)
                    setSocailMedia(Constants.SOCIAL_FB)
                    binding!!.btnFacebookConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_add)
                    binding!!.btnFacebookConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white))
                    binding!!.btnFacebookConnect.text = "Connect"
                }
            }
            R.id.btnTwitterConnect -> {
                if (!isTwitterConnected) {
                    if (HooleyApp.db.getString(Constants.TWITTER_USER_ID).isEmpty()) {
                        socialClass.loginWithTwitter()
                    }
                } else {
                    if (twitterAuthClient != null)
                        twitterAuthClient!!.cancelAuthorize()
                    val session = TwitterCore.getInstance().sessionManager.activeSession
                    if (session != null) {
//                        ClearCookies(getApplicationContext());
//                        Twitter.getSessionManager().clearActiveSession();
//                        Twitter.logOut();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                            CookieManager.getInstance().removeAllCookies(null)
                            CookieManager.getInstance().flush()
                        } else {
                            CookieSyncManager.createInstance(HooleyMain.activity!!).startSync()
                            CookieManager.getInstance().removeAllCookie()
                            CookieManager.getInstance().removeSessionCookie()
                            CookieSyncManager.getInstance().stopSync()
                            CookieSyncManager.getInstance().sync()
                        }
                        TwitterCore.getInstance().sessionManager.clearActiveSession()
                    }
                    isTwitterConnected = false
                    HooleyApp.db.remove(Constants.TWITTER_USER_ID)
                    binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_social_twitter_dissconnect)
                    setSocailMedia(Constants.SOCIAL_TW)

                    binding!!.btnTwitterConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_add)
                    binding!!.btnTwitterConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white))
                    binding!!.btnTwitterConnect.text = "Connect"
                }
            }
            R.id.btnLinkedInConnect -> {
                if (!isLinkedInConnected) {
                    if (HooleyApp.db.getString(Constants.LINKEDIN_USER_ID).isEmpty()) {
                        socialClass.loginLinkedIn()
                    }
                } else {
                    isLinkedInConnected = false
                    HooleyApp.db.remove(Constants.LINKEDIN_USER_ID)
                    binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_social_linkedin_dissconnected)
                    setSocailMedia(Constants.SOCIAL_LI)
                    binding!!.btnLinkedInConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_add)
                    binding!!.btnLinkedInConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white))
                    binding!!.btnLinkedInConnect.text = "Connect"
                }
            }
            R.id.btnInstagramConnect -> {
                if (!isInstaConnected) {

                    if (HooleyApp.db.getString(Constants.INSTAGRAM_USER_ID).isEmpty()) {
                        instagramAuthenticationFragment = InstagramAuthenticationFragment.newInstance()
                        instagramAuthenticationFragment!!.setTargetFragment(this@EditPersonalProfileFragment, REQUEST_CODE_INSTA)
                        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, instagramAuthenticationFragment!!, "instagramAuthenticationFragment")
                    }
                } else {
                    isInstaConnected = false
                    HooleyApp.db.remove(Constants.INSTAGRAM_USER_ID)
                    HooleyApp.db.remove(Constants.INSTAGRAM_USER_NAME)
                    binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram_dissconnect)
                    setSocailMedia(Constants.SOCIAL_INSTA)
                    binding!!.btnInstagramConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_add)
                    binding!!.btnInstagramConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white))
                    binding!!.btnInstagramConnect.text = "Connect"
                }
            }
            R.id.btnFlickerConnect -> {
                if (!isFlickerConnected) {
                    var url = ""
                    HooleyMain.activity!!.showDialog()

                    object : FlickrClient.RemoteDataTask() {
                        override fun onPostExecute(result: String?) {
                            super.onPostExecute(result)
                            url = FlickrClient.authorizationUrl
                            HooleyMain.activity!!.removeDialog()
                            flickrAuthFragment = FlickrAuthFragment.newInstance(url)
                            flickrAuthFragment!!.setTargetFragment(this@EditPersonalProfileFragment, REQUEST_CODE_FLICKR)
                            (HooleyMain.activity as ActivityBase).addFragment(R.id.container, flickrAuthFragment!!, "FlickrAuthFragment")
                        }
                    }.execute()
                } else {
                    isFlickerConnected = false
                    binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_social_filker_dissconnect)
                    binding!!.btnFlickerConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_add)
                    binding!!.btnFlickerConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white))
                    binding!!.btnFlickerConnect.text = "Connect"
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_FLICKR) {
                //            try {
                if (FlickrClient.logIn(data!!.getStringExtra("oauth_verifier"))) {
                    binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_socail_filker)
                    setSocailMedia(Constants.SOCIAL_FLKR)
                    binding!!.btnFlickerConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_opt_cat)
                    binding!!.btnFlickerConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.purple_color))
                    binding!!.btnFlickerConnect.text = "Disconnect"
                    isFlickerConnected = true
                } else {
                    binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_social_filker_dissconnect)
                    binding!!.btnFlickerConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_add)
                    binding!!.btnFlickerConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white))
                    binding!!.btnFlickerConnect.text = "Connect"
                    isFlickerConnected = false
                }
            } else if (TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE == requestCode) {
                twitterAuthClient!!.onActivityResult(requestCode, resultCode, data)
                isTwitterConnected = true
                binding!!.btnTwitterConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_opt_cat)
                binding!!.btnTwitterConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.purple_color))
                binding!!.btnTwitterConnect.text = "Disconnect"
            } else if (FacebookSdk.isFacebookRequestCode(requestCode)) {
                //FacebookSdk.isFacebookRequestCode(requestCode)
                callbackManager!!.onActivityResult(requestCode, resultCode, data)
                binding!!.btnFacebookConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_add)
                binding!!.btnFacebookConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white))
                binding!!.btnFacebookConnect.text = "Disconnect"
                isFBConnected = true
            } else if (requestCode == REQUEST_CODE_INSTA) {
                intsaAccessToken = data!!.getStringExtra("access_token")
                getUserInstaDetail()
                //connectStripe();
                Log.e("InstaAccessToken", " " + data.getStringExtra("access_token"))
                binding!!.btnInstagramConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_opt_cat)
                binding!!.btnInstagramConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.purple_color))
                binding!!.btnInstagramConnect.text = "Disconnect"
                isInstaConnected = true
            } else if (requestCode == Constants.CAMERA_RQ) {
                HooleyMain.activity!!.processCapturedPhoto(object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        mediaUri = HooleyMain.activity!!.compressFile(result!!)
                        binding!!.ivAvatar.setImageBitmap(HooleyMain.activity!!.handleSamplingAndRotationBitmap(result))
                    }

                })
            } else if (requestCode == Constants.IMAGE_PICKER_SELECT) {
                HooleyMain.activity!!.processGalleryPhoto(data!!, object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        mediaUri = HooleyMain.activity!!.compressFile(result!!)
                        binding!!.ivAvatar.setImageBitmap(HooleyMain.activity!!.handleSamplingAndRotationBitmap(result))
                    }

                })
            } else {
                LISessionManager.getInstance(HooleyMain.activity!!)
                        .onActivityResult(HooleyMain.activity,
                                requestCode, resultCode, data)
            }
        } else {
            if (requestCode == REQUEST_CODE_FLICKR) {
                isFlickerConnected = false
            } else if (requestCode == REQUEST_CODE_INSTA) {
                isInstaConnected = false
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
        inflater!!.inflate(R.menu.gallery_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.save -> {
                if (validateInput()) {
                    saveProfile()
                    return true
                }
                return super.onOptionsItemSelected(item)
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
        //        HooleyMain.Companion.getActivity().getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);
        Util.hideKeyboard(HooleyMain.activity!!)

    }

    override fun onClickItem(name: String, position: Int) {
        deleteUserInterests(name, position)
    }

    override fun onCountrySelected(country: Country) {
        cc = country.phoneCode
        ccIso = country.iso
    }

    override fun onCallBackFaceBook(isLogin: Boolean) {
        if (isLogin) {
            binding!!.ivFacebook.setBackgroundResource(R.drawable.ic_socail_fb)
            setSocailMedia(Constants.SOCIAL_FB)
            binding!!.btnFacebookConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_opt_cat)
            binding!!.btnFacebookConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.purple_color))
            binding!!.btnFacebookConnect.text = "Disconnect"
        } else {
            binding!!.ivFacebook.setBackgroundResource(R.drawable.ic_social_fb_dissconnect)
            binding!!.btnFacebookConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_add)
            binding!!.btnFacebookConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white))
            binding!!.btnFacebookConnect.text = "Connect"
        }
    }

    override fun onCallBackTwitter(isLogin: Boolean) {
        if (isLogin) {
            binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_socail_twitter)
            isTwitterConnected = true
            setSocailMedia(Constants.SOCIAL_TW)
            binding!!.btnTwitterConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_opt_cat)
            binding!!.btnTwitterConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.purple_color))
            binding!!.btnTwitterConnect.text = "Disconnect"
        } else {
            binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_social_twitter_dissconnect)
            isTwitterConnected = false
            binding!!.btnTwitterConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_add)
            binding!!.btnTwitterConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white))
            binding!!.btnTwitterConnect.text = "Connect"
        }

    }

    override fun onCallBackLinkedIn(isLogin: Boolean) {
        if (isLogin) {
            isLinkedInConnected = true
            binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_socail_linkedin)
            setSocailMedia(Constants.SOCIAL_LI)
            binding!!.btnLinkedInConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_opt_cat)
            binding!!.btnLinkedInConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.purple_color))
            binding!!.btnLinkedInConnect.text = "Disconnect"
        } else {
            binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_social_linkedin_dissconnected)
            isLinkedInConnected = false
            binding!!.btnLinkedInConnect.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_add)
            binding!!.btnLinkedInConnect.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white))
            binding!!.btnLinkedInConnect.text = "Connect"
        }
    }

    override fun onCallBackGooglePlus(isLogin: Boolean) {

    }

    private fun verifyPinCode(code: String) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        HooleyMain.activity!!.showDialog()
        service.verifyPinCode(messageId!!, code, false, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                binding!!.btnNotVerified.visibility = View.GONE
                binding!!.ivVerified.visibility = View.VISIBLE
                if (countDownTimer != null) {
                    countDownTimer!!.cancel()
                }
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }

    private fun verifyNumber() {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        HooleyMain.activity!!.showDialog()
        val data = cc + binding!!.edtPhone.text.toString()
        service.verifyNumber(binding!!.edtPhone.text.toString(), cc, data, object : GenericCallback<VerifyPhoneNumberModel> {
            override fun success(result: VerifyPhoneNumberModel) {
                HooleyMain.activity!!.removeDialog()
                messageId = result.apiMessageId
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_server_not_responding))
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun saveProfile() {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        HooleyApp.db.putString(Constants.USER_AVATAR, personalImageUrl)
        HooleyMain.activity!!.showDialog()
        service.updatePersonalProfile(mediaUri, personalImageUrl, binding!!.edtFirstName.text.toString().trim { it <= ' ' }, "",
                binding!!.edtAboutMe.text.toString().trim { it <= ' ' }, cc, ccIso, binding!!.edtPhone.text.toString(), binding!!.edtHomeTown.text.toString().trim { it <= ' ' }, binding!!.tvEmail.text.toString().trim { it <= ' ' }, binding!!.edtWebsite.text.toString().trim { it <= ' ' },
                responseObject.generalProfile!!.userInfo.gender!!, binding!!.tvDob.text.toString().trim { it <= ' ' }, model.relationshipList!![relationShipPosition].id!!, model.religionList!![religionPosition].id!!,
                binding!!.edtHobbies.text.toString().trim { it <= ' ' }, binding!!.edtCollege.text.toString().trim { it <= ' ' }, binding!!.edtHighSchool.text.toString().trim { it <= ' ' }, model.collageMajorList!![collegeMajorPosition].id!!, model.astrologicalSigns!![astrologicalSignId].id!!, model.politicalViewsList!![politicalViewPosition].id!!,
                model.languagesList!![languagePosition].id!!, model.ethnicBackgroundList!![ethnicPosition].id!!, binding!!.edtEmployer.text.toString().trim { it <= ' ' }, binding!!.edtOrganizations.text.toString().trim { it <= ' ' }, model.userOccupation!![userOccupationPosition].id!!, strCityId!!, strCountryId!!, strStateId!!, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun deleteUserInterests(name: String, position: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        HooleyMain.activity!!.showDialog()
        service.deleteUserInterest(name, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                responseObject.generalProfile!!.userInterestList.removeAt(position)
                adapterAddInterest!!.notifyDataSetChanged()
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }

    private fun addUserInterests(name: String) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        HooleyMain.activity!!.showDialog()

        service.addUserInterests(name, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                val obj = PersonalProfileModel.myObject()
                obj.id = "0"
                obj.name = name
                responseObject.generalProfile!!.userInterestList.add(obj)
                adapterAddInterest!!.notifyDataSetChanged()
                binding!!.edtInterest.setText("")
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }

    private fun updateUserSocial() {

        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }

        HooleyMain.activity!!.showDialog()
        service.updateUserSocialMedia(socialId!!, socialUrl!!, isConnected, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                Logger.ex("updateUserSocial:" + Gson().toJson(result))
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {

                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun getUserInstaDetail() {
        HooleyMain.activity!!.showDialog()
        val call = HooleyApp.apiService.getUserInstaData(intsaAccessToken)
        call.enqueue(object : Callback<InstaProfileModel> {
            override fun onResponse(call: Call<InstaProfileModel>, response: Response<InstaProfileModel>) {
                HooleyMain.activity!!.removeDialog()
                if (response.body() == null) {
                    Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_server_not_responding))
                    return
                }
                if (response.body()!!.meta!!.code.equals("200", ignoreCase = true)) {
                    Logger.ex("getUserInstaDetail" + Gson().toJson(response))
                    binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram)
                    HooleyApp.db.putString(Constants.INSTAGRAM_USER_ID, response.body()!!.data!!.id!!)
                    HooleyApp.db.putString(Constants.INSTAGRAM_USER_NAME, response.body()!!.data!!.username!!)
                    isInstaConnected = true
                    setSocailMedia(Constants.SOCIAL_INSTA)
                } else {
                    binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram_dissconnect)
                    isInstaConnected = false
                    Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_server_not_responding))
                }
            }

            override fun onFailure(call: Call<InstaProfileModel>, t: Throwable) {
                Logger.ex("ERROR:" + t.message)
                HooleyMain.activity!!.removeDialog()
                Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_server_not_responding))
            }
        })
    }

    private fun getCountries(area: String, countryId: String?, stateId: String?, state: Int) {
        when (state) {
            Constants.TYPE_COUNTRIES -> binding!!.pbCountries.visibility = View.VISIBLE
            Constants.TYPE_STATE -> binding!!.pbState.visibility = View.VISIBLE
            Constants.TYPE_CITY -> binding!!.pbCity.visibility = View.VISIBLE
        }

        service.getCountries(area, countryId!!, stateId!!, object : GenericCallback<CountryStateCitiesModel> {
            override fun success(result: CountryStateCitiesModel) {
                responseCountry = result
                when (state) {
                    Constants.TYPE_COUNTRIES -> {
                        binding!!.pbCountries.visibility = View.INVISIBLE
                        setCountrySpinner(responseCountry)
                    }
                    Constants.TYPE_STATE -> {
                        binding!!.pbState.visibility = View.INVISIBLE
                        binding!!.tvState.visibility = View.GONE
                        binding!!.spState.visibility = View.VISIBLE
                        setStateSpinner(responseCountry)
                    }
                    Constants.TYPE_CITY -> {
                        binding!!.pbCity.visibility = View.INVISIBLE
                        isCitySet = true
                        binding!!.spCity.visibility = View.VISIBLE
                        binding!!.tvCity.visibility = View.GONE
                        setCitySpinner(responseCountry)
                    }
                }
            }

            override fun failure(message: String) {
                when (state) {
                    Constants.TYPE_COUNTRIES -> {
                        binding!!.pbCountries.visibility = View.GONE
                    }
                    Constants.TYPE_STATE -> {
                        binding!!.pbState.visibility = View.GONE
                        binding!!.spState.visibility = View.GONE
                        binding!!.tvState.visibility = View.VISIBLE
                        binding!!.tvState.text = "No State Found"
                    }
                    Constants.TYPE_CITY -> {
                        binding!!.pbCity.visibility = View.GONE
                        binding!!.spCity.visibility = View.GONE
                        binding!!.tvCity.visibility = View.VISIBLE
                        binding!!.tvCity.text = "No City Found"
                    }
                }
            }
        })

    }

    companion object {
        private val REQUEST_CODE_INSTA = 963
        lateinit var responseObject: GetUserProfilesModel
        lateinit var instance: EditPersonalProfileFragment

        fun newInstance(response: GetUserProfilesModel): EditPersonalProfileFragment {
            instance = EditPersonalProfileFragment()
            responseObject = response
            return instance
        }
    }
}
