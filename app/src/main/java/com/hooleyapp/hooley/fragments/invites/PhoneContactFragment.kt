package com.hooleyapp.hooley.fragments.invites

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.database.ContentObserver
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterPhoneContact
import com.hooleyapp.hooley.databinding.FragmentPhoneContactBinding
import com.hooleyapp.hooley.db.DbManager
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GetInviteContactModel
import com.hooleyapp.hooley.model.PhoneContactsModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.ContactWebService
import com.hooleyapp.hooley.tables.ContactNumbers
import java.util.*


/**
 * Created by Nauman on 12/29/2017.
 */

class PhoneContactFragment : BaseFragment(), View.OnClickListener, TextWatcher {

    internal lateinit var binding: FragmentPhoneContactBinding
    private val contactNumberList: ArrayList<ContactNumbers>? = null
    private var mlist: MutableList<PhoneContactsModel>? = ArrayList()
    private var mListSearch: List<PhoneContactsModel> = ArrayList()
    private var adapter: AdapterPhoneContact? = null
    var handler = Handler()
    var mService: ContactWebService? = ContactWebService()
    var smsObserver = YourObserver(Handler())


    private val mContactReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            try {
                handler.postDelayed({ getContactList() }, 200)
            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            }

        }
    }
    private val mSendSmsReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            try {
                handler.postDelayed({ inviteContactBySms() }, 200)
            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            }
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            try {
                sendContactToServer()
                HooleyMain.activity!!.contentResolver.registerContentObserver(Uri.parse("content://sms"), true, smsObserver)
            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            }
        } else {
            try {
                HooleyMain.activity!!.contentResolver.unregisterContentObserver(smsObserver)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_phone_contact, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
        try {
            getContactList()
            LocalBroadcastManager.getInstance(HooleyMain.activity!!).registerReceiver(mContactReceiver,
                    IntentFilter("Contacts"))
            LocalBroadcastManager.getInstance(HooleyMain.activity!!).registerReceiver(mSendSmsReceiver,
                    IntentFilter("SMS"))
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

    }

    private fun setListener() {
        binding.btnSendInvite.setOnClickListener(this)
        binding.edtSearch.addTextChangedListener(this)
    }

    private fun getContactList() {
        if (mlist != null && mlist!!.size > 0)
            mlist!!.clear()
        mlist = DbManager.getContactList()
        getContactFromServer()
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity!!, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity!!)
        binding.rvPhoneContacts.layoutAnimation = animation
        binding.rvPhoneContacts.layoutManager = manager
        adapter = AdapterPhoneContact(HooleyMain.activity!!, mlist)
        binding.rvPhoneContacts.adapter = adapter
    }

    private fun sendContactToServer() {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        if (mlist == null)
            mlist = DbManager.getContactList()
        if (mService == null)
            mService = ContactWebService()
        mService!!.sendContactToServer(mlist, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyApp.db.putBoolean(Constants.SYNC_USER_CONTACTS, true)
            }

            override fun failure(message: String) {
                try {

                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    private fun getContactFromServer() {
        mService!!.getInviteContactList(InviteFragment.createdEventId, object : IWebServiceCallback<GetInviteContactModel> {
            override fun success(result: GetInviteContactModel) {
                if (mlist == null)
                    mlist = DbManager.getContactList()
                if (mlist != null && mlist!!.size > 0) {
                    for (i in mlist!!.indices) {
                        for (j in result.contactNumbersList.indices) {
                            if (mlist!![i].contactNumber.equals(result.contactNumbersList[j], ignoreCase = true)) {
                                mlist!![i].alreadySend = true
                            }
                        }
                    }
                }
                if (adapter != null) {
                    adapter!!.notifyDataSetChanged()
                }
            }

            override fun failure(message: String) {
                try {

                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnSendInvite -> if (validInput())
                inviteBySms()
            else
                Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_selection))
        }
    }

    private fun validInput(): Boolean {
        for (i in mlist!!.indices) {
            if (mlist!![i].checked!!)
                return true
        }
        return false
    }

    private fun inviteBySms() {
        for (i in mlist!!.indices) {
            if (mlist!![i].checked!!)
                sendSMS(mlist!![i].contactNumber,
                        "You've been invited to " + InviteFragment.createdEventName
                                + ".\n Please click the link below for details.\n"
                                + Constants.EVENT_SHARE + InviteFragment.createdEventId)
        }
    }

    fun inviteContactBySms() {
        mService!!.sendInviteToContact(InviteFragment.createdEventId, mlist, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                //                InviteFragment.userSearchbinding.llSocialNetwork.performClick();
            }

            override fun failure(message: String) {
                try {

                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    private fun sendSMS(phoneNumber: String, message: String) {
        val uri = Uri.parse("smsto:$phoneNumber")
        val intent = Intent(Intent.ACTION_SENDTO, uri)
        intent.putExtra("sms_body", message)
        if (intent.resolveActivity(HooleyMain.activity!!.packageManager) != null) {
            HooleyMain.activity!!.startActivityForResult(intent, 200)
        }
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

    }

    override fun afterTextChanged(s: Editable) {
        if (s.length != 0) {
            if (mlist == null)
                return
            mlist!!.clear()
            mListSearch = DbManager.getContactList()
            for (i in mListSearch.indices) {
                if (mListSearch[i].cnName.toLowerCase().contains(s.toString().toLowerCase())) {
                    mlist!!.add(mListSearch[i])
                }
            }

            if (mlist!!.size == 0) {
                binding.tvNoData.visibility = View.VISIBLE
                binding.tvNoData.text = "No Record Found"
            } else {
                binding.tvNoData.visibility = View.GONE
            }

            adapter = AdapterPhoneContact(HooleyMain.activity!!, mlist)
            binding.rvPhoneContacts.adapter = adapter
            // adapter.notifyDataSetChanged();
        } else {
            binding.tvNoData.visibility = View.GONE
            mlist!!.clear()
            mlist = DbManager.getContactList()
            adapter = AdapterPhoneContact(HooleyMain.activity!!, mlist)
            binding.rvPhoneContacts.adapter = adapter
        }
    }

    class YourObserver(handler: Handler) : ContentObserver(handler) {
        var THRESHOLD_CHANGES: Long = 5000// 5 seconds
        var previousTime: Long = 0
        var broadcaster: LocalBroadcastManager = LocalBroadcastManager.getInstance(HooleyMain.activity!!)

        override fun onChange(selfChange: Boolean) {
            super.onChange(selfChange)
            Logger.i("change in message")
            val current = System.currentTimeMillis()
            if (current - previousTime >= THRESHOLD_CHANGES) {
                previousTime = current
                try {
                    Logger.d("SMS SEND")
                    var intent = Intent("SMS")
                    broadcaster.sendBroadcast(intent)
//                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                Logger.i("Ignoring the change request")
            }
        }
    }
}
