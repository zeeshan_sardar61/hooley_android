package com.hooleyapp.hooley.fragments.others;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.DimenRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hooleyapp.hooley.R;
import com.hooleyapp.hooley.adapters.AdapterMoreMediaDialoge;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;

public class MoreMediaDialogFragment extends BottomSheetDialogFragment {

    public static final String TAG = "MoreMediaDialogFragment";

    private View contentView;
    MoreMediaDialogFragment.Builder builder;
    private View view_title_container;
    private RecyclerView rc_gallery;
    private TextView tv_title;
    private Button btn_done;
    private FrameLayout selected_photos_container_frame;
    private HorizontalScrollView hsv_selected_photos;
    private LinearLayout selected_photos_container;
    private TextView selected_photos_empty;
    private ArrayList<String> titles = new ArrayList<>();
    private AdapterMoreMediaDialoge adapterMoreDialoge;
    private Button btn_cancle;

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            Log.d(TAG, "onStateChanged() newState: " + newState);
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismissAllowingStateLoss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            Log.d(TAG, "onSlide() slideOffset: " + slideOffset);
        }
    };


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupSavedInstanceState(savedInstanceState);
    }

    private void setupSavedInstanceState(Bundle savedInstanceState) {
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void show(FragmentManager fragmentManager) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.add(this, getTag());
        ft.commitAllowingStateLoss();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onViewCreated(View contentView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(contentView, savedInstanceState);
    }


    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        contentView = View.inflate(getContext(), R.layout.bottom_picker_layout, null);
        dialog.setContentView(contentView);
        CoordinatorLayout.LayoutParams layoutParams =
                (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = layoutParams.getBehavior();
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
            if (builder != null && builder.peekHeight > 0) {
                ((BottomSheetBehavior) behavior).setPeekHeight(builder.peekHeight);

            }

        }

        ((View) contentView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(30, 0, 30, 50);
        contentView.setLayoutParams(params);


        btn_cancle = contentView.findViewById(R.id.btnMoreCancle);
        btn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        initView(contentView);
        setTitle();
        setRecyclerView();

    }


    private void setTitle() {

        if (!TextUtils.isEmpty(builder.title)) {
            tv_title.setText(builder.title);
        }
    }


    private void onMultiSelectComplete() {
        dismissAllowingStateLoss();
    }

    private void checkMultiMode() {

    }

    private void initView(View contentView) {

        view_title_container = contentView.findViewById(R.id.view_title_container);
        rc_gallery = contentView.findViewById(R.id.rc_gallery);
        tv_title = contentView.findViewById(R.id.tv_title);
        btn_done = contentView.findViewById(R.id.btn_done);
        selected_photos_container_frame = contentView.findViewById(R.id.selected_photos_container_frame);
        hsv_selected_photos = contentView.findViewById(R.id.hsv_selected_photos);
        selected_photos_container = contentView.findViewById(R.id.selected_photos_container);
        selected_photos_empty = contentView.findViewById(R.id.selected_photos_empty);

        btn_done.setVisibility(View.GONE);
        hsv_selected_photos.setVisibility(View.GONE);
        selected_photos_container_frame.setVisibility(View.GONE);


    }


    private void setRecyclerView() {
//        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rc_gallery.setLayoutManager(linearLayoutManager);
//        rc_gallery.addItemDecoration(new GridSpacingItemDecoration(gridLayoutManager.getSpanCount(), builder.spacing, false));
        updateAdapter();
    }

    private void updateAdapter() {

        adapterMoreDialoge = new AdapterMoreMediaDialoge(
                getActivity()
                , builder, builder.titles);
        rc_gallery.setAdapter(adapterMoreDialoge);
        adapterMoreDialoge.setOnItemClickListener(new AdapterMoreMediaDialoge.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, int Type) {
                builder.onImageSelectedListener.onImageSelected(position, Type);
            }

        });
    }


    private void errorMessage(String message) {
        String errorMessage = message == null ? "Something wrong." : message;

        if (builder.onErrorListener == null) {
            Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
        } else {
            builder.onErrorListener.onError(errorMessage);
        }
    }


    public interface OnMultiImageSelectedListener {
        void onImagesSelected(ArrayList<Uri> uriList);
    }

    public interface OnImageSelectedListener {
        void onImageSelected(int position, int type);
    }

    public interface OnErrorListener {
        void onError(String message);
    }


    public static class Builder {

        public Context context;
        public int previewMaxCount = 25;
        public Drawable cameraTileDrawable;
        public Drawable galleryTileDrawable;
        public int peekHeight = -1;
        public Drawable deSelectIconDrawable;
        public Drawable selectedForegroundDrawable;
        public int spacing = 1;
        public MoreMediaDialogFragment.OnImageSelectedListener onImageSelectedListener;
        public MoreMediaDialogFragment.OnMultiImageSelectedListener onMultiImageSelectedListener;
        public MoreMediaDialogFragment.OnErrorListener onErrorListener;


        public ArrayList<AdapterMoreMediaDialoge.MoreItem> titles = new ArrayList<>();
        public String title;
        public int type;

        public @MoreMediaDialogFragment.Builder.MediaType
        int mediaType = MoreMediaDialogFragment.Builder.MediaType.IMAGE;

        public Builder(@NonNull Context context) {
            this.context = context;
            setCameraTile(R.drawable.ic_camera);
            setGalleryTile(R.drawable.ic_gallery);
            setSpacingResId(R.dimen.tedbottompicker_grid_layout_margin);
        }

        public MoreMediaDialogFragment.Builder setCameraTile(@DrawableRes int cameraTileResId) {
            setCameraTile(ContextCompat.getDrawable(context, cameraTileResId));
            return this;
        }

        public MoreMediaDialogFragment.Builder setCameraTile(Drawable cameraTileDrawable) {
            this.cameraTileDrawable = cameraTileDrawable;
            return this;
        }

        public MoreMediaDialogFragment.Builder setGalleryTile(@DrawableRes int galleryTileResId) {
            setGalleryTile(ContextCompat.getDrawable(context, galleryTileResId));
            return this;
        }

        public MoreMediaDialogFragment.Builder setGalleryTile(Drawable galleryTileDrawable) {
            this.galleryTileDrawable = galleryTileDrawable;
            return this;
        }

        public MoreMediaDialogFragment.Builder setSpacingResId(@DimenRes int dimenResId) {
            this.spacing = context.getResources().getDimensionPixelSize(dimenResId);
            return this;
        }


        public MoreMediaDialogFragment.Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public MoreMediaDialogFragment.Builder setTitle(@StringRes int stringResId) {
            this.title = context.getResources().getString(stringResId);
            return this;
        }


        public MoreMediaDialogFragment.Builder setType(int type) {
            this.type = type;
            return this;
        }

        public MoreMediaDialogFragment.Builder setList(ArrayList<AdapterMoreMediaDialoge.MoreItem> titles) {
            this.titles = titles;
            return this;
        }

        public MoreMediaDialogFragment.Builder setOnImageSelectedListener(MoreMediaDialogFragment.OnImageSelectedListener onImageSelectedListener) {
            this.onImageSelectedListener = onImageSelectedListener;
            return this;
        }

        public MoreMediaDialogFragment create() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                    && ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                throw new RuntimeException("Missing required WRITE_EXTERNAL_STORAGE permission. Did you remember to request it first?");
            }

            if (onImageSelectedListener == null && onMultiImageSelectedListener == null) {
                throw new RuntimeException("You have to use setOnImageSelectedListener() or setOnMultiImageSelectedListener() for receive selected Uri");
            }

            MoreMediaDialogFragment customBottomSheetDialogFragment = new MoreMediaDialogFragment();


            customBottomSheetDialogFragment.builder = this;
            return customBottomSheetDialogFragment;
        }

        @Retention(RetentionPolicy.SOURCE)
        @IntDef({MoreMediaDialogFragment.Builder.MediaType.IMAGE, MoreMediaDialogFragment.Builder.MediaType.VIDEO})
        public @interface MediaType {
            int IMAGE = 1;
            int VIDEO = 2;
        }


    }

}
