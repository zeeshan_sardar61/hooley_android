package com.hooleyapp.hooley.fragments.event

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.Indicators.PagerIndicator
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.DefaultSliderView
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventDetailModel
import com.hooleyapp.hooley.databinding.FragmentCancelledEventBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.load
import java.util.*

class CancelledEventFragment : BaseFragment() {

    lateinit var bindind: FragmentCancelledEventBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        bindind = DataBindingUtil.inflate(inflater, R.layout.fragment_cancelled_event, container, false)
        return bindind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSlider()
        if (isPaid) {
            bindind.cvNote.visibility = View.VISIBLE
        } else {
            bindind.cvNote.visibility = View.GONE
        }

        bindind.tvReason1.text = cancelReason
        bindind.tvHostName.text = "Hosted by - $userName"
        bindind.ivAvatar.load(userAvatar)
        bindind.tvEventName.text = eventName
    }

    private fun initSlider() {
        bindind.EvenCoverSlider.stopAutoCycle()
        val urlMaps = HashMap<String, String>()
        for (loop in eventCoversList.indices) {
            urlMaps["" + loop] = eventCoversList[loop].photoUrl!!
        }
        for (name in urlMaps.keys) {
            val textSliderView = DefaultSliderView(HooleyMain.activity)
            textSliderView
                    .description("")
                    .image(urlMaps[name]).scaleType = BaseSliderView.ScaleType.CenterCrop
            textSliderView.bundle(Bundle())
            textSliderView.bundle.putString("extra", name)
            bindind.EvenCoverSlider.addSlider(textSliderView)
        }

        val handler = Handler()
        handler.postDelayed({
            bindind.EvenCoverSlider.startAutoCycle()
            bindind.EvenCoverSlider.indicatorVisibility = PagerIndicator.IndicatorVisibility.Invisible
            bindind.EvenCoverSlider.setPresetTransformer(SliderLayout.Transformer.Fade)
            bindind.EvenCoverSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom)
            bindind.EvenCoverSlider.setCustomAnimation(DescriptionAnimation())
            bindind.EvenCoverSlider.setDuration(3000)
        }, 1500)

    }

    companion object {
        lateinit var instance: CancelledEventFragment
        var eventCoversList = ArrayList<EventDetailModel.EventCover>()
        var cancelReason: String = ""
        var eventName: String = ""
        var userAvatar: String = ""
        var isPaid: Boolean = false
        var userName: String = ""

        fun newInstance(eventName_: String, usrName: String, userAvatar_: String, cancelReason_: String, eventCoversList_: ArrayList<EventDetailModel.EventCover>, isPaid_: Boolean): CancelledEventFragment {
            instance = CancelledEventFragment()
            userName = usrName
            cancelReason = cancelReason_
            eventCoversList = eventCoversList_
            eventName = eventName_
            userAvatar = userAvatar_
            isPaid = isPaid_
            return instance
        }
    }
}