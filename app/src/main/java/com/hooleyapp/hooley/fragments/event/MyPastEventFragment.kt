package com.hooleyapp.hooley.fragments.event

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterPastEvent
import com.hooleyapp.hooley.databinding.FragmentOtherEventBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.PastGuestListModel
import com.hooleyapp.hooley.model.event.PastEventsItem
import com.hooleyapp.hooley.model.event.PastEventsListModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.services.InviteWebService
import java.util.*


/**
 * Created by Nauman on 4/23/2018.
 */

class MyPastEventFragment : BaseFragment(), AdapterPastEvent.IMyEventsClickListeners, SwipeRefreshLayout.OnRefreshListener {

    internal var binding: FragmentOtherEventBinding? = null
    private var adapterPastEvent: AdapterPastEvent? = null
    private var list = ArrayList<PastEventsItem>()
    private val gson = Gson()
    private val myInviteEventModel: PastGuestListModel? = null
    internal var service = InviteWebService()
    internal var isSwiped = false

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {

        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            try {
                getPastGuestList()
            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            }

        } else {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_other_event, container, false)
        setListeners()
        return binding!!.root
    }

    private fun setListeners() {
        binding!!.slAttend.setOnRefreshListener(this)
    }


    private fun setRecyclerView() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        binding!!.rvOtherEvent.layoutAnimation = animation
        binding!!.rvOtherEvent.layoutManager = manager
        binding!!.rvOtherEvent.setEmptyView(binding!!.tvNoData)
        adapterPastEvent = AdapterPastEvent(HooleyMain.activity!!, list)
        adapterPastEvent!!.mListener = this
        binding!!.rvOtherEvent.adapter = adapterPastEvent
    }


    private fun getPastGuestList() {
        if (binding != null && isSwiped == false)
            binding!!.pbOther.visibility = View.VISIBLE

        service.getPastEventsList(object : GenericCallback<PastEventsListModel> {
            override fun success(result: PastEventsListModel) {
                if (isSwiped == true) {
                    binding!!.slAttend.isRefreshing = false
                    isSwiped = false
                }
                list = result.pastEvents!!
                binding!!.pbOther.visibility = View.GONE
                binding!!.tvNoData.visibility = View.GONE
                setRecyclerView()

            }

            override fun failure(message: String) {
                if (isSwiped == true) {
                    binding!!.slAttend.isRefreshing = false
                    isSwiped = false
                }
                binding!!.pbOther.visibility = View.GONE
                try {
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.rvOtherEvent.visibility = View.GONE
                    binding!!.tvNoData.text = message
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }

            }
        })

    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null) {
            binding!!.unbind()
        }
    }

    override fun onRefresh() {

        if (binding!!.slAttend != null) {
            binding!!.pbOther.visibility = View.GONE
            binding!!.slAttend.isRefreshing = true
        }
        isSwiped = true
        getPastGuestList()
    }

    override fun onClickItem(position: Int) {
        HooleyApp.db.putBoolean(Constants.IS_COMMENT_CLICKED, false)
        (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment
                .newInstance(list[position].eventId.toString()), "LiveEventFragment")
    }

}
