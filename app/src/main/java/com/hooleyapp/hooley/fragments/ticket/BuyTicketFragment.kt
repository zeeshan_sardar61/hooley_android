package com.hooleyapp.hooley.fragments.ticket

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.TextView
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentBuyTicketBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.others.HooleyWalletCardsFragment
import com.hooleyapp.hooley.fragments.others.HooleyWalletFragment
import com.hooleyapp.hooley.helper.addCommaInCurrency
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GetTicketByEventModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.TicketWebService

/**
 * Created by Zeeshan on 19-Mar-18.
 */


class BuyTicketFragment : BaseFragment() {
    internal lateinit var objectResponse: GetTicketByEventModel
    internal lateinit var binding: FragmentBuyTicketBinding
    private var totalTickets = 0
    private var eventTicketId: String? = null
    private var stripeTokken: String? = null
    var ticketWebService = TicketWebService()
    var isStartTicketSetup = true
    private val ticketService = TicketWebService()
    var clicked = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_buy_ticket, container, false)
        setListener()
        getTicketDetails()
        return binding.root
    }

    fun setToolbar(int: Int) {
        if (int == 1)
            (HooleyMain.activity!! as HooleyMain).setToolbarTitle("Buy Tickets & PPV")
        else
            (HooleyMain.activity!! as HooleyMain).setToolbarTitle("Buy Tickets")
    }

    private fun setListener() {}


    private fun setData() {
        Glide.with(HooleyMain.activity!!).load(objectResponse.eventBasicInfo.eventCover).into(binding.ivEventCover)
        if (!TextUtils.isEmpty(objectResponse.eventBasicInfo.profilePicture))
            Glide.with(HooleyMain.activity!!).load(objectResponse.eventBasicInfo.profilePicture).into(binding.ivAvatar)
        binding.tvEventHostName.text = "Hosted By - " + objectResponse.eventBasicInfo.hostname
        binding.tvEventName.text = objectResponse.eventBasicInfo.eventName
        addChild()
        if (objectResponse.payPerView != null) {
            binding.llPpv.visibility = View.VISIBLE
            setToolbar(1)
            if (!objectResponse.payPerView!!.isPpvAvailable) {
                binding.btnPpv.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_disable_btn)
                binding.btnPpv.isEnabled = false
            }
            binding.tvPpvAmount.text = "Ticket Amount $" + objectResponse.payPerView!!.ppvAmount.toString()
        }
        if (objectResponse.charity != null) {
            binding.llCharityView.visibility = View.VISIBLE
            setToolbar(0)
            binding.tvCharityTarget.text = "$" + HooleyMain.activity!!.addCommaInCurrency(objectResponse.charity!!.charityTarget)
            binding.tvCollected.text = "$" + HooleyMain.activity!!.addCommaInCurrency(objectResponse.charity!!.charitySoFar)

        }

        binding.btnDonate.setOnClickListener {
            if (binding.etDonateAmount.text.isNotEmpty()) {
                clicked = 0
                if (HooleyApp.db.getBoolean(Constants.IS_WALLET_SETUP)) {
                    hooleyWalletFragment = HooleyWalletCardsFragment(true, "Donate")
                    hooleyWalletFragment.setTargetFragment(BuyTicketFragment.instance, BUY_REQUEST_CODE)
                    (HooleyMain.activity as ActivityBase).addFragment(R.id.container, hooleyWalletFragment, "HooleyWalletCardsFragment")
                } else {
                    hooleyWallet = HooleyWalletFragment(false, true, "Donate")
                    hooleyWallet.setTargetFragment(BuyTicketFragment.instance, BUY_REQUEST_CODE)
                    callFragment(R.id.container, hooleyWallet, "HooleyWalletFragment")
                }
            } else {
                binding.etDonateAmount.requestFocus()
                HooleyMain.activity!!.showSuccessDialog("Please add the amount to donate")
            }
        }
        binding.btnPpv.setOnClickListener {
            clicked = 1
            if (HooleyApp.db.getBoolean(Constants.IS_WALLET_SETUP)) {
                hooleyWalletFragment = HooleyWalletCardsFragment(true, "Buy PPV")
                hooleyWalletFragment.setTargetFragment(BuyTicketFragment.instance, BUY_REQUEST_CODE)
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, hooleyWalletFragment, "HooleyWalletCardsFragment")
            } else {
                hooleyWallet = HooleyWalletFragment(false, true, "Buy PPV")
                hooleyWallet.setTargetFragment(BuyTicketFragment.instance, BUY_REQUEST_CODE)
                callFragment(R.id.container, hooleyWallet, "HooleyWalletFragment")
            }
        }

    }

    private lateinit var hooleyWalletFragment: HooleyWalletCardsFragment
    private lateinit var hooleyWallet: HooleyWalletFragment
    private val BUY_REQUEST_CODE: Int = 3000

    fun addChild() {
        if (objectResponse.ticketObj!!.ticketInfoList != null && objectResponse.ticketObj!!.ticketInfoList.size > 0) {
            binding.llGeneralTickets.visibility = View.VISIBLE

            for (i in objectResponse.ticketObj!!.ticketInfoList.indices) {
                val buyTicketCount = intArrayOf(1)
                val child = HooleyMain.activity!!.layoutInflater.inflate(R.layout.child_tickets_type, null)
                val value = objectResponse.ticketObj!!.ticketInfoList[i].price * objectResponse.ticketObj!!.ticketInfoList[i].sold
                (child.findViewById<View>(R.id.tvTicketCategoryName) as TextView).text = objectResponse.ticketObj!!.ticketInfoList[i].ticketName
                (child.findViewById<View>(R.id.tvAvailableTickets) as TextView).text = Integer.toString(objectResponse.ticketObj!!.ticketInfoList[i].available) + " Available Tickets"
                (child.findViewById<View>(R.id.tvUnitPrize) as TextView).text = "$" + Integer.toString(objectResponse.ticketObj!!.ticketInfoList[i].price)
                (child.findViewById<View>(R.id.tvTotalAmount) as TextView).text = "$" + Integer.toString(buyTicketCount[0] * objectResponse.ticketObj!!.ticketInfoList[i].price)
                (child.findViewById<View>(R.id.tvTicketsCount) as TextView).text = Integer.toString(buyTicketCount[0])

                child.findViewById<View>(R.id.ivExpandTicketCategory).setOnClickListener {
                    expand(child.findViewById(R.id.rlTicketCategoryDetail))
                    child.findViewById<View>(R.id.ivCollapseTicketCategory).visibility = View.VISIBLE
                    child.findViewById<View>(R.id.ivExpandTicketCategory).visibility = View.GONE
                }
                child.findViewById<View>(R.id.ivCollapseTicketCategory).setOnClickListener {
                    collapse(child.findViewById(R.id.rlTicketCategoryDetail))
                    child.findViewById<View>(R.id.ivCollapseTicketCategory).visibility = View.GONE
                    child.findViewById<View>(R.id.ivExpandTicketCategory).visibility = View.VISIBLE
                }

                child.findViewById<View>(R.id.ivMinusCount).setOnClickListener {
                    if (buyTicketCount[0] != 1) {
                        buyTicketCount[0] = buyTicketCount[0] - 1
                        (child.findViewById<View>(R.id.tvTotalAmount) as TextView).text = "$" + Integer.toString(buyTicketCount[0] * objectResponse.ticketObj!!.ticketInfoList[i].price)
                        (child.findViewById<View>(R.id.tvTicketsCount) as TextView).text = Integer.toString(buyTicketCount[0])
                    }
                }

                child.findViewById<View>(R.id.ivAddCount).setOnClickListener {
                    if (buyTicketCount[0] < objectResponse.ticketObj!!.ticketInfoList[i].available) {
                        buyTicketCount[0] = buyTicketCount[0] + 1
                        (child.findViewById<View>(R.id.tvTotalAmount) as TextView).text = "$" + Integer.toString(buyTicketCount[0] * objectResponse.ticketObj!!.ticketInfoList[i].price)
                        (child.findViewById<View>(R.id.tvTicketsCount) as TextView).text = Integer.toString(buyTicketCount[0])
                    }
                }

                child.findViewById<View>(R.id.btnBuyTicket).setOnClickListener {
                    clicked = 2
                    totalTickets = buyTicketCount[0]
                    eventTicketId = objectResponse.ticketObj!!.ticketInfoList[i].ticketId
                    if (totalTickets > 0 && totalTickets < objectResponse.ticketObj!!.ticketInfoList[i].available) {
                        if (HooleyApp.db.getBoolean(Constants.IS_WALLET_SETUP)) {
                            hooleyWalletFragment = HooleyWalletCardsFragment(true, "Buy Tickets")
                            hooleyWalletFragment.setTargetFragment(BuyTicketFragment.instance, BUY_REQUEST_CODE)
                            (HooleyMain.activity as ActivityBase).addFragment(R.id.container, hooleyWalletFragment, "HooleyWalletCardsFragment")
                        } else {
                            hooleyWallet = HooleyWalletFragment(false, true, "Buy Tickets")
                            hooleyWallet.setTargetFragment(BuyTicketFragment.instance, BUY_REQUEST_CODE)
                            callFragment(R.id.container, hooleyWallet, "HooleyWalletFragment")
                        }
                    } else
                        Util.showToastMessage(HooleyMain.activity!!, "Buy Ticket limit exceeded.")
                }
                binding.llTicketsCategories.addView(child)
            }
        } else {
            binding.llGeneralTickets.visibility = GONE
        }
    }

    private fun getTicketDetails() {
        HooleyMain.activity!!.showDialog()
        ticketService.getTicketByEvent(eventId!!, object : GenericCallback<GetTicketByEventModel> {
            override fun success(result: GetTicketByEventModel) {
                HooleyMain.activity!!.removeDialog()
                objectResponse = result
                setData()
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.removeDialog()
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun buyTicket() {
        HooleyMain.activity!!.showDialog()
        when (clicked) {
            0 -> {
                callApiBuyTickets(eventId!!, 0, stripeTokken!!, "0", "charity", binding.etDonateAmount.text.toString().toInt())

            }
            1 -> {
                callApiBuyTickets(eventId!!, 0, stripeTokken!!, "0", "ppv", 0)
            }
            2 -> {
                callApiBuyTickets(eventId!!, totalTickets, stripeTokken!!, eventTicketId!!, "general", 0)
            }

        }
    }

    private fun callApiBuyTickets(eventId: String, totalTickets: Int, stripeTokken: String, eventTicketId: String, ticketType: String, donateAmount: Int) {
        ticketWebService.buyTicket(eventId, totalTickets, stripeTokken, eventTicketId, ticketType, donateAmount, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, SuccessPaymentFragment.newInstance(eventTicketId, ticketType), "SuccessPaymentFragment")
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.removeDialog()
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }


    companion object {

        private lateinit var instance: BuyTicketFragment
        private var eventId: String? = null

        fun newInstance(id: String): BuyTicketFragment {
            eventId = id
            instance = BuyTicketFragment()
            return instance
        }

        fun expand(v: View) {
            v.measure(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
            val targetHeight = v.measuredHeight

            // Older versions of android (pre API 21) cancel animations for views with a height of 0.
            v.layoutParams.height = 1
            v.visibility = View.VISIBLE
            val a = object : Animation() {
                override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                    v.layoutParams.height = if (interpolatedTime == 1f)
                        WindowManager.LayoutParams.WRAP_CONTENT
                    else
                        (targetHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }

                override fun willChangeBounds(): Boolean {
                    return true
                }
            }

            // 1dp/ms
            a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong()
            v.startAnimation(a)
        }

        fun collapse(v: View) {
            val initialHeight = v.measuredHeight

            val a = object : Animation() {
                override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                    if (interpolatedTime == 1f) {
                        v.visibility = View.GONE
                    } else {
                        v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                        v.requestLayout()
                    }
                }

                override fun willChangeBounds(): Boolean {
                    return true
                }
            }

            // 1dp/ms
            a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong()
            v.startAnimation(a)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == BUY_REQUEST_CODE) {
                stripeTokken = data.extras.get("stripeToken").toString()
                buyTicket()
            }
        }
    }


}
