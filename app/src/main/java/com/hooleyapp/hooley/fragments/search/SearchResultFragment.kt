package com.hooleyapp.hooley.fragments.search

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterSearchEvent
import com.hooleyapp.hooley.databinding.FragmentSearchResultBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.hideKeyboard
import com.hooleyapp.hooley.helper.setToolBarTitle
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.INearByEventClickListener
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.NearByEventsFinalModel
import com.hooleyapp.hooley.model.SearchEventModel
import com.hooleyapp.hooley.model.staticData.Category
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.services.EventWebService
import java.util.*

/**
 * Created by Nauman on 4/5/2018.
 */

class SearchResultFragment : BaseFragment(), INearByEventClickListener {
    internal var mList: ArrayList<NearByEventsFinalModel>? = ArrayList()
    var binding: FragmentSearchResultBinding? = null
    private var objectResponse: SearchEventModel? = null
    private var adapter: AdapterSearchEvent? = null
    lateinit var nearByEvents: NearByEventsFinalModel
    private var position = 0
    private var eventService: EventWebService? = EventWebService()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_result, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchEvent()
        setToolBarTitle(HooleyMain.activity!!, "Search Results")

        binding!!.edtSearch.setText(searchString)

        binding!!.edtSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (!binding!!.edtSearch.text!!.isEmpty()) {
                    HooleyMain.activity!!.hideKeyboard()
                    searchString = binding!!.edtSearch.text.toString()
                    searchEvent()
                }

            }
            true
        }


        binding!!.ivClose.setOnClickListener {
            if (binding!!.edtSearch.text.toString().isNotEmpty()) {
                binding!!.edtSearch.setText("")
                searchString = binding!!.edtSearch.text.toString()
                searchEvent()
            }
        }

    }

    private fun searchEvent() {
        binding!!.pbSearchEvent.visibility = View.VISIBLE
        eventService!!.searchEvent(searchString, raduis, searchDate!!, catList!!, isPaid, isFree, object : IWebServiceCallback<SearchEventModel> {
            override fun success(result: SearchEventModel) {
                binding!!.pbSearchEvent.visibility = View.GONE
                objectResponse = result
                addAllDataInList()
                binding!!.tvNoData.visibility = View.GONE
            }

            override fun failure(message: String) {
                try {
                    binding!!.pbSearchEvent.visibility = View.GONE
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.rvSearchResult.visibility = View.GONE
                    binding!!.tvNoData.text = message
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    private fun addAllDataInList() {
        if (mList != null)
            mList!!.clear()
        if (objectResponse!!.featuredEventsList.size > 0)
            mList!!.add(NearByEventsFinalModel(Constants.TYPE_FEATURE_EVENT, objectResponse!!.featuredEventsList))
        mList!!.add(NearByEventsFinalModel(Constants.TYPE_MENU_EVENT, Constants.TYPE_UPCOMMING_EVENT))
        if (objectResponse!!.todayEventsList.size > 0)
            mList!!.add(NearByEventsFinalModel(Constants.TYPE_TODAY_EVENT, objectResponse!!.todayEventsList))
        if (objectResponse!!.nextWeekEventsList.size > 0)
            mList!!.add(NearByEventsFinalModel(Constants.TYPE_NEXT_WEEK_EVENT, objectResponse!!.nextWeekEventsList))
        if (objectResponse!!.commingEventsList.size > 0)
            mList!!.add(NearByEventsFinalModel(Constants.TYPE_COMMING_EVENT, objectResponse!!.commingEventsList))
        setRecylerView()
    }

    private fun setPastEvent() {
        if (mList != null)
            mList!!.clear()
        if (objectResponse!!.featuredEventsList.size > 0)
            mList!!.add(NearByEventsFinalModel(Constants.TYPE_FEATURE_EVENT, objectResponse!!.featuredEventsList))
        mList!!.add(NearByEventsFinalModel(Constants.TYPE_MENU_EVENT, Constants.TYPE_PAST_EVENT))
        if (objectResponse!!.pastEventsList.size > 0) {
            for (i in objectResponse!!.pastEventsList.indices) {
                mList!!.add(NearByEventsFinalModel(Constants.TYPE_PAST_EVENT, objectResponse!!.pastEventsList[i]))
            }
        } else {
            binding!!.tvNoData.visibility = View.VISIBLE
            binding!!.tvNoData.text = "No Past Events"
            return
        }

        setRecylerView()
    }

    private fun setRecylerView() {
        binding!!.rvSearchResult.visibility = View.VISIBLE
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding!!.rvSearchResult.layoutManager = manager
        binding!!.rvSearchResult.setEmptyView(binding!!.tvNoData)
        adapter = AdapterSearchEvent(HooleyMain.activity!!, mList!!)
        binding!!.rvSearchResult.adapter = adapter
        adapter!!.setListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onClickLike(nearByEventsFinalModel: NearByEventsFinalModel, pos: Int, mListPosition: Int) {
        nearByEvents = nearByEventsFinalModel
        position = pos
        try {
            if (nearByEvents.type == Constants.TYPE_PAST_EVENT) {
                likeEvent(nearByEvents.eventDetail.eventId, mListPosition)
            } else {
                likeEvent(nearByEvents.eventDetailList[pos].eventId, mListPosition)
            }
        } catch (ex: IndexOutOfBoundsException) {
            ex.printStackTrace()
        }

    }

    override fun onClickComment() {
        // unused
    }

    override fun onUpComingEvent() {
        addAllDataInList()
    }

    override fun onPastEvent() {
        setPastEvent()
    }

    override fun onClickShare(nearByEventsFinalModel: NearByEventsFinalModel, pos: Int) {
        nearByEvents = nearByEventsFinalModel
        position = pos
        showShareEventDialog()
    }

    override fun onClickFollow(nearByEventsFinalModel: NearByEventsFinalModel, pos: Int, listPosition: Int) {
        nearByEvents = nearByEventsFinalModel
        position = pos
        try {
            if (nearByEvents.type == Constants.TYPE_PAST_EVENT) {
                followEvent(nearByEvents.eventDetail.eventId, nearByEvents.eventDetail.isFollowed, listPosition)
            } else {
                followEvent(nearByEvents.eventDetailList[pos].eventId, nearByEvents.eventDetailList[pos].isFollowed, listPosition)
            }

        } catch (ex: IndexOutOfBoundsException) {
            ex.printStackTrace()
        }

    }

    private fun showShareEventDialog() {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        val eventName: String?
        val eventStartTime: String?
        val eventEndTime: String?
        val eventId: String?
        val eventAddress: String?
        if (nearByEvents.type == Constants.TYPE_PAST_EVENT) {
            eventName = nearByEvents.eventDetail.eventName
            eventStartTime = nearByEvents.eventDetail.startTime
            eventEndTime = nearByEvents.eventDetail.endTime
            eventId = nearByEvents.eventDetail.eventId
            eventAddress = nearByEvents.eventDetail.address
        } else {
            eventName = nearByEvents.eventDetailList[position].eventName
            eventStartTime = nearByEvents.eventDetailList[position].startTime
            eventEndTime = nearByEvents.eventDetailList[position].endTime
            eventId = nearByEvents.eventDetailList[position].eventId
            eventAddress = nearByEvents.eventDetailList[position].address
        }


        shareIntent.putExtra(Intent.EXTRA_TEXT, "Event Name = " + eventName
                + "\nEvent Date&Time =  " + DateUtils.displayDateOnly(DateUtils.getLocalDate(eventStartTime!!))
                + "\nEvent Location = " + eventAddress
                + "\nEvent " + Constants.INVITE_EVENT_URL + eventId)
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(Intent.createChooser(shareIntent, "Share Event"))
    }

    private fun shareEvent(shareMedia: String) {

        if (eventService == null)
            eventService = EventWebService()
        eventService!!.shareEvent(nearByEvents.eventDetailList[position].eventId!!, shareMedia, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {

//                Util.showToastMessage(HooleyMain.activity!!, "Share Successfully")
                HooleyMain.activity!!.showSuccessDialog("Share Successfully")

                when (nearByEvents.type) {
                    Constants.TYPE_FEATURE_EVENT -> {

                        mList!![0].eventDetailList[position].sharesCount = mList!![0].eventDetailList[position].sharesCount + 1
                        adapter!!.notifyDataSetChanged()
                    }
                    Constants.TYPE_TODAY_EVENT -> {

                        mList!![1].eventDetailList[position].sharesCount = mList!![1].eventDetailList[position].sharesCount + 1
                        adapter!!.notifyDataSetChanged()
                    }
                    Constants.TYPE_NEXT_WEEK_EVENT -> {

                        mList!![2].eventDetailList[position].sharesCount = mList!![2].eventDetailList[position].sharesCount + 1
                        adapter!!.notifyDataSetChanged()
                    }
                    Constants.TYPE_COMMING_EVENT -> {

                        mList!![2].eventDetailList[position].sharesCount = mList!![3].eventDetailList[position].sharesCount + 1
                        adapter!!.notifyDataSetChanged()
                    }
                }
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)

                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == 7) {
                shareEvent("")
            }
        }
    }

    private fun followEvent(eventId: String?, isFollow: Boolean, listPosition: Int) {
        if (binding != null)
            if (eventService == null)
                eventService = EventWebService()
        eventService!!.followEvent(eventId!!, !isFollow, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                try {

                    when (nearByEvents.type) {
                        Constants.TYPE_FEATURE_EVENT -> if (mList!![listPosition].eventDetailList[position].isFollowed) {
                            mList!![listPosition].eventDetailList[position].isFollowed = false
                            mList!![listPosition].eventDetailList[position].followersCount = mList!![listPosition].eventDetailList[position].followersCount - 1
                        } else {
                            mList!![listPosition].eventDetailList[position].isFollowed = true
                            mList!![listPosition].eventDetailList[position].followersCount = mList!![listPosition].eventDetailList[position].followersCount + 1
                        }
                        Constants.TYPE_TODAY_EVENT ->

                            if (mList!![listPosition].eventDetailList[position].isFollowed) {
                                mList!![listPosition].eventDetailList[position].isFollowed = false
                                mList!![listPosition].eventDetailList[position].followersCount = mList!![listPosition].eventDetailList[position].followersCount - 1
                            } else {
                                mList!![listPosition].eventDetailList[position].isFollowed = true
                                mList!![listPosition].eventDetailList[position].followersCount = mList!![listPosition].eventDetailList[position].followersCount + 1
                            }
                        Constants.TYPE_NEXT_WEEK_EVENT ->

                            if (mList!![listPosition].eventDetailList[position].isFollowed) {
                                mList!![listPosition].eventDetailList[position].isFollowed = false
                                mList!![listPosition].eventDetailList[position].followersCount = mList!![listPosition].eventDetailList[position].followersCount - 1
                            } else {
                                mList!![listPosition].eventDetailList[position].isFollowed = true
                                mList!![listPosition].eventDetailList[position].followersCount = mList!![listPosition].eventDetailList[position].followersCount + 1
                            }
                        Constants.TYPE_COMMING_EVENT ->

                            if (mList!![listPosition].eventDetailList[position].isFollowed) {
                                mList!![listPosition].eventDetailList[position].isFollowed = false
                                mList!![listPosition].eventDetailList[position].followersCount = mList!![listPosition].eventDetailList[position].followersCount - 1
                            } else {
                                mList!![listPosition].eventDetailList[position].isFollowed = true
                                mList!![listPosition].eventDetailList[position].followersCount = mList!![listPosition].eventDetailList[position].followersCount + 1
                            }
                        Constants.TYPE_PAST_EVENT -> if (mList!![listPosition].eventDetail.isFollowed) {
                            mList!![listPosition].eventDetail.isFollowed = false
                            mList!![listPosition].eventDetail.followersCount = mList!![listPosition].eventDetail.followersCount - 1
                        } else {
                            mList!![listPosition].eventDetail.isFollowed = true
                            mList!![listPosition].eventDetail.followersCount = mList!![listPosition].eventDetail.followersCount + 1
                        }
                    }
                } catch (ex: IndexOutOfBoundsException) {
                    ex.printStackTrace()
                }

            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })

    }

    private fun likeEvent(eventId: String?, listPosition: Int) {
        if (binding != null)
        //userSearchbinding.pbNearBy.setVisibility(View.VISIBLE);
            if (eventService == null)
                eventService = EventWebService()
        eventService!!.likeEvent(eventId!!, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                // userSearchbinding.pbNearBy.setVisibility(View.GONE);
                try {

                    when (nearByEvents.type) {
                        Constants.TYPE_FEATURE_EVENT -> if (mList!![listPosition].eventDetailList[position].isLiked) {
                            mList!![listPosition].eventDetailList[position].isLiked = false
                            mList!![listPosition].eventDetailList[position].likesCount = mList!![listPosition].eventDetailList[position].likesCount - 1
                        } else {
                            mList!![listPosition].eventDetailList[position].isLiked = true
                            mList!![listPosition].eventDetailList[position].likesCount = mList!![listPosition].eventDetailList[position].likesCount + 1
                        }
                        Constants.TYPE_TODAY_EVENT ->

                            if (mList!![listPosition].eventDetailList[position].isLiked) {
                                mList!![listPosition].eventDetailList[position].isLiked = false
                                mList!![listPosition].eventDetailList[position].likesCount = mList!![listPosition].eventDetailList[position].likesCount - 1
                            } else {
                                mList!![listPosition].eventDetailList[position].isLiked = true
                                mList!![listPosition].eventDetailList[position].likesCount = mList!![listPosition].eventDetailList[position].likesCount + 1
                            }
                        Constants.TYPE_NEXT_WEEK_EVENT ->

                            if (mList!![listPosition].eventDetailList[position].isLiked) {
                                mList!![listPosition].eventDetailList[position].isLiked = false
                                mList!![listPosition].eventDetailList[position].likesCount = mList!![listPosition].eventDetailList[position].likesCount - 1
                            } else {
                                mList!![listPosition].eventDetailList[position].isLiked = true
                                mList!![listPosition].eventDetailList[position].likesCount = mList!![listPosition].eventDetailList[position].likesCount + 1
                            }
                        Constants.TYPE_COMMING_EVENT ->

                            if (mList!![listPosition].eventDetailList[position].isLiked) {
                                mList!![listPosition].eventDetailList[position].isLiked = false
                                mList!![listPosition].eventDetailList[position].likesCount = mList!![listPosition].eventDetailList[position].likesCount - 1
                            } else {
                                mList!![listPosition].eventDetailList[position].isLiked = true
                                mList!![listPosition].eventDetailList[position].likesCount = mList!![listPosition].eventDetailList[position].likesCount + 1
                            }
                        Constants.TYPE_PAST_EVENT -> if (mList!![listPosition].eventDetail.isLiked) {
                            mList!![listPosition].eventDetail.isLiked = false
                            mList!![listPosition].eventDetail.likesCount = mList!![listPosition].eventDetail.likesCount - 1
                        } else {
                            mList!![listPosition].eventDetail.isLiked = true
                            mList!![listPosition].eventDetail.likesCount = mList!![listPosition].eventDetail.likesCount + 1
                        }
                    }
                } catch (ex: IndexOutOfBoundsException) {
                    ex.printStackTrace()
                }

            }

            override fun failure(message: String) {
                //userSearchbinding.pbNearBy.setVisibility(View.GONE);
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })

    }

    companion object {

        lateinit var instance: SearchResultFragment
        private var raduis: Double = 0.toDouble()
        private var searchString = ""
        private var searchDate: String? = null
        private var catList: ArrayList<Category>? = null
        private var isPaid = false
        private var isFree = false

        fun newInstance(miles: Double, paid: Boolean, free: Boolean, searchText: String, date: String, list: ArrayList<Category>): SearchResultFragment {
            raduis = miles
            isPaid = paid
            isFree = free
            searchString = searchText
            searchDate = date
            catList = list

            instance = SearchResultFragment()
            return instance
        }
    }

}
