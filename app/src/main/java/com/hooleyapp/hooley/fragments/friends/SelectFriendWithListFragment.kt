package com.hooleyapp.hooley.fragments.friends

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterSelectFriendsWithList
import com.hooleyapp.hooley.databinding.FragmentSelectFriendWithListBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.tables.FriendsTable

@SuppressLint("ValidFragment")
class SelectFriendWithListFragment constructor(var friendList: ArrayList<FriendsTable>) : BaseFragment(), TextWatcher, AdapterSelectFriendsWithList.CheckClickListener {

    lateinit var binding: FragmentSelectFriendWithListBinding
    private lateinit var adapter: AdapterSelectFriendsWithList
    private lateinit var intent: Intent
    private var mMainList = ArrayList<FriendsTable>()
    private var addedCount = 0

    init {
        if (mMainList.size > 0)
            mMainList.clear()
        mMainList.addAll(friendList)
        setToolBarTitle()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_friend_with_list, container, false)
        if (checkSelectAll()) {
            binding.tbSelectAll.isChecked = true
        }
        setHasOptionsMenu(true)
        setAdapter()
        setListener()
        return binding.root
    }

    fun checkSelectAll(): Boolean {
        addedCount = 0
        for (i in mMainList.indices) {
            if (mMainList[i].isChecked) {
                addedCount++
            }
        }
        if (addedCount == mMainList.size) {
            return true
        }
        return false
    }

    override fun checkClicked(position: Int) {
        mMainList[position].isChecked = !mMainList[position].isChecked
        binding.tbSelectAll.isChecked = checkSelectAll()
        adapter.notifyDataSetChanged()
    }

    private fun setToolBarTitle() {
        var isEdited = false
        for (i in 0 until mMainList.size) {
            if (mMainList[i].isChecked) {
                isEdited = true
                break
            }
        }
        if (isEdited)
            (HooleyMain.activity as HooleyMain).setToolbarTitle("Edit Tags")
        else
            (HooleyMain.activity as HooleyMain).setToolbarTitle("Tag Friends")
    }


    private fun setListener() {
        binding.edtSearch.addTextChangedListener(this)
        binding.tbSelectAll.setOnClickListener { selectAll(binding.tbSelectAll.isChecked) }
        binding.btnSelectFriend.setOnClickListener {
            intent = Intent()
            friendList = mMainList
            intent.putParcelableArrayListExtra("list", friendList)
            HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
            targetFragment!!.onActivityResult(
                    targetRequestCode,
                    Activity.RESULT_OK,
                    intent
            )
        }

        binding.ivClose.setOnClickListener {
            binding.edtSearch.setText("")
            Util.hideKeyboard(HooleyMain.activity!!)
        }
    }

    private fun selectAll(check: Boolean) {
        if (mMainList.size > 0) {
            for (i in mMainList.indices) {
                mMainList[i].isChecked = check
                if (check)
                    addedCount++
                else
                    addedCount--
            }
        }
        adapter.notifyDataSetChanged()
    }


    private fun setAdapter() {
//        mMainList.let {
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding.rvSelectFriend.layoutManager = manager
        adapter = AdapterSelectFriendsWithList(HooleyMain.activity!!, mMainList)
        adapter.mListener = this
        binding.rvSelectFriend.adapter = adapter
        binding.tvPeopleCount.text = if (mMainList.size == 1) {
            "${mMainList.size} Person "
        } else {
            "${mMainList.size} People"
        }
//        }.also {
        if (mMainList.isEmpty()) {
            binding.rvSelectFriend.visibility = View.GONE
            binding.rlSearchBar.visibility = View.GONE
            binding.view.visibility = View.GONE
            binding.btnDone.visibility = View.GONE
            binding.tvNoData.visibility = View.VISIBLE
            binding.tvNoData.text = "No friends"
        }
//        }
    }


    override fun afterTextChanged(s: Editable?) {
        if (s!!.isNotEmpty()) {
            mMainList.clear()
            if (friendList.size > 0) {
                for (i in friendList.indices) {
                    if (friendList[i].fullName.toLowerCase().contains(s.toString().toLowerCase())) {
                        mMainList.add(friendList[i])
                    }
                }
            }
            if (mMainList.size == 0) {
                binding.tvNoData.visibility = View.VISIBLE
                binding.tvNoData.text = "No Record Found"
            } else {
                binding.tvNoData.visibility = View.GONE
            }

            if (::adapter.isInitialized)
                adapter.notifyDataSetChanged()
        } else {
            binding.tvNoData.visibility = View.GONE
            mMainList.clear()
            mMainList.addAll(friendList)
            if (::adapter.isInitialized)
                adapter.notifyDataSetChanged()
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
        inflater!!.inflate(R.menu.live_post_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.share -> {
                intent = Intent()
                friendList = mMainList
                intent.putParcelableArrayListExtra("list", friendList)
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                targetFragment!!.onActivityResult(
                        targetRequestCode,
                        Activity.RESULT_OK,
                        intent
                )
                return super.onOptionsItemSelected(item)
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
    }

}

