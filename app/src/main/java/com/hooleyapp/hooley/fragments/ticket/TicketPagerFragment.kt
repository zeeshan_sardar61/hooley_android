package com.hooleyapp.hooley.fragments.ticket

import android.app.DialogFragment
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterGiftedPager
import com.hooleyapp.hooley.adapters.AdapterTicketPager
import com.hooleyapp.hooley.databinding.FragmentTicketSliderBinding
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.MyTicketModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.services.TicketWebService
import java.util.*

class TicketPagerFragment : DialogFragment(), AdapterTicketPager.TicketClickListener, AdapterGiftedPager.GiftTicketClickListener {


    var binding: FragmentTicketSliderBinding? = null
    private var itemSelectedPosition = -1
    private val ticketService = TicketWebService()
    var adapterTicketPager: AdapterTicketPager? = null
    private var adapterGiftedPager: AdapterGiftedPager? = null
    lateinit var mListener: GiftClickListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ticket_slider, container, false)
        (HooleyMain.activity as HooleyMain).removeBubbleView()
//        HooleyMain.activity!!.supportActionBar!!.hide()
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setAdapter()
        return binding!!.root
    }

    private fun setAdapter() {
        if (type == 1) {
            if (ticketObj.isGifted!!) {
                for (i in mlist!!.indices) {
                    mlist!![i].isGifted = true
                }
            }
            adapterTicketPager = AdapterTicketPager(HooleyMain.activity!!, mlist!!, ticketObj)
            binding!!.vpTicket.adapter = adapterTicketPager
            binding!!.tabTicket.setupWithViewPager(binding!!.vpTicket, true)
            adapterTicketPager!!.setListener(this)
        } else {
            adapterGiftedPager = AdapterGiftedPager(HooleyMain.activity!!, mlist!!, ticketObj)
            binding!!.vpTicket.adapter = adapterGiftedPager
            binding!!.tabTicket.setupWithViewPager(binding!!.vpTicket, true)
            adapterGiftedPager!!.mGiftListener = this
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onClickGiftTicket(position: Int, item: MyTicketModel.MyTicketsList) {
        dialog.dismiss()
        itemSelectedPosition = position
        HooleyApp.db.putString(Constants.CALL_TYPE, "GIFT_FRIEND")
        mListener.onClickTicketRefund(itemSelectedPosition, item)
    }

    override fun onClickClose() {
        dialog.dismiss()
    }

    override fun onGiftClickClose() {
        dialog.dismiss()
    }

    override fun onClickNavigate() {
        dialog.dismiss()
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + ticketObj.latitude + "," + ticketObj.longitude))
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        HooleyMain.activity!!.startActivity(intent)
    }

    override fun onClickTicketRefund(position: Int, item: MyTicketModel.TicketList) {
        HooleyMain.activity!!.YesNoDialog("", "Are you sure you want to refund?", null, object : onClickDialog {
            override fun onClickYes() {
                HooleyMain.activity!!.showDialog()
                ticketService.refundTicket(ticketObj.eventId.toString(), item.id, object : GenericCallback<GeneralModel> {
                    override fun success(result: GeneralModel) {
                        HooleyMain.activity!!.removeDialog()
                        ticketObj.ticketList.remove(item)
                        adapterTicketPager!!.notifyDataSetChanged()
                    }

                    override fun failure(message: String) {
                        HooleyMain.activity!!.removeDialog()
                        HooleyMain.activity!!.showSuccessDialog(message)

                    }
                })
            }
        })
    }

    companion object {
        lateinit var instance: TicketPagerFragment
        private lateinit var ticketObj: MyTicketModel.MyTicketsList
        private var mlist: ArrayList<MyTicketModel.TicketList>? = null
        private var type: Int = 0

        fun newInstance(ticket: MyTicketModel.MyTicketsList, list: ArrayList<MyTicketModel.TicketList>, t: Int): TicketPagerFragment {
            mlist = list
            ticketObj = ticket
            type = t
            instance = TicketPagerFragment()
            return instance
        }
    }

    interface GiftClickListener {

        fun onClickTicketRefund(position: Int, item: MyTicketModel.MyTicketsList)

    }


}
