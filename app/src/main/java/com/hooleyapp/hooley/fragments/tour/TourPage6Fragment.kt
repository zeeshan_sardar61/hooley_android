package com.hooleyapp.hooley.fragments.tour

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.HooleyApp

import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyAuth
import com.hooleyapp.hooley.activites.Splash
import com.hooleyapp.hooley.app.data.model.guestuser.GuestUserLoginModel
import com.hooleyapp.hooley.app.view.ui.activites.HooleyGuestUser
import com.hooleyapp.hooley.databinding.FragmentPagerItemBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.services.GuestUserWebService

/**
 * Created by Nauman on 11/28/2017.
 */

class TourPage6Fragment : BaseFragment(), View.OnClickListener {

    internal var binding: FragmentPagerItemBinding? = null
    private var intent: Intent? = null
    var guestUserService = GuestUserWebService()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pager_item, container, false)
        setListener()
        return binding!!.root
    }

    private fun setListener() {
        binding!!.btnGetStarted.setOnClickListener(this)
        binding!!.btnLoginScreen.setOnClickListener(this)
        binding!!.btnLoginAsGuest.setOnClickListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onClick(v: View) {
        when (v.id) {
            //                userSearchbinding.vpTour.setCurrentItem(userSearchbinding.vpTour.getCurrentItem() + 1, true);
            R.id.btnGetStarted -> {
                intent = Intent(activity, HooleyAuth::class.java)
                intent!!.putExtra("Type", "Register")
                startActivity(intent)
            }
            R.id.btnLoginScreen -> {
                intent = Intent(activity, HooleyAuth::class.java)
                intent!!.putExtra("Type", "Login")
                startActivity(intent)
            }
            R.id.btnLoginAsGuest -> {
                loginAsGuest()

            }
        }
    }

    private fun loginAsGuest() {
        Splash.activity.showDialog()
        guestUserService.guestUserLogin(object : IWebServiceCallback<GuestUserLoginModel> {
            override fun success(result: GuestUserLoginModel) {
                Splash.activity.removeDialog()
                HooleyApp.db.putString(Constants.USER_ID, result.guestUserInfo.guestId!!)
                HooleyApp.db.putBoolean(Constants.IS_LOGIN, false)
                HooleyApp.db.putBoolean(Constants.IS_GUEST_LOGIN, true)
                HooleyApp.db.putString(Constants.AUTH_TOKKEN, result.guestUserInfo.guestToken!!)

                intent = Intent(activity, HooleyGuestUser::class.java)
                startActivity(intent)
            }

            override fun failure(message: String) {
                Splash.activity.removeDialog()

            }

            override fun onTokenExpire() {
            }
        })
    }

    companion object {

        var instance: TourPage6Fragment? = null
    }
}
