package com.hooleyapp.hooley.fragments.profile

import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentBusinessProfileBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GetUserProfilesModel
import com.hooleyapp.hooley.others.AppAnimationUtil
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.services.ProfileWebService

/**
 * Created by Nauman on 12/13/2017.
 */

class BusinessProfileFragment : BaseFragment(), View.OnClickListener {


    var binding: FragmentBusinessProfileBinding? = null
    var responseObject: GetUserProfilesModel? = null
    private var businessImageUrl: String? = null
    internal var webService = ProfileWebService()
    private var stripeDialog: Dialog? = null

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {

        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            try {
                getBusinessProfile()
            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            }

        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_business_profile, container, false)
        setListener()
        getBusinessProfile()
        return binding!!.root
    }

    private fun setListener() {
        binding!!.btnEditProfile.setOnClickListener(this)
        binding!!.btnCreateBusinessProfile.setOnClickListener(this)
        binding!!.ivFaceBook.setOnClickListener(this)
        binding!!.ivTwitter.setOnClickListener(this)
        binding!!.ivInstagram.setOnClickListener(this)
        binding!!.ivLinkedIn.setOnClickListener(this)
        binding!!.ivFlicker.setOnClickListener(this)
        binding!!.btnEditPrfle.setOnClickListener(this)
    }


    fun onSocialClick(type: String) {
        if (responseObject != null) {
            if (responseObject!!.businessProfile!!.socialMediaArrayList.size > 0) {
                for (i in responseObject!!.businessProfile!!.socialMediaArrayList.indices) {
                    if (responseObject!!.businessProfile!!.socialMediaArrayList[i].socialName.equals(type, ignoreCase = true)) {
                        if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl)) {
                            try {
                                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl)))
                            } catch (ex: ActivityNotFoundException) {
                                ex.printStackTrace()
                            }

                        }
                    }
                }
            }
        }
    }

    private fun getBusinessProfile() {
        if (binding != null)
            if (HooleyApp.db.getBoolean(Constants.IS_BUSINESS_PROFILE_CREATED))
                binding!!.pbBusinessProfile.visibility = View.VISIBLE

        webService.getUserProfile(object : GenericCallback<GetUserProfilesModel> {
            override fun success(result: GetUserProfilesModel) {
                responseObject = result
                if (result.businessProfile != null && result.businessProfile!!.businessInfo.businessName != "") {
                    binding!!.rlCreateBusinessProfile.visibility = View.GONE
                    binding!!.rlMainContainer.visibility = View.VISIBLE
                    binding!!.pbBusinessProfile.visibility = View.VISIBLE
                    AppAnimationUtil.expandView(binding!!.scroll)
                    setData()
                    binding!!.pbBusinessProfile.visibility = View.GONE
                } else {
                    binding!!.rlCreateBusinessProfile.visibility = View.VISIBLE
                    binding!!.rlMainContainer.visibility = View.GONE
                    binding!!.pbBusinessProfile.visibility = View.GONE

                }
            }

            override fun failure(message: String) {
                binding!!.pbBusinessProfile.visibility = View.GONE
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })


    }

    private fun setData() {
        if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.businessName))
            binding!!.tvBusinessName.text = responseObject!!.businessProfile!!.businessInfo.businessName
        if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.aboutBusiness))
            binding!!.tvAboutBusiness.text = responseObject!!.businessProfile!!.businessInfo.aboutBusiness
        if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.businessEmail))
            binding!!.tvBusinessEmail.text = responseObject!!.businessProfile!!.businessInfo.businessEmail
        if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.website))
            binding!!.tvBusinessWebsite.text = responseObject!!.businessProfile!!.businessInfo.website
        if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.businessPhone))
            binding!!.tvBusinessPhone.text = responseObject!!.businessProfile!!.businessInfo.countryCode + responseObject!!.businessProfile!!.businessInfo.businessPhone
        if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.address))
            binding!!.tvBusinessAddress.text = responseObject!!.businessProfile!!.businessInfo.address
        if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.zipCode))
            binding!!.tvZipCode.text = responseObject!!.businessProfile!!.businessInfo.zipCode
        if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.contactName))
            binding!!.tvBusinessContactName.text = responseObject!!.businessProfile!!.businessInfo.contactName
        if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.businessProfilePicture)) {
            Glide.with(HooleyMain.activity!!).load(responseObject!!.businessProfile!!.businessInfo.businessProfilePicture).into(binding!!.ivAvatar)
            businessImageUrl = responseObject!!.businessProfile!!.businessInfo.businessProfilePicture
        }
        if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.country))
            binding!!.tvCountry.text = responseObject!!.businessProfile!!.businessInfo.country
        if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.state))
            binding!!.tvState.text = responseObject!!.businessProfile!!.businessInfo.state
        if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.businessInfo.city))
            binding!!.tvCity.text = responseObject!!.businessProfile!!.businessInfo.city


        if (responseObject!!.businessProfile!!.businessInfo.isPhoneVerified) {
            binding!!.btnVerified.visibility = View.VISIBLE
            binding!!.btnNotVerified.visibility = View.GONE
        } else {
            binding!!.btnVerified.visibility = View.GONE
            binding!!.btnNotVerified.visibility = View.VISIBLE
        }
        setIndustry()
        setCompanyType()
        setBusinessSocial()
    }

    private fun setBusinessSocial() {
        if (responseObject!!.businessProfile!!.socialMediaArrayList.size > 0) {
            for (i in responseObject!!.businessProfile!!.socialMediaArrayList.indices) {
                when (responseObject!!.businessProfile!!.socialMediaArrayList[i].socialName) {
                    Constants.SOCIAL_FB.toLowerCase() -> if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl)) {
                        binding!!.ivFaceBook.setBackgroundResource(R.drawable.ic_socail_fb)
                    } else {
                        binding!!.ivFaceBook.setBackgroundResource(R.drawable.ic_social_fb_dissconnect)
                    }
                    Constants.SOCIAL_TW.toLowerCase() -> if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl)) {
                        binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_socail_twitter)
                    } else {
                        binding!!.ivTwitter.setBackgroundResource(R.drawable.ic_social_twitter_dissconnect)
                    }
                    Constants.SOCIAL_LI.toLowerCase() -> if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl)) {
                        binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_socail_linkedin)
                    } else {
                        binding!!.ivLinkedIn.setBackgroundResource(R.drawable.ic_social_linkedin_dissconnected)
                    }
                    Constants.SOCIAL_INSTA.toLowerCase() -> if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl)) {
                        binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram)
                    } else {
                        binding!!.ivInstagram.setBackgroundResource(R.drawable.ic_socail_instagram_dissconnect)
                    }
                    Constants.SOCIAL_FLKR.toLowerCase() -> if (!TextUtils.isEmpty(responseObject!!.businessProfile!!.socialMediaArrayList[i].socialUrl)) {
                        binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_socail_filker)
                    } else {
                        binding!!.ivFlicker.setBackgroundResource(R.drawable.ic_social_filker_dissconnect)
                    }
                }
            }
        }
    }


    private fun setCompanyType() {
        if (!responseObject!!.businessProfile!!.businessInfo.companyTypeId.equals("0", ignoreCase = true)) {
            for (i in responseObject!!.businessProfile!!.companyTypeList.indices) {
                if (responseObject!!.businessProfile!!.companyTypeList[i].id.equals(responseObject!!.businessProfile!!.businessInfo.companyTypeId, ignoreCase = true)) {
                    binding!!.tvCompanyType.text = responseObject!!.businessProfile!!.companyTypeList[i].name
                }
            }
        }
    }

    private fun setIndustry() {
        if (!responseObject!!.businessProfile!!.businessInfo.industryId.equals("0", ignoreCase = true)) {
            for (i in responseObject!!.businessProfile!!.industryList.indices) {
                if (responseObject!!.businessProfile!!.industryList[i].id.equals(responseObject!!.businessProfile!!.businessInfo.industryId, ignoreCase = true)) {
                    binding!!.tvIndustry.text = responseObject!!.businessProfile!!.industryList[i].name
                }
            }
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnEditPrfle, R.id.btnEditProfile -> (HooleyMain.activity as ActivityBase).callFragment(R.id.container, EditBusinessProfileFragment.newInstance(responseObject!!), "EditPersonalProfileFragment")
            R.id.btnCreateBusinessProfile -> (HooleyMain.activity as ActivityBase).callFragment(R.id.container, EditBusinessProfileFragment.newInstance(responseObject!!), "EditPersonalProfileFragment")
            R.id.ivFaceBook -> onSocialClick(Constants.SOCIAL_FB)
            R.id.ivTwitter -> onSocialClick(Constants.SOCIAL_TW)
            R.id.ivLinkedIn -> onSocialClick(Constants.SOCIAL_LI)
            R.id.ivInstagram -> onSocialClick(Constants.SOCIAL_INSTA)
            R.id.ivFilter -> onSocialClick(Constants.SOCIAL_FLKR)
        }
    }

}
