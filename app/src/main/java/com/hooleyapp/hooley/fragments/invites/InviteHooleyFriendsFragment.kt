package com.hooleyapp.hooley.fragments.invites

import android.annotation.SuppressLint
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterInviteHooleyFriends
import com.hooleyapp.hooley.databinding.FragmentInviteHooleyFriendsBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.checkIfName
import com.hooleyapp.hooley.helper.hideKeyboard
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.FriendsModel
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.friends.HooleyUserModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.services.InviteWebService
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Nauman on 1/5/2018.
 */
@SuppressLint("ValidFragment")
class InviteHooleyFriendsFragment constructor(var createdEventId: String, var createdEventName: String) : BaseFragment(), View.OnClickListener, TextWatcher, AdapterInviteHooleyFriends.IInviteHooleyFriendClickListener {
    internal var binding: FragmentInviteHooleyFriendsBinding? = null
    private var friendList = ArrayList<FriendsTable>()
    private var adapterInviteHooleyFriends: AdapterInviteHooleyFriends? = null
    internal var gson = Gson()
    private lateinit var objectResponseMyFriends: FriendsModel
    private lateinit var objectResponse: HooleyUserModel
    private var hooleyFriendList = ArrayList<FriendsTable>()
    private var hooleyUserList = ArrayList<FriendsTable>()
    private var selectedUsers = ArrayList<FriendsTable>()
    var service = FriendWebService()
    var mService = InviteWebService()
    var startTime = System.currentTimeMillis()
    val COPY_THRESHOLD = 600

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_invite_hooley_friends, container, false)
        getHooleyFriendsList()
        binding!!.edtSearch.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                HooleyMain.activity!!.hideKeyboard()
                true
            } else {
                false
            }
        }
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
    }


    fun setListener() {
        binding!!.btnSendInvite.setOnClickListener(this)
        binding!!.edtSearch.addTextChangedListener(this)
        binding!!.rlHeader.setOnClickListener(this)
        binding!!.ivClose.setOnClickListener(this)
    }

    private fun getHooleyFriendsList() {
        binding!!.pbMyFriends.visibility = View.VISIBLE
        service.getMyFriendsList(object : GenericCallback<FriendsModel> {
            override fun success(result: FriendsModel) {
                binding!!.pbMyFriends.visibility = View.GONE
                objectResponseMyFriends = result
                addFriends()
                setRecyclerView()
            }

            override fun failure(message: String) {
                binding!!.pbMyFriends.visibility = View.GONE
                try {
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.btnSendInvite.visibility = View.GONE
                    binding!!.tvNoData.text = message
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }

    private fun addFriends() {
        if (friendList.size > 0)
            friendList.clear()
        if (::objectResponseMyFriends.isInitialized)
            friendList.addAll(objectResponseMyFriends.friendsList)
        if (friendList.size > 0) {
            binding!!.tvNoData.visibility = View.GONE
            binding!!.rvFriends.visibility = View.VISIBLE
            binding!!.btnSendInvite.visibility = View.VISIBLE
            friendList[0].header = Constants.HEADER_HOOLEY_FRIEND
        }
        if (selectedUsers.size > 0) {
            for (i in 0 until selectedUsers.size) {
                for (j in 0 until friendList.size) {
                    if (selectedUsers[i].userId == friendList[j].userId) {
                        friendList[j].isChecked = true
                    }
                }
            }
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnSendInvite -> {
                HooleyMain.activity!!.hideKeyboard()
                if (validInput())
                    sendInviteHooleyFriends()
                else
                    Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_selection))
            }
            R.id.ivClose -> {
                binding!!.edtSearch.setText("")
                try {
                    addFriends()
                    if (adapterInviteHooleyFriends != null)
                        adapterInviteHooleyFriends!!.notifyDataSetChanged()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            R.id.rlHeader -> {
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ShowSelectedUserFragment(selectedUsers, true), "ShowSelectedUserFragment")
            }
        }
    }

    override fun afterTextChanged(s: Editable?) {
        if (s!!.isNotEmpty()) {
            val diff = System.currentTimeMillis() - startTime
            if (diff > COPY_THRESHOLD) {
                startTime = System.currentTimeMillis()
                if (!HooleyMain.activity!!.checkIfName(s.toString())) {
                    if (binding!!.edtSearch.text!!.isEmpty()) {
                        try {
                            addFriends()
                            if (adapterInviteHooleyFriends != null)
                                adapterInviteHooleyFriends!!.notifyDataSetChanged()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                } else {
                    searchFriend(s.toString())
                }
            }
        } else {
            if (friendList.size > 0) {
                friendList.clear()
            }
            if (objectResponseMyFriends.friendsList.size > 0) {
                friendList.addAll(objectResponseMyFriends.friendsList)
                binding!!.rvFriends.visibility = View.VISIBLE
                binding!!.btnSendInvite.visibility = View.VISIBLE
                adapterInviteHooleyFriends!!.notifyDataSetChanged()
            } else {
                binding!!.tvNoData.visibility = VISIBLE
            }
        }
    }

    private fun searchFriend(searchString: String) {
        binding!!.pbMyFriends.visibility = View.VISIBLE
        service.getMoreInvitePeople(searchString, object : GenericCallback<HooleyUserModel> {
            override fun success(result: HooleyUserModel) {
                binding!!.pbMyFriends.visibility = View.GONE
                objectResponse = result
                updateList()
                if (binding!!.edtSearch.text!!.isEmpty()) {
                    if (friendList.size > 0) {
                        friendList.clear()
                    }
                    if (objectResponseMyFriends.friendsList.size > 0) {
                        friendList.addAll(objectResponseMyFriends.friendsList)
                        binding!!.rvFriends.visibility = View.VISIBLE
                        binding!!.btnSendInvite.visibility = View.VISIBLE
                        adapterInviteHooleyFriends!!.notifyDataSetChanged()
                    } else {
                        binding!!.tvNoData.visibility = VISIBLE
                    }
                }
            }

            override fun failure(message: String) {
                binding!!.pbMyFriends.visibility = View.GONE
                if (friendList != null)
                    friendList.clear()
                binding!!.tvNoData.text = message
                if (adapterInviteHooleyFriends != null)
                    adapterInviteHooleyFriends!!.notifyDataSetChanged()
            }
        })
    }


    private fun updateList() {
        if (binding!!.edtSearch.text!!.isBlank()) {
            return
        }
        if (hooleyFriendList.size > 0)
            hooleyFriendList.clear()
        if (hooleyUserList.size > 0)
            hooleyUserList.clear()
        for (i in 0 until objectResponse.inviteList.size) {
            if (objectResponse.inviteList[i].status == Constants.TYPE_FRIEND) {
                hooleyFriendList.add(objectResponse.inviteList[i])
            } else {
                hooleyUserList.add(objectResponse.inviteList[i])
            }
        }
        Collections.sort<FriendsTable>(hooleyFriendList, FriendsTable.sortByNameComparator)
        Collections.sort<FriendsTable>(hooleyUserList, FriendsTable.sortByNameComparator)
        if (friendList.size > 0)
            friendList.clear()
        for (loop in 0 until hooleyFriendList.size) {
            if (loop == 0) {
                hooleyFriendList[loop].header = Constants.HEADER_HOOLEY_FRIEND
            } else {
                hooleyFriendList[loop].header = Constants.TYPE_NONE
            }
            friendList.add(hooleyFriendList[loop])
        }
        for (loop in 0 until hooleyUserList.size) {
            if (loop == 0) {
                hooleyUserList[loop].header = Constants.HEADER_HOOLEY_USER
            } else {
                hooleyUserList[loop].header = Constants.TYPE_NONE
            }
            friendList.add(hooleyUserList[loop])
        }
        if (selectedUsers.size > 0) {
            for (i in 0 until selectedUsers.size) {
                for (j in 0 until friendList.size) {
                    if (selectedUsers[i].userId == friendList[j].userId) {
                        friendList[j].isChecked = true
                    }
                }
            }
        }
        if (adapterInviteHooleyFriends != null)
            adapterInviteHooleyFriends!!.notifyDataSetChanged()

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun onClickCheckItem(position: Int) {
        if (friendList[position].isChecked) {
            // Add item
            selectedUsers.add(0, friendList[position])
            checkForHeader()
        } else {
            //Remove item
            for (i in 0 until selectedUsers.size) {
                if (selectedUsers[i].userId == friendList[position].userId) {
                    selectedUsers.remove(selectedUsers[i])
                    break
                }
            }
            checkForHeader()
        }
    }

    fun checkForHeader() {
        if (selectedUsers.size > 0) {
            binding!!.rlHeader.visibility = View.VISIBLE
            if (!TextUtils.isEmpty(selectedUsers[0].profilePic))
                Glide.with(HooleyMain.activity!!).load(selectedUsers[0].profilePic).into(binding!!.ivFriendAvatar)
            else
                binding!!.ivFriendAvatar.setImageResource(R.drawable.ic_avatar_place_holder)
            binding!!.tvFriendName.text = selectedUsers[0].fullName
            if (selectedUsers.size == 1) {
                binding!!.tvAgeGroup.visibility = View.GONE
            } else {
                binding!!.tvAgeGroup.visibility = View.VISIBLE
                binding!!.tvAgeGroup.text = "& ${selectedUsers.size - 1} Others"
            }
        } else {
            binding!!.rlHeader.visibility = View.GONE
        }
    }

    fun onBackPressUpdateList() {
        if (friendList.size > 0) {
            for (loop in 0 until friendList.size) {
                friendList[loop].isChecked = false
            }
        }
        if (selectedUsers.size > 0) {
            for (i in 0 until selectedUsers.size) {
                for (j in 0 until friendList.size) {
                    if (selectedUsers[i].userId == friendList[j].userId) {
                        friendList[j].isChecked = true
                    }
                }
            }
        }
        if (adapterInviteHooleyFriends != null)
            adapterInviteHooleyFriends!!.notifyDataSetChanged()
        checkForHeader()
    }

    private fun sendInviteHooleyFriends() {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        HooleyMain.activity!!.showDialog()
        mService.sendInviteToHooleyFriends(createdEventId, createdEventName, selectedUsers, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_send_invite))
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {

                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun validInput(): Boolean {
        return selectedUsers.size > 0
    }


    private fun setRecyclerView() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        binding!!.rvFriends.layoutAnimation = animation
        binding!!.rvFriends.layoutManager = manager
        adapterInviteHooleyFriends = AdapterInviteHooleyFriends(HooleyMain.activity!!, friendList)
        adapterInviteHooleyFriends!!.mListener = this
        binding!!.rvFriends.setEmptyView(binding!!.tvNoData)
        binding!!.rvFriends.adapter = adapterInviteHooleyFriends
    }

    override fun onDestroy() {
        super.onDestroy()
        HooleyMain.activity!!.hideKeyboard()
        if (binding != null)
            binding!!.unbind()
    }

}

