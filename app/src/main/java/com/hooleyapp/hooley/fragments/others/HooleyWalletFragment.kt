package com.hooleyapp.hooley.fragments.others

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentHooleyWalletBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.services.TicketWebService
import com.stripe.android.Stripe
import com.stripe.android.TokenCallback
import com.stripe.android.model.Card
import com.stripe.android.model.Token


/**
 * Created by Nauman on 6/3/2018.
 */

@SuppressLint("ValidFragment")
class HooleyWalletFragment(var show: Boolean, var forPayment: Boolean, var title: String) : BaseFragment(), View.OnTouchListener, View.OnFocusChangeListener {

    lateinit var binding: FragmentHooleyWalletBinding
    var service = TicketWebService()
    var imageArray = arrayOf<Int>(R.drawable.ic_american_express,
            R.drawable.ic_dinners_club,
            R.drawable.ic_discover_y,
            R.drawable.ic_j_cb,
            R.drawable.ic_m_astercard,
            R.drawable.ic_u_nionpay,
            R.drawable.ic_v_isa)

    private var found: Boolean = false
    private var intent: Intent = Intent()
    var oldLength = 0
    private var stripeToken: String = ""


    fun setToolbar() {
        if (title.contains("Save")) {
            setToolBarTitle(HooleyMain.activity!!, "Hooley Wallet")
        } else {
            setToolBarTitle(HooleyMain.activity!!, title)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_hooley_wallet, container, false)

        if (!show) {
            binding.rlSaveInfo.visibility = GONE
        }
        if (title.contains("Hooley")) {
            binding.btnSaveCard.text = "Save Card"
        } else {
            binding.btnSaveCard.text = title
        }

        binding.etCardNumber.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (!binding.etCardNumber.text!!.isEmpty()) {
                    for (i in 0 until listOfPattern().size) {
                        if (binding.etCardNumber.text!!.matches(listOfPattern()[i].toRegex())) {
                            binding.ivCardLogo.visibility = View.VISIBLE
                            binding.ivCardLogo.setImageResource(imageArray[i])
                            found = true
                            break
                        }
                    }

                    if (binding.etCardNumber.text!!.trim().length == 16) {
                        if (oldLength < binding.etCardNumber.text!!.length) {
                            binding.etMonthYear.requestFocus()
                            binding.etMonthYear.performClick()
                        }


                    }
                } else {
                    found = false
                    binding.ivCardLogo.visibility = View.GONE
                }

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                oldLength = binding.etCardNumber.text!!.length
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }


        })

        binding.etMonthYear.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (s.length == 1) {
                    if (binding.etMonthYear.text!!.matches("[2-9]".toRegex())) {
                        binding.etMonthYear.text = s.insert(0, "0")
                        binding.etMonthYear.text!!.append("")
                        binding.etMonthYear.setSelection(binding.etMonthYear.text!!.length)
                    }
                }
                if (s.length == 2) {
                    if (binding.etMonthYear.text!!.matches("^(1[0-2]|0[1-9])\$".toRegex())) {
                        binding.etMonthYear.text!!.append("")
                        binding.etMonthYear.text!!.append("/")
                        binding.etMonthYear.setSelection(binding.etMonthYear.text!!.length)
                    } else {
                        binding.etMonthYear.setText("")
                    }
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                oldLength = binding.etMonthYear.text!!.length
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        binding.etCardNumber.setOnTouchListener(this)
        binding.etMonthYear.setOnTouchListener(this)
        binding.etCVC.setOnTouchListener(this)
        binding.etCardNumber.onFocusChangeListener = this
        binding.etMonthYear.onFocusChangeListener = this
        binding.etCVC.onFocusChangeListener = this

        binding.btnSaveCard.setOnClickListener {
            HooleyMain.activity!!.hideKeyboard()
            if (validateCard()) {
                val card = Card(binding.etCardNumber.text.toString(), binding.etMonthYear.text.toString().split("/")[0].toInt(), binding.etMonthYear.text.toString().split("/")[1].toInt(), binding.etCVC.text.toString())
                card.name = binding.etCardName.text.toString()
                if (card.validateCard()) {
                    HooleyMain.activity!!.showDialog()
                    val stripe = Stripe(HooleyMain.activity!!, HooleyMain.activity!!.getString(R.string.stripe_publish_key))
                    stripe.createToken(card,
                            object : TokenCallback {
                                override fun onSuccess(token: Token) {
                                    Log.e("token",
                                            " " + token.id)
                                    stripeToken = token.id
                                    if (show) {
                                        if (!binding.cbPostPone.isChecked) {
                                            HooleyMain.activity!!.removeDialog()
                                            intent.putExtra("stripeToken", stripeToken)
                                            HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                                            targetFragment!!.onActivityResult(
                                                    targetRequestCode,
                                                    Activity.RESULT_OK,
                                                    intent
                                            )
                                        } else {
                                            addCardService(stripeToken)
                                        }
                                    } else {
                                        addCardService(stripeToken)
                                    }
                                }

                                override fun onError(error: Exception) {
                                    error.printStackTrace()
                                    HooleyMain.activity!!.removeDialog()
                                    HooleyMain.activity!!.showSuccessDialog(error.message!!)
                                }
                            }
                    )
                } else {
                    HooleyMain.activity!!.showSuccessDialog("Please enter valid card")
                }
            } else {
                HooleyMain.activity!!.showSuccessDialog("Please fill all fields")
            }
        }

        return binding.root
    }


    private fun addCardService(stripeToken: String) {
        service.addCard(stripeToken, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyApp.db.putBoolean(Constants.IS_WALLET_SETUP, true)
                HooleyMain.activity!!.removeDialog()
                if (forPayment) {
                    intent.putExtra("stripeToken", "")
                    HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                    targetFragment!!.onActivityResult(
                            targetRequestCode,
                            Activity.RESULT_OK,
                            intent
                    )
                } else {
                    HooleyMain.activity!!.showSuccessDialog("Card Added Successfully")
                    callFragmentWithReplace(R.id.container, HooleyWalletCardsFragment(false, title), null)
                }
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                HooleyMain.activity!!.showSuccessDialog(message)
            }
        })

    }

    private fun validateCard(): Boolean {
        return when {
            binding.etCardName.text!!.isEmpty() -> false
            binding.etCardNumber.text!!.isEmpty() -> false
            binding.etMonthYear.text!!.isEmpty() -> false
            else -> !binding.etCVC.text!!.isEmpty()
        }
    }

    fun listOfPattern(): ArrayList<String> {
        val listOfPattern = ArrayList<String>()

        val ptAmeExp = "^3[47]$"

        listOfPattern.add(ptAmeExp)

        val ptDinners = "^3(?:0[0-5]|[68][0-9])[0-9]$"

        listOfPattern.add(ptDinners)

        val ptDiscover = "^6(?:011|5[0-9]{2})$"

        listOfPattern.add(ptDiscover)

        val ptJCBExp = "^3[5]$"

        listOfPattern.add(ptJCBExp)

        val ptMasterCard = "^5[0-5]$"

        listOfPattern.add(ptMasterCard)

        val ptUnionPay = "^6(?:2|8)$"

        listOfPattern.add(ptUnionPay)

        val ptVisa = "^4[0-9]$"

        listOfPattern.add(ptVisa)


        return listOfPattern
    }


    override fun onTouch(view: View, event: MotionEvent): Boolean {
        if (view is TypeFaceEditText) {
            view.setOnFocusChangeListener(this)
        }
        return false
    }

    override fun onFocusChange(view: View, hasFocus: Boolean) {
        when (view.id) {
            R.id.etCardNumber -> {
                if (hasFocus) {
                    if (binding.etCardName.text!!.isEmpty()) {
                        binding.etCardName.requestFocus()
                        binding.etCardName.performClick()
                        HooleyMain.activity!!.showSuccessDialog("Please enter your Name")

                    }
                }
            }
            R.id.etMonthYear -> {
                if (hasFocus) {
                    if (binding.etCardName.text!!.isEmpty()) {
                        binding.etCardName.requestFocus()
                        binding.etCardName.performClick()
                        HooleyMain.activity!!.showSuccessDialog("Please enter your Name")
                    } else if (binding.etCardNumber.text!!.trim().length == 16) {
                        binding.etCardNumber.setText(binding.etCardNumber.text!!.toString().replace("(\\d{4})(?=\\d)".toRegex(), "$1 "))
                    } else if (binding.etCardName.text!!.isEmpty()) {
                        binding.etCardNumber.requestFocus()
                        binding.etCardNumber.performClick()
                        HooleyMain.activity!!.showSuccessDialog("Card required")
                    } else if (!found) {
                        binding.etCardNumber.requestFocus()
                        binding.etCardNumber.performClick()
                        HooleyMain.activity!!.showSuccessDialog("Invalid Card")
                    } else if (binding.etCardNumber.text!!.length < 15) {
                        binding.etCardNumber.requestFocus()
                        binding.etCardNumber.performClick()
                        HooleyMain.activity!!.showSuccessDialog("Invalid Card")
                    }
                }
            }
            R.id.etCVC -> {
                if (hasFocus) {
                    if (binding.etCardName.text!!.isEmpty()) {
                        binding.etCardName.requestFocus()
                        binding.etCardName.performClick()
                        HooleyMain.activity!!.showSuccessDialog("Please enter your Name")
                    } else if (binding.etCardNumber.text!!.trim().length == 16) {
                        binding.etCardNumber.setText(binding.etCardNumber.text!!.toString().replace("(\\d{4})(?=\\d)".toRegex(), "$1 "))
                    } else if (binding.etCardName.text!!.isEmpty()) {
                        binding.etCardNumber.requestFocus()
                        HooleyMain.activity!!.showSuccessDialog("Card required")
                    } else if (!found) {
                        binding.etCardNumber.requestFocus()
                        HooleyMain.activity!!.showSuccessDialog("Invalid Card")
                    } else if (binding.etCardNumber.text!!.length < 15) {
                        binding.etCardNumber.requestFocus()
                        HooleyMain.activity!!.showSuccessDialog("Invalid Card")

                    }
                }
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        binding.unbind()
    }

}
