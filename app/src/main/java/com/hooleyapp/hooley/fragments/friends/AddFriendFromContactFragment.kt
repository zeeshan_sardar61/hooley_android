package com.hooleyapp.hooley.fragments.friends

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterAddFriendFromContact
import com.hooleyapp.hooley.databinding.FragmentAddFriendFromContactBinding
import com.hooleyapp.hooley.db.DbManager
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IFriendContactClickListener
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.ContactFriendListModel
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.PhoneContactsModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.ContactWebService
import com.hooleyapp.hooley.services.FriendWebService
import java.util.*

class AddFriendFromContactFragment : BaseFragment(), IFriendContactClickListener, ViewAvatarFragment.CardCallBackListener {

    var binding: FragmentAddFriendFromContactBinding? = null
    private var friendService = FriendWebService()
    private lateinit var adapter: AdapterAddFriendFromContact
    private lateinit var objectResponse: ContactFriendListModel
    private var hooleyContactList = ArrayList<ContactFriendListModel.ContactModel>()
    private var phoneContactList = ArrayList<ContactFriendListModel.ContactModel>()
    private var mList = ArrayList<ContactFriendListModel.ContactModel>()
    var contactList: ArrayList<PhoneContactsModel> = arrayListOf()
    private val mService = ContactWebService()
    private var handler = Handler()
    private val mContactReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            try {
                handler.postDelayed({ sendContactToServer() }, 200)
            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            }

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_friend_from_contact, container, false)
        LocalBroadcastManager.getInstance(HooleyMain.activity!!).registerReceiver(mContactReceiver,
                IntentFilter("Contacts"))
        if (HooleyApp.db.getBoolean(Constants.SYNC_USER_CONTACTS, false))
            getContactList()
        else
            sendContactToServer()
        return binding!!.root
    }


    private fun getContactList() {
//        HooleyMain.activity!!.showDialog()
        friendService.addFriendFromContact(object : GenericCallback<ContactFriendListModel> {
            override fun success(result: ContactFriendListModel) {
//                HooleyMain.activity!!.HooleyMain.activity!!.removeDialog()
                objectResponse = result
                setUpListOrder()
            }

            override fun failure(message: String) {
//                HooleyMain.activity!!.HooleyMain.activity!!.removeDialog()
            }
        })

    }

    private fun setUpListOrder() {
        if (phoneContactList.size > 0)
            phoneContactList.clear()
        if (hooleyContactList.size > 0)
            hooleyContactList.clear()
        for (i in 0 until objectResponse.contactListList.size) {
            if (objectResponse.contactListList[i].status == Constants.NO_HOOLEY_USER) {
                phoneContactList.add(objectResponse.contactListList[i])
            } else {
                hooleyContactList.add(objectResponse.contactListList[i])
            }
        }
        Collections.sort<ContactFriendListModel.ContactModel>(phoneContactList, ContactFriendListModel.ContactModel.sortByNameComparator)
        Collections.sort<ContactFriendListModel.ContactModel>(hooleyContactList, ContactFriendListModel.ContactModel.sortByNameComparator)
        if (mList.size > 0)
            mList.clear()
        for (loop in 0 until hooleyContactList.size) {
            mList.add(hooleyContactList[loop])
        }
        var separatorModel = ContactFriendListModel.ContactModel()
        separatorModel.status = Constants.SEPARATOR
        mList.add(separatorModel)
        for (loop in 0 until phoneContactList.size) {
            mList.add(phoneContactList[loop])
        }
        setRecyclerView()
    }


    private fun setRecyclerView() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding!!.rvContactFriends.layoutManager = manager
        binding!!.rvContactFriends.layoutAnimation = animation
        adapter = AdapterAddFriendFromContact(HooleyMain.activity!!, mList)
        binding!!.rvContactFriends.adapter = adapter
        adapter.mListener = this
    }

    override fun reloadFragment(flag: Boolean) {
        if (flag)
            getContactList()
    }

    override fun onClickAddFriend(position: Int) {
        mList[position].status = Constants.TYPE_PENDING_SENT
        adapter.notifyDataSetChanged()
        sendFriendRequest(position)
    }

    override fun onClickAcceptFriend(position: Int) {
        acceptFriendRequest(position)
    }

    private fun sendFriendRequest(position: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        val friendList = ArrayList<Int>()
        friendList.add(mList[position].userId.toInt())
        friendService.sendFriendRequest(friendList, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                Util.showToastMessage(HooleyMain.activity!!, "Friend Request Sent")
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun acceptFriendRequest(position: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        friendService.acceptIgnoreFriendRequest(mList[position].userId.toInt(), true, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                mList[position].status = Constants.TYPE_FRIEND
                if (adapter != null)
                    adapter.notifyDataSetChanged()
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.showSuccessDialog(message)
            }
        })

    }


    private fun sendContactToServer() {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        if (contactList.isNotEmpty())
            contactList.clear()
        contactList = DbManager.getContactList() as ArrayList<PhoneContactsModel>
        binding!!.rvContactFriends.visibility = View.GONE
        binding!!.rlContactSyncing.visibility = View.VISIBLE
        mService.sendContactToServer(contactList, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                handler.postDelayed({ getContactList() }, 200)

                binding!!.rlContactSyncing.visibility = View.GONE
                binding!!.rvContactFriends.visibility = View.VISIBLE
                HooleyApp.db.putBoolean(Constants.SYNC_USER_CONTACTS, true)
            }

            override fun failure(message: String) {
                try {
                    binding!!.rlContactSyncing.visibility = View.GONE
                    binding!!.rvContactFriends.visibility = View.VISIBLE
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }


    override fun onClickItem(position: Int) {
        var avatarfragment = ViewAvatarFragment(mList[position].userId.toInt())
        avatarfragment.mListener = this
        avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

}