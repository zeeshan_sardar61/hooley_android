package com.hooleyapp.hooley.fragments.auth

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.DatePicker
import android.widget.Toast
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyAuth
import com.hooleyapp.hooley.adapters.AdapterCountries
import com.hooleyapp.hooley.adapters.AdapterGender
import com.hooleyapp.hooley.databinding.FragmentProfileSetupBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.ICallBackUri
import com.hooleyapp.hooley.model.CountryStateCitiesModel
import com.hooleyapp.hooley.model.PersonalProfileModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.requestmodel.VerifyPhoneNumberModel
import com.hooleyapp.hooley.services.ProfileWebService
import com.rilixtech.Country
import com.rilixtech.CountryCodePicker
import java.text.ParseException
import java.util.*

class ProfileSetupFragment : BaseFragment(), CountryCodePicker.OnCountryChangeListener, View.OnClickListener, AdapterView.OnItemSelectedListener, View.OnTouchListener {
    var binding: FragmentProfileSetupBinding? = null
    private val messageId = ""
    private var cc = "1"
    private var ccIso = "us"
    private val service = ProfileWebService()
    private var responseCountry = CountryStateCitiesModel()
    private var strCountryId: String? = null
    private var strStateId: String? = null
    private var strCityId: String? = null
    private var mediaUri: Uri? = null
    private var isCitySet = false
    private var adapterSpinner: AdapterCountries? = null
    private var countryList: ArrayList<PersonalProfileModel.myObject>? = ArrayList()
    private var stateList: ArrayList<PersonalProfileModel.myObject>? = ArrayList()
    private var citiesList: ArrayList<PersonalProfileModel.myObject>? = ArrayList()
    private var isCountrySet = false
    private var isStateSet = false
    private lateinit var moreDialogeFragment: MoreDialogeFragment
    private var mListGender: ArrayList<String>? = null
    private var spinnerAdapter: AdapterGender? = null
    private var gender = -1


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile_setup, container, false)
        setListener()
        getCountries("country", "0", "0", Constants.TYPE_COUNTRIES)
        initGenderSpinner()
//        setMoreCallerBackListener()
        binding!!.ivBack.setOnClickListener {
            HooleyAuth.activity.onBackPressed()
        }
        return binding!!.root
    }

    private var userChoosenTask: String = ""

    fun selectImage() {
        val items = arrayOf<CharSequence>("Take Photo", "Choose from Library", "Cancel")

        val builder = AlertDialog.Builder(HooleyAuth.activity)
        builder.setTitle("Add Photo!")
        builder.setItems(items) { dialog, item ->
            if (items[item] == "Take Photo") {
                userChoosenTask = "Take Photo"
//                    cameraIntent()
                HooleyAuth.activity.startCamera(Constants.CAMERA_RQ)

            } else if (items[item] == "Choose from Library") {
                userChoosenTask = "Choose from Library"
//                    galleryIntent()
                HooleyAuth.activity.showGallery(Constants.IMAGE_PICKER_SELECT)

            } else if (items[item] == "Cancel") {
                dialog.dismiss()
            }
        }
        builder.show()
    }


    private fun initGenderSpinner() {
        mListGender = ArrayList()
        mListGender!!.add("Female")
        mListGender!!.add("Male")
        mListGender!!.add("Transgender")
        spinnerAdapter = AdapterGender(HooleyAuth.activity, R.layout.sp_gender_group_item, mListGender)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spGender.adapter = spinnerAdapter
    }

    fun setListener() {
        binding!!.tvDob.setOnClickListener(this)
        binding!!.ivAvatar.setOnClickListener(this)
        binding!!.btnSubmit.setOnClickListener(this)
        binding!!.ccp.setOnCountryChangeListener(this)
        binding!!.spCity.setOnTouchListener(this)
        binding!!.spCountry.setOnTouchListener(this)
        binding!!.spState.setOnTouchListener(this)
        //        userSearchbinding.ccp.registerPhoneNumberTextView(userSearchbinding.edtPhone);
        binding!!.spGender.onItemSelectedListener = this
        binding!!.ccp.setPhoneNumberInputValidityListener { countryCodePicker, b ->
            if (!b) {
                binding!!.edtPhone.error = "Please enter valid number"
            }
        }


    }


    fun showDateDialog() {
        val dialogView = View.inflate(HooleyAuth.activity, R.layout.date_picker, null)
        val alertDialog = AlertDialog.Builder(HooleyAuth.activity).create()
        val datePicker = dialogView.findViewById<DatePicker>(R.id.date_picker)
        val c = Calendar.getInstance()
        val mYear: Int
        val mMonth: Int
        val mDay: Int
        if (!TextUtils.isEmpty(binding!!.tvDob.text.toString())) {
            val splitedDate = binding!!.tvDob.text.toString().split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            mYear = Integer.parseInt(splitedDate[2])
            mMonth = Integer.parseInt(splitedDate[0]) - 1
            mDay = Integer.parseInt(splitedDate[1])
        } else {
            mYear = c.get(Calendar.YEAR)
            mMonth = c.get(Calendar.MONTH)
            mDay = c.get(Calendar.DAY_OF_MONTH)
        }
        datePicker.updateDate(mYear, mMonth, mDay)
        datePicker.maxDate = System.currentTimeMillis()
        dialogView.findViewById<View>(R.id.rlSetTimeStamp).setOnClickListener {
            val bDate = (datePicker.month + 1).toString() + "/" + datePicker.dayOfMonth + "/" + datePicker.year
            try {
                if (Util.compareBirthDate(bDate) <= 1) {
                    binding!!.tvDob.text = bDate
                    alertDialog.dismiss()
                } else {
                    Util.showToastMessage(HooleyAuth.activity, "Birth date not valid")
                }
            } catch (e: ParseException) {
                e.printStackTrace()
            }
        }
        alertDialog.setView(dialogView)
        alertDialog.show()
    }

    private fun setMoreCallerBackListener() {
        val arrayList = ArrayList<String>()
        arrayList.add("Photo")
        arrayList.add("Gallery")
        moreDialogeFragment = MoreDialogeFragment.Builder(HooleyAuth.activity).setTitle("Select").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, _ ->
            when (position) {
                0 -> {
                    moreDialogeFragment.dismiss()
                    HooleyAuth.activity.startCamera(Constants.CAMERA_RQ)
                }
                1 -> {
                    moreDialogeFragment.dismiss()
                    HooleyAuth.activity.showGallery(Constants.IMAGE_PICKER_SELECT)
                }
            }
        }.create()

    }


    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onCountrySelected(country: Country) {
        cc = country.phoneCode
        ccIso = country.iso
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tvDob -> showDateDialog()
            R.id.ivAvatar -> selectImage()
            R.id.btnSubmit -> {
                if (validateInput()) {
                    setupProfile()
                }
            }
            R.id.spCity -> Util.hideKeyboard(HooleyAuth.activity)
            R.id.spState -> Util.hideKeyboard(HooleyAuth.activity)
            R.id.spCountry -> Util.hideKeyboard(HooleyAuth.activity)
        }
    }

    private fun setupProfile() {
        if (!NetworkUtil.isInternetConnected(HooleyAuth.activity)) {
            HooleyAuth.activity.showSuccessDialog(Constants.NO_NETWORK)
            return
        }
        HooleyAuth.activity.showDialog()
        service.setupProfile(mediaUri, binding!!.tvDob.text.toString(), binding!!.edtPhone.text.toString().trim { it <= ' ' }, cc, ccIso, strCountryId!!, strStateId!!, strCityId!!, cc + binding!!.edtPhone.text.toString().trim { it <= ' ' }, gender, object : GenericCallback<VerifyPhoneNumberModel> {
            override fun success(result: VerifyPhoneNumberModel) {
                HooleyAuth.activity.removeDialog()

                if (result.userInfo != null) {
                    (activity as ActivityBase).callFragment(R.id.authContainer, PhoneAlreadyExistFragment.newInstance(cc + binding!!.edtPhone.text.toString().trim { it <= ' ' }, result.userInfo!!.profilePic!!,
                            result.userInfo!!.fullName!!,
                            result.userInfo!!.email!!), "PhoneAlreadyExistFragment")
                } else {
                    (activity as ActivityBase).addFragment(R.id.authContainer, VerifyCodeFragment.newInstance(result.apiMessageId!!, cc, binding!!.edtPhone.text.toString().trim { it <= ' ' }), "VerifyCodeFragment")
                }
            }

            override fun failure(message: String) {
                HooleyAuth.activity.removeDialog()
                try {
                    if (message.contains("Phone")) {
                        val sourceString = "<b>${cc + binding!!.edtPhone.text.toString().trim { it <= ' ' }}</b><br>" + message
                        HooleyAuth.activity.showSuccessDialog(sourceString)
                    } else {
                        HooleyAuth.activity.showSuccessDialog(message)
                    }
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun validateInput(): Boolean {
        if (Util.isContentNull(binding!!.tvDob.text.toString())) {
            Util.showToastMessage(HooleyAuth.activity, "Date of Birth is missing")
            return false
        } else if (Util.isContentNull(binding!!.edtPhone.text.toString())) {
            binding!!.edtPhone.requestFocus()
            binding!!.edtPhone.error = getString(R.string.ic_add_number)
            return false
        } else if (binding!!.edtPhone.text.toString().length > 10) {
            binding!!.edtPhone.requestFocus()
            binding!!.edtPhone.error = "Please enter a valid number"
            return false
        } else if (!isCountrySet) {
            Toast.makeText(activity, "Country is missing", Toast.LENGTH_SHORT).show()
        } else if (gender == -1) {
            Util.showToastMessage(activity!!, getString(R.string.str_select_gender))
            return false
        } else if (!isStateSet) {
            Toast.makeText(activity, "State is missing", Toast.LENGTH_SHORT).show()
        } else if (!isCitySet) {
            Toast.makeText(activity, "City is missing", Toast.LENGTH_SHORT).show()
        }
        return isCitySet
    }

    private fun setCountrySpinner(response: CountryStateCitiesModel) {
        if (countryList != null && countryList!!.size > 0)
            countryList!!.clear()
        countryList = response.countryStateCitiesList
        val `object` = PersonalProfileModel.myObject()
        `object`.id = UUID.randomUUID().toString()
        `object`.name = "Country"
        countryList!!.add(0, `object`)
        adapterSpinner = AdapterCountries(HooleyAuth.activity, R.layout.sp_gender_group_item, countryList)
        adapterSpinner!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spCountry.adapter = adapterSpinner
        binding!!.spCountry.onItemSelectedListener = this
    }

    private fun setStateSpinner(response: CountryStateCitiesModel) {
        if (stateList != null && stateList!!.size > 0)
            stateList!!.clear()
        stateList = response.countryStateCitiesList
        Collections.sort(stateList!!, PersonalProfileModel.sortByNameComparator)
        val `object` = PersonalProfileModel.myObject()
        `object`.id = UUID.randomUUID().toString()
        `object`.name = "State"
        stateList!!.add(0, `object`)
        adapterSpinner = AdapterCountries(HooleyAuth.activity, R.layout.sp_gender_group_item, stateList)
        adapterSpinner!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spState.adapter = adapterSpinner
        binding!!.spState.onItemSelectedListener = this
    }

    private fun setCitySpinner(response: CountryStateCitiesModel) {
        if (citiesList != null && citiesList!!.size > 0)
            citiesList!!.clear()
        citiesList = response.countryStateCitiesList
        Collections.sort(citiesList!!, PersonalProfileModel.sortByNameComparator)
        val `object` = PersonalProfileModel.myObject()
        `object`.id = UUID.randomUUID().toString()
        `object`.name = "City"
        citiesList!!.add(0, `object`)
        adapterSpinner = AdapterCountries(HooleyAuth.activity, R.layout.sp_gender_group_item, citiesList)
        adapterSpinner!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spCity.adapter = adapterSpinner
        binding!!.spCity.onItemSelectedListener = this
    }

    fun getCountries(area: String, countryId: String?, stateId: String?, state: Int) {
        when (state) {
            Constants.TYPE_COUNTRIES -> binding!!.pbCountries.visibility = View.VISIBLE
            Constants.TYPE_STATE -> binding!!.pbState.visibility = View.VISIBLE
            Constants.TYPE_CITY -> binding!!.pbCity.visibility = View.VISIBLE
        }

        service.getCountries(area, countryId!!, stateId!!, object : GenericCallback<CountryStateCitiesModel> {
            override fun success(result: CountryStateCitiesModel) {
                responseCountry = result
                when (state) {
                    Constants.TYPE_COUNTRIES -> {
                        binding!!.pbCountries.visibility = View.INVISIBLE
                        setCountrySpinner(responseCountry)
                        setEmptySpinners()
                    }
                    Constants.TYPE_STATE -> {
                        binding!!.pbState.visibility = View.INVISIBLE
                        binding!!.tvState.visibility = View.GONE
                        binding!!.spState.visibility = View.VISIBLE
                        isStateSet = false
                        setStateSpinner(responseCountry)
                    }
                    Constants.TYPE_CITY -> {
                        binding!!.pbCity.visibility = View.INVISIBLE
                        binding!!.tvCity.visibility = View.GONE
                        binding!!.spCity.visibility = View.VISIBLE
                        isCitySet = false
                        setCitySpinner(responseCountry)
                    }
                }
            }

            override fun failure(message: String) {
                when (state) {
                    Constants.TYPE_COUNTRIES -> binding!!.pbCountries.visibility = View.INVISIBLE
                    Constants.TYPE_STATE -> {
                        binding!!.pbState.visibility = View.INVISIBLE
                        binding!!.spState.visibility = View.INVISIBLE
                        binding!!.tvState.visibility = View.VISIBLE
                        binding!!.tvState.text = message
                        strStateId = "0"
                        isStateSet = true
                    }
                    Constants.TYPE_CITY -> {
                        binding!!.pbCity.visibility = View.INVISIBLE
                        binding!!.spCity.visibility = View.INVISIBLE
                        binding!!.tvCity.visibility = View.VISIBLE
                        binding!!.tvCity.text = message
                        strStateId = "0"
                        isCitySet = true
                    }
                }
                try {
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }

    private fun setEmptySpinners() {

        binding!!.pbState.visibility = View.INVISIBLE
        binding!!.pbCity.visibility = View.INVISIBLE

        if (citiesList != null && citiesList!!.size > 0)
            citiesList!!.clear()
        val `object` = PersonalProfileModel.myObject()
        `object`.id = UUID.randomUUID().toString()
        `object`.name = "City"
        citiesList!!.add(0, `object`)
        adapterSpinner = AdapterCountries(HooleyAuth.activity, R.layout.sp_gender_group_item, citiesList)
        adapterSpinner!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spCity.adapter = adapterSpinner

        if (stateList != null && stateList!!.size > 0)
            stateList!!.clear()
        val object1 = PersonalProfileModel.myObject()
        object1.id = UUID.randomUUID().toString()
        object1.name = "State"
        stateList!!.add(0, object1)
        adapterSpinner = AdapterCountries(HooleyAuth.activity, R.layout.sp_gender_group_item, stateList)
        adapterSpinner!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spState.adapter = adapterSpinner
        binding!!.spState.onItemSelectedListener = this
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
        when (parent.id) {
            R.id.spCountry -> if (position != 0) {
                strCountryId = countryList!![position].id
                getCountries("state", strCountryId, "0", Constants.TYPE_STATE)
                isCountrySet = true
            } else {
                isCountrySet = false
            }
            R.id.spState -> if (position != 0) {
                strStateId = stateList!![position].id
                getCountries("city", strCountryId, strStateId, Constants.TYPE_CITY)
                isStateSet = true
            } else {
                isStateSet = false
            }
            R.id.spCity -> if (position != 0) {
                strCityId = citiesList!![position].id
                isCitySet = true
            } else {
                isCitySet = false
            }
            R.id.spGender -> {
                if (position == 0) {
                    gender = 2
                } else if (position == 1) {
                    gender = 1
                } else {
                    gender = 3
                }
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constants.CAMERA_RQ) {
                HooleyAuth.activity.processCapturedPhoto(object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        mediaUri = HooleyAuth.activity.compressFile(result!!)
                        binding!!.ivAvatar.setImageBitmap(HooleyAuth.activity.handleSamplingAndRotationBitmap(result))
                    }

                })
            } else if (requestCode == Constants.IMAGE_PICKER_SELECT) {
                HooleyAuth.activity.processGalleryPhoto(data!!, object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        mediaUri = HooleyAuth.activity.compressFile(result!!)
                        binding!!.ivAvatar.setImageBitmap(HooleyAuth.activity.handleSamplingAndRotationBitmap(result))
                    }

                })
            }
        } else {
            Toast.makeText(HooleyAuth.activity, "Action Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_UP) {
            Util.hideKeyboard(HooleyAuth.activity)
        }
        return false
    }

    companion object {
        lateinit var instance: ProfileSetupFragment
        fun newInstance(): ProfileSetupFragment {
            instance = ProfileSetupFragment()
            return instance
        }
    }
}
