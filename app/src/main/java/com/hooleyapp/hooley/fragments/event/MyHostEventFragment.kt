package com.hooleyapp.hooley.fragments.event

import android.app.Dialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.RadioGroup
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterMoreMediaDialoge
import com.hooleyapp.hooley.adapters.AdapterMyHostEvent
import com.hooleyapp.hooley.databinding.FragmentMyHostEventBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.others.MoreMediaDialogFragment
import com.hooleyapp.hooley.helper.CustomDateTimePicker
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.MyHostEventModel
import com.hooleyapp.hooley.model.SetAlertModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.EventWebService
import java.util.*

/**
 * Created by Nauman on 1/17/2018.
 */

class MyHostEventFragment : BaseFragment(), RadioGroup.OnCheckedChangeListener, AdapterMyHostEvent.IMyEventsClickListeners, SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    private var binding: FragmentMyHostEventBinding? = null
    private var adapterMyHostEvent: AdapterMyHostEvent? = null
    private val offsetHeader = true
    private var firstVisibleInListview: Int = 0
    private val gson = Gson()
    private val myHostEventModel: MyHostEventModel? = null
    var service = EventWebService()
    private var publishEventList: ArrayList<MyHostEventModel.HostEvents>? = null
    private var unPublishEventList: ArrayList<MyHostEventModel.HostEvents>? = null
    internal var isPublished = false
    internal var isUnPublished = false
    internal var isSwiped = false
    internal var font1 = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)
    internal var font2 = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)
    internal val filterList = ArrayList<AdapterMoreMediaDialoge.MoreItem>()
    lateinit var model: MyHostEventModel
    private var customDialog: CustomDateTimePicker? = null

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {

        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            try {
                //                getFromDb();
            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            }

        } else {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_host_event, container, false)
        //        getFromDb();
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
        binding!!.rbPublishedEvent.isChecked = true
        binding!!.rbPublishedEvent.typeface = font1

    }

    private fun setMoreAlbumCallerBackListener() {
        if (filterList.size > 0) {
            filterList.clear()
        }
        val alphabetiacalList = AdapterMoreMediaDialoge.MoreItem("A - Z", true)
        val itemStartTime = AdapterMoreMediaDialoge.MoreItem("Time", false)
        filterList.add(alphabetiacalList)
        filterList.add(itemStartTime)

        moreFilterDialogeFragment = MoreMediaDialogFragment.Builder(HooleyMain.activity!!).setTitle("Sort by").setType(Constants.TYPE_MORE_80).setList(filterList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    FilterByAtoZ()
                    filterList[0].isCheck = (true)
                    filterList[1].isCheck = (false)
                    moreFilterDialogeFragment!!.dismiss()
                }
                1 -> {
                    filterList[0].isCheck = (false)
                    filterList[1].isCheck = (true)
                    FilterByStartTime()
                    moreFilterDialogeFragment!!.dismiss()
                }

            }
        }.create()
        moreFilterDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ivFilter -> {
                if (publishEventList != null) {
                    if (publishEventList!!.size > 0) {
                        setMoreAlbumCallerBackListener()
                    }
                }
            }
        }
    }

    private fun FilterByAtoZ() {
        if (isPublished) {
            if (publishEventList!!.size > 0) {
                publishEventList!!.clear()
                publishEventList!!.addAll(model.publishEventsArrayList)
            }
            setPublishEventRecyclerView()
            moreFilterDialogeFragment!!.dismiss()
        } else {
            if (unPublishEventList!!.size > 0) {
                unPublishEventList!!.clear()
                unPublishEventList!!.addAll(model.unPublishEventsArrayList)
            }
            setUnPublishEventRecyclerView()
            moreFilterDialogeFragment!!.dismiss()

        }
    }

    private fun FilterByCreateDate() {
        if (isPublished) {
            if (publishEventList!!.size > 0) {
                publishEventList!!.clear()
                publishEventList!!.addAll(model.publishEventsArrayList)
            }
            setCreateEventTimeRecyclerView(publishEventList!!)
            moreFilterDialogeFragment!!.dismiss()
        } else {
            if (unPublishEventList!!.size > 0) {
                unPublishEventList!!.clear()
                unPublishEventList!!.addAll(model.unPublishEventsArrayList)
            }
            setCreateEventTimeRecyclerView(unPublishEventList!!)
            moreFilterDialogeFragment!!.dismiss()

        }
    }

    private fun FilterByStartTime() {
        if (isPublished) {
            if (publishEventList!!.size > 0) {
                publishEventList!!.clear()
                publishEventList!!.addAll(model.publishEventsArrayList)
            }
            setStartEventTimeRecyclerView(publishEventList!!)
            moreFilterDialogeFragment!!.dismiss()
        } else {
            if (unPublishEventList!!.size > 0) {
                unPublishEventList!!.clear()
                unPublishEventList!!.addAll(model.unPublishEventsArrayList)
            }
            setStartEventTimeRecyclerView(unPublishEventList!!)
            moreFilterDialogeFragment!!.dismiss()

        }
    }

    private fun setListener() {
        binding!!.slHosting.setOnRefreshListener(this)
        binding!!.rgTabs.setOnCheckedChangeListener(this)
        binding!!.ivFilter.setOnClickListener(this)
    }

    private fun setPublishEventRecyclerView() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        binding!!.rvPublishedEvent.layoutAnimation = animation
        binding!!.rvPublishedEvent.layoutManager = manager
        binding!!.rvPublishedEvent.setEmptyView(binding!!.tvNoData)
        adapterMyHostEvent = AdapterMyHostEvent(HooleyMain.activity!!, sortList(publishEventList!!), true)
        adapterMyHostEvent!!.mListener = this
        binding!!.rvPublishedEvent.adapter = adapterMyHostEvent
        firstVisibleInListview = manager.findFirstVisibleItemPosition()
    }


    private fun setCreateEventTimeRecyclerView(list: ArrayList<MyHostEventModel.HostEvents>) {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        if (isPublished) {
            binding!!.rvPublishedEvent.layoutAnimation = animation
            binding!!.rvPublishedEvent.layoutManager = manager
            binding!!.rvPublishedEvent.setEmptyView(binding!!.tvNoData)
            adapterMyHostEvent = AdapterMyHostEvent(HooleyMain.activity!!, sortListCreateTime(list), true)
            adapterMyHostEvent!!.mListener = this
            binding!!.rvPublishedEvent.adapter = adapterMyHostEvent
            firstVisibleInListview = manager.findFirstVisibleItemPosition()
        } else {
            binding!!.rvPublishedEvent.layoutAnimation = animation
            binding!!.rvUnpublishedEvent.layoutManager = manager
            binding!!.rvUnpublishedEvent.setEmptyView(binding!!.tvNoData)
            adapterMyHostEvent = AdapterMyHostEvent(HooleyMain.activity!!, sortListCreateTime(list), false)
            adapterMyHostEvent!!.mListener = this
            binding!!.rvUnpublishedEvent.adapter = adapterMyHostEvent

        }
    }

    private fun setStartEventTimeRecyclerView(list: ArrayList<MyHostEventModel.HostEvents>) {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        if (isPublished) {
            binding!!.rvPublishedEvent.layoutAnimation = animation
            binding!!.rvPublishedEvent.layoutManager = manager
            binding!!.rvPublishedEvent.setEmptyView(binding!!.tvNoData)
            adapterMyHostEvent = AdapterMyHostEvent(HooleyMain.activity!!, sortListStartTime(list), true)
            adapterMyHostEvent!!.mListener = this
            binding!!.rvPublishedEvent.adapter = adapterMyHostEvent
            firstVisibleInListview = manager.findFirstVisibleItemPosition()
        } else {
            binding!!.rvPublishedEvent.layoutAnimation = animation
            binding!!.rvUnpublishedEvent.layoutManager = manager
            binding!!.rvUnpublishedEvent.setEmptyView(binding!!.tvNoData)
            adapterMyHostEvent = AdapterMyHostEvent(HooleyMain.activity!!, sortListStartTime(list), false)
            adapterMyHostEvent!!.mListener = this
            binding!!.rvUnpublishedEvent.adapter = adapterMyHostEvent

        }
    }


    private fun sortListCreateTime(list: ArrayList<MyHostEventModel.HostEvents>): List<MyHostEventModel.HostEvents>? {
        Collections.sort(list, object : Comparator<MyHostEventModel.HostEvents> {
            override fun compare(teamMember1: MyHostEventModel.HostEvents, teamMember2: MyHostEventModel.HostEvents): Int {
                return teamMember1.createdDate.compareTo(teamMember2.createdDate)
            }
        })
        return list
    }

    private fun sortListStartTime(list: ArrayList<MyHostEventModel.HostEvents>): List<MyHostEventModel.HostEvents>? {
        Collections.sort(list, object : Comparator<MyHostEventModel.HostEvents> {
            override fun compare(teamMember1: MyHostEventModel.HostEvents, teamMember2: MyHostEventModel.HostEvents): Int {
                return teamMember1.startTime!!.compareTo(teamMember2.startTime!!)
            }
        })
        return list
    }


    fun sortList(list: ArrayList<MyHostEventModel.HostEvents>): ArrayList<MyHostEventModel.HostEvents> {
        Collections.sort(list, object : Comparator<MyHostEventModel.HostEvents> {
            override fun compare(teamMember1: MyHostEventModel.HostEvents, teamMember2: MyHostEventModel.HostEvents): Int {
                return teamMember1.eventName.compareTo(teamMember2.eventName)
            }
        })
        return list
    }


    private fun setUnPublishEventRecyclerView() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        binding!!.rvPublishedEvent.layoutAnimation = animation
        binding!!.rvUnpublishedEvent.layoutManager = manager
        binding!!.rvUnpublishedEvent.setEmptyView(binding!!.tvNoData)
        adapterMyHostEvent = AdapterMyHostEvent(HooleyMain.activity!!, sortList(unPublishEventList!!), false)
        adapterMyHostEvent!!.mListener = this
        binding!!.rvUnpublishedEvent.adapter = adapterMyHostEvent
    }

    private fun getMyHostingEvent(showDialog: Boolean) {
        if (showDialog && binding != null && !isSwiped)
            binding!!.pbHostingEvents.visibility = View.VISIBLE

        service.getMyHostEvents(object : IWebServiceCallback<MyHostEventModel> {
            override fun success(result: MyHostEventModel) {
                model = result
                if (isSwiped) {
                    binding!!.slHosting.isRefreshing = false
                    isSwiped = false
                }
                val strCat = gson.toJson(result)
                HooleyApp.db.putString(Constants.RESPONSE_GSON_MY_HOST_EVENT, strCat)
                binding!!.pbHostingEvents.visibility = View.GONE
                @Suppress("SENSELESS_COMPARISON")
                if (model.publishEventsArrayList != null && model.publishEventsArrayList.size > 0 && isPublished) {
                    publishEventList = ArrayList<MyHostEventModel.HostEvents>()
                    publishEventList!!.addAll(result.publishEventsArrayList)
                    setPublishEventRecyclerView()
                }
                @Suppress("SENSELESS_COMPARISON")
                if (model.unPublishEventsArrayList != null && model.unPublishEventsArrayList.size > 0 && isUnPublished) {
                    unPublishEventList = ArrayList<MyHostEventModel.HostEvents>()
                    unPublishEventList!!.addAll(result.unPublishEventsArrayList)
                    setUnPublishEventRecyclerView()
                }
                if (isPublished && result.publishEventsArrayList.size == 0) {
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.tvNoDataUnPublished.visibility = View.GONE
                    binding!!.tvNoData.text = "No published events found"
                }
                if (isUnPublished && result.unPublishEventsArrayList.size == 0) {
                    binding!!.tvNoData.visibility = View.GONE
                    binding!!.tvNoDataUnPublished.visibility = View.VISIBLE
                    binding!!.tvNoDataUnPublished.text = "No unpublished events found"

                }

                if (isPublished && result.publishEventsArrayList.size == 0 && result.unPublishEventsArrayList.size == 0) {
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.tvNoDataUnPublished.visibility = View.GONE
                    binding!!.tvNoData.text = "No published events found"
                }
                if (isUnPublished && result.publishEventsArrayList.size == 0 && result.unPublishEventsArrayList.size == 0) {
                    binding!!.tvNoDataUnPublished.visibility = View.VISIBLE
                    binding!!.tvNoData.visibility = View.GONE
                    binding!!.tvNoDataUnPublished.text = "No unpublished events found"
                }
            }

            override fun failure(message: String) {
                if (isSwiped) {
                    binding!!.slHosting.isRefreshing = false
                    isSwiped = false
                }
                binding!!.pbHostingEvents.visibility = View.GONE
                if (message != "No events found") {
                    binding!!.tvNoData.visibility = View.VISIBLE
                    if (isPublished) {
                        binding!!.rvPublishedEvent.visibility = View.GONE
                        binding!!.tvNoData.text = message
                        binding!!.tvNoData.visibility = View.VISIBLE
                        binding!!.tvNoDataUnPublished.visibility = View.GONE
                    }
                    if (isUnPublished) {
                        binding!!.rvUnpublishedEvent.visibility = View.GONE
                        binding!!.tvNoDataUnPublished.visibility = View.VISIBLE
                        binding!!.tvNoData.visibility = View.GONE
                        binding!!.tvNoDataUnPublished.text = message
                    }
                } else {
                    binding!!.tvNoData.visibility = View.VISIBLE
                    if (isPublished) {
                        binding!!.rvPublishedEvent.visibility = View.GONE
                        binding!!.tvNoData.text = "No published events found"
                        binding!!.tvNoData.visibility = View.VISIBLE
                        binding!!.tvNoDataUnPublished.visibility = View.GONE
                    }
                    if (isUnPublished) {
                        binding!!.rvUnpublishedEvent.visibility = View.GONE
                        binding!!.tvNoDataUnPublished.visibility = View.VISIBLE
                        binding!!.tvNoData.visibility = View.GONE
                        binding!!.tvNoDataUnPublished.text = "No unpublished events found"
                    }
                }
            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })

    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        when (checkedId) {
            R.id.rbPublishedEvent -> {
                binding!!.rbPublishedEvent.typeface = font1
                binding!!.rbUnPublishedEvent.typeface = font2
                binding!!.rbPublishedEvent.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding!!.rbUnPublishedEvent.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding!!.rbPublishedEvent.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_left_radio)
                binding!!.rgTabs.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_radio_group)
                binding!!.rbUnPublishedEvent.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.base_color))
                binding!!.rvPublishedEvent.visibility = View.VISIBLE
                binding!!.rvUnpublishedEvent.visibility = View.GONE
                binding!!.tvNoDataUnPublished.visibility = View.GONE
                isPublished = true
                isUnPublished = false
                getMyHostingEvent(true)
            }
            R.id.rbUnPublishedEvent -> {
                binding!!.rgTabs.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_radio_group)
                binding!!.rbPublishedEvent.typeface = font2
                binding!!.rbUnPublishedEvent.typeface = font1
                binding!!.rbUnPublishedEvent.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding!!.rbPublishedEvent.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding!!.rbUnPublishedEvent.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_right_radio)
                binding!!.rbPublishedEvent.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.base_color))
                binding!!.rvPublishedEvent.visibility = View.GONE
                binding!!.rvUnpublishedEvent.visibility = View.VISIBLE
                binding!!.tvNoData.visibility = View.GONE
                isPublished = false
                isUnPublished = true
                getMyHostingEvent(true)
            }
        }
    }

    override fun onRefresh() {
        if (binding!!.slHosting != null) {
            binding!!.slHosting.isRefreshing = true
            binding!!.pbHostingEvents.visibility = View.GONE
        }
        isSwiped = true
        getMyHostingEvent(true)
    }


    private fun setMoreCallerBackListener(hostEvents: MyHostEventModel.HostEvents) {
        if (filterList.size > 0) {
            filterList.clear()
        }
        var eventAlertList: AdapterMoreMediaDialoge.MoreItem
        if (isPublished) {
            eventAlertList = if (hostEvents.isAlertOn) {
                AdapterMoreMediaDialoge.MoreItem("Edit Event Alert", false)
            } else {
                AdapterMoreMediaDialoge.MoreItem("Set Event Alert", false)
            }
//            val itemEditEvent = AdapterMoreMediaDialoge.MoreItem("Edit Event", false)
            filterList.add(eventAlertList)
//            filterList.add(itemEditEvent)
        } else {
            val itemEditEvent = AdapterMoreMediaDialoge.MoreItem("Edit Event", false)
            filterList.add(itemEditEvent)
        }

        moreFilterDialogeFragment = MoreMediaDialogFragment.Builder(HooleyMain.activity!!).setTitle("Action").setType(Constants.TYPE_MORE_80).setList(filterList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    setEventAlert(hostEvents)
                    moreFilterDialogeFragment!!.dismiss()
                }
            }
        }.create()

        if (!moreFilterDialogeFragment!!.isAdded)
            moreFilterDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
    }


    fun showCustomDialog(eventId: String, startTime: String, endTime: String, alertTime: String) {
        var setTime = ""
        customDialog = CustomDateTimePicker(HooleyMain.activity!!,
                object : CustomDateTimePicker.ICustomDateTimeListener {
                    override fun onSet(dialog: Dialog, calendarSelected: Calendar,
                                       dateSelected: Date, year: Int, monthFullName: String,
                                       monthShortName: String, monthNumber: Int, date: Int,
                                       weekDayFullName: String, weekDayShortName: String,
                                       hour24: Int, hour12: Int, min: Int, sec: Int,
                                       AM_PM: String) {

                        if (dateSelected.before(Calendar.getInstance().time)) {
                            Util.showToastMessage(HooleyMain.activity!!, "Invalid date, Please select future date.")
                            return
                        }
                        if (dateSelected.after(DateUtils.stringToDate(endTime))) {
                            Util.showToastMessage(HooleyMain.activity!!, "Invalid date, date/time is greater than event end date and time")
                            return
                        }

                        setTime = (String.format("%04d-%02d-%02d", year, monthNumber + 1, calendarSelected.get(Calendar.DAY_OF_MONTH))
                                + " " + String.format("%02d:%02d", hour12, min) +
                                " " + AM_PM)

                        setEventAlert(eventId, setTime)

                    }

                    override fun onCancel() {

                    }
                })
        /**
         * Pass Directly current time format it will return AM and PM if you set
         * false
         */
        customDialog!!.set24HourFormat(false)
        /**
         * Pass Directly current data and time to show when it pop up
         */
        if (TextUtils.isEmpty(alertTime)) {
            customDialog!!.setDate(Calendar.getInstance())
        } else {
            customDialog!!.setDate(DateUtils.stringToDate(DateUtils.SendDateToDate(alertTime)))
        }
        customDialog!!.showDialog()
    }

    private fun setEventAlert(eventId: String, date: String) {
        service.setEventAlert(Integer.parseInt(eventId), date, true, object : GenericCallback<SetAlertModel> {
            override fun success(result: SetAlertModel) {
                if (isPublished) {
                    for (i in publishEventList!!.indices) {
                        if (publishEventList!![i].id == eventId) {
                            publishEventList!![i].isAlertOn = true
                            publishEventList!![i].alertDate = DateUtils.SendDateToDate(date)
                            break
                        }
                    }
                } else {
                    for (i in unPublishEventList!!.indices) {
                        if (unPublishEventList!![i].id == eventId) {
                            unPublishEventList!![i].isAlertOn = true
                            unPublishEventList!![i].alertDate = DateUtils.SendDateToDate(date)
                            break
                        }
                    }
                }
//                context.showToastMessage("Alert added")
                HooleyMain.activity!!.showSuccessDialog("Alert added")
                adapterMyHostEvent!!.notifyDataSetChanged()
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }

    private fun setEventAlert(hostEvents: MyHostEventModel.HostEvents) {
        if (hostEvents.isAlertOn) {
            showCustomDialog(hostEvents.id!!, hostEvents.startTime!!, hostEvents.endTime!!, hostEvents.alertDate!!)
        } else {
            showCustomDialog(hostEvents.id!!, hostEvents.startTime!!, hostEvents.endTime!!, "")
        }
    }

    override fun onClickAlert(position: Int) {
        if (isPublished) {
            if (publishEventList!![position].isAlertOn) {
                showCustomDialog(publishEventList!![position].id!!, publishEventList!![position].startTime!!, publishEventList!![position].endTime!!, publishEventList!![position].alertDate!!)
            } else {
                showCustomDialog(publishEventList!![position].id!!, publishEventList!![position].startTime!!, publishEventList!![position].endTime!!, "")
            }
        } else {
            if (unPublishEventList!![position].isAlertOn) {
                showCustomDialog(unPublishEventList!![position].id!!, unPublishEventList!![position].startTime!!, unPublishEventList!![position].endTime!!, unPublishEventList!![position].alertDate!!)
            } else {
                showCustomDialog(unPublishEventList!![position].id!!, unPublishEventList!![position].startTime!!, unPublishEventList!![position].endTime!!, "")
            }
        }
    }

    override fun onClickEdit(position: Int) {
//        if (isPublished) {
//            HooleyApp.db.putBoolean(Constants.IS_COMMENT_CLICKED, false)
//            HooleyApp.db.putBoolean(Constants.IS_PUBLISH_EVENT, isPublished)
//            (HooleyMain.activity as ActivityBase).callFragment(R.id.container, UpdateEventFragment.newInstance(publishEventList!![position].id!!), "updateEventFragment")
//        } else {
//            HooleyApp.db.putBoolean(Constants.IS_COMMENT_CLICKED, false)
//            HooleyApp.db.putBoolean(Constants.IS_PUBLISH_EVENT, isPublished)
//            (HooleyMain.activity as ActivityBase).callFragment(R.id.container, UpdateEventFragment.newInstance(unPublishEventList!![position].id!!), "updateEventFragment")
//
//        }
    }

    override fun onClickItem(position: Int) {
        if (isPublished) {
            HooleyApp.db.putBoolean(Constants.IS_COMMENT_CLICKED, false)
            HooleyApp.db.putBoolean(Constants.IS_PUBLISH_EVENT, isPublished)
            (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment
                    .newInstance(publishEventList!![position].id!!), "LiveEventFragment")
        } else {
            HooleyApp.db.putBoolean(Constants.IS_COMMENT_CLICKED, false)
            HooleyApp.db.putBoolean(Constants.IS_PUBLISH_EVENT, isPublished)
            (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment
                    .newInstance(unPublishEventList!![position].id!!), "LiveEventFragment")
        }
    }

    override fun onLongClick(position: Int) {
        if (isPublished) {
            setMoreCallerBackListener(publishEventList!![position])
        }
    }

    companion object {

        var moreFilterDialogeFragment: MoreMediaDialogFragment? = null

    }


}
