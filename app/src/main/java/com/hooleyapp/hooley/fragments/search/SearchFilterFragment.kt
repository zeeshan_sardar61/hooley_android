package com.hooleyapp.hooley.fragments.search

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.VPDashBoardAdapter
import com.hooleyapp.hooley.databinding.FragmentSearchFilterBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.hideKeyboard
import com.hooleyapp.hooley.services.EventWebService

/**
 * Created by Nauman on 4/4/2018.
 */

class SearchFilterFragment : BaseFragment(), View.OnClickListener, RadioGroup.OnCheckedChangeListener {


    var eventService = EventWebService()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_filter, container, false)

        HooleyMain.activity!!.hideKeyboard()
        setupViewPager()

        return mainBinding.root
    }

    private lateinit var pageAdapter: VPDashBoardAdapter

    private fun setupViewPager() {
        pageAdapter = VPDashBoardAdapter(childFragmentManager)
        pageAdapter.addFragment(SearchFragment(), "SearchFragment")
        pageAdapter.addFragment(UserSearchFragment(), "UserSearchFragment")
        mainBinding.vPager.adapter = pageAdapter
        mainBinding.vPager.currentItem = 0

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
    }


    private fun setListener() {
        mainBinding.ivSearchFilter.setOnClickListener(this)
        mainBinding.ivSearchFilterFill.setOnClickListener(this)
        mainBinding.rbHooleyEvents.setOnClickListener(this)
        mainBinding.rbHooleyUsers.setOnClickListener(this)
        mainBinding.rgGuestList.setOnCheckedChangeListener(this)

    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivSearchFilter -> {
                mainBinding.ivSearchFilterFill.visibility = View.VISIBLE
                mainBinding.llFilter.visibility = View.VISIBLE
                mainBinding.ivSearchFilter.visibility = View.GONE
            }
            R.id.ivSearchFilterFill -> {
                mainBinding.ivSearchFilter.visibility = View.VISIBLE
                mainBinding.ivSearchFilterFill.visibility = View.GONE
                mainBinding.llFilter.visibility = View.GONE
            }
        }
    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        when (checkedId) {
            R.id.rbHooleyEvents -> {
                mainBinding.edtSearch.hint = "Enter Address/Zip/City/Host/Venue"
                setTabs(true)
                mainBinding.vPager.currentItem = 0
            }
            R.id.rbHooleyUsers -> {
                mainBinding.edtSearch.hint = ""
                setTabs(false)
                mainBinding.vPager.currentItem = 1
            }
        }
    }


    private fun setTabs(value: Boolean) {
        if (value) {
            mainBinding.rbHooleyEvents.typeface = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)
            mainBinding.rbHooleyUsers.typeface = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)
            mainBinding.rbHooleyEvents.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
            mainBinding.rbHooleyEvents.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_left_radio)
            mainBinding.rbHooleyUsers.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
            mainBinding.rbHooleyUsers.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
        } else {
            mainBinding.rbHooleyEvents.typeface = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)
            mainBinding.rbHooleyUsers.typeface = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)
            mainBinding.rbHooleyEvents.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
            mainBinding.rbHooleyEvents.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
            mainBinding.rbHooleyUsers.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
            mainBinding.rbHooleyUsers.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_right_radio)
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        HooleyMain.activity!!.hideKeyboard()
    }

    companion object {

        lateinit var instance: SearchFilterFragment
        lateinit var mainBinding: FragmentSearchFilterBinding

        fun newInstance(): SearchFilterFragment {
            instance = SearchFilterFragment()
            return instance
        }
    }

}
