package com.hooleyapp.hooley.fragments.ticket

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.apis.ApiRequestJson
import com.hooleyapp.hooley.databinding.FragmentEventTicketSetupBinding
import com.hooleyapp.hooley.db.DbManager
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.event.AddIncomeSharePartnerFragment
import com.hooleyapp.hooley.fragments.event.ConfirmCreateEventFragment
import com.hooleyapp.hooley.fragments.event.CreateEventFragment
import com.hooleyapp.hooley.fragments.stripe.StripeConnectFragment
import com.hooleyapp.hooley.helper.hideKeyboard
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.*
import com.hooleyapp.hooley.model.staticData.Category
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.requestmodel.SaveTicketRequestModel
import com.hooleyapp.hooley.requestmodel.UpdateEventRequestModel
import com.hooleyapp.hooley.services.EventWebService
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.services.ProfileWebService
import com.hooleyapp.hooley.services.TicketWebService
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

/**
 * Created by Zeeshan on 02-Mar-18.
 */

class EventTicketSetupFragment : BaseFragment(), View.OnClickListener {
    var webService = ProfileWebService()
    var ticketWebService = TicketWebService()
    var eventWebService = EventWebService()
    internal lateinit var binding: FragmentEventTicketSetupBinding
    private var availableIncome = 98
    private var friendList: MutableList<FriendsTable>? = null
    private var isAddeds: Boolean = true
    private val REQUEST_CODE_STRIPE = 965
    private val REQUEST_ADD_INCOME_SHARE = 966
    private var shareIncomePartnerList: ArrayList<ShareIncomePartnerModel> = ArrayList()
    private val ticketCategoriesList = ArrayList<TicketCategoriesModel>()
    private var stripeConnectFragment: StripeConnectFragment? = null
    private var code: String? = null
    private val service = FriendWebService()
    private var stripeDialog: Dialog? = null
    private var addIncomeShareFragment: AddIncomeSharePartnerFragment? = null

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            if (view != null) {
                Util.hideKeyboard(HooleyMain.activity!!)
            }
        }

    }

    fun setToolbar() {
        if (eventType == "2") {
            (HooleyMain.activity as HooleyMain).setToolbarTitle("Setup Tickets & PPV")
        } else {
            (HooleyMain.activity as HooleyMain).setToolbarTitle("Setup Donation")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_event_ticket_setup, container, false)
        setHasOptionsMenu(true)
        setListener()
        getFriendList()
        getTermsAndConditions()
        binding.tvIncomePercentage.text = "$availableIncome%"
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu?.clear()
    }

    private fun getTermsAndConditions() {
        ticketWebService.getTicketTerms(object : GenericCallback<GetTermsConditionModel> {
            override fun success(result: GetTermsConditionModel) {
                if (!TextUtils.isEmpty(result.stripeEmail)) {
                    binding.tvSetup.text = "Stripe Connected"
                    binding.tvStripConnectedEmail.visibility = View.VISIBLE
                    binding.tvStripConnectedEmail.text = result.stripeEmail
                    HooleyApp.db.putBoolean(Constants.IS_STRIPE_CONNECTED, true)
                    binding.ivStripeArrow.visibility = View.GONE
                } else {
                    HooleyApp.db.putBoolean(Constants.IS_STRIPE_CONNECTED, false)
                    binding.tvSetup.text = "Setup Hooley Wallet"
                    binding.ivStripeArrow.visibility = View.VISIBLE
                    binding.tvStripConnectedEmail.visibility = View.GONE
                }
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun setupHolleyWalletDialog(heading: String, content: String) {
        stripeDialog = Dialog(HooleyMain.activity!!)
        stripeDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        stripeDialog!!.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        stripeDialog!!.setContentView(R.layout.dialog_hooley_wallet)
        val tvHeading = stripeDialog!!.findViewById<TextView>(R.id.tvHeading)
        val tvContent = stripeDialog!!.findViewById<TextView>(R.id.tvContent)
        val ivClose = stripeDialog!!.findViewById<ImageView>(R.id.ivClose)
        tvHeading.text = heading
        tvContent.text = content
        ivClose.setOnClickListener { stripeDialog!!.dismiss() }

    }

    private fun setListener() {
        binding.rlAddCategoryBtn.setOnClickListener(this)
        binding.rlEditableButtons.setOnClickListener(this)
        binding.btnSaveTicket.setOnClickListener(this)
        binding.rlStripe.setOnClickListener(this)
        binding.ivShareInfo.setOnClickListener(this)
        binding.ivInfoHooleyWallet.setOnClickListener(this)
        binding.ivShareRevenue.setOnClickListener(this)
        binding.tvTitle.setOnClickListener(this)
        binding.tbSetupCheck.setOnClickListener(this)
        binding.tbPpvCheck.setOnClickListener(this)
        binding.tbTicketRefundCheck.setOnClickListener(this)

        if (eventType == "3") {
            binding.rlCharity.visibility = View.VISIBLE
            binding.rlPPV.visibility = View.GONE
            binding.bppv.visibility = View.GONE
            binding.bppv1.visibility = View.GONE
        }

        setToolbar()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rlAddCategoryBtn -> addChild(binding.llCategories)
            R.id.ivShareRevenue -> setUpShareIncomeList()
            R.id.tvTitle -> setUpShareIncomeList()
            R.id.rlEditableButtons -> showRemove(isAddeds)
            R.id.tbSetupCheck -> {
                if (binding.tbSetupCheck.isChecked) {
                    binding.llTSetup.visibility = View.VISIBLE
                    binding.tbSetupCheck.isChecked = true
                } else {
                    binding.llTSetup.visibility = View.GONE
                    binding.tbSetupCheck.isChecked = false
                }
            }
            R.id.tbPpvCheck -> {
                if (binding.tbPpvCheck.isChecked) {
                    binding.rlPpvEt.visibility = View.VISIBLE
                    binding.tbPpvCheck.isChecked = true
                } else {
                    binding.rlPpvEt.visibility = View.GONE
                    binding.tbPpvCheck.isChecked = false
                }
            }
            R.id.tbTicketRefundCheck -> {
                binding.tbTicketRefundCheck.isChecked = binding.tbTicketRefundCheck.isChecked
            }
            R.id.btnSaveTicket -> {
                HooleyApp.db.putInt(Constants.SHOW_LIVE_EVENT_TAB, 2)
                if (eventType == "3") {
                    if (validateInput()) {
                        binding.btnSaveTicket.isEnabled = false
                        binding.btnSaveTicket.isClickable = false
                        HooleyMain.activity!!.hideKeyboard()
                        if (isUpdate != null) {
                            saveFreeToPaidEvent()
                            saveTicket(shareIncomePartnerList, ticketCategoriesList, "true")
                        } else {
                            saveTicket(shareIncomePartnerList, ticketCategoriesList, "false")
                        }


                    }
                } else {
                    if (validateInput()) {
                        binding.btnSaveTicket.isEnabled = false
                        binding.btnSaveTicket.isClickable = false
                        HooleyMain.activity!!.hideKeyboard()
                        if (isUpdate != null) {
                            saveFreeToPaidEvent()
                            saveTicket(shareIncomePartnerList, ticketCategoriesList, "true")
                        } else {
                            saveTicket(shareIncomePartnerList, ticketCategoriesList, "false")
                        }
                    }
                }
            }
            R.id.rlStripe -> {
                HooleyMain.activity!!.hideKeyboard()
                stripeConnectFragment = StripeConnectFragment.newInstance()
                stripeConnectFragment!!.setTargetFragment(this@EventTicketSetupFragment, REQUEST_CODE_STRIPE)
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, stripeConnectFragment!!, "stripeConnectFragment")
            }
            R.id.ivInfoHooleyWallet -> {
                setupHolleyWalletDialog("Setup Hooley Wallet", getString(R.string.str_setup_holley_wallet))
                stripeDialog!!.show()
            }
            R.id.ivShareInfo -> {
                setupHolleyWalletDialog("Share Income", getString(R.string.str_share_income))
                stripeDialog!!.show()
            }
        }
    }

    private fun saveFreeToPaidEvent() {
        var hostList = ArrayList<FriendsTable>()
        var coHOstList = ArrayList<FriendsTable>()
        var catList = ArrayList<Category>()
        var geofenceList = ArrayList<GeoFenceModel>()

        if (editEventModel!!.eventHostProfileList.size == 0) {
            val table = FriendsTable()
            table.userId = Integer.parseInt(HooleyApp.db.getString(Constants.USER_ID))
            hostList.add(table)
        }

        if (editEventModel!!.eventGeofenceList.size > 0) {
            for (i in editEventModel!!.eventGeofenceList.indices) {
                val model = GeoFenceModel()
                model.placeLat = editEventModel!!.eventGeofenceList[i].latitude!!.toDouble()
                model.placeLong = editEventModel!!.eventGeofenceList[i].longitude!!.toDouble()
                model.raduis = editEventModel!!.eventGeofenceList[i].radius!!.toString()
                model.address = (HooleyMain.activity as ActivityBase).getCompleteAddressString(model.placeLat, model.placeLong)
                geofenceList.add(model)
            }
        }

        if (editEventModel!!.eventCategoryList.size > 0) {
            for (i in editEventModel!!.eventCategoryList.indices) {
                val category = Category()
                category.id = Integer.parseInt(editEventModel!!.eventCategoryList[i].Id!!)
                category.name = editEventModel!!.eventCategoryList[i].Name!!
                category.iconUrl = editEventModel!!.eventCategoryList[i].icon!!
                category.isActive = editEventModel!!.eventCategoryList[i].isAdded
                catList.add(category)
            }
        }

        if (editEventModel!!.eventCoHostProfileList.size > 0)
            for (i in editEventModel!!.eventCoHostProfileList.indices) {
                coHOstList.add(editEventModel!!.eventCoHostProfileList[i])
            }


        json = ApiRequestJson.getInstance().saveEventDataString(eventCoverList!!,
                eventId!!,
                editEventModel!!.eventBasicInfo!!.eventName!!,
                editEventModel!!.eventBasicInfo!!.eventType!!,
                editEventModel!!.eventBasicInfo!!.eventDetails!!,
                editEventModel!!.eventBasicInfo!!.eventHashs,
                editEventModel!!.eventBasicInfo!!.eventStartTime!!,
                editEventModel!!.eventBasicInfo!!.eventEndTime!!,
                !editEventModel!!.eventBasicInfo!!.isPrivate,
                true,
                null,
                coHOstList,
                catList,
                geofenceList,
                editEventModel!!.eventBasicInfo!!.minimumAgeId!!)

    }

    private fun getFriendList() {
        if (friendList == null)
            friendList = ArrayList()
        if (friendList!!.size > 0)
            friendList!!.clear()
        friendList = DbManager.getInstance().friendList
        if (friendList!!.size > 0) {
            if (friendList != null && friendList!!.size > 0) {
                if (shareIncomePartnerList.size > 0)
                    shareIncomePartnerList.clear()
                for (i in friendList!!.indices) {
                    shareIncomePartnerList.add(ShareIncomePartnerModel(friendList!![i], 0))
                }
            }
        } else {
            getFriends(false)
        }
    }

    private fun setUpShareIncomeList() {
        addIncomeShareFragment = AddIncomeSharePartnerFragment.newInstance(shareIncomePartnerList)
        addIncomeShareFragment!!.setTargetFragment(this@EventTicketSetupFragment, REQUEST_ADD_INCOME_SHARE)
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, addIncomeShareFragment!!, "AddIncomeSharePartnerFragment")
    }

    private fun getFriends(showDialog: Boolean) {
        if (showDialog) {
            HooleyMain.activity!!.showDialog()
        }
        service.getMyFriendsList(object : GenericCallback<FriendsModel> {
            override fun success(result: FriendsModel) {
                HooleyMain.activity!!.removeDialog()
                if (showDialog) {
                    if (friendList == null)
                        friendList = ArrayList()
                    friendList!!.addAll(result.friendsList)
                } else {
                    if (friendList!!.size != result.friendsList.size) {
                        friendList!!.clear()
                        friendList!!.addAll(result.friendsList)
                        DbManager.getInstance().friendList = friendList
                    }
                }
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    //                    Util.showToastMessage(HooleyMain.Companion.getActivity(), message);
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }


    fun validate(v: View): Boolean {
        if (Util.isContentNull((v.findViewById<View>(R.id.edtTicketName) as com.hooleyapp.hooley.helper.TypeFaceEditText).text.toString())) {
            v.findViewById<View>(R.id.edtTicketName).requestFocus()
            HooleyMain.activity!!.showSuccessDialog(getString(R.string.cat_name))
            return false
        } else if (Util.isContentNull((v.findViewById<View>(R.id.edtQuantity) as com.hooleyapp.hooley.helper.TypeFaceEditText).text.toString())) {
            v.findViewById<View>(R.id.edtQuantity).requestFocus()
            HooleyMain.activity!!.showSuccessDialog(getString(R.string.quantity))
            return false
        } else if (Util.isContentNull((v.findViewById<View>(R.id.edtPrice) as com.hooleyapp.hooley.helper.TypeFaceEditText).text.toString())) {
            v.findViewById<View>(R.id.edtPrice).requestFocus()
            HooleyMain.activity!!.showSuccessDialog(getString(R.string.price))
            return false
        } else {
            return true
        }
    }

    fun addChild(layout: LinearLayout) {
        val count = layout.childCount
        if (count > 0) {
            val prevChild = layout.getChildAt(count - 1)
            if (validate(prevChild)) {
                val child = HooleyMain.activity!!.layoutInflater.inflate(R.layout.ticket_category_child_item, null)
                child.findViewById<View>(R.id.ivClose).setOnClickListener {
                    layout.removeView(child)
                    if (layout.childCount == 0) {
                        showRemove(false)
                    }
                }
                layout.addView(child)
            }
        } else {
            val child = HooleyMain.activity!!.layoutInflater.inflate(R.layout.ticket_category_child_item, null)
            child.findViewById<View>(R.id.ivClose).setOnClickListener {
                layout.removeView(child)
                if (layout.childCount == 0) {
                    showRemove(false)
                }
            }
            layout.addView(child)
        }

    }

    fun addAddedPartnerChild(model: ShareIncomePartnerModel) {
        binding.llContainer.visibility = View.VISIBLE
        val child = HooleyMain.activity!!.layoutInflater.inflate(R.layout.added_partner_ichild_item, null)
        (child.findViewById<View>(R.id.tvName) as TextView).text = model.friendsTable.fullName
        (child.findViewById<View>(R.id.tvPercentage) as TextView).text = model.percent.toString() + "%"
        if (!TextUtils.isEmpty(model.friendsTable.profilePic))
            Glide.with(HooleyMain.activity!!).load(model.friendsTable.profilePic).into(child.findViewById<View>(R.id.ivPartnerAdded) as ImageView)
        child.findViewById<View>(R.id.ivCancel).setOnClickListener {
            availableIncome += model.percent
            binding.tvIncomePercentage.text = "$availableIncome%"
            binding.llContainer.removeView(child)
            model.percent = 0
        }

        binding.llContainer.addView(child)
    }

    private fun showRemove(check: Boolean) {
        val childCount = binding.llCategories.childCount
        for (i in 0 until childCount) {
            val child = binding.llCategories.getChildAt(i)
            if (check) {
                if (validate(child)) {
                    child.findViewById<View>(R.id.ivClose).visibility = View.VISIBLE
                    child.findViewById<View>(R.id.edtTicketName).isEnabled = false
                    child.findViewById<View>(R.id.edtPrice).isEnabled = false
                    if (HooleyApp.db.getInt(Constants.TYPE_THEME) == 0) {
                        child.findViewById<View>(R.id.edtTicketName).background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_edt_transparent)
                        child.findViewById<View>(R.id.edtQuantity).background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_edt_transparent)
                        child.findViewById<View>(R.id.edtPrice).background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_edt_transparent)
                    }
                    child.findViewById<View>(R.id.edtQuantity).isEnabled = false
                    child.findViewById<View>(R.id.edtTicketName).alpha = .5f
                    child.findViewById<View>(R.id.edtQuantity).alpha = .5f
                    child.findViewById<View>(R.id.edtPrice).alpha = .5f
                    binding.ivAddCategories.setAlpha(100)
                    binding.rlAddCategoryBtn.isClickable = false
                    binding.tvEdit.text = "Save"
                }

            } else {
                child.findViewById<View>(R.id.ivClose).visibility = View.INVISIBLE
                child.findViewById<View>(R.id.edtTicketName).isEnabled = true
                child.findViewById<View>(R.id.edtQuantity).isEnabled = true
                child.findViewById<View>(R.id.edtTicketName).alpha = 1f
                child.findViewById<View>(R.id.edtTicketName).background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_save_transparent)
                child.findViewById<View>(R.id.edtQuantity).background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_save_transparent)
                child.findViewById<View>(R.id.edtPrice).background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_save_transparent)
                child.findViewById<View>(R.id.edtQuantity).alpha = 1f
                child.findViewById<View>(R.id.edtPrice).alpha = 1f
                binding.ivAddCategories.setAlpha(255)
                binding.rlAddCategoryBtn.isClickable = true
                binding.tvEdit.text = "Edit"
            }
        }
        if (childCount == 0) {
            binding.ivAddCategories.setAlpha(255)
            binding.rlAddCategoryBtn.isClickable = true
            binding.tvEdit.text = "Edit"
        }
        isAddeds = !check
    }


    private fun validateInput(): Boolean {
        if (!HooleyApp.db.getBoolean(Constants.IS_STRIPE_CONNECTED)) {
            HooleyMain.activity!!.showSuccessDialog("Your Stripe account has not been created. Please connect to Stripe and create your account before setting up tickets.")
            return false
        }

        if (binding.rlCharity.visibility == View.VISIBLE) {
            if (binding.edtCharityPrice.text.toString().isEmpty()) {
                Util.showToastMessage(HooleyMain.activity!!, "Please set charity target")
                return false
            } else {
                if (binding.edtCharityPrice.text.toString() == "0") {
                    HooleyMain.activity!!.showSuccessDialog("Please add valid charity target")
                    return false
                }
            }
        } else {
            if (!binding.tbSetupCheck.isChecked && !binding.tbPpvCheck.isChecked) {
                Util.showToastMessage(HooleyMain.activity!!, "Please Setup tickets or PPV")
                return false
            }
            if (binding.tbPpvCheck.isChecked) {
                if (binding.edtPpvPrice.text.toString().isEmpty()) {
                    binding.edtPpvPrice.requestFocus()
                    Util.showToastMessage(HooleyMain.activity!!, "Please add ppv price")
                    return false
                } else {
                    if (binding.edtPpvPrice.text.toString() == "0") {
                        HooleyMain.activity!!.showSuccessDialog("Please add valid ppv price")
                        return false
                    }
                }
            }
        }


        if (binding.tbSetupCheck.isChecked)
            if (binding.llCategories.childCount == 0) {
                Util.showToastMessage(HooleyMain.activity!!, "Please add at least one Ticket Category")
                return false
            }


        ticketCategoriesList.clear()
        val childCount = binding.llCategories.childCount
        for (i in 0 until childCount) {
            val child = binding.llCategories.getChildAt(i)
            if (Util.isContentNull((child.findViewById<View>(R.id.edtTicketName) as EditText).text.toString())) {
                child.findViewById<View>(R.id.edtTicketName).requestFocus()
                HooleyMain.activity!!.showSuccessDialog("Provide All Detail")
                return false
            } else if (Util.isContentNull((child.findViewById<View>(R.id.edtQuantity) as EditText).text.toString())) {
                child.findViewById<View>(R.id.edtQuantity).requestFocus()
                HooleyMain.activity!!.showSuccessDialog("Provide All Detail")
                return false
            } else if (Util.isContentNull((child.findViewById<View>(R.id.edtPrice) as EditText).text.toString())) {
                child.findViewById<View>(R.id.edtPrice).requestFocus()
                HooleyMain.activity!!.showSuccessDialog("Provide All Detail")
                return false
            } else {
                ticketCategoriesList.add(TicketCategoriesModel((child.findViewById<View>(R.id.edtTicketName) as EditText).text.toString(),
                        (child.findViewById<View>(R.id.edtQuantity) as EditText).text.toString(),
                        (child.findViewById<View>(R.id.edtPrice) as EditText).text.toString(),
                        "0", (child.findViewById<View>(R.id.edtQuantity) as EditText).text.toString()))
            }
        }
        return true
    }

    internal var gson = Gson()

    fun saveTicket(shareIncomePartnerList: ArrayList<ShareIncomePartnerModel>,
                   ticketCategoriesList: ArrayList<TicketCategoriesModel>, isUpdate: String) {
        HooleyMain.activity!!.showDialog()
        var ticketInfoArrayList = ArrayList<SaveTicketRequestModel.TicketObj.TicketInfo>()
        var incomeShareArrayList = ArrayList<SaveTicketRequestModel.TicketObj.IncomeShare>()


        for (i in ticketCategoriesList.indices) {
            ticketInfoArrayList.add(SaveTicketRequestModel.TicketObj.TicketInfo(ticketCategoriesList[i].ticketName, ticketCategoriesList[i].tickeQuantity, ticketCategoriesList[i].ticketPrice))
        }

        for (i in shareIncomePartnerList.indices) {
            if (shareIncomePartnerList[i].percent != 0)
                incomeShareArrayList.add(SaveTicketRequestModel.TicketObj.IncomeShare(shareIncomePartnerList[i].friendsTable.userId.toString(), shareIncomePartnerList[i].percent))
        }

        var charity: Int = 0
        var ppv: Int = 0
        charity = if (binding.edtCharityPrice.text.toString() == "") {
            0
        } else {
            binding.edtCharityPrice.text.toString().toInt()
        }

        ppv = if (binding.edtPpvPrice.text.toString() == "") {
            0
        } else {
            binding.edtPpvPrice.text.toString().toInt()
        }

        var ticketObj: SaveTicketRequestModel.TicketObj
        if (ticketCategoriesList.size > 0) {
            ticketObj = SaveTicketRequestModel.TicketObj(binding.tbTicketRefundCheck.isChecked, charity, ppv, ticketInfoArrayList, incomeShareArrayList)
        } else {
            ticketObj = SaveTicketRequestModel.TicketObj(binding.tbTicketRefundCheck.isChecked, charity, ppv, null, incomeShareArrayList)
        }
        var updateEventModel: UpdateEventRequestModel = gson.fromJson(json, UpdateEventRequestModel::class.java)

        updateEventModel.saveTicket = ticketObj

        eventWebService.savePaidEvent(eventCoverList!!, updateEventModel, isUpdate, object : IWebServiceCallback<SaveEventModel> {
            override fun success(result: SaveEventModel) {
                HooleyMain.activity!!.removeDialog()
                if (result.success) {
                    Logger.ex("publishEvent:$result")
                    HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                    HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                    (HooleyMain.activity as ActivityBase).removeFragment(CreateEventFragment())
                    val r = Runnable {
                        (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, ConfirmCreateEventFragment.newInstance(result.eventObject.id!!, result.eventObject.name!!), null)
                    }
                    Handler().postDelayed(r, 250)
                } else {
                    Util.showToastMessage(HooleyMain.activity!!, result.exception)
                }
            }

            override fun failure(message: String) {
                binding.btnSaveTicket.isEnabled = true
                binding.btnSaveTicket.isClickable = true
                HooleyMain.activity!!.removeDialog()
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }
            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_STRIPE && resultCode == Activity.RESULT_OK) {
            HooleyApp.db.putBoolean(Constants.IS_STRIPE_CONNECTED, true)
            code = data!!.getStringExtra("code")
            connectStripe()
            Log.e("StripeCode", " " + data.getStringExtra("code"))
        } else if (requestCode == REQUEST_ADD_INCOME_SHARE && resultCode == Activity.RESULT_OK) {
            shareIncomePartnerList = data!!.getParcelableArrayListExtra("list")
            if (shareIncomePartnerList.size > 0) {
                binding.llContainer.removeAllViews()
                availableIncome = 98
                for (i in shareIncomePartnerList.indices) {
                    if (shareIncomePartnerList[i].percent != 0) {
                        availableIncome -= shareIncomePartnerList[i].percent
                        addAddedPartnerChild(shareIncomePartnerList[i])
                    }
                }
                binding.tvIncomePercentage.text = "$availableIncome%"
            }
        } else if (requestCode == REQUEST_ADD_INCOME_SHARE && resultCode == Activity.RESULT_CANCELED) {
            for (i in friendList!!.indices) {
                shareIncomePartnerList.add(ShareIncomePartnerModel(friendList!![i], 0))
            }
        }
    }

    override fun onResume() {
        Util.hideKeyboard(HooleyMain.activity!!)
        super.onResume()
    }

    private fun connectStripe() {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        HooleyMain.activity!!.showDialog()
        webService.connectStripe(code!!, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                Util.showToastMessage(HooleyMain.activity!!, "Stripe Connect Successfully")
                getTermsAndConditions()
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    companion object {

        private lateinit var instance: EventTicketSetupFragment
        private var eventStartDate: String? = null
        private var eventEndDate: String? = null
        private var eventId: String? = null
        private var eventName: String? = null
        private var json: String? = null
        private var eventCoverList: ArrayList<ItemCreateEventWallpaper>? = null
        private var editEventModel: EditEventModel? = null
        private var isUpdate: String? = null
        var eventType: String = ""

        fun newInstance(coverList: ArrayList<ItemCreateEventWallpaper>, eName: String, startDate: String, endDate: String, jsonData: String, eventType_: String): EventTicketSetupFragment {
            eventCoverList = coverList
            eventStartDate = startDate
            eventEndDate = endDate
            eventName = eName
            json = jsonData
            eventType = eventType_
            instance = EventTicketSetupFragment()
            return instance
        }

        fun newInstance(coverList: ArrayList<ItemCreateEventWallpaper>, model: EditEventModel, id: String, update: String): EventTicketSetupFragment {
            eventCoverList = coverList
            editEventModel = model
            eventType = editEventModel!!.eventBasicInfo!!.eventType.toString()
            eventId = id
            isUpdate = update
            instance = EventTicketSetupFragment()
            return instance
        }
    }
}
