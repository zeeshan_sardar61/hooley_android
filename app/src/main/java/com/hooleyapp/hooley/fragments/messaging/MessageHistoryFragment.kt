package com.hooleyapp.hooley.fragments.messaging

import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterMessageHistory
import com.hooleyapp.hooley.adapters.AdapterSelectFriends
import com.hooleyapp.hooley.databinding.FragmentMessageHistoryBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.friends.SelectFriendFragment
import com.hooleyapp.hooley.fragments.profile.ProfileMainFragment.Companion.fragmentProfileMainBinding
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.FriendsModel
import com.hooleyapp.hooley.model.GetAllThreadModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.services.MessageService
import java.util.*

/**
 * Created by Nauman on 4/24/2018.
 */

class MessageHistoryFragment : BaseFragment(), AdapterSelectFriends.ISelectFriendListener, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    var binding: FragmentMessageHistoryBinding? = null
    var selectedPosition = -1
    var mlist: ArrayList<GetAllThreadModel.ThreadInfo>? = null
    internal var isSwiped = false
    private var service: MessageService? = null
    private var objectResponseMessage: GetAllThreadModel? = null
    private var adapterMesageHistory: AdapterMessageHistory? = null
    private val FriendsDialog: Dialog? = null
    private val rvFriends: RecyclerView? = null
    private var friendService: FriendWebService? = null
    private var objectResponseFriends: FriendsModel? = null
    private val adapterSelectFriend: AdapterSelectFriends? = null
    private val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            getAllThread()
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {

        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            try {
                LocalBroadcastManager.getInstance(HooleyMain.activity!!).registerReceiver(mMessageReceiver,
                        IntentFilter("Message"))
                fragmentProfileMainBinding!!.btnCreateThread.visibility = View.VISIBLE
                fragmentProfileMainBinding!!.ivbtnBack.visibility = View.VISIBLE
                getMyFriendsList()
            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            }

        } else {
            fragmentProfileMainBinding!!.btnCreateThread.visibility = View.GONE
            fragmentProfileMainBinding!!.ivbtnBack.visibility = View.GONE
            LocalBroadcastManager.getInstance(HooleyMain.activity!!).unregisterReceiver(mMessageReceiver)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_message_history, container, false)
        setListeners()
        getAllThread()
        return binding!!.root
    }

    private fun setListeners() {
        fragmentProfileMainBinding!!.btnCreateThread.setOnClickListener(this)
        binding!!.slMessage.setOnRefreshListener(this)
    }

    private fun getMyFriendsList() {
        friendService = FriendWebService()
        friendService!!.getMyFriendsList(object : GenericCallback<FriendsModel> {

            override fun success(result: FriendsModel) {
                objectResponseFriends = result
            }

            override fun failure(message: String) {
                try {

                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun getAllThread() {
        if (!isSwiped)
            binding!!.pbMessage.visibility = View.VISIBLE
        service = MessageService()
        service!!.getMessageHistory(object : GenericCallback<GetAllThreadModel> {
            override fun success(result: GetAllThreadModel) {
                if (isSwiped) {
                    binding!!.slMessage.isRefreshing = false
                    isSwiped = false
                }
                binding!!.pbMessage.visibility = View.GONE
                binding!!.rvMessageHistory.visibility = View.VISIBLE
                objectResponseMessage = result
                if (mlist == null)
                    mlist = ArrayList()
                if (mlist!!.size > 0)
                    mlist!!.clear()
                mlist!!.addAll(objectResponseMessage!!.messageHistoryList)
                setMessageRv()
            }

            override fun failure(message: String) {
                binding!!.pbMessage.visibility = View.GONE
                if (isSwiped) {
                    binding!!.slMessage.isRefreshing = false
                    isSwiped = false
                }
                try {
                    GetAllThreadModel.ThreadInfo()
                    if (mlist == null)
                        mlist = ArrayList()
                    if (mlist!!.size > 0)
                        mlist!!.clear()
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.tvNoData.text = message
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun setMessageRv() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding!!.rvMessageHistory.layoutAnimation = animation
        binding!!.rvMessageHistory.layoutManager = manager
        adapterMesageHistory = AdapterMessageHistory(HooleyMain.activity!!, mlist)
        //        adapterMesageHistory.setListener(this);
        binding!!.rvMessageHistory.adapter = adapterMesageHistory
    }


    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onFriendSelected(position: Int) {
        selectedPosition = position
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnCreateThread -> {
                HooleyApp.db.putString(Constants.CALL_TYPE, "CREATE_MESSAGE")
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, SelectFriendFragment.newInstance(), "SelectFriendFragment")
            }
        }
    }

    override fun onRefresh() {
        binding!!.slMessage.isRefreshing = true
        binding!!.pbMessage.visibility = View.GONE
        isSwiped = true
        getAllThread()
    }
}
