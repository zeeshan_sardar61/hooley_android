package com.hooleyapp.hooley.fragments.invites

import android.annotation.SuppressLint
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterSelectInviteGuest
import com.hooleyapp.hooley.databinding.FragmentInvitePeopleNearbyBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.hideKeyboard
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.EventInviteNearByMeModel
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.InviteWebService
import java.util.*

class SendInvitePeopleNearbyMeFragment : BaseFragment(), View.OnClickListener, TextWatcher, AdapterSelectInviteGuest.ISelectFriendListener {

    internal lateinit var binding: FragmentInvitePeopleNearbyBinding
    private var adapterSelectInviteGuest: AdapterSelectInviteGuest? = null
    var service = InviteWebService()
    var count = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_invite_people_nearby, container, false)
        setRecyclerView()
        setListener()
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    private fun setRecyclerView() {
        binding.tvPeopleCount.text = mList!!.size.toString() + " People"
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        binding.rvSelectFriend.layoutManager = manager
        binding.rvSelectFriend.layoutAnimation = animation
        if (inviteList != null) {
            inviteList!!.clear()
        }
        inviteList!!.addAll(mList!!)
        adapterSelectInviteGuest = AdapterSelectInviteGuest(HooleyMain.activity!!, inviteList)
        adapterSelectInviteGuest!!.mlistener = this
        binding.rvSelectFriend.adapter = adapterSelectInviteGuest
    }

    private fun setListener() {
        binding.tbSelectAll.setOnClickListener(this)
        binding.ivClose.setOnClickListener(this)
        binding.btnSendInvite.setOnClickListener(this)
        binding.edtSearch.addTextChangedListener(this)
    }

    private fun selectAll(check: Boolean) {
        if (mList != null && mList!!.size > 0) {
            for (i in mList!!.indices) {
                mList!![i].isChecked = check
            }
        }
        if (adapterSelectInviteGuest != null)
            adapterSelectInviteGuest!!.notifyDataSetChanged()
    }

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

    }

    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

    }

    override fun afterTextChanged(s: Editable) {
        if (s.isNotEmpty()) {
            if (inviteList == null)
                return
            inviteList!!.clear()
            if (mList != null && mList!!.size > 0) {
                if (mListSearch!!.size > 0)
                    mListSearch!!.clear()
                mListSearch!!.addAll(mList!!)
            }
            for (i in mListSearch!!.indices) {
                if (mListSearch!![i].fullName!!.toLowerCase().contains(s.toString().toLowerCase())) {
                    inviteList!!.add(mListSearch!![i])
                }
            }

            if (inviteList!!.size == 0) {
                binding.tvNoData.visibility = View.VISIBLE
                binding.tvNoData.text = "No Record Found"
            } else {
                binding.tvNoData.visibility = View.GONE
            }

            binding.tvPeopleCount.text = if (inviteList!!.size == 1) {
                inviteList!!.size.toString() + " Person"
            } else {
                inviteList!!.size.toString() + " People"
            }
            if (adapterSelectInviteGuest != null)
                adapterSelectInviteGuest!!.notifyDataSetChanged()
        } else {
            binding.tvNoData.visibility = View.GONE
            if (mListSearch == null)
                return
            if (inviteList != null)
                inviteList!!.clear()
            inviteList!!.addAll(mListSearch!!)
            binding.tvPeopleCount.text = if (inviteList!!.size == 1) {
                inviteList!!.size.toString() + " Person"
            } else {
                inviteList!!.size.toString() + " People"
            }
            if (adapterSelectInviteGuest != null)
                adapterSelectInviteGuest!!.notifyDataSetChanged()
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivClose -> {
                binding.edtSearch.setText("")
                Util.hideKeyboard(HooleyMain.activity!!)
            }
            R.id.tbSelectAll -> selectAll(binding.tbSelectAll.isChecked)
            R.id.btnSendInvite -> if (validInput())
                sendInviteNearBy()
            else
                Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_selection))
        }
    }

    private fun validInput(): Boolean {
        if (mList!!.size > 0) {
            for (i in mList!!.indices) {
                if (mList!![i].isChecked)
                    return true
            }
            return false
        } else {
            return false
        }
    }

    private fun sendInviteNearBy() {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        HooleyMain.activity!!.showDialog()
        service.sendInvitePeopleNearBy(InviteFragment.createdEventId, InviteFragment.createdEventName, mList!!, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                for (i in mList!!.indices) {
                    if (mList!![i].isChecked)
                        mList!![i].isChecked = false
                }
                Util.showToastMessage(HooleyMain.activity!!, "Invitation Sent")
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()

            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {

                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        HooleyMain.activity!!.hideKeyboard()
        binding.unbind()
    }

    override fun onFriendSelected(position: Int) {
        count = 0
        for (i in mList!!.indices) {
            if (mList!![i].isChecked) {
                count++
            }
        }

        if (count == mList!!.size) {
            binding.tbSelectAll.isChecked = true
        } else if (count > 0) {
            binding.tbSelectAll.isChecked = false
        } else if (count == 0) {
            binding.tbSelectAll.isChecked = false
        }
    }

    companion object {
        lateinit var instance: SendInvitePeopleNearbyMeFragment
        var mList: ArrayList<EventInviteNearByMeModel.UserNearMe>? = null
        var inviteList: ArrayList<EventInviteNearByMeModel.UserNearMe>? = ArrayList()
        var mListSearch: ArrayList<EventInviteNearByMeModel.UserNearMe>? = ArrayList()

        fun newInstance(list: ArrayList<EventInviteNearByMeModel.UserNearMe>): SendInvitePeopleNearbyMeFragment {
            mList = list
            instance = SendInvitePeopleNearbyMeFragment()
            return instance
        }
    }
}
