package com.hooleyapp.hooley.fragments.auth

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient

import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentFlickrAuthBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.log.Logger

/**
 * Created by Nauman on 2/21/2018.
 */

class FlickrAuthFragment : BaseFragment() {
    lateinit var intent: Intent
    internal lateinit var binding: FragmentFlickrAuthBinding


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_flickr_auth, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setWebView()
    }


    fun setWebView() {

        binding.wvFlickr.settings.javaScriptEnabled = true

        binding.wvFlickr.webViewClient = object : WebViewClient() {

            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                //                showProgressDialog();

                /** TODO: This part should be deleted after flickr fixes the oauth redirection issue  */
                if (url.endsWith("flickr.com/")) {
                    binding.wvFlickr.loadUrl(authorizationUrl)
                    return
                }

                /* After successful login the redirected url will contain a parameter named 'oauth_verifier' */
                if (url.indexOf("oauth_verifier=") > -1) {
                    /* Get the 'oauth_verifier' parameter from url and do next steps */
                    //                    performLoginSuccess();
                    Logger.e("performLoginSuccess" + " / oAuthVerifier = " + url.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[2])

                    intent = Intent()
                    intent.putExtra("oauth_verifier", url.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[2])
                    HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                    targetFragment!!.onActivityResult(
                            targetRequestCode,
                            Activity.RESULT_OK,
                            intent
                    )

                }
                /* After successful login the redirected url will contain a parameter named 'oauth_verifier' */
                if (url.indexOf("oauth_token_secret=") > -1) {
                    /* Get the 'oauth_verifier' parameter from url and do next steps */
                    //                    performLoginSuccess();
                }

                //                oauth_callback_confirmed=true&oauth_token=72157665981012228-8c84b6e5365c5a26&oauth_token_secret=71d91ebf62b344a1
            }

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                if (url.contains(PROTECTED_RESOURCE_URL) && !url.contains("oauth_verifier=")) {
                    /* User not authenticated our app */
                    HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                } else if (!url.contains("oauth_verifier=")) {
                    Logger.e("killProgressDialog")
                }
            }
        }

        binding.wvFlickr.loadUrl(authorizationUrl)
    }

    companion object {

        lateinit var instance: FlickrAuthFragment
        private var authorizationUrl: String? = null
        private val PROTECTED_RESOURCE_URL = "http://api.flickr.com/services/rest/"


        fun newInstance(url: String): FlickrAuthFragment {
            authorizationUrl = url
            instance = FlickrAuthFragment()
            return instance
        }
    }
}
