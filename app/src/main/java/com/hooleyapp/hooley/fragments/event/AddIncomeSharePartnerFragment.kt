package com.hooleyapp.hooley.fragments.event

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterShareIncome
import com.hooleyapp.hooley.databinding.FragmentAddIncomeShareBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.setToolBarTitle
import com.hooleyapp.hooley.model.ShareIncomePartnerModel
import com.hooleyapp.hooley.others.Util
import java.util.*

class AddIncomeSharePartnerFragment : BaseFragment(), TextWatcher {
    var binding: FragmentAddIncomeShareBinding? = null
    private var adapter: AdapterShareIncome? = null
    private var intent: Intent? = null
    private val mSearchList = ArrayList<ShareIncomePartnerModel>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_income_share, container, false)
        //        HooleyMain.activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setToolBarTitle(HooleyMain.activity!!, "Share Revenue")
        setAdapter()
        setListener()
        setHasOptionsMenu(true)
        mSearchList.addAll(mList!!)
        return binding!!.root
    }

    private fun setListener() {
        binding!!.edtSearch.addTextChangedListener(this)
        binding!!.btnDone.setOnClickListener {
            intent = Intent()
            intent!!.putParcelableArrayListExtra("list", mList)
            HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
            targetFragment!!.onActivityResult(
                    targetRequestCode,
                    Activity.RESULT_OK,
                    intent
            )
        }

        binding!!.ivClose.setOnClickListener {
            binding!!.edtSearch.setText("")
            Util.hideKeyboard(HooleyMain.activity!!)
        }
    }

    private fun setAdapter() {
        if (mList != null) {
            val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
            binding!!.rvSelectFriend.layoutManager = manager
            adapter = AdapterShareIncome(HooleyMain.activity!!, mList)
            binding!!.rvSelectFriend.adapter = adapter
        } else {
            binding!!.rvSelectFriend.visibility = View.GONE
            binding!!.rlSearchBar.visibility = View.GONE
            binding!!.view.visibility = View.GONE
            binding!!.btnDone.visibility = View.GONE
            binding!!.tvNoData.visibility = View.VISIBLE
            binding!!.tvNoData.text = "No friends"
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

    }

    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

    }

    override fun afterTextChanged(s: Editable) {

        if (s.isNotEmpty()) {
            if (mList == null)
                return
            mList!!.clear()
            if (mSearchList != null && mSearchList.size > 0) {
                for (i in mSearchList.indices) {
                    if (mSearchList[i].friendsTable.fullName.toLowerCase().contains(s.toString().toLowerCase())) {
                        mList!!.add(mSearchList[i])
                    }
                }
            }

            if (mList!!.size == 0) {
                binding!!.tvNoData.visibility = View.VISIBLE
                binding!!.tvNoData.text = "No Record Found"
            } else {
                binding!!.tvNoData.visibility = View.GONE
            }

            if (adapter != null)
                adapter!!.notifyDataSetChanged()
        } else {
            binding!!.tvNoData.visibility = View.GONE
            if (mSearchList == null)
                return
            if (mList != null)
                mList!!.clear()

            mList!!.addAll(mSearchList)
            if (adapter != null)
                adapter!!.notifyDataSetChanged()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu!!.clear()
        inflater!!.inflate(R.menu.gallery_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.save -> {
                intent = Intent()
                intent!!.putParcelableArrayListExtra("list", mList)
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                targetFragment!!.onActivityResult(
                        targetRequestCode,
                        Activity.RESULT_OK,
                        intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {

        lateinit var instance: AddIncomeSharePartnerFragment
        private var mList: ArrayList<ShareIncomePartnerModel>? = ArrayList()

        fun newInstance(list: ArrayList<ShareIncomePartnerModel>): AddIncomeSharePartnerFragment {
            mList = list
            instance = AddIncomeSharePartnerFragment()
            return instance
        }
    }
}
