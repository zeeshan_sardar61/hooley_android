package com.hooleyapp.hooley.fragments.event

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.RadioGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterBookmarkEvents
import com.hooleyapp.hooley.databinding.FragmentBookmarkEventsBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.notification.NotificationMainFragment
import com.hooleyapp.hooley.fragments.search.SearchFilterFragment
import com.hooleyapp.hooley.helper.hideKeyboard
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.IBookmarkEventsClickListener
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.BookmarkEventModel
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.services.EventWebService
import java.util.*

/**
 * Created by Nauman on 3/29/2018.
 */

class FeatureEventFragment : BaseFragment(), RadioGroup.OnCheckedChangeListener, View.OnClickListener, IBookmarkEventsClickListener, SwipeRefreshLayout.OnRefreshListener {
    internal var binding: FragmentBookmarkEventsBinding? = null
    internal var objectResponse: BookmarkEventModel? = null
    internal var adapter: AdapterBookmarkEvents? = null
    private var position = 0
    private var mlistFilterBookmark: ArrayList<BookmarkEventModel.EventDetail>? = null
    private val eventService = EventWebService()
    internal var isSwiped = false
    internal var bold = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)
    internal var regular = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bookmark_events, container, false)
        setListener()
        binding!!.rbAllEvent.typeface = bold
        binding!!.rbAllEvent.isChecked = true
        getFeatureEvent()
        return binding!!.root
    }

    private fun setListener() {
        binding!!.rgTab.setOnCheckedChangeListener(this)
        binding!!.slBookmarks.setOnRefreshListener(this)
    }


    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        when (checkedId) {
            R.id.rbAllEvent -> {
                binding!!.tvNoData.text = "No Upcoming Events"
                binding!!.rbAllEvent.typeface = bold
                binding!!.rbPastEvents.typeface = regular
                FilterByAll()
                binding!!.rbAllEvent.setTextColor(activity!!.resources.getColor(android.R.color.white))
                binding!!.rbAllEvent.background = activity!!.resources.getDrawable(R.drawable.bg_btn_left_radio)
                binding!!.rbPastEvents.setTextColor(activity!!.resources.getColor(R.color.app_purple_color))
                binding!!.rbPastEvents.setBackgroundColor(activity!!.resources.getColor(android.R.color.transparent))
            }
            R.id.rbPastEvents -> {
                binding!!.tvNoData.text = "No Past Events"
                binding!!.rbAllEvent.typeface = regular
                binding!!.rbPastEvents.typeface = bold
                FilterByPast()
                binding!!.rbAllEvent.setTextColor(activity!!.resources.getColor(R.color.app_purple_color))
                binding!!.rbAllEvent.setBackgroundColor(activity!!.resources.getColor(android.R.color.transparent))
                binding!!.rbPastEvents.setTextColor(activity!!.resources.getColor(android.R.color.white))
                binding!!.rbPastEvents.background = activity!!.resources.getDrawable(R.drawable.bg_btn_right_radio)
            }
        }
    }


    /**
     * Initialize the contents of the Fragment host's standard options menu.  You
     * should place your menu items in to <var>menu</var>.  For this method
     * to be called, you must have first called [.setHasOptionsMenu].  See
     * for more information.
     *
     * @param menu     The options menu in which you place your items.
     * @param inflater
     * @see .setHasOptionsMenu
     *
     * @see .onPrepareOptionsMenu
     *
     * @see .onOptionsItemSelected
     */
    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        //        menu.clear();
        //        inflater.inflate(R.menu.menu_main, menu);
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     *
     *
     *
     * Derived classes should call through to the base class for it to
     * perform the default menu handling.
     *
     * @param item The menu item that was selected.
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     * @see .onCreateOptionsMenu
     */
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.ic_search -> {
                HooleyMain.activity!!.hideKeyboard()
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, SearchFilterFragment.newInstance(), "SearchFilterFragment")
                return true
            }
            R.id.ic_notification -> {
                HooleyMain.activity!!.hideKeyboard()
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, NotificationMainFragment.newInstance(), "NotificationMainFragment")
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onClick(v: View) {
        when (v.id) {

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onClickLike(eventObj: BookmarkEventModel.EventDetail, pos: Int) {
        position = pos
        likeEvent(objectResponse!!.bookmarkedEventsList[pos].eventId)

    }

    override fun onClickShare(eventObj: BookmarkEventModel.EventDetail, pos: Int) {
        position = pos
        showShareEventDialog()
    }

    override fun onClickFollow(eventObj: BookmarkEventModel.EventDetail, pos: Int) {
        position = pos
        followEvent(eventObj.eventId, position)
    }

    private fun setBookmarkRv() {
        val manager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_from_right)
        binding!!.rvBookmarkEvent.layoutAnimation = animation
        binding!!.rvBookmarkEvent.layoutManager = manager
        binding!!.rvBookmarkEvent.setEmptyView(binding!!.tvNoData)
        if (mlistFilterBookmark != null) {
            if (mlistFilterBookmark!!.size > 0)
                mlistFilterBookmark!!.clear()
        } else {
            mlistFilterBookmark = ArrayList<BookmarkEventModel.EventDetail>()
        }
        if (objectResponse!!.bookmarkedEventsList != null)
            mlistFilterBookmark!!.addAll(objectResponse!!.bookmarkedEventsList)
        adapter = AdapterBookmarkEvents(HooleyMain.activity!!, mlistFilterBookmark!!)
        adapter!!.setListener(this)
        binding!!.rvBookmarkEvent.adapter = adapter
        if (binding!!.rbAllEvent.isChecked) {
            FilterByAll()
        } else {
            FilterByPast()
        }
    }

    private fun FilterByAll() {
        if (mlistFilterBookmark != null) {
            if (mlistFilterBookmark!!.size > 0)
                mlistFilterBookmark!!.clear()
        } else {
            mlistFilterBookmark = ArrayList<BookmarkEventModel.EventDetail>()
        }
        if (objectResponse == null)
            return
        if (objectResponse!!.bookmarkedEventsList != null)
            for (i in objectResponse!!.bookmarkedEventsList.indices) {
                if (!objectResponse!!.bookmarkedEventsList[i].isPast)
                    mlistFilterBookmark!!.add(objectResponse!!.bookmarkedEventsList[i])
            }
        if (adapter != null)
            adapter!!.notifyDataSetChanged()
    }


    private fun FilterByPast() {
        if (mlistFilterBookmark != null) {
            if (mlistFilterBookmark!!.size > 0)
                mlistFilterBookmark!!.clear()
        } else {
            mlistFilterBookmark = ArrayList<BookmarkEventModel.EventDetail>()
        }
        if (objectResponse == null)
            return
        if (objectResponse!!.bookmarkedEventsList != null) {
            for (i in objectResponse!!.bookmarkedEventsList.indices) {
                if (objectResponse!!.bookmarkedEventsList[i].isPast)
                    mlistFilterBookmark!!.add(objectResponse!!.bookmarkedEventsList[i])
            }
        }
        if (adapter != null)
            adapter!!.notifyDataSetChanged()
    }

    private fun getFeatureEvent() {
        if (binding != null && isSwiped == false) {
            binding!!.pbBookmark.visibility = View.VISIBLE
        }
        eventService.getFeatureEvent(object : IWebServiceCallback<BookmarkEventModel> {
            override fun success(result: BookmarkEventModel) {
                if (isSwiped == true) {
                    binding!!.slBookmarks.isRefreshing = false
                    isSwiped = false
                }
                objectResponse = result
                binding!!.pbBookmark.visibility = View.GONE
                binding!!.tvNoData.visibility = View.GONE
                binding!!.rvBookmarkEvent.visibility = View.VISIBLE
                setBookmarkRv()
            }

            override fun failure(message: String) {
                if (isSwiped == true) {
                    binding!!.slBookmarks.isRefreshing = false
                    isSwiped = false
                }
                binding!!.pbBookmark.visibility = View.GONE

                try {
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.rvBookmarkEvent.visibility = View.GONE
                    binding!!.tvNoData.text = message
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    private fun likeEvent(eventId: String?) {
        if (binding != null)
            binding!!.pbBookmark.visibility = View.VISIBLE
        eventService.likeEvent(eventId!!, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                binding!!.pbBookmark.visibility = View.GONE
                if (objectResponse!!.bookmarkedEventsList[position].isLiked) {
                    objectResponse!!.bookmarkedEventsList[position].isLiked = false
                    objectResponse!!.bookmarkedEventsList[position].likesCount = objectResponse!!.bookmarkedEventsList[position].likesCount - 1
                    adapter!!.notifyDataSetChanged()
                } else {
                    objectResponse!!.bookmarkedEventsList[position].isLiked = true
                    objectResponse!!.bookmarkedEventsList[position].likesCount = objectResponse!!.bookmarkedEventsList[position].likesCount + 1
                    adapter!!.notifyDataSetChanged()
                }
            }

            override fun failure(message: String) {
                binding!!.pbBookmark.visibility = View.GONE
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    private fun showShareEventDialog() {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Event Name = " + mlistFilterBookmark!![position].eventName
                + "\nEvent Date&Time =  " + DateUtils.displayDateOnly(DateUtils.getLocalDate(mlistFilterBookmark!![position].startTime!!))
                + "\nEvent Location = " + mlistFilterBookmark!![position].address
                + "\nEvent " + Constants.INVITE_EVENT_URL + mlistFilterBookmark!![position].eventId)
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(Intent.createChooser(shareIntent, "Share Event"))
    }

    private fun followEvent(eventId: String?, listPosition: Int) {
        eventService.followEvent(eventId!!, !mlistFilterBookmark!![listPosition].isFollowed, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                if (mlistFilterBookmark!![listPosition].isFollowed) {
                    mlistFilterBookmark!![listPosition].isFollowed = false
                    mlistFilterBookmark!![listPosition].followersCount = mlistFilterBookmark!![listPosition].followersCount - 1
                } else {
                    mlistFilterBookmark!![listPosition].isFollowed = true
                    mlistFilterBookmark!![listPosition].followersCount = mlistFilterBookmark!![listPosition].followersCount + 1
                }
                if (adapter != null)
                    adapter!!.notifyDataSetChanged()
            }

            override fun failure(message: String) {
                try {

                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    private fun shareEvent(shareMedia: String) {
        if (binding != null)
            binding!!.pbBookmark.visibility = View.VISIBLE
        eventService.shareEvent(objectResponse!!.bookmarkedEventsList[position].eventId!!, "", object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                binding!!.pbBookmark.visibility = View.GONE
                objectResponse!!.bookmarkedEventsList[position].sharesCount = objectResponse!!.bookmarkedEventsList[position].sharesCount + 1
                adapter!!.notifyDataSetChanged()
            }

            override fun failure(message: String) {
                binding!!.pbBookmark.visibility = View.GONE
                try {

                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }


            }

            override fun onTokenExpire() {
                onTokenExpire()
            }
        })
    }

    override fun onRefresh() {
        if (binding!!.slBookmarks != null) {
            binding!!.slBookmarks.isRefreshing = true
            binding!!.pbBookmark.visibility = View.GONE
        }
        isSwiped = true
        getFeatureEvent()
    }

    companion object {

        lateinit var instance: FeatureEventFragment

        fun newInstance(): FeatureEventFragment {
            instance = FeatureEventFragment()
            return instance
        }
    }
}
