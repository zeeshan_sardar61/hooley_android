package com.hooleyapp.hooley.fragments.notification

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterMessageNotification
import com.hooleyapp.hooley.databinding.FragmentNotificationChildBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.MessageAlertNotificationModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.MessageEvent
import com.hooleyapp.hooley.services.NotificationWebService
import org.greenrobot.eventbus.EventBus

/**
 * Created by Nauman on 4/11/2018.
 */

class MessageAlertNotificationFragment : BaseFragment(), SwipeRefreshLayout.OnRefreshListener {

    var binding: FragmentNotificationChildBinding? = null
    private var objectResponse: MessageAlertNotificationModel? = null
    private var adapter: AdapterMessageNotification? = null
    private val service = NotificationWebService()
    internal var isSwiped = false

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            Handler().postDelayed({ EventBus.getDefault().post(MessageEvent(Constants.TYPE_MESSAGES_ALERT, true)) }, 1000)
        } else {

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification_child, container, false)
        setListener()
        return binding!!.root
    }

    private fun setListener() {
        binding!!.sLRecycler.setOnRefreshListener(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showFriendRequestNotification()
    }


    private fun setNotificationRv() {
        val manager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding!!.rvNotifications.layoutManager = manager
        binding!!.rvNotifications.setEmptyView(binding!!.tvNoData)
        adapter = AdapterMessageNotification(HooleyMain.activity!!, objectResponse!!.mlist)
        //        adapter.setListener(this);
        binding!!.rvNotifications.adapter = adapter
    }

    private fun showFriendRequestNotification() {
        if (binding != null && !isSwiped)
            binding!!.pbNotification.visibility = View.VISIBLE
        service.getMessageAlterNotification(object : GenericCallback<MessageAlertNotificationModel> {
            override fun success(result: MessageAlertNotificationModel) {
                if (isSwiped == true) {
                    binding!!.sLRecycler.isRefreshing = false
                    isSwiped = false
                }
                objectResponse = result
                binding!!.tvNoData.visibility = View.GONE
                binding!!.pbNotification.visibility = View.GONE
                binding!!.rvNotifications.visibility = View.VISIBLE
                setNotificationRv()
            }

            override fun failure(message: String) {
                if (isSwiped) {
                    binding!!.sLRecycler.isRefreshing = false
                    isSwiped = false
                }
                binding!!.pbNotification.visibility = View.GONE
                try {
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.rvNotifications.visibility = View.GONE
                    binding!!.tvNoData.text = message
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })


    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onRefresh() {
        if (binding!!.sLRecycler != null) {
            binding!!.sLRecycler.isRefreshing = true
            binding!!.pbNotification.visibility = View.GONE
        }
        isSwiped = true
        showFriendRequestNotification()
    }
}
