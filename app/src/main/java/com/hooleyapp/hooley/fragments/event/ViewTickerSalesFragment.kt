package com.hooleyapp.hooley.fragments.event

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentViewTicketSalesBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.ticket.ScanTicketFragment
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.ticketStats.TicketStatsResponse
import com.hooleyapp.hooley.services.TicketWebService

class ViewTickerSalesFragment : BaseFragment(), View.OnClickListener {
    var ticketWebService = TicketWebService()
    var binding: FragmentViewTicketSalesBinding? = null
    private var objectResponse: TicketStatsResponse? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_view_ticket_sales, container, false)
        HooleyMain.activity!!.supportActionBar!!.show()
        hideDrawer(HooleyMain.activity!!)
        getViewTickerSale()
        setListener()
        return binding!!.root
    }

    private fun setListener() {
        binding!!.btnScanTicket.setOnClickListener(this)
    }

    private fun getViewTickerSale() {
        ticketWebService.viewTicketSales(eventId, object : GenericCallback<TicketStatsResponse> {
            override fun success(result: TicketStatsResponse) {
                objectResponse = result
                setData()
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun setData() {
        binding!!.tvEventName.text = eventName
        binding!!.tvEventAddress.text = eventAddress
        binding!!.tvEventTimeStamp.text = eventTime
        if (objectResponse!!.latestSaleStats != null) {
            for (i in objectResponse!!.latestSaleStats!!.indices) {
                addViewSales(i)
            }
            for (i in objectResponse!!.latestScanStats!!.indices) {
                addViewStatsScan(i)
            }
        }

    }

    fun addViewSales(pos: Int) {
        val child = HooleyMain.activity!!.layoutInflater.inflate(R.layout.child_ticket_sales, null)
        (child.findViewById<View>(R.id.tvTickerCategory) as TextView).text = objectResponse!!.latestSaleStats!![pos]?.ticketCategory
        (child.findViewById<View>(R.id.tvTotalTickets) as TextView).text = objectResponse!!.latestSaleStats!![pos]?.total!!.toString()
        (child.findViewById<View>(R.id.tvSoldTickets) as TextView).text = objectResponse!!.latestSaleStats!![pos]?.sold!!.toString()
        (child.findViewById<View>(R.id.tvRemainingTicket) as TextView).text = objectResponse!!.latestSaleStats!![pos]?.available!!.toString()
        binding!!.llSaleStats.addView(child)
    }

    fun addViewStatsScan(pos: Int) {
        val child = HooleyMain.activity!!.layoutInflater.inflate(R.layout.child_ticket_sales, null)
        (child.findViewById<View>(R.id.tvTickerCategory) as TextView).text = objectResponse!!.latestScanStats!![pos]?.ticketCategory
        (child.findViewById<View>(R.id.tvTotalTickets) as TextView).text = objectResponse!!.latestScanStats!![pos]?.sold.toString()
        (child.findViewById<View>(R.id.tvSoldTickets) as TextView).text = objectResponse!!.latestScanStats!![pos]?.scan.toString()
        (child.findViewById<View>(R.id.tvRemainingTicket) as TextView).text = objectResponse!!.latestScanStats!![pos]?.left.toString()
        binding!!.llScanStats.addView(child)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnScanTicket -> (HooleyMain.activity as ActivityBase).callFragment(R.id.container, ScanTicketFragment.newInstance(), "ScanTicketFragment")
        }
    }

    companion object {

        lateinit var instance: ViewTickerSalesFragment
        lateinit var eventId: String
        private var eventName: String? = null
        private var eventAddress: String? = null
        private var eventTime: String? = null

        fun newInstance(id: String, name: String, address: String, time: String): ViewTickerSalesFragment {
            eventName = name
            eventAddress = address
            eventTime = time
            eventId = id
            instance = ViewTickerSalesFragment()
            return instance
        }
    }
}
