package com.hooleyapp.hooley.fragments.friends


import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.CompoundButton
import android.widget.Toast
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterSelectFriends
import com.hooleyapp.hooley.databinding.FragmentSelectFriendBinding
import com.hooleyapp.hooley.db.DbManager
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.messaging.ChatWindowFragment
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.FriendsModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.tables.FriendsTable

class SelectFriendFragment : BaseFragment(), CompoundButton.OnCheckedChangeListener, TextWatcher, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    internal var binding: FragmentSelectFriendBinding? = null
    internal var objectResponse: FriendsModel? = null
    private var checkedIndex = -1
    internal var isSwiped = false
    private var friendList: MutableList<FriendsTable>? = null
    private val mListSearch = ArrayList<FriendsTable>()
    private var service: FriendWebService? = null
    private var adapterSelectFriend: AdapterSelectFriends? = null
    private var intent: Intent? = null
    private var addedCount = 0
    private val REQUEST_CODE_ADD_HOST = 965
    var friendListIdsTemp: ArrayList<Int> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_friend, container, false)
        HooleyMain.activity!!.supportActionBar!!.show()
        getFriends(true)
        setListener()
        setHasOptionsMenu(true)
        return binding!!.root
    }

    fun setView() {
        when (HooleyApp.db.getString(Constants.CALL_TYPE)) {
            "ADD_HOST" -> {
                (HooleyMain.activity as HooleyMain).setToolbarTitle("Add Host")
                binding!!.rlFilter.visibility = View.GONE
            }
            "ADD_CO_HOST" -> {
                (HooleyMain.activity as HooleyMain).setToolbarTitle("Add Cohost")
                binding!!.rlFilter.visibility = View.GONE
            }
            "CREATE_MESSAGE" -> {
                (HooleyMain.activity as HooleyMain).setToolbarTitle("Create Message")
                binding!!.rlFilter.visibility = View.GONE
            }
            "TAG_FRIENDS" -> {
                (HooleyMain.activity as HooleyMain).setToolbarTitle("Tag Friends")
                binding!!.rlFilter.visibility = View.VISIBLE
            }
            "GIFT_FRIEND" -> {
                (HooleyMain.activity as HooleyMain).setToolbarTitle("Send as Gift")
                binding!!.rlFilter.visibility = View.GONE
            }
        }
    }

    private fun getFriendList() {
        if (friendList == null)
            friendList = ArrayList<FriendsTable>()
        if (friendList!!.size > 0)
            friendList!!.clear()
        friendList = DbManager.getInstance().friendList
        if (friendList!!.size > 0) {
            //set
            val friendsTable = FriendsTable()
            friendsTable.userId = Integer.parseInt(HooleyApp.db.getString(Constants.USER_ID))
            friendsTable.fullName = HooleyApp.db.getString(Constants.FULL_NAME)
            friendsTable.isChecked = true
            friendsTable.profilePic = HooleyApp.db.getString(Constants.USER_AVATAR)
            friendList!!.add(0, friendsTable)
            setRecyclerView()
            getFriends(false)
        } else {
            getFriends(true)
        }
    }

    private fun setListener() {
        binding!!.tbSelectAll.setOnCheckedChangeListener(this)
        binding!!.edtSearch.addTextChangedListener(this)
        binding!!.ivClose.setOnClickListener(this)
        binding!!.slSelection.setOnRefreshListener(this)
        binding!!.btnSelectFriend.setOnClickListener(this)
    }

    private fun getFriends(showDialog: Boolean) {
        if (showDialog && !isSwiped) {
            HooleyMain.activity!!.showDialog()
        }
        service = FriendWebService()
        service!!.getMyFriendsList(object : GenericCallback<FriendsModel> {
            override fun success(result: FriendsModel) {
                if (isSwiped) {
                    binding!!.slSelection.isRefreshing = false
                    isSwiped = false
                }
                objectResponse = result
                binding!!.rvSelectFriend.visibility = View.VISIBLE
                binding!!.tvNoData.visibility = View.GONE
                binding!!.btnSelectFriend.visibility = View.VISIBLE
                binding!!.rlSearchBar.visibility = View.VISIBLE
                binding!!.rlActionTabs.visibility = View.VISIBLE
                HooleyMain.activity!!.removeDialog()
                if (friendList == null)
                    friendList = ArrayList()
                else
                    friendList!!.clear()
                friendList!!.addAll(objectResponse!!.friendsList)


                if (friendListIds != null) {
                    binding!!.tbSelectAll.isChecked = friendListIds!!.size == friendList!!.size

                    for (i in friendList!!.indices) {
                        for (j in friendListIds!!.indices) {
                            if (friendList!![i].getUserId() == friendListIds!![j].getUserId()) {
                                friendList!![i].isChecked = true
                            }
                        }
                    }

                }


                if (HooleyApp.db.getString(Constants.CALL_TYPE) == "ADD_HOST") {
                    val friendsTable = FriendsTable()
                    friendsTable.userId = Integer.parseInt(HooleyApp.db.getString(Constants.USER_ID))
                    friendsTable.fullName = HooleyApp.db.getString(Constants.FULL_NAME)
                    friendsTable.isChecked = true
                    friendsTable.profilePic = HooleyApp.db.getString(Constants.USER_AVATAR)
                    friendList!!.add(0, friendsTable)
                }
                //                setView();
                setRecyclerView()

            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                if (isSwiped) {
                    binding!!.slSelection.isRefreshing = false
                    isSwiped = false
                }
                try {
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.btnSelectFriend.visibility = View.GONE
                    binding!!.rlSearchBar.visibility = View.GONE
                    binding!!.rlActionTabs.visibility = View.GONE
                    binding!!.tvNoData.text = message
                    binding!!.rvSelectFriend.visibility = View.GONE
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun setRecyclerView() {
        binding!!.tvPeopleCount.text = friendList!!.size.toString() + " Friends"
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding!!.rvSelectFriend.layoutAnimation = animation
        binding!!.rvSelectFriend.layoutManager = manager
        friendList!!.sortBy { it.fullName?.toString()!!.toLowerCase() }
        adapterSelectFriend = AdapterSelectFriends(HooleyMain.activity!!, friendList)
        binding!!.rvSelectFriend.adapter = adapterSelectFriend
    }

    private fun selectAll(check: Boolean) {
        if (friendList != null && friendList!!.size > 0) {
            for (i in friendList!!.indices) {
                friendList!![i].isChecked = check
                if (check)
                    addedCount++
                else
                    addedCount--
            }
        }
        if (adapterSelectFriend != null)
            adapterSelectFriend!!.notifyDataSetChanged()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()

        //HooleyApp.db.putString(Constants.CALL_TYPE, "");

    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
        inflater!!.inflate(R.menu.live_post_menu, menu)
    }


    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        selectAll(isChecked)
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

    }

    override fun afterTextChanged(s: Editable) {
        if (s.isNotEmpty()) {
            if (friendList == null)
                return
            friendList!!.clear()
            if (objectResponse != null && objectResponse!!.friendsList.size > 0) {
                for (i in 0 until objectResponse!!.friendsList.size) {
                    if (objectResponse!!.friendsList[i].fullName.toLowerCase().contains(s.toString().toLowerCase())) {
                        friendList!!.add(objectResponse!!.friendsList[i])
                    }
                }
            }

            if (friendList!!.size == 0) {
                binding!!.tvNoData.visibility = View.VISIBLE
                binding!!.tvNoData.text = "No Record Found"
            } else {
                binding!!.tvNoData.visibility = View.GONE
            }
            binding!!.tvPeopleCount.text = friendList!!.size.toString() + " Friends"
            if (adapterSelectFriend != null)
                adapterSelectFriend!!.notifyDataSetChanged()
        } else {
            binding!!.tvNoData.visibility = View.GONE
            if (friendList != null)
                friendList!!.clear()
            friendList!!.addAll(objectResponse!!.friendsList)
            binding!!.tvPeopleCount.text = friendList!!.size.toString() + " Friends"
            if (adapterSelectFriend != null)
                adapterSelectFriend!!.notifyDataSetChanged()
        }

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivClose -> {
                binding!!.edtSearch.setText("")
                Util.hideKeyboard(HooleyMain.activity!!)
            }

            R.id.btnSelectFriend -> {
                getSelectFriendsId()
                if (checkedIndex != -1) {
                    HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                    if (HooleyApp.db.getString(Constants.CALL_TYPE) == "CREATE_MESSAGE") {
                        (HooleyMain.activity as ActivityBase).callFragment(R.id.container, ChatWindowFragment("", friendList!![checkedIndex].userId.toString(), friendList!![checkedIndex].fullName), "ChatWindowFragment")
                    } else {
                        intent = Intent()
                        var tempList = arrayListOf<FriendsTable>()
                        tempList.addAll(friendListIds!!)
                        intent!!.putParcelableArrayListExtra("friendListIds", tempList)
                        targetFragment!!.onActivityResult(
                                targetRequestCode,
                                Activity.RESULT_OK,
                                intent
                        )
                        friendListIds!!.clear()
                    }
                } else {
                    if (HooleyApp.db.getString(Constants.CALL_TYPE) == "CREATE_MESSAGE") {
                        Toast.makeText(HooleyMain.activity, "Please select contact to Chat", Toast.LENGTH_SHORT).show()
                    } else if (HooleyApp.db.getString(Constants.CALL_TYPE) == "TAG_FRIENDS") {
                        Toast.makeText(HooleyMain.activity, "Please select friend to tag", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.share -> {
                getSelectFriendsId()
                if (checkedIndex != -1) {
                    HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                    if (HooleyApp.db.getString(Constants.CALL_TYPE) == "CREATE_MESSAGE") {
                        (HooleyMain.activity as ActivityBase).callFragment(R.id.container, ChatWindowFragment("", friendList!![checkedIndex].userId.toString(), friendList!![checkedIndex].fullName), "ChatWindowFragment")
                    } else {
                        intent = Intent()
                        var tempList = arrayListOf<FriendsTable>()
                        tempList.addAll(friendListIds!!)
                        intent!!.putParcelableArrayListExtra("friendListIds", tempList)
                        targetFragment!!.onActivityResult(
                                targetRequestCode,
                                Activity.RESULT_OK,
                                intent
                        )
                        friendListIds!!.clear()
                    }
                } else {
                    if (HooleyApp.db.getString(Constants.CALL_TYPE) == "CREATE_MESSAGE") {
                        Toast.makeText(HooleyMain.activity, "Please select contact to Chat", Toast.LENGTH_SHORT).show()
                    } else if (HooleyApp.db.getString(Constants.CALL_TYPE) == "TAG_FRIENDS") {
                        Toast.makeText(HooleyMain.activity, "Please select friend to tag", Toast.LENGTH_SHORT).show()
                    }
                }
                return super.onOptionsItemSelected(item)
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }


    private fun getSelectFriendsId() {
        if (friendListIds == null)
            friendListIds = ArrayList()
        if (friendListIds!!.size > 0)
            friendListIds!!.clear()
        if (friendList != null && friendList!!.size > 0) {
            for (i in friendList!!.indices) {
                if (friendList!![i].isChecked) {
                    checkedIndex = i
                    friendListIds!!.add(friendList!![i])
                }
            }
        }
    }

    override fun onRefresh() {
        binding!!.slSelection.isRefreshing = true
        isSwiped = true
        getFriends(true)
    }

    companion object {

        lateinit var instance: SelectFriendFragment
        var friendListIds: ArrayList<FriendsTable>? = null

        fun newInstance(): SelectFriendFragment {
            instance = SelectFriendFragment()
            return instance
        }

        fun newInstance(riendListIds: ArrayList<FriendsTable>?): SelectFriendFragment {
            instance = SelectFriendFragment()
            friendListIds = riendListIds
            return instance
        }
    }
}
