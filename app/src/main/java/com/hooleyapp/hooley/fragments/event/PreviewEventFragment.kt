package com.hooleyapp.hooley.fragments.event

import android.annotation.SuppressLint
import android.app.Dialog
import android.databinding.DataBindingUtil
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.text.TextUtils
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.*
import com.alimuzaffar.lib.pin.PinEntryEditText
import com.bumptech.glide.Glide
import com.daimajia.slider.library.Tricks.ViewPagerEx
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterWallpaperPager
import com.hooleyapp.hooley.apis.ApiRequestJson
import com.hooleyapp.hooley.databinding.FragmentEventPreviewBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.nearby.DashBoardFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.fragments.ticket.EventTicketSetupFragment
import com.hooleyapp.hooley.helper.hideKeyboard
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.*
import com.hooleyapp.hooley.model.staticData.Category
import com.hooleyapp.hooley.model.staticData.StaticDataModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.requestmodel.VerifyPhoneNumberModel
import com.hooleyapp.hooley.services.EventWebService
import com.hooleyapp.hooley.services.ProfileWebService
import com.hooleyapp.hooley.tables.FriendsTable
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Nauman on 12/28/2017.
 */

class PreviewEventFragment : BaseFragment(), View.OnClickListener, ViewPagerEx.OnPageChangeListener {
    private val service = EventWebService()
    internal var placeAddress: String? = null
    internal var editIndex: Int = 0
    internal var createIndex: Int = 0
    private var messageId: String? = ""
    private var verifyNumberDialog: Dialog? = null
    private var countDownTimer: CountDownTimer? = null
    internal var isValid = true
    private val profileWebService = ProfileWebService()
    private var moreDialogeFragment: MoreDialogeFragment? = null
    internal lateinit var binding: FragmentEventPreviewBinding
    private var adapter: AdapterWallpaperPager? = null
    var isPaid: Boolean = false
    private var fullDetail: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_event_preview, container, false)
        setToolbar()
        setHasOptionsMenu(true)
        isPaid = when (eventType) {
            "1" -> false
            "2" -> true
            else -> true
        }

        if (eventType == "3") {
            binding.btnSetUpTicket.text = "Setup Tickets & Donation"
            binding.llAge.visibility = View.GONE
        }
        setMoreCallerBackListener()
        if (editEventModel != null) {
            if (editEventModel!!.eventBasicInfo!!.isPublished) {
                binding.btnPublishEvent.text = "Update Event"
                binding.btnPublishEvent.visibility = VISIBLE
                binding.btnSetUpTicket.visibility = View.GONE
                binding.btnSaveAsUnPublishEvent.visibility = View.GONE

            } else {
                eventType = editEventModel!!.eventBasicInfo!!.eventType + ""
                if (eventType == "3") {
                    binding.btnSetUpTicket.text = "Setup Tickets & Donation"
                    binding.btnSetUpTicket.visibility = VISIBLE
                    binding.btnSaveAsUnPublishEvent.visibility = VISIBLE
                    binding.llAge.visibility = View.GONE
                    binding.btnPublishEvent.visibility = GONE
                } else if (eventType == "2") {
                    binding.btnSetUpTicket.visibility = VISIBLE
                    binding.btnPublishEvent.visibility = GONE
                    binding.btnSaveAsUnPublishEvent.visibility = VISIBLE
                } else {
                    binding.btnSetUpTicket.visibility = GONE

                }
            }

            binding.tvHashes.text = getHashes(editEventModel!!.eventBasicInfo!!.eventHashs)

        }
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu?.clear()
    }


    private fun getHashes(eventHashs: ArrayList<String>): String {
        return eventHashs.joinToString().replace(",", " ")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        HooleyMain.activity!!.hideKeyboard()
        setUiData()
        setListener()
    }

    private fun setListener() {
        binding.btnEditEvent.setOnClickListener(this)
        binding.btnPublishEvent.setOnClickListener(this)
        binding.btnSaveAsUnPublishEvent.setOnClickListener(this)
        binding.btnSetUpTicket.setOnClickListener(this)
    }

    @SuppressLint("SetTextI18n")
    private fun setUiData() {
//        if (HooleyApp.db.getBoolean(Constants.IS_PUBLISH_EVENT))
//            userSearchbinding.btnSaveAsUnPublishEvent.visibility = View.GONE
//        else
//            userSearchbinding.btnSaveAsUnPublishEvent.visibility = View.VISIBLE

        if (hostList.size > 0) {
            if (!TextUtils.isEmpty(hostList[0].profilePic))
                Glide.with(HooleyMain.activity!!).load(hostList[0].profilePic).into(binding.ivAvatar)
//            userSearchbinding.tvUserName.text = hostList[0].fullName

        } else {
            if (!TextUtils.isEmpty(HooleyApp.db.getString(Constants.USER_AVATAR)))
                Glide.with(HooleyMain.activity!!).load(HooleyApp.db.getString(Constants.USER_AVATAR)).into(binding.ivAvatar)
//            if (!TextUtils.isEmpty(HooleyApp.db.getString(Constants.FULL_NAME)))
//                userSearchbinding.tvUserName.text = HooleyApp.db.getString(Constants.FULL_NAME)
        }

        if (!eventHashsString.isNullOrEmpty()) {
            binding.llHash.visibility = VISIBLE
            binding.tvHashes.text = eventHashsString
        }

        if (editEventModel != null) {
            binding.tvMinAge.text = "Minimum Age " + getAgeName(editEventModel!!.eventBasicInfo!!.minimumAgeId!!)
        } else {
            binding.tvMinAge.text = "Minimum Age $selectedAge"
        }
        if (editEventModel == null) {
            binding.tvEventDate.text = DateUtils.convertDateNew(DateUtils.SendDateToDate(eventStartTime), DateUtils.SendDateToDate(eventEndTime))
            if (!TextUtils.isEmpty(eventName))
                binding.tvEventName.text = eventName
            if (!TextUtils.isEmpty(eventDetail))
                binding.tvEventDetail.text = eventDetail

            if (eventType == "3") {
                fullDetail = "Charity Event"
            } else {
                fullDetail = if (!isPublic)
                    "Private Event"
                else
                    "Public Event"
            }
            if (isPaid) {
                binding.btnPublishEvent.visibility = View.GONE
                binding.btnSetUpTicket.visibility = VISIBLE
                binding.tvEventType.text = fullDetail
                binding.btnSaveAsUnPublishEvent.visibility = VISIBLE

            } else {
                binding.tvEventType.text = fullDetail
                binding.btnSetUpTicket.visibility = View.GONE
                binding.btnPublishEvent.visibility = VISIBLE
                binding.btnSaveAsUnPublishEvent.visibility = VISIBLE

            }
        } else {
            binding.tvEventDate.text = DateUtils.convertDateNew(DateUtils.SendDateToDate(editEventModel!!.eventBasicInfo!!.eventStartTime!!), DateUtils.SendDateToDate(editEventModel!!.eventBasicInfo!!.eventEndTime!!))
//            binding.tvEventDate.text = DateUtils.convertDateNew(DateUtils.getLocalDate(editEventModel!!.eventBasicInfo!!.eventStartTime!!), DateUtils.getLocalDate(editEventModel!!.eventBasicInfo!!.eventEndTime!!))
            if (!TextUtils.isEmpty(editEventModel!!.eventBasicInfo!!.eventName)) {
                binding.tvEventName.text = editEventModel!!.eventBasicInfo!!.eventName
                eventName = editEventModel!!.eventBasicInfo!!.eventName!!
            }
            if (TextUtils.isEmpty(editEventModel!!.eventBasicInfo!!.eventDetails))
                binding.tvEventDetail.text = editEventModel!!.eventBasicInfo!!.eventName
            else
                binding.tvEventDetail.text = editEventModel!!.eventBasicInfo!!.eventDetails

            if (editEventModel!!.eventBasicInfo!!.eventType!! == "3") {
                fullDetail = "Charity Event"
            } else {
                fullDetail = if (editEventModel!!.eventBasicInfo!!.isPrivate)
                    "Private Event"
                else
                    "Public Event"
            }
        }
        binding.tvEventType.text = fullDetail

//
//            if (editEventModel!!.eventBasicInfo!!.isPaid)

//            if (editEventModel!!.eventBasicInfo!!.inviteFriend)
//                userSearchbinding.tvAllowGuest.text = "Yes"
//            else
//                userSearchbinding.tvAllowGuest.text = "No"

        initPager()
        setOptCat()
        setEventType()
        setAddress()
        setCoHostRecyclerView()
    }

    var model = StaticDataModel()
    internal var gson = Gson()


    private fun getAgeName(minimumAgeId: String): String {

        model = gson.fromJson(HooleyApp.db.getString(Constants.STATIC_MODEL), StaticDataModel::class.java)
        for (i in model.minimumAgeList!!.indices) {
            if (model.minimumAgeList!![i].id.equals(minimumAgeId)) {
                selectedAge = model.minimumAgeList!![i].name!!
                return selectedAge
            }
        }
        return ""
    }


    private fun initPager() {
        adapter = AdapterWallpaperPager()
        binding.vpWallPaper.adapter = adapter
        for (i in eventCoverList!!.indices) {
            addView(eventCoverList!![i])
        }
    }

    private fun setMoreCallerBackListener() {
        val arrayList = ArrayList<String>()
        arrayList.add("Location Based Event")
        arrayList.add("Pay-Per-View")
        arrayList.add("Charity Event")
        moreDialogeFragment = MoreDialogeFragment.Builder(activity!!).setTitle("Select Event Type").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, _ ->
            when (position) {
                0 -> {
                    moreDialogeFragment!!.dismiss()
                }
                1 -> {
                    moreDialogeFragment!!.dismiss()
                }
                2 -> {
                    moreDialogeFragment!!.dismiss()
                }
            }
        }.create()
    }

    //-----------------------------------------------------------------------------
    // Here's what the app should do to add a view to the ViewPager.
    fun addView(item: ItemCreateEventWallpaper) {
        val pageIndex = adapter!!.addView(item)
        // You might want to make "newPage" the currently displayed page:
        adapter!!.notifyDataSetChanged()
        binding.vpWallPaper.setCurrentItem(pageIndex, true)
    }

    var arrayList = ArrayList<String>()
    private fun setCoHostRecyclerView() {
        if (editEventModel == null) {
            if (coHOstList!!.size > 0) {
                binding.llCoHosts.visibility = VISIBLE
                for (i in coHOstList!!.indices) {
                    arrayList.add(coHOstList!![i].fullName)
                }
                binding.tvCohost1.text = arrayList.joinToString()
            }
        } else {
            if (editEventModel!!.eventCoHostProfileList.size > 0) {
                if (coHOstList != null)
                    coHOstList!!.clear()
                for (i in editEventModel!!.eventCoHostProfileList.indices) {
                    coHOstList!!.add(editEventModel!!.eventCoHostProfileList[i])
                }
                if (coHOstList!!.size > 0) {
                    binding.llCoHosts.visibility = VISIBLE
                    for (i in coHOstList!!.indices) {
                        arrayList.add(coHOstList!![i].fullName)
                    }
                    binding.tvCohost1.text = arrayList.joinToString()
                }
            }
        }
    }

    private fun setAddress() {
        if (editEventModel == null) {
            if (geofenceList != null && geofenceList!!.size > 0) {
                placeAddress = geofenceList!![0].address
                if (!TextUtils.isEmpty(placeAddress)) {
                    binding.tvEventAddress.text = placeAddress
                }
            }
        } else {
            if (editEventModel!!.eventGeofenceList.size > 0) {
                placeAddress = editEventModel!!.eventGeofenceList[0].address
                if (!TextUtils.isEmpty(placeAddress)) {
                    binding.tvEventAddress.text = placeAddress
                }
            }
        }
    }

    private fun setEventType() {
        val builder = StringBuilder()
        if (editEventModel == null) {
            if (catList.size > 0) {
                if (checkActiveForCreate(catList)) {
                    builder.append(catList[createIndex].name)
                } else {
                    for (i in catList.indices) {
                        if (catList[i].isActive) {
                            builder.append(catList[i].name + ", ")
                        }
                    }
                }
                binding.tvEventCategory.text = builder
            }
        } else {
            if (editEventModel!!.eventCategoryList.size > 0) {
                if (checkActiveForEdit(editEventModel!!.eventCategoryList)) {
                    builder.append(editEventModel!!.eventCategoryList[editIndex].Name)
                } else {
                    for (i in editEventModel!!.eventCategoryList.indices) {
                        if (editEventModel!!.eventCategoryList[i].isAdded) {
                            builder.append(editEventModel!!.eventCategoryList[i].Name!! + ",")
                        }
                    }
                    binding.tvEventCategory.text = builder
                }
            }
        }
    }

    private fun checkActiveForEdit(categories: ArrayList<EditEventModel.EventCategory>): Boolean {
        var count = 0
        for (i in categories.indices) {
            if (categories[i].isAdded) {
                editIndex = i
                count++
            }
        }
        return count == 1
    }

    private fun checkActiveForCreate(categories: ArrayList<Category>): Boolean {
        var count = 0
        for (i in categories.indices) {
            if (categories[i].isActive) {
                createIndex = i
                count++
            }
        }

        return count == 1
    }

    fun addChild(layout: LinearLayout, name: String) {
        val child = HooleyMain.activity!!.layoutInflater.inflate(R.layout.child_preview_cat, null)
        (child.findViewById<View>(R.id.tvCatName) as TextView).text = name
        layout.addView(child)
    }

    private fun setOptCat() {
        val builder = StringBuilder()
        if (editEventModel == null) {
            if (optionalCat != null && optionalCat!!.size > 0) {
                if (optionalCat!!.size == 1) {
                    builder.append(optionalCat!![0])
                } else {
                    for (i in optionalCat!!.indices) {
                        if (i == optionalCat!!.size - 1) {
                            builder.append(optionalCat!![i])
                        } else {
                            builder.append(optionalCat!![i] + ", ")
                        }
                    }
                }
                binding.tvOptCat.text = builder.toString()
                binding.llOptCat.visibility = VISIBLE
            }

        } else {
            if (editEventModel!!.eventOptionalCategoryList.size > 0) {

                if (editEventModel!!.eventOptionalCategoryList.size == 1) {
                    builder.append(editEventModel!!.eventOptionalCategoryList[0].categoryName)
                } else {
                    for (i in editEventModel!!.eventOptionalCategoryList.indices) {
                        if (i == editEventModel!!.eventCategoryList.size - 1) {
                            builder.append(editEventModel!!.eventOptionalCategoryList[i].categoryName)
                        } else {
                            builder.append(editEventModel!!.eventOptionalCategoryList[i].categoryName!! + ",")
                        }
                    }
                }
                binding.tvOptCat.text = builder.toString()
                binding.llOptCat.visibility = VISIBLE
            }
        }

    }

    private fun setToolbar() {
        HooleyMain.activity!!.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        HooleyMain.activity!!.supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
    }

    private fun VerifyNumberDialog(number: String) {
        isValid = true
        verifyNumberDialog = Dialog(HooleyMain.activity!!)
        verifyNumberDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        verifyNumberDialog!!.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        verifyNumberDialog!!.setContentView(R.layout.dialog_verify_number)
        val btnVerifyCode = verifyNumberDialog!!.findViewById<Button>(R.id.btnVerifyCode)
        val ivClose = verifyNumberDialog!!.findViewById<ImageView>(R.id.ivClose)
        val tvPhoneNumber = verifyNumberDialog!!.findViewById<TextView>(R.id.tvPhoneNumber)
        val tvResendCode = verifyNumberDialog!!.findViewById<TextView>(R.id.tvResendCode)
        val tvTimer = verifyNumberDialog!!.findViewById<TextView>(R.id.tvTimer)
        val rlResendCode = verifyNumberDialog!!.findViewById<RelativeLayout>(R.id.rlResendCode)
        tvPhoneNumber.text = number
        tvResendCode.setOnClickListener { verifyNumber() }
        val pincode = verifyNumberDialog!!.findViewById<PinEntryEditText>(R.id.pinCode)

        ivClose.setOnClickListener { verifyNumberDialog!!.dismiss() }

        btnVerifyCode.setOnClickListener(View.OnClickListener {
            if (Util.isContentNull(pincode.text.toString())) {
                pincode.requestFocus()
                pincode.error = getString(R.string.required)
                return@OnClickListener
            }

            if (TextUtils.isEmpty(messageId)) {
                Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_server_not_responding))
                return@OnClickListener
            }
            if (isValid) {
                verifyPinCode(pincode.text.toString())
                verifyNumberDialog!!.dismiss()
            } else {
                Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_invalid_code))
            }
        })

        val currentDateTimeString = SimpleDateFormat("HH:mm:ss").format(Date())
        val format = SimpleDateFormat("HH:mm:ss")
        var date2: Date? = null
        try {
            date2 = format.parse(currentDateTimeString)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        countDownTimer = object : CountDownTimer((60 * 3 * 1000).toLong(), 100) {
            override fun onTick(l: Long) {
                val seconds = l / 1000
                val currentMinute = seconds / 60
                tvTimer.text = String.format("%02d", seconds / 60) + ":" + String.format("%02d", seconds % 60)
                if (currentMinute == 0L && seconds == 0L) {
                    rlResendCode.visibility = VISIBLE
                    isValid = false
                    tvTimer.text = "00:00"
                    if (countDownTimer != null) {
                        countDownTimer!!.cancel()
                    }
                } else {

                }
            }

            override fun onFinish() {
                rlResendCode.visibility = VISIBLE
                tvTimer.text = "00:00"
                isValid = false
                if (countDownTimer != null) {
                    countDownTimer!!.cancel()
                }
            }
        }.start()
        verifyNumberDialog!!.show()
    }


    private fun verifyPinCode(code: String) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        HooleyMain.activity!!.showDialog()
        profileWebService.verifyPinCode(messageId!!, code, false, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                HooleyApp.db.putBoolean(Constants.IS_PHONE_VERIFIED, true)
                if (countDownTimer != null) {
                    countDownTimer!!.cancel()
                }
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }

    private fun verifyNumber() {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        HooleyMain.activity!!.showDialog()
        profileWebService.verifyNumber(HooleyApp.db.getString(Constants.VERIFIED_PHONE_NUMBER), HooleyApp.db.getString(Constants.COUNTRY_CODE), HooleyApp.db.getString(Constants.COUNTRY_CODE) + HooleyApp.db.getString(Constants.VERIFIED_PHONE_NUMBER), object : GenericCallback<VerifyPhoneNumberModel> {
            override fun success(result: VerifyPhoneNumberModel) {
                HooleyMain.activity!!.removeDialog()
                messageId = result.apiMessageId
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_server_not_responding))
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun createEvent() {
        if (isPaid && !HooleyApp.db.getBoolean(Constants.IS_PHONE_VERIFIED)) {
            verifyNumber()
            VerifyNumberDialog("+" + HooleyApp.db.getString(Constants.VERIFIED_PHONE_NUMBER))
            return
        }
        HooleyMain.activity!!.showDialog()
        if (hostList.size == 0) {
            val table = FriendsTable()
            table.userId = Integer.parseInt(HooleyApp.db.getString(Constants.USER_ID))
            hostList.add(table)
        }
        HooleyApp.db.putInt(Constants.SHOW_LIVE_EVENT_TAB, 2)
        service.createEvent(eventCoverList!!,
                eventName,
                eventType,
                eventDetail,
                eventHashes,
                eventStartTime,
                eventEndTime,
                isPublic,
                isPublish,
                optionalCat!!,
                coHOstList!!,
                catList,
                geofenceList!!,
                ageId,
                isUpdate!!, object : IWebServiceCallback<SaveEventModel> {
            override fun success(result: SaveEventModel) {
                HooleyMain.activity!!.removeDialog()
                if (result.success) {
                    Logger.ex("publishEvent:$result")
                    HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                    val r = Runnable {
                        (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, ConfirmCreateEventFragment.newInstance(result.eventObject.id!!, eventName), null)
                    }
                    if (isPublish)
                        Handler().postDelayed(r, 250)
                    else
                        (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, DashBoardFragment(), null)

                } else {
                    Util.showToastMessage(HooleyMain.activity!!, result.exception)
                }
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.unbind()
        if (editEventModel != null) {
            editEventModel = null
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnEditEvent -> HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
            R.id.btnPublishEvent -> {
                HooleyMain.activity!!.hideKeyboard()
                isPublish = true
                if (editEventModel == null) {
                    createEvent()
                } else {
                    updateEvent()
                }
            }
            R.id.btnSaveAsUnPublishEvent -> {
                HooleyMain.activity!!.hideKeyboard()
                isPublish = false
                if (editEventModel == null) {
                    createEvent()
                } else {
                    updateEvent()
                }
            }
            R.id.btnSetUpTicket -> {
//                moreDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
                if (editEventModel != null) {
                    (HooleyMain.activity as ActivityBase).addFragment(R.id.container, EventTicketSetupFragment.newInstance(eventCoverList!!, editEventModel!!, eventId!!, "true"), "EventTicketSetupFragment")
                } else {
                    var json = ApiRequestJson.getInstance().saveEventDataString(eventCoverList!!,
                            "",
                            eventName,
                            eventType,
                            eventDetail,
                            eventHashes,
                            eventStartTime,
                            eventEndTime,
                            isPublic,
                            saveAsUnpublished,
                            optionalCat,
                            coHOstList,
                            catList,
                            geofenceList!!,
                            ageId)
                    (HooleyMain.activity as ActivityBase).addFragment(R.id.container, EventTicketSetupFragment.newInstance(eventCoverList!!, eventName, eventStartTime, eventEndTime, json, eventType), "EventTicketSetupFragment")

                }
            }
        }
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {

    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    private fun updateEvent() {
        HooleyMain.activity!!.showDialog()
        HooleyApp.db.putInt(Constants.SHOW_LIVE_EVENT_TAB, 2)
        service.updateEvent(eventCoverList!!, editEventModel!!, eventId!!, isPublish, isUpdate!!, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                val r = Runnable {
                    (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, ConfirmCreateEventFragment.newInstance(eventId!!, eventName), null)
                }
                if (isPublish)
                    Handler().postDelayed(r, 250)
                else
                    (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, DashBoardFragment(), null)

                editEventModel = null
//                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
//                HooleyMain.activity!!.onBackPressed()
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.removeDialog()
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    companion object {

        lateinit var instance: PreviewEventFragment
        lateinit var eventName: String
        lateinit var eventDetail: String
        lateinit var eventStartTime: String
        lateinit var eventEndTime: String
        lateinit var ageId: String
        var eventType: String = ""
        var eventHashsString: String = ""
        var selectedAge: String = ""
        var isPublic: Boolean = false
        var saveAsUnpublished: Boolean = false
        private var optionalCat: ArrayList<String>? = null
        private var hostList = ArrayList<FriendsTable>()
        private var coHOstList: ArrayList<FriendsTable>? = ArrayList()
        private lateinit var catList: ArrayList<Category>
        private var geofenceList: ArrayList<GeoFenceModel>? = null
        private var eventCoverList: ArrayList<ItemCreateEventWallpaper>? = null
        private var eventId: String? = null
        private var editEventModel: EditEventModel? = null
        private var isUpdate: String? = null
        private var isPublish: Boolean = false
        var eventHashes = ArrayList<String>()


        fun newInstance(coverList: ArrayList<ItemCreateEventWallpaper>,
                        name: String,
                        eventTypes: String,
                        detail: String,
                        eventHashs: ArrayList<String>,
                        event_hsh_string: String,
                        starttime: String,
                        endTime: String,
                        ispublic: Boolean,
                        saveasunpublish: Boolean,
                        optionalcat: ArrayList<String>,
                        cohostlist: ArrayList<FriendsTable>,
                        catlist: ArrayList<Category>,
                        geofencelist: ArrayList<GeoFenceModel>,
                        ageid: String,
                        selectedage: String,
                        update: String): PreviewEventFragment {
            eventCoverList = coverList
            eventName = name
            eventType = eventTypes
            eventDetail = detail
            eventHashes = eventHashs
            eventHashsString = event_hsh_string
            eventStartTime = starttime
            eventEndTime = endTime
            isPublic = ispublic
            saveAsUnpublished = saveasunpublish
            optionalCat = optionalcat
            coHOstList = cohostlist
            selectedAge = selectedage
            catList = catlist
            geofenceList = geofencelist
            ageId = ageid
            isUpdate = update
            instance = PreviewEventFragment()
            return instance
        }


        fun newInstance(coverList: ArrayList<ItemCreateEventWallpaper>, model: EditEventModel, id: String, update: String): PreviewEventFragment {
            eventCoverList = coverList
            editEventModel = model
            eventId = id
            isUpdate = update
            instance = PreviewEventFragment()
            return instance
        }
    }
}
