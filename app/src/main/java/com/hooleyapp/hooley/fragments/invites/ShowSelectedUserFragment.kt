package com.hooleyapp.hooley.fragments.invites

import android.annotation.SuppressLint
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterShowSelectedUser
import com.hooleyapp.hooley.databinding.FragmentShowSelectedUserBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.setToolBarTitle
import com.hooleyapp.hooley.tables.FriendsTable

@SuppressLint("ValidFragment")
class ShowSelectedUserFragment constructor(var selectedUserList: ArrayList<FriendsTable>, var showCheckBox: Boolean) : BaseFragment(), AdapterShowSelectedUser.IInviteHooleyFriendClickListener {
    lateinit var binding: FragmentShowSelectedUserBinding
    private lateinit var adapter: AdapterShowSelectedUser

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_show_selected_user, container, false)
        setToolbar()
        setRecyclerView()
        return binding.root
    }

    fun setToolbar() {
        if (showCheckBox)
            setToolBarTitle(HooleyMain.activity!!, "Invite Guests")
        else
            setToolBarTitle(HooleyMain.activity!!, "Interested Friends")
    }

    private fun setRecyclerView() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        binding.rvSelectedUsers.layoutAnimation = animation
        binding.rvSelectedUsers.layoutManager = manager
        adapter = AdapterShowSelectedUser(HooleyMain.activity!!, selectedUserList, showCheckBox)
        adapter.mListener = this
        binding.rvSelectedUsers.adapter = adapter
    }


    override fun onClickCheckItem(position: Int) {
        selectedUserList.remove(selectedUserList[position])
        if (::adapter.isInitialized) {
            adapter.notifyDataSetChanged()
        }
        if (selectedUserList.size == 0)
            HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding.unbind()
    }


}