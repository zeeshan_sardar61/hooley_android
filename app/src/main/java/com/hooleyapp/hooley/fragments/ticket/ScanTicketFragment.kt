package com.hooleyapp.hooley.fragments.ticket

import android.app.Dialog
import android.databinding.DataBindingUtil
import android.graphics.PointF
import android.graphics.drawable.ColorDrawable
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import com.dlazaro66.qrcodereaderview.QRCodeReaderView
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentTicketScanningBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.ScanTicketResponseModel
import com.hooleyapp.hooley.services.TicketWebService

class ScanTicketFragment : BaseFragment(), QRCodeReaderView.OnQRCodeReadListener {
    lateinit var qRDialog: Dialog
    var binding: FragmentTicketScanningBinding? = null
    var ticketWebService = TicketWebService()
    lateinit var eventId: String
    lateinit var eventName: String
    lateinit var price: String
    lateinit var startTime: String
    lateinit var endTime: String
    lateinit var ticketId: String
    internal var isScanned = false
    internal var mediaPlayer: MediaPlayer? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ticket_scanning, container, false)
        HooleyMain.activity!!.supportActionBar!!.hide()
        binding!!.ivBack.setOnClickListener {
            HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
        }
        initializeScanner()
        startAnimation()
        return binding!!.root
    }

    private fun initializeScanner() {
        binding!!.qrCodeReader.setOnQRCodeReadListener(this)
        binding!!.qrCodeReader.setQRDecodingEnabled(true)
        binding!!.qrCodeReader.setAutofocusInterval(500L)
        binding!!.qrCodeReader.setTorchEnabled(true)
        binding!!.qrCodeReader.setBackCamera()
    }

    private fun startAnimation() {
        val topToBottom = AnimationUtils.loadAnimation(HooleyMain.activity, R.anim.top_to_bottom)
        val bottomToTop = AnimationUtils.loadAnimation(HooleyMain.activity, R.anim.bottom_to_top)
        bottomToTop.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}

            override fun onAnimationEnd(animation: Animation) {
                binding!!.ivScannerAnim.startAnimation(topToBottom)
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })
        topToBottom.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}

            override fun onAnimationEnd(animation: Animation) {
                binding!!.ivScannerAnim.startAnimation(bottomToTop)
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })
        binding!!.ivScannerAnim.startAnimation(topToBottom)
    }


    override fun onResume() {
        super.onResume()
        binding!!.qrCodeReader.startCamera()

    }

    override fun onPause() {
        super.onPause()
        binding!!.qrCodeReader.stopCamera()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onQRCodeRead(text: String, points: Array<PointF>) {
        //        userSearchbinding.qrCodeReader.stopCamera();
        Logger.ex("Scanned Result$text")
        //        Util.showToastMessage(HooleyMain.activity, text);
        if (!isScanned) {
            playSound()
            handleQrResult(text)
        }
    }

    private fun setupDialog(status: String?) {
        qRDialog = Dialog(HooleyMain.activity!!)
        qRDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        qRDialog.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        qRDialog.setContentView(R.layout.dialog_scan_ticket)
        val tvContent = qRDialog.findViewById<TextView>(R.id.tvContent)
        val tvOK = qRDialog.findViewById<TextView>(R.id.tvOk)
        tvContent.text = status
        tvOK.setOnClickListener {
            qRDialog.dismiss()
            isScanned = false
            Handler().postDelayed({
                //                        userSearchbinding.qrCodeReader.startCamera();
            }, 200)
        }
        qRDialog.show()
    }

    private fun handleQrResult(text: String) {
        isScanned = true
        if (!text.contains("%")) {
            setupDialog("Invalid Qr Code")
            return
        }
        val txtString = text.split("%".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        try {

            for (i in txtString.indices) {
                Logger.ex("Result " + i + " " + txtString[i])
                val testString = txtString[i].split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                when (i) {
                    0 -> eventName = testString[1]
                    2 -> price = testString[1]
                    3 -> startTime = testString[1]
                    4 -> endTime = testString[1]
                    5 -> eventId = testString[1]
                    6 -> ticketId = testString[1]
                }
            }
        } catch (ex: IndexOutOfBoundsException) {
            ex.printStackTrace()
            return
        }

        try {
            scanTicket()
        } catch (ex: IndexOutOfBoundsException) {
            ex.printStackTrace()
        }

    }

    fun scanTicket() {
        ticketWebService.scanTicket(eventId, eventName, price, startTime, endTime, ticketId, object : GenericCallback<ScanTicketResponseModel> {
            override fun success(result: ScanTicketResponseModel) {
                setupDialog(result.status)
            }

            override fun failure(message: String) {
                try {
                    setupDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    fun playSound() {
        mediaPlayer = MediaPlayer()
        try {
            val afd = HooleyMain.activity!!.assets.openFd("ic_scan_beep.mp3")
            mediaPlayer!!.setDataSource(afd.fileDescriptor, afd.startOffset, afd.length)
            afd.close()
            mediaPlayer!!.prepare()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mediaPlayer!!.start()
    }

    companion object {


        lateinit var instance: ScanTicketFragment


        fun newInstance(): ScanTicketFragment {
            instance = ScanTicketFragment()
            return instance
        }
    }
}
