package com.hooleyapp.hooley.fragments.friends

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterFriendsRequestCardPager
import com.hooleyapp.hooley.databinding.CardPagerFragmentBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.FriendClickListener
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.FriendRequestNotificationModel
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.FriendWebService
import java.util.*

/**
 * M
 * Created by Nauman on 4/11/2018.
 */

class FriendRequestCardPager : BaseFragment(), FriendClickListener {

    var binding: CardPagerFragmentBinding? = null
    private var service: FriendWebService? = null
    private var adapterCardPager: AdapterFriendsRequestCardPager? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.card_pager_fragment, container, false)
        HooleyMain.activity!!.supportActionBar!!.hide()
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        service = FriendWebService()
        if (tagList != null && tagList!!.size > 0) {
            setCardAdapter()
        }
    }

    private fun setCardAdapter() {
        adapterCardPager = AdapterFriendsRequestCardPager(HooleyMain.activity!!, tagList!!)
        adapterCardPager!!.setListener(this)
        binding!!.cardPager.adapter = adapterCardPager
        binding!!.cardPager.currentItem = position
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
        HooleyMain.activity!!.supportActionBar!!.show()
    }

    override fun onClickViewProfile(friendId: Int, friendName: String) {
        HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
        (HooleyMain.activity as ActivityBase).callFragment(R.id.container, FriendsProfileFragment.newInstance(friendId, friendName), "FriendsProfileFragment")

    }

    override fun addFriend(friendId: Int) {

    }

    override fun AcceptRequest(position: Int, friendId: Int, value: Boolean) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        service!!.acceptIgnoreFriendRequest(friendId, value, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    companion object {
        lateinit var instance: FriendRequestCardPager
        private var tagList: ArrayList<FriendRequestNotificationModel.FriendRequest>? = null
        private var position: Int = 0

        fun newInstance(pos: Int, tagUserList: ArrayList<FriendRequestNotificationModel.FriendRequest>): FriendRequestCardPager {
            position = pos
            tagList = tagUserList
            instance = FriendRequestCardPager()
            return instance
        }
    }
}
