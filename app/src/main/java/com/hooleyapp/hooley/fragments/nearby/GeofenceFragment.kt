package com.hooleyapp.hooley.fragments.nearby


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.UiModeManager.MODE_NIGHT_YES
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.content.res.Resources
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.*
import android.widget.*
import com.facebook.FacebookSdk.getApplicationContext
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentGeofenceBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.PersonalProfileModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.EventWebService
import java.io.IOException
import java.text.DecimalFormat
import java.util.*


class GeofenceFragment : BaseFragment(), OnMapReadyCallback, View.OnClickListener, SeekBar.OnSeekBarChangeListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, ResultCallback<LocationSettingsResult>, GoogleMap.OnMapLongClickListener, View.OnTouchListener {

    protected var mGoogleApiClient: GoogleApiClient? = null
    lateinit var mLocationRequest: LocationRequest
    lateinit var mLocationSettingsRequest: LocationSettingsRequest
    protected var mCurrentLocation: Location? = null
    internal var binding: FragmentGeofenceBinding? = null
    var mRequestingLocationUpdates: Boolean? = true
    var mGooglemap: GoogleMap? = null
    lateinit var locationManager: LocationManager
    lateinit var mapFragment: SupportMapFragment
    var ct = "CT_" + javaClass.simpleName
    private var mapCircle: Circle? = null
    private var circleLat: Double? = null
    //polyLines
    private var circleLon: Double? = null
    private var mGestureDetector: GestureDetector? = null
    private val mLatlngs = ArrayList<LatLng>()
    private var mPolylineOptions: PolylineOptions? = null
    private var mPolygonOptions: PolygonOptions? = null
    private var mDrawFinished = false
    private val perMile = 1609.34
    private var intent: Intent? = null
    private var isCircle: Boolean = false
    private var showInfoDialog: Dialog? = null
    private var displayInfoDialog = true
    private var currentProgress = 100.0f
    var handler = Handler()
    var service = EventWebService()


    private val currentCountry: String
        get() {
            var mCountry = ""
            var geocoder = Geocoder(getApplicationContext(), Locale.getDefault())
            val addresses: List<Address>?
            try {
                addresses = geocoder.getFromLocation(java.lang.Double.parseDouble(HooleyApp.db.getString(Constants.CURRENT_LAT)), java.lang.Double.parseDouble(HooleyApp.db.getString(Constants.CURRENT_LANG)), 1)
                if (addresses != null && addresses.isNotEmpty()) {
                    val address = addresses[0].getAddressLine(0)
                    val address11 = addresses[0].getAddressLine(1)
                    val city = addresses[0].locality
                    mCountry = addresses[0].countryName
                    Log.e("Address_c>>", address)

                }
            } catch (e: IOException) {
            }

            return mCountry
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_geofence, container, false)
        initMap()
        createLocationRequest()
        buildLocationSettingsRequest()
        checkLocationSettings()
        setHasOptionsMenu(true)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPlaces()
        setListener()
    }

    private fun initPlaces() {
//        autocompleteFragment = childFragmentManager.findFragmentById(R.id.place_autocomplete_fragment) as SupportPlaceAutocompleteFragment

        val autocompleteFragment = childFragmentManager.findFragmentById(R.id.place_autocomplete_fragment) as AutocompleteSupportFragment
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS))
        autocompleteFragment.view!!.visibility = View.GONE

        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                // TODO: Get info about the selected place.
                Log.i("TAG", "Place: " + place.name)
                Log.d("POSITION", place.latLng.toString() + "")

                circleLat = place.latLng!!.latitude
                circleLon = place.latLng!!.longitude
                try {
                    binding!!.tvLocationName.text = (HooleyMain.activity as ActivityBase).getCompleteAddressString(circleLat!!, circleLon!!)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

                val loc = LatLng(place.latLng!!.latitude, place.latLng!!.longitude)
                if (mGooglemap != null) {
//                    mGooglemap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 11.5f))
                    addCircle(currentProgress.toDouble(), circleLat!!, circleLon!!)
                    val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
                    val animateZomm = currentZoomLevel + 5
                    mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat!!, circleLon!!), animateZomm))
                    mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
                }
            }

            override fun onError(status: Status) {
                // TODO: Handle the error.
                Log.i("TAG", "An error occurred: $status")
            }
        })

        val root = autocompleteFragment.view!!

        binding!!.tvLocationName.setOnClickListener {
            root.findViewById<EditText>(R.id.places_autocomplete_search_input)
                    .performClick()
        }
    }

    fun setListener() {
        binding!!.ivMyLocation.setOnClickListener(this)
        binding!!.btnSaveMapLocation.setOnClickListener(this)
        binding!!.sbGeofence.setOnSeekBarChangeListener(this)
        binding!!.ivDrawCircle.setOnClickListener(this)
        mGestureDetector = GestureDetector(context, GestureListener())
        binding!!.drawerView.setOnTouchListener(this)
        binding!!.ivDrawPolygon.setOnClickListener(this)
        binding!!.ivMapInfo.setOnClickListener(this)
        binding!!.ivMinus.setOnClickListener(this)
        binding!!.ivPlus.setOnClickListener(this)

        binding!!.ivMinus.setOnLongClickListener {
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    if (binding!!.ivMinus.isPressed) {
                        if (currentProgress > 30.0f) {
                            var temp = currentProgress
                            temp -= 100f
                            if (temp <= 30.0f) {
                                currentProgress = 30.0f
                            } else {
                                currentProgress -= 100f
                            }
                            binding!!.sbGeofence.progress = currentProgress.toInt()
                        }
                    } else {
                        timer.cancel()
                        updateUI()
                    }
                }
            }, 100, 200)
            true
        }

        binding!!.ivPlus.setOnLongClickListener {
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    if (binding!!.ivPlus.isPressed) {
                        if (currentProgress < 64373.0f) {
                            currentProgress += 100f
                            binding!!.sbGeofence.progress = currentProgress.toInt()
                        }
                    } else {
                        timer.cancel()
                        updateUI()
                    }
                }
            }, 100, 200)
            true
        }
    }

    private fun updateUI() {
        Handler(Looper.getMainLooper()).post {
            if (mapCircle != null) {
                if (currentProgress > 30) {
                    mapCircle!!.radius = currentProgress.toDouble()
                    val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
                    val animateZomm = currentZoomLevel + 5
                    mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat!!, circleLon!!), animateZomm))
                    mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
                }
            }
        }
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(context!!)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mGooglemap = googleMap
        if (ActivityCompat.checkSelfPermission(HooleyMain.activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(HooleyMain.activity!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        if (HooleyApp.db.getInt(Constants.TYPE_THEME) == MODE_NIGHT_YES) {
            try {
                val success = mGooglemap!!.setMapStyle(MapStyleOptions.loadRawResourceStyle(HooleyMain.activity!!, R.raw.black_map))
                if (!success) {
                    Log.e("", "Style parsing failed.")
                }
            } catch (e: Resources.NotFoundException) {
                Log.e("", "Can't find style. Error: ", e)
            }
        } else {
            try {
                val success = mGooglemap!!.setMapStyle(MapStyleOptions.loadRawResourceStyle(HooleyMain.activity!!, R.raw.white_map))
                if (!success) {
                    Log.e("", "Style parsing failed.")
                }
            } catch (e: Resources.NotFoundException) {
                Log.e("", "Can't find style. Error: ", e)
            }
        }
        mGooglemap!!.setOnMapLongClickListener(this)
        setGeoFenceOnMap()
    }

    @SuppressLint("MissingPermission")
    private fun setGeoFenceOnMap() {
        if (displayInfoDialog) {
            displayInfoDialog = false
            showInfoDialog()
        }
        if (geoFenceList != null && geoFenceList!!.size > 0) {
            mGooglemap!!.isMyLocationEnabled = false
            if (geoFenceList!!.size > 1) {
                isCircle = false
                // draw poligon
                Logger.v("geoFenceList")
                if (mLatlngs.size > 0)
                    mLatlngs.clear()
                var latLng: LatLng
                for (i in geoFenceList!!.indices) {
                    latLng = LatLng(java.lang.Double.parseDouble(geoFenceList!![i].latitude!!), java.lang.Double.parseDouble(geoFenceList!![i].longitude!!))
                    mLatlngs.add(latLng)
                }
                circleLat = geoFenceList!![0].latitude!!.toDouble()
                circleLon = geoFenceList!![0].longitude!!.toDouble()
                currentProgress = (geoFenceList!![0].radius!!.toDouble() * 1609.344).toFloat()
                binding!!.sbGeofence.progress = currentProgress.toInt()
                mGooglemap!!.uiSettings.isZoomGesturesEnabled = true
                mGooglemap!!.uiSettings.setAllGesturesEnabled(true)
                mPolygonOptions = PolygonOptions()
                mPolygonOptions!!.fillColor(ContextCompat.getColor(activity!!, R.color.app_purple_light_color))
                mPolygonOptions!!.strokeColor(ContextCompat.getColor(activity!!, R.color.app_purple_color))
                mPolygonOptions!!.strokeWidth(3f)
                mPolygonOptions!!.addAll(mLatlngs)
                mGooglemap!!.addPolygon(mPolygonOptions)

                var centerLatLngObj = (HooleyMain.activity as ActivityBase).computeCentroid(mLatlngs)

                if (mGooglemap != null) {
                    val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
                    val animateZomm = currentZoomLevel + 5
                    mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat!!, circleLon!!), animateZomm))
                    mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
                }
                centerLatLngObj = (HooleyMain.activity as ActivityBase).computeCentroid(mLatlngs)
                circleLat = centerLatLngObj.latitude
                circleLon = centerLatLngObj.longitude
                try {
                    binding!!.tvLocationName.text = (HooleyMain.activity as ActivityBase).getCompleteAddressString(circleLat!!, circleLon!!)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

                binding!!.ivDrawCircle.isChecked = false
                binding!!.ivDrawPolygon.isChecked = true

            } else {
                isCircle = true
                // draw circle
                addCircle(java.lang.Double.parseDouble(geoFenceList!![0].radius!!) * perMile, java.lang.Double.parseDouble(geoFenceList!![0].latitude!!), java.lang.Double.parseDouble(geoFenceList!![0].longitude!!))
                var radius: Double? = java.lang.Double.parseDouble(geoFenceList!![0].radius!!)
                radius = radius!! * 1609.34f
                circleLat = java.lang.Double.parseDouble(geoFenceList!![0].latitude!!)
                circleLon = java.lang.Double.parseDouble(geoFenceList!![0].longitude!!)
                binding!!.sbGeofence.progress = radius.toInt()

                if (mGooglemap != null) {
                    val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
                    val animateZomm = currentZoomLevel + 5
                    mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat!!, circleLon!!), animateZomm))
                    mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
                }
                try {
                    binding!!.tvLocationName.text = (HooleyMain.activity as ActivityBase).getCompleteAddressString(circleLat!!, circleLon!!)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

                binding!!.ivDrawCircle.isChecked = true
                binding!!.ivDrawPolygon.isChecked = false
            }
        } else {
            handler.postDelayed({
                binding!!.ivMyLocation.performClick()
            }, 2000)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
        inflater!!.inflate(R.menu.gallery_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.save -> {
                if (binding!!.tvLocationName.text.toString().isNotEmpty()) {
                    if (isPaid) {
                        compareCurrentCountry(circleLat, circleLon)
                    } else {
                        proceedToCreateEvent()
                    }
                } else {
                    Util.showToastMessage(HooleyMain.activity!!, "Address cannot be empty")
                }

                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    protected fun checkLocationSettings() {
        if (mGoogleApiClient != null) {
            val result = LocationServices.SettingsApi.checkLocationSettings(
                    mGoogleApiClient,
                    mLocationSettingsRequest
            )
            result.setResultCallback(this)
        } else {
            buildGoogleApiClient()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(HooleyMain.activity!!, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                            settingsRequest()
                        }
                    }
                } else {
                    Toast.makeText(HooleyMain.activity, R.string.str_permission, Toast.LENGTH_LONG).show()
                }
                return
            }
        }
    }

    fun settingsRequest() {
        try {
            mLocationRequest = LocationRequest.create()
            mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            mLocationRequest.interval = (30 * 1000).toLong()
            mLocationRequest.fastestInterval = (5 * 1000).toLong()
            val builder = LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest)
            mLocationSettingsRequest = builder.build()
            builder.setAlwaysShow(true)
            if (mGoogleApiClient != null) {
                val result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, mLocationSettingsRequest)
                result.setResultCallback { result ->
                    val status = result.status
                    val state = result.locationSettingsStates
                    when (status.statusCode) {
                        LocationSettingsStatusCodes.SUCCESS -> {
                            startLocationUpdates()
                            createLocationRequest()
                            buildLocationSettingsRequest()
                            checkLocationSettings()
                        }
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                            status.startResolutionForResult(HooleyMain.activity, REQUEST_CHECK_SETTINGS)
                        } catch (e: IntentSender.SendIntentException) {
                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                        }
                    }
                }
            } else {
                buildGoogleApiClient()
            }
        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
        }
    }

    private fun googleServicesAvailable(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val isAvailable = apiAvailability.isGooglePlayServicesAvailable(context!!)
        when {
            isAvailable == ConnectionResult.SUCCESS -> return true
            apiAvailability.isUserResolvableError(isAvailable) -> {
                val dialog = apiAvailability.getErrorDialog(HooleyMain.activity, isAvailable, 0)
                dialog.show()
            }
            else -> Toast.makeText(HooleyMain.activity, "Can't connect ", Toast.LENGTH_SHORT).show()
        }
        return false
    }

    private fun initMap() {
        locationManager = HooleyMain.activity!!.getSystemService(LOCATION_SERVICE) as LocationManager
        if (!googleServicesAvailable()) {
            Toast.makeText(HooleyMain.activity, "Google service is not available", Toast.LENGTH_SHORT).show()
            return
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission()
        } else {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                settingsRequest()
            }
        }
        mapFragment = childFragmentManager.findFragmentById(R.id.mpGeofence) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    fun checkLocationPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(HooleyMain.activity!!, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(HooleyMain.activity!!, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(HooleyMain.activity!!, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                        MY_PERMISSIONS_REQUEST_LOCATION)
            } else {
                ActivityCompat.requestPermissions(HooleyMain.activity!!, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                        MY_PERMISSIONS_REQUEST_LOCATION)
            }
            return false
        } else {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                settingsRequest()
            }
            return true
        }
    }

    override fun onResult(locationSettingsResult: LocationSettingsResult) {
        val status = locationSettingsResult.status
        Log.d(ct, "onResult-> " + status.statusCode)
        Log.i(ct, "onResult-> " + status.statusMessage!!)
        when (status.statusCode) {
            LocationSettingsStatusCodes.CANCELED -> Log.i(ct, "Location handling cancelled!")
            LocationSettingsStatusCodes.SUCCESS -> {
                Log.i(ct, "All location settings are satisfied.")
                startLocationUpdates()
            }
            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                Log.i(ct, "Location settings are not satisfied. Show the user a dialog to" + "upgrade location settings ")

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(HooleyMain.activity, REQUEST_CHECK_SETTINGS)
                } catch (e: IntentSender.SendIntentException) {
                    Log.i(ct, "PendingIntent unable to execute request.")
                }
            }
            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(ct, "Location settings are inadequate, and cannot be fixed here. Dialog " + "not created.")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(ct, "onActivityResult-> $resultCode")
        when (requestCode) {
            REQUEST_CHECK_SETTINGS -> when (resultCode) {
                Activity.RESULT_OK -> {
                    Log.i(ct, "User agreed to make required location settings changes.")
                    startLocationUpdates()
                }
                Activity.RESULT_CANCELED -> {
                    Toast.makeText(context, "Cannot use Repairer until you allow to provide your location!", Toast.LENGTH_SHORT).show()
                    Log.i(ct, "User chose not to make required location settings changes.")
                }
            }
        }
    }

    protected fun startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    mLocationRequest,
                    this
            ).setResultCallback { status ->
                mRequestingLocationUpdates = true
                if (status.isSuccess) {
                    Log.i(ct, "Status true! Going to load!")
                } else {
                    Log.e(ct, "There is a problem")
                }
                getLastLocation()
            }
        }
    }

    fun moveToCurrentLocation() {
        if (mCurrentLocation == null) {
            if (HooleyMain.activity != null) {
                if (ActivityCompat.checkSelfPermission(HooleyMain.activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(HooleyMain.activity!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                }
            }
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
            if (mCurrentLocation != null) {
                circleLat = mCurrentLocation!!.latitude
                circleLon = mCurrentLocation!!.longitude
                val loc = LatLng(mCurrentLocation!!.latitude, mCurrentLocation!!.longitude)
                if (mGooglemap != null) {
                    mGooglemap!!.clear()
                    val markerOption = MarkerOptions().position(loc).flat(true).draggable(false)
                    markerOption.icon(bitmapDescriptorFromVector(HooleyMain.activity, R.drawable.ic_map_cirlce_centre))
                    mGooglemap!!.addMarker(markerOption)
                    addCircle(currentProgress.toDouble(), circleLat!!, circleLon!!)
                    val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
                    val animateZomm = currentZoomLevel + 5
                    mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat!!, circleLon!!), animateZomm))
                    mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
                }
            }
        } else {
            circleLat = mCurrentLocation!!.latitude
            circleLon = mCurrentLocation!!.longitude
            val loc = LatLng(mCurrentLocation!!.latitude, mCurrentLocation!!.longitude)
            if (mGooglemap != null) {
                mGooglemap!!.clear()
                addCircle(currentProgress.toDouble(), circleLat!!, circleLon!!)
                val markerOption = MarkerOptions().position(loc).flat(true).draggable(false)
                markerOption.icon(bitmapDescriptorFromVector(HooleyMain.activity, R.drawable.ic_map_cirlce_centre))
                mGooglemap!!.addMarker(markerOption)
                val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
                val animateZomm = currentZoomLevel + 5
                mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat!!, circleLon!!), animateZomm))
                mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
            }
        }
        try {
            binding!!.tvLocationName.text = (HooleyMain.activity as ActivityBase).getCompleteAddressString(circleLat!!, circleLon!!)
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

    }

    private fun bitmapDescriptorFromVector(context: Context?, vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context!!, vectorResId)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    fun addCircle(radius: Double, cLat: Double, cLong: Double) {
        try {
            isCircle = true
            mGooglemap!!.clear()
            mLatlngs.clear()
            mPolylineOptions = null
            mPolygonOptions = null
            mDrawFinished = true
            mGooglemap!!.uiSettings.isZoomGesturesEnabled = true
            mGooglemap!!.uiSettings.setAllGesturesEnabled(true)
            binding!!.drawerView.visibility = View.GONE

            if (mapCircle != null) {
                mapCircle!!.remove()
            }
            mapCircle = mGooglemap!!.addCircle(CircleOptions()
                    .center(LatLng(cLat, cLong))
                    .radius(radius)
                    .fillColor(ContextCompat.getColor(activity!!, R.color.app_purple_light_color))
                    .strokeColor(ContextCompat.getColor(activity!!, R.color.app_purple_color))
                    .strokeWidth(2f)
                    .clickable(true))
            mapCircle!!.isVisible = true


            val loc = LatLng(cLat, cLong)
            val markerOption = MarkerOptions().position(loc).flat(true).draggable(false)
            markerOption.icon(bitmapDescriptorFromVector(HooleyMain.activity, R.drawable.ic_map_cirlce_centre))
            mGooglemap!!.addMarker(markerOption)
            binding!!.ivDrawCircle.isChecked = true
            binding!!.ivDrawPolygon.isChecked = false
        } catch (e: Exception) {
        }
    }

    fun getLastLocation() {
        if (mRequestingLocationUpdates!!) {
            if (mCurrentLocation != null) {
                Log.e(ct, "getLastLocation-> Now fetched!")
                Log.d(ct, "getLastLocation-> Lat is: " + mCurrentLocation!!.latitude)
                Log.d(ct, "getLastLocation-> Lon is: " + mCurrentLocation!!.longitude)
                stopLocationUpdates()
            } else {
                Log.i(ct, "getLastLocation-> Cannot fetch! (mCurrentLocation)")
            }
        } else {
            Log.i(ct, "getLastLocation-> Cannot fetch! (mRequestingLocationUpdates)")
        }
    }

    protected fun stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        ).setResultCallback { status ->
            mRequestingLocationUpdates = false
            if (status.isSuccess) {
                Log.i(ct, "stopLocationUpdates-> Status true! Going to load!")
            } else {
                Log.e(ct, "stopLocationUpdates-> There is a problem")
            }
        }
    }

    protected fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    protected fun buildLocationSettingsRequest() {
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        mLocationSettingsRequest = builder.build()
    }

    fun drawZone() {
        isCircle = false
        mGooglemap!!.clear()
        mLatlngs.clear()
        mPolylineOptions = null
        mPolygonOptions = null
        mDrawFinished = true
        binding!!.drawerView.visibility = View.VISIBLE
        mGooglemap!!.uiSettings.isScrollGesturesEnabled = false
        if (mapCircle != null) {
            mapCircle!!.isVisible = false
            mapCircle = null
        }

        binding!!.ivDrawCircle.isChecked = false
        binding!!.ivDrawPolygon.isChecked = true
    }

    private fun showInfoDialog() {
        showInfoDialog = Dialog(HooleyMain.activity!!)
        showInfoDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        showInfoDialog!!.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        showInfoDialog!!.setContentView(R.layout.dialog_map_info)
        val ivClose = showInfoDialog!!.findViewById<ImageView>(R.id.ivClose)
        val tvText = showInfoDialog!!.findViewById<TextView>(R.id.tvText)
        tvText.text = HooleyMain.activity!!.getString(R.string.str_map_set_location)
        ivClose.setOnClickListener { showInfoDialog!!.dismiss() }
        showInfoDialog!!.show()
    }

    override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null)
            mGoogleApiClient!!.connect()
    }

    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient != null)
            mGoogleApiClient!!.disconnect()

    }

    override fun onResume() {
        super.onResume()
        if (mGoogleApiClient != null)
            if (mGoogleApiClient!!.isConnected) {
                startLocationUpdates()
            }
        if (isVisible) {
            if (mGooglemap != null)
                mGooglemap!!.clear()
            binding!!.sbGeofence.progress = 100
            binding!!.tvSbProgress.text = "100 meters"
        }
    }

    override fun onConnected(bundle: Bundle?) {
        if (mGooglemap != null)
            mGooglemap!!.uiSettings.isMyLocationButtonEnabled = false
        if (geoFenceList == null) {
            moveToCurrentLocation()
        }

    }

    override fun onConnectionSuspended(i: Int) {
        Log.i(ct, "Connection suspended")
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.i(ct, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.errorCode)
    }

    override fun onLocationChanged(location: Location) {
        mCurrentLocation = location
        getLastLocation()
    }

    @SuppressLint("MissingPermission")
    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnSaveMapLocation -> if (binding!!.tvLocationName.text.toString().isNotEmpty()) {
                if (isPaid) {
                    compareCurrentCountry(circleLat, circleLon)
                } else {
                    proceedToCreateEvent()
                }
            } else {
                Util.showToastMessage(HooleyMain.activity!!, "Address cannot be empty")
            }
            R.id.ivMyLocation -> {
                binding!!.rlTop.visibility = View.VISIBLE
                moveToCurrentLocation()
            }
            R.id.ivDrawCircle -> {
                binding!!.rlTop.visibility = View.VISIBLE
                if (circleLat == null)
                    moveToCurrentLocation()
                addCircle(currentProgress.toDouble(), circleLat!!, circleLon!!)
            }
            R.id.ivDrawPolygon -> {
                binding!!.rlTop.visibility = View.GONE
                drawZone()
            }
            R.id.ivMapInfo -> {
                showInfoDialog()
            }
            R.id.ivMinus -> if (currentProgress >= 100.0f) {
                var temp = currentProgress
                temp -= 100f
                if (temp <= 100.0f) {
                    currentProgress = 100.0f
                } else {
                    currentProgress -= 100f
                }
                binding!!.sbGeofence.progress = (currentProgress).toInt()
                updateUI()
            }
            R.id.ivPlus -> if (currentProgress < 64373.0f) {
                currentProgress += 100f
                binding!!.sbGeofence.progress = (currentProgress).toInt()
                updateUI()
            }
        }
    }

    private fun proceedToCreateEvent() {
        intent = Intent()
        intent!!.putExtra("isCircle", isCircle)
        if (isCircle) {
            if (circleLat == null || circleLon == null) {
                Util.showToastMessage(HooleyMain.activity!!, "Please select location")
                return
            }
            intent!!.putExtra("Lat", circleLat!!)
            intent!!.putExtra("Long", circleLon!!)
            intent!!.putExtra("radius", (currentProgress / perMile).toFloat())
        } else {
            if (mLatlngs != null && mLatlngs.size > 0) {
                intent!!.putParcelableArrayListExtra("mLatlngs", mLatlngs)
                if (binding!!.tvLocationName.text.toString().isNotEmpty()) {
                    intent!!.putExtra("address", binding!!.tvLocationName.text.toString())
                }
            } else {
                Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_select_location))
                return
            }
        }
        if (callApi) {
            var model = PersonalProfileModel.GeoFence()
            if (isCircle) {
                model.latitude = circleLat!!.toString()
                model.longitude = circleLon!!.toString()
                model.radius = (currentProgress / perMile).toString()
                model.address = binding!!.tvLocationName.text.toString()
            } else {
                for (loop in mLatlngs.indices) {
                    model.latitude = java.lang.Double.toString(mLatlngs[loop].latitude)
                    model.longitude = java.lang.Double.toString(mLatlngs[loop].longitude)
                    model.radius = (30 / perMile).toString()
                    val latLng = (HooleyMain.activity as ActivityBase).computeCentroid(mLatlngs)
                    model.address = (HooleyMain.activity as ActivityBase).getCompleteAddressString(latLng.latitude, latLng.longitude)
                }
            }
            updateGeoFence(eventId, model)
        } else {
            HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
            targetFragment!!.onActivityResult(
                    targetRequestCode,
                    Activity.RESULT_OK,
                    intent
            )
        }
    }

    fun updateGeoFence(eventId: String, model: PersonalProfileModel.GeoFence) {
        HooleyMain.activity!!.showDialog()
        service.updateGeoFence(eventId, model, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()

            }

            override fun onTokenExpire() {
                HooleyMain.activity!!.removeDialog()

            }

        })
    }

    private fun compareCurrentCountry(circleLat: Double?, circleLon: Double?) {
        val geocoder = Geocoder(getApplicationContext(), Locale.getDefault())
        val addresses: List<Address>?
        try {
            addresses = geocoder.getFromLocation(circleLat!!, circleLon!!, 1)
            if (addresses != null && addresses.isNotEmpty()) {
                val address = addresses[0].getAddressLine(0)
                val address11 = addresses[0].getAddressLine(1)
                val city = addresses[0].locality
                val country = addresses[0].countryName
                Log.e("Address>>", address)
                if (country.equals(currentCountry, ignoreCase = true)) {
                    proceedToCreateEvent()
                } else {
                    Util.showToastMessage(HooleyMain.activity!!, getString(R.string.str_select_current_country_address))
                }
            }
        } catch (e: IOException) {
        }
    }

    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
        if (mapCircle != null) {
            mapCircle!!.radius = progress.toDouble()
        }
        val f = DecimalFormat("##.00")
        currentProgress = seekBar.progress.toFloat()
        try {
            if (currentProgress < 1609.344) {
                if (progress <= 100) {
                    currentProgress = 100.0f
                    binding!!.tvSbProgress.text = 100.toString() + " meters"
                } else {
                    binding!!.tvSbProgress.text = f.format(currentProgress) + " meters"
                }
            } else {
                binding!!.tvSbProgress.text = "${f.format(currentProgress / 1609.344)} miles"
            }
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar) {
        mGooglemap!!.clear()
        val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
        val animateZomm = currentZoomLevel - 5
        if (circleLat != null) {
            addCircle(currentProgress.toDouble(), circleLat!!, circleLon!!)
            mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat!!, circleLon!!), animateZomm))
            mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
        } else {
            Util.showToastMessage(HooleyMain.activity!!, "Please turn on your location!")
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                settingsRequest()
            }
        }
    }

    override fun onMapLongClick(latLng: LatLng) {
        circleLat = latLng.latitude
        circleLon = latLng.longitude
        try {
            binding!!.tvLocationName.text = (HooleyMain.activity as ActivityBase).getCompleteAddressString(circleLat!!, circleLon!!)
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }
        addCircle(currentProgress.toDouble(), circleLat!!, circleLon!!)
        val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
        val animateZomm = currentZoomLevel + 5
        mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat!!, circleLon!!), animateZomm))
        mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        var X1 = event.x.toInt()
        var Y1 = event.y.toInt()
        var point = Point()
        point.x = X1
        point.y = Y1
        val firstGeoPoint = mGooglemap!!.projection.fromScreenLocation(
                point)
        when (event.action) {

            MotionEvent.ACTION_DOWN -> {
            }

            MotionEvent.ACTION_MOVE -> if (mDrawFinished) {
                X1 = event.x.toInt()
                Y1 = event.y.toInt()
                point = Point()
                point.x = X1
                point.y = Y1
                val geoPoint = mGooglemap!!.projection
                        .fromScreenLocation(point)
                mLatlngs.add(geoPoint)
                mPolylineOptions = PolylineOptions()
                mPolylineOptions!!.color(ContextCompat.getColor(activity!!, R.color.app_purple_color))
                mPolylineOptions!!.width(5f)
                mPolylineOptions!!.addAll(mLatlngs)
                mGooglemap!!.addPolyline(mPolylineOptions)
                binding!!.tvLocationName.text = ""
            }
            MotionEvent.ACTION_UP -> {
                Log.d(ct, "Points array size " + mLatlngs.size)
                mLatlngs.add(firstGeoPoint)
                mGooglemap!!.clear()
                mPolylineOptions = null
                binding!!.drawerView.visibility = View.GONE
                mGooglemap!!.uiSettings.isZoomGesturesEnabled = true
                mGooglemap!!.uiSettings.setAllGesturesEnabled(true)
                mPolygonOptions = PolygonOptions()
                mPolygonOptions!!.fillColor(ContextCompat.getColor(activity!!, R.color.app_purple_light_color))
                mPolygonOptions!!.strokeColor(ContextCompat.getColor(activity!!, R.color.app_purple_color))
                mPolygonOptions!!.strokeWidth(3f)
                mPolygonOptions!!.addAll(mLatlngs)
                mGooglemap!!.addPolygon(mPolygonOptions)
                mDrawFinished = false
                val latLng = (HooleyMain.activity as ActivityBase).computeCentroid(mLatlngs)
                circleLat = latLng.latitude
                circleLon = latLng.longitude
                try {
                    binding!!.tvLocationName.text = (HooleyMain.activity as ActivityBase).getCompleteAddressString(latLng.latitude, latLng.longitude)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        }
        return mGestureDetector!!.onTouchEvent(event)
    }

    override fun onDestroy() {
        super.onDestroy()
        callApi = false
        if (binding != null)
            binding!!.unbind()
    }

    private inner class GestureListener : GestureDetector.SimpleOnGestureListener() {
        override fun onDown(e: MotionEvent): Boolean {
            return true
        }

        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            return false
        }
    }

    companion object {

        val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 1000
        val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2
        val MY_PERMISSIONS_REQUEST_LOCATION = 99
        protected val REQUEST_CHECK_SETTINGS = 0x1
        internal var isPaid = false
        private var geoFenceList: ArrayList<PersonalProfileModel.GeoFence>? = null
        private var instance: GeofenceFragment? = null
        private var callApi: Boolean = false
        private var eventId: String = ""

        fun newInstance(userGeofence: ArrayList<PersonalProfileModel.GeoFence>): GeofenceFragment {
            instance = GeofenceFragment()
            geoFenceList = userGeofence
            return instance!!
        }

        fun newInstance(userGeofence: ArrayList<PersonalProfileModel.GeoFence>, callApis: Boolean, eventId_: String): GeofenceFragment {
            instance = GeofenceFragment()
            geoFenceList = userGeofence
            callApi = callApis
            eventId = eventId_
            return instance!!
        }

        fun newInstance(isPaidFragment: Boolean): GeofenceFragment {
            instance = GeofenceFragment()
            isPaid = isPaidFragment
            return instance!!
        }
    }
}
