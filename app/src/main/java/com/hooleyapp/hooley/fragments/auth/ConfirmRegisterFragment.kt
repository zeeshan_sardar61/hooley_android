package com.hooleyapp.hooley.fragments.auth

import android.animation.Animator
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.databinding.FragmentConfirmRegisterBinding
import com.hooleyapp.hooley.fragments.BaseFragment

/**
 * Created by Nauman on 12/5/2017.
 */

class ConfirmRegisterFragment : BaseFragment(), View.OnClickListener, Animator.AnimatorListener {

    var binding: FragmentConfirmRegisterBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_confirm_register, container, false)
        setListener()
        return binding!!.root
    }

    private fun setListener() {
        val sourceString = "Your account is almost ready.\n Check your email <b>$userEmail</b> \n to verify your\n account and continue to Sign In <br><br>Don't forget to check your spam folder too!"
        binding!!.tvCheckMail.text = Html.fromHtml(sourceString)
        binding!!.btnLogin.setOnClickListener(this)
        binding!!.lavConfirmSignUp.addAnimatorListener(this)
    }


    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnLogin -> (activity as ActivityBase).callFragment(R.id.authContainer, LoginFragment.newInstance(), "LoginFragment")
        }
    }

    override fun onAnimationStart(animator: Animator) {

    }

    override fun onAnimationEnd(animator: Animator) {
        binding!!.lavConfirmSignUp.visibility = View.GONE
        binding!!.lavConfirmSignUpLoop.visibility = View.VISIBLE
    }

    override fun onAnimationCancel(animator: Animator) {

    }

    override fun onAnimationRepeat(animator: Animator) {

    }

    companion object {

        lateinit var instance: ConfirmRegisterFragment
        private var userEmail: String? = null

        fun newInstance(email: String): ConfirmRegisterFragment {
            userEmail = email
            instance = ConfirmRegisterFragment()
            return instance
        }
    }
}
