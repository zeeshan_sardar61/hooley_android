package com.hooleyapp.hooley.fragments.ticket

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentConfrimGiftTicketBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.model.MyTicketModel
import com.hooleyapp.hooley.services.TicketWebService
import com.hooleyapp.hooley.tables.FriendsTable

/**
 * Created by adilmalik on 09/05/2018.
 */

class ConfirmGiftTicketFragment : BaseFragment(), View.OnClickListener {
    var binding: FragmentConfrimGiftTicketBinding? = null
    var ticketWebService = TicketWebService()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_confrim_gift_ticket, container, false)
        return binding!!.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setData()
        setListener()
    }

    private fun setListener() {
        binding!!.btnSendGift.setOnClickListener(this)
    }

    private fun setData() {
        //        if (!TextUtils.isEmpty(ticketObj.qrCodeUrl))
        //            Glide.with(HooleyMain.activity).load(ticketObj.qrCodeUrl).into(userSearchbinding.ivQrCode);
        //        if (!TextUtils.isEmpty(ticketObj.eventName))
        //            userSearchbinding.tvEventName.setText(ticketObj.eventName);
        //        if (!TextUtils.isEmpty(ticketObj.eventAddress))
        //            userSearchbinding.tvEventAddress.setText(ticketObj.eventAddress);
        //        userSearchbinding.tvTimeStamp.setText(DateUtils.convertDate(DateUtils.getLocalDate(ticketObj.startTime), DateUtils.getLocalDate(ticketObj.endTime)));
        //        userSearchbinding.tvTicketCount.setText("Number of Tickets (" + ticketObj.totalTicket + ")");

        binding!!.tvFriendName.text = friendObj!!.fullName
        binding!!.tvHomeTown.text = friendObj!!.cityName + "/" + friendObj!!.stateName + "/" + friendObj!!.countryName
        if (!TextUtils.isEmpty(friendObj!!.profilePic))
            Glide.with(HooleyMain.activity!!).load(friendObj!!.profilePic).into(binding!!.ivFriendAvatar)

    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnSendGift -> sendGiftTicket()
        }
    }

    private fun sendGiftTicket() {
        HooleyMain.activity!!.showDialog()
        //        ticketWebService.sendTicketGift(String.valueOf(friendObj.userId), numberOfTicket, ticketObj.eventTicketId, new GenericCallback<GeneralModel>() {
        //            @Override
        //            public void success(GeneralModel result) {
        //                HooleyMain.activity!!.removeDialog();
        //                HooleyMain.activity.getSupportFragmentManager().popBackStackImmediate();
        //            }
        //
        //            @Override
        //            public void failure(String message) {
        //                try {
        //                    HooleyMain.activity!!.removeDialog();
        //                    Util.showToastMessage(HooleyMain.activity, message);
        //                } catch (NullPointerException ex) {
        //                    ex.printStackTrace();
        //                }
        //            }
        //        });
    }

    companion object {

        lateinit var instance: ConfirmGiftTicketFragment
        private var ticketObj: MyTicketModel.MyTicketsList? = null
        private var friendObj: FriendsTable? = null
        private var numberOfTicket: String? = null


        fun newInstance(obj: MyTicketModel.MyTicketsList, ticketCount: String, frndObj: FriendsTable): ConfirmGiftTicketFragment {
            numberOfTicket = ticketCount
            friendObj = frndObj
            ticketObj = obj
            instance = ConfirmGiftTicketFragment()
            return instance
        }
    }
}
