package com.hooleyapp.hooley.fragments.event


import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.GridLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.*
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterHooleyGallery
import com.hooleyapp.hooley.adapters.AdapterWallpaperPager
import com.hooleyapp.hooley.databinding.FragmentHooleyGalleryBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.IHooleyGalleryClickListener
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.HooleyGalleryFinalModel
import com.hooleyapp.hooley.model.HooleyGalleryModel
import com.hooleyapp.hooley.model.ItemCreateEventWallpaper
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.EventWebService
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class HooleyGalleryFragment : BaseFragment(), View.OnClickListener, IHooleyGalleryClickListener {

    lateinit var binding: FragmentHooleyGalleryBinding
    var eventWallpapers = ArrayList<Int>()
    var imageUrlList = ArrayList<String>()
    lateinit var eventGallryCategories: ArrayList<HooleyGalleryModel.EventGallryCategories>
    lateinit var eventGalleryLists: ArrayList<HooleyGalleryModel.EventGalleryList>
    var galleryFinalArrayList = ArrayList<HooleyGalleryFinalModel>()
    private var adapter: AdapterHooleyGallery? = null
    private var adapter1: AdapterWallpaperPager? = null
    private var imageUrl = ""
    private var isUri = false
    private var intent: Intent? = null
    private val imageUri: Uri? = null
    internal var bold = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)
    internal var regular = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)
    var eventWebService = EventWebService()

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding.unbind()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_hooley_gallery, container, false)
        setListener()
        getHooleyGalleryData()
        setHasOptionsMenu(true)
        setupViewPager()
        return binding.root
    }

    private fun setListener() {
        binding.btnDone.setOnClickListener(this)
    }


    private fun setRecyclerView() {
        val manager = GridLayoutManager(HooleyMain.activity, 3)
        manager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (galleryFinalArrayList[position].type == 0) 3 else 1
            }
        }
        binding.rvHooleyGallery.layoutManager = manager
        binding.rvHooleyGallery.setHasFixedSize(true)
        adapter = AdapterHooleyGallery(context!!, galleryFinalArrayList, isEdit)
        adapter!!.setmListener(this)
        binding.rvHooleyGallery.adapter = adapter

    }

    private fun getHooleyGalleryData() {
        HooleyMain.activity!!.showDialog()
        eventWebService.getEventWallpappers(object : IWebServiceCallback<HooleyGalleryModel> {
            override fun success(result: HooleyGalleryModel) {
                HooleyMain.activity!!.removeDialog()

                if (result.success) {
                    eventGallryCategories = result.eventGallryCategories
                    eventGalleryLists = result.eventGalleryLists
                    setArrayForAdapter()
                    setRecyclerView()
                }
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                HooleyMain.activity!!.showSuccessDialog(message)
            }

            override fun onTokenExpire() {
                HooleyMain.activity!!.removeDialog()

            }
        })
    }

    /**
     * Initialize the contents of the Fragment host's standard options menu.  You
     * should place your menu items in to <var>menu</var>.  For this method
     * to be called, you must have first called [.setHasOptionsMenu].  See
     * for more information.
     *
     * @param menu     The options menu in which you place your items.
     * @param inflater
     * @see .setHasOptionsMenu
     *
     * @see .onPrepareOptionsMenu
     *
     * @see .onOptionsItemSelected
     */
    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu!!.clear()
        inflater!!.inflate(R.menu.gallery_menu, menu)
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     *
     *
     *
     * Derived classes should call through to the base class for it to
     * perform the default menu handling.
     *
     * @param item The menu item that was selected.
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     * @see .onCreateOptionsMenu
     */
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.save -> {
                if (TextUtils.isEmpty(imageUrl) && !isUri) {
                    Util.showToastMessage(HooleyMain.activity!!, "Please Select Cover to upload")
                    return false
                }
                intent = Intent()
                intent!!.putExtra("isUri", isUri)
                if (isUri)
                    intent!!.putExtra("Image", imageUri)
                else
                    intent!!.putExtra("Image", imageUrlList)
                targetFragment!!.onActivityResult(
                        targetRequestCode,
                        Activity.RESULT_OK,
                        intent
                )
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                return super.onOptionsItemSelected(item)
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun setArrayForAdapter() {
        Log.e("arraySize", " " + eventGallryCategories.size)
        for (i in eventGallryCategories.indices) {
            galleryFinalArrayList.add(HooleyGalleryFinalModel(eventGallryCategories[i].categoryId!!, 0, eventGallryCategories[i].ccategoryName!!))
            for (k in eventGalleryLists.indices) {
                if (eventGalleryLists[k].categoryId!!.equals(eventGallryCategories[i].categoryId!!, ignoreCase = true)) {
                    galleryFinalArrayList.add(HooleyGalleryFinalModel(eventGalleryLists[k].categoryId!!, eventGalleryLists[k].imageUrl!!, 1))
                }
            }
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnDone -> {
                if (TextUtils.isEmpty(imageUrl) && !isUri) {
                    Util.showToastMessage(HooleyMain.activity!!, "Please Select Cover to upload")
                    return
                }
                intent = Intent()
                intent!!.putExtra("isUri", isUri)
                if (isUri)
                    intent!!.putExtra("Image", imageUri)
                else
                    intent!!.putExtra("Image", imageUrlList)
                targetFragment!!.onActivityResult(
                        targetRequestCode,
                        Activity.RESULT_OK,
                        intent
                )
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
            }
        }
    }


    private fun setupViewPager() {
        adapter1 = AdapterWallpaperPager()
        binding.vpWallPaper.adapter = adapter1
        binding.tabEventWallpaper.setupWithViewPager(binding.vpWallPaper, true)
    }

    fun addView(item: ItemCreateEventWallpaper) {
        val pageIndex = adapter1!!.addView(item)
        // You might want to make "newPage" the currently displayed page:
        adapter1!!.notifyDataSetChanged()
        binding.vpWallPaper.setCurrentItem(pageIndex, true)
    }

    fun removeView(position: Int, imgPositions: ArrayList<Int>) {
        var pageIndex = adapter1!!.removeView(binding.vpWallPaper, position)
        // You might want to choose what page to display, if the current page was "defunctPage".
        imageUrlList.removeAt(position)
        eventWallpapers.removeAt(position)
        adapter1!!.notifyDataSetChanged()
        if (pageIndex == adapter1!!.count)
            pageIndex--

        if (pageIndex > 0)
            binding.vpWallPaper.currentItem = pageIndex
        if (eventWallpapers.size == 0) {
            binding.rlAddCover.visibility = View.VISIBLE
            binding.ivEdit.visibility = View.GONE
            binding.ivDelete.visibility = View.GONE
            binding.ivUploadPic.visibility = View.GONE
            binding.ivEventCover.visibility = View.VISIBLE
            binding.vpWallPaper.visibility = View.GONE
        }
    }


    override fun onClickImage(Url: String, isSelect: Boolean, imgPositions: ArrayList<Int>) {
        isUri = false
        imageUrl = Url
        if (isEdit) {
            val item = ItemCreateEventWallpaper(imageUrl, null, Constants.TYPE_URL)
            binding.rlAddCover.visibility = View.GONE
            binding.ivEdit.visibility = View.GONE
            binding.ivDelete.visibility = View.GONE
            binding.ivUploadPic.visibility = View.GONE
            binding.vpWallPaper.visibility = View.VISIBLE
            if (item != null)
                addView(item)
            imageUrlList.add(imageUrl)
            //            addView(Url);
            eventWallpapers.add(binding.vpWallPaper.currentItem)
        } else {
            val item = ItemCreateEventWallpaper(imageUrl, null, Constants.TYPE_URL)
            if (isSelect) {
                binding.rlAddCover.visibility = View.GONE
                binding.ivEdit.visibility = View.GONE
                binding.ivDelete.visibility = View.GONE
                binding.ivUploadPic.visibility = View.GONE
                binding.vpWallPaper.visibility = View.VISIBLE
                if (item != null)
                    addView(item)
                imageUrlList.add(imageUrl)
                //            addView(Url);
                eventWallpapers.add(binding.vpWallPaper.currentItem)
            } else {
                removeView(matchUri(Url), imgPositions)
            }

        }
    }


    //To match the particular URL
    internal fun matchUri(Uri: String): Int {
        for (i in imageUrlList.indices) {
            if (Uri.equals(imageUrlList[i], ignoreCase = true)) {
                return i
            }
        }
        return -1
    }

    companion object {
        lateinit var instance: HooleyGalleryFragment
        var isEdit: Boolean = false

        fun newInstance(isEdit_: Boolean): HooleyGalleryFragment {
            instance = HooleyGalleryFragment()
            isEdit = isEdit_
            return instance
        }
    }
}// Required empty public constructor
