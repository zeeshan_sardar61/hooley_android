package com.hooleyapp.hooley.fragments.invites

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.VPDashBoardAdapter
import com.hooleyapp.hooley.databinding.FragementInviteBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.hideKeyboard
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.others.Constants

/**
 * Created by Nauman on 12/29/2017.
 */

class InviteFragment : BaseFragment(), View.OnClickListener {
    private var adapter: VPDashBoardAdapter? = null
    private val REQUEST_CODE = 963

    init {
        setToolbar()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragement_invite, container, false)
        setSelectedBackground(R.id.llPastGuestList)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
        setupViewPager()
    }

    private fun setListener() {
        binding!!.llPastGuestList.setOnClickListener(this)
        binding!!.llPhoneContact.setOnClickListener(this)
        binding!!.llSocialNetwork.setOnClickListener(this)
        binding!!.llNearBy.setOnClickListener(this)
        binding!!.llFriends.setOnClickListener(this)
    }

    fun setToolbar() {
        HooleyMain.activity!!.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        HooleyMain.activity!!.supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
    }

    private fun setupViewPager() {
        adapter = VPDashBoardAdapter(childFragmentManager)
        adapter!!.addFragment(PastGuestListFragment(), "PastGuestListFragment")
        adapter!!.addFragment(PhoneContactFragment(), "PhoneContactFragment")
        adapter!!.addFragment(PeopleNearMeFragment(), "PeopleNearMeFragment")
        binding!!.vpInvite.adapter = adapter
    }


    private fun showShareEventDialog() {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, "You've been invited to " + createdEventName
                + ".\n Please click the link below for details.\n"
                + Constants.EVENT_SHARE + createdEventId)
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(Intent.createChooser(shareIntent, "Invite Event"))
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.llPastGuestList -> {
                setSelectedBackground(R.id.llPastGuestList)
                binding!!.vpInvite.setCurrentItem(0, true)
            }
            R.id.llPhoneContact -> {
                setSelectedBackground(R.id.llPhoneContact)
                binding!!.vpInvite.setCurrentItem(1, true)
            }
            R.id.llSocialNetwork ->
                showShareEventDialog()
            R.id.llNearBy -> {
                setSelectedBackground(R.id.llNearBy)
                binding!!.vpInvite.setCurrentItem(2, true)
            }
            R.id.llFriends -> {
                HooleyMain.activity!!.hideKeyboard()
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, InviteHooleyFriendsFragment(createdEventId, createdEventName), "InviteHooleyFriendsFragment")
            }
        }
    }

    private fun setSelectedBackground(id: Int) {
        when (id) {
            R.id.llPastGuestList -> {
                HooleyMain.activity!!.hideKeyboard()
                binding!!.llPastGuestList.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                binding!!.llPhoneContact.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding!!.llNearBy.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding!!.llSocialNetwork.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding!!.llFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
            R.id.llPhoneContact -> {
                HooleyMain.activity!!.hideKeyboard()
                binding!!.llPastGuestList.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding!!.llPhoneContact.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                binding!!.llNearBy.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding!!.llSocialNetwork.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding!!.llFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
            R.id.llNearBy -> {
                HooleyMain.activity!!.hideKeyboard()
                binding!!.llPastGuestList.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding!!.llPhoneContact.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding!!.llNearBy.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                binding!!.llSocialNetwork.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding!!.llFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
            R.id.llSocialNetwork -> {
                HooleyMain.activity!!.hideKeyboard()
                binding!!.llPastGuestList.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding!!.llPhoneContact.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding!!.llNearBy.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding!!.llSocialNetwork.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                binding!!.llFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
            R.id.llFriends -> {
                HooleyMain.activity!!.hideKeyboard()
                binding!!.llPastGuestList.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding!!.llPhoneContact.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding!!.llNearBy.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding!!.llSocialNetwork.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding!!.llFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        HooleyMain.activity!!.hideKeyboard()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val fragment = adapter!!.getItem(0)
            fragment.onActivityResult(requestCode, resultCode, data)

        } else if (requestCode == 5007 && resultCode == Activity.RESULT_OK) {
            val fragment = adapter!!.getItem(2)
            fragment.onActivityResult(requestCode, resultCode, data)

        } else if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_CANCELED) {
            HooleyMain.activity!!.showSuccessDialog("Cancelled")
            val fragment = adapter!!.getItem(0)
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    companion object {

        lateinit var instance: InviteFragment
        lateinit var createdEventId: String
        lateinit var createdEventName: String

        var binding: FragementInviteBinding? = null

        fun newInstance(id: String, name: String): InviteFragment {
            createdEventName = name
            createdEventId = id
            instance = InviteFragment()
            return instance
        }
    }

}
