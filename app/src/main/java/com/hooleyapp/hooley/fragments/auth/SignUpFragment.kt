package com.hooleyapp.hooley.fragments.auth

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatDelegate
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.CallbackManager
import com.facebook.FacebookSdk
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyAuth
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.view.ui.fragments.others.PrivacyPolicyFragment
import com.hooleyapp.hooley.databinding.FragmentSignUpBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.others.TermsAndConditionFragment
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.ISocialCallBackListener
import com.hooleyapp.hooley.model.LoginModel
import com.hooleyapp.hooley.model.staticData.StaticDataModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.services.AuthWebService
import com.hooleyapp.hooley.social.SocialClass
import java.util.*

/**
 * Created by Nauman on 4/18/2018.
 */

class SignUpFragment : BaseFragment(), View.OnClickListener, ISocialCallBackListener {

    var binding: FragmentSignUpBinding? = null
    private var callbackManager: CallbackManager? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    var socialClass = SocialClass()
    var service = AuthWebService()
    private var gson = Gson()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_up, container, false)
        binding!!.ivBack.setOnClickListener {
            HooleyAuth.activity.onBackPressed()
        }
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
        callbackManager = socialClass.initFacebookSdk()
        mGoogleApiClient = socialClass.initGooglePlus()
    }

    fun setListener() {
        binding!!.rlFacebook.setOnClickListener(this)
        binding!!.rlGoogle.setOnClickListener(this)
        binding!!.rlEmail.setOnClickListener(this)
        binding!!.tvSignIn.setOnClickListener(this)
        binding!!.tvTerms.setOnClickListener(this)
        binding!!.tvPrivacy.setOnClickListener(this)
        socialClass.setMListener(this)

    }

    private fun signInWithGoogle() {
        if (mGoogleApiClient!!.isConnected)
            Auth.GoogleSignInApi.signOut(mGoogleApiClient)
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        HooleyAuth.activity.startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onClick(v: View) {
        when (v.id) {

            R.id.rlFacebook -> socialClass.loginWithFacebook(HooleyAuth.activity)
            R.id.rlGoogle -> signInWithGoogle()
            R.id.rlEmail -> {
                HooleyApp.db.putString(Constants.SOCIAL_MEDIA_TYPE, "HOOLEY")
                (activity as ActivityBase).callFragmentWithReplace(R.id.authContainer, RegisterFragment.newInstance(), "RegisterFragment")
            }
            R.id.tvTerms -> (activity as ActivityBase).callFragmentWithReplace(R.id.authContainer, TermsAndConditionFragment.newInstance(false, true), "TermsAndConditionFragment")
            R.id.tvPrivacy -> (activity as ActivityBase).callFragmentWithReplace(R.id.authContainer, PrivacyPolicyFragment(), "PrivacyPolicyFragment")
            R.id.tvSignIn -> (activity as ActivityBase).callFragmentWithReplace(R.id.authContainer, LoginFragment.newInstance(), "LoginFragment")
        }
    }

    private fun getStaticData(result: LoginModel) {
        service.getStaticData(result.userId!!, "2", object : GenericCallback<StaticDataModel> {
            override fun success(data: StaticDataModel) {
                HooleyAuth.activity.removeDialog()
                Log.d("okTAG", gson.toJson(data))
                HooleyApp.db.putString(Constants.STATIC_MODEL, gson.toJson(data))
                performAction(result)
            }

            override fun failure(message: String) {
                HooleyAuth.activity.removeDialog()
                try {
                    HooleyAuth.activity.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }
            }
        })
    }

    fun performAction(result: LoginModel) {
        if (!result.isFirstLogin) {
            (activity as ActivityBase).callFragmentWithReplace(R.id.authContainer, ProfileSetupFragment.newInstance(), "ProfileSetupFragment")
        } else {
            val intent = Intent(activity, HooleyMain::class.java)
            startActivity(intent)
            activity!!.finish()
        }
    }

    private fun loginWithSMUser(loginMedia: String) {
        if (!NetworkUtil.isInternetConnected(HooleyAuth.activity)) {
            HooleyAuth.activity.showSuccessDialog(Constants.NO_NETWORK)
            return
        }
        HooleyAuth.activity.showDialog()
        service.loginWithSMUser(loginMedia, object : GenericCallback<LoginModel> {
            override fun success(result: LoginModel) {
                HooleyAuth.activity.removeDialog()
                HooleyApp.db.putBoolean(Constants.IS_FIRST_LOGIN, result.isFirstLogin)
                HooleyApp.db.putBoolean(Constants.IS_BUSINESS_PROFILE_CREATED, result.isBusinessProfile)
                HooleyApp.db.putString(Constants.USER_ID, result.userId!!)
                HooleyApp.db.putString(Constants.AUTH_TOKKEN, result.userToken!!)

                HooleyApp.db.putString(Constants.EMAIL_ID, result.generalProfile!!.userInfo.email!!)
                HooleyApp.db.putString(Constants.FULL_NAME, result.generalProfile!!.userInfo.firstName + " " + result.generalProfile!!.userInfo.lastName)
                if (!TextUtils.isEmpty(result.generalProfile!!.userInfo.profilePicture))
                    HooleyApp.db.putString(Constants.USER_AVATAR, result.generalProfile!!.userInfo.profilePicture!!)
                if (result.generalProfile!!.userGeofence.size > 0) {
                    if (result.generalProfile!!.userGeofence.size > 1) {
                        val listLatLong = ArrayList<LatLng>()
                        for (i in result.generalProfile!!.userGeofence.indices) {
                            listLatLong.add(LatLng(java.lang.Double.parseDouble(result.generalProfile!!.userGeofence[i].latitude), java.lang.Double.parseDouble(result.generalProfile!!.userGeofence[i].longitude)))
                        }
                        val centerLatLngObj = (HooleyMain.activity as ActivityBase).computeCentroid(listLatLong)
                        HooleyApp.db.putDouble(Constants.PERSONAL_LAT, centerLatLngObj.latitude)
                        HooleyApp.db.putDouble(Constants.PERSONAL_LANG, centerLatLngObj.latitude)
                        HooleyApp.db.putString(Constants.PERSONAL_RADIUS, "5")
                    } else {
                        HooleyApp.db.putDouble(Constants.PERSONAL_LAT, java.lang.Double.parseDouble(result.generalProfile!!.userGeofence[0].latitude))
                        HooleyApp.db.putDouble(Constants.PERSONAL_LANG, java.lang.Double.parseDouble(result.generalProfile!!.userGeofence[0].longitude))
                        HooleyApp.db.putString(Constants.PERSONAL_RADIUS, "5")
                    }
                }
                if (result.isDark) {
                    HooleyApp.db.putInt(Constants.TYPE_THEME, AppCompatDelegate.MODE_NIGHT_YES)
//                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
//                    HooleyAuth.activity.delegate.setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                } else {
                    HooleyApp.db.putInt(Constants.TYPE_THEME, AppCompatDelegate.MODE_NIGHT_NO)
//                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
//                    HooleyAuth.activity.delegate.setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                }
                getStaticData(result)
            }

            override fun failure(message: String) {
                HooleyAuth.activity.removeDialog()
                try {
                    HooleyAuth.activity.showSuccessDialog(message)
                    if (message == "No user found") {
                        (activity as ActivityBase).callFragmentWithReplace(R.id.authContainer, CompleteRegisterFragment.newInstance(loginMedia), "CompleteRegisterFragment")
                    }
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)

            if (result.isSuccess) {
                socialClass.handleSignInResult(result)
            } else {
                socialClass.handleSignInResult(null)
            }
        }
        if (FacebookSdk.isFacebookRequestCode(requestCode)) {
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onCallBackFaceBook(isLogin: Boolean) {
        if (isLogin)
            loginWithSMUser(Constants.SOCIAL_FACE_BOOK)
    }

    override fun onCallBackTwitter(isLogin: Boolean) {

    }

    override fun onCallBackLinkedIn(isLogin: Boolean) {

    }

    override fun onCallBackGooglePlus(isLogin: Boolean) {
        if (isLogin) {
            mGoogleApiClient!!.disconnect()
            loginWithSMUser(Constants.SOCIAL_GOOGLE)
        }
    }

    companion object {

        lateinit var instance: SignUpFragment
        private val RC_SIGN_IN = 9001

        fun newInstance(): SignUpFragment {
            instance = SignUpFragment()
            return instance
        }
    }
}
