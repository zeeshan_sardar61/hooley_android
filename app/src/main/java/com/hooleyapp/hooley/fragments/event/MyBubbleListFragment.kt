package com.hooleyapp.hooley.fragments.event

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterBubbleList
import com.hooleyapp.hooley.databinding.FragmentMyBubbleListBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.model.BubbleListItem
import java.util.*

class MyBubbleListFragment : BaseFragment(), View.OnClickListener, AdapterBubbleList.IBubbleClickListener {

    var binding: FragmentMyBubbleListBinding? = null
    private var adapterBubbleList: AdapterBubbleList? = null
    var bubbleList: ArrayList<BubbleListItem>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_bubble_list, container, false)
//        HooleyMain.activity!!.supportActionBar!!.hide()
        bubbleList = (HooleyMain.activity as HooleyMain).bubbleList
        setListener()
        setData()
        return binding!!.root
    }

    private fun setData() {
        if (bubbleList != null && bubbleList!!.size > 0) {
            if (bubbleList!!.size % 2 == 0) {
                if (bubbleList!!.size > 1) {
                    if (!TextUtils.isEmpty(bubbleList!![0].eventAvatar)) {
                        binding!!.rlFirstColumn.visibility = View.VISIBLE
                        Glide.with(HooleyMain.activity!!).load(bubbleList!![0].eventAvatar).into(binding!!.ivFirstEventAvatar)
                        binding!!.rlFirstColumn.setOnClickListener(this)
                    }
                    if (!TextUtils.isEmpty(bubbleList!![1].eventAvatar)) {
                        binding!!.rlSecondColumn.visibility = View.VISIBLE
                        Glide.with(HooleyMain.activity!!).load(bubbleList!![1].eventAvatar).into(binding!!.ivSecondEventAvatar)
                        binding!!.rlSecondColumn.setOnClickListener(this)
                    }
                }
                if (bubbleList!!.size > 3) {
                    if (!TextUtils.isEmpty(bubbleList!![2].eventAvatar)) {
                        binding!!.rlThirdColumn.visibility = View.VISIBLE
                        Glide.with(HooleyMain.activity!!).load(bubbleList!![2].eventAvatar).into(binding!!.ivThirdEventAvatar)
                        binding!!.rlThirdColumn.setOnClickListener(this)
                    }
                    if (!TextUtils.isEmpty(bubbleList!![3].eventAvatar)) {
                        binding!!.rlFourthColumn.visibility = View.VISIBLE
                        Glide.with(HooleyMain.activity!!).load(bubbleList!![3].eventAvatar).into(binding!!.ivFourthEventAvatar)
                        binding!!.rlFourthColumn.setOnClickListener(this)
                    }
                }
                if (bubbleList!!.size > 5) {
                    if (!TextUtils.isEmpty(bubbleList!![4].eventAvatar)) {
                        binding!!.rlFifthColumn.visibility = View.VISIBLE
                        Glide.with(HooleyMain.activity!!).load(bubbleList!![4].eventAvatar).into(binding!!.ivFifthEventAvatar)
                        binding!!.rlFifthColumn.setOnClickListener(this)
                    }
                    if (!TextUtils.isEmpty(bubbleList!![5].eventAvatar)) {
                        binding!!.rlSixthColumn.visibility = View.VISIBLE
                        Glide.with(HooleyMain.activity!!).load(bubbleList!![5].eventAvatar).into(binding!!.ivSixthEventAvatar)
                        binding!!.rlSixthColumn.setOnClickListener(this)
                    }
                }
            } else {
                if (bubbleList!!.size > 2) {
                    if (!TextUtils.isEmpty(bubbleList!![0].eventAvatar)) {
                        binding!!.rlFirstColumn.visibility = View.VISIBLE
                        Glide.with(HooleyMain.activity!!).load(bubbleList!![0].eventAvatar).into(binding!!.ivFirstEventAvatar)
                        binding!!.rlFirstColumn.setOnClickListener(this)
                    }
                    if (!TextUtils.isEmpty(bubbleList!![1].eventAvatar)) {
                        binding!!.rlSecondColumn.visibility = View.VISIBLE
                        Glide.with(HooleyMain.activity!!).load(bubbleList!![1].eventAvatar).into(binding!!.ivSecondEventAvatar)
                        binding!!.rlSecondColumn.setOnClickListener(this)
                    }
                    if (!TextUtils.isEmpty(bubbleList!![2].eventAvatar)) {
                        binding!!.rlThirdColumn.visibility = View.VISIBLE
                        Glide.with(HooleyMain.activity!!).load(bubbleList!![2].eventAvatar).into(binding!!.ivThirdEventAvatar)
                        binding!!.rlThirdColumn.setOnClickListener(this)
                    }
                }
                if (bubbleList!!.size > 4) {
                    if (!TextUtils.isEmpty(bubbleList!![3].eventAvatar)) {
                        binding!!.rlFourthColumn.visibility = View.VISIBLE
                        Glide.with(HooleyMain.activity!!).load(bubbleList!![3].eventAvatar).into(binding!!.ivFourthEventAvatar)
                        binding!!.rlFourthColumn.setOnClickListener(this)
                    }
                    if (!TextUtils.isEmpty(bubbleList!![4].eventAvatar)) {
                        binding!!.rlFifthColumn.visibility = View.VISIBLE
                        Glide.with(HooleyMain.activity!!).load(bubbleList!![4].eventAvatar).into(binding!!.ivFifthEventAvatar)
                        binding!!.rlFifthColumn.setOnClickListener(this)
                    }
                }
            }
        }
    }

    private fun setListener() {
        binding!!.rlRoot.setOnClickListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
        (HooleyMain.activity as HooleyMain).isBubbleShowing = true
    }

    fun setAdapter() {
        val manager = GridLayoutManager(HooleyMain.activity, 2)
        binding!!.rvBubble.layoutManager = manager
        binding!!.rvBubble.setHasFixedSize(true)
        adapterBubbleList = AdapterBubbleList(context!!, bubbleList)
        adapterBubbleList!!.setMListener(this)
        binding!!.rvBubble.adapter = adapterBubbleList
    }


    @Throws(IndexOutOfBoundsException::class)
    override fun onClick(v: View) {
        when (v.id) {
            R.id.rlRoot -> {
                HooleyMain.activity!!.supportActionBar!!.show()
                if (!(HooleyMain.activity as HooleyMain).isBubbleShowing) {
                    (HooleyMain.activity as HooleyMain).addNewChatHead()
                }
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
            }
            R.id.rlFirstColumn -> {
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment.newInstance(bubbleList!![0].eventId), "LiveEventFragment")
                hideDrawer(HooleyMain.activity!!)
            }
            R.id.rlSecondColumn -> {
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment.newInstance(bubbleList!![1].eventId), "LiveEventFragment")
                hideDrawer(HooleyMain.activity!!)
            }
            R.id.rlThirdColumn -> {
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment.newInstance(bubbleList!![2].eventId), "LiveEventFragment")
                hideDrawer(HooleyMain.activity!!)
            }
            R.id.rlFourthColumn -> {
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment.newInstance(bubbleList!![3].eventId), "LiveEventFragment")
                hideDrawer(HooleyMain.activity!!)
            }
            R.id.rlFifthColumn -> {
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment.newInstance(bubbleList!![4].eventId), "LiveEventFragment")
                hideDrawer(HooleyMain.activity!!)
            }
            R.id.rlSixthColumn -> {
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment.newInstance(bubbleList!![5].eventId), "LiveEventFragment")
                hideDrawer(HooleyMain.activity!!)
            }
        }
    }

    override fun onBubbleClickListener(position: Int) {
        HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
        (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment.newInstance(bubbleList!![position].eventId), "LiveEventFragment")
        hideDrawer(HooleyMain.activity!!)
    }

    companion object {

        lateinit var instance: MyBubbleListFragment

        fun newInstance(): MyBubbleListFragment {
            instance = MyBubbleListFragment()
            return instance
        }
    }

}