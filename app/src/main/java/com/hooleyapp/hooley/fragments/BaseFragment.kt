package com.hooleyapp.hooley.fragments


import android.app.FragmentManager
import android.app.NotificationManager
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v7.widget.SearchView
import android.text.format.DateFormat
import android.view.View
import com.google.android.gms.maps.model.Circle
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyAuth
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.social.SocialClass
import java.io.File
import java.net.MalformedURLException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
open class BaseFragment : Fragment() {
    var dialog: ProgressDialog? = null


    protected val screenHeight: Int
        get() {
            val activity = activity ?: return 0
            return activity.findViewById<View>(android.R.id.content).height
        }

    val outputMediaFile: File?
        get() {
            val mediaStorageDir = File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    "Spottie")
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null
                }
            }
            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            return File(mediaStorageDir.path + File.separator + "IMG_" + timeStamp
                    + ".jpg")
        }

    fun changeLanguage(local_language: String) {
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = Locale(local_language)
        res.updateConfiguration(conf, dm)
    }

    fun getRealPathFromURI(contentUri: Uri): String? {
        var path: String? = null
        val projection = arrayOf(MediaStore.Images.Media.DATA, MediaStore.Images.Media.DATE_ADDED, MediaStore.Images.ImageColumns.ORIENTATION)
        val cursor = activity!!.contentResolver.query(contentUri,
                projection, MediaStore.Images.Media.DATE_ADDED, null, "date_added ASC")
        cursor!!.moveToFirst()
        if (cursor != null && cursor.moveToFirst()) {
            val uri = Uri.parse(cursor.getString(cursor
                    .getColumnIndex(MediaStore.Images.Media.DATA)))
            path = uri.toString()
            cursor.close()
        }
        return path
    }


    protected fun callFragmentWithAddBackStack(containerId: Int, fragment: Fragment, tag: String) {
        activity!!.supportFragmentManager
                .beginTransaction()
                .replace(containerId, fragment, tag)
                .setCustomAnimations(R.anim.slide_in_enter, R.anim.slide_in_exit,
                        R.anim.slide_pop_enter, R.anim.slide_pop_exit)
                .addToBackStack(tag)
                .commit()
    }

    fun callFragmentWithBackStack(containerId: Int, fragment: Fragment, tag: String) {
        activity!!.supportFragmentManager
                .beginTransaction()
                .replace(containerId, fragment, tag)
                .setCustomAnimations(R.anim.slide_in_enter, R.anim.slide_in_exit,
                        R.anim.slide_pop_enter, R.anim.slide_pop_exit)
                .addToBackStack(tag)
                .commit()
    }


    fun callFragment(containerId: Int, fragment: Fragment, tag: String?) {
        val transaction = activity!!.supportFragmentManager
                .beginTransaction()
                .add(containerId, fragment, tag)
                .setCustomAnimations(R.anim.slide_in_enter, R.anim.slide_in_exit,
                        R.anim.slide_pop_enter, R.anim.slide_pop_exit)
        if (tag != null)
            transaction.addToBackStack(tag)
                    .commit()
        else
            transaction
                    .commit()
    }

    fun callFragmentWithReplace(containerId: Int, fragment: Fragment, tag: String?) {
        val transaction = activity!!.supportFragmentManager
                .beginTransaction()
                .replace(containerId, fragment, tag)
                .setCustomAnimations(R.anim.slide_in_enter, R.anim.slide_in_exit,
                        R.anim.slide_pop_enter, R.anim.slide_pop_exit)
        if (tag != null)
            transaction.addToBackStack(tag)
                    .commit()
        else
            transaction
                    .commit()
    }

    fun clearBackStack() {
        activity!!.supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    fun DateFormatter(date: String): String {
        val a = date.replace("\\D+".toRegex(), "")
        val timeInMillis = java.lang.Long.parseLong(a)
        if (timeInMillis < 0)
            return ""
        val calendar = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault())
        calendar.timeInMillis = timeInMillis
        val _date = calendar.time
        val mYear = calendar.get(Calendar.YEAR)
        val mMonth = calendar.get(Calendar.MONTH) + 1
        val mDay = calendar.get(Calendar.DAY_OF_MONTH)
        return "$mDay/$mMonth/$mYear"
    }

    fun DateFormatter1(date: String): String {
        val a = date.replace("\\D+".toRegex(), "")
        val timeInMillis = java.lang.Long.parseLong(a)
        if (timeInMillis < 0)
            return ""
        val calendar = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault())
        calendar.timeInMillis = timeInMillis
        val _date = calendar.time
        val mYear = calendar.get(Calendar.YEAR)
        val mMonth = calendar.get(Calendar.MONTH) + 1
        val mDay = calendar.get(Calendar.DAY_OF_MONTH)
        return "$mYear-$mMonth-$mDay"
    }

    fun getUriFromUrl(thisUrl: String): Uri {
        val builder: Uri.Builder
        var url: URL? = null
        try {
            url = URL(thisUrl)
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        }

        builder = Uri.Builder()
                .scheme(url!!.protocol)
                .authority(url.authority)
                .appendPath(url.path)
        return builder.build()
    }

    fun DateHeader(date: String): Long {
        val a = date.replace("\\D+".toRegex(), "")
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = java.lang.Long.parseLong(a)
        return calendar.get(Calendar.DAY_OF_MONTH).toLong()
    }

    fun GetDateTime(): String {
        val calendar = Calendar.getInstance()
        val mYear = calendar.get(Calendar.YEAR)
        val mMonth = calendar.get(Calendar.MONTH)
        val mDay = calendar.get(Calendar.DAY_OF_MONTH)

        val month_date = SimpleDateFormat("MMM")
        val month_name = month_date.format(calendar.time)
        val delegate = "hh:mm aaa"
        val time = DateFormat.format(delegate, Calendar.getInstance().time) as String

        return "$month_name $mDay, $mYear $time"
    }


    fun Get24FormatTime(date: String): String {
        val a = date.replace("\\D+".toRegex(), "")
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = java.lang.Long.parseLong(a)

        val delegate = "hh:mm"

        return DateFormat.format(delegate, calendar.time) as String
    }

    fun GetDateTimeComment(milli: String): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = java.lang.Long.parseLong(DateMilli(milli))
        val mYear = calendar.get(Calendar.YEAR)
        val mDay = calendar.get(Calendar.DAY_OF_MONTH)

        val month_date = SimpleDateFormat("MMM")
        val month_name = month_date.format(calendar.time)
        val delegate = "hh:mm aaa"
        val time = DateFormat.format(delegate, calendar.time) as String

        return "$month_name $mDay, $mYear $time"
    }

    fun DateMilli(date: String): String {
        return date.replace("\\D+".toRegex(), "")
    }


    fun ClearAllFragments() {
        for (loop in 0 until activity!!.supportFragmentManager.backStackEntryCount) {
            activity!!.supportFragmentManager.popBackStackImmediate()
        }
    }

    fun getPath(uri: Uri): String {

        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = HooleyMain.activity!!.managedQuery(uri, projection, null, null, null)
        val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()

        return cursor.getString(column_index)

    }

    fun getZoomLevel(circle: Circle?): Int {
        var zoomLevel = 0
        if (circle != null) {
            val radius = circle.radius
            val scale = radius / 500
            zoomLevel = (16 - Math.log(scale) / Math.log(2.0)).toInt()
        }
        return zoomLevel
    }

    private lateinit var notificationManager: NotificationManager

    fun onTokenExpireLogOut() {
        notificationManager = ActivityBase.activity.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
        SocialClass.logoutFaceBook()
        val i = Intent(ActivityBase.activity, HooleyAuth::class.java)
        ActivityBase.activity.startActivity(i)
        HooleyApp.db.putBoolean(Constants.IS_LOGIN, false)
        HooleyApp.db.putBoolean(Constants.IS_GUEST_LOGIN, false)
        ActivityBase.activity.finish()
    }

    companion object {
        var searchView: SearchView? = null

        fun getDataColumn(context: Context, uri: Uri, selection: String,
                          selectionArgs: Array<String>): String? {
            var cursor: Cursor? = null
            val column = "_data"
            val projection = arrayOf(column)
            try {
                cursor = context.contentResolver.query(uri, projection, selection, selectionArgs, null)
                if (cursor != null && cursor.moveToFirst()) {
                    val column_index = cursor.getColumnIndexOrThrow(column)
                    return cursor.getString(column_index)
                }
            } finally {
                cursor?.close()
            }
            return null
        }
    }
}// Required empty public constructor
