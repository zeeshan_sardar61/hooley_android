package com.hooleyapp.hooley.fragments.ticket

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentTicketsRefundBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.load
import com.hooleyapp.hooley.model.MyTicketModel
import com.hooleyapp.hooley.others.DateUtils

/**
 * Created by Nauman on 5/7/2018.
 */

class MyTicketRefundFragment : BaseFragment() {

    var binding: FragmentTicketsRefundBinding? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_tickets_refund, container, false)
        HooleyMain.activity!!.supportActionBar!!.show()

        //        getSendGiftTickets();
        binding!!.tvEventName.text = model.eventName
        binding!!.tvEventDate.text = DateUtils.convertDate(model.startTime, model.endTime)
        binding!!.tvEventAddress.text = model.eventAddress
        binding!!.tvEventCategory.text = "Category (" + model.ticketCategoryName + ")"
        binding!!.tvEventTotalTickets.text = model.ticketList.size.toString() + " Total Tickets"
        binding!!.tvTicketsCounts.text = model.ticketList.size.toString()
        binding!!.ivEventPic.load(model.coverPhoto)


        binding!!.ivAddCount.setOnClickListener {
            if (Integer.parseInt(binding!!.tvTicketsCounts.text.toString()) < model.ticketList.size) {
                binding!!.tvTicketsCounts.text = (Integer.parseInt(binding!!.tvTicketsCounts.text.toString()) + 1).toString()
            }
        }

        binding!!.ivMinusCount.setOnClickListener {
            if (Integer.parseInt(binding!!.tvTicketsCounts.text.toString()) != 1) {
                binding!!.tvTicketsCounts.text = (Integer.parseInt(binding!!.tvTicketsCounts.text.toString()) - 1).toString()
            }
        }


        return binding!!.root
    }


    companion object {

        lateinit var instance: MyTicketRefundFragment
        private lateinit var model: MyTicketModel.MyTicketsList

        fun newInstance(myTicketsList: MyTicketModel.MyTicketsList): MyTicketRefundFragment {
            instance = MyTicketRefundFragment()
            model = myTicketsList
            return instance
        }
    }
}
