package com.hooleyapp.hooley.fragments.invites

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.animation.AnimationUtils
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterPastGuestsList
import com.hooleyapp.hooley.databinding.FragmentPastGuestListDetailBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.hideKeyboard
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GetPastEventGuestListModel
import com.hooleyapp.hooley.model.PastGuestListModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.tables.FriendsTable

@SuppressLint("ValidFragment")

class PastGuestListFriendsFragment(var model: PastGuestListModel.Guest, var selectedAll: Boolean) : BaseFragment(), View.OnClickListener, TextWatcher, AdapterPastGuestsList.IPastGuestListListener {

    internal var guestList: MutableList<FriendsTable>? = null
    private val mListSearch = ArrayList<FriendsTable>()
    var binding: FragmentPastGuestListDetailBinding? = null
    private var adapterPastGuestsList: AdapterPastGuestsList? = null
    private var objectResponse: GetPastEventGuestListModel? = null
    private val addFriendList = ArrayList<Int>()
    private var service: FriendWebService? = null
    internal var checkedIndex = -1
    private var guestListIds: java.util.ArrayList<Int>? = null
    private var clickedPosition: Int = -1
    var count = 0


    private var totalGuestCount = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_past_guest_list_detail, container, false)
        //        HooleyMain.activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setHasOptionsMenu(true)
        setListener()
        setToolbar()
        getPastGuestList()
        var parts = model.invitGuestCount.split(" ", ignoreCase = true)
        totalGuestCount = parts[0]
        Log.d("ok", totalGuestCount)
        return binding!!.root
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
        inflater!!.inflate(R.menu.live_post_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.share -> {
                HooleyMain.activity!!.hideKeyboard()
                if (validInput()) {
                    getSelectedGuestsId()
                    if (checkedIndex != -1) {
                        intent = Intent()
                        intent!!.putExtra("guestListIds", guestListIds)
                        HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                        targetFragment!!.onActivityResult(
                                targetRequestCode,
                                Activity.RESULT_OK,
                                intent
                        )
                    }
                } else {
                    HooleyMain.activity!!.showSuccessDialog("No items selected")
                }
                return super.onOptionsItemSelected(item)
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun setListener() {
        binding!!.btnDone.setOnClickListener(this)
        binding!!.tbSelectAll.setOnClickListener(this)
        binding!!.edtSearch.addTextChangedListener(this)
        binding!!.ivClose.setOnClickListener(this)

    }

    private fun setToolbar() {
        binding!!.tvHooleyFriends.text = model.eventName
    }


    private fun getPastGuestList() {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        HooleyMain.activity!!.showDialog()
        service = FriendWebService()
        service!!.getPastGuestList(model.eventid, object : GenericCallback<GetPastEventGuestListModel> {
            override fun success(result: GetPastEventGuestListModel) {
                HooleyMain.activity!!.removeDialog()
                objectResponse = result
                setRecyclerView()
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    binding!!.rlSearchBar.visibility = View.GONE
                    binding!!.rlAllSelect.visibility = View.GONE
                    binding!!.rvSelectFriend.visibility = View.GONE
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.view.visibility = View.GONE
                    binding!!.btnDone.visibility = View.GONE
                    binding!!.tvNoData.text = message
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }


    private fun setRecyclerView() {
        binding!!.tvHooleyFriends.text = model.eventName
        if (guestList == null)
            guestList = ArrayList()
        guestList!!.clear()
        guestList!!.addAll(objectResponse!!.guestList)
        for (i in guestList!!.indices) {
            if (model.userIdList != null && model.userIdList.size > 0) {
                if (model.userIdList.contains(guestList!![i].userId)) {
                    guestList!![i].isChecked = true
                    checkedIndex = i
                }
            }
        }
        binding!!.tvPeopleCount.text = if (guestList!!.size == 1) {
            "${guestList!!.size} Person "
        } else {
            "${guestList!!.size} People "
        }
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        binding!!.rvSelectFriend.layoutManager = manager
        binding!!.rvSelectFriend.layoutAnimation = animation
        adapterPastGuestsList = AdapterPastGuestsList(guestList)
        adapterPastGuestsList!!.mListener = this
        binding!!.rvSelectFriend.adapter = adapterPastGuestsList

        if (selectedAll) {
            binding!!.tbSelectAll.performClick()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    private var intent: Intent? = null


    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnDone -> {
                HooleyMain.activity!!.hideKeyboard()
                if (validInput()) {
                    getSelectedGuestsId()
                    if (checkedIndex != -1) {
                        intent = Intent()
                        intent!!.putExtra("guestListIds", guestListIds)
                        HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                        targetFragment!!.onActivityResult(
                                targetRequestCode,
                                Activity.RESULT_OK,
                                intent
                        )
                    }
                } else {
                    HooleyMain.activity!!.showSuccessDialog("No items selected")
                }
            }
            R.id.ivClose -> binding!!.edtSearch.setText("")
            R.id.tbSelectAll -> selectAll(binding!!.tbSelectAll.isChecked)
        }

    }


    private fun validInput(): Boolean {
        if (guestList!!.size > 0) {
            for (i in guestList!!.indices) {
                if (guestList!![i].isChecked) {
                    return true
                    break
                }
            }
            return false
        } else {
            return false
        }
    }

    private fun selectAll(check: Boolean) {
        for (i in guestList!!.indices) {
            guestList!![i].isChecked = check
            if (check) {
                checkedIndex = i
                model.isSelectedAll = true
                model.selection = 1
            } else {
                checkedIndex = -1
                model.selection = 0
            }

        }


        if (adapterPastGuestsList != null)
            adapterPastGuestsList!!.notifyDataSetChanged()
    }

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

    }

    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

    }

    override fun afterTextChanged(s: Editable) {
        if (s.isNotEmpty()) {
            if (guestList == null)
                return
            guestList!!.clear()
            if (objectResponse != null && objectResponse!!.guestList.size > 0) {
                if (mListSearch.size > 0)
                    mListSearch.clear()
                mListSearch.addAll(objectResponse!!.guestList)
            }
            for (i in mListSearch.indices) {
                if (mListSearch[i].fullName!!.toLowerCase().contains(s.toString().toLowerCase())) {
                    guestList!!.add(mListSearch[i])
                }
            }

            if (guestList!!.size == 0) {
                binding!!.tvNoData.visibility = View.VISIBLE
                binding!!.tvNoData.text = "No Record Found"
                binding!!.tvPeopleCount.text = if (guestList!!.size == 1) {
                    "${guestList!!.size} Person "
                } else {
                    "${guestList!!.size} People"
                }

            } else {
                binding!!.tvNoData.visibility = View.GONE
                binding!!.tvPeopleCount.text = if (guestList!!.size == 1) {
                    "${guestList!!.size} Person "
                } else {
                    "${guestList!!.size} People"
                }
            }
            if (adapterPastGuestsList != null)
                adapterPastGuestsList!!.notifyDataSetChanged()
        } else {
            binding!!.tvNoData.visibility = View.GONE
            if (guestList != null)
                guestList!!.clear()
            guestList!!.addAll(mListSearch)
            binding!!.tvPeopleCount.text = if (guestList!!.size == 1) {
                "${guestList!!.size} Person "
            } else {
                "${guestList!!.size} People"
            }
            if (adapterPastGuestsList != null)
                adapterPastGuestsList!!.notifyDataSetChanged()
        }
    }


    override fun onClickItem(position: Int) {
        clickedPosition = position
        count = 0
        for (i in guestList!!.indices) {
            if (guestList!![i].isChecked) {
                count++
            }
        }

        if (count == Integer.parseInt(totalGuestCount)) {
            checkedIndex = position
            binding!!.tbSelectAll.isChecked = true
        } else if (count > 0) {
            checkedIndex = position
            binding!!.tbSelectAll.isChecked = false
        } else if (count == 0) {
            checkedIndex = -1
            binding!!.tbSelectAll.isChecked = false
        }
    }

    private fun getSelectedGuestsId() {
        if (guestListIds == null)
            guestListIds = java.util.ArrayList()
        if (guestListIds!!.size > 0)
            guestListIds!!.clear()

        for (i in guestList!!.indices) {
            if (guestList!![i].isChecked) {
                guestListIds!!.add(guestList!![i].userId)
            }
        }
    }


}
