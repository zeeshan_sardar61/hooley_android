package com.hooleyapp.hooley.fragments.auth

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import com.google.android.gms.maps.model.LatLng
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyAuth
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterGender
import com.hooleyapp.hooley.databinding.FragmentCompleteRegisterBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.hideKeyboard
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.LoginModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.AuthWebService
import java.util.*

class CompleteRegisterFragment : BaseFragment(), AdapterView.OnItemSelectedListener, View.OnClickListener {
    var binding: FragmentCompleteRegisterBinding? = null
    var service = AuthWebService()
    var isEmailFound = true
    private var email = ""
    private var mListGender: ArrayList<String>? = null
    private var spinnerAdapter: AdapterGender? = null
    private var gender = -1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_complete_register, container, false)
        checkForFields()
        setListener()
        initGenderSpinner()
        return binding!!.root
    }

    fun setListener() {
        binding!!.spGender.onItemSelectedListener = this
        binding!!.btnSignUp.setOnClickListener(this)
    }

    fun checkForFields() {
        //        if (!TextUtils.isEmpty(HooleyApp.db.getString(Constants.SOCIAL_USER_GENDER))) {
        //            gender = HooleyApp.db.getString(Constants.SOCIAL_USER_GENDER);
        //            userSearchbinding.rlGender.setVisibility(View.GONE);
        //        } else {
        //            userSearchbinding.rlGender.setVisibility(View.VISIBLE);
        //        }
        if (!TextUtils.isEmpty(HooleyApp.db.getString(Constants.SOCIAL_USER_EMAIL))) {
            email = HooleyApp.db.getString(Constants.SOCIAL_USER_EMAIL)
            isEmailFound = true
            binding!!.rlEmail.visibility = View.GONE
        } else {
            isEmailFound = false
            binding!!.rlEmail.visibility = View.VISIBLE
        }

    }

    private fun initGenderSpinner() {
        mListGender = ArrayList()
        mListGender!!.add("Female")
        mListGender!!.add("Male")
        mListGender!!.add("Other")
        spinnerAdapter = AdapterGender(HooleyAuth.activity, R.layout.sp_gender_group_item, mListGender)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spGender.adapter = spinnerAdapter
    }


    private fun validateInput(): Boolean {
        if (!isEmailFound) {
            if (Util.isContentNull(binding!!.edEmail.text.toString())) {
                binding!!.edEmail.requestFocus()
                binding!!.edEmail.error = getString(R.string.str_error_email)
                return false
            } else if (!Util.isEmailValid(binding!!.edEmail.text.toString().trim())) {
                binding!!.edEmail.requestFocus()
                binding!!.edEmail.error = getString(R.string.invalid_email)
                return false
            }
        }
        return true

    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }


    fun registerUser() {
        if (!NetworkUtil.isInternetConnected(HooleyAuth.activity)) {
            HooleyAuth.activity.showSuccessDialog(Constants.NO_NETWORK)
            return
        }
        HooleyAuth.activity.showDialog()
        email = if (isEmailFound) {
            HooleyApp.db.getString(Constants.SOCIAL_USER_EMAIL)
        } else {
            binding!!.edEmail.text.toString()
        }
        service.registerUser(HooleyApp.db.getString(Constants.SOCIAL_USER_FIRST_NAME), HooleyApp.db.getString(Constants.SOCIAL_USER_LAST_NAME), email, "", HooleyApp.db.getString(Constants.SOCIAL_USER_PROFILE_PIC), socialMediaType!!, isEmailFound, HooleyApp.db.getString(Constants.SOCIAL_ID), HooleyApp.db.getString(Constants.SOCIAL_ID), object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyAuth.activity.removeDialog()
                if (!isEmailFound)
                    (activity as ActivityBase).callFragmentWithReplace(R.id.authContainer, ConfirmRegisterFragment.newInstance(binding!!.edEmail.text.toString()), "ConfirmRegisterFragment")
                else {

                }
            }

            override fun failure(message: String) {
                HooleyAuth.activity.removeDialog()
                try {
                    HooleyAuth.activity.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }


    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        when (position) {
            0 -> {
                gender = 2
                HooleyApp.db.putString(Constants.SOCIAL_USER_GENDER, "female")
            }
            1 -> {
                gender = 1
                HooleyApp.db.putString(Constants.SOCIAL_USER_GENDER, "male")
            }
            else -> {
                gender = 3
                HooleyApp.db.putString(Constants.SOCIAL_USER_GENDER, "other")
            }
        }

    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnSignUp -> if (validateInput()) {
                HooleyAuth.activity.hideKeyboard()
                if (isEmailFound)
                    loginWithSMUser(socialMediaType)
                else
                    registerUser()
            }
        }

    }

    private fun loginWithSMUser(loginMedia: String?) {

        if (!NetworkUtil.isInternetConnected(HooleyAuth.activity)) {
            HooleyAuth.activity.showSuccessDialog(Constants.NO_NETWORK)
            return
        }
        HooleyAuth.activity.showDialog()
        service.loginWithSMUser(loginMedia!!, object : GenericCallback<LoginModel> {
            override fun success(result: LoginModel) {
                HooleyAuth.activity.removeDialog()

                HooleyApp.db.putBoolean(Constants.IS_FIRST_LOGIN, result.isFirstLogin)
                HooleyApp.db.putBoolean(Constants.IS_BUSINESS_PROFILE_CREATED, result.isBusinessProfile)
                HooleyApp.db.putString(Constants.USER_ID, result.userId!!)
                HooleyApp.db.putString(Constants.EMAIL_ID, result.generalProfile!!.userInfo.email!!)
                HooleyApp.db.putString(Constants.AUTH_TOKKEN, result.userToken!!)
                HooleyApp.db.putString(Constants.FULL_NAME, result.generalProfile!!.userInfo.firstName + " " + result.generalProfile!!.userInfo.lastName)
                if (!TextUtils.isEmpty(result.generalProfile!!.userInfo.profilePicture))
                    HooleyApp.db.putString(Constants.USER_AVATAR, result.generalProfile!!.userInfo.profilePicture!!)

                if (result.generalProfile!!.userGeofence.size > 0) {
                    if (result.generalProfile!!.userGeofence.size > 1) {
                        val listLatLong = ArrayList<LatLng>()
                        for (i in result.generalProfile!!.userGeofence.indices) {
                            listLatLong.add(LatLng(java.lang.Double.parseDouble(result.generalProfile!!.userGeofence[i].latitude), java.lang.Double.parseDouble(result.generalProfile!!.userGeofence[i].longitude)))
                        }
                        val centerLatLngObj = (HooleyMain.activity as ActivityBase).computeCentroid(listLatLong)
                        HooleyApp.db.putDouble(Constants.PERSONAL_LAT, centerLatLngObj.latitude)
                        HooleyApp.db.putDouble(Constants.PERSONAL_LANG, centerLatLngObj.latitude)
                        HooleyApp.db.putString(Constants.PERSONAL_RADIUS, "5")
                    } else {
                        HooleyApp.db.putDouble(Constants.PERSONAL_LAT, java.lang.Double.parseDouble(result.generalProfile!!.userGeofence[0].latitude))
                        HooleyApp.db.putDouble(Constants.PERSONAL_LANG, java.lang.Double.parseDouble(result.generalProfile!!.userGeofence[0].longitude))
                        HooleyApp.db.putString(Constants.PERSONAL_RADIUS, "5")
                    }
                }
                if (!result.isFirstLogin || !result.generalProfile!!.userInfo.isPhoneVerified) {
                    (activity as ActivityBase).callFragmentWithReplace(R.id.authContainer, ProfileSetupFragment.newInstance(), "ProfileSetupFragment")
                } else {
                    val intent = Intent(activity, HooleyMain::class.java)
                    startActivity(intent)
                    activity!!.finish()
                }
            }

            override fun failure(message: String) {
                HooleyAuth.activity.removeDialog()

                try {
                    HooleyAuth.activity.showSuccessDialog(message)
                    if (message == "No user found") {
                        (activity as ActivityBase).callFragmentWithReplace(R.id.authContainer, CompleteRegisterFragment.newInstance(loginMedia), "CompleteRegisterFragment")
                    }
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }


            }
        })
    }

    companion object {

        lateinit var instance: CompleteRegisterFragment
        private var socialMediaType: String? = null

        fun newInstance(mediaType: String?): CompleteRegisterFragment {
            instance = CompleteRegisterFragment()
            socialMediaType = mediaType
            return instance
        }
    }
}
