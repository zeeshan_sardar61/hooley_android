package com.hooleyapp.hooley.fragments.event

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.databinding.DataBindingUtil
import android.location.Location
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.view.*
import android.widget.Toast
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.Tricks.ViewPagerEx
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.VPDashBoardAdapter
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventDetailModel
import com.hooleyapp.hooley.app.view.ui.fragments.event.ChatStreamFragment
import com.hooleyapp.hooley.app.view.ui.fragments.event.EventDetailFragment
import com.hooleyapp.hooley.app.view.ui.fragments.event.WhosHereFragment
import com.hooleyapp.hooley.databinding.FragmentLiveEventBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.others.HooleyWalletCardsFragment
import com.hooleyapp.hooley.fragments.others.HooleyWalletFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IBuyPPV
import com.hooleyapp.hooley.interfaces.ICallBackUri
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.BubbleListItem
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.ItemPostMedia
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.services.EventWebService
import com.hooleyapp.hooley.services.TicketWebService
import java.util.*

/**
 * Created by Zeeshan on 18-Jan-18.
 */

class LiveEventFragment : BaseFragment(), View.OnClickListener, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener, IBuyPPV {

    private var adapter: VPDashBoardAdapter? = null
    private var eventWebService: EventWebService? = null
    private var showBubble = false
    private var moreDialogeFragment: MoreDialogeFragment? = null
    private val gson = Gson()
    private var mBubbleList: ArrayList<BubbleListItem>? = null
    private lateinit var hooleyWalletFragment: HooleyWalletCardsFragment
    private lateinit var hooleyWallet: HooleyWalletFragment
    private val BUY_REQUEST_CODE: Int = 3000
    private var stripeTokken: String? = null
    var ticketWebService = TicketWebService()
    private lateinit var eventGalleryFragment: EventGalleryFragment
    private lateinit var whosHereFragment: WhosHereFragment

    private val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.extras!!.getString("tag")) {
                Constants.TYPE_MESSAGES_ALERT -> fragmentLiveEventbinding!!.ivChatStreamLiveCircle.visibility = View.VISIBLE
                Constants.TYPE_EVENT_GALLERY_ALERT -> fragmentLiveEventbinding!!.ivGalleryLiveCircle.visibility = View.VISIBLE
                Constants.TYPE_WHOS_HER_ALERT -> fragmentLiveEventbinding!!.ivWhosHereLiveCircle.visibility = View.VISIBLE
            }
        }
    }

    private val currentFragment: Fragment
        get() = adapter!!.getItem(fragmentLiveEventbinding!!.vpEventGallery.currentItem)

    fun setToolBar() {
        try {
            (HooleyMain.activity as HooleyMain).setToolbarTitle(objectResult.eventDisplayBasicInfo!!.eventName!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

//    if(HooleyApp.db.getString("Block").isNotEmpty()){
//        Log.e("ok", "LiveVisible")
//        HooleyApp.db.remove("Block")
//        getEventDetails()
//    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentLiveEventbinding = DataBindingUtil.inflate(inflater, R.layout.fragment_live_event, container, false)
        setListener()
        getEventDetails()
        (HooleyMain.activity as HooleyMain).removeBubbleView()

        LocalBroadcastManager.getInstance(HooleyMain.activity!!).registerReceiver(mMessageReceiver,
                IntentFilter("ChatStreamMessage"))
        handleSilentPush()
        setMoreCallerBackListener()
        return fragmentLiveEventbinding!!.root
    }

    fun handleSilentPush() {
        if (HooleyApp.db.getBoolean(Constants.SHOW_CHAT_ALERT, false))
            fragmentLiveEventbinding!!.ivChatStreamLiveCircle.visibility = View.VISIBLE
        if (HooleyApp.db.getBoolean(Constants.SHOW_GALLERY_ALERT, false))
            fragmentLiveEventbinding!!.ivGalleryLiveCircle.visibility = View.VISIBLE
        if (HooleyApp.db.getBoolean(Constants.SHOW_WHO_HERE_ALERT, false))
            fragmentLiveEventbinding!!.ivWhosHereLiveCircle.visibility = View.VISIBLE
    }

    private fun checkForSelectedItem() {
        when (HooleyApp.db.getInt(Constants.SHOW_LIVE_EVENT_TAB, 2)) {
            0 -> {
                setSelectedBackground(R.id.llEventGallery)
                fragmentLiveEventbinding!!.vpEventGallery.setCurrentItem(0, true)
            }
            1 -> {
                setSelectedBackground(R.id.llWhoHere)
                fragmentLiveEventbinding!!.vpEventGallery.setCurrentItem(1, true)
            }
            2 -> {
                setSelectedBackground(R.id.llDetail)
                fragmentLiveEventbinding!!.vpEventGallery.setCurrentItem(2, true)
            }
        }
    }


    private fun setSelectedTab() {
        fragmentLiveEventbinding!!.vpEventGallery.setCurrentItem(HooleyApp.db.getInt(Constants.SHOW_LIVE_EVENT_TAB), false)
        when (fragmentLiveEventbinding!!.vpEventGallery.currentItem) {
            0 -> setSelectedBackground(R.id.llEventGallery)
            1 -> setSelectedBackground(R.id.llWhoHere)
            2 -> setSelectedBackground(R.id.llDetail)
        }
    }

    private fun setListener() {
        fragmentLiveEventbinding!!.llEventGallery.setOnClickListener(this)
        fragmentLiveEventbinding!!.llWhoHere.setOnClickListener(this)
        fragmentLiveEventbinding!!.llChatStream.setOnClickListener(this)
        fragmentLiveEventbinding!!.llLiveEventCamera.setOnClickListener(this)
        fragmentLiveEventbinding!!.llDetail.setOnClickListener(this)
        fragmentLiveEventbinding!!.btnRetry.setOnClickListener(this)
        eventGalleryFragment = EventGalleryFragment()
        whosHereFragment = WhosHereFragment()
        eventGalleryFragment.mListener = this
        whosHereFragment.mListener = this
    }

    private fun setupViewPager(objectResult: EventDetailModel) {
        try {
            fragmentLiveEventbinding!!.rbTop.visibility = View.VISIBLE
            adapter = VPDashBoardAdapter(childFragmentManager)
            adapter!!.addFragment(eventGalleryFragment, "EventGalleryFragment")
            adapter!!.addFragment(whosHereFragment, "WhosHereFragment")
            adapter!!.addFragment(EventDetailFragment(eventId, objectResult), "EventDetailFragment")
            fragmentLiveEventbinding!!.vpEventGallery.adapter = adapter
            checkForSelectedItem()
        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
            return
        }
    }

    private fun setUpSinglePagerItem(objectResult: EventDetailModel) {
        try {
        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
            return
        }

        fragmentLiveEventbinding!!.rbTop.visibility = View.GONE
        adapter = VPDashBoardAdapter(childFragmentManager)
        adapter!!.addFragment(EventDetailFragment(eventId, objectResult), "EventDetailFragment")
        fragmentLiveEventbinding!!.vpEventGallery.adapter = adapter
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.llEventGallery -> {
                if (objectResult.isMyEvent) {
                    fragmentLiveEventbinding!!.ivGalleryLiveCircle.visibility = View.GONE
                    HooleyApp.db.putBoolean(Constants.SHOW_GALLERY_ALERT, false)
                    HooleyApp.db.putInt(Constants.SHOW_LIVE_EVENT_TAB, 0)
                    setSelectedBackground(R.id.llEventGallery)
                    fragmentLiveEventbinding!!.vpEventGallery.setCurrentItem(0, true)
                } else {
                    setSelectedBackground(R.id.llEventGallery)
                    fragmentLiveEventbinding!!.vpEventGallery.setCurrentItem(0, true)
                }
            }
            R.id.llWhoHere -> {
                if (objectResult.isMyEvent) {
                    fragmentLiveEventbinding!!.ivWhosHereLiveCircle.visibility = View.GONE
                    HooleyApp.db.putBoolean(Constants.SHOW_WHO_HERE_ALERT, false)
                    HooleyApp.db.putInt(Constants.SHOW_LIVE_EVENT_TAB, 1)
                    setSelectedBackground(R.id.llWhoHere)
                    fragmentLiveEventbinding!!.vpEventGallery.setCurrentItem(1, true)
                } else {
                    setSelectedBackground(R.id.llWhoHere)
                    fragmentLiveEventbinding!!.vpEventGallery.setCurrentItem(1, true)
                }
            }
            R.id.llLiveEventCamera -> {
                if (!objectResult.eventDisplayBasicInfo!!.isPastEvent) {
                    if (objectResult.isTicketSetup) {
                        if (objectResult.ppvPrice > 0) {
                            if (!objectResult.isPpvPurchased && !objectResult.isMyEvent) {
                                HooleyMain.activity!!.showSuccessDialog("Buy PPV in order to access Camera")
                            } else {
                                if (!isPostAllow)
                                    return
                                moreDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
                            }
                        } else {
                            if (!isPostAllow)
                                return
                            moreDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
                        }
                    } else {
                        if (!isPostAllow)
                            return
                        moreDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
                    }
                }

            }
            R.id.llChatStream -> {
                if (objectResult.isTicketSetup) {
                    if (objectResult.ppvPrice > 0) {
                        if (!objectResult.isPpvPurchased && !objectResult.isMyEvent) {
                            HooleyMain.activity!!.showSuccessDialog("Buy PPV in order to access Chat")
                        } else {

                            fragmentLiveEventbinding!!.ivChatStreamLiveCircle.visibility = View.GONE
                            HooleyApp.db.putBoolean(Constants.SHOW_CHAT_ALERT, false)
                            (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ChatStreamFragment.newInstance(eventId, (objectResult.isAttended || objectResult.isFollow)), "ChatStreamFragment")
                        }
                    } else {
                        fragmentLiveEventbinding!!.ivChatStreamLiveCircle.visibility = View.GONE
                        HooleyApp.db.putBoolean(Constants.SHOW_CHAT_ALERT, false)
                        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ChatStreamFragment.newInstance(eventId, (objectResult.isAttended || objectResult.isFollow)), "ChatStreamFragment")
                    }
                } else {
                    fragmentLiveEventbinding!!.ivChatStreamLiveCircle.visibility = View.GONE
                    HooleyApp.db.putBoolean(Constants.SHOW_CHAT_ALERT, false)
                    (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ChatStreamFragment.newInstance(eventId, (objectResult.isAttended || objectResult.isFollow)), "ChatStreamFragment")
                }
            }
            R.id.llDetail -> {
                //                ((ActivityBase) HooleyMain.Companion.getActivity()).callFragment(R.id.container, EventDetailFragment.newInstance(eventId), "EventDetailFragment");
                setSelectedBackground(R.id.llDetail)
                HooleyApp.db.putInt(Constants.SHOW_LIVE_EVENT_TAB, 2)
                fragmentLiveEventbinding!!.vpEventGallery.setCurrentItem(2, true)
            }
            R.id.btnRetry -> getEventDetails()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
    }

    private fun setSelectedBackground(id: Int) {
        when (id) {
            R.id.llEventGallery -> {
                fragmentLiveEventbinding!!.llEventGallery.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.selected_tabs_color))
                fragmentLiveEventbinding!!.llWhoHere.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.unselected_tabs_color))
                fragmentLiveEventbinding!!.llDetail.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.unselected_tabs_color))
            }
            R.id.llWhoHere -> {
                fragmentLiveEventbinding!!.llEventGallery.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.unselected_tabs_color))
                fragmentLiveEventbinding!!.llWhoHere.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.selected_tabs_color))
                fragmentLiveEventbinding!!.llDetail.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.unselected_tabs_color))
            }
            R.id.llDetail -> {
                fragmentLiveEventbinding!!.llEventGallery.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.unselected_tabs_color))
                fragmentLiveEventbinding!!.llWhoHere.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.unselected_tabs_color))
                fragmentLiveEventbinding!!.llDetail.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.selected_tabs_color))
            }
        }
    }

    private fun getEventDetails() {
        HooleyMain.activity!!.showDialog()
        eventWebService = EventWebService()
        eventWebService!!.seeEventDetail(eventId, object : IWebServiceCallback<EventDetailModel> {
            override fun success(result: EventDetailModel) {
                HooleyMain.activity!!.removeDialog()
                objectResult = result
                isPostAllow = objectResult.eventDisplayBasicInfo!!.isPostAllow
                setToolBar()
                if (fragmentLiveEventbinding != null) {
                    fragmentLiveEventbinding!!.llRetry.visibility = View.GONE

                    if (objectResult.isCanceledEvent) {
                        if (objectResult.isCanceledEvent) {
                            var isPaid = when (objectResult.eventDisplayBasicInfo?.eventType) {
                                "2", "3" ->
                                    true
                                else ->
                                    false
                            }
                            (HooleyMain.activity as ActivityBase).addFragment(R.id.container, CancelledEventFragment.newInstance(objectResult.eventDisplayBasicInfo!!.eventName!!,
                                    objectResult.eventHostList!![0].fullName!!,
                                    objectResult.eventDisplayBasicInfo!!.profilePic!!,
                                    objectResult.cancelReason,
                                    objectResult.eventCoversList,
                                    isPaid), null)
                        }

                    } else {
                        if (objectResult.isLive || objectResult.eventDisplayBasicInfo!!.isPastEvent) {
                            if (adapter != null) {
                                adapter!!.notifyDataSetChanged()
                                fragmentLiveEventbinding!!.vpEventGallery.invalidate()
                                setupViewPager(objectResult)
                            } else {
                                setupViewPager(objectResult)
                            }
                            if (objectResult.isLive) {
                                showBubble = true
                                HooleyApp.db.putString(Constants.CHAT_HEAD_EVENT_AVATAR, objectResult.eventCoversList[0].photoUrl!!)
                                HooleyApp.db.putString(Constants.CHAT_HEAD_EVENT_ID, eventId)
                            }
                        } else {
                            showBubble = HooleyApp.db.getBoolean(Constants.HAS_BUBBLE_HISTORY)
                            setUpSinglePagerItem(objectResult)
                        }
                    }

                }
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    if (message.contains("timeout"))
                        fragmentLiveEventbinding!!.llRetry.visibility = View.VISIBLE
                    else
                        HooleyMain.activity!!.showSuccessDialog(message)

                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })

    }

    override fun onSliderClick(slider: BaseSliderView) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {

    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    private fun setMoreCallerBackListener() {
        val arrayList = ArrayList<String>()
        arrayList.add("Photo")
        arrayList.add("Video")
        arrayList.add("Gallery")
        moreDialogeFragment = MoreDialogeFragment.Builder(activity!!).setTitle("Select").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, _ ->
            when (position) {
                0 -> {
                    moreDialogeFragment!!.dismiss()
                    if (checkInsideLocation(HooleyApp.db.getDouble(Constants.CURRENT_LAT, 0.0).toFloat(), HooleyApp.db.getDouble(Constants.CURRENT_LANG, 0.0).toFloat())) {
                        HooleyMain.activity!!.startCamera(Constants.CAMERA_RQ)
                    } else {
                        HooleyMain.activity!!.showSuccessDialog("You can only use the camera at the event")
                    }
                }
                1 -> {
                    moreDialogeFragment!!.dismiss()
                    if (checkInsideLocation(HooleyApp.db.getDouble(Constants.CURRENT_LAT, 0.0).toFloat(), HooleyApp.db.getDouble(Constants.CURRENT_LANG, 0.0).toFloat())) {
                        HooleyMain.activity!!.startVideoCamera(Constants.VIDEO_RQ)
                    } else {
                        HooleyMain.activity!!.showSuccessDialog("You can only post media that was taken at the event")
                    }
                }
                2 -> {
                    moreDialogeFragment!!.dismiss()
                    HooleyMain.activity!!.showCameraGalleryImage(Constants.IMAGE_PICKER_SELECT)
                }
            }
        }.create()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            try {
                when (requestCode) {
                    Constants.CAMERA_RQ -> HooleyMain.activity!!.processCapturedPhoto(object : ICallBackUri {
                        override fun imageUri(result: Uri?) {
                            var exif = ExifInterface(result!!.path)
                            var latLong = FloatArray(2)
                            var hasLatLong: Boolean = exif.getLatLong(latLong)
                            if (hasLatLong) {
                                if (checkInsideLocation(latLong[0], latLong[1])) {
                                    itemPostMedia = ItemPostMedia(result, false)
                                    (HooleyMain.activity as ActivityBase).callFragmentWithReplaceWithAllowingStateLoss(R.id.container, LivePostFragment.newInstance(eventName, eventId, itemPostMedia!!), null)
                                } else {
                                    HooleyMain.activity!!.showSuccessDialog("You can only use the camera at the event")
                                }
                            } else {
                                HooleyMain.activity!!.showSuccessDialog("Please enable GeoLocation tags in Camera Setting first, then capture pictures to upload")
                            }
                        }
                    })
                    VIDEO_RQ -> HooleyMain.activity!!.processCapturedVideo(object : ICallBackUri {
                        override fun imageUri(result: Uri?) {
                            var exif = ExifInterface(result!!.path)
                            var latLong = FloatArray(2)
                            var hasLatLong: Boolean = exif.getLatLong(latLong)
                            if (hasLatLong) {
                                if (checkInsideLocation(latLong[0], latLong[1])) {
                                    itemPostMedia = ItemPostMedia(result, true)
                                    (HooleyMain.activity as ActivityBase).callFragmentWithReplaceWithAllowingStateLoss(R.id.container, LivePostFragment.newInstance(eventName, eventId, itemPostMedia!!), null)
                                } else {
                                    HooleyMain.activity!!.showSuccessDialog("You can only use the camera at the event")
                                }
                            } else {
                                HooleyMain.activity!!.showSuccessDialog("Please enable GeoLocation tags in Camera Setting first, then capture pictures to upload")
                            }
                        }
                    })
                    IMAGE_PICKER_SELECT -> {
                        HooleyMain.activity!!.processGalleryPhoto(data!!, object : ICallBackUri {
                            override fun imageUri(result: Uri?) {
                                var exif = ExifInterface(result!!.path)
                                var latLong = FloatArray(2)
                                var hasLatLong: Boolean = exif.getLatLong(latLong)
                                if (hasLatLong) {
                                    if (checkInsideLocation(latLong[0], latLong[1])) {
                                        itemPostMedia = if (result.toString().contains("mp4")) {
                                            ItemPostMedia(result, true)
                                        } else {
                                            ItemPostMedia(result, false)
                                        }
                                        (HooleyMain.activity as ActivityBase).callFragmentWithReplaceWithAllowingStateLoss(R.id.container, LivePostFragment.newInstance(eventName, eventId, itemPostMedia!!), null)
                                    } else {
                                        HooleyMain.activity!!.showSuccessDialog("You can only post media that was taken at the event")
                                    }
                                } else {
                                    HooleyMain.activity!!.showSuccessDialog("Please enable GeoLocation tags in Camera Setting first, then capture pictures to upload")
                                }
                            }

                        })
                    }
                    BUY_REQUEST_CODE -> {
                        stripeTokken = data!!.extras?.get("stripeToken").toString()
                        callApiBuyTickets(eventId, 0, stripeTokken!!, "0", "ppv", 0)
                    }
                }
            } catch (e: Exception) {

                e.printStackTrace()
            }
        } else {
            Toast.makeText(HooleyMain.activity, "Action Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    private fun callApiBuyTickets(eventId: String, totalTickets: Int, stripeTokken: String, eventTicketId: String, ticketType: String, donateAmount: Int) {
        HooleyMain.activity!!.showDialog()
        ticketWebService.buyTicket(eventId, totalTickets, stripeTokken, eventTicketId, ticketType, donateAmount, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                fragmentLiveEventbinding!!.llDetail.performClick()
                HooleyMain.activity!!.showSuccessDialog("Payment Successful")
                getEventDetails()
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.removeDialog()
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun checkInsideLocation(media_lat: Float, media_long: Float): Boolean {
        var eventLocation = Location("eventLocation")
        var mediaLocation = Location("mediaLocation")

        eventLocation.latitude = objectResult.eventDisplayBasicInfo!!.eventLat
        eventLocation.longitude = objectResult.eventDisplayBasicInfo!!.eventLong

        mediaLocation.latitude = media_lat.toDouble()
        mediaLocation.longitude = media_long.toDouble()
        val dist = eventLocation.distanceTo(mediaLocation)

        return dist < 150
    }

    override fun onDestroy() {
        super.onDestroy()
        HooleyApp.db.putInt(Constants.SHOW_LIVE_EVENT_TAB, 2)
        fragmentLiveEventbinding = null
        try {
            if (objectResult.isLive && objectResult.success)
                addBubbleInList()
        } catch (e: IndexOutOfBoundsException) {
        }
    }

    private fun addBubbleInList() {
        if (showBubble) {
            mBubbleList = (HooleyMain.activity as HooleyMain).bubbleList
            if (mBubbleList != null && mBubbleList!!.size > 0) {
                for (i in mBubbleList!!.indices) {
                    if (mBubbleList!![i].eventId.equals(HooleyApp.db.getString(Constants.CHAT_HEAD_EVENT_ID), ignoreCase = true)) {
                        mBubbleList!!.removeAt(i)
                    }
                }
                //                if (isAdded) {
                if (mBubbleList!!.size > 5)
                    mBubbleList!!.removeAt(0)
                mBubbleList!!.add(0, BubbleListItem(HooleyApp.db.getString(Constants.CHAT_HEAD_EVENT_ID), HooleyApp.db.getString(Constants.CHAT_HEAD_EVENT_AVATAR)))
                //                }
                if (mBubbleList!!.size > 1) {
                    HooleyApp.db.putBoolean(Constants.HAS_BUBBLE_HISTORY, true)
                }
                val bubbleListString = gson.toJson(mBubbleList)
                HooleyApp.db.putString(Constants.GSON_BUBBLE_LIST, bubbleListString)
            } else {
                mBubbleList = ArrayList()
                mBubbleList!!.add(BubbleListItem(HooleyApp.db.getString(Constants.CHAT_HEAD_EVENT_ID), HooleyApp.db.getString(Constants.CHAT_HEAD_EVENT_AVATAR)))
                val bubbleListString = gson.toJson(mBubbleList)
                HooleyApp.db.putString(Constants.GSON_BUBBLE_LIST, bubbleListString)
            }
            if (!(HooleyMain.activity as HooleyMain).isBubbleShowing) {
                HooleyApp.db.putBoolean(Constants.SHOW_BUBBLE, showBubble)
                HooleyApp.db.putBoolean(Constants.IS_BUBBLE_CLICKABLE, true)
                (HooleyMain.activity as HooleyMain).addNewChatHead()
            }
        }
    }

    override fun onBuyPPV() {
        if (HooleyApp.db.getBoolean(Constants.IS_WALLET_SETUP)) {
            hooleyWalletFragment = HooleyWalletCardsFragment(true, "Buy PPV")
            hooleyWalletFragment.setTargetFragment(instance, BUY_REQUEST_CODE)
            (HooleyMain.activity as ActivityBase).addFragment(R.id.container, hooleyWalletFragment, "HooleyWalletCardsFragment")
        } else {
            hooleyWallet = HooleyWalletFragment(false, true, "Buy PPV")
            hooleyWallet.setTargetFragment(instance, BUY_REQUEST_CODE)
            callFragment(R.id.container, hooleyWallet, "HooleyWalletFragment")
        }
    }

    companion object {

        private val CAMERA_RQ = 6969
        private val VIDEO_RQ = 6868
        private val IMAGE_PICKER_SELECT = 7979
        lateinit var instance: LiveEventFragment
        var eventId: String = ""
        var eventName = ""
        lateinit var objectResult: EventDetailModel
        var isPostAllow = false
        var fragmentLiveEventbinding: FragmentLiveEventBinding? = null
        private var filePath: String? = null
        private var itemPostMedia: ItemPostMedia? = null

        fun newInstance(id: String): LiveEventFragment {
            eventId = id
            instance = LiveEventFragment()
            return instance
        }
    }

}
