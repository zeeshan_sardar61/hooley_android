package com.hooleyapp.hooley.fragments.ticket

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentSuccessPaymentBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.profile.ProfileMainFragment
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.others.Constants

/**
 * Created by Zeeshan on 20-Mar-18.
 */

class SuccessPaymentFragment : BaseFragment(), View.OnClickListener {
    var binding: FragmentSuccessPaymentBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_success_payment, container, false)
        when (ticketType) {
            "charity" -> {
                binding!!.tvText.text = "Thank you for your donation!"
                binding!!.tvText1.text = "Receipt and donation details sent to"
                binding!!.animTicketPurchased.clearAnimation()
                binding!!.animTicketPurchased.setAnimation(HooleyMain.activity!!.resources.getIdentifier("ic_charity", "raw", HooleyMain.activity!!.packageName))
                binding!!.animTicketPurchased.playAnimation()
                binding!!.tvTicketsBottom.visibility = View.GONE
                binding!!.btnViewTicket.text = "Done"
            }
            "ppv" -> {
                binding!!.animTicketPurchased.clearAnimation()
                binding!!.tvText1.text = "Receipt and PPV details sent to"
                binding!!.animTicketPurchased.setAnimation(HooleyMain.activity!!.resources.getIdentifier("ic_mask", "raw", HooleyMain.activity!!.packageName))
                binding!!.animTicketPurchased.playAnimation()
                binding!!.tvTicketsBottom.text = (HooleyMain.activity as ActivityBase).noTrailingwhiteLines(Html.fromHtml("<p>Your pay-per-view event details can be found in the <b>\"Following\"</b> tab of the My Events page.</p>"))
                binding!!.btnViewTicket.text = "Done"
            }
            "general" -> {
                binding!!.animTicketPurchased.clearAnimation()
                binding!!.animTicketPurchased.setAnimation(HooleyMain.activity!!.resources.getIdentifier("ani_thank_you", "raw", HooleyMain.activity!!.packageName))
                binding!!.animTicketPurchased.playAnimation()
            }
        }
        binding!!.tvEmail.text = HooleyApp.db.getString(Constants.EMAIL_ID)
        setListener()
        return binding!!.root
    }

    private fun setListener() {
        binding!!.btnViewTicket.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnViewTicket -> {
                if (binding!!.btnViewTicket.text.toString() != "Done") {
                    callFragment(R.id.container, ProfileMainFragment.newInstance(true), "ProfileMainFragment")
                    HooleyApp.db.putBoolean(Constants.IS_FRIEND_CLICK, false)
                    hideDrawer(HooleyMain.activity!!)

                } else {
                    HooleyMain.activity!!.onBackPressed()
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
        HooleyMain.activity!!.supportActionBar!!.show()
    }

    companion object {
        lateinit var instance: SuccessPaymentFragment
        private var ticketId: String? = null
        private var ticketType: String? = null

        fun newInstance(id: String, ticketType_: String): SuccessPaymentFragment {
            ticketId = id
            ticketType = ticketType_
            instance = SuccessPaymentFragment()
            return instance
        }
    }
}
