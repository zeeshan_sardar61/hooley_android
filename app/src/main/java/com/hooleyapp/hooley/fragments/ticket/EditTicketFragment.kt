package com.hooleyapp.hooley.fragments.ticket

import android.annotation.SuppressLint
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.event.requestmodel.TicketListItem
import com.hooleyapp.hooley.app.data.model.event.responseModel.GetEditTicketResponse
import com.hooleyapp.hooley.app.data.model.event.responseModel.TicketDetail
import com.hooleyapp.hooley.databinding.FragmentEditTicketBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.TicketWebService

@SuppressLint("ValidFragment")
class EditTicketFragment constructor(var eventId: String) : BaseFragment() {

    private lateinit var objectResponse: GetEditTicketResponse
    lateinit var binding: FragmentEditTicketBinding
    var ticketWebService = TicketWebService()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_ticket, container, false)
        getEditTicket()
        return binding.root
    }

    private fun setListener() {
        binding.rlAddCategory.setOnClickListener {
            addChild(binding.llTicketsCategory)
        }
        binding.btnSaveTicket.setOnClickListener {
            if (validateInputs()) {
                saveEditTicket()
            }
        }
        binding.tbDisablePPV.setOnClickListener {
            disablePPV()
        }
    }

    private fun disablePPV() {
        binding.pbTickets.visibility = View.VISIBLE
        ticketWebService.enableDisablePPV(eventId, !binding.tbDisablePPV.isChecked, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                binding.pbTickets.visibility = View.GONE
//                userSearchbinding.tbDisablePPV.isChecked = !userSearchbinding.tbDisablePPV.isChecked
            }

            override fun failure(message: String) {
                binding.pbTickets.visibility = View.GONE
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (e: Exception) {
                }
            }
        })
    }

    private fun validateInputs(): Boolean {
        var isValid = true
        for (i in 0 until binding.llTickets.childCount) {
            if ((binding.llTickets.getChildAt(i).findViewById<View>(R.id.etQuantity) as EditText).text!!.toString().isNotEmpty()) {
                if ((binding.llTickets.getChildAt(i).findViewById<View>(R.id.etQuantity) as EditText).text!!.toString().toInt() < objectResponse.ticketDetail!![i]?.sold!!) {
                    (binding.llTickets.getChildAt(i).findViewById<View>(R.id.etQuantity) as EditText).requestFocus()
                    (binding.llTickets.getChildAt(i).findViewById<View>(R.id.etQuantity) as EditText).error = "Quantity Exceeded"
                    isValid = false
                    break
                }
            }
        }
        return isValid
    }

    private fun saveEditTicket() {
        var ppvPrice = ""
        if (binding.edtPpv.text.toString().isNotEmpty())
            ppvPrice = binding.edtPpv.text.toString()
        else
            ppvPrice = objectResponse.ppvTicket?.price?.toString()!!

        var mlist = arrayListOf<TicketListItem?>()
        for (i in 0 until binding.llTickets.childCount) {
            if ((binding.llTickets.getChildAt(i).findViewById<View>(R.id.etQuantity) as EditText).text!!.toString().isNotEmpty()) {
                var obj = TicketListItem((binding.llTickets.getChildAt(i).findViewById<View>(R.id.etQuantity) as EditText).text!!.toString().toInt(), objectResponse.ticketDetail!![i]?.price, objectResponse.ticketDetail!![i]?.ticketId, objectResponse.ticketDetail!![i]?.ticketName)
                mlist.add(obj)
            }
        }
        for (i in 0 until binding.llTicketsCategory.childCount) {
            var obj = TicketListItem((binding.llTicketsCategory.getChildAt(i).findViewById<View>(R.id.edtQuantity) as EditText).text!!.toString().toInt(),
                    (binding.llTicketsCategory.getChildAt(i).findViewById<View>(R.id.edtPrice) as EditText).text!!.toString().toInt(),
                    0, (binding.llTicketsCategory.getChildAt(i).findViewById<View>(R.id.edtTicketName) as EditText).text!!.toString())
            mlist.add(obj)
        }
        binding.pbTickets.visibility = View.VISIBLE

        ticketWebService.saveEditTicket(eventId, mlist, ppvPrice, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                binding.pbTickets.visibility = View.GONE
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
            }

            override fun failure(message: String) {
                binding.pbTickets.visibility = View.GONE
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (e: Exception) {
                }
            }
        })
    }

    private fun getEditTicket() {
        binding.pbTickets.visibility = View.VISIBLE
        ticketWebService.getEditTicket(eventId, object : GenericCallback<GetEditTicketResponse> {
            override fun success(result: GetEditTicketResponse) {
                binding.pbTickets.visibility = View.GONE
                objectResponse = result
                setData()
                setListener()
            }

            override fun failure(message: String) {
                binding.pbTickets.visibility = View.GONE
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (e: Exception) {
                }
            }
        })
    }


    private fun setData() {
        binding.btnSaveTicket.visibility = View.VISIBLE
        if (objectResponse.ppvTicket == null) {
            binding.llPPV.visibility = View.GONE
        } else {
            binding.llPPV.visibility = View.VISIBLE
            binding.tbDisablePPV.isChecked = !objectResponse.ppvTicket?.isEnable!!
            binding.tvPpvPrice.text = objectResponse.ppvTicket?.price!!.toString()
            binding.tvSoldPPV.text = objectResponse.ppvTicket?.sold!!.toString()
        }
        if (objectResponse.donationTarget == null) {
            binding.rlCharityTarget.visibility = View.GONE
        } else {
            binding.rlCharityTarget.visibility = View.VISIBLE
            binding.tvRemoteDonationAmount.text = objectResponse.donationTarget!!.toString()
        }
        if (objectResponse.ticketDetail != null && objectResponse.ticketDetail?.size!! > 0) {
            binding.rlTicketCategory.visibility = View.VISIBLE
            for (i in 0 until objectResponse.ticketDetail?.size!!) {
                addTicketCategoryChild(objectResponse.ticketDetail!![i])
            }
        }
    }

    private fun addTicketCategoryChild(ticketDetail: TicketDetail?) {
        val child = HooleyMain.activity!!.layoutInflater.inflate(R.layout.adapter_charity_ticket, null)
        (child.findViewById<View>(R.id.tvTicketCategory) as TextView).text = ticketDetail!!.ticketName
        (child.findViewById<View>(R.id.tvTicketPrice) as TextView).text = ticketDetail.price!!.toString()
        (child.findViewById<View>(R.id.tvLeftTickets) as TextView).text = ticketDetail.available!!.toString()
        (child.findViewById<View>(R.id.tvSoldTickets) as TextView).text = ticketDetail.sold!!.toString()
        binding.llTickets.addView(child)
    }

    private fun addChild(layout: LinearLayout) {
        val count = layout.childCount
        if (count > 0) {
            val prevChild = layout.getChildAt(count - 1)
            if (validate(prevChild)) {
                val child = HooleyMain.activity!!.layoutInflater.inflate(R.layout.ticket_category_child_item, null)
                child.findViewById<View>(R.id.ivClose).setOnClickListener {
                    layout.removeView(child)
                    if (layout.childCount == 0) {
                        showRemove(false)
                    }
                }
                layout.addView(child)
            }
        } else {
            val child = HooleyMain.activity!!.layoutInflater.inflate(R.layout.ticket_category_child_item, null)
            child.findViewById<View>(R.id.ivClose).setOnClickListener {
                layout.removeView(child)
                if (layout.childCount == 0) {
                    showRemove(false)
                }
            }
            layout.addView(child)
        }

    }

    private fun showRemove(check: Boolean) {
        val childCount = binding.llTicketsCategory.childCount
        for (i in 0 until childCount) {
            val child = binding.llTicketsCategory.getChildAt(i)
            if (check) {
                if (validate(child)) {
                    child.findViewById<View>(R.id.ivClose).visibility = View.VISIBLE
                    child.findViewById<View>(R.id.edtTicketName).isEnabled = false
                    child.findViewById<View>(R.id.edtPrice).isEnabled = false
                    if (HooleyApp.db.getInt(Constants.TYPE_THEME) == 0) {
                        child.findViewById<View>(R.id.edtTicketName).background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_edt_transparent)
                        child.findViewById<View>(R.id.edtQuantity).background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_edt_transparent)
                        child.findViewById<View>(R.id.edtPrice).background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_edt_transparent)
                    }
                    child.findViewById<View>(R.id.edtQuantity).isEnabled = false
                    child.findViewById<View>(R.id.edtTicketName).alpha = .5f
                    child.findViewById<View>(R.id.edtQuantity).alpha = .5f
                    child.findViewById<View>(R.id.edtPrice).alpha = .5f
                }
            } else {
                child.findViewById<View>(R.id.ivClose).visibility = View.INVISIBLE
                child.findViewById<View>(R.id.edtTicketName).isEnabled = true
                child.findViewById<View>(R.id.edtQuantity).isEnabled = true
                child.findViewById<View>(R.id.edtTicketName).alpha = 1f
                child.findViewById<View>(R.id.edtTicketName).background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_save_transparent)
                child.findViewById<View>(R.id.edtQuantity).background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_save_transparent)
                child.findViewById<View>(R.id.edtPrice).background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_save_transparent)
                child.findViewById<View>(R.id.edtQuantity).alpha = 1f
                child.findViewById<View>(R.id.edtPrice).alpha = 1f
            }
        }
    }

    private fun validate(v: View): Boolean {
        return when {
            Util.isContentNull((v.findViewById<View>(R.id.edtTicketName) as EditText).text.toString()) -> {
                v.findViewById<View>(R.id.edtTicketName).requestFocus()
                (v.findViewById<View>(R.id.edtTicketName) as EditText).error = getString(R.string.cat_name)
                false
            }
            Util.isContentNull((v.findViewById<View>(R.id.edtQuantity) as EditText).text.toString()) -> {
                v.findViewById<View>(R.id.edtQuantity).requestFocus()
                (v.findViewById<View>(R.id.edtQuantity) as EditText).error = getString(R.string.quantity)
                false
            }
            Util.isContentNull((v.findViewById<View>(R.id.edtPrice) as EditText).text.toString()) -> {
                v.findViewById<View>(R.id.edtPrice).requestFocus()
                (v.findViewById<View>(R.id.edtPrice) as EditText).error = getString(R.string.price)
                false
            }
            else -> true
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
    }

}