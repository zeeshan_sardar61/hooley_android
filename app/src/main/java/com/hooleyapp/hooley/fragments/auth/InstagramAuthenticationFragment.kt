package com.hooleyapp.hooley.fragments.auth

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentInstagramAuthenticationBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.log.Logger

/**
 * Created by Zeeshan on 13-Mar-18.
 */

class InstagramAuthenticationFragment : BaseFragment() {
    lateinit var intent: Intent
    lateinit var binding: FragmentInstagramAuthenticationBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_instagram_authentication, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setToolbar()
        setWebView()

    }

    fun setWebView() {

        binding.wvInsta.settings.javaScriptEnabled = true
        binding.wvInsta.settings.loadWithOverviewMode = true
        binding.wvInsta.settings.useWideViewPort = true
        binding.wvInsta.settings.builtInZoomControls = true
        binding.wvInsta.loadUrl(authUrl + HooleyMain.activity!!.getString(R.string.insta_client_id) + "&redirect_uri=" + PROTECTED_RESOURCE_URL + "&response_type=token")
        binding.wvInsta.webViewClient = object : WebViewClient() {

            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                if (url.indexOf("access_token=") > -1) {
                    Logger.e("hi" + " / access_token = " + url.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1])
                    intent = Intent()
                    intent.putExtra("access_token", url.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1])
                    HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                    targetFragment!!.onActivityResult(
                            targetRequestCode,
                            Activity.RESULT_OK,
                            intent
                    )
                }
            }

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                if (url.contains(PROTECTED_RESOURCE_URL) && !url.contains("code=")) {
                    HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                } else if (!url.contains("code=")) {
                    Logger.e("killProgressDialog")
                }
            }
        }
    }

    private fun setToolbar() {
        HooleyMain.activity!!.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        HooleyMain.activity!!.supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
    }

    companion object {

        lateinit var instance: InstagramAuthenticationFragment
        private val authUrl = "https://api.instagram.com/oauth/authorize/?client_id="
        private val PROTECTED_RESOURCE_URL = "https://www.messagemuse.com/"

        fun newInstance(): InstagramAuthenticationFragment {
            instance = InstagramAuthenticationFragment()
            return instance
        }
    }
}
