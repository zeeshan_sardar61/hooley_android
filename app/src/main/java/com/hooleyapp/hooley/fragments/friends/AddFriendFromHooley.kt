package com.hooleyapp.hooley.fragments.friends

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterAddFriendHooley
import com.hooleyapp.hooley.databinding.FragmentAddFriendHooleyBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.helper.checkIfName
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IFriendContactClickListener
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.friends.GetFriendOfFriendModel
import com.hooleyapp.hooley.model.friends.HooleyUserModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*
import kotlin.collections.ArrayList

class AddFriendFromHooley : BaseFragment(), TextWatcher, IFriendContactClickListener, View.OnClickListener, ViewAvatarFragment.CardCallBackListener {

    lateinit var binding: FragmentAddFriendHooleyBinding
    private lateinit var objectResponse: HooleyUserModel
    var friendService = FriendWebService()
    var mList: ArrayList<FriendsTable> = ArrayList()
    private lateinit var adapter: AdapterAddFriendHooley
    private lateinit var objectResponseSuggestion: GetFriendOfFriendModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_friend_hooley, container, false)
        setListener()
        getFriendsOfFriends()
        return binding.root
    }


//    private fun sortByAlphabetical() {
//                Collections.sort(mList!!, FriendsTable.sortByNameComparator)
//                adapterMyFriends!!.notifyDataSetChanged()
//    }


    private fun getFriendsOfFriends() {
        binding.pbMyFriends.visibility = View.VISIBLE
        friendService.getFriendsOfFriends(object : GenericCallback<GetFriendOfFriendModel> {
            override fun success(result: GetFriendOfFriendModel) {
                binding.pbMyFriends.visibility = View.GONE
                objectResponseSuggestion = result
                if (mList.size > 0)
                    mList.clear()
                mList.addAll(objectResponseSuggestion.guestList)
                Collections.sort(mList, FriendsTable.sortByNameComparator)
                setAdapter()
            }

            override fun failure(message: String) {
                binding.pbMyFriends.visibility = View.GONE
                setAdapter()
            }
        })
    }

    private fun setListener() {
        binding.ivClose.setOnClickListener(this)
        binding.edtSearch.addTextChangedListener(this)

    }


    private fun setAdapter() {
        if (mList.size > 0)
            binding.llSuggestion.visibility = View.VISIBLE
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        binding.rvHooleyUsers.layoutManager = manager
        binding.rvHooleyUsers.layoutAnimation = animation
        binding.rvHooleyUsers.setEmptyView(binding.tvNoData)
        adapter = AdapterAddFriendHooley(mList)
        adapter.mListener = this
        binding.rvHooleyUsers.adapter = adapter

    }

    override fun reloadFragment(flag: Boolean) {
        if (flag)
            getFriendsOfFriends()
    }

    override fun afterTextChanged(s: Editable?) {
        if (!HooleyMain.activity!!.checkIfName(s.toString())) {
            if (binding.edtSearch.text!!.isEmpty()) {
                try {
                    if (mList.size > 0)
                        mList.clear()
                    if (::objectResponseSuggestion.isInitialized)
                        mList.addAll(objectResponseSuggestion.guestList)
                    if (mList.size > 0)
                        binding.llSuggestion.visibility = View.VISIBLE
                    if (mList.size > 0) {
                        binding.tvNoData.visibility = View.GONE
                        binding.rvHooleyUsers.visibility = View.VISIBLE
                    }
                    adapter.notifyDataSetChanged()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        } else {
            searchFriend(s.toString())
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivClose -> {
                binding.edtSearch.setText("")
                try {
                    if (mList.size > 0)
                        mList.clear()
                    if (::objectResponseSuggestion.isInitialized)
                        mList.addAll(objectResponseSuggestion.guestList)
                    if (mList.size > 0)
                        binding.llSuggestion.visibility = View.VISIBLE
                    adapter.notifyDataSetChanged()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun searchFriend(searchString: String) {
        binding.pbMyFriends.visibility = View.VISIBLE
        friendService.getMoreInvitePeople(searchString, object : GenericCallback<HooleyUserModel> {
            override fun success(result: HooleyUserModel) {
                binding.pbMyFriends.visibility = View.GONE
                objectResponse = result
                updateList()
            }

            override fun failure(message: String) {
                binding.pbMyFriends.visibility = View.GONE
                mList.clear()
                binding.tvNoData.text = message
                adapter.notifyDataSetChanged()
            }
        })
    }

    private fun updateList() {
        if (binding.edtSearch.text!!.isBlank()) {
//            if (mList.size > 0)
//                mList.clear()
//            if (::objectResponseSuggestion.isInitialized)
//                mList.addAll(objectResponseSuggestion.guestList)
//            userSearchbinding.llSuggestion.visibility = View.VISIBLE
            return
        }
        if (mList.size > 0)
            mList.clear()
        mList.addAll(objectResponse.inviteList)
        if (mList.size > 0)
            binding.llSuggestion.visibility = View.GONE

        if (adapter != null)
            adapter.notifyDataSetChanged()

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun onClickAddFriend(position: Int) {
        mList[position].status = Constants.TYPE_PENDING_SENT
        adapter.notifyDataSetChanged()
        sendFriendRequest(position)
    }

    override fun onClickAcceptFriend(position: Int) {
        acceptFriendRequest(position)
    }

    private fun sendFriendRequest(position: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        val friendList = java.util.ArrayList<Int>()
        friendList.add(mList[position].userId.toInt())
        friendService.sendFriendRequest(friendList, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                Util.showToastMessage(HooleyMain.activity!!, "Friend Request Sent")
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun acceptFriendRequest(position: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        friendService.acceptIgnoreFriendRequest(mList[position].userId.toInt(), true, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                mList[position].status = Constants.TYPE_FRIEND
                if (adapter != null)
                    adapter.notifyDataSetChanged()
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.showSuccessDialog(message)
            }
        })

    }

    override fun onClickItem(position: Int) {
        Util.hideKeyboard(HooleyMain.activity!!)
        var avatarfragment = ViewAvatarFragment(mList[position].userId.toInt())
        avatarfragment.mListener = this
        avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)

    }


    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding.unbind()
    }

}