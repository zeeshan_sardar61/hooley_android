package com.hooleyapp.hooley.fragments.auth

import android.animation.Animator
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.FragmentConfirmForgotPassBinding
import com.hooleyapp.hooley.fragments.BaseFragment

/**
 * Created by Nauman on 3/1/2018.
 */

class ConfirmForgotPasswordFragment : BaseFragment(), View.OnClickListener, Animator.AnimatorListener {

    var binding: FragmentConfirmForgotPassBinding? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_confirm_forgot_pass, container, false)

        setListener()
        var value = "Check your email <b>$userEmail</b> to reset your password and continue to Sign In<br><br>Don't forget to check your spam folder too!"
        binding!!.tvText.text = Html.fromHtml(value)
        return binding!!.root
    }

    private fun setListener() {
        binding!!.btnLogin.setOnClickListener(this)
        binding!!.lavConfirmSignUp.addAnimatorListener(this)

    }


    override fun onDestroy() {
        super.onDestroy()
        if (binding != null) {
            binding!!.unbind()
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnLogin -> activity!!.supportFragmentManager.popBackStackImmediate()
        }
    }

    override fun onAnimationStart(animator: Animator) {

    }

    override fun onAnimationEnd(animator: Animator) {
        binding!!.lavConfirmSignUp.visibility = View.VISIBLE
    }

    override fun onAnimationCancel(animator: Animator) {

    }

    override fun onAnimationRepeat(animator: Animator) {

    }

    companion object {

        lateinit var insance: ConfirmForgotPasswordFragment
        private var userEmail = "x"

        fun newInstance(email: String): ConfirmForgotPasswordFragment {
            userEmail = email
            insance = ConfirmForgotPasswordFragment()
            return insance
        }
    }
}
