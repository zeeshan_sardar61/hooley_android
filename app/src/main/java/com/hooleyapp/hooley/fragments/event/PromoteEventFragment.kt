package com.hooleyapp.hooley.fragments.event

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.design.widget.BottomSheetDialog
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.appyvet.materialrangebar.RangeBar
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterStateCity
import com.hooleyapp.hooley.databinding.FragmentPromoteEventBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.others.HooleyWalletCardsFragment
import com.hooleyapp.hooley.fragments.others.HooleyWalletFragment
import com.hooleyapp.hooley.helper.CustomDateTimePicker
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GetCityStateModel
import com.hooleyapp.hooley.model.GetEventPromotionModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.EventWebService
import com.stripe.android.Stripe
import com.stripe.android.TokenCallback
import com.stripe.android.model.Token
import com.stripe.android.view.CardInputWidget
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by Nauman on 5/2/2018.
 */

class PromoteEventFragment : BaseFragment(), View.OnClickListener, RangeBar.OnRangeBarChangeListener,
//        OnRangeSeekbarChangeListener,
        SeekBar.OnSeekBarChangeListener, AdapterStateCity.IStateCityClickListener, CompoundButton.OnCheckedChangeListener {

    var binding: FragmentPromoteEventBinding? = null
    private var customDialog: CustomDateTimePicker? = null
    private var previousSeletedDate: Date? = null
    private var objectResponseStateCity: GetCityStateModel? = null
    private var stateCityDialog: Dialog? = null
    private var elvStateCity: ExpandableListView? = null
    private var adapterCityState: AdapterStateCity? = null
    private var btnAddStateCity: TextView? = null
    internal var value = 0
    private var objectResponseEventData: GetEventPromotionModel? = null
    private var promoteAudiencesList: ArrayList<GetEventPromotionModel.PromoteAudiences>? = ArrayList()
    private var stripeTokken: String? = null   //Will be send in Api Later
    private val webService = EventWebService()
    private val gson = Gson()
    private var stateCityModel: GetCityStateModel? = null
    var statesCitiesList: ArrayList<GetCityStateModel.StateCity>? = ArrayList()

    val isStateCitySelected: Boolean?
        get() {
            if (statesCitiesList == null || statesCitiesList!!.size == 0) {
                return false
            } else {
                for (i in statesCitiesList!!.indices) {
                    if (statesCitiesList!![i].isChecked) {
                        return true
                    }
                }
                return false
            }
        }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_promote_event, container, false)
        getPromotionData()
        getLocalStateCity()
        return binding!!.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
    }

    private fun setListener() {
        binding!!.btnAddCityState.setOnClickListener(this)
        binding!!.ivPromotionStart.setOnClickListener(this)
        binding!!.ivPromotionEnd.setOnClickListener(this)
        binding!!.rangedSeekbar.setOnRangeBarChangeListener(this)
        binding!!.sbAmount.setOnSeekBarChangeListener(this)
        binding!!.cbFemale.setOnCheckedChangeListener(this)
        binding!!.cbMale.setOnCheckedChangeListener(this)
        binding!!.cbTransgender.setOnCheckedChangeListener(this)
        binding!!.btnPromote.setOnClickListener(this)
        binding!!.ivMinus.setOnClickListener(this)
        binding!!.ivPlus.setOnClickListener(this)
        binding!!.ivLeft.setOnClickListener(this)
        binding!!.ivRight.setOnClickListener(this)
    }

    private fun showCustomDialog(view: TextView) {
        customDialog = CustomDateTimePicker(HooleyMain.activity!!,
                object : CustomDateTimePicker.ICustomDateTimeListener {
                    override fun onSet(dialog: Dialog, calendarSelected: Calendar,
                                       dateSelected: Date, year: Int, monthFullName: String,
                                       monthShortName: String, monthNumber: Int, date: Int,
                                       weekDayFullName: String, weekDayShortName: String,
                                       hour24: Int, hour12: Int, min: Int, sec: Int,
                                       AM_PM: String) {
                        if (view.id == binding!!.tvPromotionEndDate.id) {
                            if (previousSeletedDate != null) {
                                if (dateSelected.before(previousSeletedDate))
                                    if (TimeUnit.MILLISECONDS.toHours(dateSelected.time - previousSeletedDate!!.time) < 1) {
                                        /*     if (DateUtils.hourDifference(dateSelected , previousSeletedDate) < 1) {*/
                                        //                                        Util.showToastMessage(HooleyMain.activity, getString(R.string.str_invalid_select_date));
                                        Util.showToastMessage(HooleyMain.activity!!, "Invalid date, date is greater than event start date")
                                        return
                                    } /*else if (TimeUnit.MILLISECONDS.toHours((dateSelected.getTime() - previousSeletedDate.getTime())) > 24) {
                                    Util.showToastMessage(HooleyMain.activity, "End date more then on day");
                                    return;
                                }*/
                                if (dateSelected.before(Calendar.getInstance().time)) {
                                    Util.showToastMessage(HooleyMain.activity!!, "Invalid date, Please select future date.")
                                    return
                                }
                                if (dateSelected.after(DateUtils.stringToDate(DateUtils.getLocalDate(eventEndDate!!)))) {
                                    Util.showToastMessage(HooleyMain.activity!!, "Invalid date, date is greater than event end date")
                                    return
                                } else {
                                    if (TimeUnit.MILLISECONDS.toHours(dateSelected.time - DateUtils.stringToDate(DateUtils.getLocalDate(eventEndDate!!))!!.time) == 0L) {
                                        Util.showToastMessage(HooleyMain.activity!!, "Invalid date, less than one hour")
                                        return
                                    }
                                }
                            } else {
                                Util.showToastMessage(HooleyMain.activity!!, "Please select start date")
                                return
                            }
                        } else {
                            if (dateSelected.before(Calendar.getInstance().time)) {
                                Util.showToastMessage(HooleyMain.activity!!, "Invalid date, Please select future date.")
                                return
                            }
                            if (dateSelected.after(DateUtils.stringToDate(eventEndDate!!))) {
                                Util.showToastMessage(HooleyMain.activity!!, "Invalid date, date is greater than event end date")
                                return
                            }
                            previousSeletedDate = dateSelected
                        }
                        view.text = ""
                        view.text = (String.format("%04d-%02d-%02d", year, monthNumber + 1, calendarSelected.get(Calendar.DAY_OF_MONTH))
                                + " " + String.format("%02d:%02d", hour12, min) +
                                " " + AM_PM)

                    }

                    override fun onCancel() {

                    }
                })
        /**
         * Pass Directly current time format it will return AM and PM if you set
         * false
         */
        customDialog!!.set24HourFormat(false)
        /**
         * Pass Directly current data and time to show when it pop up
         */
        if (TextUtils.isEmpty(view.text.toString()))
            customDialog!!.setDate(Calendar.getInstance())
        else {
            customDialog!!.setDate(DateUtils.stringToDate(DateUtils.SendDateToDate(view.text.toString())))
        }
        customDialog!!.showDialog()
    }

    private fun getLocalStateCity() {
        stateCityModel = gson.fromJson(HooleyApp.db.getString(Constants.RESPONSE_GSON_STATE_CITY), GetCityStateModel::class.java)
        if (stateCityModel != null) {
            if (stateCityModel!!.statesWithCitiesList.size > 0) {
                statesCitiesList!!.addAll(stateCityModel!!.statesWithCitiesList)
            }
        } else {
            val country = Util.loadJSONFromAsset(HooleyMain.activity!!)
            HooleyApp.db.putString(Constants.RESPONSE_GSON_STATE_CITY, country!!)
            stateCityModel = gson.fromJson(country, GetCityStateModel::class.java)
            if (stateCityModel != null)
                if (stateCityModel!!.statesWithCitiesList.size > 0) {
                    statesCitiesList!!.addAll(stateCityModel!!.statesWithCitiesList)
                }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    private lateinit var hooleyWalletFragment: HooleyWalletCardsFragment
    private lateinit var hooleyWallet: HooleyWalletFragment
    private var BUY_REQUEST_CODE: Int = 3000

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnAddCityState -> if (stateCityModel != null)
                showStateCityDialog()
            else if (statesCitiesList != null && statesCitiesList!!.size > 0)
                showStateCityDialog()
            else
                Util.showToastMessage(HooleyMain.activity!!, "Please Wait cities are not yet loaded from server")
            R.id.ivPromotionStart -> showCustomDialog(binding!!.tvPromotionStartDate)
            R.id.ivPromotionEnd -> showCustomDialog(binding!!.tvPromotionEndDate)
            R.id.btnPromote -> if (validateInput()) {
                //                    promoteEvent();
//                showBottomSheetDialog()
                if (HooleyApp.db.getBoolean(Constants.IS_WALLET_SETUP)) {
                    hooleyWalletFragment = HooleyWalletCardsFragment(true, "Promote Event")
                    hooleyWalletFragment.setTargetFragment(PromoteEventFragment.instance, BUY_REQUEST_CODE)
                    (HooleyMain.activity as ActivityBase).addFragment(R.id.container, hooleyWalletFragment, "HooleyWalletCardsFragment")
                } else {
                    hooleyWallet = HooleyWalletFragment(false, true, "Promote Event")
                    hooleyWallet.setTargetFragment(PromoteEventFragment.instance, BUY_REQUEST_CODE)
                    callFragment(R.id.container, hooleyWallet, "HooleyWalletFragment")
                }
            }
            R.id.ivPlus -> if (binding!!.sbAmount.progress < Integer.parseInt(objectResponseEventData!!.minMaxRange!!.maxAmount)) {
                binding!!.sbAmount.progress = binding!!.sbAmount.progress + 5
            }
            R.id.ivMinus -> if (binding!!.sbAmount.progress > Integer.parseInt(objectResponseEventData!!.minMaxRange!!.minAmount)) {
                binding!!.sbAmount.progress = binding!!.sbAmount.progress - 5
            }
            R.id.ivRight -> {
//                if (userSearchbinding!!.rangedSeekbar.selectedMinValue.toInt() > Integer.parseInt(objectResponseEventData!!.minMaxRange!!.minAge)) {
//                    userSearchbinding!!.rangedSeekbar.setMinValue(userSearchbinding!!.tvStartProgress.text.toString().toFloat() - 1)
//                    userSearchbinding!!.rangedSeekbar.apply()
//                }
            }
            R.id.ivLeft -> {
//                if (userSearchbinding!!.rangedSeekbar.selectedMaxValue.toInt() < Integer.parseInt(objectResponseEventData!!.minMaxRange!!.maxAge)) {
//                    userSearchbinding!!.rangedSeekbar.setMaxValue(userSearchbinding!!.tvEndProgress.text.toString().toFloat() + 1)
//                    userSearchbinding!!.rangedSeekbar.apply()
//                }
            }
        }


    }

    private fun validateInput(): Boolean {
        if (Util.isContentNull(binding!!.tvPromotionStartDate.text.toString())) {
            binding!!.tvPromotionStartDate.requestFocus()
            binding!!.tvPromotionStartDate.error = getString(R.string.required)
            return false
        } else {
            binding!!.tvPromotionStartDate.error = null
            binding!!.tvPromotionStartDate.clearFocus()
        }
        if (Util.isContentNull(binding!!.tvPromotionEndDate.text.toString())) {
            binding!!.tvPromotionEndDate.requestFocus()
            binding!!.tvPromotionEndDate.error = getString(R.string.required)
            return false
        } else if (binding!!.sbAmount.progress == 0) {
            Util.showToastMessage(HooleyMain.activity!!, "Select Amount please")
            return false
        } else if (getPromoteAudiencesList() == null || getPromoteAudiencesList()!!.size == 0) {
            Util.showToastMessage(HooleyMain.activity!!, "Select Audience please")
            return false
        } else if (!isStateCitySelected!!) {
            Util.showToastMessage(HooleyMain.activity!!, "Select State please")
            return false
        } else {
            return true
        }
    }

    private fun promoteEvent() {
        HooleyMain.activity!!.showDialog()
        webService.promoteEvent(eventId!!, binding!!.sbAmount.progress, binding!!.tvPromotionStartDate.text.toString(), binding!!.tvPromotionEndDate.text.toString(), binding!!.tvStartProgress.text.toString(), binding!!.tvEndProgress.text.toString(), getPromoteAudiencesList()!!, statesCitiesList!!, stripeTokken!!, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                HooleyMain.activity!!.showSuccessDialog("Event Promoted")
                HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })

    }

    private fun getPromoteAudiencesList(): ArrayList<GetEventPromotionModel.PromoteAudiences>? {
        if (promoteAudiencesList == null)
            promoteAudiencesList = ArrayList<GetEventPromotionModel.PromoteAudiences>()
        if (promoteAudiencesList!!.size > 0)
            promoteAudiencesList!!.clear()
        if (binding!!.cbMale.isChecked) {
            promoteAudiencesList!!.add(objectResponseEventData!!.mlist[0])
        }
        if (binding!!.cbFemale.isChecked) {
            promoteAudiencesList!!.add(objectResponseEventData!!.mlist[1])
        }
        if (binding!!.cbTransgender.isChecked) {
            promoteAudiencesList!!.add(objectResponseEventData!!.mlist[2])
        }
        return promoteAudiencesList
    }

//    override fun valueChanged(minValue: Number, maxValue: Number) {
//        binding!!.tvStartProgress.text = minValue.toString()
//        binding!!.tvEndProgress.text = maxValue.toString()
//    }

    override fun onRangeChangeListener(rangeBar: RangeBar?, leftPinIndex: Int, rightPinIndex: Int, leftPinValue: String?, rightPinValue: String?) {
        binding!!.tvStartProgress.text = leftPinValue!!
        binding!!.tvEndProgress.text = rightPinValue!!
    }

    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
        binding!!.tvTotalAmount.text = "$$progress"
        binding!!.tvSbProgress.text = "$$progress"
    }

    override fun onStartTrackingTouch(seekBar: SeekBar) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar) {

    }

    fun getStateCity() {
        webService.getUKStateCities(object : IWebServiceCallback<GetCityStateModel> {
            override fun success(result: GetCityStateModel) {
                objectResponseStateCity = result
                val strCat = gson.toJson(objectResponseStateCity)
                HooleyApp.db.putString(Constants.RESPONSE_GSON_STATE_CITY, strCat)
                if (statesCitiesList!!.size > 0)
                    statesCitiesList!!.clear()
                statesCitiesList!!.addAll(objectResponseStateCity!!.statesWithCitiesList)
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })

    }

    private fun getPromotionData() {
        HooleyMain.activity!!.showDialog()
        webService.getPromotionData(eventId!!, object : IWebServiceCallback<GetEventPromotionModel> {
            override fun success(result: GetEventPromotionModel) {
                HooleyMain.activity!!.removeDialog()
                objectResponseEventData = result
                setData()
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })

    }

    private fun setData() {
        binding!!.tvMinAge.text = objectResponseEventData!!.minMaxRange!!.minAge!! + " Years"
        binding!!.tvMaxAge.text = objectResponseEventData!!.minMaxRange!!.maxAge.toString() + " Years"
        binding!!.sbAmount.max = Integer.parseInt(objectResponseEventData!!.minMaxRange!!.maxAmount)
//        binding!!.rangedSeekbar.rightPinValue(objectResponseEventData!!.minMaxRange!!.maxAge!!)
//        binding!!.rangedSeekbar.leftPinValue(objectResponseEventData!!.minMaxRange!!.minAge!!)
        binding!!.rangedSeekbar.setRangePinsByValue(objectResponseEventData!!.minMaxRange!!.minAge!!.toFloat(), objectResponseEventData!!.minMaxRange!!.maxAge!!.toFloat())
        binding!!.tvEndProgress.text = objectResponseEventData!!.minMaxRange!!.maxAge.toString()
//        binding!!.rangedSeekbar.apply()
    }

    private fun showStateCityDialog() {
        stateCityDialog = Dialog(HooleyMain.activity!!, R.style.Theme_Dialog)
        stateCityDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        stateCityDialog!!.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        stateCityDialog!!.setContentView(R.layout.dialog_satate_city)
        elvStateCity = stateCityDialog!!.findViewById(R.id.elvStateCity)
        btnAddStateCity = stateCityDialog!!.findViewById(R.id.btnAddStateCity)
        elvStateCity!!.setGroupIndicator(null)
        elvStateCity!!.isClickable = true
        adapterCityState = AdapterStateCity(elvStateCity!!, HooleyMain.activity!!, statesCitiesList!!)
        adapterCityState!!.setListener(this)
        elvStateCity!!.setAdapter(adapterCityState)
        btnAddStateCity!!.setOnClickListener {
            stateCityDialog!!.dismiss()
            setStateView()
        }
        stateCityDialog!!.show()
    }

    private fun setStateView() {
        binding!!.llStateCity.removeAllViews()
        for (i in statesCitiesList!!.indices) {
            if (statesCitiesList!![i].isChecked) {
                val child = HooleyMain.activity!!.layoutInflater.inflate(R.layout.layout_state_city, null)
                (child.findViewById<View>(R.id.tvState) as TextView).text = statesCitiesList!![i].name
                var cityCount = 0
                for (j in statesCitiesList!![i].citiesList.indices) {
                    if (statesCitiesList!![i].citiesList[j].isChecked)
                        cityCount++
                }
                (child.findViewById<View>(R.id.tvCityCount) as TextView).text = "$cityCount Cities Selected"
                child.findViewById<View>(R.id.ivClose).setOnClickListener {
                    onParentCheckChange(false, i)
                    binding!!.llStateCity.removeView(child)
                }
                binding!!.llStateCity.addView(child)
            } else {
                var cityCount = 0
                for (j in statesCitiesList!![i].citiesList.indices) {
                    if (statesCitiesList!![i].citiesList[j].isChecked)
                        cityCount++
                }
                if (cityCount > 0) {
                    statesCitiesList!![i].isChecked = true
                    val child = HooleyMain.activity!!.layoutInflater.inflate(R.layout.layout_state_city, null)
                    (child.findViewById<View>(R.id.tvState) as TextView).text = statesCitiesList!![i].name
                    (child.findViewById<View>(R.id.tvCityCount) as TextView).text = "$cityCount Cities Selected"
                    child.findViewById<View>(R.id.ivClose).setOnClickListener {
                        onParentCheckChange(false, i)
                        binding!!.llStateCity.removeView(child)
                    }
                    binding!!.llStateCity.addView(child)
                }
            }
        }
    }

    override fun onClickParent(isExpanded: Boolean, position: Int) {
        if (isExpanded)
            elvStateCity!!.collapseGroup(position)
        else
            elvStateCity!!.expandGroup(position)
    }

    override fun onParentCheckChange(isChecked: Boolean, position: Int) {
        statesCitiesList!![position].isChecked = isChecked
        for (i in statesCitiesList!![position].citiesList.indices) {
            statesCitiesList!![position].citiesList[i].isChecked = isChecked
        }
        if (adapterCityState != null)
            adapterCityState!!.notifyDataSetChanged()
    }

    override fun onChildCheckChange(isChecked: Boolean, groupPosition: Int, childPosition: Int) {
        statesCitiesList!![groupPosition].citiesList[childPosition].isChecked = isChecked
        adapterCityState!!.notifyDataSetChanged()
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        when (buttonView.id) {
            R.id.cbMale -> binding!!.cbMale.isChecked = isChecked
            R.id.cbFemale -> binding!!.cbFemale.isChecked = isChecked
            R.id.cbTransgender -> binding!!.cbTransgender.isChecked = isChecked
        }
    }

    fun showBottomSheetDialog() {
        val view = layoutInflater.inflate(R.layout.fragment_stripe_bottom_sheet, null)
        val dialog = BottomSheetDialog(HooleyMain.activity!!)
        dialog.setContentView(view)
        val addCard = dialog.findViewById<Button>(R.id.addCard)
        val cardInputWidget = dialog.findViewById<CardInputWidget>(R.id.card_input_widget)
        val progressBar = dialog.findViewById<ProgressBar>(R.id.pbStripeSheet)
        addCard!!.setOnClickListener {
            progressBar!!.visibility = View.VISIBLE
            val cardToSave = cardInputWidget!!.card
            if (cardToSave != null) {
                if (cardToSave.validateCard()) {
                    val stripe = Stripe(HooleyMain.activity!!, HooleyMain.activity!!.getString(R.string.stripe_publish_key))
                    stripe.createToken(cardToSave,
                            object : TokenCallback {
                                override fun onSuccess(token: Token) {
                                    Log.e("tokken", " " + token.id)
                                    stripeTokken = token.id
                                    promoteEvent()
                                    //buyTicket();
                                    dialog.dismiss()
                                }

                                override fun onError(error: Exception) {
                                    error.printStackTrace()
                                    Log.e("StriError", " $error")
                                    Util.showToastMessage(HooleyMain.activity!!, "Enter Valid Card")
                                    progressBar.visibility = View.GONE
                                }
                            }
                    )
                } else {
                    progressBar.visibility = View.GONE
                    Util.showToastMessage(HooleyMain.activity!!, "Enter Valid Card")
                }
            } else {
                progressBar.visibility = View.GONE
                Util.showToastMessage(HooleyMain.activity!!, "Enter Valid Card")
            }
        }
        dialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == BUY_REQUEST_CODE) {
                stripeTokken = data.extras.get("stripeToken").toString()
                promoteEvent()
            }
        }
    }

    companion object {
        lateinit var instance: PromoteEventFragment
        private var eventId: String? = null
        private var eventStartDate: String? = null
        private var eventEndDate: String? = null


        fun newInstance(id: String, startDate: String, endDate: String): PromoteEventFragment {
            eventId = id
            eventStartDate = startDate
            eventEndDate = endDate
            instance = PromoteEventFragment()
            return instance
        }
    }

}
