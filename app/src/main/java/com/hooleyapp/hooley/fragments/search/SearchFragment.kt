package com.hooleyapp.hooley.fragments.search

import android.app.Dialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import android.view.animation.Animation
import android.view.animation.Transformation
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.CompoundButton
import android.widget.SeekBar
import android.widget.TextView
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterEventAges
import com.hooleyapp.hooley.adapters.AdapterEventCategory
import com.hooleyapp.hooley.databinding.FragmentSearchBinding
import com.hooleyapp.hooley.databinding.FragmentUserSearchBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.search.SearchFilterFragment.Companion.mainBinding
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.BookMarkSearchModel
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.PersonalProfileModel
import com.hooleyapp.hooley.model.staticData.Category
import com.hooleyapp.hooley.model.staticData.StaticDataModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.services.EventWebService
import java.text.DecimalFormat
import java.util.*

class SearchFragment : BaseFragment(), View.OnClickListener, CompoundButton.OnCheckedChangeListener, SeekBar.OnSeekBarChangeListener, AdapterView.OnItemClickListener {


    var eventService = EventWebService()
    internal lateinit var binding: FragmentSearchBinding
    private var list: ArrayList<Category> = ArrayList()
    private var adapterCategory: AdapterEventCategory? = null
    private var objectResponse: BookMarkSearchModel? = null
    private var customDialog: CustomDateTimePicker? = null
    private var currentProgress = 8046.72f
    private var eventData = ""
    private var searchText = ""
    private val gson = Gson()
    var model = StaticDataModel()
    private val searchBookmarkList = ArrayList<BookMarkSearchModel.SearchbookMark>()
    private var listAge: ArrayList<PersonalProfileModel.myObject> = ArrayList()
    private var adapterAge: AdapterEventAges? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false)
        if (userVisibleHint) {
            getEventData()
            setEditListener()
        }
        return binding.root
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser && isResumed) {
            getEventData()
            setEditListener()
        }
    }

    companion object {

        lateinit var instance: SearchFragment
        private var isPaid = false
        private var isFree = false
        lateinit var userSearchBinding: FragmentUserSearchBinding

        fun newInstance(): SearchFragment {
            instance = SearchFragment()
            return instance
        }
    }

    private fun setListener() {
        binding.rlClearAllSearch.setOnClickListener(this)
        binding.ivCollapseEvent.setOnClickListener(this)
        binding.ivExpandEvent.setOnClickListener(this)
        binding.ivCollapseBookMark.setOnClickListener(this)
        binding.ivExpandBookMark.setOnClickListener(this)
        binding.llDate.setOnClickListener(this)
        binding.btnShowResult.setOnClickListener(this)
        binding.btnBookMarkSearch.setOnClickListener(this)
        binding.cbFree.setOnCheckedChangeListener(this)
        binding.cbPaid.setOnCheckedChangeListener(this)
        binding.sbGeofence.setOnSeekBarChangeListener(this)
        binding.ivPlus.setOnClickListener(this)
        binding.ivMinus.setOnClickListener(this)
        binding.ivCollapseAge.setOnClickListener(this)
        binding.ivExpandAge.setOnClickListener(this)

        binding.ivMinus.setOnLongClickListener {
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    if (binding.ivMinus.isPressed) {
                        if (currentProgress >= 100.0f) {
                            var temp = currentProgress
                            temp -= 100f
                            if (temp <= 100.0f) {
                                currentProgress = 100.0f
                            } else {
                                currentProgress -= 100f
                            }
                            binding.sbGeofence.progress = (currentProgress).toInt()
                        }
                    } else {
                        timer.cancel()
                    }
                }
            }, 100, 200)
            true
        }
        binding.ivPlus.setOnLongClickListener {
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    if (binding.ivPlus.isPressed) {
                        if (currentProgress < 64373.0f) {
                            currentProgress += 100f
                            binding.sbGeofence.progress = (currentProgress).toInt()
                        }
                    } else {
                        timer.cancel()
                    }
                }
            }, 100, 200)

            true
        }

    }

    fun setEditListener() {
        mainBinding.edtSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (!mainBinding.edtSearch.text!!.isEmpty()) {
//                    if (mainBinding.rbHooleyEvents.isChecked) {
                    HooleyMain.activity!!.hideKeyboard()
                    showResultFragment(currentProgress.toDouble(), isPaid, isFree, searchText, eventData, list)
//                    }else{
//                        HooleyMain.activity!!.hideKeyboard()
//                        UserSearchFragment.addStringToSearch(mainBinding.edtSearch.text.toString())
//                    }
                }
            }
            true
        }

        mainBinding.ivClose.setOnClickListener {
            if (mainBinding.edtSearch.text.toString().isNotEmpty()) {
                HooleyMain.activity!!.hideKeyboard()
                mainBinding.edtSearch.setText("")
            }
        }

    }

    fun expand(v: View) {
        v.measure(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        val targetHeight = v.measuredHeight

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.layoutParams.height = 1
        v.visibility = View.VISIBLE
        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                v.layoutParams.height = if (interpolatedTime == 1f)
                    WindowManager.LayoutParams.WRAP_CONTENT
                else
                    (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // 1dp/ms
        a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
        binding.tvEventCategory.visibility = View.GONE
    }

    fun collapse(v: View) {
        val initialHeight = v.measuredHeight

        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // 1dp/ms
        a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
        setEventCategoryLabel()
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        when (buttonView.id) {
            R.id.cbPaid -> SearchFragment.isPaid = isChecked
            R.id.cbFree -> SearchFragment.isFree = isChecked
        }
    }

    override fun onResume() {
        super.onResume()
        binding.sbGeofence.progress = 8046
        binding.tvSbProgress.text = "5 miles"
    }


    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.unbind()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
        setAgeGridEvent()
        getBookmarkSearch()
    }

    private fun getBookmarkSearch() {
        binding.pbSearchFilter.visibility = View.VISIBLE
        eventService.getBookMarkSearch(object : IWebServiceCallback<BookMarkSearchModel> {
            override fun success(result: BookMarkSearchModel) {
                binding.pbSearchFilter.visibility = View.GONE
                binding.btnBookMarkSearch.isEnabled = true
                objectResponse = result
                //setGridEvent();
//                if (objectResponse!!.searchbookMarkList != null && objectResponse!!.searchbookMarkList.size > 0)
//                    setBookMarkSearch()
                if (objectResponse?.searchbookMarkList != null && objectResponse?.searchbookMarkList!!.size > 0) {
                    searchBookmarkList.clear()
                    binding.llBookMarkSearch.removeAllViews()
                    searchBookmarkList.addAll(objectResponse!!.searchbookMarkList)
                    setBookMarkSearch()
                }
            }

            override fun failure(message: String) {
                try {
                    binding.pbSearchFilter.visibility = View.GONE
                    binding.rlClearAllSearch.visibility = View.GONE
                    //setGridEvent();
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    private fun setBookMarkSearch() {
        binding.rlClearAllSearch.visibility = View.VISIBLE
        for (loop in searchBookmarkList.indices) {
            addChild(loop)
        }
    }

    fun addChild(position: Int) {
        val child = HooleyMain.activity!!.layoutInflater.inflate(R.layout.bookmark_search_item, null)
        (child.findViewById<View>(R.id.tvSearch) as TextView).text = searchBookmarkList[position].searchText!!.trim().substring(0, searchBookmarkList[position].searchText!!.trim().length - 1)
        child.findViewById<View>(R.id.ivClose).setOnClickListener { removeBookMarkSearch(searchBookmarkList[position].id, child) }
        child.findViewById<View>(R.id.tvSearch).setOnClickListener { showResultFragment(searchBookmarkList[position].miles, searchBookmarkList[position].isPaid, searchBookmarkList[position].isFree, searchBookmarkList[position].searchText, "", getEventTypeList(searchBookmarkList[position].eventTypeList)) }
        binding.llBookMarkSearch.addView(child)
    }

    private fun getEventTypeList(eventTypeList: ArrayList<BookMarkSearchModel.EventType>?): ArrayList<Category> {
        return if (eventTypeList != null && eventTypeList.size > 0) {
            for (i in eventTypeList.indices) {
                for (j in list.indices) {
                    if (list[j].id!! == eventTypeList[i].typeId!!.toInt())
                        list[j].isActive = true
                }
            }
            list
        } else {
            list
        }
    }

    private fun addBookMarkSearch() {
        HooleyMain.activity!!.showDialog()
        eventService.addBookMarkSearch((currentProgress * 1609.34).toFloat(), list, isPaid, isFree, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyMain.activity!!.removeDialog()
                HooleyMain.activity!!.showSuccessDialog("Search Bookmarked")
                getBookmarkSearch()
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }
            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    private fun removeAllBookMarkSearch() {
        eventService.removeAllBookMarkSearch(object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                binding.llBookMarkSearch.removeAllViews()
                binding.rlClearAllSearch.visibility = View.GONE
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    private fun removeBookMarkSearch(id: String?, child: View) {
        eventService.removeBookMarkSearch(id!!, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                binding.llBookMarkSearch.removeView(child)
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }
            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    fun expandAge(v: View) {
        v.measure(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        val targetHeight = v.measuredHeight
        v.layoutParams.height = 1
        v.visibility = View.VISIBLE
        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                v.layoutParams.height = if (interpolatedTime == 1f)
                    WindowManager.LayoutParams.WRAP_CONTENT
                else
                    (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }
        a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
        binding.tvMinimumAge.visibility = View.GONE
        binding.viewLine.visibility = View.VISIBLE
    }


    private fun setEventCategoryLabel() {
        var cattegoryName: String? = null
        for (i in list.indices) {
            if (list[i].isActive) {
                if (cattegoryName == null) {
                    cattegoryName = list[i].name
                } else {
                    cattegoryName = cattegoryName + ", " + list[i].name
                }
            }
        }
        binding.tvEventCategory.text = cattegoryName
        if (cattegoryName != null)
            binding.tvEventCategory.visibility = View.VISIBLE
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivCollapseEvent -> {
                collapse(binding.rlSelectEvent)
                binding.ivCollapseEvent.visibility = View.GONE
                binding.ivExpandEvent.visibility = View.VISIBLE
            }
            R.id.ivExpandEvent -> {
                expand(binding.rlSelectEvent)
                binding.ivCollapseEvent.visibility = View.VISIBLE
                binding.ivExpandEvent.visibility = View.GONE
            }
            R.id.ivCollapseBookMark -> {
                collapse(binding.llBookMark)
                binding.ivCollapseBookMark.visibility = View.GONE
                binding.ivExpandBookMark.visibility = View.VISIBLE
            }
            R.id.ivExpandBookMark -> if (binding.llBookMarkSearch.childCount > 0) {
                expand(binding.llBookMark)
                binding.ivCollapseBookMark.visibility = View.VISIBLE
                binding.ivExpandBookMark.visibility = View.GONE
            } else {
                HooleyMain.activity!!.showSuccessDialog("No bookmarked search found")
            }
            R.id.llDate -> showCustomDialog(binding.tvEventDate)
            R.id.btnBookMarkSearch -> {
                binding.btnBookMarkSearch.isEnabled = false
                addBookMarkSearch()
            }
            R.id.btnShowResult -> showResultFragment(currentProgress.toDouble(), isPaid, isFree, searchText, eventData, list)
            R.id.rlClearAllSearch -> removeAllBookMarkSearch()
            R.id.ivMinus -> if (currentProgress >= 100.0f) {
                var temp = currentProgress
                temp -= 100f
                if (temp <= 100.0f) {
                    currentProgress = 100.0f
                } else {
                    currentProgress -= 100f
                }
                binding.sbGeofence.progress = (currentProgress).toInt()
            }
            R.id.ivPlus -> if (currentProgress < 64373.0f) {
                currentProgress += 100f
                binding.sbGeofence.progress = (currentProgress).toInt()
            }
//           R.id.tvClearAll -> {
//               if (recentSearchList != null)
//                   clearAllSearch()
//           }
            R.id.ivCollapseAge -> {
                collapseAge(binding.rlSelectAge)
                binding.ivCollapseAge.visibility = View.GONE
                binding.ivExpandAge.visibility = View.VISIBLE
            }
            R.id.ivExpandAge -> {
                expandAge(binding.rlSelectAge)
                binding.ivCollapseAge.visibility = View.VISIBLE
                binding.ivExpandAge.visibility = View.GONE
            }
        }
    }

    private fun getEventData() {
        model = gson.fromJson(HooleyApp.db.getString(Constants.STATIC_MODEL), StaticDataModel::class.java)
        list.addAll(model.categories!!)
        setGridEvent()
    }

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        list[position].isActive = !list[position].isActive
        adapterCategory!!.notifyDataSetChanged()
    }

    private fun showResultFragment(meters: Double, paid: Boolean, free: Boolean, text: String?, date: String, mlist: ArrayList<Category>) {
        var date = date
        if (!TextUtils.isEmpty(binding.tvEventDate.text))
            eventData = binding.tvEventDate.text.toString()
        if (!TextUtils.isEmpty(mainBinding.edtSearch.text))
            searchText = mainBinding.edtSearch.text.toString()
        if (!TextUtils.isEmpty(binding.tvDate.text))
            date = binding.tvEventDate.text.toString()
        HooleyMain.activity!!.hideKeyboard()
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, SearchResultFragment.newInstance(meters / 1609.34, paid, free, searchText, date, mlist), "SearchResultFragment")
        hideDrawer(HooleyMain.activity!!)
    }


    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
        val f = DecimalFormat("##.00")
        currentProgress = seekBar.progress.toFloat()
        try {
            if (currentProgress < 1609.344) {
                if (progress <= 100) {
                    currentProgress = 100.0f
                    binding.tvSbProgress.text = 100.toString() + " meters"
                } else {
                    binding.tvSbProgress.text = f.format(currentProgress) + " meters"
                }
            } else {
                binding.tvSbProgress.text = "${f.format(currentProgress / 1609.344)} miles"
            }
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

    }

    override fun onStartTrackingTouch(seekBar: SeekBar) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar) {
        currentProgress = seekBar.progress.toFloat()
    }

    fun collapseAge(v: View) {
        val initialHeight = v.measuredHeight

        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // 1dp/ms
        a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
        setEventAgeLabel()
        binding.viewLine.visibility = View.GONE
    }

    private fun setEventAgeLabel() {
        var cattegoryName: String? = null
        for (i in listAge.indices) {
            if (listAge[i].isActive) {
                if (cattegoryName == null) {
                    cattegoryName = listAge[i].name
                }
            }
        }
        binding.tvMinimumAge.text = cattegoryName
        if (cattegoryName != null)
            binding.tvMinimumAge.visibility = View.VISIBLE
    }


    private fun showCustomDialog(view: TextView) {
        customDialog = CustomDateTimePicker(HooleyMain.activity!!,
                object : CustomDateTimePicker.ICustomDateTimeListener {
                    override fun onSet(dialog: Dialog, calendarSelected: Calendar,
                                       dateSelected: Date, year: Int, monthFullName: String,
                                       monthShortName: String, monthNumber: Int, date: Int,
                                       weekDayFullName: String, weekDayShortName: String,
                                       hour24: Int, hour12: Int, min: Int, sec: Int,
                                       AM_PM: String) {
                        view.text = ""
                        view.text = String.format("%02d-%02d-%04d", monthNumber + 1, calendarSelected.get(Calendar.DAY_OF_MONTH), year)

                    }

                    override fun onCancel() {

                    }
                })
        customDialog!!.set24HourFormat(true)
        customDialog!!.setDate(Calendar.getInstance())
        customDialog!!.showDialog()
    }


    @Throws(NullPointerException::class)
    private fun setGridEvent() {
        list.sortBy { it.name }
        adapterCategory = AdapterEventCategory(HooleyMain.activity!!, list)
        binding.gvEventType.adapter = adapterCategory
        binding.gvEventType.onItemClickListener = this
    }

    fun getAgeList(): ArrayList<PersonalProfileModel.myObject> {

        listAge.addAll(model.minimumAgeList!!)

        return listAge

    }


    private fun setAgeGridEvent() {
        adapterAge = AdapterEventAges(HooleyMain.activity!!, getAgeList())
        binding.gvAge.adapter = adapterAge
    }

}
