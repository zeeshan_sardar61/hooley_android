package com.hooleyapp.hooley.fragments.auth

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyAuth
import com.hooleyapp.hooley.databinding.FragmentVerifyPhoneContactBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.hideKeyboard
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.requestmodel.VerifyPhoneNumberModel
import com.hooleyapp.hooley.services.ProfileWebService

class VerifyCodeFragment : BaseFragment(), View.OnClickListener {
    var service = ProfileWebService()
    lateinit var binding: FragmentVerifyPhoneContactBinding
    private var countDownTimer: CountDownTimer? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_verify_phone_contact, container, false)
        startTimer()
        binding.ivBack.setOnClickListener {
            HooleyAuth.activity.onBackPressed()
        }
        setListener()
        binding.tvPhoneNumber.text = "+$cc$phoneNumberText"
        binding.tvMsg.text = "To make sure it’s you, please enter the\n 4-digit code in the text message\n sent to you."
        binding.pinCode.setOnPinEnteredListener { str ->
            str?.also {
                if (str.length == 4) {
                    HooleyAuth.activity.hideKeyboard()
                    verifyPinCode(binding.pinCode.text.toString())
                }

            }

        }
        return binding.root
    }

    private fun setListener() {
        binding.rlResendCode.setOnClickListener(this)
    }


    fun startTimer() {
        countDownTimer = object : CountDownTimer((60 * 3 * 1000).toLong(), 100) {
            override fun onTick(l: Long) {
                val seconds = l / 1000
                val currentMinute = seconds / 60
                binding.tvTimer.text = String.format("%02d", seconds / 60) + ":" + String.format("%02d", seconds % 60)
                if (currentMinute == 0L && seconds == 0L) {
                    binding.rlResendCode.visibility = View.VISIBLE
                    binding.tvTimer.text = "00:00"
                    if (countDownTimer != null) {
                        countDownTimer!!.cancel()
                    }
                } else {

                }
            }

            override fun onFinish() {
                binding.rlResendCode.visibility = View.VISIBLE
                binding.tvTimer.text = "00:00"
                if (countDownTimer != null) {
                    countDownTimer!!.cancel()
                }
            }
        }.start()
    }

    private fun verifyPinCode(code: String) {
        if (!NetworkUtil.isInternetConnected(HooleyAuth.activity)) {
            HooleyAuth.activity.showSuccessDialog(Constants.NO_NETWORK)
            return
        }
        HooleyAuth.activity.showDialog()
        service.verifyPinCode(apiMessageId!!, code, false, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                HooleyAuth.activity.removeDialog()
                if (countDownTimer != null) {
                    countDownTimer!!.cancel()
                }
                HooleyApp.db.putBoolean(Constants.IS_PHONE_VERIFIED, true)
                HooleyApp.db.putString(Constants.VERIFIED_PHONE_NUMBER, cc + phoneNumberText!!)
                HooleyApp.db.putString(Constants.COUNTRY_CODE, cc)
                Util.hideKeyboard(HooleyAuth.activity)
                val intent = Intent(activity, HooleyAuth::class.java)
                startActivity(intent)
                activity!!.finish()
            }

            override fun failure(message: String) {
                HooleyAuth.activity.removeDialog()
                try {
                    HooleyAuth.activity.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }

    override fun onClick(v: View) {
        when (v.id) {
//            R.id.btnVerifyCode ->
            R.id.rlResendCode -> verifyNumber()
        }
    }

    private fun verifyNumber() {
        if (!NetworkUtil.isInternetConnected(HooleyAuth.activity)) {
            HooleyAuth.activity.showSuccessDialog(Constants.NO_NETWORK)
            return
        }
        HooleyAuth.activity.showDialog()
        service.verifyNumber(phoneNumberText!!, cc, cc + phoneNumberText!!, object : GenericCallback<VerifyPhoneNumberModel> {
            override fun success(result: VerifyPhoneNumberModel) {
                HooleyAuth.activity.removeDialog()
                apiMessageId = result.apiMessageId
                binding.rlResendCode.visibility = View.GONE
                startTimer()
            }

            override fun failure(message: String) {
                HooleyAuth.activity.removeDialog()
                try {
                    HooleyAuth.activity.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    companion object {

        lateinit var instance: VerifyCodeFragment
        private var apiMessageId: String? = null
        private var phoneNumberText: String? = null
        private var cc = ""

        fun newInstance(messageId: String, countryCode: String, phoneNumber: String): VerifyCodeFragment {
            apiMessageId = messageId
            phoneNumberText = phoneNumber
            cc = countryCode
            instance = VerifyCodeFragment()
            return instance
        }
    }
}
