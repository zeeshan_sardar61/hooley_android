package com.hooleyapp.hooley.fragments.event

import android.app.Dialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentPostponeEventBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.others.TermsAndConditionFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GetUpdateEventActionModel
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.EventWebService
import java.util.*
import java.util.concurrent.TimeUnit

class PostPoneEventFragment : BaseFragment(), View.OnClickListener {
    var binding: FragmentPostponeEventBinding? = null
    var mService = EventWebService()
    private var customDialog: CustomDateTimePicker? = null
    private var previousSeletedDate: Date? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_postpone_event, container, false)
        setListener()
        setData()
        getEventUpdateAction()
        return binding!!.root
    }

    private fun setListener() {
        binding!!.ivEventStart.setOnClickListener(this)
        binding!!.ivEventEnd.setOnClickListener(this)
        binding!!.rlPosponeEventIndef.setOnClickListener(this)
        binding!!.rlPostPoneEventDate.setOnClickListener(this)
        binding!!.cbPostPone.setOnClickListener(this)
        binding!!.cbPostPoneIndefinitely.setOnClickListener(this)
        binding!!.btnPostPoneEvent.setOnClickListener(this)
        binding!!.tvTermsConditions.setOnClickListener(this)
        binding!!.rlPostPoneEventDate.performClick()
        binding!!.ivInfoIndefinitely.setOnClickListener { HooleyMain.activity!!.showInfoDialog("Postpone Indefinitely", "Guests will be alerted by both email and an in-app notification. The event will still still show up in searches but users won’t be able to purchase tickets (if applicable).") }
        binding!!.ivInfoDate.setOnClickListener { HooleyMain.activity!!.showInfoDialog("Postpone to Below\nDate & Time", "Guests will be alerted to the new date & time by both email and an in-app notification.") }

    }

    private fun setData() {
        if (isPaid) {
            binding!!.rlAlterTicketHolder.visibility = View.VISIBLE
            binding!!.llPostPoneEvent.visibility = View.VISIBLE
        } else {
            binding!!.rlAlterTicketHolder.visibility = View.GONE
            binding!!.llPostPoneEvent.visibility = View.GONE
        }
        binding!!.tvEventStartDate.text = DateUtils.convertDateForEvent(DateUtils.getLocalDate(eventStartDate!!))
        binding!!.tvEventEndDate.text = DateUtils.convertDateForEvent(DateUtils.getLocalDate(eventEndDate!!))
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.cbPostPoneIndefinitely, R.id.rlPosponeEventIndef -> {
                binding!!.llPostPoneEvent.alpha = 1f
                binding!!.rlPosponeEvent.alpha = .5f
                binding!!.cbPostPone.isChecked = false
                binding!!.cbPostPoneIndefinitely.isChecked = true
            }

            R.id.cbPostPone, R.id.rlPostPoneEventDate -> {
                binding!!.llPostPoneEvent.alpha = .5f
                binding!!.rlPosponeEvent.alpha = 1f
                binding!!.cbPostPone.isChecked = true
                binding!!.cbPostPoneIndefinitely.isChecked = false
            }

            R.id.ivEventStart -> if (binding!!.cbPostPone.isChecked)
                showCustomDialog(binding!!.tvEventStartDate)
            R.id.ivEventEnd -> if (binding!!.cbPostPone.isChecked)
                showCustomDialog(binding!!.tvEventEndDate)
            R.id.btnPostPoneEvent -> if (validInput())
                setUpdateEventAction()
            R.id.tvTermsConditions -> (activity as ActivityBase).addFragment(R.id.container, TermsAndConditionFragment.newInstance(true, false), "TermsAndConditionFragment")
        }
    }

    private fun validInput(): Boolean {
        if (!binding!!.cbPostPone.isChecked && !binding!!.cbPostPoneIndefinitely.isChecked) {
            Util.showToastMessage(HooleyMain.activity!!, "Select your choice")
            return false
        } else if (TextUtils.isEmpty(binding!!.edtReason.text.toString())) {
            binding!!.edtReason.requestFocus()
            binding!!.edtReason.error = getString(R.string.required)
            return false
        } else if (!binding!!.tbTerms.isChecked) {
            Util.showToastMessage(HooleyMain.activity!!, "Please agree to terms and conditions")
            return false
        } else {
            return true
        }
    }

    fun showCustomDialog(view: TextView) {
        customDialog = CustomDateTimePicker(HooleyMain.activity!!,
                object : CustomDateTimePicker.ICustomDateTimeListener {
                    override fun onSet(dialog: Dialog, calendarSelected: Calendar,
                                       dateSelected: Date, year: Int, monthFullName: String,
                                       monthShortName: String, monthNumber: Int, date: Int,
                                       weekDayFullName: String, weekDayShortName: String,
                                       hour24: Int, hour12: Int, min: Int, sec: Int,
                                       AM_PM: String) {
                        if (view.id == binding!!.tvEventEndDate.id) {
                            if (previousSeletedDate != null) {
                                if (TimeUnit.MILLISECONDS.toHours(dateSelected.time - previousSeletedDate!!.time) < 1) {
                                    Util.showToastMessage(HooleyMain.activity!!, "End date is smaller then previous date")
                                    return
                                }
                                if (dateSelected.before(Calendar.getInstance().time)) {
                                    Util.showToastMessage(HooleyMain.activity!!, "Invalid date, Please select future date.")
                                    return
                                }
                            }
                        } else {
                            if (dateSelected.before(Calendar.getInstance().time)) {
                                Util.showToastMessage(HooleyMain.activity!!, "Invalid date, Please select future date.")
                                return
                            }
                            previousSeletedDate = dateSelected
                        }
                        view.text = ""
                        view.text = (String.format("%04d-%02d-%02d", year, monthNumber + 1, calendarSelected.get(Calendar.DAY_OF_MONTH))
                                + " " + String.format("%02d:%02d", hour12, min) +
                                " " + AM_PM)

                    }

                    override fun onCancel() {

                    }
                })
        /**
         * Pass Directly current time format it will return AM and PM if you set
         * false
         */
        customDialog!!.set24HourFormat(false)
        /**
         * Pass Directly current data and time to show when it pop up
         */
        if (TextUtils.isEmpty(view.text.toString()))
            customDialog!!.setDate(Calendar.getInstance())
        else {
            customDialog!!.setDate(DateUtils.stringToDate(DateUtils.SendDateToDate(view.text.toString())))
        }
        //        customDialog.setDate(Calendar.getInstance());
        customDialog!!.showDialog()
    }


    fun getEventUpdateAction() {
        mService.getEventUpdateAction(eventId!!, updateActionId, object : IWebServiceCallback<GetUpdateEventActionModel> {
            override fun success(result: GetUpdateEventActionModel) {
                binding!!.tvTerms.text = result.termsAndConditionInfo.termsAndConditionText
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    fun setUpdateEventAction() {
        HooleyMain.activity!!.showDialog()
        if (binding!!.cbPostPoneIndefinitely.isChecked) {
            mService.setUpdateEventAction(eventId!!, updateActionId, null, null,
                    true, true, binding!!.edtReason.text.toString(), object : IWebServiceCallback<GeneralModel> {
                override fun success(result: GeneralModel) {
                    //                Util.showToastMessage(HooleyMain.activity, "Event updated successfully");
                    HooleyMain.activity!!.removeDialog()
                    HooleyMain.activity!!.showSuccessDialog("Event postponed successfully")
                    (HooleyMain.activity as ActivityBase).callFragment(R.id.container, MyEventMainFragment.newInstance(), "MyEventMainFragment")
                }

                override fun failure(message: String) {
                    HooleyMain.activity!!.removeDialog()
                    try {
                        HooleyMain.activity!!.showSuccessDialog(message)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }

                override fun onTokenExpire() {
                    onTokenExpireLogOut()
                }
            })
        } else {
            mService.setUpdateEventAction(eventId!!, updateActionId, binding!!.tvEventStartDate.text.toString(), binding!!.tvEventEndDate.text.toString(),
                    true, true, binding!!.edtReason.text.toString(), object : IWebServiceCallback<GeneralModel> {
                override fun success(result: GeneralModel) {
                    //                Util.showToastMessage(HooleyMain.activity, "Event updated successfully");
                    HooleyMain.activity!!.removeDialog()
                    HooleyMain.activity!!.showSuccessDialog("Event postponed successfully")
                    (HooleyMain.activity as ActivityBase).callFragment(R.id.container, MyEventMainFragment.newInstance(), "MyEventMainFragment")
                }

                override fun failure(message: String) {
                    HooleyMain.activity!!.removeDialog()
                    try {
                        HooleyMain.activity!!.showSuccessDialog(message)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }

                override fun onTokenExpire() {
                    onTokenExpireLogOut()
                }
            })
        }
    }

    companion object {

        lateinit var instance: PostPoneEventFragment
        private var isPaid: Boolean = false
        private var updateActionId: Int = 0
        private var eventId: String? = null
        private var eventStartDate: String? = null
        private var eventEndDate: String? = null

        fun newInstance(id: String, paid: Boolean, startDate: String, endDate: String, actionId: Int): PostPoneEventFragment {
            eventId = id
            updateActionId = actionId
            eventStartDate = startDate
            eventEndDate = endDate
            isPaid = paid
            instance = PostPoneEventFragment()
            return instance
        }
    }

}
