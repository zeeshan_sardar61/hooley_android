package com.hooleyapp.hooley.fragments.friends

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatDialogFragment
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.gigamole.infinitecycleviewpager.HorizontalInfiniteCycleViewPager
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterFriendsCardPager
import com.hooleyapp.hooley.fragments.messaging.ChatWindowFragment
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.fragments.profile.ProfileMainFragment
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.FriendCardClickListener
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

/**
 * Created by Nauman on 3/6/2018.
 */

@SuppressLint("ValidFragment")
class FriendCardPagerFragment constructor(var position: Int = 0, var tagList: ArrayList<FriendsTable>) : AppCompatDialogFragment(), FriendCardClickListener {

    private var service: FriendWebService = FriendWebService()
    private var adapterCardPager: AdapterFriendsCardPager? = null
    private var cardPager: HorizontalInfiniteCycleViewPager? = null
    private lateinit var contentView: View
    lateinit var mListener: ViewAvatarFragment.CardCallBackListener


    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        contentView = View.inflate(context, R.layout.card_pager_fragment, null)
        dialog.setContentView(contentView)
        (HooleyMain.activity as HooleyMain).removeBubbleView()
        cardPager = contentView.findViewById(R.id.cardPager)
        if (tagList != null && tagList.size > 0) {
            setCardAdapter()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window.requestFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    fun show(fragmentManager: FragmentManager) {
        val ft = fragmentManager.beginTransaction()
        ft.add(this, tag)
        ft.commitAllowingStateLoss()
    }

    private fun setCardAdapter() {
        adapterCardPager = AdapterFriendsCardPager(tagList)
        adapterCardPager!!.mListener = this
        cardPager!!.adapter = adapterCardPager
        cardPager!!.currentItem = position
    }

    override fun onClickViewProfile(position: Int) {
        dismiss()
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, FriendsProfileFragment.newInstance(tagList[position].userId, tagList[position].fullName!!), "FriendsProfileFragment")
    }

    override fun onClickAddFriend(position: Int) {
        dismiss()
        if (::mListener.isInitialized)
            mListener.reloadFragment(true)
        sendFriendRequest(tagList[position].userId)
    }

    override fun onClickAcceptRequest(position: Int, value: Boolean) {
        dismiss()
        friendRequest(tagList[position].userId, value)
    }

    override fun onClickMessage(position: Int) {
        dismiss()
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ChatWindowFragment("", tagList[position].userId.toString(), tagList[position].fullName!!), "ChatWindowFragment")
    }

    override fun onClickClose() {
        dismiss()
    }

    override fun onClickMyProfile() {
        dismiss()
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ProfileMainFragment.newInstance(false), "ProfileMainFragment")
        hideDrawer(HooleyMain.activity!!)
    }


    private fun sendFriendRequest(friendId: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        val friendList = ArrayList<Int>()
        friendList.add(friendId)
        service.sendFriendRequest(friendList, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                Util.showToastMessage(HooleyMain.activity!!, "Friend Request Sent")
                dismiss()
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }


            }
        })
    }

    private fun friendRequest(userId: Int, reply: Boolean) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        service.acceptIgnoreFriendRequest(userId, reply, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                dismiss()
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

}
