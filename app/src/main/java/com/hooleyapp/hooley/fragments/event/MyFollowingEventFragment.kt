package com.hooleyapp.hooley.fragments.event

import android.app.Dialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterMoreMediaDialoge
import com.hooleyapp.hooley.adapters.AdapterOtherEvent
import com.hooleyapp.hooley.databinding.FragmentOtherEventBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.others.MoreMediaDialogFragment
import com.hooleyapp.hooley.helper.CustomDateTimePicker
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.MyFollowingEventModel
import com.hooleyapp.hooley.model.MyHostEventModel
import com.hooleyapp.hooley.model.SetAlertModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.EventWebService
import java.util.*

/**
 * Created by Nauman on 1/19/2018.
 */

class MyFollowingEventFragment : BaseFragment(), AdapterOtherEvent.IMyEventsClickListeners, SwipeRefreshLayout.OnRefreshListener {

    internal var binding: FragmentOtherEventBinding? = null
    private val responseObj: MyFollowingEventModel? = null
    private var adapterOtherEvent: AdapterOtherEvent? = null
    private val offsetHeader = true
    private var list = ArrayList<MyHostEventModel.HostEvents>()
    private val gson = Gson()
    private val myFollowEventModel: MyFollowingEventModel? = null
    private val service = EventWebService()
    internal var isSwiped = false
    private var moreFilterDialogeFragment: MoreMediaDialogFragment? = null
    internal val filterList = ArrayList<AdapterMoreMediaDialoge.MoreItem>()
    private var customDialog: CustomDateTimePicker? = null

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {

        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            try {
                getMyFollowingEvent()
            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            }

        } else {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_other_event, container, false)
        setListener()
        return binding!!.root
    }

    private fun setListener() {
        binding!!.slAttend.setOnRefreshListener(this)
    }

    private fun getMyFollowingEvent() {
        if (binding != null && isSwiped == false)
            binding!!.pbOther.visibility = View.VISIBLE

        service.getMyFollowingEvent(object : IWebServiceCallback<MyFollowingEventModel> {
            override fun success(result: MyFollowingEventModel) {
                binding!!.tvNoData.visibility = View.GONE
                if (isSwiped == true) {
                    binding!!.slAttend.isRefreshing = false
                    isSwiped = false
                }
                binding!!.pbOther.visibility = View.GONE
                list = result.followingEvent
                setRecyclerView()
            }

            override fun failure(message: String) {
                binding!!.pbOther.visibility = View.GONE
                if (isSwiped == true) {
                    binding!!.slAttend.isRefreshing = false
                    isSwiped = false
                }
                try {
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.rvOtherEvent.visibility = View.GONE
                    binding!!.tvNoData.text = message
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    private fun setRecyclerView() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        binding!!.rvOtherEvent.layoutAnimation = animation
        binding!!.rvOtherEvent.layoutManager = manager
        binding!!.rvOtherEvent.setEmptyView(binding!!.tvNoData)
        adapterOtherEvent = AdapterOtherEvent(HooleyMain.activity!!, list, true)
        adapterOtherEvent!!.mListener = this
        binding!!.rvOtherEvent.adapter = adapterOtherEvent
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null) {
            binding!!.unbind()
        }
    }

    override fun onRefresh() {
        if (binding!!.slAttend != null) {
            binding!!.slAttend.isRefreshing = true
            binding!!.pbOther.visibility = View.GONE
        }
        isSwiped = true
        getMyFollowingEvent()
    }


    fun showCustomDialog(eventId: String, startTime: String, endTime: String, alertTime: String) {
        var setTime = ""
        customDialog = CustomDateTimePicker(HooleyMain.activity!!,
                object : CustomDateTimePicker.ICustomDateTimeListener {
                    override fun onSet(dialog: Dialog, calendarSelected: Calendar,
                                       dateSelected: Date, year: Int, monthFullName: String,
                                       monthShortName: String, monthNumber: Int, date: Int,
                                       weekDayFullName: String, weekDayShortName: String,
                                       hour24: Int, hour12: Int, min: Int, sec: Int,
                                       AM_PM: String) {

                        if (dateSelected.before(Calendar.getInstance().time)) {
                            Util.showToastMessage(HooleyMain.activity!!, "Invalid date, Please select future date.")
                            return
                        }
                        if (dateSelected.after(DateUtils.stringToDate(endTime))) {
                            Util.showToastMessage(HooleyMain.activity!!, "Invalid date, date/time is greater than event end date and time")
                            return
                        }

                        setTime = (String.format("%04d-%02d-%02d", year, monthNumber + 1, calendarSelected.get(Calendar.DAY_OF_MONTH))
                                + " " + String.format("%02d:%02d", hour12, min) +
                                " " + AM_PM)

                        setEventAlert(eventId, setTime)

                    }

                    override fun onCancel() {

                    }
                })
        /**
         * Pass Directly current time format it will return AM and PM if you set
         * false
         */
        customDialog!!.set24HourFormat(false)
        /**
         * Pass Directly current data and time to show when it pop up
         */
        if (TextUtils.isEmpty(setTime)) {
            customDialog!!.setDate(Calendar.getInstance())
        } else {
            customDialog!!.setDate(DateUtils.stringToDate(DateUtils.getLocalDate(alertTime)))
        }
        customDialog!!.showDialog()
    }


    override fun onClickItem(position: Int) {
        HooleyApp.db.putBoolean(Constants.IS_COMMENT_CLICKED, false)
        (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment
                .newInstance(list[position].id!!), "LiveEventFragment")
    }

    override fun onClickAlert(position: Int) {
        if (list[position].isAlertOn) {
            showCustomDialog(list[position].id!!, list[position].startTime!!, list[position].endTime!!, list[position].alertDate!!)
        } else {
            showCustomDialog(list[position].id!!, list[position].startTime!!, list[position].endTime!!, "")
        }
    }

    override fun onLongClick(position: Int) {
        setMoreCallerBackListener(list[position])
    }


    private fun setMoreCallerBackListener(hostEvents: MyHostEventModel.HostEvents) {
        if (filterList.size > 0) {
            filterList.clear()
        }
        var eventAlertList: AdapterMoreMediaDialoge.MoreItem
        eventAlertList = if (hostEvents.isAlertOn) {
            AdapterMoreMediaDialoge.MoreItem("Edit Event Alert", false)
        } else {
            AdapterMoreMediaDialoge.MoreItem("Set Event Alert", false)
        }

        filterList.add(eventAlertList)

        moreFilterDialogeFragment = MoreMediaDialogFragment.Builder(HooleyMain.activity!!).setTitle("Action").setType(Constants.TYPE_MORE_80).setList(filterList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    setEventAlert(hostEvents)
                    moreFilterDialogeFragment!!.dismiss()
                }
            }
        }.create()
        if (!moreFilterDialogeFragment!!.isAdded)
            moreFilterDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
    }

    private fun setEventAlert(eventId: String, date: String) {
        service.setEventAlert(Integer.parseInt(eventId), date, true, object : GenericCallback<SetAlertModel> {
            override fun success(result: SetAlertModel) {
                for (i in list.indices) {
                    if (list[i].id == eventId) {
                        list[i].isAlertOn = true
                        break
                    }
                }
//                context.showToastMessage("Alert added")
                HooleyMain.activity!!.showSuccessDialog("Alert added")
                adapterOtherEvent!!.notifyDataSetChanged()
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })

    }

    private fun setEventAlert(hostEvents: MyHostEventModel.HostEvents) {
        if (hostEvents.isAlertOn) {
            showCustomDialog(hostEvents.id!!, hostEvents.startTime!!, hostEvents.endTime!!, hostEvents.alertDate!!)
        } else {
            showCustomDialog(hostEvents.id!!, hostEvents.startTime!!, hostEvents.endTime!!, "")
        }
    }


}
