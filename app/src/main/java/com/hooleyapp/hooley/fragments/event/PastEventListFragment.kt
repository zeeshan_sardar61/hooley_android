package com.hooleyapp.hooley.fragments.event

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterPastEventList
import com.hooleyapp.hooley.databinding.FragmentPastEventListBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.model.PastGuestListModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.Util
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by Nauman on 5/18/2018.
 */

class PastEventListFragment : BaseFragment() {

    var binding: FragmentPastEventListBinding? = null
    private var adapterPastEvent: AdapterPastEventList? = null
    private var list: ArrayList<PastGuestListModel.Guest>? = null
    private val gson = Gson()
    private var myInviteEventModel: PastGuestListModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_past_event_list, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getFromLocalDb()
    }


    private fun getFromLocalDb() {
        myInviteEventModel = gson.fromJson(HooleyApp.db.getString(Constants.RESPONSE_GSON_MY_PAST_EVENT), PastGuestListModel::class.java)
        if (myInviteEventModel != null) {
            if (myInviteEventModel!!.guestArrayList.size > 0) {
                if (list == null)
                    list = ArrayList<PastGuestListModel.Guest>()
                if (list!!.size > 0)
                    list!!.clear()
                list!!.addAll(myInviteEventModel!!.guestArrayList)
                setRecyclerView()
                getPastGuestList(false)
            } else {
                getPastGuestList(true)
            }
        } else {
            getPastGuestList(true)

        }
    }


    private fun setRecyclerView() {
        binding!!.tvTitle.text = "Past Event (" + list!!.size + ")"
        val manager = LinearLayoutManager(HooleyMain.activity)
        binding!!.rvEventList.layoutManager = manager
        binding!!.rvEventList.setEmptyView(binding!!.tvNoData)
        adapterPastEvent = AdapterPastEventList(HooleyMain.activity!!, list)
        binding!!.rvEventList.adapter = adapterPastEvent
    }

    // TODO MOVE TO SERVICE
    private fun getPastGuestList(showDialog: Boolean) {
        if (showDialog && binding != null)
            binding!!.pbOther.visibility = View.VISIBLE
        val call = HooleyApp.apiService.getPastGuestList(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<PastGuestListModel> {

            override fun onResponse(call: Call<PastGuestListModel>, response: Response<PastGuestListModel>) {
                binding!!.pbOther.visibility = View.GONE
                val strCat = gson.toJson(response.body())
                HooleyApp.db.putString(Constants.RESPONSE_GSON_MY_PAST_EVENT, strCat)
                if (response.body()!!.success) {
                    if (showDialog) {
                        if (list == null)
                            list = ArrayList<PastGuestListModel.Guest>()
                        if (list!!.size > 0)
                            list!!.clear()
                        if (response.body()!!.guestArrayList != null)
                            list!!.addAll(response.body()!!.guestArrayList)
                        setRecyclerView()
                    } else {
                        if (response.body()!!.guestArrayList != null && list!!.size != response.body()!!.guestArrayList.size) {
                            list!!.clear()
                            list!!.addAll(response.body()!!.guestArrayList)
                            adapterPastEvent!!.notifyDataSetChanged()
                        }
                    }
                    binding!!.tvNoData.visibility = View.GONE
                } else {
                    if (list != null && list!!.size > 0) {
                        list!!.clear()
                        adapterPastEvent!!.notifyDataSetChanged()
                    }
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.rvEventList.visibility = View.GONE
                    if (!TextUtils.isEmpty(response.body()!!.exception))
                        binding!!.tvNoData.text = response.body()!!.exception
                }
            }

            override fun onFailure(call: Call<PastGuestListModel>, t: Throwable) {
                Log.d("ERROR", "ERROR:" + t.message)
                binding!!.pbOther.visibility = View.GONE
                Util.showToastMessage(HooleyMain.activity!!, "Server not responding")
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null) {
            binding!!.unbind()
        }
    }

    companion object {

        lateinit var instance: PastEventListFragment
        fun newInstance(): PastEventListFragment {
            instance = PastEventListFragment()
            return instance
        }
    }
}
