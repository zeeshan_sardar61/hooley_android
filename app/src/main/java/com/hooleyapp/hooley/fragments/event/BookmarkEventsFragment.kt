package com.hooleyapp.hooley.fragments.event

import android.app.PendingIntent
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RadioGroup
import android.widget.ToggleButton
import com.facebook.CallbackManager
import com.facebook.FacebookSdk
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterBookmarkEvents
import com.hooleyapp.hooley.databinding.FragmentBookmarkEventsBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.IBookmarkEventsClickListener
import com.hooleyapp.hooley.interfaces.ISocialCallBackListener
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.BookmarkEventModel
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.*
import com.hooleyapp.hooley.services.EventWebService
import com.hooleyapp.hooley.social.SocialClass
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*


/**
 * Created by Adil on 21-Mar-18.
 */

class BookmarkEventsFragment : BaseFragment(), RadioGroup.OnCheckedChangeListener, View.OnClickListener, IBookmarkEventsClickListener, ISocialCallBackListener, SwipeRefreshLayout.OnRefreshListener {
    internal var binding: FragmentBookmarkEventsBinding? = null
    internal var objectResponse: BookmarkEventModel? = null
    internal var adapter: AdapterBookmarkEvents? = null
    private var position = 0
    private val socailClass = SocialClass()
    private var callbackManager: CallbackManager? = null
    //    private var twitterAuthClient: TwitterAuthClient? = null
    private val ivFacebook: ImageView? = null
    private val ivTwitter: ImageView? = null
    private val tbFacebookConnect: ToggleButton? = null
    private val tbTwitterConnect: ToggleButton? = null
    private var mlistFilterBookmark: ArrayList<BookmarkEventModel.EventDetail>? = null
    private val eventService = EventWebService()
    internal var isSwiped = false
    internal var bold = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)
    internal var regular = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bookmark_events, container, false)
        callbackManager = socailClass.initFacebookSdk()
//        twitterAuthClient = socailClass.initTwitter()
        binding!!.rbAllEvent.typeface = bold
        binding!!.rbAllEvent.isChecked = true
        setListener()
        showEventBookmark()
        return binding!!.root
    }

    private fun setListener() {
        binding!!.rgTab.setOnCheckedChangeListener(this)
        binding!!.slBookmarks.setOnRefreshListener(this)
        socailClass.setMListener(this)
    }


    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        when (checkedId) {
            R.id.rbAllEvent -> {
                binding!!.rbAllEvent.typeface = bold
                binding!!.rbPastEvents.typeface = regular
                FilterByAll()
                binding!!.rbAllEvent.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding!!.rbAllEvent.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_left_radio)
                binding!!.rbPastEvents.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding!!.rbPastEvents.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
            }
            R.id.rbPastEvents -> {
                binding!!.rbAllEvent.typeface = regular
                binding!!.rbPastEvents.typeface = bold
                FilterByPast()
                binding!!.rbAllEvent.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding!!.rbAllEvent.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding!!.rbPastEvents.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding!!.rbPastEvents.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_right_radio)
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        shareEvent(event.message)
    }

    private fun shareEvent(shareMedia: String) {
        if (binding != null && !isSwiped) {
            binding!!.pbBookmark.visibility = View.VISIBLE
        }
        eventService.shareEvent(objectResponse!!.bookmarkedEventsList[position].eventId!!, shareMedia, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                binding!!.pbBookmark.visibility = View.GONE
                objectResponse!!.bookmarkedEventsList[position].sharesCount = objectResponse!!.bookmarkedEventsList[position].sharesCount + 1
                adapter!!.notifyDataSetChanged()

            }

            override fun failure(message: String) {
                binding!!.pbBookmark.visibility = View.GONE
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }


    override fun onClick(v: View) {
        when (v.id) {

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onClickLike(eventObj: BookmarkEventModel.EventDetail, pos: Int) {
        position = pos
        likeEvent(objectResponse!!.bookmarkedEventsList[pos].eventId!!)

    }

    override fun onClickShare(eventObj: BookmarkEventModel.EventDetail, pos: Int) {
        position = pos
        object : GetBitmapTask() {
            override fun onPostExecute(bmp: Bitmap) {
                super.onPostExecute(bmp)
                //                HooleyMain.removeDialog();
                showShareEventDialog(bmp)
            }
        }.execute(mlistFilterBookmark!![position].coverPhoto)
    }

    override fun onClickFollow(eventObj: BookmarkEventModel.EventDetail, pos: Int) {
        position = pos
        followEvent(eventObj.eventId!!, position)

    }

    override fun onCallBackFaceBook(isLogin: Boolean) {
        if (isLogin)
            ivFacebook!!.setBackgroundResource(R.drawable.ic_socail_fb)
        else {
            ivFacebook!!.setBackgroundResource(R.drawable.ic_social_fb_dissconnect)
            tbFacebookConnect!!.isChecked = false
        }
    }

    override fun onCallBackTwitter(isLogin: Boolean) {
        if (isLogin)
            ivTwitter!!.setBackgroundResource(R.drawable.ic_socail_twitter)
        else {
            ivTwitter!!.setBackgroundResource(R.drawable.ic_social_twitter_dissconnect)
            tbTwitterConnect!!.isChecked = false
        }
    }

    override fun onCallBackLinkedIn(isLogin: Boolean) {}

    override fun onCallBackGooglePlus(isLogin: Boolean) {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (FacebookSdk.isFacebookRequestCode(requestCode)) {
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
        }
//        else if (TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE == requestCode) {
//            twitterAuthClient!!.onActivityResult(requestCode, resultCode, data)
//        }

    }

    private fun setBookmarkRv() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_from_right)
        val manager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding!!.rvBookmarkEvent.layoutAnimation = animation
        binding!!.rvBookmarkEvent.layoutManager = manager
        binding!!.rvBookmarkEvent.setEmptyView(binding!!.tvNoData)
        if (mlistFilterBookmark != null) {
            if (mlistFilterBookmark!!.size > 0)
                mlistFilterBookmark!!.clear()
        } else {
            mlistFilterBookmark = ArrayList()
        }
        mlistFilterBookmark!!.addAll(objectResponse!!.bookmarkedEventsList)
        adapter = AdapterBookmarkEvents(HooleyMain.activity!!, mlistFilterBookmark!!)
        adapter!!.setListener(this)
        binding!!.rvBookmarkEvent.adapter = adapter

        if (binding!!.rbAllEvent.isChecked) {
            FilterByAll()
        } else {
            FilterByPast()
        }
    }

    private fun FilterByAll() {
        if (mlistFilterBookmark != null) {
            if (mlistFilterBookmark!!.size > 0)
                mlistFilterBookmark!!.clear()
        } else {
            mlistFilterBookmark = ArrayList()
        }
        if (objectResponse == null)
            return
        for (i in objectResponse!!.bookmarkedEventsList.indices) {
            if (!objectResponse!!.bookmarkedEventsList[i].isPast)
                mlistFilterBookmark!!.add(objectResponse!!.bookmarkedEventsList[i])
        }
        if (adapter != null)
            adapter!!.notifyDataSetChanged()
    }


    private fun FilterByPast() {
        if (mlistFilterBookmark != null) {
            if (mlistFilterBookmark!!.size > 0)
                mlistFilterBookmark!!.clear()
        } else {
            mlistFilterBookmark = ArrayList()
        }
        if (objectResponse == null)
            return
        for (i in objectResponse!!.bookmarkedEventsList.indices) {
            if (objectResponse!!.bookmarkedEventsList[i].isPast)
                mlistFilterBookmark!!.add(objectResponse!!.bookmarkedEventsList[i])
        }
        if (adapter != null)
            adapter!!.notifyDataSetChanged()
    }

    private fun showShareEventDialog(bitmap: Bitmap) {
        val bitmapPath = MediaStore.Images.Media.insertImage(HooleyMain.activity!!.contentResolver, bitmap, "Thumb", null)
        val bitmapUri = Uri.parse(bitmapPath)
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "image/png"
        intent.type = "text/plain"
        val receiver = Intent(HooleyMain.activity, ApplicationSelectorReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(HooleyMain.activity, 0, receiver, PendingIntent.FLAG_UPDATE_CURRENT)
        intent.putExtra(Intent.EXTRA_STREAM, bitmapUri)
        intent.putExtra(Intent.EXTRA_TEXT,
                Constants.EVENT_SHARE + mlistFilterBookmark!![position].eventId
                        + "\n" + mlistFilterBookmark!![position].eventName
                        + "\nStart Date = " + DateUtils.formatDateNew(mlistFilterBookmark!![position].startTime!!))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            startActivityForResult(Intent.createChooser(intent, null, pendingIntent.intentSender), 7)
        } else {
            startActivityForResult(Intent.createChooser(intent, "Share"), 7)
        }
    }

    private fun showEventBookmark() {
        if (binding != null && !isSwiped) {
            binding!!.pbBookmark.visibility = View.VISIBLE
        }
        eventService.getBookMarkEvent(object : IWebServiceCallback<BookmarkEventModel> {
            override fun success(result: BookmarkEventModel) {
                if (isSwiped) {
                    binding!!.slBookmarks.isRefreshing = false
                    isSwiped = false
                }
                objectResponse = result
                binding!!.pbBookmark.visibility = View.GONE
                binding!!.tvNoData.visibility = View.GONE
                binding!!.rvBookmarkEvent.visibility = View.VISIBLE
                setBookmarkRv()
            }

            override fun failure(message: String) {
                binding!!.pbBookmark.visibility = View.GONE
                if (isSwiped) {
                    binding!!.slBookmarks.isRefreshing = false
                    isSwiped = false
                }
                try {
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.rvBookmarkEvent.visibility = View.GONE
                    binding!!.tvNoData.text = message
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    private fun likeEvent(eventId: String) {
        eventService.likeEvent(eventId, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                if (objectResponse!!.bookmarkedEventsList[position].isLiked) {
                    objectResponse!!.bookmarkedEventsList[position].isLiked = false
                    objectResponse!!.bookmarkedEventsList[position].likesCount = objectResponse!!.bookmarkedEventsList[position].likesCount - 1
                    adapter!!.notifyDataSetChanged()
                } else {
                    objectResponse!!.bookmarkedEventsList[position].isLiked = true
                    objectResponse!!.bookmarkedEventsList[position].likesCount = objectResponse!!.bookmarkedEventsList[position].likesCount + 1
                    adapter!!.notifyDataSetChanged()
                }
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    private fun followEvent(eventId: String, listPosition: Int) {
        eventService.followEvent(eventId, !mlistFilterBookmark!![listPosition].isFollowed, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                if (mlistFilterBookmark!![listPosition].isFollowed) {
                    mlistFilterBookmark!![listPosition].isFollowed = false
                    mlistFilterBookmark!![listPosition].followersCount = mlistFilterBookmark!![listPosition].followersCount - 1
                } else {
                    mlistFilterBookmark!![listPosition].isFollowed = true
                    mlistFilterBookmark!![listPosition].followersCount = mlistFilterBookmark!![listPosition].followersCount + 1
                }
                if (adapter != null)
                    adapter!!.notifyDataSetChanged()
            }

            override fun failure(message: String) {
                try {

                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    override fun onRefresh() {
        binding!!.slBookmarks.isRefreshing = true
        binding!!.pbBookmark.visibility = View.GONE
        isSwiped = true
        showEventBookmark()
    }

    companion object {

        lateinit var instance: BookmarkEventsFragment

        fun newInstance(): BookmarkEventsFragment {
            instance = BookmarkEventsFragment()
            return instance
        }
    }


}
