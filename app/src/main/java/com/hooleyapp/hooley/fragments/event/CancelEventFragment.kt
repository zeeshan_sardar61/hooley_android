package com.hooleyapp.hooley.fragments.event

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentCancelEventBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.others.TermsAndConditionFragment
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.services.EventWebService

class CancelEventFragment : BaseFragment(), View.OnClickListener {
    var mService = EventWebService()
    var binding: FragmentCancelEventBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cancel_event, container, false)
        setHasOptionsMenu(true)
        setListener()
        setData()
        return binding!!.root
    }

    fun setData() {
        (HooleyMain.activity as HooleyMain).setToolbarTitle("Cancel Event")
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
    }

    private fun setListener() {
        binding!!.btnCancelEvent.setOnClickListener(this)
        binding!!.tvTermsConditions.setOnClickListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnCancelEvent -> if (validInput())
                setUpdateEventAction()
            R.id.tvTermsConditions -> (activity as ActivityBase).callFragment(R.id.container, TermsAndConditionFragment.newInstance(true, false), "TermsAndConditionFragment")
        }
    }

    private fun validInput(): Boolean {
        if (TextUtils.isEmpty(binding!!.edtReason.text.toString())) {
            binding!!.edtReason.requestFocus()
            binding!!.edtReason.error = getString(R.string.required)
            return false
        } else {
            return true
        }
    }

    fun setUpdateEventAction() {
        HooleyMain.activity!!.showDialog()
        mService.setUpdateEventAction(eventId!!, 2, "", "", true, true, binding!!.edtReason.text.toString(),
                object : IWebServiceCallback<GeneralModel> {
                    override fun success(result: GeneralModel) {
                        HooleyMain.activity!!.removeDialog()
                        HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                        HooleyMain.activity!!.onBackPressed()
                        HooleyMain.activity!!.showSuccessDialog("Event Canceled Successfully")
                    }

                    override fun failure(message: String) {
                        HooleyMain.activity!!.removeDialog()
                        try {
                            HooleyMain.activity!!.showSuccessDialog(message)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }

                    override fun onTokenExpire() {
                        HooleyMain.activity!!.removeDialog()
                        onTokenExpireLogOut()
                    }
                })
    }

    companion object {

        lateinit var instance: CancelEventFragment
        private var isCancel: Boolean = false
        private var isPaid: Boolean = false
        private var updateActionId: Int = 0
        private var eventId: String? = null

        fun newInstance(id: String, cancel: Boolean, paid: Boolean, actionId: Int): CancelEventFragment {
            eventId = id
            updateActionId = actionId
            isPaid = paid
            isCancel = cancel
            instance = CancelEventFragment()
            return instance
        }
    }
}
