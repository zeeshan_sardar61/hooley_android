package com.hooleyapp.hooley.fragments.event

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.location.Location
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterPostMedia
import com.hooleyapp.hooley.adapters.AdapterSelectFriends
import com.hooleyapp.hooley.adapters.AdapterTags
import com.hooleyapp.hooley.apis.ApiRequestJson
import com.hooleyapp.hooley.databinding.FragmentLivePostBinding
import com.hooleyapp.hooley.db.DbManager
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.friends.SelectFriendFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.ICallBackUri
import com.hooleyapp.hooley.interfaces.IInterestDeleteClick
import com.hooleyapp.hooley.interfaces.IPostMediaClickListener
import com.hooleyapp.hooley.model.FriendsModel
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.ItemPostMedia
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.services.MediaWebService
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

/**
 * Created by Nauman on 2/19/2018.
 */

class LivePostFragment : BaseFragment(), IInterestDeleteClick, View.OnClickListener, AdapterSelectFriends.ISelectFriendListener, IPostMediaClickListener {
    var mediaArrayList: ArrayList<ItemPostMedia>? = ArrayList()
    var mediaService = MediaWebService()
    internal lateinit var binding: FragmentLivePostBinding
    private var friendList: MutableList<FriendsTable>? = null
    private var tagFrinds: ArrayList<FriendsTable> = ArrayList()
    private var adapterTags: AdapterTags? = null
    private var selectedPosition = -1
    private var service: FriendWebService? = null
    private var moreDialogeFragment: MoreDialogeFragment? = null
    private val REQUEST_CODE_TAG_FRIEND = 965
    private var selectFriendFragment: SelectFriendFragment? = null
    private var adapterPostMedia: AdapterPostMedia? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_live_post, container, false)
        setTagsFriendsAdapter()
        HooleyMain.activity!!
        setMoreCallerBackListener()
        setToolBarTitle(HooleyMain.activity!!, "Upload Media")
        if (itemPostMedia != null)
            mediaArrayList!!.add(itemPostMedia!!)
        setMediaAdapter()
        getFriendList()
//        setHasOptionsMenu(true)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //        setData();
        setListener()
    }


    private fun setListener() {
        binding.rlTagsFriends.setOnClickListener(this)
        binding.btnShare.setOnClickListener(this)
    }


    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
//        menu!!.clear()
//        inflater!!.inflate(R.menu.live_post_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.share -> {
                if (clicked == 0) {
                    if (mediaArrayList != null && mediaArrayList!!.size > 0) {
                        Util.hideKeyboard(HooleyMain.activity!!)
                        postMedia()
                    } else {
                        HooleyMain.activity!!.showSuccessDialog("Add some media")
                    }
                }
                return super.onOptionsItemSelected(item)
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun setMoreCallerBackListener() {
        val arrayList = ArrayList<String>()
        arrayList.add("Photo")
        arrayList.add("Video")
        arrayList.add("Gallery")
        moreDialogeFragment = MoreDialogeFragment.Builder(activity!!).setTitle("Select").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    moreDialogeFragment!!.dismiss()
                    HooleyMain.activity!!.startCamera(Constants.CAMERA_RQ)

                }
                1 -> {
                    moreDialogeFragment!!.dismiss()
                    HooleyMain.activity!!.startVideoCamera(Constants.VIDEO_RQ)
                }
                2 -> {
                    moreDialogeFragment!!.dismiss()
                    HooleyMain.activity!!.showGallery(Constants.IMAGE_PICKER_SELECT)
                }
            }
        }.create()

    }

    private fun getFriendList() {
        if (friendList == null)
            friendList = ArrayList()
        if (friendList!!.size > 0)
            friendList!!.clear()
        friendList = DbManager.getInstance().friendList
        if (friendList!!.size > 0) {
            //set
            getFriends(false)
        } else {
            getFriends(false)
        }
    }

    private fun setTagsFriendsAdapter() {
        if (tagFrinds.size > 0)
            tagFrinds.clear()

        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity!!, R.anim.layout_animation_from_right)
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.HORIZONTAL, false)
        binding.rvTags.layoutManager = manager
        binding.rvTags.layoutAnimation = animation
        adapterTags = AdapterTags(HooleyMain.activity!!, tagFrinds)
        adapterTags!!.setmListener(this)
        binding.rvTags.adapter = adapterTags


    }

    private fun setMediaAdapter() {
        val manager = GridLayoutManager(HooleyMain.activity, 3)
        binding.rvMedia.layoutManager = manager
        binding.rvMedia.setHasFixedSize(true)
        adapterPostMedia = AdapterPostMedia(context!!, mediaArrayList)
        adapterPostMedia!!.mlistner = this
        binding.rvMedia.adapter = adapterPostMedia
    }

    private fun validInput(): Boolean {
        if (friendList!!.size > 0) {
            for (i in friendList!!.indices) {
                if (friendList!![i].isChecked)
                    return true
            }
            return false
        } else {
            return false
        }
    }

    private fun getFriends(showDialog: Boolean) {
        if (showDialog) {
            HooleyMain.activity!!.showDialog()
        }
        service = FriendWebService()
        service!!.getMyFriendsList(object : GenericCallback<FriendsModel> {
            override fun success(result: FriendsModel) {
                HooleyMain.activity!!.removeDialog()
                if (showDialog) {
                    if (friendList == null)
                        friendList = ArrayList()
                    friendList!!.addAll(result.friendsList)
                } else {
                    if (friendList!!.size != result.friendsList.size) {
                        friendList!!.clear()
                        friendList!!.addAll(result.friendsList)
                        DbManager.getInstance().friendList = friendList
                    }
                }
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()

            }
        })
    }

    private fun postMedia() {
        if (NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            HooleyMain.activity!!.showDialog()
            mediaService.postMedia(mediaArrayList, ApiRequestJson.getInstance().uploadEventMediaDataString(eventId, binding.edtCaption.text.toString(), tagFrinds), object : GenericCallback<GeneralModel> {
                override fun success(result: GeneralModel) {
                    HooleyMain.activity!!.removeDialog()
                    HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate()
                    HooleyApp.db.putInt(Constants.SHOW_LIVE_EVENT_TAB, 0)
                    (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment.newInstance(eventId), "LiveEventFragment")
                    hideDrawer(HooleyMain.activity!!)
                }

                override fun failure(message: String) {
                    HooleyMain.activity!!.removeDialog()
                    try {
                        HooleyMain.activity!!.showSuccessDialog(message)
                    } catch (ex: NullPointerException) {
                        ex.printStackTrace()
                    }

                }
            })
        } else {
            HooleyMain.activity!!.showSuccessDialog(Constants.NO_NETWORK)
        }
    }

    override fun onClickItem(name: String, position: Int) {
        for (loop in friendList!!.indices) {
            if (tagFrinds[position].userId == friendList!![loop].userId) {
                friendList!![loop].isChecked = false
                tagFrinds.removeAt(position)
            }
        }
        adapterTags!!.notifyDataSetChanged()
    }


    private var clicked: Int = 0

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rlTagsFriends -> {
                HooleyMain.activity!!.hideKeyboard()
                clicked = 1
                HooleyApp.db.putString(Constants.CALL_TYPE, "TAG_FRIENDS")
                selectFriendFragment = SelectFriendFragment.newInstance(tagFrinds)
                selectFriendFragment!!.setTargetFragment(this@LivePostFragment, REQUEST_CODE_TAG_FRIEND)
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, selectFriendFragment!!, "selectFriendFragment")
            }
            R.id.btnShare -> if (mediaArrayList != null && mediaArrayList!!.size > 0) {
                HooleyMain.activity!!.hideKeyboard()
                postMedia()
            } else {
                HooleyMain.activity!!.showSuccessDialog("Add some media")
            }
        }
    }

    override fun onFriendSelected(position: Int) {
        selectedPosition = position
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constants.CAMERA_RQ) {
                HooleyMain.activity!!.processCapturedPhoto(object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        var exif = ExifInterface(result!!.path)
                        var latLong = FloatArray(2)
                        var hasLatLong: Boolean = exif.getLatLong(latLong)
                        if (hasLatLong) {
                            if (checkInsideLocation(latLong[0], latLong[1])) {
                                mediaArrayList!!.add(ItemPostMedia(HooleyMain.activity!!.compressFile(result), false))
                                if (adapterPostMedia != null)
                                    adapterPostMedia!!.notifyDataSetChanged()
                                Log.d("PATH", "Selected File Path:$result")
                            } else {
                                HooleyMain.activity!!.showSuccessDialog("You cannot post media until unless you are not in event")
                            }
                        } else {
                            HooleyMain.activity!!.showSuccessDialog("Please enable GeoLocation tags in Camera Setting first, then capture pictures to upload")
                        }
                    }
                })
            } else if (requestCode == Constants.VIDEO_RQ) {
                HooleyMain.activity!!.processCapturedVideo(object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        var exif = ExifInterface(result!!.path)
                        var latLong = FloatArray(2)
                        var hasLatLong: Boolean = exif.getLatLong(latLong)
                        if (hasLatLong) {
                            if (checkInsideLocation(latLong[0], latLong[1])) {
                                mediaArrayList!!.add(ItemPostMedia(result, true))
                                if (adapterPostMedia != null)
                                    adapterPostMedia!!.notifyDataSetChanged()
                            } else {
                                HooleyMain.activity!!.showSuccessDialog("You cannot post media until unless you are not in event")
                            }
                        } else {
                            HooleyMain.activity!!.showSuccessDialog("Please enable GeoLocation tags in Camera Setting first, then capture pictures to upload")
                        }

                    }

                })

            } else if (requestCode == Constants.IMAGE_PICKER_SELECT) {
                HooleyMain.activity!!.processGalleryPhoto(data!!, object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        var exif = ExifInterface(result!!.path)
                        var latLong = FloatArray(2)
                        var hasLatLong: Boolean = exif.getLatLong(latLong)
                        if (hasLatLong) {
                            if (checkInsideLocation(latLong[0], latLong[1])) {
                                if (result.toString().contains("mp4")) {
                                    mediaArrayList!!.add(ItemPostMedia(result, true))
                                    if (adapterPostMedia != null)
                                        adapterPostMedia!!.notifyDataSetChanged()
                                } else {
                                    mediaArrayList!!.add(ItemPostMedia(HooleyMain.activity!!.compressFile(result), false))
                                    if (adapterPostMedia != null)
                                        adapterPostMedia!!.notifyDataSetChanged()
                                }
                            } else {
                                HooleyMain.activity!!.showSuccessDialog("You cannot post media until unless you are not in event")
                            }
                        } else {
                            HooleyMain.activity!!.showSuccessDialog("Please enable GeoLocation tags in Camera Setting first, then capture pictures to upload")
                        }

                    }

                })
            } else if (requestCode == REQUEST_CODE_TAG_FRIEND && resultCode == Activity.RESULT_OK) {
                clicked = 0
                tagFrinds = data!!.getParcelableArrayListExtra("friendListIds")
                addingTagFriends(tagFrinds)
            }
        } else {
            clicked = 0
            Toast.makeText(HooleyMain.activity, "Action Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    private fun addingTagFriends(tags: ArrayList<FriendsTable>) {
        if (adapterTags != null)
            adapterTags!!.updateList(tags)

    }

    private fun checkInsideLocation(media_lat: Float, media_long: Float): Boolean {
        var eventLocation = Location("eventLocation")
        var mediaLocation = Location("mediaLocation")

        eventLocation.latitude = LiveEventFragment.objectResult.eventDisplayBasicInfo!!.eventLat
        eventLocation.longitude = LiveEventFragment.objectResult.eventDisplayBasicInfo!!.eventLong

        mediaLocation.latitude = media_lat.toDouble()
        mediaLocation.longitude = media_long.toDouble()
        val dist = eventLocation.distanceTo(mediaLocation)

        return dist < 150
    }

    override fun addMedia() {
        moreDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
    }

    companion object {

        lateinit var instance: LivePostFragment
        var itemPostMedia: ItemPostMedia? = null
        lateinit var eventId: String
        var eventName = ""

        fun newInstance(name: String, evntId: String, item: ItemPostMedia): LivePostFragment {
            eventName = name
            eventId = evntId
            itemPostMedia = item
            instance = LivePostFragment()
            return instance
        }
    }
}
