package com.hooleyapp.hooley.fragments.event

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.view.*
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.Tricks.ViewPagerEx
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.adapters.VPDashBoardAdapter
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventDetailModel
import com.hooleyapp.hooley.app.view.ui.activites.HooleyGuestUser
import com.hooleyapp.hooley.app.view.ui.fragments.guest.GuestUserEventDetailFragment
import com.hooleyapp.hooley.app.view.ui.fragments.guest.GuestUserEventGalleryFragment
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.services.EventWebService

class GuestUserLiveEventFragment : BaseFragment(), View.OnClickListener, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    private var adapter: VPDashBoardAdapter? = null
    private var eventWebService = EventWebService()

    private val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.extras!!.getString("tag")) {
                Constants.TYPE_EVENT_GALLERY_ALERT -> fragmentLiveEventbinding!!.ivGalleryLiveCircle.visibility = View.VISIBLE
            }
        }
    }


    fun setToolBar() {
        try {
            (HooleyGuestUser.activity as HooleyGuestUser).setToolbarTitle(objectResult.eventDisplayBasicInfo!!.eventName!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentLiveEventbinding = DataBindingUtil.inflate(inflater, R.layout.fragment_guest_user_live_event, container, false)
        setListener()
        getEventDetails()
        LocalBroadcastManager.getInstance(ActivityBase.activity).registerReceiver(mMessageReceiver,
                IntentFilter("ChatStreamMessage"))
        handleSilentPush()
        return fragmentLiveEventbinding!!.root
    }

    fun handleSilentPush() {
        if (HooleyApp.db.getBoolean(Constants.SHOW_GALLERY_ALERT, false))
            fragmentLiveEventbinding!!.ivGalleryLiveCircle.visibility = View.VISIBLE
    }

    private fun setSelectedTab() {
        when (fragmentLiveEventbinding!!.vpEventGallery.currentItem) {
            0 -> setSelectedBackground(R.id.llEventGallery)
            1 -> setSelectedBackground(R.id.llDetail)
        }
    }

    override fun onResume() {
        super.onResume()
        if (fragmentLiveEventbinding != null) {
            setSelectedTab()
        }
    }

    private fun setListener() {
        fragmentLiveEventbinding!!.llEventGallery.setOnClickListener(this)
        fragmentLiveEventbinding!!.llDetail.setOnClickListener(this)
        fragmentLiveEventbinding!!.btnRetry.setOnClickListener(this)
    }

    private fun setupViewPager(objectResult: EventDetailModel) {
        try {
            fragmentLiveEventbinding!!.rbTop.visibility = View.VISIBLE
            adapter = VPDashBoardAdapter(childFragmentManager)
            adapter!!.addFragment(GuestUserEventGalleryFragment(objectResult), "GuestUserEventGalleryFragment")
            adapter!!.addFragment(GuestUserEventDetailFragment(eventId, objectResult), "GuestUserEventDetailFragment")
            fragmentLiveEventbinding!!.vpEventGallery.adapter = adapter
            fragmentLiveEventbinding!!.vpEventGallery.setCurrentItem(1, false)
            setSelectedTab()
        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
            return
        }
    }

    private fun setUpSinglePagerItem(objectResult: EventDetailModel) {
        try {
        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
            return
        }
        fragmentLiveEventbinding!!.rbTop.visibility = View.GONE
        adapter = VPDashBoardAdapter(childFragmentManager)
        adapter!!.addFragment(GuestUserEventDetailFragment(eventId, objectResult), "GuestUserEventDetailFragment")
        fragmentLiveEventbinding!!.vpEventGallery.adapter = adapter
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.llEventGallery -> {
                fragmentLiveEventbinding!!.ivGalleryLiveCircle.visibility = View.GONE
                HooleyApp.db.putBoolean(Constants.SHOW_GALLERY_ALERT, false)
                setSelectedBackground(R.id.llEventGallery)
                fragmentLiveEventbinding!!.vpEventGallery.setCurrentItem(0, true)
            }
            R.id.llDetail -> {
                setSelectedBackground(R.id.llDetail)
                fragmentLiveEventbinding!!.vpEventGallery.setCurrentItem(1, true)
            }
            R.id.btnRetry -> getEventDetails()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
    }

    private fun setSelectedBackground(id: Int) {
        when (id) {
            R.id.llEventGallery -> {
                fragmentLiveEventbinding!!.llEventGallery.setBackgroundColor(ContextCompat.getColor(ActivityBase.activity, R.color.app_box_selected_color))
                fragmentLiveEventbinding!!.llDetail.setBackgroundColor(ContextCompat.getColor(ActivityBase.activity, R.color.app_bg_layout))
            }
            R.id.llDetail -> {
                fragmentLiveEventbinding!!.llEventGallery.setBackgroundColor(ContextCompat.getColor(ActivityBase.activity, R.color.app_bg_layout))
                fragmentLiveEventbinding!!.llDetail.setBackgroundColor(ContextCompat.getColor(ActivityBase.activity, R.color.app_box_selected_color))
            }
        }
    }

    private fun getEventDetails() {
        ActivityBase.activity.showDialog()
        eventWebService.seeEventDetail(eventId, object : IWebServiceCallback<EventDetailModel> {
            override fun success(result: EventDetailModel) {
                ActivityBase.activity.removeDialog()
                objectResult = result
                isPostAllow = objectResult.eventDisplayBasicInfo!!.isPostAllow
                setToolBar()
                fragmentLiveEventbinding!!.llRetry.visibility = View.GONE
                if (objectResult.isLive) {
                    setupViewPager(objectResult)
                } else {
                    setUpSinglePagerItem(objectResult)
                }
            }

            override fun failure(message: String) {
                ActivityBase.activity.removeDialog()
                try {
                    if (message.contains("timeout"))
                        fragmentLiveEventbinding!!.llRetry.visibility = View.VISIBLE
                    else
                        ActivityBase.activity.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }
            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })

    }

    override fun onSliderClick(slider: BaseSliderView) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {

    }

    override fun onPageScrollStateChanged(state: Int) {

    }


    companion object {

        lateinit var instance: GuestUserLiveEventFragment
        var eventId: String = ""
        var eventName = ""
        lateinit var objectResult: EventDetailModel
        var isPostAllow = false
        var fragmentLiveEventbinding: com.hooleyapp.hooley.databinding.FragmentGuestUserLiveEventBinding? = null

        fun newInstance(id: String): GuestUserLiveEventFragment {
            eventId = id
            instance = GuestUserLiveEventFragment()
            return instance
        }
    }

}
