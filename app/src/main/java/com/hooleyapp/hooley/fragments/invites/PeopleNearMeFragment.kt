package com.hooleyapp.hooley.fragments.invites


import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.app.UiModeManager.MODE_NIGHT_YES
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.content.res.Resources
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.SeekBar
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterPeopleNearMe
import com.hooleyapp.hooley.databinding.FragmentPeopleNearMeBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.hideKeyboard
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.EventInviteNearByMeModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.InviteWebService
import java.text.DecimalFormat
import java.util.*


class PeopleNearMeFragment : BaseFragment(), GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, ResultCallback<LocationSettingsResult>, View.OnClickListener, OnMapReadyCallback, CompoundButton.OnCheckedChangeListener, GoogleMap.OnMapLongClickListener {

    var mGoogleApiClient: GoogleApiClient? = null
    lateinit var mLocationRequest: LocationRequest
    lateinit var mLocationSettingsRequest: LocationSettingsRequest
    var mCurrentLocation: Location? = null
    private var mapCircle: Circle? = null
    private var circleLat: Double? = null
    private var circleLon: Double? = null
    var mRequestingLocationUpdates: Boolean? = null
    var mGooglemap: GoogleMap? = null
    lateinit var locationManager: LocationManager
    lateinit var mapFragment: SupportMapFragment
    lateinit var marker: MarkerOptions
    var ct = "CT_" + javaClass.simpleName
    internal var binding: FragmentPeopleNearMeBinding? = null
    internal var adapter: AdapterPeopleNearMe? = null
    internal var mList: ArrayList<EventInviteNearByMeModel.UserNearMe>? = ArrayList()
    internal var arrayList2 = ArrayList<EventInviteNearByMeModel.UserNearMe>()
    private var autocompleteFragment: AutocompleteSupportFragment? = null
    private var sendInvitePeopleNearbyMeFragment: SendInvitePeopleNearbyMeFragment? = null
    private val REQUEST_CODE_INVITE_PEOPLE = 963
    var service = InviteWebService()
    private var showInfoDialog: Dialog? = null
    private var currentProgress = 8046.72f
    private var list = listOf<Place.Field>()
    var intent: Intent? = null
    private val AUTOCOMPLETE_REQUEST_CODE: Int = 5007

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        try {
            if (isVisibleToUser && isResumed) {
                showInfoDialog()
            } else {
                if (showInfoDialog?.isShowing!!) {
                    showInfoDialog?.dismiss()!!
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_people_near_me, container, false)
        HooleyMain.activity!!.hideKeyboard()
        initMap()
        buildGoogleApiClient()
        createLocationRequest()
        buildLocationSettingsRequest()
        checkLocationSettings()
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPlaces()
        setListener()

    }

    fun setListener() {
        binding!!.sbGeofence.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (mapCircle != null) {
                    mapCircle!!.radius = progress.toDouble()
                }
                val f = DecimalFormat("##.00")
                currentProgress = seekBar!!.progress.toFloat()
                try {
                    if (currentProgress < 1609.344) {
                        if (progress <= 100) {
                            binding!!.tvSbProgress.text = 100.toString() + " meters"
                        } else {
                            binding!!.tvSbProgress.text = f.format(currentProgress) + " meters"
                        }
                    } else {
                        binding!!.tvSbProgress.text = "${f.format(currentProgress / 1609.344)} miles"
                    }
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                mGooglemap!!.clear()
                val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
                val animateZomm = currentZoomLevel - 5
                if (circleLat != null) {
                    mapCircle!!.radius = currentProgress.toDouble()
                    addCircle()
                    mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat!!, circleLon!!), animateZomm))
                    mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
                    getEventInviteNearByMe(false)
                } else {
                    Util.showToastMessage(HooleyMain.activity!!, "Please turn on your location!")
                    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        settingsRequest()
                    }
                }
            }
        })
        binding!!.btnFindPeople.setOnClickListener(this)
        binding!!.ivMapInfo.setOnClickListener(this)
        binding!!.ivMinus.setOnClickListener(this)
        binding!!.ivPlus.setOnClickListener(this)
        binding!!.ivMyLocation.setOnClickListener(this)

        binding!!.ivMinus.setOnLongClickListener {
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        if (binding!!.ivMinus.isPressed) {
                            if (currentProgress > 100.0f) {
                                var temp = currentProgress
                                temp -= 100f
                                if (temp <= 100.0f) {
                                    currentProgress = 100.0f
                                } else {
                                    currentProgress -= 100f
                                }
                                binding!!.sbGeofence.progress = currentProgress.toInt()
                            }
                        } else {
                            timer.cancel()
                            updateUI()
                        }
                    } else {
                        settingsRequest()
                        Util.showToastMessage(HooleyMain.activity!!, "Please turn on your location!")
                    }
                }
            }, 100, 200)
            true
        }

        binding!!.ivPlus.setOnLongClickListener {
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        if (binding!!.ivPlus.isPressed) {
                            if (currentProgress < 64373) {
                                currentProgress += 100f
                                binding!!.sbGeofence.progress = currentProgress.toInt()
                            }
                        } else {
                            timer.cancel()
                            updateUI()
                        }
                    } else {
                        settingsRequest()
                        Util.showToastMessage(HooleyMain.activity!!, "Please turn on your location!")
                    }
                }
            }, 100, 200)

            true
        }
    }

    private fun updateUI() {
        Handler(Looper.getMainLooper()).post {
            if (mapCircle != null) {
                if (currentProgress > 100) {
                    mapCircle!!.radius = currentProgress.toDouble()
                    val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
                    val animateZomm = currentZoomLevel + 5
                    mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat!!, circleLon!!), animateZomm))
                    mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
                }
            }
        }
    }

    private fun initPlaces() {
        list = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)
        binding!!.tvLocationName.setOnClickListener {
            try {
                intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, list).build(HooleyMain.activity!!)
                HooleyMain.activity!!.startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }

        }
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(HooleyMain.activity!!)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (mGoogleApiClient != null) {
                settingsRequest()
            } else {
                buildGoogleApiClient()
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mGooglemap = googleMap
        mGooglemap!!.setOnMapLongClickListener(this)
        if (HooleyApp.db.getInt(Constants.TYPE_THEME) == MODE_NIGHT_YES) {
            try {
                // Customise the styling of the base map using a JSON object defined
                // in a raw resource file.
                val success = mGooglemap!!.setMapStyle(MapStyleOptions.loadRawResourceStyle(HooleyMain.activity!!, R.raw.black_map))
                if (!success) {
                    Log.e("", "Style parsing failed.")
                }
            } catch (e: Resources.NotFoundException) {
                Log.e("", "Can't find style. Error: ", e)
            }

        } else {
            try {
                // Customise the styling of the base map using a JSON object defined
                // in a raw resource file.
                val success = mGooglemap!!.setMapStyle(MapStyleOptions.loadRawResourceStyle(HooleyMain.activity!!, R.raw.white_map))
                if (!success) {
                    Log.e("", "Style parsing failed.")
                }
            } catch (e: Resources.NotFoundException) {
                Log.e("", "Can't find style. Error: ", e)
            }
        }

        if (ActivityCompat.checkSelfPermission(HooleyMain.activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(HooleyMain.activity!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        mGooglemap!!.isMyLocationEnabled = false

    }

    protected fun checkLocationSettings() {
        val result = LocationServices.SettingsApi.checkLocationSettings(
                mGoogleApiClient,
                mLocationSettingsRequest
        )
        result.setResultCallback(this)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(HooleyMain.activity!!, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                            if (mGoogleApiClient != null) {
                                settingsRequest()
                            } else {
                                buildGoogleApiClient()
                            }
                        }
                        mGooglemap!!.isMyLocationEnabled = false
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(activity, "permission denied", Toast.LENGTH_LONG).show()
                }
                return
            }
        }// other 'case' lines to check for other
        // permissions this app might request
    }

    fun googleServicesAvailable(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val isAvailable = apiAvailability.isGooglePlayServicesAvailable(context!!)
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true
        } else if (apiAvailability.isUserResolvableError(isAvailable)) {
            val dialog = apiAvailability.getErrorDialog(activity, isAvailable, 0)
            dialog.show()
        } else {
            Toast.makeText(activity, "Cant connect ", Toast.LENGTH_SHORT).show()
        }
        return false
    }

    private fun initMap() {
        locationManager = HooleyMain.activity!!.getSystemService(LOCATION_SERVICE) as LocationManager
        if (!googleServicesAvailable()) {
            Toast.makeText(activity, "google service not available", Toast.LENGTH_SHORT).show()
            return
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission()
        } else {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (mGoogleApiClient != null) {
                    settingsRequest()
                } else {
                    buildGoogleApiClient()
                }
            }
        }
        mapFragment = childFragmentManager.findFragmentById(R.id.mpGeofence) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    fun checkLocationPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(HooleyMain.activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(HooleyMain.activity!!, Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(HooleyMain.activity!!, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        MY_PERMISSIONS_REQUEST_LOCATION)
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(HooleyMain.activity!!, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        MY_PERMISSIONS_REQUEST_LOCATION)
            }
            return false
        } else {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (mGoogleApiClient != null) {
                    settingsRequest()
                } else {
                    buildGoogleApiClient()
                }
            }
            return true
        }
    }

    override fun onResult(locationSettingsResult: LocationSettingsResult) {
        val status = locationSettingsResult.status
        Log.d(ct, "onResult-> " + status.statusCode)
        Log.i(ct, "onResult-> " + status.statusMessage!!)
        when (status.statusCode) {
            LocationSettingsStatusCodes.CANCELED -> Log.i(ct, "Location handling cancelled!")
            LocationSettingsStatusCodes.SUCCESS -> {
                Log.i(ct, "All location settings are satisfied.")
                startLocationUpdates()
            }
            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                Log.i(ct, "Location settings are not satisfied. Show the user a dialog to" + "upgrade location settings ")

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS)
                } catch (e: IntentSender.SendIntentException) {
                    Log.i(ct, "PendingIntent unable to execute request.")
                }

            }
            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(ct, "Location settings are inadequate, and cannot be fixed here. Dialog " + "not created.")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(ct, "onActivityResult-> $resultCode")
        when (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            REQUEST_CHECK_SETTINGS -> when (resultCode) {
                Activity.RESULT_OK -> {
                    Log.i(ct, "User agreed to make required location settings changes.")
                    startLocationUpdates()
                }
                Activity.RESULT_CANCELED -> {
                    Toast.makeText(context, "Cannot use Repairer until you allow to provide your location!", Toast.LENGTH_SHORT).show()
                    Log.i(ct, "User chose not to make required location settings changes.")
                }
            }
            AUTOCOMPLETE_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val place: Place = Autocomplete.getPlaceFromIntent(data!!)
                    Log.i("TAG", "Place: " + place.name + ", " + place.id)
                    circleLat = place.latLng!!.latitude
                    circleLon = place.latLng!!.longitude
                    binding!!.tvLocationName.text = (HooleyMain.activity as ActivityBase).getCompleteAddressString(circleLat!!, circleLon!!)
                    val loc = LatLng(circleLat!!, circleLon!!)
                    if (mGooglemap != null) {
                        mGooglemap!!.clear()
                        addCircle()

                    }
                } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    // TODO: Handle the error.
                    val status: Status = Autocomplete.getStatusFromIntent(data!!)
                    Log.i("TAG", status.statusMessage)
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    // The user canceled the operation.
                }

            }
        }
    }

    protected fun startLocationUpdates() {
        /*ACCESS_COARSE_LOCATION, Manifest.permission.CAMERA*/
        if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    mLocationRequest,
                    this
            ).setResultCallback { status ->
                mRequestingLocationUpdates = true
                if (status.isSuccess) {
                    Log.i(ct, "Status true! Going to load!")
                } else {
                    Log.e(ct, "There is a problem")
                }
                getLastLocation()
            }
        }
    }

    fun moveToCurrentLocation() {
        if (mCurrentLocation == null) {
            if (HooleyMain.activity != null) {
                if (ActivityCompat.checkSelfPermission(HooleyMain.activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return
                }
            }
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
            if (mCurrentLocation != null) {
                circleLat = mCurrentLocation!!.latitude
                circleLon = mCurrentLocation!!.longitude
                val loc = LatLng(mCurrentLocation!!.latitude, mCurrentLocation!!.longitude)
                if (mGooglemap != null) {
                    mGooglemap!!.clear()
                    mGooglemap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f))
                    addCircle()
                    getEventInviteNearByMe(false)
                }
            }

        } else {
            circleLat = mCurrentLocation!!.latitude
            circleLon = mCurrentLocation!!.longitude
            val loc = LatLng(mCurrentLocation!!.latitude, mCurrentLocation!!.longitude)
            if (mGooglemap != null) {
                mGooglemap!!.clear()
                mGooglemap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f))
                addCircle()
                getEventInviteNearByMe(false)
            }
        }
        try {
            binding!!.tvLocationName.text = (HooleyMain.activity as ActivityBase).getCompleteAddressString(circleLat!!, circleLon!!)
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

    }

    fun addCircle() {
        mGooglemap!!.clear()
        mGooglemap!!.uiSettings.isZoomGesturesEnabled = true
        mGooglemap!!.uiSettings.setAllGesturesEnabled(true)
        if (mapCircle != null) {
            mapCircle!!.remove()
        }

        val iMeter = currentProgress

        mapCircle = mGooglemap!!.addCircle(CircleOptions()
                .center(LatLng(circleLat!!, circleLon!!))
                .radius(iMeter.toDouble())
                .fillColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_light_color))
                .strokeColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.purple_color))
                .strokeWidth(2f)
                .clickable(true))
        mapCircle!!.isVisible = true

        val markerOption = MarkerOptions().position(LatLng(circleLat!!, circleLon!!)).flat(true).draggable(false)
        markerOption.icon(bitmapDescriptorFromVector(HooleyMain.activity, R.drawable.ic_map_cirlce_centre))
        mGooglemap!!.addMarker(markerOption)
    }

    fun getLastLocation() {
        if (mRequestingLocationUpdates!!) {
            if (mCurrentLocation != null) {
                Log.e(ct, "getLastLocation-> Now fetched!")
                Log.d(ct, "getLastLocation-> Lat is: " + mCurrentLocation!!.latitude)
                Log.d(ct, "getLastLocation-> Lon is: " + mCurrentLocation!!.longitude)
                stopLocationUpdates()
            } else {
                Log.i(ct, "getLastLocation-> Cannot fetch! (mCurrentLocation)")
            }
        } else {
            Log.i(ct, "getLastLocation-> Cannot fetch! (mRequestingLocationUpdates)")
        }
    }

    protected fun stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        if (!mGoogleApiClient!!.isConnected)
            return
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient!!,
                this
        ).setResultCallback { status ->
            mRequestingLocationUpdates = false
            if (status.isSuccess) {
                Log.i(ct, "stopLocationUpdates-> Status true! Going to load!")
            } else {
                Log.e(ct, "stopLocationUpdates-> There is a problem")
            }
        }
    }

    fun settingsRequest() {
        try {
            mLocationRequest = LocationRequest.create()
            mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            mLocationRequest.interval = (30 * 1000).toLong()
            mLocationRequest.fastestInterval = (5 * 1000).toLong()
            val builder = LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest)
            mLocationSettingsRequest = builder.build()
            builder.setAlwaysShow(true) //this is the key ingredient
            val result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient!!, mLocationSettingsRequest)
            result.setResultCallback { result ->
                val status = result.status
                val state = result.locationSettingsStates
                when (status.statusCode) {
                    LocationSettingsStatusCodes.SUCCESS -> {
                        startLocationUpdates()
                        createLocationRequest()
                        buildLocationSettingsRequest()
                        checkLocationSettings()
                    }
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                        status.startResolutionForResult(HooleyMain.activity, REQUEST_CHECK_SETTINGS)
                    } catch (e: IntentSender.SendIntentException) {
                    }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                    }
                }
            }
        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
        }

    }

    protected fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    protected fun buildLocationSettingsRequest() {
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        mLocationSettingsRequest = builder.build()
    }

    private fun bitmapDescriptorFromVector(context: Context?, vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context!!, vectorResId)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    override fun onStart() {
        super.onStart()
        mGoogleApiClient!!.connect()
    }

    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient!!.isConnected) {
            mGoogleApiClient!!.disconnect()
        }
    }

    override fun onResume() {
        super.onResume()
        // Within {@code onPause()}, we pause location updates, but leave the
        // connection to GoogleApiClient intact.  Here, we resume receiving
        // location updates if the user has requested them.
        if (mGoogleApiClient!!.isConnected) {
            startLocationUpdates()
        }
        if (isVisible) {
            if (mGooglemap != null)
                mGooglemap!!.clear()
            binding!!.sbGeofence.progress = 8046
            binding!!.tvSbProgress.text = "5 miles"
        }
    }

    override fun onConnected(bundle: Bundle?) {
        // Provides a simple way of getting a device's location and is well suited for
        // applications that do not require a fine-grained location and that do not need location
        // updates. Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.
        if (mGooglemap != null)
            mGooglemap!!.uiSettings.isMyLocationButtonEnabled = false
        moveToCurrentLocation()
    }

    override fun onConnectionSuspended(i: Int) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i(ct, "Connection suspended")
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(ct, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.errorCode)
    }

    override fun onLocationChanged(location: Location) {
        mCurrentLocation = location
        getLastLocation()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivMyLocation -> moveToCurrentLocation()
            R.id.btnFindPeople -> if (mList != null && mList!!.size > 0) {
                sendInvitePeopleNearbyMeFragment = SendInvitePeopleNearbyMeFragment.newInstance(mList!!)
                sendInvitePeopleNearbyMeFragment!!.setTargetFragment(InviteFragment.instance, REQUEST_CODE_INVITE_PEOPLE)
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, sendInvitePeopleNearbyMeFragment!!, "SendInvitePeopleNearbyMeFragment")
            }
            R.id.ivDrawCircle -> {
                mGooglemap!!.clear()
                addCircle()
                val loc = LatLng(circleLat!!, circleLon!!)
                if (mGooglemap != null) {
                    mGooglemap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, mGooglemap!!.cameraPosition.zoom))
                }
            }
            R.id.ivMapInfo -> showInfoDialog()
            R.id.ivMinus -> if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (currentProgress >= 100.0f) {
                    var temp = currentProgress
                    temp -= 100f
                    if (temp <= 100.0f) {
                        currentProgress = 100.0f
                    } else {
                        currentProgress -= 100f
                    }
                    binding!!.sbGeofence.progress = (currentProgress).toInt()
                    addCircle()
                    getEventInviteNearByMe(false)
                    updateUI()
                }
            } else {
                settingsRequest()
                Util.showToastMessage(HooleyMain.activity!!, "Please turn on your location!")
            }
            R.id.ivPlus -> if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (currentProgress < 64373.0f) {
                    currentProgress += 100f
                    binding!!.sbGeofence.progress = (currentProgress).toInt()
                    addCircle()
                    getEventInviteNearByMe(false)
                    updateUI()
                }
            } else {
                settingsRequest()
                Util.showToastMessage(HooleyMain.activity!!, "Please turn on your location!")
            }
        }//showPeopleNearByMeDailog(mList);
    }

    private fun showInfoDialog() {

        binding!!.llHelp.visibility = View.VISIBLE
//        showInfoDialog = Dialog(HooleyMain.activity!!)
//        showInfoDialog!!.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
//        showInfoDialog!!.setContentView(R.layout.dialog_map_info)
//        val ivClose = showInfoDialog!!.findViewById<ImageView>(R.id.ivClose)
//        val tvText = showInfoDialog!!.findViewById<TextView>(R.id.tvText)
        binding!!.tvText.text = HooleyMain.activity!!.getString(R.string.str_map_invite)
        binding!!.ivClose.setOnClickListener {
            binding!!.llHelp.visibility = View.GONE
        }
//        showInfoDialog!!.show()
    }

    private fun getEventInviteNearByMe(showDialog: Boolean) {
        if (showDialog)
            HooleyMain.activity!!.showDialog()
        service.getEventNearByMe(circleLat, circleLon, currentProgress / 1609.344f, object : GenericCallback<EventInviteNearByMeModel> {
            override fun success(result: EventInviteNearByMeModel) {
                HooleyMain.activity!!.removeDialog()
                if (mList!!.size > 0)
                    mList!!.clear()
                mList!!.addAll(result.userNearMeList)
                setMarker(mList)
            }

            override fun failure(message: String) {
                HooleyMain.activity!!.removeDialog()
                try {
                    if (mList!!.size > 0)
                        mList!!.clear()
                    setMarker(mList)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }


            }
        })
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {}

    override fun onMapLongClick(latLng: LatLng) {

        circleLat = latLng.latitude
        circleLon = latLng.longitude
        try {
            binding!!.tvLocationName.text = (HooleyMain.activity as ActivityBase).getCompleteAddressString(circleLat!!, circleLon!!)
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

        addCircle()
        getEventInviteNearByMe(false)
        mGooglemap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11.5f))

    }

    fun setMarker(arrayList: ArrayList<EventInviteNearByMeModel.UserNearMe>?) {
        // Adding marker
        if (arrayList!!.size > 0 && mGooglemap != null) {
            for (i in arrayList.indices) {
                val loc = LatLng(arrayList[i].latitude!!, arrayList[i].longitude!!)
                marker = MarkerOptions().position(loc).flat(false).draggable(false).title(arrayList[i].fullName)
                marker.icon(bitmapDescriptorFromVector(HooleyMain.activity, R.drawable.ic_white_marker))
                mGooglemap!!.addMarker(marker)
            }
        } else {
            mGooglemap!!.clear()
        }
    }

    companion object {
        protected val REQUEST_CHECK_SETTINGS = 0x1
        val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 1000
        val MY_PERMISSIONS_REQUEST_LOCATION = 99
        val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2

        fun newInstance(): PeopleNearMeFragment {
            val fragment = PeopleNearMeFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }


}
