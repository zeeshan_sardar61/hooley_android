package com.hooleyapp.hooley.fragments.auth

import android.annotation.TargetApi
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatDelegate
import android.text.InputType
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyAuth
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.FragmentPhoneAlreadyExistBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.load
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.LoginModel
import com.hooleyapp.hooley.model.staticData.StaticDataModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.services.AuthWebService
import java.lang.Double
import java.util.*

class PhoneAlreadyExistFragment : BaseFragment(), CompoundButton.OnCheckedChangeListener {

    internal lateinit var binding: FragmentPhoneAlreadyExistBinding
    private var gson = Gson()
    lateinit var notificationManager: NotificationManager
    var service = AuthWebService()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_phone_already_exist, container, false)
        setData()
        setListener()
        return binding.root
    }


    private fun loginUser() {
        if (!NetworkUtil.isInternetConnected(HooleyAuth.activity)) {
            HooleyAuth.activity.showSuccessDialog(Constants.NO_NETWORK)
            return
        }
        HooleyAuth.activity.showDialog()
        service.loginUser(email, binding.edPass.text.toString(), object : GenericCallback<LoginModel> {
            override fun success(result: LoginModel) {
                try {
                    HooleyApp.db.putBoolean(Constants.IS_FIRST_LOGIN, result.isFirstLogin)
                    HooleyApp.db.putBoolean(Constants.IS_PHONE_VERIFIED, result.generalProfile!!.userInfo.isPhoneVerified)
                    HooleyApp.db.putString(Constants.VERIFIED_PHONE_NUMBER, result.generalProfile!!.userInfo.countryCode + result.generalProfile!!.userInfo.phoneNo)
                    HooleyApp.db.putString(Constants.COUNTRY_CODE, result.generalProfile!!.userInfo.countryCode!!)
                    HooleyApp.db.putBoolean(Constants.IS_BUSINESS_PROFILE_CREATED, result.isBusinessProfile)
                    HooleyApp.db.putBoolean(Constants.IS_WALLET_SETUP, result.isWalletSetup)
                    HooleyApp.db.putString(Constants.USER_ID, result.userId!!)
                    HooleyApp.db.putString(Constants.AUTH_TOKKEN, result.userToken!!)
                    HooleyApp.db.putString(Constants.EMAIL_ID, email)
                    HooleyApp.db.putString(Constants.FULL_NAME, result.generalProfile!!.userInfo.firstName + " " + result.generalProfile!!.userInfo.lastName)
                    if (!TextUtils.isEmpty(result.generalProfile!!.userInfo.profilePicture))
                        HooleyApp.db.putString(Constants.USER_AVATAR, result.generalProfile!!.userInfo.profilePicture!!)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

                if (result.generalProfile!!.userGeofence.size > 0) {
                    if (result.generalProfile!!.userGeofence.size > 1) {
                        val listLatLong = ArrayList<LatLng>()
                        for (i in result.generalProfile!!.userGeofence.indices) {
                            listLatLong.add(LatLng(Double.parseDouble(result.generalProfile!!.userGeofence[i].latitude), Double.parseDouble(result.generalProfile!!.userGeofence[i].longitude)))
                        }
                        val centerLatLngObj = (HooleyMain.activity as ActivityBase).computeCentroid(listLatLong)
                        HooleyApp.db.putDouble(Constants.PERSONAL_LAT, centerLatLngObj.latitude)
                        HooleyApp.db.putDouble(Constants.PERSONAL_LANG, centerLatLngObj.latitude)
                        HooleyApp.db.putString(Constants.PERSONAL_RADIUS, result.generalProfile!!.userGeofence[0].radius!!)
                    } else {
                        HooleyApp.db.putDouble(Constants.PERSONAL_LAT, java.lang.Double.parseDouble(result.generalProfile!!.userGeofence[0].latitude))
                        HooleyApp.db.putDouble(Constants.PERSONAL_LANG, java.lang.Double.parseDouble(result.generalProfile!!.userGeofence[0].longitude))
                        HooleyApp.db.putString(Constants.PERSONAL_RADIUS, result.generalProfile!!.userGeofence[0].radius!!)
                    }
                }
                if (result.isDark) {
                    HooleyApp.db.putInt(Constants.TYPE_THEME, AppCompatDelegate.MODE_NIGHT_YES)
//                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
//                    HooleyAuth.activity.delegate.setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES)

                } else {
                    HooleyApp.db.putInt(Constants.TYPE_THEME, AppCompatDelegate.MODE_NIGHT_NO)
//                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
//                    HooleyAuth.activity.delegate.setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                }
                getStaticData(result)

            }

            override fun failure(message: String) {
                HooleyAuth.activity.removeDialog()
                try {
                    HooleyAuth.activity.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }


    fun performAction(result: LoginModel) {
        if (!result.isFirstLogin) {
            (activity as ActivityBase).callFragmentWithReplace(R.id.authContainer, ProfileSetupFragment.newInstance(), "ProfileSetupFragment")
        } else {
            val intent = Intent(activity, HooleyMain::class.java)
            startActivity(intent)
            activity!!.finish()
        }
    }


    private fun getStaticData(result: LoginModel) {
        notificationManager = HooleyAuth.activity.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
        service.getStaticData(result.userId!!, "2", object : GenericCallback<StaticDataModel> {
            override fun success(data: StaticDataModel) {
                HooleyAuth.activity.removeDialog()
                Log.d("okTAG", gson.toJson(data))
                HooleyApp.db.putString(Constants.STATIC_MODEL, gson.toJson(data))
                performAction(result)
            }

            override fun failure(message: String) {
                HooleyAuth.activity.removeDialog()
                try {
                    HooleyAuth.activity.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }
            }
        })
    }

    private fun setData() {
        binding.tvUserPhone.text = phone
        binding.ivAvatar.load(profile_pic)
        binding.tvUserName.text = name
        binding.tvUserEmail.text = email
    }

    private fun setListener() {
        binding.btnLogin.setOnClickListener {
            if (binding.edPass.text.toString().isNotEmpty()) {
                loginUser()
            } else {
                HooleyAuth.activity.showSuccessDialog("Please enter your password")
            }
        }

        binding.ivBack.setOnClickListener {
            HooleyAuth.activity.onBackPressed()
        }
        binding.btnTry.setOnClickListener {
            HooleyAuth.activity.onBackPressed()
        }
        binding.cbViewPass.setOnCheckedChangeListener(this)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        if (isChecked) {
            binding.cbViewPass.backgroundTintList = ContextCompat.getColorStateList(HooleyAuth.activity, R.color.app_black_color)
            binding.edPass.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
        } else {
            binding.cbViewPass.backgroundTintList = ContextCompat.getColorStateList(HooleyAuth.activity, R.color.app_gray_color)
            binding.edPass.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        }
        binding.edPass.setSelection(binding.edPass.length())
    }


    companion object {

        lateinit var instance: PhoneAlreadyExistFragment
        var email: String = ""
        var name: String = ""
        var profile_pic: String = ""
        var phone: String = ""

        fun newInstance(phone_: String, profile_pic_: String, name_: String, email_: String): PhoneAlreadyExistFragment {
            instance = PhoneAlreadyExistFragment()
            phone = phone_
            profile_pic = profile_pic_
            name = name_
            email = email_
            return instance
        }
    }
}