package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.AdapterInterestBinding
import java.util.*

/**
 * Created by Zeeshan on 23-Jan-18.
 */

class AdapterInterest(private val context: Context, internal var mList: ArrayList<String>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<AdapterInterestBinding>(LayoutInflater.from(parent.context), R.layout.adapter_interest, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        holder.binding!!.tvInterest.text = mList!![position]
    }

    override fun getItemCount(): Int {
        return if (mList == null) 0 else mList!!.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterInterestBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

}
