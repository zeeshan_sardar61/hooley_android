package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.AdapterTagsFriendsBinding
import com.hooleyapp.hooley.interfaces.IInterestDeleteClick
import com.hooleyapp.hooley.tables.FriendsTable

class AdapterTags(private val context: Context, var mList: ArrayList<FriendsTable>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var binding: AdapterTagsFriendsBinding
    private var mListener: IInterestDeleteClick? = null

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_tags_friends, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as AdapterTags.ItemViewHolder

        if (!TextUtils.isEmpty(mList[position].profilePic))
            Glide.with(context).load(mList[position].profilePic).into(holder.binding!!.ivFriendAvatar)
        else
            Glide.with(context).load(R.drawable.ic_avatar_place_holder).into(holder.binding!!.ivFriendAvatar)

        holder.binding!!.ivClose.setOnClickListener {
            mListener!!.onClickItem(mList[position].fullName, position)
        }

    }

    fun setmListener(listener: IInterestDeleteClick) {
        this.mListener = listener
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterTagsFriendsBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

    fun updateList(friendList: ArrayList<FriendsTable>) {
        mList = friendList
        notifyDataSetChanged()
    }

}