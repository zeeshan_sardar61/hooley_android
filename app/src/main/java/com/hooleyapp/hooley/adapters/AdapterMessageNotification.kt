package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterNotificationEventAlertBinding
import com.hooleyapp.hooley.fragments.messaging.ChatWindowFragment
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.model.MessageAlertNotificationModel
import com.hooleyapp.hooley.others.DateUtils
import java.util.*

/**
 * Created by Nauman on 4/11/2018.
 */

class AdapterMessageNotification
//    private IBookmarkEventsClickListner mListener;


(private val context: Context, mList: ArrayList<MessageAlertNotificationModel.MessageNotification>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal var mList = ArrayList<MessageAlertNotificationModel.MessageNotification>()

    init {
        this.mList = mList
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<AdapterNotificationEventAlertBinding>(LayoutInflater.from(parent.context), R.layout.adapter_notification_event_alert, parent, false)
        return ItemViewHolder(binding.root)
    }


    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        if (!TextUtils.isEmpty(mList[position].profilePic))
            Glide.with(context).load(mList[position].profilePic).into(holder.binding!!.ivEventAvatar)
        holder.binding!!.tvPeopleCount.text = DateUtils.convertDateToTimeAgo(DateUtils.getLocalDate(mList[position].sentTime!!))
        holder.binding!!.tvTimeStamp.visibility = View.GONE
        holder.binding!!.tvText.visibility = View.GONE
        holder.binding!!.tvTitle.text = (HooleyMain.activity as ActivityBase).noTrailingwhiteLines(Html.fromHtml(mList[position].bodyText))
        if (!mList[position].isSeen)
            holder.binding!!.rlContainer.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.unseen_color))
        else
            holder.binding!!.rlContainer.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.base_color))
        holder.binding!!.ivEventAvatar.setOnClickListener {
            var avatarfragment = ViewAvatarFragment(mList[position].friendId!!.toInt())
            avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
        }
        holder.binding!!.llContainer.setOnClickListener { (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ChatWindowFragment("", mList[position].friendId.toString(), mList[position].fullName!!), "ChatWindowFragment") }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterNotificationEventAlertBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }
}