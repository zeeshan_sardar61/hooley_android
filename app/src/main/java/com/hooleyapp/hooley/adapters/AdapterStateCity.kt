package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ExpandableListView
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.ItemChildStateCityBinding
import com.hooleyapp.hooley.databinding.ItemParentStateCityBinding
import com.hooleyapp.hooley.model.GetCityStateModel
import java.util.*

/**
 * Created by Nauman on 5/3/2018.
 */

class AdapterStateCity(private val elvStateCity: ExpandableListView, private val context: Context, private val mlist: ArrayList<GetCityStateModel.StateCity>) : BaseExpandableListAdapter() {
    lateinit var childBinding: ItemChildStateCityBinding
    lateinit var parentBinding: ItemParentStateCityBinding
    lateinit var mListener: IStateCityClickListener

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return mlist[groupPosition].citiesList[childPosition]

    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getChildView(groupPosition: Int, childPosition: Int,
                              isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        childBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_child_state_city, parent, false)
        convertView = childBinding.root
        convertView.tag = childBinding
        childBinding.tvCityName.text = mlist[groupPosition].citiesList[childPosition].name
        childBinding.cbChildCheck.isChecked = mlist[groupPosition].citiesList[childPosition].isChecked
        childBinding.cbChildCheck.setOnCheckedChangeListener { buttonView, isChecked -> mListener.onChildCheckChange(isChecked, groupPosition, childPosition) }
        return convertView

    }


    override fun getChildrenCount(groupPosition: Int): Int {
        return mlist[groupPosition].citiesList.size
    }

    override fun getGroup(groupPosition: Int): Any {
        return mlist[groupPosition]
    }

    override fun getGroupCount(): Int {
        return mlist.size
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean,
                              convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        parentBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_parent_state_city, parent, false)
        convertView = parentBinding.root
        convertView.tag = parentBinding
        parentBinding.tvState.text = mlist[groupPosition].name
        parentBinding.cbSelectState.isChecked = mlist[groupPosition].isChecked
        parentBinding.tvCityCount.text = mlist[groupPosition].citiesList.size.toString() + " Cities"
        if (!elvStateCity.isGroupExpanded(groupPosition)) {
            parentBinding.ivExpandEvent.visibility = View.VISIBLE
            parentBinding.ivCollapseEvent.visibility = View.GONE
        } else {
            parentBinding.ivExpandEvent.visibility = View.GONE
            parentBinding.ivCollapseEvent.visibility = View.VISIBLE
        }
        parentBinding.cbSelectState.setOnCheckedChangeListener { _, isChecked -> mListener.onParentCheckChange(isChecked, groupPosition) }
        parentBinding.ivCollapseEvent.setOnClickListener {
            parentBinding.ivCollapseEvent.visibility = View.GONE
            parentBinding.ivExpandEvent.visibility = View.VISIBLE
            mListener.onClickParent(true, groupPosition)
        }
        parentBinding.ivExpandEvent.setOnClickListener {
            parentBinding.ivExpandEvent.visibility = View.GONE
            parentBinding.ivCollapseEvent.visibility = View.VISIBLE
            mListener.onClickParent(false, groupPosition)
        }
        return convertView

    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    fun setListener(listener: IStateCityClickListener) {
        this.mListener = listener
    }

    interface IStateCityClickListener {
        fun onClickParent(isExpanded: Boolean, position: Int)

        fun onParentCheckChange(isChecked: Boolean, position: Int)

        fun onChildCheckChange(isChecked: Boolean, groupPosition: Int, childPosition: Int)

    }
}

