package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventDetailModel
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.databinding.AdapterCommentsBinding
import com.hooleyapp.hooley.databinding.LayoutCommentHeaderBinding
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

/**
 * Created by Nauman on 4/13/2018.
 */

class AdapterPostComment(private val context: Context, internal var mList: ArrayList<EventDetailModel.Comments>?, private val imageInfoModel: EventGalleryFeedModel.EventMediaInfo, private val tagFriendsList: ArrayList<FriendsTable>) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), AdapterPostMediaPager.IClickPostListener {
    lateinit var mlistener: CommentClickListener
    private var adapterTags: AdapterFriendsTags? = null
    private var adapter: AdapterPostMediaPager? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == 0) {
            val binding = DataBindingUtil.inflate<LayoutCommentHeaderBinding>(LayoutInflater.from(parent.context), R.layout.layout_comment_header, parent, false)
            return LayoutHeaderViewHolder(binding.root)
        } else {
            val binding = DataBindingUtil.inflate<AdapterCommentsBinding>(LayoutInflater.from(parent.context), R.layout.adapter_comments, parent, false)
            return ItemViewHolder(binding.root)
        }
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {

        if (position == 0) {
            val layoutHeaderViewHolder = mHolder as LayoutHeaderViewHolder
            setData(layoutHeaderViewHolder)
        } else {

            val holder = mHolder as ItemViewHolder
            if (!TextUtils.isEmpty(mList!![position].profilePic))
                Glide.with(context).load(mList!![position].profilePic).into(holder.binding!!.ivUserProfile)
            holder.binding!!.ivUserProfile.setOnClickListener {
                var avatarfragment = ViewAvatarFragment(mList!![position].userId!!.toInt())
                avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
            }
            holder.binding!!.tvUserName.text = mList!![position].fullName
            holder.binding!!.tvComment.text = mList!![position].commentText
            if (mList!![position].commentTime!! != "Now")
                holder.binding!!.tvCommentDate.text = DateUtils.convertSingleDate(DateUtils.getLocalDate(mList!![position].commentTime!!))
            else
                holder.binding!!.tvCommentDate.text = "Just Now"
        }
    }


    override fun getItemViewType(position: Int): Int {
        return if (position == 0) 0 else 1
    }


    override fun getItemCount(): Int {
        return if (mList == null) 0 else mList!!.size

    }

    override fun onClickPost(position: Int, listPosition: Int) {
        //        if (imageInfoModel.isVideo)
        //            ((ActivityBase) HooleyMain.activity).addFragment(R.id.container, ViewPostMediaFragment.newInstance(imageInfoModel), "VideoPlayerActivity");
        //        else
        //            ((ActivityBase) HooleyMain.activity).addFragment(R.id.container, ViewPostMediaFragment.newInstance(imageInfoModel), "ViewPostMediaFragment");
        //
        mlistener.onClickImage()
    }

    private fun setData(layoutHeaderViewHolder: LayoutHeaderViewHolder) {
        if (!TextUtils.isEmpty(imageInfoModel.profilePic))
            Glide.with(HooleyMain.activity!!).load(imageInfoModel.profilePic).into(layoutHeaderViewHolder.binding!!.ivUserProfile)
        layoutHeaderViewHolder.binding!!.tvFullName.text = imageInfoModel.fullName
        if (!TextUtils.isEmpty(imageInfoModel.imageCaption)) {
            layoutHeaderViewHolder.binding!!.tvPostDetail.visibility = View.VISIBLE
            layoutHeaderViewHolder.binding!!.tvPostDetail.text = imageInfoModel.imageCaption
        } else {
            layoutHeaderViewHolder.binding!!.tvPostDetail.visibility = View.GONE
        }
        layoutHeaderViewHolder.binding!!.tvPostTime.text = DateUtils.convertDateToTimeAgo(DateUtils.getLocalDate(imageInfoModel.postedTime))
        layoutHeaderViewHolder.binding!!.tvLike.text = Integer.toString(imageInfoModel.likesCount)
        layoutHeaderViewHolder.binding!!.tvComment.text = Integer.toString(imageInfoModel.commentsCount)
        layoutHeaderViewHolder.binding!!.tvShare.text = Integer.toString(imageInfoModel.shareCounts)
        layoutHeaderViewHolder.binding!!.tvFav.text = Integer.toString(imageInfoModel.favoriteCount)

        if (imageInfoModel.mediaFiles.size == 1) {
            layoutHeaderViewHolder.binding!!.tlMediaPost.visibility = View.GONE
        } else {
            layoutHeaderViewHolder.binding!!.tlMediaPost.visibility = View.VISIBLE
        }

        layoutHeaderViewHolder.binding!!.llLike.setOnClickListener { mlistener.onClickLike() }
        layoutHeaderViewHolder.binding!!.ivPostMore.setOnClickListener { mlistener.onClickMore() }
        layoutHeaderViewHolder.binding!!.llShare.setOnClickListener { mlistener.onclickShare() }
        layoutHeaderViewHolder.binding!!.llFav.setOnClickListener { mlistener.onClickFav() }
        layoutHeaderViewHolder.binding!!.ivUserProfile.setOnClickListener {
            var avatarfragment = ViewAvatarFragment(imageInfoModel.userId!!.toInt())
            avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
        }

        if (imageInfoModel.isLiked) {
            layoutHeaderViewHolder.binding!!.ivLike.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_red_heart)
        } else {
            layoutHeaderViewHolder.binding!!.ivLike.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_purple_heart)
        }
        if (imageInfoModel.isFavorited) {
            layoutHeaderViewHolder.binding!!.ivFav.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_fav)
        } else {
            layoutHeaderViewHolder.binding!!.ivFav.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_un_fav)
        }
        initPager(layoutHeaderViewHolder, imageInfoModel.mediaFiles, 0)
        setTagsRecyclerView(layoutHeaderViewHolder)

    }

    private fun initPager(holder: LayoutHeaderViewHolder, mediaFiles: ArrayList<EventGalleryFeedModel.MediaFile>, position: Int) {
        adapter = AdapterPostMediaPager(mediaFiles, position)
        adapter!!.setMListener(this)
        holder.binding!!.vpMediaPost.adapter = adapter
        holder.binding!!.tlMediaPost.setupWithViewPager(holder.binding!!.vpMediaPost, true)
    }

    private fun setTagsRecyclerView(layoutHeaderViewHolder: LayoutHeaderViewHolder) {
        if (tagFriendsList.size > 0) {
            val manager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            layoutHeaderViewHolder.binding!!.rvTags.layoutManager = manager
            layoutHeaderViewHolder.binding!!.rvTags.setHasFixedSize(true)
            adapterTags = AdapterFriendsTags(HooleyMain.activity!!, tagFriendsList, imageInfoModel.eventId!!, imageInfoModel.imageId)
            layoutHeaderViewHolder.binding!!.rvTags.adapter = adapterTags
        } else {
            layoutHeaderViewHolder.binding!!.rvTags.visibility = View.GONE
        }
    }

    fun setMListener(listener: CommentClickListener) {
        this.mlistener = listener
    }

    interface CommentClickListener {
        fun onClickLike()

        fun onclickShare()

        fun onClickMore()

        fun onClickFav()

        fun onClickImage()
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterCommentsBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

    inner class LayoutHeaderViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: LayoutCommentHeaderBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }
}