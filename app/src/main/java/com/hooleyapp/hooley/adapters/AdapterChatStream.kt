package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterChatStreamReceiverItemBinding
import com.hooleyapp.hooley.databinding.AdapterChatStreamSenderItemBinding
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.model.GetChatStreamModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import java.util.*

/**
 * Created by Nauman on 3/15/2018.
 */

class AdapterChatStream(private val context: Context, private val arrayList: ArrayList<GetChatStreamModel.ChatStream>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_SENDER) {
            val binding: AdapterChatStreamSenderItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_chat_stream_sender_item, parent, false)
            return SenderViewHolder(binding.root)
        } else {
            val binding: AdapterChatStreamReceiverItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_chat_stream_receiver_item, parent, false)
            return ReceiverViewHolder(binding.root)
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {

        if (getItemViewType(position) == VIEW_SENDER) {
            val holder = viewHolder as SenderViewHolder
            holder.binding!!.tvMessage.text = arrayList[position].messageText
            if (arrayList[position].messageTime!! != "Just Now")
                holder.binding!!.tvTimeStamp.text = DateUtils.convertSingleDate(DateUtils.getLocalDate(arrayList[position].messageTime!!))
            else
                holder.binding!!.tvTimeStamp.text = arrayList[position].messageTime!!
            if (!TextUtils.isEmpty(arrayList[position].profilePic))
                Glide.with(context).load(arrayList[position].profilePic).into(holder.binding!!.ivSenderAvatar)
            holder.binding!!.ivSenderAvatar.setOnClickListener {
                var avatarfragment = ViewAvatarFragment(arrayList[position].senderId!!.toInt())
                avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
            }
        } else {
            val holder = viewHolder as ReceiverViewHolder
            if (!TextUtils.isEmpty(arrayList[position].profilePic))
                Glide.with(context).load(arrayList[position].profilePic).into(holder.binding!!.ivReceiverAvatar)
            holder.binding!!.ivReceiverAvatar.setOnClickListener {
                var avatarfragment = ViewAvatarFragment(arrayList[position].senderId!!.toInt())
                avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
            }
            holder.binding!!.tvMessage.text = arrayList[position].messageText
            if (arrayList[position].messageTime!! != "Just Now")
                holder.binding!!.tvTimeStamp.text = DateUtils.convertSingleDate(DateUtils.getLocalDate(arrayList[position].messageTime!!))
            else
                holder.binding!!.tvTimeStamp.text = arrayList[position].messageTime!!
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (arrayList[position].senderId.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true)) 0 else 1
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    inner class SenderViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterChatStreamSenderItemBinding? = null

        init {
            binding = DataBindingUtil.bind(v)

        }
    }

    inner class ReceiverViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterChatStreamReceiverItemBinding? = null

        init {
            binding = DataBindingUtil.bind(v)

        }
    }

    companion object {


        private val VIEW_SENDER = 0
        private val VIEW_RECEIVER = 1
    }


}
