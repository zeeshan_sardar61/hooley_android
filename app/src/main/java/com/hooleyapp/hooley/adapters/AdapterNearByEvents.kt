package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.NearByEventsHeaderBinding
import com.hooleyapp.hooley.interfaces.INearByEventClickListener
import com.hooleyapp.hooley.interfaces.INearByEventHeaderClickListener
import com.hooleyapp.hooley.interfaces.INearByMoreClickListener
import com.hooleyapp.hooley.model.NearByEventsFinalModel
import com.hooleyapp.hooley.others.Constants
import java.util.*


/**
 * Created by Zeeshan on 19-Feb-18.
 */

class AdapterNearByEvents(private val context: Context, list: ArrayList<NearByEventsFinalModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), INearByEventHeaderClickListener, INearByMoreClickListener {
    internal var mList = ArrayList<NearByEventsFinalModel>()
    private var mListener: INearByEventClickListener? = null

    init {
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        //        if (viewType == Constants.TYPE_FEATURE_EVENT) {
        val binding = DataBindingUtil.inflate<NearByEventsHeaderBinding>(LayoutInflater.from(parent.context), R.layout.near_by_events_header, parent, false)
        return HeaderHolder(binding.root)
        //        } else {
        //            AdapterNearByEventBinding userSearchbinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_near_by_event, parent, false);
        //            return new ItemViewHolder(userSearchbinding.getRoot());
        //
        //        }
    }


    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        //        if (mList.get(position).type == Constants.TYPE_FEATURE_EVENT) {
        val holder = mHolder as HeaderHolder
        var typeName = ""
        if (mList[position].eventDetailList.size > 0) {

            when (mList[position].type) {
                Constants.TYPE_TODAY_EVENT -> typeName = "Today"
                Constants.TYPE_FEATURE_EVENT -> typeName = "Featured Events"
                Constants.TYPE_NEXT_WEEK_EVENT -> typeName = "Next Week"
                Constants.TYPE_COMMING_EVENT -> typeName = "Coming Weeks"
            }
            holder.binding!!.vpNearByHeader.clipToPadding = false
            val margin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (30 * 2).toFloat(), context.resources.displayMetrics).toInt()
            holder.binding!!.vpNearByHeader.pageMargin = -margin
            //            holder.userSearchbinding.vpNearByHeader.setPageMargin(dpToPx(25, HooleyMain.activity));
            val adapter = AdapterNearByEventsHeader(context, mList[position].eventDetailList, position)
            holder.binding!!.vpNearByHeader.adapter = adapter
            adapter.setListener(this)
            //                holder.userSearchbinding.tabFeaturEvent.setupWithViewPager(holder.userSearchbinding.vpNearByHeader, true);
            holder.binding!!.tvEventHeader.text = typeName
        }

    }


    override fun getItemViewType(position: Int): Int {
        return mList[position].type
    }

    override fun getItemCount(): Int {
        return mList.size
    }


    fun setListener(listener: INearByEventClickListener) {
        this.mListener = listener
    }

    override fun onClickHeaderLike(position: Int, mListPos: Int) {
        mListener!!.onClickLike(mList[mListPos], position, mListPos)
    }

    override fun onClickHeaderComment() {

    }

    override fun onClickHeaderFollow(position: Int, mListPos: Int) {
        mListener!!.onClickFollow(mList[mListPos], position, mListPos)
    }

    override fun onClickHeaderShare(position: Int, mListPos: Int) {
        mListener!!.onClickShare(mList[mListPos], position)
    }

    override fun onClickMoreLike(position: Int, type: Int, mListPos: Int) {
        try {
            when (type) {
                Constants.TYPE_TODAY_EVENT -> mListener!!.onClickLike(mList[mListPos], position, mListPos)
                Constants.TYPE_NEXT_WEEK_EVENT -> mListener!!.onClickLike(mList[mListPos], position, mListPos)
                Constants.TYPE_COMMING_EVENT -> mListener!!.onClickLike(mList[mListPos], position, mListPos)
            }

        } catch (ex: IndexOutOfBoundsException) {
            ex.printStackTrace()

        }

    }


    inner class HeaderHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: NearByEventsHeaderBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }
}
