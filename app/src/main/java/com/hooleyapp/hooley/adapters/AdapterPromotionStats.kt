package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ExpandableListView
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.ItemChildPromotionStatsBinding
import com.hooleyapp.hooley.databinding.ItemParentPromotionStatsBinding
import com.hooleyapp.hooley.model.EventPromotionStatsModel
import java.util.*

class AdapterPromotionStats(private val elvStateCity: ExpandableListView, private val context: Context, private val mlist: ArrayList<EventPromotionStatsModel.StateCity>?) : BaseExpandableListAdapter() {
    lateinit var childBinding: ItemChildPromotionStatsBinding
    lateinit var parentBinding: ItemParentPromotionStatsBinding
    lateinit var mListener: IPromotionStatsClickListener

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return mlist!![groupPosition].citiesList[childPosition]

    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getChildView(groupPosition: Int, childPosition: Int,
                              isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        childBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_child_promotion_stats, parent, false)
        convertView = childBinding.root
        convertView.tag = childBinding
        childBinding.tvCityName.text = mlist!![groupPosition].citiesList[childPosition].name
        childBinding.tvCityViews.text = mlist[groupPosition].citiesList[childPosition].viewCount.toString() + " View"
        return convertView

    }


    override fun getChildrenCount(groupPosition: Int): Int {
        return if (mlist!![groupPosition].citiesList == null) {
            0
        } else {
            mlist[groupPosition].citiesList.size
        }
    }

    override fun getGroup(groupPosition: Int): Any {
        return mlist!![groupPosition]
    }

    override fun getGroupCount(): Int {
        return mlist?.size ?: 0
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean,
                              convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        parentBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_parent_promotion_stats, parent, false)
        convertView = parentBinding.root
        convertView.tag = parentBinding
        parentBinding.tvState.text = mlist!![groupPosition].name
        parentBinding.tvViews.text = mlist[groupPosition].viewCount.toString() + " Views"
        if (!elvStateCity.isGroupExpanded(groupPosition)) {
            parentBinding.ivExpand.visibility = View.VISIBLE
            parentBinding.ivCollapse.visibility = View.GONE
        } else {
            parentBinding.ivExpand.visibility = View.GONE
            parentBinding.ivCollapse.visibility = View.VISIBLE
        }
        parentBinding.ivCollapse.setOnClickListener {
            parentBinding.ivCollapse.visibility = View.GONE
            parentBinding.ivExpand.visibility = View.VISIBLE
            mListener.onClickParent(true, groupPosition)
        }
        parentBinding.ivExpand.setOnClickListener {
            parentBinding.ivExpand.visibility = View.GONE
            parentBinding.ivCollapse.visibility = View.VISIBLE
            mListener.onClickParent(false, groupPosition)
        }
        return convertView
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    fun setListener(listener: IPromotionStatsClickListener) {
        this.mListener = listener
    }

    interface IPromotionStatsClickListener {
        fun onClickParent(isExpanded: Boolean, position: Int)

    }
}
