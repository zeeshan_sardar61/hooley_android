package com.hooleyapp.hooley.adapters

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dhruvvaishnav.sectionindexrecyclerviewlib.IndicatorScrollRecyclerView
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterInviteGuestEventBinding
import com.hooleyapp.hooley.fragments.friends.FriendCardPagerFragment
import com.hooleyapp.hooley.helper.load
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

/**
 * Created by Nauman on 5/18/2018.
 */

class AdapterPastGuestsList(private val mList: List<FriendsTable>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), IndicatorScrollRecyclerView.SectionedAdapter {
    internal lateinit var binding: AdapterInviteGuestEventBinding
    lateinit var mListener: AdapterPastGuestsList.IPastGuestListListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_invite_guest_event, parent, false)
        } catch (e: InflateException) {
            /* map is already there, just return view as it is */
        }

        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            holder.binding!!.tvFriendName.text = mList!![position].fullName
            holder.binding!!.tvAddress.text = "${mList[position].cityName}, ${mList[position].stateName}"
            if (!TextUtils.isEmpty(mList[position].profilePic))
                holder.binding!!.civFriendAvatar.load(mList[position].profilePic!!)
            else {
                holder.binding!!.civFriendAvatar.setImageResource(R.drawable.ic_avatar_place_holder)
            }
            holder.binding!!.civFriendAvatar.setOnClickListener {
                var friendCardFragment = FriendCardPagerFragment(position, (mList as ArrayList<FriendsTable>?)!!)
                friendCardFragment.show(HooleyMain.activity!!.supportFragmentManager)
            }
            holder.binding!!.rbSelectItem.isChecked = mList[position].isChecked

            holder.binding!!.rbSelectItem.setOnClickListener {
                mList[position].isChecked = !mList[position].isChecked
                mListener.onClickItem(position)
                notifyDataSetChanged()
            }
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }
    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    override fun getSectionName(position: Int): String {
        return mList!![position].fullName!![0].toString()
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterInviteGuestEventBinding? = null

        init {
            binding = DataBindingUtil.bind(v)

        }
    }

    interface IPastGuestListListener {
        fun onClickItem(position: Int)
    }
}