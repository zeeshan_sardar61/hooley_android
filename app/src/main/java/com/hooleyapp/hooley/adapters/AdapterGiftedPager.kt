package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.view.PagerAdapter
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterTicketGiftedBinding
import com.hooleyapp.hooley.model.MyTicketModel
import com.hooleyapp.hooley.others.DateUtils
import java.util.*

class AdapterGiftedPager(private val context: Context, private val mList: ArrayList<MyTicketModel.TicketList>, private val ticketObj: MyTicketModel.MyTicketsList) : PagerAdapter() {
    private var binding: AdapterTicketGiftedBinding? = null
    lateinit var mGiftListener: AdapterTicketPager.TicketClickListener

    override fun getCount(): Int {
        return mList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = HooleyMain.activity!!.layoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.adapter_ticket_gifted, container, false)
        if (!TextUtils.isEmpty(ticketObj.eventName))
            binding!!.tvEventName.text = ticketObj.eventName
        if (!TextUtils.isEmpty(ticketObj.eventAddress))
            binding!!.tvEventAddress.text = ticketObj.eventAddress
        if (!TextUtils.isEmpty(ticketObj.ticketCategoryName))
            binding!!.tvCatName.text = ticketObj.ticketCategoryName
        binding!!.tvTimeStamp.text = DateUtils.displayDateOnly(DateUtils.getLocalDate(ticketObj.startTime))

        binding!!.ivClose.setOnClickListener {
            mGiftListener.onClickClose()
        }
        binding!!.tvFriendName.text = ticketObj.giftedFrom
        container.addView(binding!!.root)
        return binding!!.root
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    interface GiftTicketClickListener {

        fun onGiftClickClose()

    }

}
