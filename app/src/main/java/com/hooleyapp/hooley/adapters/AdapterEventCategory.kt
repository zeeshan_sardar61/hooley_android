package com.hooleyapp.hooley.adapters

import android.content.Context
import android.content.res.ColorStateList
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v4.widget.ImageViewCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.databinding.AdapterEventTypeItemBinding
import com.hooleyapp.hooley.model.staticData.Category
import java.util.*


/**
 * Created by Nauman on 12/19/2017.
 */

class AdapterEventCategory(private val mContext: Context, internal var mlist: ArrayList<Category>) : BaseAdapter() {
    internal lateinit var binding: AdapterEventTypeItemBinding

    override fun getCount(): Int {
        return mlist.size
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getItem(position: Int): Any? {
        return mlist[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_event_type_item, parent, false)
        binding.tvCatName.text = mlist[position].name
        Glide.with(ActivityBase.activity).load(mlist[position].iconUrl).into(binding.ivLogo)
        if (mlist[position].isActive) {
            binding.rlItem.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.bg_purple_gradiant)
            ImageViewCompat.setImageTintList(binding.ivLogo, ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.simple_white)))
            binding.ivLogo.visibility = View.VISIBLE
            binding.tvCatName.setTextColor(ContextCompat.getColor(ActivityBase.activity, R.color.simple_white))
        } else {
            binding.rlItem.setBackgroundColor(ContextCompat.getColor(ActivityBase.activity, R.color.base_color))
            ImageViewCompat.setImageTintList(binding.ivLogo, ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.app_purple_color)))
            binding.ivLogo.visibility = View.VISIBLE
            binding.tvCatName.setTextColor(ContextCompat.getColor(ActivityBase.activity, R.color.text_color))
        }
        return binding.root
    }
}