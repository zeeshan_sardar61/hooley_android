package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.AdapterBubbleListBinding
import com.hooleyapp.hooley.model.BubbleListItem


class AdapterBubbleList(private val context: Context, internal var mList: List<BubbleListItem>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var binding: AdapterBubbleListBinding
    private var mListener: IBubbleClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_bubble_list, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        if (!TextUtils.isEmpty(mList!![position].eventAvatar))
            Glide.with(context).load(mList!![position].eventAvatar).into(holder.binding!!.ivEventAvatar)
        holder.binding!!.rlEventCoverOverlay.setOnClickListener { mListener!!.onBubbleClickListener(position) }
    }

    override fun getItemCount(): Int {
        return if (mList == null) 0 else mList!!.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterBubbleListBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

    fun setMListener(listener: IBubbleClickListener) {
        this.mListener = listener
    }

    interface IBubbleClickListener {
        fun onBubbleClickListener(position: Int)
    }
}
