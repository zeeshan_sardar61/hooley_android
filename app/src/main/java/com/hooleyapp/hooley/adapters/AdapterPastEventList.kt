package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterPastEventGuestListBinding
import com.hooleyapp.hooley.fragments.event.PastEventListFriendFragment
import com.hooleyapp.hooley.model.PastGuestListModel
import com.hooleyapp.hooley.others.DateUtils
import java.util.*

/**
 * Created by Nauman on 5/18/2018.
 */

class AdapterPastEventList(private val context: Context, private val mList: ArrayList<PastGuestListModel.Guest>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal lateinit var binding: AdapterPastEventGuestListBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_past_event_guest_list, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            holder.binding.tvEventName.text = mList!![position].eventName
            holder.binding.tvEventAddress.text = DateUtils.displayDateOnly(DateUtils.getLocalDate(mList[position].startTime))
            holder.binding.tvEventGuestCount.text = mList[position].invitGuestCount
            Glide.with(context).load(mList[position].coverPhoto).into(holder.binding.ivEventAvatar)
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }
        holder.binding.rlItem.setOnClickListener { (HooleyMain.activity as ActivityBase).callFragment(R.id.container, PastEventListFriendFragment.newInstance(mList!![position].eventName, mList[position].eventid), "PastEventListFriendFragment") }
    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterPastEventGuestListBinding = DataBindingUtil.bind(v)!!
    }
}
