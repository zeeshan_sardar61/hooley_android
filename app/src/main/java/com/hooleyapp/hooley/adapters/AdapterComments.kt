package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventDetailModel
import com.hooleyapp.hooley.databinding.AdapterCommentsBinding
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.others.DateUtils
import java.util.*

/**
 * Created by Zeeshan on 17-Jan-18.
 */

class AdapterComments(private val context: Context, internal var mList: ArrayList<EventDetailModel.Comments>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding: AdapterCommentsBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_comments, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        if (!TextUtils.isEmpty(mList!![position].profilePic))
            Glide.with(context).load(mList!![position].profilePic).into(holder.binding!!.ivUserProfile)
        holder.binding!!.tvUserName.text = mList!![position].fullName
        holder.binding!!.tvComment.text = mList!![position].commentText
        holder.binding!!.tvCommentDate.text = DateUtils.convertSingleDate(DateUtils.getLocalDate(mList!![position].commentTime!!))
        holder.binding!!.ivUserProfile.setOnClickListener {
            var avatarfragment = ViewAvatarFragment(mList!![position].userId!!.toInt())
            avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
        }
        //        holder.userSearchbinding.tvCommentDate.setText(((ActivityBase)context).GetDate(mList.get(position).commentTime));
    }

    override fun getItemCount(): Int {
        return if (mList == null) 0 else mList!!.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterCommentsBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }
}
