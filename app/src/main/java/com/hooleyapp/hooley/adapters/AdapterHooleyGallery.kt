package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterHooleyGalleryBinding
import com.hooleyapp.hooley.databinding.HeaderHooleyGalleryBinding
import com.hooleyapp.hooley.interfaces.IHooleyGalleryClickListener
import com.hooleyapp.hooley.model.HooleyGalleryFinalModel
import com.hooleyapp.hooley.others.Util
import java.util.*

/**
 * Created by Zeeshan on 22-Dec-17.
 */

class AdapterHooleyGallery(private val context: Context, private val arrayList: ArrayList<HooleyGalleryFinalModel>, var isEdit: Boolean) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal var isPresent = false
    lateinit var mListener: IHooleyGalleryClickListener
    var selectPos = 0
    internal var count = 0
    private val imagesPostition = ArrayList<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_HEADER) {
            val binding = DataBindingUtil.inflate<HeaderHooleyGalleryBinding>(LayoutInflater.from(parent.context), R.layout.header_hooley_gallery, parent, false)
            return HeaderViewHolder(binding.root)
        } else {
            val binding = DataBindingUtil.inflate<AdapterHooleyGalleryBinding>(LayoutInflater.from(parent.context), R.layout.adapter_hooley_gallery, parent, false)
            return ChildViewHolder(binding.root)
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {

        if (arrayList[position].type == VIEW_HEADER) {
            val holder = viewHolder as HeaderViewHolder
            holder.binding!!.tvGalleryHeader.text = arrayList[position].categoryName
        } else {
            val holder = viewHolder as ChildViewHolder
            Glide.with(context).load(arrayList[position].imageUrl).into(holder.binding!!.ivHooleyGallery)


            if (checkIfPresent(position) == position) {
                holder.binding!!.rlChecked.visibility = View.VISIBLE
            } else {
                holder.binding!!.rlChecked.visibility = View.GONE
            }


            holder.binding!!.ivHooleyGallery.setOnClickListener {
                /* Code added by shahzaib*/
                //selectPos = position;
                if (isEdit) {
                    imagesPostition.clear()
                    if (arrayList[position].isSelected) {
                        removeIndex(position)
                        arrayList[position].isSelected = false
                    } else {
                        imagesPostition.add(position)
                        arrayList[position].isSelected = true
                        mListener.onClickImage(arrayList[position].imageUrl, arrayList[position].isSelected, imagesPostition)
                    }
                    notifyDataSetChanged()
                } else {
                    if (imagesPostition.size < 4 && !arrayList[position].isSelected) {
                        imagesPostition.add(position)
                        arrayList[position].isSelected = true
                        mListener.onClickImage(arrayList[position].imageUrl, arrayList[position].isSelected, imagesPostition)
                        //size is less and a new element is selected
                    } else if (imagesPostition.size < 4 && arrayList[position].isSelected) {
                        removeIndex(position)
                        arrayList[position].isSelected = false
                        mListener.onClickImage(arrayList[position].imageUrl, arrayList[position].isSelected, imagesPostition)
                        //size is less than 4 but an item which is already in the list is again selected

                    } else if (imagesPostition.size == 4 && arrayList[position].isSelected) {
                        removeIndex(position)
                        arrayList[position].isSelected = false
                        mListener.onClickImage(arrayList[position].imageUrl, arrayList[position].isSelected, imagesPostition)
                        //size is full and an element from the array is selected
                    } else {
                        Util.showToastMessage(HooleyMain.activity!!, "Can't Select More than 4 pictures")
                    }
                    //mListener.onClickImage(arrayList.get(position).imageUrl,arrayList.get(position).isSelected() , imagesPostition);
                }
                notifyDataSetChanged()
            }
        }
    }


    private fun checkIfPresent(index: Int): Int {
        var ind = -1
        for (i in imagesPostition.indices) {
            if (index == imagesPostition[i]) {
                ind = index
                break
            }
        }
        return ind
    }

    private fun removeIndex(index: Int) {
        try {
            for (i in imagesPostition.indices) {
                if (index == imagesPostition[i]) {
                    imagesPostition.removeAt(i)
                }
            }
        } catch (e: Exception) {
        }
    }


    override fun getItemViewType(position: Int): Int {
        return arrayList[position].type
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }


    inner class HeaderViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: HeaderHooleyGalleryBinding? = null

        init {
            binding = DataBindingUtil.bind(v)

        }
    }

    inner class ChildViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterHooleyGalleryBinding? = null

        init {
            binding = DataBindingUtil.bind(v)

        }
    }

    fun setmListener(listener: IHooleyGalleryClickListener) {
        this.mListener = listener
    }

    companion object {


        private val VIEW_HEADER = 0
        private val VIEW_NORMAL = 1
    }

}
