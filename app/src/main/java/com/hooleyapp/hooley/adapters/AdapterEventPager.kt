package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.databinding.ItemEventPagerBinding
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.interfaces.IEventPagerClick
import com.hooleyapp.hooley.model.MapListFinalModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import java.util.*

/**
 * Created by Zeeshan on 21-Feb-18.
 */

class AdapterEventPager(private val context: Context, private val mList: ArrayList<MapListFinalModel>) : PagerAdapter(), ViewAvatarFragment.CardCallBackListener {
    override fun reloadFragment(flag: Boolean) {
        if (flag) {
            mListener!!.onClickViewProfile()
        }
    }

    lateinit var binding: ItemEventPagerBinding
    private var mListener: IEventPagerClick? = null
    private val mListpostion: Int = 0

    override fun getCount(): Int {
        return mList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = ActivityBase.activity.layoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.item_event_pager, container, false)
        if (!TextUtils.isEmpty(mList[position].eventDetail.coverPhoto))
            Glide.with(ActivityBase.activity).load(mList[position].eventDetail.coverPhoto).into(binding.ivEventCover)
        if (!TextUtils.isEmpty(mList[position].eventDetail.profilePic))
            Glide.with(ActivityBase.activity).load(mList[position].eventDetail.profilePic).into(binding.ivHost)
        if (!TextUtils.isEmpty(mList[position].eventDetail.eventName))
            binding.tvEventName.text = mList[position].eventDetail.eventName
        if (!TextUtils.isEmpty(mList[position].eventDetail.fullName))
            binding.tvHostName.text = mList[position].eventDetail.fullName
        if (!TextUtils.isEmpty(mList[position].eventDetail.startTime) && !TextUtils.isEmpty(mList[position].eventDetail.endTime))
            binding.tvEventDate.text = DateUtils.displayDateOnly(DateUtils.getLocalDate(mList[position].eventDetail.startTime!!))
        if (!TextUtils.isEmpty(mList[position].eventDetail.address))
            binding.tvEventAddress.text = mList[position].eventDetail.address
        binding.tvLikeEvent.text = mList[position].eventDetail.likesCount.toString()
        binding.tvShareEvent.text = mList[position].eventDetail.sharesCount.toString()
        binding.tvFollowEvent.text = mList[position].eventDetail.followersCount.toString()

        if (mList[position].eventDetail.isLiked) {
            binding.ivlikeEvent.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_red_heart)
        } else {
            binding.ivlikeEvent.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_purple_heart)
        }


        if (mList[position].eventDetail.isFollowed) {
            binding.ivFollowEvent.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_filled_star)
        } else {
            binding.ivFollowEvent.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_follow)
        }

        binding.ivHost.setOnClickListener {
            if (!HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)) {
                val avatarfragment = ViewAvatarFragment(mList[position].eventDetail.hostedId!!.toInt())
                avatarfragment.mListener = this
                avatarfragment.show(ActivityBase.activity.supportFragmentManager)
            }
        }

        binding.llShareEvent.setOnClickListener { mListener!!.onClickShare(mList[position].eventDetail.eventId!!, mList[position].eventDetail.coverPhoto!!, position) }

        binding.llFollowEvent.setOnClickListener {
            if (mList[position].eventDetail.isFollowed) {
                mListener!!.onClickFollow(mList[position].eventDetail.eventId!!, false, position)
            } else {
                mListener!!.onClickFollow(mList[position].eventDetail.eventId!!, true, position)
            }
        }

        binding.lllikeEvent.setOnClickListener {
            if (mList[position].eventDetail.isLiked) {
                mListener!!.onClickLiked(mList[position].eventDetail.eventId!!, false, position)
            } else {
                mListener!!.onClickLiked(mList[position].eventDetail.eventId!!, true, position)
            }
        }


        binding.btnViewEvent.setOnClickListener { mListener!!.onClickViewEvent(mList[position].eventDetail.eventId!!, position) }

        binding.ivClose.setOnClickListener { mListener!!.onClickClose() }

        container.addView(binding.root)
        return binding.root
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    fun setListener(listener: IEventPagerClick) {
        this.mListener = listener
    }

}
