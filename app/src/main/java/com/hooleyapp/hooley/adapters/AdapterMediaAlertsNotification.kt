package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterNotificationEventAlertBinding
import com.hooleyapp.hooley.fragments.event.LiveEventFragment
import com.hooleyapp.hooley.fragments.media.AllFeedsSinglePostCommentFragment
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.model.MediaAlertNotificationModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import java.util.*

/**
 * Created by Nauman on 4/11/2018.
 */

class AdapterMediaAlertsNotification
//    private IBookmarkEventsClickListner mListener;


(private val context: Context, mList: ArrayList<MediaAlertNotificationModel.MediaNotification>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal var mList = ArrayList<MediaAlertNotificationModel.MediaNotification>()

    init {
        this.mList = mList
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<AdapterNotificationEventAlertBinding>(LayoutInflater.from(parent.context), R.layout.adapter_notification_event_alert, parent, false)
        return ItemViewHolder(binding.root)
    }


    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        if (!TextUtils.isEmpty(mList[position].profilePic))
            Glide.with(context).load(mList[position].profilePic).into(holder.binding!!.ivEventAvatar)
        holder.binding!!.tvPeopleCount.text = DateUtils.convertDateToTimeAgo(DateUtils.getLocalDate(mList[position].sentTime!!))
        holder.binding!!.tvTimeStamp.visibility = View.GONE
        holder.binding!!.tvText.visibility = View.GONE
        holder.binding!!.tvTitle.text = (HooleyMain.activity as ActivityBase).noTrailingwhiteLines(Html.fromHtml(mList[position].bodyText))
        if (!mList[position].isSeen)
            holder.binding!!.rlContainer.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.unseen_color))
        else
            holder.binding!!.rlContainer.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.base_color))
        holder.binding!!.ivEventAvatar.setOnClickListener {
            var avatarfragment = ViewAvatarFragment(mList[position].userId!!.toInt())
            avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
        }
        holder.binding!!.llContainer.setOnClickListener {
            when (mList[position].type) {
                Constants.TYPE_LIKE_COMMENT_EVENT -> {
                    (HooleyMain.activity as ActivityBase).addFragment(R.id.container, LiveEventFragment
                            .newInstance(mList[position].eventId!!), "LiveEventFragment")
                    hideDrawer(HooleyMain.activity!!)
                }
                Constants.TYPE_LIKE_COMMENT_MEDIA -> (HooleyMain.activity as ActivityBase).addFragment(R.id.container, AllFeedsSinglePostCommentFragment.newInstance(mList[position].eventId!!, mList[position].postId!!), "AllFeedsSinglePostCommentFragment")
            }
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterNotificationEventAlertBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }


    //    public void setListener(IBookmarkEventsClickListner listener) {
    //        this.mListener = listener;
    //    }


}
