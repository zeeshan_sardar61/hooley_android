package com.hooleyapp.hooley.adapters

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.ActivityBase.Companion.activity
import com.hooleyapp.hooley.databinding.AdapterNearByEventHeaderBinding
import com.hooleyapp.hooley.fragments.event.GuestUserLiveEventFragment
import com.hooleyapp.hooley.fragments.event.LiveEventFragment
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.interfaces.INearByEventHeaderClickListener
import com.hooleyapp.hooley.model.NearByEventListFeedModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import java.util.*

/**
 * Created by Zeeshan on 19-Feb-18.
 */

class AdapterNearByEventsHeader(private val context: Context, internal var mList: ArrayList<NearByEventListFeedModel.EventDetail>, private val listPosition: Int) : PagerAdapter() {
    private var mListener: INearByEventHeaderClickListener? = null


    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val inflater = activity.layoutInflater
        val binding = DataBindingUtil.inflate<AdapterNearByEventHeaderBinding>(inflater, R.layout.adapter_near_by_event_header, container, false)
        Glide.with(context).load(mList[position].coverPhoto).into(binding.ivEventCover)
        binding.tvEventDate.text = DateUtils.displayDateOnly(DateUtils.getLocalDate(mList[position].startTime!!))
        binding.tvLike.text = Integer.toString(mList[position].likesCount)
        binding.tvFollow.text = Integer.toString(mList[position].followersCount)
        binding.tvShare.text = Integer.toString(mList[position].sharesCount)
        binding.tvEventName.text = mList[position].eventName
        binding.tvEventAddress.text = mList[position].address

        if (mList[position].isLiked) {
            binding.ivLike.background = ContextCompat.getDrawable(context, R.drawable.ic_red_heart)
        } else {
            binding.ivLike.background = ContextCompat.getDrawable(context, R.drawable.ic_purple_heart)
        }

        if (mList[position].isFollowed) {
            binding.ivFollow.background = ContextCompat.getDrawable(context, R.drawable.ic_filled_star)
        } else {
            binding.ivFollow.background = ContextCompat.getDrawable(context, R.drawable.ic_follow_it)
        }
        if (mList[position].isLive)
            binding.rlLive.visibility = View.VISIBLE
        else
            binding.rlLive.visibility = View.GONE

        binding.rlLive.setOnClickListener {
            if (HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)) {
                (activity as ActivityBase).callFragment(R.id.container, GuestUserLiveEventFragment
                        .newInstance(mList[position].eventId!!), "GuestUserLiveEventFragment")
                hideDrawer(activity)
            } else {
                HooleyApp.db.putBoolean(Constants.IS_COMMENT_CLICKED, false)
                HooleyApp.db.putBoolean(Constants.IS_BUBBLE_CLICKABLE, false)
                (activity as ActivityBase).callFragment(R.id.container, LiveEventFragment
                        .newInstance(mList[position].eventId!!), "LiveEventFragment")
                hideDrawer(activity)
            }
        }
        binding.ivEventCover.setOnClickListener {
            if (HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)) {
                (activity as ActivityBase).callFragment(R.id.container, GuestUserLiveEventFragment
                        .newInstance(mList[position].eventId!!), "GuestUserLiveEventFragment")
                hideDrawer(activity)

            } else {
                HooleyApp.db.putBoolean(Constants.IS_COMMENT_CLICKED, false)
                HooleyApp.db.putBoolean(Constants.IS_BUBBLE_CLICKABLE, false)
                (activity as ActivityBase).callFragment(R.id.container, LiveEventFragment
                        .newInstance(mList[position].eventId!!), "LiveEventFragment")
                hideDrawer(activity)
            }
        }

        binding.tvEventAddress.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + mList[position].eventLat + "," + mList[position].eventLong))
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }


        binding.llFollow.setOnClickListener {
            if (!HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)) {
                var count = Integer.parseInt(binding.tvFollow.text.toString())
                if (mList[position].isFollowed) {
                    binding.ivFollow.background = ContextCompat.getDrawable(context, R.drawable.ic_follow_it)
                    count--
                    binding.tvFollow.text = Integer.toString(count)
                } else {
                    binding.ivFollow.background = ContextCompat.getDrawable(context, R.drawable.ic_filled_star)
                    count++
                    binding.tvFollow.text = Integer.toString(count)
                }
            }
            mListener!!.onClickHeaderFollow(position, listPosition)
        }


        binding.llLike.setOnClickListener {
            if (!HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)) {
                var count = Integer.parseInt(binding.tvLike.text.toString())
                if (mList[position].isLiked) {
                    binding.ivLike.background = ContextCompat.getDrawable(context, R.drawable.ic_purple_heart)
                    count--
                    binding.tvLike.text = Integer.toString(count)
                } else {
                    binding.ivLike.background = ContextCompat.getDrawable(context, R.drawable.ic_red_heart)
                    count++
                    binding.tvLike.text = Integer.toString(count)
                }
            }
            mListener!!.onClickHeaderLike(position, listPosition)
        }
        binding.llShare.setOnClickListener { mListener!!.onClickHeaderShare(position, listPosition) }

        if (position == 0) {
            if (mList.size == 1)
                binding.llFeatureEvents.setPadding(0, 0, dpToPx(10, context), 0)
            else
                binding.llFeatureEvents.setPadding(0, 0, dpToPx(30, context), 0)
        } else if (position == mList.size - 1)
            binding.llFeatureEvents.setPadding(dpToPx(30, context), 0, 0, 0)
        else
            binding.llFeatureEvents.setPadding(dpToPx(30, context), 0, dpToPx(30, context), 0)

        container.addView(binding.root)
        return binding.root
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as LinearLayout)
    }

    //-----------------------------------------------------------------------------
    // Used by ViewPager; can be used by app as well.
    // Returns the total number of pages that the ViewPage can display.  This must
    // never be 0.
    override fun getCount(): Int {
        return mList.size
    }

    //-----------------------------------------------------------------------------
    // Used by ViewPager.
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }


    fun setListener(listener: INearByEventHeaderClickListener) {
        this.mListener = listener
    }

    companion object {

        fun dpToPx(dp: Int, context: Context): Int {
            val density = context.resources.displayMetrics.density
            return Math.round(dp.toFloat() * density)
        }
    }

}
