package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.AdapterOtherEventBinding
import com.hooleyapp.hooley.model.event.PastEventsItem
import com.hooleyapp.hooley.others.DateUtils
import java.util.*

/**
 * Created by Nauman on 4/23/2018.
 */

class AdapterPastEvent(private val context: Context, private val mList: ArrayList<PastEventsItem>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal lateinit var binding: AdapterOtherEventBinding
    lateinit var mListener: IMyEventsClickListeners

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_other_event, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            holder.binding!!.tvEventName.text = mList!![position].eventName
            holder.binding!!.tvInterestedPeople.text = mList[position].invitGuestCount.toString() + " people interested"
            holder.binding!!.tvEventTimeStamp.text = DateUtils.displayDateOnly(DateUtils.getLocalDate(mList[position].startTime!!))
            Glide.with(context).load(mList[position].coverPhoto).into(holder.binding!!.ivEventAvatar)
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

        holder.binding!!.rlItem.setOnClickListener {
            mListener.onClickItem(position)
        }

        holder.binding!!.rlAlert.visibility = GONE

    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterOtherEventBinding? = null

        init {
            binding = DataBindingUtil.bind(v)

        }
    }

    interface IMyEventsClickListeners {
        fun onClickItem(position: Int)
    }

}
