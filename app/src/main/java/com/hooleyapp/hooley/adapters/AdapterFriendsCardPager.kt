package com.hooleyapp.hooley.adapters

import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.ItemCardPagerBinding
import com.hooleyapp.hooley.interfaces.FriendCardClickListener
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

/**
 * Created by Nauman on 3/6/2018.
 */

class AdapterFriendsCardPager(private val mList: ArrayList<FriendsTable>) : PagerAdapter() {
    lateinit var mListener: FriendCardClickListener
    private var binding: ItemCardPagerBinding? = null

    override fun getCount(): Int {
        return mList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = HooleyMain.activity!!.layoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.item_card_pager, container, false)

        if (!TextUtils.isEmpty(mList[position].profilePic))
            Glide.with(HooleyMain.activity!!).load(mList[position].profilePic).into(binding!!.ivAvatar)
        else
            binding!!.ivAvatar.setImageResource(R.drawable.ic_avatar_place_holder)
        if (!TextUtils.isEmpty(mList[position].fullName))
            binding!!.tvUserName.text = mList[position].fullName
        if (!TextUtils.isEmpty(mList[position].ageGroup))
            binding!!.tvAgeGroup.text = mList[position].ageGroup
        binding!!.tvAddress.text = mList[position].cityName + ", " + mList[position].stateName

        try {
            if (mList[position].status != null) {
                when {
                    mList[position].status.equals(Constants.TYPE_FRIEND, ignoreCase = true) -> {
                        binding!!.btnViewProfile.visibility = View.VISIBLE
                        binding!!.btnAddFriend.visibility = View.GONE
                        binding!!.btnMyProfile.visibility = View.GONE
                        binding!!.btnMessage.visibility = View.VISIBLE
                        checkAndSet(position)
                    }
                    mList[position].status.equals(Constants.TYPE_PENDING_SENT, ignoreCase = true) -> {
                        binding!!.llFriend.visibility = View.VISIBLE
                        binding!!.llFriendRequest.visibility = View.GONE
                        binding!!.btnViewProfile.visibility = View.GONE
                        binding!!.btnAddFriend.visibility = View.VISIBLE
                        binding!!.btnAddFriend.text = "Pending"
                        binding!!.btnAddFriend.isEnabled = false
                        binding!!.btnMyProfile.visibility = View.GONE
                        binding!!.btnMessage.visibility = View.VISIBLE

                        checkAndSet(position)
                    }
                    mList[position].status.equals(Constants.TYPE_PENDING_RECEIVED, ignoreCase = true) -> {
                        binding!!.llFriend.visibility = View.GONE
                        binding!!.llFriendRequest.visibility = View.VISIBLE

                        checkAndSet(position)
                    }
                    else -> {
                        binding!!.llFriend.visibility = View.VISIBLE
                        binding!!.llFriendRequest.visibility = View.GONE
                        binding!!.btnViewProfile.visibility = View.GONE
                        binding!!.btnAddFriend.visibility = View.VISIBLE
                        binding!!.btnMyProfile.visibility = View.GONE
                        binding!!.btnMessage.visibility = View.VISIBLE

                        checkAndSet(position)
                    }
                }
                if (mList[position].userId.toString().equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true)) {
                    binding!!.btnViewProfile.visibility = View.GONE
                    binding!!.btnAddFriend.visibility = View.GONE
                    binding!!.btnMessage.visibility = View.GONE
                    binding!!.btnMyProfile.visibility = View.VISIBLE
                }
            } else {
                checkAndSetFriend(position)

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        // Setting Listener
        binding!!.btnAccept.setOnClickListener { mListener.onClickAcceptRequest(position, true) }
        binding!!.btnIgnore.setOnClickListener { mListener.onClickAcceptRequest(position, false) }
        binding!!.btnViewProfile.setOnClickListener { mListener.onClickViewProfile(position) }
        binding!!.btnMessage.setOnClickListener { mListener.onClickMessage(position) }
        binding!!.ivClose.setOnClickListener { mListener.onClickClose() }
        binding!!.btnAddFriend.setOnClickListener { mListener.onClickAddFriend(position) }
        binding!!.btnMyProfile.setOnClickListener { mListener.onClickMyProfile() }
        container.addView(binding!!.root)
        return binding!!.root
    }

    private fun checkAndSetFriend(position: Int) {
        if (mList[position].isMessageAccessible) {
            binding!!.btnMessage.visibility = View.VISIBLE
        } else {
            binding!!.btnMessage.isEnabled = false
            binding!!.btnMessage.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_disable_btn)
        }
    }

    private fun checkAndSet(position: Int) {
        if (mList[position].isFriendRequestAccessible) {
            binding!!.btnAddFriend.visibility = View.VISIBLE
        } else {
            binding!!.btnAddFriend.isEnabled = false
//            binding!!.btnAddFriend.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_disable_btn)
        }
        if (mList[position].isMessageAccessible) {
            binding!!.btnMessage.visibility = View.VISIBLE
        } else {
            binding!!.btnMessage.isEnabled = false
            binding!!.btnMessage.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_disable_btn)
        }
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

}