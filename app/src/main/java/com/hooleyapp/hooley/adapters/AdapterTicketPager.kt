package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterTicketSliderBinding
import com.hooleyapp.hooley.model.MyTicketModel
import com.hooleyapp.hooley.others.DateUtils
import java.util.*

class AdapterTicketPager(private val context: Context, private val mList: ArrayList<MyTicketModel.TicketList>, private val ticketObj: MyTicketModel.MyTicketsList) : PagerAdapter() {
    lateinit var mListener: TicketClickListener
    private var binding: AdapterTicketSliderBinding? = null

    override fun getCount(): Int {
        return mList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }


    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = HooleyMain.activity!!.layoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.adapter_ticket_slider, container, false)
//        if (!TextUtils.isEmpty(mList[position].qrCodeUrl)) {
//            AsyncTask.execute {
//                HooleyMain.activity!!.runOnUiThread {
//                    userSearchbinding!!.ivQrCode.setImageBitmap(Util.createImageFromString(HooleyMain.activity!!, mList[position].qrCodeUrl))
//                }
//            }
//        }

        binding!!.ivQrCode.setImageBitmap(mList[position].qrBM)

        if (!TextUtils.isEmpty(ticketObj.eventName))
            binding!!.tvEventName.text = ticketObj.eventName
        if (!TextUtils.isEmpty(ticketObj.eventAddress))
            binding!!.tvEventAddress.text = ticketObj.eventAddress
        if (!TextUtils.isEmpty(ticketObj.ticketCategoryName))
            binding!!.tvCatName.text = ticketObj.ticketCategoryName
        binding!!.tvTimeStamp.text = DateUtils.displayDateOnly(DateUtils.getLocalDate(ticketObj.startTime))
        if (mList[position].isValid!!) {
            binding!!.llTicket.alpha = 1f
            binding!!.rlBottom.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_bg_valid_ticket)
            if (ticketObj.isRefundAllow!!) {
                binding!!.ivRefund.visibility = View.VISIBLE
            } else {
                binding!!.ivRefund.visibility = View.GONE
            }
        } else {
            binding!!.llTicket.alpha = .5f
            binding!!.tvTickStatus.text = "Ticket has Expired"
            binding!!.ivGift.visibility = View.GONE
            binding!!.ivRefund.visibility = View.GONE
            binding!!.ivNavigate.isEnabled = false
            binding!!.ivNavigate.setImageResource(R.drawable.ic_disable_location)
            binding!!.rlBottom.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_bg_expired_ticket)
        }

        if (mList[position].isGifted) {
            binding!!.ivGift.visibility = View.GONE
            binding!!.ivRefund.visibility = View.GONE
        }

        binding!!.ivClose.setOnClickListener { mListener.onClickClose() }

        binding!!.ivNavigate.setOnClickListener {
            if (mList[position].isValid!!)
                mListener.onClickNavigate()
        }

        binding!!.ivGift.setOnClickListener {
            if (mList[position].isValid!!)
                mListener.onClickGiftTicket(position, ticketObj)
        }


        binding!!.ivRefund.setOnClickListener {
            if (mList[position].isValid!!)
                mListener.onClickTicketRefund(position, ticketObj.ticketList[position])
        }

        container.addView(binding!!.root)
        return binding!!.root
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    fun setListener(listener: TicketClickListener) {
        mListener = listener
    }


    interface TicketClickListener {

        fun onClickGiftTicket(position: Int, item: MyTicketModel.MyTicketsList)

        fun onClickNavigate()

        fun onClickClose()

        fun onClickTicketRefund(position: Int, item: MyTicketModel.TicketList)

    }
}