package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterMessageHistoryBinding
import com.hooleyapp.hooley.fragments.messaging.ChatWindowFragment
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GetAllThreadModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.MessageService
import java.util.*

/**
 * Created by Nauman on 4/24/2018.
 */

class AdapterMessageHistory(private val context: Context, internal var mList: ArrayList<GetAllThreadModel.ThreadInfo>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var service: MessageService? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<AdapterMessageHistoryBinding>(LayoutInflater.from(parent.context), R.layout.adapter_message_history, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {

        val holder = mHolder as ItemViewHolder
        if (!TextUtils.isEmpty(mList!![position].profilePic)) {
            Glide.with(context).load(mList!![position].profilePic).into(holder.binding!!.civFriendAvatar)
        } else {
            Glide.with(context).load(R.drawable.ic_avatar_place_holder).into(holder.binding!!.civFriendAvatar)
        }
        holder.binding!!.tvFriendName.text = mList!![position].fullName
        holder.binding!!.tvLastMessage.text = mList!![position].lastMessage
        holder.binding!!.tvTimeStamp.text = DateUtils.convertSingleDate(DateUtils.getLocalDate(mList!![position].lastMessageTime!!))
        holder.binding!!.rlDelete.setOnClickListener {
            holder.binding!!.rlDelete.isEnabled = false
            deleteThread(position, holder.binding!!.rlDelete)
        }
        if (!mList!![position].isRead) {
            holder.binding!!.rlItem.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.chat_unread_color))
            holder.binding!!.tvFriendName.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
            holder.binding!!.tvTimeStamp.setTypeface(null, Typeface.BOLD)
        } else {
            holder.binding!!.rlItem.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.base_color))
            holder.binding!!.tvFriendName.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.text_color))
            holder.binding!!.tvTimeStamp.setTypeface(null, Typeface.NORMAL)
        }
        holder.binding!!.srLayout.close(true)
        holder.binding!!.civFriendAvatar.setOnClickListener {
            var avatarfragment = ViewAvatarFragment(mList!![position].friendId!!.toInt())
            avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
        }
        holder.binding!!.rlContainer.setOnClickListener { (HooleyMain.activity as ActivityBase).callFragment(R.id.container, ChatWindowFragment(mList!![position].threadId!!, mList!![position].friendId!!, mList!![position].fullName!!), "ChatWindowFragment") }

    }

    private fun deleteThread(position: Int, rlDelete: RelativeLayout) {

        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }

        service = MessageService()
        service!!.removeThread(object : GenericCallback<GeneralModel> {

            override fun success(result: GeneralModel) {
                mList!!.removeAt(position)
                notifyDataSetChanged()
                rlDelete.isEnabled = true
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                    rlDelete.isEnabled = true
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        }, mList!![position].threadId!!)
    }


    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun getItemCount(): Int {
        return if (mList == null) 0 else mList!!.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterMessageHistoryBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

}