package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Build
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterNotificationEventAlertBinding
import com.hooleyapp.hooley.fragments.event.LiveEventFragment
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.model.EventInviteNotificationModel
import com.hooleyapp.hooley.others.DateUtils
import java.util.*

/**
 * Created by Nauman on 4/11/2018.
 */

class AdapterEventAlertNotification(private val context: Context, mList: ArrayList<EventInviteNotificationModel.EventInvite>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal var mList = ArrayList<EventInviteNotificationModel.EventInvite>()

    init {
        this.mList = mList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding: AdapterNotificationEventAlertBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_notification_event_alert, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        Glide.with(context).load(mList[position].coverPhoto).into(holder.binding!!.ivEventAvatar)
        holder.binding!!.tvTimeStamp.text = DateUtils.displayDateOnly(DateUtils.getLocalDate(mList[position].eventStartTime!!))
        holder.binding!!.tvPeopleCount.text = DateUtils.convertDateToTimeAgo(DateUtils.getLocalDate(mList[position].sentTime!!))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.binding!!.tvTitle.text = (HooleyMain.activity as ActivityBase).noTrailingwhiteLines(Html.fromHtml(mList[position].bodyText, Html.FROM_HTML_MODE_COMPACT))
        } else {
            holder.binding!!.tvTitle.text = (HooleyMain.activity as ActivityBase).noTrailingwhiteLines(Html.fromHtml(mList[position].bodyText))
        }
        if (!mList[position].isSeen)
            holder.binding!!.rlContainer.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.unseen_color))
        else
            holder.binding!!.rlContainer.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.base_color))
        holder.binding!!.rlContainer.setOnClickListener(View.OnClickListener {
            if (mList[position].isDisable)
                return@OnClickListener
            (HooleyMain.activity as ActivityBase).addFragment(R.id.container, LiveEventFragment
                    .newInstance(mList[position].eventId!!), "LiveEventFragment")
            hideDrawer(HooleyMain.activity!!)
        })
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterNotificationEventAlertBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

}