package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.AdapterRecentSearchBinding

/**
 * Created by Nauman on 3/6/2018.
 */

class AdapterRecentSearches(private val context: Context, var mList: ArrayList<String>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal lateinit var binding: AdapterRecentSearchBinding
    lateinit var mListener: SearchItemClick


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_recent_search, parent, false)
        } catch (e: InflateException) {
            /* map is already there, just return view as it is */
        }

        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder

        holder.binding!!.tvUserName.text = mList[position]

        holder.binding!!.rlEvent.setOnClickListener { mListener.onSearchClick(mList[position]) }

    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterRecentSearchBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

    interface SearchItemClick {

        fun onSearchClick(name: String)

    }

}