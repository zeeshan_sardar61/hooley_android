package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.AdapterSendGiftTicketBinding
import com.hooleyapp.hooley.model.MyTicketModel
import com.hooleyapp.hooley.others.DateUtils
import java.util.*

class AdapterSendGiftTicket(private val context: Context, private val mList: ArrayList<MyTicketModel.MyTicketsList>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    internal lateinit var binding: AdapterSendGiftTicketBinding
    private var mListener: ISendGiftTicketClickListener? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_send_gift_ticket, parent, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }

        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            holder.binding!!.tvEventName.text = mList!![position].eventName
            holder.binding!!.tvEventTimeStamp.text = DateUtils.displayDateOnly(DateUtils.getLocalDate(mList[position].startTime))
            Glide.with(context).load(mList[position].coverPhoto).into(holder.binding!!.ivEventAvatar)
            holder.binding!!.tvGiftedTo.text = mList[position].giftedFrom
            holder.binding!!.rlEvent.setOnClickListener { mListener!!.onClickGiftItem(position) }
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterSendGiftTicketBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

    fun setListener(listener: ISendGiftTicketClickListener) {
        mListener = listener

    }

    interface ISendGiftTicketClickListener {
        fun onClickGiftItem(position: Int)
    }
}
