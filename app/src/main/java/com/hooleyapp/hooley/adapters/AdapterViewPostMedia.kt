package com.hooleyapp.hooley.adapters

import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.helper.TouchImageView
import java.util.*

/**
 * Created by adilmalik on 18/10/2018.
 */

class AdapterViewPostMedia(// This holds all the currently displayable views, in order from left to right.
        private val mList: ArrayList<EventGalleryFeedModel.MediaFile>) : PagerAdapter() {
    lateinit var mListener: IViewPostClickListener


    //-----------------------------------------------------------------------------
    // Used by ViewPager.  "Object" represents the page; tell the ViewPager where the
    // page should be displayed, from left-to-right.  If the page no longer exists,
    // return POSITION_NONE.
    //    @Override
    //    public int getItemPosition(Object object) {
    //        int index = views.indexOf(object);
    //        if (index == -1)
    //            return POSITION_NONE;
    //        else
    //            return mList.;
    //    }

    //-----------------------------------------------------------------------------
    // Used by ViewPager.  Called when ViewPager needs a page to display; it is our job
    // to add the page to the container, which is normally the ViewPager itself.  Since
    // all our pages are persistent, we simply retrieve it from our "views" ArrayList.
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        //        View v = views.get(position);
        val inflater = HooleyMain.activity!!.layoutInflater
        val v = inflater.inflate(R.layout.adapter_view_post, container, false)
        val ivMedia = v.findViewById<TouchImageView>(R.id.ivMedia)
        val rlMovie = v.findViewById<RelativeLayout>(R.id.rlMovie)
        val ivPost = v.findViewById<ImageView>(R.id.ivPost)
        val ivMovie = v.findViewById<ImageView>(R.id.ivMovie)


        if (mList[position].isVideo!!) {
            rlMovie.visibility = View.VISIBLE
            ivMedia.visibility = View.GONE
            Glide.with(HooleyMain.activity!!).load(mList[position].thumbnailUrl).into(ivPost)
        } else {
            rlMovie.visibility = View.GONE
            ivMedia.visibility = View.VISIBLE
            if (!TextUtils.isEmpty(mList[position].thumbnailUrl)) {
                Glide.with(HooleyMain.activity!!).load(mList[position].thumbnailUrl).into(ivMedia)
            }
        }
        ivMovie.setOnClickListener { mListener.onClickVideo(position) }
        container.addView(v)
        return v
    }

    //-----------------------------------------------------------------------------
    // Used by ViewPager.  Called when ViewPager no longer needs a page to display; it
    // is our job to remove the page from the container, which is normally the
    // ViewPager itself.  Since all our pages are persistent, we do nothing to the
    // contents of our "views" ArrayList.
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }

    //-----------------------------------------------------------------------------
    // Used by ViewPager; can be used by app as well.
    // Returns the total number of pages that the ViewPage can display.  This must
    // never be 0.
    override fun getCount(): Int {
        return mList.size
    }

    //-----------------------------------------------------------------------------
    // Used by ViewPager.
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }


    //-----------------------------------------------------------------------------
    // Removes the "view" at "position" from "views".
    // Retuns position of removed view.
    // The app should call this to remove pages; not used by ViewPager.
    fun removeView(pager: ViewPager, position: Int): Int {
        // ViewPager doesn't have a delete method; the closest is to set the adapter
        // again.  When doing so, it deletes all its views.  Then we can delete the view
        // from from the adapter and finally set the adapter to the pager again.  Note
        // that we set the adapter to null before removing the view from "views" - that's
        // because while ViewPager deletes all its views, it will call destroyItem which
        // will in turn cause a null pointer ref.
        pager.adapter = null
        mList.removeAt(position)
        pager.adapter = this

        return position
    }

    fun setmListener(listener: IViewPostClickListener) {
        this.mListener = listener
    }


    interface IViewPostClickListener {
        fun onClickVideo(position: Int)

    }
    //-----------------------------------------------------------------------------
    // Returns the "view" at "position".
    // The app should call this to retrieve a view; not used by ViewPager.
    //    public View getView(int position) {
    //        return views.get(position);
    //    }

    // Other relevant methods:

    // finishUpdate - called by the ViewPager - we don't care about what pages the
    // pager is displaying so we don't use this method.
}