package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.AdapterPastGuestListBinding
import com.hooleyapp.hooley.model.PastGuestListModel
import com.hooleyapp.hooley.others.DateUtils

/**
 * Created by Nauman on 12/29/2017.
 */

class AdapterPastGuestList(private val context: Context, private val mList: ArrayList<PastGuestListModel.Guest>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    internal lateinit var binding: AdapterPastGuestListBinding
    lateinit var mListener: IPastGuestListListeners

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_past_guest_list, parent, false)
        return ItemViewHolder(binding.root)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            holder.binding!!.tvEventName.text = mList!![position].eventName
            holder.binding!!.tvEventAddress.text = DateUtils.displayDateOnly(DateUtils.getLocalDate(mList[position].startTime))
            holder.binding!!.tvEventGuestCount.text = mList[position].invitGuestCount
            Glide.with(context).load(mList[position].coverPhoto).into(holder.binding!!.ivEventAvatar)
            holder.binding!!.rbSelectItem.isChecked = mList[position].isChecked
            when {
                mList[position].selection == 0 -> {
                    holder.binding!!.tvEventGuestCount.text = mList[position].invitGuestCount
                    holder.binding!!.rbSelectItem.background = context.getDrawable(R.drawable.ic_uncheck
                    )
                    mList[position].isSelectedAll = false
                    mList[position].isChecked = false
                }
                mList[position].selection == 1 -> {
                    mList[position].isSelectedAll = true
                    mList[position].isChecked = true
//                    holder.userSearchbinding!!.tvEventGuestCount.text = mList[position].invitGuestCount + "/" + mList[position].invitGuestCount
                    holder.binding!!.rbSelectItem.background = context.getDrawable(R.drawable.ic_checked)
                }
                else -> {
                    mList[position].isSelectedAll = false
                    mList[position].isChecked = true
//                    holder.userSearchbinding!!.tvEventGuestCount.text = mList[position].userIdList.size.toString() + "/" + mList[position].invitGuestCount
                    holder.binding!!.rbSelectItem.background = context.getDrawable(R.drawable.ic_selective_item)

                }
            }

        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

        holder.binding!!.rlItem.setOnClickListener {
            mListener.onClickItem(position)
        }
        holder.binding!!.rbSelectItem.setOnClickListener {
            if (mList!![position].userIdList == null) {
                mList[position].userIdList = ArrayList()
            }
            when {
                mList[position].selection == 0 -> {
                    mList[position].selection = 1
                    mList[position].isSelectedAll = true
                    mList[position].userIdList.clear()
                }
                mList[position].selection == 1 -> {
                    mList[position].selection = 0
                    mList[position].isSelectedAll = false
                    mList[position].userIdList.clear()
                }
                else -> {
                    mList[position].selection = 1
                    mList[position].userIdList.clear()
                }
            }
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterPastGuestListBinding? = null

        init {
            binding = DataBindingUtil.bind(v)

        }
    }

    interface IPastGuestListListeners {
        fun onClickItem(position: Int)
    }
}
