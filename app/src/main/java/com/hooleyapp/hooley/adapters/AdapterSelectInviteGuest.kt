package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterPeopleNearMeBinding
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.model.EventInviteNearByMeModel

class AdapterSelectInviteGuest(private val context: Context, internal var mList: List<EventInviteNearByMeModel.UserNearMe>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var selectedPosition = -1
    lateinit var mlistener: ISelectFriendListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<AdapterPeopleNearMeBinding>(LayoutInflater.from(parent.context), R.layout.adapter_people_near_me, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        if (!TextUtils.isEmpty(mList!![position].profilePicUrl))
            Glide.with(context).load(mList!![position].profilePicUrl).into(holder.binding!!.ivUserProfile)
        holder.binding!!.tvUserName.text = mList!![position].fullName
        holder.binding!!.tvUserDistance.visibility = View.GONE
        holder.binding!!.ivUserProfile.setOnClickListener {
            var avatarFragment = ViewAvatarFragment(Integer.parseInt(mList!![position].userId))
            avatarFragment.show(HooleyMain.activity!!.supportFragmentManager)
        }

        holder.binding!!.rbSelectUser.isChecked = mList!![position].isChecked
        holder.binding!!.rbSelectUser.setOnClickListener {
            selectedPosition = position
            mList!![position].isChecked = !mList!![position].isChecked
            holder.binding!!.rbSelectUser.isChecked = mList!![position].isChecked
            mlistener.onFriendSelected(position)
        }
    }

    override fun getItemCount(): Int {
        return if (mList == null) 0 else mList!!.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterPeopleNearMeBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

    fun setMListener(listener: ISelectFriendListener) {
        mlistener = listener
    }

    interface ISelectFriendListener {
        fun onFriendSelected(position: Int)
    }

}
