package com.hooleyapp.hooley.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.model.PersonalProfileModel
import java.util.*

/**
 * Created by Nauman on 12/17/2017.
 */

class AdapterCountries(var mcontext: Context, textViewResourceId: Int, private val mList: ArrayList<PersonalProfileModel.myObject>?) : ArrayAdapter<String>(mcontext, textViewResourceId) {
    internal var inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val row = inflater.inflate(R.layout.sp_gender_drop_down_tem, parent, false)
        val label = row.findViewById<TextView>(R.id.spinnerItem)
        try {
            label.text = mList!![position].name
        } catch (ex: IndexOutOfBoundsException) {
        }

        return row
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val row = inflater.inflate(R.layout.sp_gender_group_item, parent, false)
        val label = row.findViewById<TextView>(R.id.spinnerItem)
        try {
            label.text = mList!![position].name
        } catch (ex: IndexOutOfBoundsException) {
        }

        return row
    }

    override fun getCount(): Int {
        super.getCount()
        return if (mList != null && mList.size > 0) mList.size else 0
    }
}