package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterFriendsTagsBinding
import com.hooleyapp.hooley.fragments.friends.CardPagerFragment
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.TagUserModel
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

/**
 * Created by Nauman on 2/9/2018.
 */

class AdapterFriendsTags(private val context: Context, internal var mList: ArrayList<FriendsTable>?, private val eventId: String, private val imageId: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val showMore = false
    var friendWebService = FriendWebService()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<AdapterFriendsTagsBinding>(LayoutInflater.from(parent.context), R.layout.adapter_friends_tags, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        if (!TextUtils.isEmpty(mList!![position].profilePic))
            Glide.with(HooleyMain.activity!!).load(mList!![position].profilePic).into(holder.binding!!.ivFriendAvatar)

        holder.binding!!.ivFriendAvatar.setOnClickListener {
            var avatarfragment = ViewAvatarFragment(mList!![position].userId.toInt())
            avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
        }
    }

    private fun getTagUser(pos: Int, eventId: String, imageId: String) {
        friendWebService.getTagUser(eventId, imageId, object : GenericCallback<TagUserModel> {
            override fun success(result: TagUserModel) {
                if (result.success) {
                    var avatarfragment = CardPagerFragment(pos, result.tagList)
                    avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
                }
//                    (HooleyMain.activity as ActivityBase).callFragment(R.id.container, CardPagerFragment.newInstance(pos, result.tagList), "CardPagerFragment")

            }

            override fun failure(message: String) {

            }
        })
    }

    override fun getItemCount(): Int {
        return if (mList == null) 0 else mList!!.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterFriendsTagsBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

}
