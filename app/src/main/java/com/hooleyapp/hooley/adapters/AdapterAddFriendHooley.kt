package com.hooleyapp.hooley.adapters

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.AdapterPastEventFriendsBinding
import com.hooleyapp.hooley.helper.load
import com.hooleyapp.hooley.interfaces.IFriendContactClickListener
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.tables.FriendsTable

class AdapterAddFriendHooley(private val mList: List<FriendsTable>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal lateinit var binding: AdapterPastEventFriendsBinding
    lateinit var mListener: IFriendContactClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_past_event_friends, parent, false)
        } catch (e: InflateException) {
            /* map is already there, just return view as it is */
        }

        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            holder.binding!!.tvFriendName.text = mList!![position].fullName
            holder.binding!!.tvAddress.text = "${mList[position].cityName}, ${mList[position].stateName}"
            if (!TextUtils.isEmpty(mList[position].profilePic))
                holder.binding!!.civFriendAvatar.load(mList[position].profilePic!!)
            else {
                holder.binding!!.civFriendAvatar.setImageResource(R.drawable.ic_avatar_place_holder)
            }
            if (mList[position].userId.toString() == HooleyApp.db.getString(Constants.USER_ID)) {
                holder.binding!!.tvAddFriend.visibility = View.GONE
                holder.binding!!.tvAccept.visibility = View.GONE
                holder.binding!!.tvFriend.visibility = View.GONE
                holder.binding!!.tvRequested.visibility = View.GONE
            } else {
                when (mList[position].status) {
                    Constants.TYPE_NONE -> {
                        holder.binding!!.tvAddFriend.visibility = View.VISIBLE
                        holder.binding!!.tvFriend.visibility = View.GONE
                        holder.binding!!.tvRequested.visibility = View.GONE
                        holder.binding!!.tvAccept.visibility = View.GONE
                    }
                    Constants.TYPE_PENDING_SENT -> {
                        holder.binding!!.tvAccept.visibility = View.GONE
                        holder.binding!!.tvAddFriend.visibility = View.GONE
                        holder.binding!!.tvFriend.visibility = View.GONE
                        holder.binding!!.tvRequested.visibility = View.VISIBLE
                    }
                    Constants.TYPE_FRIEND -> {
                        holder.binding!!.tvAddFriend.visibility = View.GONE
                        holder.binding!!.tvFriend.visibility = View.VISIBLE
                        holder.binding!!.tvRequested.visibility = View.GONE
                        holder.binding!!.tvAccept.visibility = View.GONE
                    }
                    Constants.TYPE_PENDING_RECEIVED -> {
                        holder.binding!!.tvAddFriend.visibility = View.GONE
                        holder.binding!!.tvAccept.visibility = View.VISIBLE
                        holder.binding!!.tvFriend.visibility = View.GONE
                        holder.binding!!.tvRequested.visibility = View.GONE
                    }
                }
            }
            holder.binding!!.tvAddFriend.setOnClickListener {
                mListener.onClickAddFriend(position)
            }
            holder.binding!!.tvAccept.setOnClickListener {
                mListener.onClickAcceptFriend(position)
            }
            holder.binding!!.rlItem.setOnClickListener {
                mListener.onClickItem(position)
            }
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }
    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }


    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterPastEventFriendsBinding? = null

        init {
            binding = DataBindingUtil.bind(v)

        }
    }
}