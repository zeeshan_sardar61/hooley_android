package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterShareIncomePartnerBinding
import com.hooleyapp.hooley.model.ShareIncomePartnerModel
import com.hooleyapp.hooley.others.Util
import java.util.*

class AdapterShareIncome(private val context: Context, private val mList: ArrayList<ShareIncomePartnerModel>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal lateinit var binding: AdapterShareIncomePartnerBinding


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_share_income_partner, parent, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }

        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            holder.binding!!.tvUserName.text = mList!![position].friendsTable.fullName
            if (!TextUtils.isEmpty(mList[position].friendsTable.profilePic))
                Glide.with(context).load(mList[position].friendsTable.profilePic).into(holder.binding!!.ivUserProfile)
            holder.binding!!.rlAddAmount.visibility = View.GONE
            holder.binding!!.tvCityName.text = mList[position].friendsTable.cityName + ", " + mList[position].friendsTable.stateName
            holder.binding!!.rlMainBody.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, (R.color.base_color)))
            if (mList[position].percent != 0) {
                holder.binding!!.tvShareIncome.text = mList[position].percent.toString() + "%"
                holder.binding!!.tvShareIncome.visibility = View.VISIBLE
                holder.binding!!.ivClose.visibility = View.VISIBLE
                holder.binding!!.rlAddAmount.visibility = View.GONE

            } else {
                holder.binding!!.tvShareIncome.visibility = View.GONE
                holder.binding!!.ivClose.visibility = View.GONE
                //                holder.userSearchbinding.rlAddAmount.setVisibility(View.GONE);
            }
            holder.binding!!.rlMainBody.setOnClickListener {
                if (mList[position].percent == 0) {
                    holder.binding!!.rlAddAmount.visibility = View.VISIBLE
//                    holder.userSearchbinding!!.rlMainBody.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, (R.color.income_partner)))
                }
            }
            holder.binding!!.btnSave.setOnClickListener(View.OnClickListener {
                if (TextUtils.isEmpty(holder.binding!!.edtPrice.text.toString())) {
                    Util.showToastMessage(HooleyMain.activity!!, "please select amount")
                    return@OnClickListener
                }
                mList[position].percent = Integer.parseInt(holder.binding!!.edtPrice.text.toString())
                notifyDataSetChanged()
                holder.binding!!.rlMainBody.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, (R.color.income_partner)))
                holder.binding!!.edtPrice.setText("")
            })
            holder.binding!!.ivClose.setOnClickListener {
                mList[position].percent = 0
                notifyDataSetChanged()
            }
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterShareIncomePartnerBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

}
