package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.dhruvvaishnav.sectionindexrecyclerviewlib.IndicatorScrollRecyclerView
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.AdapterPhoneContactBinding
import com.hooleyapp.hooley.model.PhoneContactsModel


/**
 * Created by Nauman on 12/29/2017.
 */

class AdapterPhoneContact(private val context: Context, private val mList: List<PhoneContactsModel>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), IndicatorScrollRecyclerView.SectionedAdapter {
    internal lateinit var binding: AdapterPhoneContactBinding
    private var selectedPosition = -1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_phone_contact, parent, false)
        } catch (e: InflateException) {
            /* map is already there, just return view as it is */
        }

        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            holder.binding!!.tvContactName.text = mList!![position].cnName
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

        holder.binding!!.cbAddFriend.isChecked = mList!![position].checked!!
        if (mList[position].alreadySend!!) {
            holder.binding!!.btnSendAgain.visibility = View.VISIBLE
        } else {
            holder.binding!!.btnSendAgain.visibility = View.GONE
        }

        if (selectedPosition == position) {
            mList[position].checked = !mList[position].checked!!
            holder.binding!!.cbAddFriend.isChecked = mList[position].checked!!
        } else {
            mList[position].checked = false
            holder.binding!!.cbAddFriend.isChecked = mList[position].checked!!
        }
        holder.binding!!.cbAddFriend.setOnClickListener {
            //                mList.get(position).setChecked(!mList.get(position).getChecked());
            //                if (mList.get(position).getChecked()) {
            //                    holder.userSearchbinding.cbAddFriend.setChecked(true);
            //                } else {
            //                    holder.userSearchbinding.cbAddFriend.setChecked(false);
            //                }
            selectedPosition = position
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }


    override fun getSectionName(position: Int): String {
        return mList!![position].cnName[0].toString()
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterPhoneContactBinding? = null

        init {
            binding = DataBindingUtil.bind(v)

        }
    }
}