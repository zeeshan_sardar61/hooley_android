package com.hooleyapp.hooley.adapters

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.fragments.others.MoreMediaDialogFragment
import java.util.*

class AdapterMoreMediaDialoge(internal var context: Context, internal var builder: MoreMediaDialogFragment.Builder, titles: ArrayList<MoreItem>) : RecyclerView.Adapter<AdapterMoreMediaDialoge.MoreDialogeViewHolder>() {
    internal var onItemClickListener: OnItemClickListener? = null
    internal var titles = ArrayList<MoreItem>()

    init {
        this.titles = titles
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoreDialogeViewHolder {
        val view = View.inflate(context, R.layout.item_bottom_picker, null)
        return MoreDialogeViewHolder(view)
    }


    override fun onBindViewHolder(holder: AdapterMoreMediaDialoge.MoreDialogeViewHolder, position: Int) {

        holder.tvTitle.text = titles[position].itemName
        if (titles[position].isCheck!!) {
            holder.ivChecked.visibility = View.VISIBLE
        } else {
            holder.ivChecked.visibility = View.GONE
        }

        if (position == titles.size - 1) {
            holder.itemView.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.bg_dialog_last_item)
            holder.view1.visibility = View.GONE
        }



        if (onItemClickListener != null) {
            holder.itemView.setOnClickListener { onItemClickListener!!.onItemClick(holder.itemView, position, builder.type) }
        }
    }


    override fun getItemCount(): Int {
        return titles.size
    }


    fun setOnItemClickListener(
            onItemClickListener: AdapterMoreMediaDialoge.OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int, Type: Int)
    }


    inner class MoreDialogeViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var view1: View
        var ivChecked: ImageView
        var tvTitle: TextView

        init {
            tvTitle = view.findViewById(R.id.tvTitle)
            ivChecked = view.findViewById(R.id.ivChecked)
            view1 = view.findViewById(R.id.view_dialog)
        }
    }

    class MoreItem(var itemName: String, var isCheck: Boolean?)
}
