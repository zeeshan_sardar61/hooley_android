package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterMyHostEventBinding
import com.hooleyapp.hooley.model.MyHostEventModel
import com.hooleyapp.hooley.others.DateUtils

/**
 * Created by Nauman on 1/18/2018.
 */

class AdapterMyHostEvent(private val context: Context, private val mList: List<MyHostEventModel.HostEvents>?, private val isPublish: Boolean) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal lateinit var binding: AdapterMyHostEventBinding
    // This object helps you save/restore the open/close state of each view
    private val viewBinderHelper = ViewBinderHelper()
    lateinit var mListener: IMyEventsClickListeners


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_my_host_event, parent, false)
        } catch (e: InflateException) {

        }

        return ItemViewHolder(binding.root)
    }


    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
//            if (mList!![position].isPastEvent) {
//                holder.userSearchbinding!!.rlEdit.visibility = View.GONE
//            } else {
//                holder.userSearchbinding!!.rlEdit.visibility = View.VISIBLE
//            }
            holder.binding!!.tvEventName.text = mList!![position].eventName
            holder.binding!!.tvInterestedPeople.text = mList[position].peopleCount!! + " people interested"
            if (mList[position].isIndefinetly) {
                holder.binding!!.tvEventTimeStamp.text = "Event Postponed indefinitely"
                holder.binding!!.tvEventTimeStamp.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.live_red))
            } else {
                holder.binding!!.tvEventTimeStamp.text = DateUtils.displayDateOnly(DateUtils.getLocalDate(mList[position].startTime!!))
            }
//            viewBinderHelper.bind(holder.userSearchbinding!!.srLayout, mList[position].id)
            Glide.with(context).load(mList[position].coverPhoto).into(holder.binding!!.ivEventAvatar)
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

        if (!isPublish) {
            holder.binding!!.rlAlert.visibility = View.GONE
        }

//        holder.userSearchbinding!!.rlEdit.setOnClickListener {
//            mListener.onClickEdit(position)
//        }

        holder.binding!!.tvEventTimeStamp.setOnClickListener {
            mListener.onClickItem(position)

        }
        holder.binding!!.tvEventName.setOnClickListener {
            mListener.onClickItem(position)
        }

        holder.binding!!.rlAlert.setOnClickListener {
            mListener.onClickAlert(position)
        }

//        holder.userSearchbinding!!.rlItem.setOnLongClickListener {
//            mListener.onLongClick(position)
//            return@setOnLongClickListener true
//        }

        if (mList!![position].isAlertOn) {
            holder.binding!!.ivEventAlert.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_active_alert)
            holder.binding!!.tvEventAlert.text = "Active"
            holder.binding!!.tvEventAlert.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
        } else {
            holder.binding!!.ivEventAlert.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_un_active_alert)
            holder.binding!!.tvEventAlert.text = "Set Active"
            holder.binding!!.tvEventAlert.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.text_color))
        }
    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterMyHostEventBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

    interface IMyEventsClickListeners {
        fun onClickItem(position: Int)

        fun onClickEdit(position: Int)

        fun onClickAlert(position: Int)

        fun onLongClick(position: Int)
    }
}