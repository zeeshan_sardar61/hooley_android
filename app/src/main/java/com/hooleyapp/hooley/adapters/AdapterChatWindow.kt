package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.activites.VideoPlayerActivity
import com.hooleyapp.hooley.app.data.model.messages.GetAllMessageModel
import com.hooleyapp.hooley.databinding.AdapterChatStreamReceiverItemBinding
import com.hooleyapp.hooley.databinding.AdapterChatStreamSenderItemBinding
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.helper.ChatImageOverlayView
import com.hooleyapp.hooley.helper.handleSamplingAndRotationBitmap
import com.hooleyapp.hooley.helper.load
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.stfalcon.frescoimageviewer.ImageViewer
import java.util.*

/**
 * Created by Nauman on 4/24/2018.
 */

class AdapterChatWindow(private val context: Context, private val friendId: String, private val arrayList: ArrayList<GetAllMessageModel.MessageInfo>, val blocked: Boolean) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val VIEW_SENDER = 0
    private val VIEW_RECEIVER = 1
    private var overlayView: ChatImageOverlayView? = null
    private var imageViewer: ImageViewer? = null
    private var mListener: IChatWindowClickListener? = null

    private val customFormatter: ImageViewer.Formatter<String>
        get() = ImageViewer.Formatter { customImage -> customImage }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_SENDER) {
            val binding: AdapterChatStreamSenderItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_chat_stream_sender_item, parent, false)
            SenderViewHolder(binding.root)
        } else {
            val binding: AdapterChatStreamReceiverItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_chat_stream_receiver_item, parent, false)
            ReceiverViewHolder(binding.root)
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {

        if (getItemViewType(position) == VIEW_SENDER) {
            val holder = viewHolder as SenderViewHolder
            holder.bindDataWithSenderHolder(arrayList[position])

            holder.binding.ivSenderAvatar.setOnClickListener {
                if (!blocked) {
                    var avatarfragment = ViewAvatarFragment(arrayList[position].senderId!!.toInt())
                    avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
                }
            }
            holder.binding.ivVideoPlay.setOnClickListener { (HooleyMain.activity as ActivityBase).addFragment(R.id.container, VideoPlayerActivity.newInstance(arrayList[position].mediaUrl), "VideoPlayerActivity") }
            holder.binding.ivImage.setOnClickListener { onImageViewClick(arrayList[position].mediaUrl) }

        } else {
            val holder = viewHolder as ReceiverViewHolder
            holder.bindDataWithReceiverHolder(arrayList[position])
            holder.binding.ivVideoPlay.setOnClickListener {
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, VideoPlayerActivity.newInstance(arrayList[position].mediaUrl), "VideoPlayerActivity")
            }
            holder.binding.ivImage.setOnClickListener { onImageViewClick(arrayList[position].mediaUrl) }

            holder.binding.ivImage.setOnLongClickListener {
                mListener!!.onItemLongClick(position)
                false
            }

            holder.binding.ivVideo.setOnLongClickListener {
                mListener!!.onItemLongClick(position)
                false
            }
            holder.binding.ivReceiverAvatar.setOnClickListener {
                var avatarfragment = ViewAvatarFragment(friendId.toInt())
                avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (arrayList[position].senderId.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true)) 0 else 1
    }


    override fun getItemCount(): Int {
        return arrayList.size
    }


    inner class SenderViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterChatStreamSenderItemBinding = DataBindingUtil.bind(v)!!

        fun bindDataWithSenderHolder(item: GetAllMessageModel.MessageInfo) {
            if (item.type == null)
                return
            if (!TextUtils.isEmpty(item.profilePic))
                binding.ivSenderAvatar.load(item.profilePic!!)
            else
                Glide.with(HooleyMain.activity!!).load(R.drawable.ic_avatar_place_holder).into(binding.ivSenderAvatar)
            binding.tvTimeStamp.text = DateUtils.convertSingleDate(DateUtils.getLocalDate(item.sentTime!!))
            when (item.type) {
                Constants.TYPE_CHAT_TEXT -> {
                    binding.tvMessage.text = item.messageText
                    binding.ivImage.visibility = View.GONE
                    binding.ivVideo.visibility = View.GONE
                    binding.tvMessage.visibility = View.VISIBLE
                    binding.ivVideoPlay.visibility = View.GONE
                    binding.pbUploadImage.visibility = View.GONE
                    binding.rlMediaShare.visibility = View.GONE
                }
                Constants.TYPE_CHAT_IMAGE -> {
                    if (item.mediaUrl.isNotEmpty()) {

                        if (item.mediaUrl.startsWith("http")) {
                            binding.ivImage.load(item.thumbnailUrl!!)
                        } else {
                            if (!item.mediaUrl.startsWith("file"))
                                item.mediaUrl = "file://" + item.mediaUrl
                            Glide.with(HooleyMain.activity!!).load(Uri.parse(item.mediaUrl)).into(binding.ivImage)
                            try {
                                binding.ivImage.setImageBitmap(HooleyMain.activity!!.handleSamplingAndRotationBitmap(Uri.parse(item.mediaUrl)))
                            } catch (e: Exception) {
                            }
                        }
                    }
                    binding.rlMediaShare.visibility = View.VISIBLE
                    binding.ivImage.visibility = View.VISIBLE
                    binding.ivVideo.visibility = View.GONE
                    binding.tvMessage.visibility = View.GONE
                    binding.ivVideoPlay.visibility = View.GONE
                    if (item.isLoaderShow)
                        binding.pbUploadImage.visibility = View.VISIBLE
                    else
                        binding.pbUploadImage.visibility = View.GONE
                }
                Constants.TYPE_CHAT_VIDEO -> {
                    if (item.mediaUrl.startsWith("http")) {
                        binding.ivVideo.load(item.mediaUrl)
                    } else {
                        if (!item.mediaUrl.startsWith("file"))
                            item.mediaUrl = "file://" + item.mediaUrl
                        binding.ivVideo.load(Uri.parse(item.mediaUrl).toString())
                    }
                    if (item.isLoaderShow)
                        binding.pbUploadImage.visibility = View.VISIBLE
                    else
                        binding.pbUploadImage.visibility = View.GONE

                    binding.rlMediaShare.visibility = View.VISIBLE
                    binding.ivImage.visibility = View.GONE
                    binding.ivVideo.visibility = View.VISIBLE
                    binding.tvMessage.visibility = View.GONE
                    binding.ivVideoPlay.visibility = View.VISIBLE
                }
            }
        }

    }

    inner class ReceiverViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterChatStreamReceiverItemBinding = DataBindingUtil.bind(v)!!

        fun bindDataWithReceiverHolder(item: GetAllMessageModel.MessageInfo) {
            if (item.type == null)
                return
            when (item.type) {
                Constants.TYPE_CHAT_TEXT -> {
                    binding.tvMessage.text = item.messageText
                    binding.ivImage.visibility = View.GONE
                    binding.ivVideo.visibility = View.GONE
                    binding.tvMessage.visibility = View.VISIBLE
                    binding.ivVideoPlay.visibility = View.GONE
                    binding.rlMediaMsg.visibility = View.GONE
                }
                Constants.TYPE_CHAT_IMAGE -> {
                    if (item.mediaUrl.isNotEmpty()) {
                        if (item.mediaUrl.startsWith("http")) {
                            binding.ivImage.load(item.thumbnailUrl!!)
                        } else {
                            if (!item.mediaUrl.startsWith("file")) {
                                item.mediaUrl = "file://" + item.mediaUrl
                                try {
                                    binding.ivImage.setImageBitmap(HooleyMain.activity!!.handleSamplingAndRotationBitmap(Uri.parse(item.mediaUrl)))
                                } catch (e: Exception) {
                                }
                            }
                        }
                    }
                    binding.rlMediaMsg.visibility = View.VISIBLE
                    binding.ivImage.visibility = View.VISIBLE
                    binding.ivVideo.visibility = View.GONE
                    binding.tvMessage.visibility = View.GONE
                    binding.ivVideoPlay.visibility = View.GONE
                }
                Constants.TYPE_CHAT_VIDEO -> {
                    try {
                        if (item.mediaUrl.contains("Error")) {
                            arrayList.remove(item)
                        } else {
                            if (item.mediaUrl.startsWith("http")) {
                                Glide.with(HooleyMain.activity!!).load(item.mediaUrl).into(binding.ivVideo)
                            } else {
                                if (!item.mediaUrl.startsWith("file"))
                                    item.mediaUrl = "file://" + item.mediaUrl
                                Glide.with(HooleyMain.activity!!).load(Uri.parse(item.mediaUrl)).into(binding.ivVideo)
                            }
                            binding.rlMediaMsg.visibility = View.VISIBLE
                            binding.ivImage.visibility = View.GONE
                            binding.ivVideo.visibility = View.VISIBLE
                            binding.tvMessage.visibility = View.GONE
                            binding.ivVideoPlay.visibility = View.VISIBLE
                        }
                    } catch (e: java.lang.Exception) {

                    }
                }
            }
            if (item.sentTime!! != "Just Now")
                binding.tvTimeStamp.text = DateUtils.convertSingleDate(DateUtils.getLocalDate(item.sentTime!!))
            else
                binding.tvTimeStamp.text = DateUtils.convertSingleDate(item.sentTime!!)
            if (!TextUtils.isEmpty(item.profilePic))
                Glide.with(context).load(item.profilePic).into(binding.ivReceiverAvatar)

        }

    }

    fun setListener(listener: IChatWindowClickListener) {
        mListener = listener

    }

    fun onImageViewClick(imageUrl: String) {
        overlayView = ChatImageOverlayView(HooleyMain.activity!!)
        //        overlayView.setMListener(this);
        val imageArray = ArrayList<String>()
        imageArray.add(imageUrl)
        imageViewer = ImageViewer.Builder(HooleyMain.activity, imageArray)
                .setFormatter(customFormatter)
                .setStartPosition(0)
                .setOverlayView(overlayView)
                .show()
    }

    interface IChatWindowClickListener {
        fun onItemLongClick(position: Int)
    }


//    private fun bindDataWithSenderHolder(holder: SenderViewHolder, position: Int) {
//        if (arrayList[position].type == null)
//            return
//        when (arrayList[position].type) {
//            Constants.TYPE_CHAT_TEXT -> {
//                holder.userSearchbinding.tvMessage.text = arrayList[position].messageText
//                holder.userSearchbinding.ivImage.visibility = View.GONE
//                holder.userSearchbinding!!.ivVideo.visibility = View.GONE
//                holder.userSearchbinding!!.tvMessage.visibility = View.VISIBLE
//                holder.userSearchbinding!!.ivVideoPlay.visibility = View.GONE
//                holder.userSearchbinding!!.pbUploadImage.visibility = View.GONE
//                holder.userSearchbinding!!.rlMediaShare.visibility = View.GONE
//            }
//            Constants.TYPE_CHAT_IMAGE -> {
//                if (arrayList[position].messageText.startsWith("http")) {
//                    holder.userSearchbinding!!.ivImage.load(arrayList[position].messageText)
//                } else {
//                    if (!arrayList[position].messageText.startsWith("file"))
//                        arrayList[position].messageText = "file://" + arrayList[position].messageText
////                    Glide.with(HooleyMain.activity!!).load(Uri.parse(arrayList[position].messageText)).into(holder.userSearchbinding!!.ivImage)
//                    try {
//                        holder.userSearchbinding!!.ivImage.setImageBitmap(HooleyMain.activity!!.handleSamplingAndRotationBitmap(Uri.parse(arrayList[position].messageText)))
//                    } catch (e: Exception) {
//                    }
//                }
//                holder.userSearchbinding!!.rlMediaShare.visibility = View.VISIBLE
//                holder.userSearchbinding!!.ivImage.visibility = View.VISIBLE
//                holder.userSearchbinding!!.ivVideo.visibility = View.GONE
//                holder.userSearchbinding!!.tvMessage.visibility = View.GONE
//                holder.userSearchbinding!!.ivVideoPlay.visibility = View.GONE
//                if (arrayList[position].isLoaderShow)
//                    holder.userSearchbinding!!.pbUploadImage.visibility = View.VISIBLE
//                else
//                    holder.userSearchbinding!!.pbUploadImage.visibility = View.GONE
//            }
//            Constants.TYPE_CHAT_VIDEO -> {
//                if (arrayList[position].messageText.startsWith("http")) {
//                    holder.userSearchbinding!!.ivVideo.load(arrayList[position].messageText)
//                } else {
//                    if (!arrayList[position].messageText.startsWith("file"))
//                        arrayList[position].messageText = "file://" + arrayList[position].messageText
//                    holder.userSearchbinding!!.ivVideo.load(Uri.parse(arrayList[position].messageText).toString())
//                }
//                if (arrayList[position].isLoaderShow)
//                    holder.userSearchbinding!!.pbUploadImage.visibility = View.VISIBLE
//                else
//                    holder.userSearchbinding!!.pbUploadImage.visibility = View.GONE
//
//                holder.userSearchbinding!!.rlMediaShare.visibility = View.VISIBLE
//                holder.userSearchbinding!!.ivImage.visibility = View.GONE
//                holder.userSearchbinding!!.ivVideo.visibility = View.VISIBLE
//                holder.userSearchbinding!!.tvMessage.visibility = View.GONE
//                holder.userSearchbinding!!.ivVideoPlay.visibility = View.VISIBLE
//            }
//        }
//        holder.userSearchbinding!!.tvTimeStamp.text = DateUtils.convertSingleDate(DateUtils.getLocalDate(arrayList[position].sentTime!!))
//        if (!TextUtils.isEmpty(arrayList[position].profilePic))
//            holder.userSearchbinding!!.ivSenderAvatar.load(arrayList[position].profilePic!!)
//        else
//            Glide.with(HooleyMain.activity!!).load(R.drawable.ic_avatar_place_holder).into(holder.userSearchbinding!!.ivSenderAvatar)
//
//        holder.userSearchbinding!!.ivSenderAvatar.setOnClickListener {
//            if (!blocked) {
//                var avatarfragment = ViewAvatarFragment(arrayList[position].senderId!!.toInt())
//                avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
//            }
//        }
//        holder.userSearchbinding!!.ivVideoPlay.setOnClickListener {
//            (HooleyMain.activity as ActivityBase).callFragment(R.id.container, VideoPlayerActivity.newInstance(arrayList[position].messageText), "VideoPlayerActivity")
//        }
//        holder.userSearchbinding!!.ivImage.setOnClickListener { onImageViewClick(arrayList[position].messageText) }
//    }
//
//    private fun bindDataWithReceiverHolder(holder: ReceiverViewHolder, position: Int) {
//        if (arrayList[position].type == null)
//            return
//        when (arrayList[position].type) {
//            Constants.TYPE_CHAT_TEXT -> {
//                holder.userSearchbinding!!.tvMessage.text = arrayList[position].messageText
//                holder.userSearchbinding!!.ivImage.visibility = View.GONE
//                holder.userSearchbinding!!.ivVideo.visibility = View.GONE
//                holder.userSearchbinding!!.tvMessage.visibility = View.VISIBLE
//                holder.userSearchbinding!!.ivVideoPlay.visibility = View.GONE
//                holder.userSearchbinding!!.rlMediaMsg.visibility = View.GONE
//            }
//            Constants.TYPE_CHAT_IMAGE -> {
//                if (!arrayList[position].messageText.contains("Error")) {
//                    if (arrayList[position].messageText.startsWith("http")) {
//                        holder.userSearchbinding!!.ivImage.load(arrayList[position].messageText)
//                    } else {
//                        if (!arrayList[position].messageText.startsWith("file"))
//                            arrayList[position].messageText = "file://" + arrayList[position].messageText
//                        holder.userSearchbinding!!.ivImage.setImageBitmap(HooleyMain.activity!!.handleSamplingAndRotationBitmap(Uri.parse(arrayList[position].messageText)))
//                    }
//                }
//                holder.userSearchbinding!!.rlMediaMsg.visibility = View.VISIBLE
//                holder.userSearchbinding!!.ivImage.visibility = View.VISIBLE
//                holder.userSearchbinding!!.ivVideo.visibility = View.GONE
//                holder.userSearchbinding!!.tvMessage.visibility = View.GONE
//                holder.userSearchbinding!!.ivVideoPlay.visibility = View.GONE
//            }
//            Constants.TYPE_CHAT_VIDEO -> {
//                try {
//                    if (arrayList[position].messageText.contains("Error")) {
//                        arrayList.remove(arrayList[position])
//                    } else {
//                        if (arrayList[position].messageText.startsWith("http")) {
//                            Glide.with(HooleyMain.activity!!).load(arrayList[position].messageText).into(holder.userSearchbinding!!.ivVideo)
//                        } else {
//                            if (!arrayList[position].messageText.startsWith("file"))
//                                arrayList[position].messageText = "file://" + arrayList[position].messageText
//                            Glide.with(HooleyMain.activity!!).load(Uri.parse(arrayList[position].messageText)).into(holder.userSearchbinding!!.ivVideo)
//                        }
//                        holder.userSearchbinding!!.rlMediaMsg.visibility = View.VISIBLE
//                        holder.userSearchbinding!!.ivImage.visibility = View.GONE
//                        holder.userSearchbinding!!.ivVideo.visibility = View.VISIBLE
//                        holder.userSearchbinding!!.tvMessage.visibility = View.GONE
//                        holder.userSearchbinding!!.ivVideoPlay.visibility = View.VISIBLE
//                    }
//                } catch (e: java.lang.Exception) {
//
//                }
//            }
//        }
//        holder.userSearchbinding!!.tvTimeStamp.text = DateUtils.convertSingleDate(DateUtils.getLocalDate(arrayList[position].sentTime!!))
//        if (!TextUtils.isEmpty(arrayList[position].profilePic))
//            Glide.with(context).load(arrayList[position].profilePic).into(holder.userSearchbinding!!.ivReceiverAvatar)
//        holder.userSearchbinding!!.ivReceiverAvatar.setOnClickListener {
//
//            var avatarfragment = ViewAvatarFragment(friendId.toInt())
//            avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
//        }
//        holder.userSearchbinding!!.ivVideoPlay.setOnClickListener {
//            (HooleyMain.activity as ActivityBase).callFragment(R.id.container, VideoPlayerActivity.newInstance(arrayList[position].messageText), "VideoPlayerActivity")
//        }
//        holder.userSearchbinding!!.ivImage.setOnClickListener { onImageViewClick(arrayList[position].messageText) }
//
//        holder.userSearchbinding!!.ivImage.setOnLongClickListener {
//            mListener!!.onItemLongClick(position)
//            false
//        }
//
//        holder.userSearchbinding!!.ivVideo.setOnLongClickListener {
//            mListener!!.onItemLongClick(position)
//            false
//        }
//    }

}
