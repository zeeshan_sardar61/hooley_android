package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.AdapterMorePreferencesBinding
import com.hooleyapp.hooley.model.UserPreferencesModel
import java.util.*

/**
 * Created by Nauman on 3/6/2018.
 */

class AdapterUserMorePreferences(private val context: Context, var mList: ArrayList<UserPreferencesModel.UserAccessPrivacy>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal lateinit var binding: AdapterMorePreferencesBinding
    lateinit var mListener: MoreItemClick


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_more_preferences, parent, false)
        } catch (e: InflateException) {
            /* map is already there, just return view as it is */
        }

        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder

        holder.binding!!.tvname.text = mList!![position].name
        holder.binding!!.tvPrivacyName.text = mList!![position].accessPrivacyName

        holder.binding!!.rlPrivacy.setOnClickListener {
            mListener.onMoreItemClick(position, mList!![position])
        }

        if (position == mList!!.size - 1) {
            binding.vSep.visibility = View.GONE
        }


    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterMorePreferencesBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

    interface MoreItemClick {

        fun onMoreItemClick(position: Int, item: UserPreferencesModel.UserAccessPrivacy)

    }

}