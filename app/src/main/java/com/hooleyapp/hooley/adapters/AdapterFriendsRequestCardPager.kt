package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.view.PagerAdapter
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.ItemCardPagerBinding
import com.hooleyapp.hooley.fragments.messaging.ChatWindowFragment
import com.hooleyapp.hooley.interfaces.FriendClickListener
import com.hooleyapp.hooley.model.FriendRequestNotificationModel
import com.hooleyapp.hooley.others.Constants
import java.util.*

/**
 * Created by Nauman on 4/11/2018.
 */

class AdapterFriendsRequestCardPager(private val context: Context, private val mList: ArrayList<FriendRequestNotificationModel.FriendRequest>) : PagerAdapter() {
    lateinit var mListener: FriendClickListener
    private var binding: ItemCardPagerBinding? = null

    override fun getCount(): Int {
        return mList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = HooleyMain.activity!!.layoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.item_card_pager, container, false)
        if (!TextUtils.isEmpty(mList[position].profilePic))
            Glide.with(HooleyMain.activity!!).load(mList[position].profilePic).into(binding!!.ivAvatar)
        else
            binding!!.ivAvatar.setImageResource(R.drawable.ic_avatar_place_holder)
        if (!TextUtils.isEmpty(mList[position].fullName))
            binding!!.tvUserName.text = mList[position].fullName
        if (!TextUtils.isEmpty(mList[position].ageGroup))
            binding!!.tvAgeGroup.text = mList[position].ageGroup
        if (!TextUtils.isEmpty(mList[position].homeTown))
            binding!!.tvAddress.text = mList[position].homeTown

        binding!!.btnAccept.setOnClickListener { mListener.AcceptRequest(position, mList[position].SenderId, true) }
        binding!!.btnIgnore.setOnClickListener { mListener.AcceptRequest(position, mList[position].SenderId, false) }
        binding!!.btnViewProfile.setOnClickListener { mListener.onClickViewProfile(mList[position].SenderId, mList[position].fullName!!) }

        binding!!.btnMessage.setOnClickListener {
            HooleyMain.activity!!.supportActionBar!!.show()
            (HooleyMain.activity as ActivityBase).callFragment(R.id.container, ChatWindowFragment("", mList[position].SenderId.toString(), mList[position].fullName!!), "ChatWindowFragment")
        }
        binding!!.ivClose.setOnClickListener { HooleyMain.activity!!.supportFragmentManager.popBackStackImmediate() }
        if (mList[position].status!!.equals(Constants.TYPE_FRIEND, ignoreCase = true)) {
            binding!!.btnViewProfile.visibility = View.VISIBLE
            binding!!.btnAddFriend.visibility = View.GONE
            binding!!.btnMyProfile.visibility = View.GONE
            binding!!.btnMessage.visibility = View.VISIBLE
        } else if (mList[position].status!!.equals(Constants.TYPE_PENDING_SENT, ignoreCase = true)) {
            binding!!.llFriend.visibility = View.VISIBLE
            binding!!.llFriendRequest.visibility = View.GONE
            binding!!.btnViewProfile.visibility = View.GONE
            binding!!.btnAddFriend.visibility = View.VISIBLE
            binding!!.btnAddFriend.text = "Pending"
            binding!!.btnAddFriend.isEnabled = false
            binding!!.btnMyProfile.visibility = View.GONE
            binding!!.btnMessage.visibility = View.VISIBLE
        } else if (mList[position].status!!.equals(Constants.TYPE_PENDING_RECEIVED, ignoreCase = true)) {
            binding!!.llFriend.visibility = View.GONE
            binding!!.llFriendRequest.visibility = View.VISIBLE
        } else {
            binding!!.llFriend.visibility = View.VISIBLE
            binding!!.llFriendRequest.visibility = View.GONE
            binding!!.btnViewProfile.visibility = View.GONE
            binding!!.btnAddFriend.visibility = View.VISIBLE
            binding!!.btnMyProfile.visibility = View.GONE
            binding!!.btnMessage.visibility = View.VISIBLE

        }

        if (mList[position].SenderId.toString().equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true)) {
            binding!!.btnViewProfile.visibility = View.GONE
            binding!!.btnAddFriend.visibility = View.GONE
            binding!!.btnMessage.visibility = View.GONE
            binding!!.btnMyProfile.visibility = View.VISIBLE
        }

        container.addView(binding!!.root)
        return binding!!.root
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    fun setListener(listener: FriendClickListener) {
        this.mListener = listener
    }

    companion object {

        private val TINT = 0x66B2FF
    }

}