package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterInviteHooleyFriendsBinding
import com.hooleyapp.hooley.fragments.friends.FriendCardPagerFragment
import com.hooleyapp.hooley.tables.FriendsTable

class AdapterShowSelectedUser(private val context: Context, private val mList: ArrayList<FriendsTable>?, var showCheckBox: Boolean) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal lateinit var binding: AdapterInviteHooleyFriendsBinding
    lateinit var mListener: IInviteHooleyFriendClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_invite_hooley_friends, parent, false)
        } catch (e: InflateException) {
            /* map is already there, just return view as it is */
        }

        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            holder.binding!!.tvFriendName.text = mList!![position].fullName
            holder.binding!!.tvAgeGroup.text = "${mList[position].cityName}, ${mList[position].stateName}"

            if (!TextUtils.isEmpty(mList[position].profilePic))
                Glide.with(context).load(mList[position].profilePic).into(holder.binding!!.ivFriendAvatar)
            else
                holder.binding!!.ivFriendAvatar.setImageResource(R.drawable.ic_avatar_place_holder)
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }
        holder.binding!!.rlItemContainer.setOnClickListener {
            var friendCardFragment = FriendCardPagerFragment(position, mList!!)
            friendCardFragment.show(HooleyMain.activity!!.supportFragmentManager)
        }
        holder.binding!!.ivFriendAvatar.setOnClickListener {
            var friendCardFragment = FriendCardPagerFragment(position, mList!!)
            friendCardFragment.show(HooleyMain.activity!!.supportFragmentManager)
        }
        if (showCheckBox) {
            holder.binding!!.cbAddFriend.visibility = View.VISIBLE
            holder.binding!!.cbAddFriend.isChecked = mList!![position].isChecked
            holder.binding!!.cbAddFriend.setOnClickListener {
                mList[position].isChecked = !mList[position].isChecked
                holder.binding!!.cbAddFriend.isChecked = mList[position].isChecked
                mListener.onClickCheckItem(position)
            }
        } else {
            holder.binding!!.cbAddFriend.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterInviteHooleyFriendsBinding? = null

        init {
            binding = DataBindingUtil.bind(v)

        }
    }

    interface IInviteHooleyFriendClickListener {
        fun onClickCheckItem(position: Int)
    }
}