package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterPeopleNearMeBinding
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.tables.FriendsTable

/**
 * Created by Nauman on 4/24/2018.
 */

class AdapterSelectFriends(private val context: Context, internal var mList: List<FriendsTable>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var selectedPosition = -1
    private var mlistener: AdapterSelectInviteGuest.ISelectFriendListener? = null
    private val count = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<AdapterPeopleNearMeBinding>(LayoutInflater.from(parent.context), R.layout.adapter_people_near_me, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        if (!TextUtils.isEmpty(mList!![position].profilePic)) {
            Glide.with(context).load(mList!![position].profilePic).into(holder.binding!!.ivUserProfile)
        } else {
            Glide.with(context).load(R.drawable.ic_avatar_place_holder).into(holder.binding!!.ivUserProfile)
        }
        holder.binding!!.tvUserName.text = mList!![position].fullName


        holder.binding!!.tvUserDistance.visibility = View.VISIBLE
        holder.binding!!.tvUserDistance.text = mList!![position].cityName + ", " + mList!![position].stateName

        holder.binding!!.ivUserProfile.setOnClickListener {
            var avatarFragment = ViewAvatarFragment(mList!![position].userId)
            avatarFragment.show(HooleyMain.activity!!.supportFragmentManager)
        }

        holder.binding!!.rbSelectUser.setOnClickListener {
            if (HooleyApp.db.getString(Constants.CALL_TYPE) == "ADD_HOST") {
                selectedPosition = position
                notifyDataSetChanged()
            } else if (HooleyApp.db.getString(Constants.CALL_TYPE) == "GIFT_FRIEND") {
                selectedPosition = position
                notifyDataSetChanged()
            } else if (HooleyApp.db.getString(Constants.CALL_TYPE) == "CREATE_MESSAGE") {
                selectedPosition = position
                notifyDataSetChanged()
            } else if (HooleyApp.db.getString(Constants.CALL_TYPE) == "ADD_CO_HOST") {

                if (!mList!![position].isChecked) {
                    if (returnTrueCount() < 3) {
                        mList!![position].isChecked = !mList!![position].isChecked
                    } else {
                        Util.showToastMessage(HooleyMain.activity!!, "You can't select more than 3")
                    }
                } else {
                    mList!![position].isChecked = !mList!![position].isChecked

                }
                notifyDataSetChanged()
            } else {
                selectedPosition = position
                mList!![position].isChecked = !mList!![position].isChecked
                holder.binding!!.rbSelectUser.isChecked = mList!![position].isChecked
            }
        }

        if (HooleyApp.db.getString(Constants.CALL_TYPE) == "ADD_HOST" || HooleyApp.db.getString(Constants.CALL_TYPE) == "GIFT_FRIEND") {
            if (selectedPosition == position) {
                mList!![position].isChecked = !mList!![position].isChecked
                holder.binding!!.rbSelectUser.isChecked = mList!![position].isChecked
            } else {
                mList!![position].isChecked = false
                holder.binding!!.rbSelectUser.isChecked = mList!![position].isChecked
            }
        } else if (HooleyApp.db.getString(Constants.CALL_TYPE) == "CREATE_MESSAGE") {
            if (selectedPosition == position) {
                mList!![position].isChecked = !mList!![position].isChecked
                holder.binding!!.rbSelectUser.isChecked = mList!![position].isChecked
            } else {
                mList!![position].isChecked = false
                holder.binding!!.rbSelectUser.isChecked = mList!![position].isChecked
            }

        } else if (HooleyApp.db.getString(Constants.CALL_TYPE) == "ADD_CO_HOST") {
            holder.binding!!.rbSelectUser.isChecked = mList!![position].isChecked


        } else {
            holder.binding!!.rbSelectUser.isChecked = mList!![position].isChecked
        }
    }

    private fun returnTrueCount(): Int {
        var count = 0

        for (i in mList!!.indices) {
            if (mList!![i].isChecked)
                count++
        }
        return count
    }

    override fun getItemCount(): Int {
        return if (mList == null) 0 else mList!!.size
    }

    fun setMListener(listener: AdapterSelectInviteGuest.ISelectFriendListener) {
        mlistener = listener
    }

    interface ISelectFriendListener {
        fun onFriendSelected(position: Int)
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterPeopleNearMeBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

}
