package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.LayoutChildItemsBinding
import com.hooleyapp.hooley.tables.FriendsTable

/**
 * Created by Nauman on 4/23/2018.
 */

class AdapterAddCoHost(private val context: Context, internal var mList: ArrayList<FriendsTable>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    internal lateinit var binding: LayoutChildItemsBinding
    lateinit var mListener: onListItemRemovedListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.layout_child_items, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder

        if (!TextUtils.isEmpty(mList!![position].profilePic))
            Glide.with(context).load(mList!![position].profilePic).into(holder.binding!!.ivFriendAvatar)
        holder.binding!!.tvFriendName.text = mList!![position].fullName
        holder.binding!!.ivClose.setOnClickListener {
            mList!!.removeAt(position)
            notifyDataSetChanged()
            mListener.onListSizeChange()
        }

        if (position == mList!!.size - 1) {
            holder.binding!!.viewLine.visibility = GONE
        }
    }

    override fun getItemCount(): Int {
        return if (mList == null) 0 else mList!!.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: LayoutChildItemsBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

    interface onListItemRemovedListener {

        fun onListSizeChange()
    }
}
