package com.hooleyapp.hooley.adapters

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterPostMediaBinding
import com.hooleyapp.hooley.helper.handleSamplingAndRotationBitmap
import com.hooleyapp.hooley.interfaces.IPostMediaClickListener
import com.hooleyapp.hooley.model.UpdatePostMediaItem
import com.hooleyapp.hooley.others.Constants
import java.util.*

class AdapteUpdatePostMedia(internal var mList: ArrayList<UpdatePostMediaItem>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    lateinit var mlistener: IPostMediaClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<AdapterPostMediaBinding>(LayoutInflater.from(parent.context), R.layout.adapter_post_media, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        if (position == mList!!.size && position < 6) {
            holder.binding!!.rlMedia.visibility = View.GONE
            holder.binding!!.rlAddMedia.visibility = View.VISIBLE
            holder.binding!!.rlAddMedia.setOnClickListener { mlistener.addMedia() }
        } else {
            if (position < 6) {
                holder.binding!!.rlAddMedia.visibility = View.GONE
                holder.binding!!.rlMedia.visibility = View.VISIBLE
                if (mList!![position].isVideo) {
                    if (mList!![position].type == Constants.TYPE_URL)
                        Glide.with(ActivityBase.activity).load(mList!![position].mediaUrl).into(holder.binding!!.ivEventCover)
                    else
                        Glide.with(ActivityBase.activity).load(mList!![position].mediaUri).into(holder.binding!!.ivEventCover)
                } else {
                    if (mList!![position].type == Constants.TYPE_URL)
                        Glide.with(ActivityBase.activity).load(mList!![position].mediaUrl).into(holder.binding!!.ivEventCover)
                    else
                        holder.binding!!.ivEventCover.setImageBitmap(HooleyMain.activity!!.handleSamplingAndRotationBitmap(mList!![position].mediaUri!!))
                }
            } else {
                holder.binding!!.rlAddMedia.visibility = View.GONE
                holder.binding!!.rlMedia.visibility = View.GONE
            }
            holder.binding!!.ivRemoveTag.setOnClickListener {
                mList!!.removeAt(position)
                notifyDataSetChanged()
            }
        }
    }

    override fun getItemCount(): Int {
        return if (mList == null) 0 else mList!!.size + 1
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterPostMediaBinding? = null

        init {
            binding = DataBindingUtil.bind(v)

        }
    }

}
