package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterMyTicketsBinding
import com.hooleyapp.hooley.model.MyTicketModel
import com.hooleyapp.hooley.others.DateUtils
import java.util.*

/**
 * Created by adilmalik on 07/05/2018.
 */

class AdapterMyTickets(private val context: Context, private val mList: ArrayList<MyTicketModel.MyTicketsList>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal lateinit var binding: AdapterMyTicketsBinding
    private var mListener: IMyTicketClickListener? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_my_tickets, parent, false)
        } catch (e: InflateException) {
            /* map is already there, just return view as it is */
        }

        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            holder.binding!!.tvEventName.text = mList!![position].eventName
            holder.binding!!.tvEventTimeStamp.text = DateUtils.displayDateOnly(DateUtils.getLocalDate(mList[position].startTime))
            Glide.with(context).load(mList[position].coverPhoto).into(holder.binding!!.ivEventAvatar)
            if (mList[position].alertOn!!) {
                holder.binding!!.ivTicketAlert.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_active_alert)
                holder.binding!!.tvTickerStatus.text = "Active"
                holder.binding!!.tvTickerStatus.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                holder.binding!!.tvAlterText.visibility = View.GONE
            } else {
                holder.binding!!.ivTicketAlert.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_un_active_alert)
                holder.binding!!.tvTickerStatus.text = "Set Alert"
                holder.binding!!.tvTickerStatus.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.text_color))
                holder.binding!!.tvAlterText.visibility = View.GONE
            }
            holder.binding!!.ivTicketAlert.setOnClickListener {
                mListener!!.setAlert(position)
            }
            holder.binding!!.rlEvent.setOnClickListener { mListener!!.onClickItem(position) }
            holder.binding!!.rlEvent.setOnLongClickListener {
                mListener!!.onLongClick(position)
                return@setOnLongClickListener true
            }

            if (mList[position].isGifted!!) {
                holder.binding!!.tvAlterText.visibility = View.VISIBLE
                holder.binding!!.tvTitle.visibility = View.VISIBLE
                holder.binding!!.tvAlterText.text = mList[position].giftedFrom
            } else {
                holder.binding!!.tvTitle.visibility = View.GONE
                holder.binding!!.tvAlterText.visibility = View.GONE
            }
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterMyTicketsBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

    fun setListener(listener: IMyTicketClickListener) {
        mListener = listener

    }

    interface IMyTicketClickListener {

        fun onClickItem(position: Int)

        fun setAlert(position: Int)

        fun onLongClick(position: Int)

    }

}