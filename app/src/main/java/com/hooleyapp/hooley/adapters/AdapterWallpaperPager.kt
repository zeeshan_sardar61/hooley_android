package com.hooleyapp.hooley.adapters

import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.helper.handleSamplingAndRotationBitmap
import com.hooleyapp.hooley.model.ItemCreateEventWallpaper
import com.hooleyapp.hooley.others.Constants
import java.util.*

/**
 * Created by Nauman on 2/6/2018.
 */

class AdapterWallpaperPager : PagerAdapter() {
    // This holds all the currently displayable views, in order from left to right.
    //    private ArrayList<View> views = new ArrayList<View>();
    private val mList = ArrayList<ItemCreateEventWallpaper>()

    //-----------------------------------------------------------------------------
    // Used by ViewPager.  "Object" represents the page; tell the ViewPager where the
    // page should be displayed, from left-to-right.  If the page no longer exists,
    // return POSITION_NONE.
    override fun getItemPosition(`object`: Any): Int {
        //        int index = views.indexOf(object);
        //        if (index == -1)
        return PagerAdapter.POSITION_NONE
        //        else
        //            return mList.;
    }

    //-----------------------------------------------------------------------------
    // Used by ViewPager.  Called when ViewPager needs a page to display; it is our job
    // to add the page to the container, which is normally the ViewPager itself.  Since
    // all our pages are persistent, we simply retrieve it from our "views" ArrayList.
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        //        View v = views.get(position);
        val inflater = HooleyMain.activity!!.layoutInflater
        val v = inflater.inflate(R.layout.fragment_event_wallpaper, container, false)
        val ivEventCover = v.findViewById<ImageView>(R.id.ivEventCover)
        //        if (!TextUtils.isEmpty(mList.get(position) )) {
        //            Glide.with(HooleyMain.activity).load(mList.get(position)).into(ivEventCover);
        //        }
        if (mList[position].type == Constants.TYPE_URI)
        //            Util.handleSamplingAndRotationBitmap(mList.get(position).uri,HooleyMain.activity);
        {
            //            Bitmap bm = Util.handleSamplingAndRotationBitmap(mList.get(position).uri, HooleyMain.activity);
            //            ivEventCover.setImageBitmap(bm);
            //            ivEventCover.setScaleType(ImageView.ScaleType.CENTER_CROP);
//                    Glide.with(ActivityBase.activity).load(Uri.parse(arrayList[position].messageText)).into(holder.userSearchbinding!!.ivImage)
            ivEventCover.setImageBitmap(HooleyMain.activity!!.handleSamplingAndRotationBitmap(mList[position].uri!!))
//            ivEventCover.setImageURI(mList[position].uri)

        } else
            Glide.with(HooleyMain.activity!!).load(mList[position].url).into(ivEventCover)//            ivEventCover.setImageURI(mList.get(position).uri);

        container.addView(v)
        return v
    }

    //-----------------------------------------------------------------------------
    // Used by ViewPager.  Called when ViewPager no longer needs a page to display; it
    // is our job to remove the page from the container, which is normally the
    // ViewPager itself.  Since all our pages are persistent, we do nothing to the
    // contents of our "views" ArrayList.
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }

    //-----------------------------------------------------------------------------
    // Used by ViewPager; can be used by app as well.
    // Returns the total number of pages that the ViewPage can display.  This must
    // never be 0.
    override fun getCount(): Int {
        return mList.size
    }

    //-----------------------------------------------------------------------------
    // Used by ViewPager.
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    //-----------------------------------------------------------------------------
    // Add "view" to right end of "views".
    // Returns the position of the new view.
    // The app should call this to add pages; not used by ViewPager.
    fun addView(item: ItemCreateEventWallpaper): Int {
        mList.add(item)
        return mList.size
    }

    //-----------------------------------------------------------------------------
    // Add "view" to right end of "views".
    // Returns the position of the new view.
    // The app should call this to add pages; not used by ViewPager.
    fun editView(position: Int, item: ItemCreateEventWallpaper): Int {
        mList[position] = item
        return position
    }

    //-----------------------------------------------------------------------------
    // Removes the "view" at "position" from "views".
    // Retuns position of removed view.
    // The app should call this to remove pages; not used by ViewPager.
    fun removeView(pager: ViewPager, position: Int): Int {
        // ViewPager doesn't have a delete method; the closest is to set the adapter
        // again.  When doing so, it deletes all its views.  Then we can delete the view
        // from from the adapter and finally set the adapter to the pager again.  Note
        // that we set the adapter to null before removing the view from "views" - that's
        // because while ViewPager deletes all its views, it will call destroyItem which
        // will in turn cause a null pointer ref.
        pager.adapter = null
        mList.removeAt(position)
        pager.adapter = this

        return position
    }

    //-----------------------------------------------------------------------------
    // Returns the "view" at "position".
    // The app should call this to retrieve a view; not used by ViewPager.
    //    public View getView(int position) {
    //        return views.get(position);
    //    }

    // Other relevant methods:

    // finishUpdate - called by the ViewPager - we don't care about what pages the
    // pager is displaying so we don't use this method.
}