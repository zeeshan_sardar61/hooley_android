package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterEventTypeAgeBinding
import com.hooleyapp.hooley.model.PersonalProfileModel
import java.util.*


/**
 * Created by Nauman on 12/19/2017.
 */

class AdapterEventAges(private val mContext: Context, internal var mlist: ArrayList<PersonalProfileModel.myObject>) : BaseAdapter() {
    internal lateinit var binding: AdapterEventTypeAgeBinding
    private var lastCheckedPosition = -1

    override fun getCount(): Int {
        return mlist.size
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getItem(position: Int): Any? {
        return mlist[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_event_type_age, parent, false)
        binding.tvCatName.text = mlist[position].name

        if (lastCheckedPosition != -1) {
            if (lastCheckedPosition == position) {
                mlist[lastCheckedPosition].isActive = !mlist[lastCheckedPosition].isActive
            } else {
                mlist[position].isActive = false
            }
        }

        if (mlist[position].isActive) {
            binding.rlItem.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_purple_gradiant)
            binding.tvCatName.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white))
        } else {
            binding.rlItem.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.base_color))
            binding.tvCatName.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.text_color))
        }

        binding.rlItem.setOnClickListener {
            lastCheckedPosition = position
            notifyDataSetChanged()

        }

        return binding.root
    }

}