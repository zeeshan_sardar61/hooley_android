package com.hooleyapp.hooley.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.AdapterPeopleNearMeBinding
import com.hooleyapp.hooley.tables.FriendsTable

class AdapterSelectFriendsWithList(private val context: Context, internal var mList: ArrayList<FriendsTable>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var mListener: CheckClickListener


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<AdapterPeopleNearMeBinding>(LayoutInflater.from(parent.context), R.layout.adapter_people_near_me, parent, false)
        return ItemViewHolder(binding.root)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        holder.bindHolderViewData(mList!![position])
        holder.binding!!.tvUserDistance.visibility = View.VISIBLE
        holder.binding!!.tvUserDistance.text = "${mList!![position].cityName} , ${mList!![position].stateName}"
        holder.binding!!.rbSelectUser.isChecked = mList!![position].isChecked
        holder.binding!!.rbSelectUser.setOnClickListener {
            mListener.checkClicked(position)

        }
    }

    override fun getItemCount(): Int {
        return if (mList == null) 0 else mList!!.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterPeopleNearMeBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }

        fun bindHolderViewData(friendTable: FriendsTable) {
            friendTable.profilePic.let {
                Glide.with(context).load(it).into(binding!!.ivUserProfile)
            }
            friendTable.fullName.let {
                binding!!.tvUserName.text = it
            }
            binding!!.rbSelectUser.isChecked = friendTable.isChecked
        }
    }

    interface CheckClickListener {

        fun checkClicked(position: Int)
    }

}
