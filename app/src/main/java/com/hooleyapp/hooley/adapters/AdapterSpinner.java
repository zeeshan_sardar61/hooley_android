package com.hooleyapp.hooley.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.hooleyapp.hooley.R;
import com.hooleyapp.hooley.activites.HooleyMain;
import com.hooleyapp.hooley.model.PersonalProfileModel;

import java.util.ArrayList;

/**
 * Created by Nauman on 12/13/2017.
 */

public class AdapterSpinner extends ArrayAdapter<String> {

    private final int HINT_BUTTON_DISABLED_COLOR = Color.parseColor("#BDBDBD");
    public ArrayList<PersonalProfileModel.myObject> mList;
    LayoutInflater inflater;
    private Context context;
    private int HINT_BUTTON_COLOR = Color.BLACK;

    public AdapterSpinner(Context context, int textViewResourceId, ArrayList<PersonalProfileModel.myObject> list) {
        super(context, textViewResourceId);
        this.context = context;
        this.mList = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View row = inflater.inflate(R.layout.sp_gender_drop_down_tem, parent, false);
        TextView label = row.findViewById(R.id.spinnerItem);
        View view = row.findViewById(R.id.view);
        try {
            label.setText(mList.get(position).getName());
            label.setTextColor(mList.get(position).getId().equalsIgnoreCase("0") ? HooleyMain.Companion.getActivity().getResources().getColor(R.color.app_purple_color) : ContextCompat.getColor(HooleyMain.Companion.getActivity(), R.color.text_color));
            label.setBackgroundColor(mList.get(position).getId().equalsIgnoreCase("0") ? HooleyMain.Companion.getActivity().getResources().getColor(R.color.app_bg_layout) : ContextCompat.getColor(HooleyMain.Companion.getActivity(), R.color.app_white_color));
            view.setVisibility(mList.get(position).getId().equalsIgnoreCase("0") ? View.VISIBLE : View.GONE);

        } catch (IndexOutOfBoundsException ex) {
        }
        return row;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = inflater.inflate(R.layout.sp_gender_group_item, parent, false);
        TextView label = row.findViewById(R.id.spinnerItem);
        try {
            label.setText(mList.get(position).getName());
            label.setTextColor(mList.get(position).getId().equalsIgnoreCase("0") ? HINT_BUTTON_DISABLED_COLOR : ContextCompat.getColor(HooleyMain.Companion.getActivity(), R.color.text_color));
        } catch (IndexOutOfBoundsException ex) {
        }
        return row;
    }

    @Override
    public int getCount() {
        super.getCount();
        return mList != null && mList.size() > 0 ? mList.size() : 0;
    }
}