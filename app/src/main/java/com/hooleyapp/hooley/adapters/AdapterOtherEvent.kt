package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterOtherEventBinding
import com.hooleyapp.hooley.model.MyHostEventModel
import com.hooleyapp.hooley.others.DateUtils

/**
 * Created by Nauman on 1/23/2018.
 */

class AdapterOtherEvent(private val context: Context, private val mList: List<MyHostEventModel.HostEvents>?, private val showAlert: Boolean?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal lateinit var binding: AdapterOtherEventBinding
    lateinit var mListener: IMyEventsClickListeners

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_other_event, parent, false)
        } catch (e: InflateException) {
        }

        return ItemViewHolder(binding.root)
    }


    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            holder.binding!!.tvEventName.text = mList!![position].eventName
            holder.binding!!.tvInterestedPeople.text = mList[position].peopleCount!! + " people interested"
            holder.binding!!.tvEventTimeStamp.text = DateUtils.displayDateOnly(DateUtils.getLocalDate(mList[position].startTime!!))
            Glide.with(context).load(mList[position].coverPhoto).into(holder.binding!!.ivEventAvatar)
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

        holder.binding!!.rlItem.setOnClickListener {
            mListener.onClickItem(position)
        }

        if (showAlert!!) {
            holder.binding!!.rlAlert.visibility = VISIBLE
        } else {
            holder.binding!!.rlAlert.visibility = GONE
        }

        holder.binding!!.rlAlert.setOnClickListener {
            mListener.onClickAlert(position)
        }

        holder.binding!!.rlItem.setOnLongClickListener {
            mListener.onLongClick(position)
            return@setOnLongClickListener true
        }

        if (mList!![position].isAlertOn) {
            holder.binding!!.ivEventAlert.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_active_alert)
            holder.binding!!.tvEventAlert.text = "Active"
            holder.binding!!.tvEventAlert.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
        } else {
            holder.binding!!.ivEventAlert.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_un_active_alert)
            holder.binding!!.tvEventAlert.text = "Set Active"
            holder.binding!!.tvEventAlert.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.text_color))
        }

    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterOtherEventBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

    interface IMyEventsClickListeners {

        fun onClickItem(position: Int)

        fun onClickAlert(position: Int)


        fun onLongClick(position: Int)

    }
}
