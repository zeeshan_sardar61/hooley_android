package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.AdapterOptionalCatBinding
import java.util.*

/**
 * Created by Nauman on 12/27/2017.
 */

class AdapterOptionCategory(private val mContext: Context, internal var mlist: ArrayList<String>) : BaseAdapter() {
    internal lateinit var binding: AdapterOptionalCatBinding

    override fun getCount(): Int {
        return mlist.size
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_optional_cat, parent, false)
        binding.tvCatName.text = mlist[position]
        binding.ivClose.setOnClickListener {
            mlist.removeAt(position)
            notifyDataSetChanged()
        }

        return binding.root
    }

}