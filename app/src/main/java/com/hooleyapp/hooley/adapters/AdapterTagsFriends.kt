package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterTagsFriendsBinding
import com.hooleyapp.hooley.interfaces.IInterestDeleteClick
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

/**
 * Created by Nauman on 2/19/2018.
 */

class AdapterTagsFriends(private val mContext: Context, internal var mlist: ArrayList<FriendsTable>) : BaseAdapter() {
    internal lateinit var binding: AdapterTagsFriendsBinding
    private var mListener: IInterestDeleteClick? = null


    override fun getCount(): Int {
        return mlist.size
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_tags_friends, parent, false)
        if (!TextUtils.isEmpty(mlist[position].profilePic))
            Glide.with(HooleyMain.activity!!).load(mlist[position].profilePic).into(binding.ivFriendAvatar)
        binding.ivClose.setOnClickListener { mListener!!.onClickItem(mlist[position].fullName, position) }
        return binding.root
    }


    fun setmListener(listener: IInterestDeleteClick) {
        this.mListener = listener
    }
}
