package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.view.ui.activites.HooleyGuestUser
import com.hooleyapp.hooley.databinding.AdapterMenuSearchBinding
import com.hooleyapp.hooley.databinding.AdapterSearchPastEventBinding
import com.hooleyapp.hooley.databinding.NearByEventsHeaderBinding
import com.hooleyapp.hooley.fragments.event.GuestUserLiveEventFragment
import com.hooleyapp.hooley.fragments.event.LiveEventFragment
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.interfaces.INearByEventClickListener
import com.hooleyapp.hooley.interfaces.INearByEventHeaderClickListener
import com.hooleyapp.hooley.interfaces.INearByMoreClickListener
import com.hooleyapp.hooley.model.NearByEventsFinalModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import java.util.*

/**
 * Created by Nauman on 4/6/2018.
 */

class AdapterSearchEvent(private val context: Context, list: ArrayList<NearByEventsFinalModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), INearByEventHeaderClickListener, INearByMoreClickListener {
    internal var mList = ArrayList<NearByEventsFinalModel>()
    private var mListener: INearByEventClickListener? = null

    init {
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == Constants.TYPE_MENU_EVENT) {
            val binding = DataBindingUtil.inflate<AdapterMenuSearchBinding>(LayoutInflater.from(parent.context), R.layout.adapter_menu_search, parent, false)
            return MenuHolder(binding.root)
        } else if (viewType == Constants.TYPE_PAST_EVENT) {
            val binding = DataBindingUtil.inflate<AdapterSearchPastEventBinding>(LayoutInflater.from(parent.context), R.layout.adapter_search_past_event, parent, false)
            return PastEventHolder(binding.root)
        } else {
            val binding = DataBindingUtil.inflate<NearByEventsHeaderBinding>(LayoutInflater.from(parent.context), R.layout.near_by_events_header, parent, false)
            return HeaderHolder(binding.root)
        }
    }


    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {


        if (mList[position].type == Constants.TYPE_MENU_EVENT) {
            val holder = mHolder as MenuHolder
            when (mList[position].id) {
                Constants.TYPE_UPCOMMING_EVENT -> {
                    holder.binding!!.rbUpComingEvent.setTextColor(ContextCompat.getColor(ActivityBase.activity, android.R.color.white))
                    holder.binding!!.rbUpComingEvent.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.bg_btn_left_radio)
                    holder.binding!!.rbPastEvent.setTextColor(ContextCompat.getColor(ActivityBase.activity, R.color.app_purple_color))
                    holder.binding!!.rbPastEvent.setBackgroundColor(ContextCompat.getColor(ActivityBase.activity, android.R.color.transparent))
                }
                Constants.TYPE_PAST_EVENT -> {
                    holder.binding!!.rbPastEvent.setTextColor(ContextCompat.getColor(ActivityBase.activity, android.R.color.white))
                    holder.binding!!.rbPastEvent.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.bg_btn_right_radio)
                    holder.binding!!.rbUpComingEvent.setTextColor(ContextCompat.getColor(ActivityBase.activity, R.color.app_purple_color))
                    holder.binding!!.rbUpComingEvent.setBackgroundColor(ContextCompat.getColor(ActivityBase.activity, android.R.color.transparent))
                }
            }
            holder.binding!!.rgTabs.setOnCheckedChangeListener { group, checkedId ->
                when (checkedId) {
                    R.id.rbUpComingEvent -> mListener!!.onUpComingEvent()
                    R.id.rbPastEvent -> {
                        holder.binding!!.rbPastEvent.setTextColor(ContextCompat.getColor(ActivityBase.activity, android.R.color.white))
                        holder.binding!!.rbPastEvent.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.bg_rb_default)
                        holder.binding!!.rbUpComingEvent.setTextColor(ContextCompat.getColor(ActivityBase.activity, R.color.app_purple_color))
                        holder.binding!!.rbUpComingEvent.setBackgroundColor(ContextCompat.getColor(ActivityBase.activity, android.R.color.transparent))
                        mListener!!.onPastEvent()
                    }
                }
            }

        } else if (mList[position].type == Constants.TYPE_PAST_EVENT) {
            val holder = mHolder as PastEventHolder
            setPastEvent(position, holder)
        } else {
            val holder = mHolder as HeaderHolder
            var typeName = ""
            if (mList[position].eventDetailList != null && mList[position].eventDetailList.size > 0) {

                when (mList[position].type) {
                    Constants.TYPE_TODAY_EVENT -> typeName = "Today"
                    Constants.TYPE_FEATURE_EVENT -> typeName = "Featured Events "

                    Constants.TYPE_NEXT_WEEK_EVENT -> typeName = "Next Week"
                    Constants.TYPE_COMMING_EVENT -> typeName = "Coming Weeks"
                }

                holder.binding!!.vpNearByHeader.clipToPadding = false
                val margin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (30 * 2).toFloat(), ActivityBase.activity.resources.displayMetrics).toInt()
                holder.binding!!.vpNearByHeader.pageMargin = -margin

                holder.binding!!.tvEventHeader.text = typeName
                val adapter = AdapterNearByEventsHeader(context, mList[position].eventDetailList, position)
                holder.binding!!.vpNearByHeader.adapter = adapter
                adapter.setListener(this)
            }

        }

    }

    override fun getItemViewType(position: Int): Int {
        return mList[position].type
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    private fun setPastEvent(pos: Int, holder: PastEventHolder) {
        if (mList[pos].eventDetail != null) {
            holder.binding!!.rlLive.visibility = View.GONE
            if (!TextUtils.isEmpty(mList[pos].eventDetail.coverPhoto))
                Glide.with(context).load(mList[pos].eventDetail.coverPhoto).into(holder.binding!!.ivEventCover)
            holder.binding!!.tvEventDate.text = DateUtils.displayDateOnly(DateUtils.getLocalDate(mList[pos].eventDetail.startTime!!))
            holder.binding!!.tvShare.text = Integer.toString(mList[pos].eventDetail.sharesCount)
            holder.binding!!.tvLike.text = Integer.toString(mList[pos].eventDetail.likesCount)
            holder.binding!!.tvFollow.text = Integer.toString(mList[pos].eventDetail.followersCount)
            holder.binding!!.tvEventName.text = mList[pos].eventDetail.eventName
            holder.binding!!.tvEventAddress.text = mList[pos].eventDetail.address
            if (mList[pos].eventDetail.isLiked) {
                holder.binding!!.ivLike.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_red_heart)
            } else {
                holder.binding!!.ivLike.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_purple_heart)
            }
            if (mList[pos].eventDetail.isFollowed) {
                holder.binding!!.ivFollow.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_filled_star)
            } else {
                holder.binding!!.ivFollow.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_follow_it)
            }


            if (mList[pos].eventDetail.isLive)
                holder.binding!!.ivLive.visibility = View.VISIBLE
            else
                holder.binding!!.ivLive.visibility = View.GONE

            holder.binding!!.ivEventCover.setOnClickListener {
                if (!HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)) {
                    (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment
                            .newInstance(mList[pos].eventDetail.eventId!!), "LiveEventFragment")
                    hideDrawer(HooleyMain.activity!!)
                } else {
                    (HooleyGuestUser.activity as ActivityBase).callFragment(R.id.container, GuestUserLiveEventFragment
                            .newInstance(mList[pos].eventDetail.eventId!!), "GuestUserLiveEventFragment")
                    hideDrawer(HooleyGuestUser.activity)

                }
            }
            holder.binding!!.ivLike.setOnClickListener {
                if (!HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)) {
                    var count = Integer.parseInt(holder.binding!!.tvLike.text.toString())
                    if (mList[pos].eventDetail.isLiked) {
                        holder.binding!!.ivLike.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_purple_heart)
                        count--
                        holder.binding!!.tvLike.text = Integer.toString(count)
                    } else {
                        holder.binding!!.ivLike.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_red_heart)
                        count++
                        holder.binding!!.tvLike.text = Integer.toString(count)
                    }
                }
                mListener!!.onClickLike(mList[pos], pos, pos)
            }
            holder.binding!!.ivFollow.setOnClickListener {
                if (!HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)) {
                    var count = Integer.parseInt(holder.binding!!.tvFollow.text.toString())
                    if (mList[pos].eventDetail.isFollowed) {
                        holder.binding!!.ivFollow.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_follow_it)
                        count--
                        holder.binding!!.tvFollow.text = Integer.toString(count)
                    } else {
                        holder.binding!!.ivFollow.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_filled_star)
                        count++
                        holder.binding!!.tvFollow.text = Integer.toString(count)
                    }
                }
                mListener!!.onClickFollow(mList[pos], pos, pos)
            }

            holder.binding!!.ivShare.setOnClickListener { mListener!!.onClickShare(mList[pos], pos) }

        }
    }

    fun setListener(listener: INearByEventClickListener) {
        this.mListener = listener
    }

    override fun onClickHeaderLike(position: Int, mListPosition: Int) {
        mListener!!.onClickLike(mList[mListPosition], position, mListPosition)
    }

    override fun onClickHeaderFollow(position: Int, mListPos: Int) {
        mListener!!.onClickFollow(mList[mListPos], position, mListPos)
    }

    override fun onClickHeaderComment() {

    }

    override fun onClickHeaderShare(position: Int, mListPosition: Int) {
        mListener!!.onClickShare(mList[mListPosition], position)
    }

    override fun onClickMoreLike(position: Int, type: Int, mListPosition: Int) {
        when (type) {
            Constants.TYPE_TODAY_EVENT -> mListener!!.onClickLike(mList[mListPosition], position, mListPosition)
            Constants.TYPE_NEXT_WEEK_EVENT -> mListener!!.onClickLike(mList[mListPosition], position, mListPosition)
            Constants.TYPE_COMMING_EVENT -> mListener!!.onClickLike(mList[mListPosition], position, mListPosition)
        }

    }


    inner class HeaderHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: NearByEventsHeaderBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

    inner class PastEventHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterSearchPastEventBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

    inner class MenuHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterMenuSearchBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }
}
