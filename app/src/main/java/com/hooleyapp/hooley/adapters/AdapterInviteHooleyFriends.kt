package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.dhruvvaishnav.sectionindexrecyclerviewlib.IndicatorScrollRecyclerView
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterInviteHooleyFriendsBinding
import com.hooleyapp.hooley.fragments.friends.FriendCardPagerFragment
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

/**
 * Created by Nauman on 1/5/2018.
 */

class AdapterInviteHooleyFriends(private val context: Context, private val mList: List<FriendsTable>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), IndicatorScrollRecyclerView.SectionedAdapter {
    internal lateinit var binding: AdapterInviteHooleyFriendsBinding
    lateinit var mListener: IInviteHooleyFriendClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_invite_hooley_friends, parent, false)
        } catch (e: InflateException) {
            /* map is already there, just return view as it is */
        }

        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            when {
                (mList!![position].header == Constants.HEADER_HOOLEY_FRIEND) -> {
                    holder.binding!!.llHeader.visibility = View.VISIBLE
                    holder.binding!!.tvHeaderText.text = "Hooley Friends"
                }
                (mList[position].header == Constants.HEADER_HOOLEY_USER) -> {
                    holder.binding!!.llHeader.visibility = View.VISIBLE
                    holder.binding!!.tvHeaderText.text = "Hooley Users"
                }
                else -> {
                    holder.binding!!.llHeader.visibility = View.GONE
                }
            }
            holder.binding!!.tvFriendName.text = mList[position].fullName
            if (!mList[position].cityName.isEmpty()) {
                holder.binding!!.tvAgeGroup.text = "${mList[position].cityName}, ${mList[position].stateName}"
            } else {
                holder.binding!!.tvAgeGroup.visibility = GONE
            }
            if (!TextUtils.isEmpty(mList[position].profilePic))
                Glide.with(context).load(mList[position].profilePic).into(holder.binding!!.ivFriendAvatar)
            else
                holder.binding!!.ivFriendAvatar.setImageResource(R.drawable.ic_avatar_place_holder)
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

        holder.binding!!.cbAddFriend.isChecked = mList!![position].isChecked
        holder.binding!!.cbAddFriend.setOnClickListener {
            mList[position].isChecked = !mList[position].isChecked
            holder.binding!!.cbAddFriend.isChecked = mList[position].isChecked
            mListener.onClickCheckItem(position)
        }

        holder.binding!!.ivFriendAvatar.setOnClickListener {
            var friendCardFragment = FriendCardPagerFragment(position, (mList as ArrayList<FriendsTable>?)!!)
            friendCardFragment.show(HooleyMain.activity!!.supportFragmentManager)
        }

        holder.binding!!.rlItem.setOnClickListener {
            var friendCardFragment = FriendCardPagerFragment(position, (mList as ArrayList<FriendsTable>?)!!)
            friendCardFragment.show(HooleyMain.activity!!.supportFragmentManager)
        }
    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    override fun getSectionName(position: Int): String {
        return mList!![position].fullName[0].toString()

    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterInviteHooleyFriendsBinding? = null

        init {
            binding = DataBindingUtil.bind(v)

        }
    }

    interface IInviteHooleyFriendClickListener {
        fun onClickCheckItem(position: Int)
    }
}