package com.hooleyapp.hooley.adapters

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import java.io.File
import java.util.*

/**
 * Created by Zeeshan on 22-Jan-18.
 */

class AdapterMoreDialoge(internal var context: Context, internal var builder: MoreDialogeFragment.Builder, titles: ArrayList<String>) : RecyclerView.Adapter<AdapterMoreDialoge.MoreDialogeViewHolder>() {
    internal var onItemClickListener: OnItemClickListener? = null
    internal var titles = ArrayList<String>()

    init {
        this.titles = titles

        var cursor: Cursor? = null
        try {
            val columns: Array<String>
            val orderBy: String
            val uri: Uri
            if (builder.mediaType == MoreDialogeFragment.Builder.MediaType.IMAGE) {
                uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                columns = arrayOf(MediaStore.Images.Media.DATA)
                orderBy = MediaStore.Images.Media.DATE_ADDED + " DESC"
            } else {
                uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                columns = arrayOf(MediaStore.Video.VideoColumns.DATA)
                orderBy = MediaStore.Video.VideoColumns.DATE_ADDED + " DESC"
            }

            cursor = context.applicationContext.contentResolver.query(uri, columns, null, null, orderBy)

            if (cursor != null) {

                var count = 0
                while (cursor.moveToNext() && count < builder.previewMaxCount) {

                    val dataIndex: String
                    if (builder.mediaType == MoreDialogeFragment.Builder.MediaType.IMAGE) {
                        dataIndex = MediaStore.Images.Media.DATA
                    } else {
                        dataIndex = MediaStore.Video.VideoColumns.DATA
                    }
                    val imageLocation = cursor.getString(cursor.getColumnIndex(dataIndex))
                    val imageFile = File(imageLocation)
                    count++

                }

            }

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (cursor != null && !cursor.isClosed) {
                cursor.close()
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoreDialogeViewHolder {
        val view = View.inflate(context, R.layout.item_bottom_picker, null)
        return MoreDialogeViewHolder(view)
    }


    override fun onBindViewHolder(holder: MoreDialogeViewHolder, position: Int) {

        holder.tvTitle.text = titles[position]

        if (onItemClickListener != null) {
            holder.itemView.setOnClickListener { onItemClickListener!!.onItemClick(holder.itemView, position, builder.type) }
        }

        if (titles[position].contains("Report") || titles[position].contains("Delete")) {
            holder.tvTitle.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_red_color))
        }

        if (position == titles.size - 1) {
            holder.itemView.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_dialog_last_item)
            holder.view1.visibility = View.GONE
        }
    }


    override fun getItemCount(): Int {
        return titles.size
    }


    fun setOnItemClickListener(
            onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int, Type: Int)
    }


    inner class MoreDialogeViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        //        TedSquareFrameLayout root;
        //        TedSquareImageView iv_thumbnail;
        var tvTitle: TextView
        var view1: View

        init {
            //            root = (TedSquareFrameLayout) view.findViewById(R.id.root);
            //            iv_thumbnail = (TedSquareImageView) view.findViewById(R.id.iv_thumbnail);
            tvTitle = view.findViewById(R.id.tvTitle)
            view1 = view.findViewById(R.id.view_dialog)
        }
    }
}
