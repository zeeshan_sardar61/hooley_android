package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.media.MyMediaAlbumModel
import com.hooleyapp.hooley.app.view.ui.fragments.media.MediaStreamDetailFragment
import com.hooleyapp.hooley.databinding.AdapterMyMediaAlbumBinding
import com.hooleyapp.hooley.fragments.event.LiveEventFragment
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.others.DateUtils
import java.util.*

/**
 * Created by Nauman on 4/3/2018.
 */

class AdapterMediaAlbum(private val context: Context, mList: ArrayList<MyMediaAlbumModel.EventAlbum>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    internal var mList = ArrayList<MyMediaAlbumModel.EventAlbum>()

    init {
        this.mList = mList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<AdapterMyMediaAlbumBinding>(LayoutInflater.from(parent.context), R.layout.adapter_my_media_album, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        if (position == mList.size - 1) {
            if (position % 2 != 0) {
                holder.binding.Seprator.visibility = View.VISIBLE
            } else {
                holder.binding.Seprator.visibility = View.GONE
            }
        }
        holder.bindWithData(mList[position])
        holder.binding.ivLive.setOnClickListener {
            (HooleyMain.activity as ActivityBase).addFragment(R.id.container, LiveEventFragment.newInstance(mList[position].eventId!!), "LiveEventFragment")
            hideDrawer(HooleyMain.activity!!)
        }
        holder.binding.ivCoverPhoto.setOnClickListener {
            (HooleyMain.activity as ActivityBase).addFragment(R.id.container, MediaStreamDetailFragment(mList[position].eventId!!, mList[position].eventName!!, mList[position].isLive), "MediaStreamDetailFragment")
            hideDrawer(HooleyMain.activity!!)
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var binding: AdapterMyMediaAlbumBinding = DataBindingUtil.bind(v)!!

        fun bindWithData(item: MyMediaAlbumModel.EventAlbum) {
            if (!TextUtils.isEmpty(item.thumbnailUrl))
                Glide.with(context).load(item.thumbnailUrl).into(binding.ivCoverPhoto)
            binding.tvEventTimeStamp.text = DateUtils.getDate(item.startTime!!)
            binding.tvEventName.text = item.eventName
            binding.tvAlbumCount.text = item.mediaCount.toString()
            if (item.isLive)
                binding.ivLive.visibility = View.VISIBLE
            else
                binding.ivLive.visibility = View.GONE
        }
    }

}
