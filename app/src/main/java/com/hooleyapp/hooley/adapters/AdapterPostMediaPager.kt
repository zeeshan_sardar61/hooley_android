package com.hooleyapp.hooley.adapters

import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.squareup.picasso.Picasso
import java.util.*

class AdapterPostMediaPager(// This holds all the currently displayable views, in order from left to right.
        private val mList: ArrayList<EventGalleryFeedModel.MediaFile>, private val listPosition: Int) : PagerAdapter() {
    private var mlistener: IClickPostListener? = null


    //-----------------------------------------------------------------------------
    // Used by ViewPager.  "Object" represents the page; tell the ViewPager where the
    // page should be displayed, from left-to-right.  If the page no longer exists,
    // return POSITION_NONE.
    //    @Override
    //    public int getItemPosition(Object object) {
    //        int index = views.indexOf(object);
    //        if (index == -1)
    //            return POSITION_NONE;
    //        else
    //            return mList.;
    //    }

    //-----------------------------------------------------------------------------
    // Used by ViewPager.  Called when ViewPager needs a page to display; it is our job
    // to add the page to the container, which is normally the ViewPager itself.  Since
    // all our pages are persistent, we simply retrieve it from our "views" ArrayList.
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        //        View v = views.get(position);
        val inflater = ActivityBase.activity.layoutInflater
        val v = inflater.inflate(R.layout.adapter_post_media_pager, container, false)
        val ivEventCover = v.findViewById<ImageView>(R.id.ivEventCover)
        val ivMovie = v.findViewById<ImageView>(R.id.ivMovie)
        val ivPicture = v.findViewById<ImageView>(R.id.ivPicture)


        if (!TextUtils.isEmpty(mList[position].thumbnailUrl)) {
            //            Glide.with(ActivityBase.activity).load(mList.get(position).thumbnailUrl).fit.into(ivEventCover);
            Picasso.with(ActivityBase.activity).load(mList[position].thumbnailUrl).fit().centerCrop().into(ivEventCover)
        }
        if (mList[position].isVideo!!) {

            ivMovie.visibility = View.VISIBLE
            ivPicture.visibility = View.GONE
        } else {
            ivMovie.visibility = View.GONE
            ivPicture.visibility = View.VISIBLE
        }
        ivEventCover.setOnClickListener { mlistener!!.onClickPost(position, listPosition) }

        container.addView(v)
        return v
    }

    //-----------------------------------------------------------------------------
    // Used by ViewPager.  Called when ViewPager no longer needs a page to display; it
    // is our job to remove the page from the container, which is normally the
    // ViewPager itself.  Since all our pages are persistent, we do nothing to the
    // contents of our "views" ArrayList.
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }

    //-----------------------------------------------------------------------------
    // Used by ViewPager; can be used by app as well.
    // Returns the total number of pages that the ViewPage can display.  This must
    // never be 0.
    override fun getCount(): Int {
        return mList.size
    }

    //-----------------------------------------------------------------------------
    // Used by ViewPager.
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }


    //-----------------------------------------------------------------------------
    // Removes the "view" at "position" from "views".
    // Retuns position of removed view.
    // The app should call this to remove pages; not used by ViewPager.
    fun removeView(pager: ViewPager, position: Int): Int {
        // ViewPager doesn't have a delete method; the closest is to set the adapter
        // again.  When doing so, it deletes all its views.  Then we can delete the view
        // from from the adapter and finally set the adapter to the pager again.  Note
        // that we set the adapter to null before removing the view from "views" - that's
        // because while ViewPager deletes all its views, it will call destroyItem which
        // will in turn cause a null pointer ref.
        pager.adapter = null
        mList.removeAt(position)
        pager.adapter = this

        return position
    }

    fun setMListener(listener: IClickPostListener) {
        mlistener = listener
    }

    interface IClickPostListener {
        fun onClickPost(position: Int, listPosition: Int)
    }

    //-----------------------------------------------------------------------------
    // Returns the "view" at "position".
    // The app should call this to retrieve a view; not used by ViewPager.
    //    public View getView(int position) {
    //        return views.get(position);
    //    }

    // Other relevant methods:

    // finishUpdate - called by the ViewPager - we don't care about what pages the
    // pager is displaying so we don't use this method.
}