package com.hooleyapp.hooley.adapters

import android.databinding.DataBindingUtil
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterTagsFriendsBinding
import com.hooleyapp.hooley.model.event.GetEditPostModel
import java.util.*

class AdapterUpdateTagFriends(internal var mlist: ArrayList<GetEditPostModel.TagUserItem>) : BaseAdapter() {
    internal lateinit var binding: AdapterTagsFriendsBinding
    var mListener: ITagClickListener? = null


    override fun getCount(): Int {
        return mlist.size
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_tags_friends, parent, false)
        if (!TextUtils.isEmpty(mlist[position].profilePic))
            Glide.with(HooleyMain.activity!!).load(mlist[position].profilePic).into(binding.ivFriendAvatar)
        binding.ivClose.setOnClickListener { mListener!!.onClickItem(position) }
        return binding.root
    }

    interface ITagClickListener {
        fun onClickItem(position: Int)
    }
}
