package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterPendingFriendsRequestBinding
import com.hooleyapp.hooley.fragments.friends.FriendCardPagerFragment
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

/**
 * Created by Nauman on 3/6/2018.
 */

class AdapterPendingFriendRequest(private val context: Context, private var mList: ArrayList<FriendsTable>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val service: FriendWebService
    internal lateinit var binding: AdapterPendingFriendsRequestBinding
    private val viewBinderHelper = ViewBinderHelper()

    init {
        service = FriendWebService()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_pending_friends_request, parent, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            holder.binding!!.tvFriendName.text = mList[position].fullName
            holder.binding!!.tvAddress.text = mList[position].cityName + ", " + mList[position].stateName
            viewBinderHelper.bind(holder.binding!!.srLayout, mList[position].userId.toString())
            if (!TextUtils.isEmpty(mList[position].profilePic)) {
                Glide.with(context).load(mList[position].profilePic).into(holder.binding!!.civFriendAvatar)
            } else {
                Glide.with(context).load(R.drawable.ic_avatar_place_holder).into(holder.binding!!.civFriendAvatar)
            }
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

        holder.binding!!.tvFriendName.setOnClickListener {
            var friendCardFragment = FriendCardPagerFragment(position, mList)
            friendCardFragment.show(HooleyMain.activity!!.supportFragmentManager)
        }
        holder.binding!!.tvAddress.setOnClickListener {
            var friendCardFragment = FriendCardPagerFragment(position, mList)
            friendCardFragment.show(HooleyMain.activity!!.supportFragmentManager)
        }
        holder.binding!!.rlAccept.setOnClickListener(View.OnClickListener {
            if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
                Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
                return@OnClickListener
            }
            service.acceptIgnoreFriendRequest(mList[position].userId, true, object : GenericCallback<GeneralModel> {
                override fun success(result: GeneralModel) {
                    mList.removeAt(position)
                    notifyDataSetChanged()
                }

                override fun failure(message: String) {
                    try {
                        HooleyMain.activity!!.showSuccessDialog(message)
                    } catch (ex: NullPointerException) {
                        ex.printStackTrace()
                    }

                }
            })
        })

        holder.binding!!.rlIgnore.setOnClickListener(View.OnClickListener {
            if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
                Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
                return@OnClickListener
            }
            service.acceptIgnoreFriendRequest(mList[position].userId, false, object : GenericCallback<GeneralModel> {
                override fun success(result: GeneralModel) {
                    mList.removeAt(position)
                    notifyDataSetChanged()
                }

                override fun failure(message: String) {
                    try {
                        HooleyMain.activity!!.showSuccessDialog(message)
                    } catch (ex: NullPointerException) {
                        ex.printStackTrace()
                    }

                }
            })
        })
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterPendingFriendsRequestBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

    fun filterList(filteredList: ArrayList<FriendsTable>) {
        mList = filteredList
        notifyDataSetChanged()
    }
}