package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.AdapterUserPreferencesBinding
import com.hooleyapp.hooley.model.UserPreferencesModel
import java.util.*

/**
 * Created by Nauman on 3/6/2018.
 */

class AdapterUserPreferences(private val context: Context, var mList: ArrayList<UserPreferencesModel.PrivacyCheckModel>?, var isFromInfo: Boolean) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal lateinit var binding: AdapterUserPreferencesBinding
    lateinit var mListener: ItemClick


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            //adapter_user_preferences
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_user_preferences, parent, false)
        } catch (e: InflateException) {
            /* map is already there, just return view as it is */
        }

        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder

//        Html.fromHtml(HooleyMain.activity!!.getString(R.string.str_enable_msg_alerts))

        holder.binding!!.tvName.text = Html.fromHtml(mList!![position].name)
        holder.binding!!.tbCheck.isChecked = mList!![position].isActive!!

        holder.binding!!.llItem.setOnClickListener {
            mListener.onCheckClick(position, mList!![position], isFromInfo)
        }

        if (position == mList!!.size - 1) {
            binding.vSep.visibility = View.GONE
        }

    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterUserPreferencesBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

    interface ItemClick {

        fun onCheckClick(position: Int, item: UserPreferencesModel.PrivacyCheckModel, fromInfo: Boolean)

    }

}