package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterMyFriendsBinding
import com.hooleyapp.hooley.fragments.friends.FriendCardPagerFragment
import com.hooleyapp.hooley.fragments.friends.FriendsProfileFragment
import com.hooleyapp.hooley.fragments.messaging.ChatWindowFragment
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.fragments.profile.ProfileMainFragment
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

/**
 * Created by Nauman on 3/6/2018.
 */

class AdapterMyFriends(private val context: Context, mList: ArrayList<FriendsTable>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal lateinit var binding: AdapterMyFriendsBinding
    private var service: FriendWebService? = null
    private val viewBinderHelper = ViewBinderHelper()
    internal var mList = ArrayList<FriendsTable>()

    init {
        this.mList = mList!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_my_friends, parent, false)
        } catch (e: InflateException) {
            /* map is already there, just return view as it is */
        }

        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            holder.binding!!.tvFriendName.text = mList[position].fullName
            holder.binding!!.tvAddress.text = mList[position].cityName + ", " + mList[position].stateName
            viewBinderHelper.bind(holder.binding!!.srLayout, mList[position].userId.toString())
            if (!TextUtils.isEmpty(mList[position].profilePic)) {
                Glide.with(context).load(mList[position].profilePic).into(holder.binding!!.civFriendAvatar)
            } else {
                Glide.with(context).load(R.drawable.ic_avatar_place_holder).into(holder.binding!!.civFriendAvatar)
            }
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

        if (mList[position].isFriend) {
            holder.binding!!.rlAddFriend.visibility = View.GONE
            holder.binding!!.rlViewProfile.visibility = View.VISIBLE
            binding.rlMyProfile.visibility = View.GONE
            binding.rlMessage.visibility = View.VISIBLE
        } else {
            if (mList[position].userId.toString().equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true)) {
                holder.binding!!.rlViewProfile.visibility = View.GONE
                holder.binding!!.rlAddFriend.visibility = View.GONE
                holder.binding!!.rlMessage.visibility = View.GONE
                holder.binding!!.rlMyProfile.visibility = View.VISIBLE
            } else {
                holder.binding!!.rlViewProfile.visibility = View.GONE
                holder.binding!!.rlAddFriend.visibility = View.VISIBLE
                binding.rlMyProfile.visibility = View.GONE
                binding.rlMessage.visibility = View.VISIBLE
            }
        }


        holder.binding!!.rlAddFriend.setOnClickListener {
            //Fixme this code is temprory
            sendFriendRequest(mList[position].userId, position)
        }
        holder.binding!!.rlMyProfile.setOnClickListener {
            (HooleyMain.activity as ActivityBase).callFragment(R.id.container, ProfileMainFragment.newInstance(false), "ProfileMainFragment")
            hideDrawer(HooleyMain.activity!!)
        }

        holder.binding!!.civFriendAvatar.setOnClickListener {
            var avatarFragment = ViewAvatarFragment(mList[position].userId)
            avatarFragment.show(HooleyMain.activity!!.supportFragmentManager)
        }
        holder.binding!!.rlItem.setOnClickListener {
            var friendCardFragment = FriendCardPagerFragment(position, mList)
            friendCardFragment.show(HooleyMain.activity!!.supportFragmentManager)
        }
        holder.binding!!.rlViewProfile.setOnClickListener { (HooleyMain.activity as ActivityBase).callFragment(R.id.container, FriendsProfileFragment.newInstance(mList[position].userId, mList[position].fullName), "FriendsProfileFragment") }

        holder.binding!!.rlMessage.setOnClickListener { (HooleyMain.activity as ActivityBase).callFragment(R.id.container, ChatWindowFragment("", mList[position].userId.toString(), mList[position].fullName), "ChatWindowFragment") }

        checkAndSet(position)

    }

    override fun getItemCount(): Int {
        return mList.size
    }

    private fun checkAndSet(position: Int) {
        if (mList[position].isMessageAccessible) {
            binding.rlMessage.visibility = View.VISIBLE
        } else {
            binding.rlMessage.isClickable = false
            binding.rlMessage.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_grey_gradiant)
        }
    }


    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterMyFriendsBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }


    private fun sendFriendRequest(friendId: Int, position: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        service = FriendWebService()
        val friendList = ArrayList<Int>()
        friendList.add(friendId)
        service!!.sendFriendRequest(friendList, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                Util.showToastMessage(HooleyMain.activity!!, "Friend Request Sent")
                //                mList.get(position).isfriend = true;
                //                notifyDataSetChanged();
                mList[position].status = Constants.TYPE_PENDING_SENT
            }

            override fun failure(message: String) {
                try {
                    HooleyMain.activity!!.showSuccessDialog(message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }
            }
        })
    }

    fun filterList(filteredList: ArrayList<FriendsTable>) {
        mList = filteredList
        notifyDataSetChanged()
    }

}