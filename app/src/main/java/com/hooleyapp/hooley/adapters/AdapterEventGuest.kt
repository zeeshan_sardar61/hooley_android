package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventDetailModel
import com.hooleyapp.hooley.databinding.AdapterEventGuestBinding
import java.util.*

/**
 * Created by Zeeshan on 17-Jan-18.
 */

class AdapterEventGuest(private val context: Context, internal var mList: ArrayList<EventDetailModel.EventGuest>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding: AdapterEventGuestBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_event_guest, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        if (!TextUtils.isEmpty(mList!![position].profilePic))
            Glide.with(context).load(mList!![position].profilePic).into(holder.binding!!.ivEventGuest)
    }

    override fun getItemCount(): Int {
        return if (mList == null) 0 else mList!!.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterEventGuestBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }
}
