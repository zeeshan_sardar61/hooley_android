package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.AdapterHostBinding
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

/**
 * Created by Nauman on 12/28/2017.
 */

class AdapterHost(private val context: Context, private val mList: ArrayList<FriendsTable>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val view: View? = null
    internal lateinit var binding: AdapterHostBinding


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_host, parent, false)
            //            view = LayoutInflater.from(context).inflate(R.layout.adapter_host, parent, false);
        } catch (e: InflateException) {
            /* map is already there, just return view as it is */
        }

        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            if (!TextUtils.isEmpty(mList!![position].profilePic))
                Glide.with(context).load(mList[position].profilePic).into(holder.binding!!.civFriendAvatar)
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterHostBinding? = null

        init {
            binding = DataBindingUtil.bind(v)

        }
    }
}
