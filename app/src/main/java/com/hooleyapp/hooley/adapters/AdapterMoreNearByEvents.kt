package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterMoreNearByEventsBinding
import com.hooleyapp.hooley.fragments.event.LiveEventFragment
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.interfaces.INearByMoreClickListener
import com.hooleyapp.hooley.model.NearByEventListFeedModel
import java.util.*

/**
 * Created by Zeeshan on 19-Feb-18.
 */

class AdapterMoreNearByEvents(private val context: Context, internal var mList: ArrayList<NearByEventListFeedModel.EventDetail>?, private val eventType: Int, private val listPosition: Int) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mListener: INearByMoreClickListener? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<AdapterMoreNearByEventsBinding>(LayoutInflater.from(parent.context), R.layout.adapter_more_near_by_events, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        if (position + 1 < mList!!.size) {
            Glide.with(context).load(mList!![position + 1].coverPhoto).into(holder.binding!!.ivEventCover)
            holder.binding!!.tvLike.text = Integer.toString(mList!![position + 1].likesCount)
            holder.binding!!.tvEventName.text = mList!![position + 1].eventName

            if (mList!![position + 1].isLiked) {
                holder.binding!!.ivLike.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_red_heart)
            } else {
                holder.binding!!.ivLike.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_purple_heart)
            }

            holder.binding!!.llContainer.setOnClickListener {
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment
                        .newInstance(mList!![position + 1].eventId!!), "LiveEventFragment")
                hideDrawer(HooleyMain.activity!!)
            }


            holder.binding!!.ivLike.setOnClickListener { mListener!!.onClickMoreLike(position + 1, eventType, listPosition) }

        }

    }

    override fun getItemCount(): Int {
        return if (mList == null) 0 else mList!!.size - 1
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterMoreNearByEventsBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }


    fun setListener(listener: INearByMoreClickListener) {
        this.mListener = listener
    }
}
