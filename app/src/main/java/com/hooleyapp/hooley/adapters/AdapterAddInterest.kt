package com.hooleyapp.hooley.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.AdapterInterestsBinding
import com.hooleyapp.hooley.interfaces.IInterestDeleteClick
import com.hooleyapp.hooley.model.PersonalProfileModel
import java.util.*

/**
 * Created by Zeeshan on 23-Jan-18.
 */

class AdapterAddInterest(private val mContext: Context, internal var mlist: ArrayList<PersonalProfileModel.myObject>) : BaseAdapter() {
    internal lateinit var binding: AdapterInterestsBinding
    private var mListener: IInterestDeleteClick? = null


    override fun getCount(): Int {
        return mlist.size
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_interests, parent, false)
        binding.tvCatName.text = mlist[position].name
        binding.ivClose.setOnClickListener { mListener!!.onClickItem(mlist[position].name!!, position) }

        return binding.root
    }


    fun setmListener(listener: IInterestDeleteClick) {
        this.mListener = listener
    }
}
