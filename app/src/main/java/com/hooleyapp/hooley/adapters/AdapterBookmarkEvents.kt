package com.hooleyapp.hooley.adapters

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterBookmarkEventsBinding
import com.hooleyapp.hooley.fragments.event.GuestUserLiveEventFragment
import com.hooleyapp.hooley.fragments.event.LiveEventFragment
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.interfaces.IBookmarkEventsClickListener
import com.hooleyapp.hooley.model.BookmarkEventModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import java.util.*

/**
 * Created by Zeeshan on 21-Mar-18.
 */

class AdapterBookmarkEvents(private val context: Context, internal var mList: ArrayList<BookmarkEventModel.EventDetail>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mListener: IBookmarkEventsClickListener? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding: AdapterBookmarkEventsBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_bookmark_events, parent, false)
        return ItemViewHolder(binding.root)
    }


    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        Glide.with(context).load(mList[position].coverPhoto).into(holder.binding!!.ivEventCover)
        holder.binding!!.tvEventDate.text = DateUtils.displayDateOnly(DateUtils.getLocalDate(mList[position].startTime!!))
        holder.binding!!.tvLike.text = Integer.toString(mList[position].likesCount)
        holder.binding!!.tvFollow.text = Integer.toString(mList[position].followersCount)
        holder.binding!!.tvShare.text = Integer.toString(mList[position].sharesCount)
        holder.binding!!.tvEventName.text = mList[position].eventName
        holder.binding!!.tvEventAddress.text = mList[position].address
        if (mList[position].isLiked) {
            holder.binding!!.ivLike.background = ContextCompat.getDrawable(context, R.drawable.ic_red_heart)
        } else {
            holder.binding!!.ivLike.background = ContextCompat.getDrawable(context, R.drawable.ic_purple_heart)
        }

        if (mList[position].isFollowed) {
            holder.binding!!.ivFollow.background = ContextCompat.getDrawable(context, R.drawable.ic_filled_star)
        } else {
            holder.binding!!.ivFollow.background = ContextCompat.getDrawable(context, R.drawable.ic_follow_it)
        }
        if (mList[position].isLive)
            holder.binding!!.rlLive.visibility = View.VISIBLE
        else
            holder.binding!!.rlLive.visibility = View.GONE

        holder.binding!!.rlEventMainCover.setOnClickListener {
            if (HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)) {
                (ActivityBase.activity as ActivityBase).callFragment(R.id.container, GuestUserLiveEventFragment
                        .newInstance(mList[position].eventId!!), "GuestUserLiveEventFragment")
                hideDrawer(ActivityBase.activity)
            } else {
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment
                        .newInstance(mList[position].eventId!!), "LiveEventFragment")
                hideDrawer(HooleyMain.activity!!)
            }
        }
        holder.binding!!.tvEventAddress.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + mList[position].eventLat + "," + mList[position].eventLong))
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            ActivityBase.activity.startActivity(intent)
        }

        holder.binding!!.llLike.setOnClickListener { mListener!!.onClickLike(mList[position], position) }
        holder.binding!!.llShare.setOnClickListener { mListener!!.onClickShare(mList[position], position) }
        holder.binding!!.llFollow.setOnClickListener { mListener!!.onClickFollow(mList[position], position) }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterBookmarkEventsBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

    fun setListener(listener: IBookmarkEventsClickListener) {
        this.mListener = listener
    }

}
