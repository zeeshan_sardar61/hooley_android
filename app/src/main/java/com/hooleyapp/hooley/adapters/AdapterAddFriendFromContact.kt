package com.hooleyapp.hooley.adapters

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterHooleyContactBinding
import com.hooleyapp.hooley.databinding.LayoutContactChildBinding
import com.hooleyapp.hooley.interfaces.IFriendContactClickListener
import com.hooleyapp.hooley.model.ContactFriendListModel
import com.hooleyapp.hooley.others.Constants
import java.util.*

class AdapterAddFriendFromContact(var mContext: Context, var mList: ArrayList<ContactFriendListModel.ContactModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var TYPE_HOOLEY_USER = 0
    private var TYPE_PHONE_CONTACT = 1

    lateinit var bindingHooleyContact: AdapterHooleyContactBinding
    lateinit var bindingPhoneContact: LayoutContactChildBinding
    lateinit var mListener: IFriendContactClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == TYPE_HOOLEY_USER) {
            try {
                bindingHooleyContact = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_hooley_contact, parent, false)
            } catch (e: InflateException) {
                e.printStackTrace()
            }
            HooleyContactItemViewHolder(bindingHooleyContact.root)
        } else {
            try {
                bindingPhoneContact = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.layout_contact_child, parent, false)
            } catch (e: InflateException) {
                e.printStackTrace()
            }
            PhoneContactItemViewHolder(bindingPhoneContact.root)
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == TYPE_HOOLEY_USER) {
            val holder = holder as HooleyContactItemViewHolder
            try {
                holder.binding!!.tvFriendName.text = mList[position].fullName
                holder.binding!!.tvAddress.text = "${mList[position].cityName}, ${mList[position].stateName}"
                if (!TextUtils.isEmpty(mList[position].profilePic))
                    Glide.with(mContext).load(mList[position].profilePic).into(holder.binding!!.civFriendAvatar)
                else
                    holder.binding!!.civFriendAvatar.setImageResource(R.drawable.ic_avatar_place_holder)
                if (mList[position].userId == HooleyApp.db.getString(Constants.USER_ID)) {
                    holder.binding!!.tvAddFriend.visibility = View.GONE
                    holder.binding!!.tvAccept.visibility = View.GONE
                    holder.binding!!.tvFriend.visibility = View.GONE
                    holder.binding!!.tvRequested.visibility = View.GONE
                } else {
                    when (mList[position].status) {
                        Constants.TYPE_NONE -> {
                            holder.binding!!.tvAddFriend.visibility = View.VISIBLE
                            holder.binding!!.tvFriend.visibility = View.GONE
                            holder.binding!!.tvRequested.visibility = View.GONE
                            holder.binding!!.tvAccept.visibility = View.GONE
                        }
                        Constants.TYPE_PENDING_SENT -> {
                            holder.binding!!.tvAccept.visibility = View.GONE
                            holder.binding!!.tvAddFriend.visibility = View.GONE
                            holder.binding!!.tvFriend.visibility = View.GONE
                            holder.binding!!.tvRequested.visibility = View.VISIBLE
                        }
                        Constants.TYPE_FRIEND -> {
                            holder.binding!!.tvAddFriend.visibility = View.GONE
                            holder.binding!!.tvFriend.visibility = View.VISIBLE
                            holder.binding!!.tvRequested.visibility = View.GONE
                            holder.binding!!.tvAccept.visibility = View.GONE
                        }
                        Constants.TYPE_PENDING_RECEIVED -> {
                            holder.binding!!.tvAddFriend.visibility = View.GONE
                            holder.binding!!.tvAccept.visibility = View.VISIBLE
                            holder.binding!!.tvFriend.visibility = View.GONE
                            holder.binding!!.tvRequested.visibility = View.GONE
                        }
                    }
                }
                holder.binding!!.tvAddFriend.setOnClickListener {
                    mListener.onClickAddFriend(position)
                }
                holder.binding!!.tvAccept.setOnClickListener {
                    mListener.onClickAcceptFriend(position)
                }
                holder.binding!!.rlItem.setOnClickListener {
                    mListener.onClickItem(position)
                }
            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            }
        } else {
            val holder = holder as PhoneContactItemViewHolder
            try {
                if (mList[position].status == Constants.SEPARATOR) {
                    holder.bindingPhoneContact!!.llHeader.visibility = View.VISIBLE
                    holder.bindingPhoneContact!!.rlItem.visibility = View.GONE
                } else {
                    holder.bindingPhoneContact!!.llHeader.visibility = View.GONE
                    holder.bindingPhoneContact!!.rlItem.visibility = View.VISIBLE
                    holder.bindingPhoneContact!!.tvContactName.text = mList[position].fullName
                    holder.bindingPhoneContact!!.tvInviteFriends.visibility = View.VISIBLE
                    holder.bindingPhoneContact!!.tvInviteFriends.setOnClickListener {
                        val uri = Uri.parse("smsto:" + mList[position].contactNumber)
                        val intent = Intent(Intent.ACTION_SENDTO, uri)
                        intent.putExtra("sms_body", "Please install Hooley app using following link.\n" + "https://play.google.com/store/apps/details?id=com.hooleyapp.hooley")
                        if (intent.resolveActivity(HooleyMain.activity!!.packageManager) != null) {
                            HooleyMain.activity!!.startActivity(intent)
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            mList[position].status == Constants.NO_HOOLEY_USER -> TYPE_PHONE_CONTACT
            mList[position].status == Constants.SEPARATOR -> TYPE_PHONE_CONTACT
            else -> TYPE_HOOLEY_USER
        }
    }

    inner class HooleyContactItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var binding: AdapterHooleyContactBinding? = null

        init {
            binding = DataBindingUtil.bind(v)
        }
    }

    inner class PhoneContactItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var bindingPhoneContact: LayoutContactChildBinding? = null

        init {
            bindingPhoneContact = DataBindingUtil.bind(v)
        }
    }

}