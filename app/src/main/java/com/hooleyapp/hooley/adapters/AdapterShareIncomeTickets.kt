package com.hooleyapp.hooley.adapters

import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.HooleyApp.Companion.context
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.databinding.AdapterShareIcomeTicketsBinding
import com.hooleyapp.hooley.model.ticketStats.IncomeShareUsersItem

class AdapterShareIncomeTickets(var mList: List<IncomeShareUsersItem>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var bindinge: AdapterShareIcomeTicketsBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            bindinge = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_share_icome_tickets, parent, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }

        return ItemViewHolder(bindinge.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            holder.bindinge.tvUserName.text = mList[position].fullName
            if (!TextUtils.isEmpty(mList[position].profilePic))
                Glide.with(context).load(mList[position].profilePic).into(holder.bindinge.ivUserProfile)
            holder.bindinge.rlMainBody.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, (R.color.base_color)))
            holder.bindinge.tvShareIncome.text = mList[position].percentage.toString() + "%"

        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var bindinge: AdapterShareIcomeTicketsBinding = DataBindingUtil.bind(v)!!
    }

}