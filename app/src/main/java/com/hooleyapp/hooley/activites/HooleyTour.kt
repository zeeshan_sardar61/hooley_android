package com.hooleyapp.hooley.activites

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.View
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.adapters.VPDashBoardAdapter
import com.hooleyapp.hooley.databinding.ActivityAppTourBinding
import com.hooleyapp.hooley.model.UserGuideModel
import com.hooleyapp.hooley.others.Util
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Nauman on 11/28/2017.
 */

class HooleyTour : ActivityBase(), ViewPager.OnPageChangeListener, View.OnClickListener {

    internal var binding: ActivityAppTourBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_app_tour)
        //        setupViewPager();
        //        userSearchbinding.tabTour.setupWithViewPager(userSearchbinding.vpTour, true);
        setListener()
        getUserGuide()
    }

    private fun getUserGuide() {
        //        showDialog(getApplicationContext());
        val call = HooleyApp.apiService.userGuideModel
        call.enqueue(object : Callback<UserGuideModel> {
            override fun onResponse(call: Call<UserGuideModel>, response: Response<UserGuideModel>) {
                //               HooleyMain.activity!!.removeDialog();
                if (response.body()!!.success) {
                    val gson = Gson()
                    val respone = gson.toJson(response.body())
                    Log.d("RESPONS", "registerUser:$respone")
                    setupViewPager(response)
                    binding!!.tabTour.setupWithViewPager(binding!!.vpTour, true)
                    //                    Util.showToastMessage(getAppActivity(), "Register User Successfully");
                } else {
                    //                    Util.showToastMessage(getActivity(), "Unexpected response from server");
                }
            }

            override fun onFailure(call: Call<UserGuideModel>, t: Throwable) {
                Log.d("ERROR", "ERROR:" + t.message)
                Util.showToastMessage(appActivity, "Server not responding")
                //               HooleyMain.activity!!.removeDialog();
            }
        })
    }


    private fun setListener() {
        binding!!.vpTour.addOnPageChangeListener(this)
        binding!!.ivBack.setOnClickListener(this)
        binding!!.ivNext.setOnClickListener(this)
        binding!!.tvSkip.setOnClickListener(this)
        binding!!.tvContinue.setOnClickListener(this)
    }


    private fun setupViewPager(response: Response<UserGuideModel>) {
        val adapter = VPDashBoardAdapter(supportFragmentManager)
        //        for (int i = 0; i < response.body().userGuideArrayList.size(); i++) {
        //            adapter.addFragment(TourPage6Fragment.newInstance(response.body().userGuideArrayList.get(i).guideText), "");
        //        }
        //        adapter.addFragment(new TourPage6Fragment(), "");
        //        adapter.addFragment(new TourPage6Fragment(), "");
        //        adapter.addFragment(new TourPage6Fragment(), "");
        binding!!.vpTour.adapter = adapter
    }


    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        if (position == binding!!.vpTour.adapter!!.count - 1) {
            binding!!.tvSkip.visibility = View.GONE
            binding!!.ivNext.visibility = View.GONE
            binding!!.tvContinue.visibility = View.VISIBLE
        } else {
            binding!!.tvSkip.visibility = View.VISIBLE
            binding!!.ivNext.visibility = View.VISIBLE
            binding!!.tvContinue.visibility = View.GONE
        }
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tvSkip -> binding!!.vpTour.setCurrentItem(binding!!.vpTour.currentItem + 1, true)
            R.id.tvContinue -> {
                val intent = Intent(this, HooleyAuth::class.java)
                startActivity(intent)
                finish()
            }
            R.id.ivNext -> binding!!.vpTour.setCurrentItem(binding!!.vpTour.currentItem + 1, true)
            R.id.ivBack -> {
                if (binding!!.vpTour.currentItem == 0)
                    return
                binding!!.vpTour.setCurrentItem(binding!!.vpTour.currentItem - 1, true)
            }
        }
    }
}
