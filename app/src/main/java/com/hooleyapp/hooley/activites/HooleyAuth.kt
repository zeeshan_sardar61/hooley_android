package com.hooleyapp.hooley.activites

import android.app.UiModeManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.ActivityHooleyAuthBinding
import com.hooleyapp.hooley.fragments.auth.LoginFragment
import com.hooleyapp.hooley.fragments.auth.ProfileSetupFragment
import com.hooleyapp.hooley.fragments.auth.SignUpFragment
import com.hooleyapp.hooley.helper.Permissions

/**
 * Created by Nauman on 11/28/2017.
 */

class HooleyAuth : ActivityBase() {

    internal var binding: ActivityHooleyAuthBinding? = null

    companion object {
        lateinit var activity: AppCompatActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val umm = getSystemService(Context.UI_MODE_SERVICE) as UiModeManager
        umm.nightMode = UiModeManager.MODE_NIGHT_YES
        binding = DataBindingUtil.setContentView(this, R.layout.activity_hooley_auth)
        ActivityBase.setStatusBarGradiant(0)
        activity = appActivity
        if (intent.extras != null) {
            if (intent.extras!!.getString("Type") != null) {
                if (intent.extras!!.getString("Type")!!.equals("Register", ignoreCase = true))
                    callFragment(R.id.authContainer, SignUpFragment.newInstance(), null)
                else
                    callFragment(R.id.authContainer, LoginFragment.newInstance(), null)
            } else {
                callFragment(R.id.authContainer, LoginFragment.newInstance(), null)
            }
        } else {
            callFragment(R.id.authContainer, LoginFragment.newInstance(), null)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Permissions.checkPermission(this)
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            Permissions.PERMISSIONS_REQUEST_WRITE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (!Permissions.checkPermission(this)) {
                        Permissions.showSettings(this)
                    }
                } else {
                    if (!Permissions.checkPermission(this)) {
                        Permissions.showSettings(this)
                    }
                }
                return
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val fragment = supportFragmentManager.findFragmentById(R.id.authContainer)
        when (fragment) {
            is LoginFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is SignUpFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is ProfileSetupFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }
}
