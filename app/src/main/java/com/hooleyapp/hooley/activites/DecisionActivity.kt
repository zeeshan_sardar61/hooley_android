package com.hooleyapp.hooley.activites

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatDelegate
import android.telephony.TelephonyManager
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.app.view.ui.activites.HooleyGuestUser
import com.hooleyapp.hooley.contacts.ContactApi
import com.hooleyapp.hooley.databinding.ActivityDecisionBinding
import com.hooleyapp.hooley.db.DbManager
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.Util

/**
 * Created by Nauman on 12/7/2017.
 */

class DecisionActivity : ActivityBase() {

    internal lateinit var intent: Intent
    private var binding: ActivityDecisionBinding? = null

    var handler = Handler()
    private var themeType: Int = 0


    private lateinit var tag: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_decision)
        setStatusBarColor(R.color.clr_start_color, 0)
        Util.logv("DecisionActivity", "Starting")
        getCountyCode()
        ContactApi.init(this, applicationContext)
        DbManager.getInstance().exportDB()
        DbManager.getInstance().exportRoom()

        if (HooleyApp.db.getInt(Constants.TYPE_THEME) == 1) {
            themeType = AppCompatDelegate.MODE_NIGHT_NO
            AppCompatDelegate.setDefaultNightMode(themeType)
        } else {
            themeType = AppCompatDelegate.MODE_NIGHT_YES
            HooleyApp.db.putInt(Constants.TYPE_THEME, themeType)
            AppCompatDelegate.setDefaultNightMode(themeType)
        }

        if (getIntent().extras != null)
            if (getIntent().extras.get("tag") != null) {
                tag = getIntent().extras.get("tag") as String
                HooleyApp.db.putString(Constants.TYPE_NOTIFICATION, tag)
                when (tag) {
                    Constants.TYPE_MESSAGES_ALERT -> {
                        var friendName: String = getIntent().extras.get("body") as String
                        val threadId: String = getIntent().extras.get("threadId") as String
                        val friendId: String = getIntent().extras.get("friendId") as String
                        val name = friendName.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                        friendName = name[0]
                        HooleyApp.db.putString(Constants.THREAD_ID, threadId)
                        HooleyApp.db.putString(Constants.FRIEND_NAME, friendName)
                        HooleyApp.db.putString(Constants.FRIEND_ID, friendId)
                        HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, true)
                    }
                    Constants.TYPE_EVENT_ALERTS -> {
                        HooleyApp.db.putString(Constants.EVENT_ID, getIntent().extras.get("eventId") as String)
                        HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, true)
                    }
                    Constants.TYPE_FRIENDS_ALERT -> {
                        HooleyApp.db.putString(Constants.FRIEND_UID, getIntent().extras.get("userId") as String)
                        HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, true)
                    }
                    Constants.TYPE_MEDIA_ALERT -> {
                        HooleyApp.db.putString(Constants.EVENT_ID, getIntent().extras.get("eventId") as String)
                        HooleyApp.db.putString(Constants.MEDIA_ID, getIntent().extras.get("mediaId") as String)
                        HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, true)
                    }
                    Constants.TYPE_TICKET_ALERT -> {
                        HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, true)
                    }
                    Constants.TYPE_TICKETSALE_ALERT -> {
                        HooleyApp.db.putString(Constants.EVENT_ID, getIntent().extras.get("eventId") as String)
                        HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, true)
                    }
                    Constants.TYPE_UPDATE_ANNOUNCEMENTS_ALERT -> {
                        HooleyApp.db.putString(Constants.EVENT_ID, getIntent().extras.get("eventId") as String)
                        HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, true)
                    }
                    Constants.TYPE_ANNOUNCEMENTS_ALERT -> {
                        HooleyApp.db.putString(Constants.EVENT_ID, getIntent().extras.get("eventId") as String)
                        HooleyApp.db.putString(Constants.EVENT_NAME, getIntent().extras.get("eventName") as String)
                        HooleyApp.db.putString(Constants.EVENT_ADDRESS, getIntent().extras.get("address") as String)
                        if (getIntent().extras.get("imageUrl") != null)
                            HooleyApp.db.putString(Constants.EVENT_ANNOUNCEMENT_IMGURL, getIntent().extras.get("imageUrl") as String)
                        HooleyApp.db.putString(Constants.EVENT_ANNOUNCEMENT_MSG, getIntent().extras.get("detailMessage") as String)
                        HooleyApp.db.putString(Constants.EVENT_START_TIME, getIntent().extras.get("startTime") as String)
                        HooleyApp.db.putString(Constants.EVENT_END_TIME, getIntent().extras.get("endTime") as String)
                        HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, true)
                    }
                }

            }

        if (HooleyApp.db.getBoolean(Constants.IS_LOGIN)) {
            intent = Intent(this@DecisionActivity, HooleyMain::class.java)
            if (getIntent().extras != null)
                if (getIntent().extras.get("tag") != null) {
                    intent.putExtra("tagger", tag)
                }
        } else if (HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)) {
            intent = Intent(this@DecisionActivity, HooleyGuestUser::class.java)
        } else {
            intent = Intent(this@DecisionActivity, Splash::class.java)
        }


        handler.postDelayed({
            startActivity(intent)
            finish()
        }, 1500)
    }

    private fun getCountyCode() {
        binding!!.ccp.setOnCountryChangeListener { country ->
            HooleyApp.db.putString(Constants.CURRENT_CC, country.phoneCode)
            Logger.ex("selected code " + country.phoneCode)
        }
        val tm = this.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        val countryCodeValue = tm.networkCountryIso
        Logger.ex("getCountyCode $countryCodeValue")
        binding!!.ccp.setCountryForNameCode(countryCodeValue)
    }
}
