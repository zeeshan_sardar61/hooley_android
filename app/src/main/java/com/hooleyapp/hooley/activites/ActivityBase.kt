/**
 *
 */
package com.hooleyapp.hooley.activites


import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.net.PlacesClient
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.others.Util
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 * Base for all the activities. Provides saving state functionality.
 *
 */
open class ActivityBase : AppCompatActivity() {

    /**
     * @return the lifecycleState
     */
    /**
     * @param lifecycleState the lifecycleState to set
     */
    var lifecycleState: State? = null

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */

    val appActivity: AppCompatActivity
        get() = this


    /**
     * Restores the saved application state, in case of be needed, and does default settings for the action bar.
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Util.LOG_ENABLED) {
            Log.v(TAG, "protected void onCreate(Bundle savedInstanceState)")
        }
        Util.adjustFontScale(resources.configuration, this)
        lifecycleState = State.CREATED
        Util.restoreSavedApplicationState(this, savedInstanceState)
        activity = this
        Places.initialize(applicationContext, getString(R.string.google_maps_key))
        var placesClient: PlacesClient = Places.createClient(this)

    }

    protected fun callFragmentWithAddBackStack(containerId: Int, fragment: Fragment, tag: String) {
        supportFragmentManager
                .beginTransaction()
                .replace(containerId, fragment, tag)
                .setCustomAnimations(R.anim.slide_in_enter, R.anim.slide_in_exit,
                        R.anim.slide_pop_enter, R.anim.slide_pop_exit)
                .addToBackStack(tag)
                .commit()
    }

    fun callFragmentWithReplace(containerId: Int, fragment: Fragment, tag: String?) {
        val transaction = supportFragmentManager
                .beginTransaction()
                .replace(containerId, fragment, tag)
                .setCustomAnimations(R.anim.slide_in_enter, R.anim.slide_in_exit,
                        R.anim.slide_pop_enter, R.anim.slide_pop_exit)
        if (tag != null)
            transaction.addToBackStack(tag)
                    .commit()
        else
            transaction
                    .commit()
//        supportFragmentManager.popBackStackImmediate()
    }

    fun callFragmentWithReplaceWithAllowingStateLoss(containerId: Int, fragment: Fragment, tag: String?) {
        val transaction = supportFragmentManager
                .beginTransaction()
                .replace(containerId, fragment, tag)
                .setCustomAnimations(R.anim.slide_in_enter, R.anim.slide_in_exit,
                        R.anim.slide_pop_enter, R.anim.slide_pop_exit)
        if (tag != null)
            transaction.addToBackStack(tag)
                    .commit()
        else
            transaction
                    .commitAllowingStateLoss()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)

    }


    fun GetTimeHours(time: String): String {

        val newTimeString: String? = null
        val time1 = newTimeString!!.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val time2 = time1[1].split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        return time2[0]

    }

    fun GetDate(date: String): String {
        val df = SimpleDateFormat("yy-d-M'T'hh:mm:ss")
        val startDate: Date
        var newDateString: String? = null
        try {
            startDate = df.parse(date)
            newDateString = df.format(startDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val date1 = newDateString!!.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val date2 = date1[2].split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        return date1[1] + "/" + date2[0] + "/" + date1[0]
    }

    fun GetFomatedDate(date: String): Date? {
        val df = SimpleDateFormat("yy-d-M'T'hh:mm:ss")
        var startDate: Date? = null
        try {
            startDate = df.parse(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return startDate
    }

    fun GetMonthName(date: String): String {
        val df = SimpleDateFormat("yy-MM-dd")
        val startDate: Date
        var monthName: String? = null
        var newDateString: String? = null
        try {
            startDate = df.parse(date)
            val cal = Calendar.getInstance()
            cal.time = startDate
            monthName = SimpleDateFormat("MMM").format(cal.time)
            newDateString = df.format(startDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val date1 = newDateString!!.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        return monthName + " " + date1[0]
    }


    /**
     * if you will pass tag as title of fragment it will add that
     * fragment to stack otherwise will not add on stack.
     *
     * @param containerId
     * @param fragment
     * @param tag
     */
    fun callFragment(containerId: Int, fragment: Fragment, tag: String?) {
        window.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
        )
        val transaction = supportFragmentManager
                .beginTransaction()
                .replace(containerId, fragment, tag)
                .setCustomAnimations(R.anim.slide_in_enter, R.anim.slide_in_exit,
                        R.anim.slide_pop_enter, R.anim.slide_pop_exit)
        if (tag != null)
            transaction.addToBackStack(tag)
                    .commitAllowingStateLoss()
        else
            transaction
                    .commit()
    }

    fun callFragmentLossState(containerId: Int, fragment: Fragment, tag: String?) {
        val transaction = supportFragmentManager
                .beginTransaction()
                .replace(containerId, fragment, tag)
                .setCustomAnimations(R.anim.slide_in_enter, R.anim.slide_in_exit,
                        R.anim.slide_pop_enter, R.anim.slide_pop_exit)
        if (tag != null)
            transaction.addToBackStack(tag)
                    .commitAllowingStateLoss()
        else
            transaction
                    .commitAllowingStateLoss()
    }

    /**
     * if you will pass tag as title of fragment it will add that
     * fragment to stack otherwise will not add on stack.
     *
     * @param containerId
     * @param fragment
     * @param tag
     */
    fun addFragment(containerId: Int, fragment: Fragment, tag: String?) {
        window.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
        )
        val transaction = supportFragmentManager
                .beginTransaction()
                .add(containerId, fragment, tag)
                .setCustomAnimations(R.anim.slide_in_enter, R.anim.slide_in_exit,
                        R.anim.slide_pop_enter, R.anim.slide_pop_exit)
        if (tag != null)
            transaction.addToBackStack(tag)
                    .commit()
        else
            transaction
                    .commit()
    }

    /**
     * if you will pass tag as title of fragment it will add that
     * fragment to stack otherwise will not add on stack.
     *
     * @param fragment
     */
    fun removeFragment(fragment: Fragment) {
        //        getWindow().setSoftInputMode(
        //                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        //        );
        val transaction = supportFragmentManager
                .beginTransaction()
                .remove(fragment)
                .setCustomAnimations(R.anim.slide_in_enter, R.anim.slide_in_exit,
                        R.anim.slide_pop_enter, R.anim.slide_pop_exit)
        transaction
                .commit()
    }

    override fun onStart() {
        super.onStart()
        lifecycleState = State.STARTED
    }

    override fun onResume() {
        super.onResume()
        lifecycleState = State.RESUMED
    }

    override fun onPause() {
        super.onPause()
        lifecycleState = State.PAUSED
    }


    override fun onStop() {
        super.onStop()
        lifecycleState = State.STOPPED
    }

    override fun onDestroy() {
        super.onDestroy()
        lifecycleState = State.DESTROYED
    }

    /**
     * The possibles states of an activity lifecycle.
     */
    enum class State {
        CREATED, STARTED, RESUMED, PAUSED, STOPPED, DESTROYED
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    fun showKeyboard() {
        (getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager)
                .toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
    }

    fun hideKeyboard() {
        (getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager)
                .toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY)
    }

    @SuppressLint("NewApi")
    fun blurRenderScript(smallBitmap: Bitmap, radius: Int): Bitmap {
        var smallBitmap = smallBitmap

        try {
            smallBitmap = RGB565toARGB888(smallBitmap)
        } catch (e: Exception) {
            e.printStackTrace()
        }


        val bitmap = Bitmap.createBitmap(
                smallBitmap.width, smallBitmap.height,
                Bitmap.Config.ARGB_8888)

        val renderScript = RenderScript.create(applicationContext)

        val blurInput = Allocation.createFromBitmap(renderScript, smallBitmap)
        val blurOutput = Allocation.createFromBitmap(renderScript, bitmap)

        val blur = ScriptIntrinsicBlur.create(renderScript,
                Element.U8_4(renderScript))
        blur.setInput(blurInput)
        blur.setRadius(radius.toFloat()) // radius must be 0 < r <= 25
        blur.forEach(blurOutput)

        blurOutput.copyTo(bitmap)
        renderScript.destroy()

        return bitmap

    }

    private fun RGB565toARGB888(img: Bitmap): Bitmap {
        val numPixels = img.width * img.height
        val pixels = IntArray(numPixels)

        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.width, 0, 0, img.width, img.height)

        //Create a Bitmap of the appropriate format.
        val result = Bitmap.createBitmap(img.width, img.height, Bitmap.Config.ARGB_8888)

        //Set RGB pixels.
        result.setPixels(pixels, 0, result.width, 0, 0, result.width, result.height)
        return result
    }


    fun computeCentroid(points: ArrayList<LatLng>): LatLng {
        var latitude = 0.0
        var longitude = 0.0
        val n = points.size

        for (point in points) {
            latitude += point.latitude
            longitude += point.longitude
        }

        return LatLng(latitude / n, longitude / n)
    }

    fun getCompleteAddressString(LATITUDE: Double, LONGITUDE: Double): String {
        var strAdd = ""
        val geocoder = Geocoder(activity, Locale.getDefault())
        try {
            val addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1)
            if (addresses != null) {
                val returnedAddress = addresses[0]
                val strReturnedAddress = StringBuilder()

                for (i in 0..returnedAddress.maxAddressLineIndex) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i))
                }
                strAdd = strReturnedAddress.toString()
                Logger.i("My Current location address$strReturnedAddress")
            } else {
                Logger.i("My Current location address" + "No Address returned!")
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Logger.i("My Current location address" + "Cannot get Address!")
        }

        return strAdd
    }


    fun noTrailingwhiteLines(text: CharSequence): CharSequence {
        var text = text

        while (text[text.length - 1] == '\n') {
            text = text.subSequence(0, text.length - 1)
        }

        while (text[text.length - 2] == '\n') {
            text = text.subSequence(0, text.length - 2)
        }
        return text
    }

    companion object {

        var toolbar: Toolbar? = null
        lateinit var activity: AppCompatActivity
        val TAG = ActivityBase::class.java.name

        /** */


        fun withSuffix(count: Long): String {
            if (count < 1000) return "" + count
            val exp = (Math.log(count.toDouble()) / Math.log(1000.0)).toInt()
            return String.format("%.1f %c",
                    count / Math.pow(1000.0, exp.toDouble()),
                    "kMGTPE"[exp - 1])
        }


        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        fun setStatusBarGradiant(flag: Int) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val window = activity.window
                val background = ContextCompat.getDrawable(activity, R.drawable.bg_tour_header)
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(activity, android.R.color.transparent)
                //window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
                activity.window.decorView.systemUiVisibility = flag
                window.setBackgroundDrawable(background)
            }
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        fun setStatusBarColor(color: Int, flag: Int) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val window = activity.window
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(activity, color)
                activity.window.decorView.systemUiVisibility = flag
                //            window.windowLightStatusBar(ContextCompat.getColor(activity, color));
            }
        }

    }

}
