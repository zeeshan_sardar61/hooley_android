package com.hooleyapp.hooley.activites

import android.app.UiModeManager
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.adapters.VPDashBoardAdapter
import com.hooleyapp.hooley.databinding.ActivitySplashBinding
import com.hooleyapp.hooley.fragments.nearby.DashBoardFragment
import com.hooleyapp.hooley.fragments.tour.*
import com.hooleyapp.hooley.helper.Permissions


/**
 * Created by Nauman on 11/28/2017.
 */

class Splash : ActivityBase(), View.OnClickListener, ViewPager.OnPageChangeListener {

    internal var binding: ActivitySplashBinding? = null
    internal var intent: Intent? = null

    companion object {
        lateinit var activity: AppCompatActivity

    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val umm = ActivityBase.activity.getSystemService(Context.UI_MODE_SERVICE) as UiModeManager
        umm.nightMode = UiModeManager.MODE_NIGHT_NO
//        Util.getKeyHash(this)
//        hashFromSHA1("82:DD:D5:E7:DF:EC:32:2E:64:48:00:89:46:72:98:56:B9:A4:84:C6")
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)
        ActivityBase.setStatusBarColor(R.color.clr_start_color, 0)
        activity = appActivity
        setListener()
        setupViewPager()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Permissions.checkPermission(this)
        }
    }

//    fun hashFromSHA1(sha1: String) {
//        val arr = sha1.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//        val byteArr = ByteArray(arr.size)
//
//        for (i in arr.indices) {
//            byteArr[i] = Integer.decode("0x" + arr[i]).toByte()
//        }
//
//        Log.e("hash : ", Base64.encodeToString(byteArr, Base64.NO_WRAP))
//    }


    private fun setupViewPager() {
        val adapter = VPDashBoardAdapter(supportFragmentManager)
        adapter.addFragment(TourPage1Fragment(), "TourPage1Fragment")
        adapter.addFragment(TourPage2Fragment(), "TourPage2Fragment")
        adapter.addFragment(TourPage3Fragment(), "TourPage3Fragment")
        adapter.addFragment(TourPage4Fragment(), "TourPage4Fragment")
        adapter.addFragment(TourPage5Fragment(), "TourPage5Fragment")
        adapter.addFragment(TourPage6Fragment(), "TourPage6Fragment")
        binding!!.vpTour.adapter = adapter
    }

    private fun setListener() {
        binding!!.vpTour.addOnPageChangeListener(this)
        binding!!.llSkip.setOnClickListener(this)
        //        userSearchbinding.btnTakeTour.setOnClickListener(this);
        //        userSearchbinding.ivGetStarted.setOnClickListener(this);
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnTakeTour -> {
                intent = Intent(this, HooleyTour::class.java)
                startActivity(intent)
            }
            R.id.ivGetStarted -> {
                intent = Intent(this, HooleyMain::class.java)
                startActivity(intent)
            }
            R.id.llSkip -> binding!!.vpTour.currentItem = 5
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        when (position) {
            0 -> {
                ActivityBase.setStatusBarColor(R.color.clr_start_color, 0)
                binding!!.llSkip.visibility = View.GONE
                binding!!.tvSwipeBegin.visibility = View.VISIBLE
                binding!!.ivPage1.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_selected_white_dot)
                binding!!.ivPage2.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_white_dot)
                binding!!.ivPage3.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_white_dot)
                binding!!.ivPage4.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_white_dot)
                binding!!.ivPage5.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_white_dot)
                binding!!.ivPage6.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_white_dot)
            }
            1 -> {
                binding!!.tvSwipeBegin.visibility = View.GONE
                binding!!.llSkip.visibility = View.VISIBLE
                ActivityBase.setStatusBarGradiant(0)
                binding!!.ivPage1.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage2.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_selected_orng_dot)
                binding!!.ivPage3.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage4.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage5.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage6.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
            }
            2 -> {
                binding!!.tvSwipeBegin.visibility = View.GONE
                binding!!.llSkip.visibility = View.VISIBLE
                binding!!.ivPage1.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage2.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage3.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_selected_orng_dot)
                binding!!.ivPage4.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage5.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage6.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
            }
            3 -> {
                binding!!.tvSwipeBegin.visibility = View.GONE
                binding!!.ivPage1.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage2.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage3.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage4.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_selected_orng_dot)
                binding!!.ivPage5.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage6.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
            }
            4 -> {
                binding!!.llSkip.visibility = View.VISIBLE
                ActivityBase.setStatusBarGradiant(0)
                binding!!.ivPage1.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage2.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage3.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage4.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage5.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_selected_orng_dot)
                binding!!.ivPage6.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
            }
            5 -> {
                //                setStatusBarColor(R.color.app_white_color, View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                ActivityBase.setStatusBarGradiant(0)
                binding!!.llSkip.visibility = View.GONE
                binding!!.tvSwipeBegin.visibility = View.GONE
                binding!!.ivPage1.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage2.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage3.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage4.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage5.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_selected_orng_dot)
                binding!!.ivPage6.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_selected_orng_dot)
            }
        }

    }

    override fun onPageScrollStateChanged(state: Int) {

    }
}
