package com.hooleyapp.hooley.activites

import android.annotation.TargetApi
import android.app.Activity
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.UiModeManager
import android.app.UiModeManager.MODE_NIGHT_YES
import android.arch.lifecycle.Lifecycle
import android.content.*
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.Point
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.provider.Settings
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.hooleyapp.hooley.Bubbles.BubbleLayout
import com.hooleyapp.hooley.Bubbles.BubblesManager
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.TypeToken
import com.hooleyapp.hooley.adapters.AdapterMenuItems
import com.hooleyapp.hooley.app.view.ui.fragments.event.EventDetailAnnouncementFragment
import com.hooleyapp.hooley.app.view.ui.fragments.event.EventDetailFragment
import com.hooleyapp.hooley.app.view.ui.fragments.event.EventDetailGuestListFragment
import com.hooleyapp.hooley.app.view.ui.fragments.others.AboutHooleyFragment
import com.hooleyapp.hooley.app.view.ui.fragments.others.FaqsFragment
import com.hooleyapp.hooley.app.view.ui.fragments.others.FeedbackFragment
import com.hooleyapp.hooley.app.view.ui.fragments.others.PrivacyPolicyFragment
import com.hooleyapp.hooley.databinding.ActivityHooleyMainBinding
import com.hooleyapp.hooley.fragments.event.*
import com.hooleyapp.hooley.fragments.friends.FriendsProfileFragment
import com.hooleyapp.hooley.fragments.friends.SelectFriendFragment
import com.hooleyapp.hooley.fragments.invites.InviteFragment
import com.hooleyapp.hooley.fragments.invites.InviteHooleyFriendsFragment
import com.hooleyapp.hooley.fragments.invites.PastGuestListFriendsFragment
import com.hooleyapp.hooley.fragments.invites.ShowSelectedUserFragment
import com.hooleyapp.hooley.fragments.media.AllFeedsSinglePostCommentFragment
import com.hooleyapp.hooley.fragments.messaging.ChatWindowFragment
import com.hooleyapp.hooley.fragments.nearby.DashBoardFragment
import com.hooleyapp.hooley.fragments.nearby.GeofenceFragment
import com.hooleyapp.hooley.fragments.nearby.MyGeoFenceFragment
import com.hooleyapp.hooley.fragments.nearby.NearByFragment
import com.hooleyapp.hooley.fragments.notification.NotificationMainFragment
import com.hooleyapp.hooley.fragments.others.*
import com.hooleyapp.hooley.fragments.profile.EditBusinessProfileFragment
import com.hooleyapp.hooley.fragments.profile.EditPersonalProfileFragment
import com.hooleyapp.hooley.fragments.profile.ProfileMainFragment
import com.hooleyapp.hooley.fragments.search.SearchFilterFragment
import com.hooleyapp.hooley.fragments.ticket.BuyTicketFragment
import com.hooleyapp.hooley.fragments.ticket.EventTicketSetupFragment
import com.hooleyapp.hooley.fragments.ticket.MyTicketFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IMenuItem
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.BubbleListItem
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.*
import com.hooleyapp.hooley.requestmodel.UpdateLatLongRequestModel
import com.hooleyapp.hooley.services.AuthWebService
import com.hooleyapp.hooley.social.SocialClass
import de.hdodenhof.circleimageview.CircleImageView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class HooleyMain : ActivityBase(), IMenuItem, View.OnClickListener, LocationListener {
    private var ivAvatar_: ImageView? = null
    private lateinit var ivEventAvatar: CircleImageView
    var service = AuthWebService()
    internal var binding: ActivityHooleyMainBinding? = null
    internal var handler = Handler()
    private var adapter: AdapterMenuItems? = null
    private var locationManager: LocationManager? = null
    private var bubblesManager: BubblesManager? = null
    private var bubbleView: BubbleLayout? = null
    private var resideMenu: ResideLayout? = null
    private val gson = Gson()
    private var tag = ""
    private lateinit var optionMenu: Menu
    internal lateinit var umm: UiModeManager
    private var themeType: Int = 0
    var isBubbleShowing: Boolean = false


    private val silentPushReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.extras!!.getString("tag")) {
                Constants.TYPE_MESSAGES_ALERT -> HooleyApp.db.putBoolean(Constants.SHOW_CHAT_ALERT, true)
                Constants.TYPE_EVENT_GALLERY_ALERT -> HooleyApp.db.putBoolean(Constants.SHOW_GALLERY_ALERT, true)
                Constants.TYPE_WHOS_HER_ALERT -> HooleyApp.db.putBoolean(Constants.SHOW_WHO_HERE_ALERT, true)
                Constants.TYPE_MEDIA_ALERT -> {
                    HooleyApp.db.putInt(Constants.UNREAD_NOTIFICATION_COUNT, intent.extras!!.get("badge") as Int)
                    invalidateOptionsMenu()
                }
                else -> {
                    HooleyApp.db.putInt(Constants.UNREAD_NOTIFICATION_COUNT, intent.extras!!.get("badge") as Int)
                    invalidateOptionsMenu()
                }
            }
        }
    }
    private val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            try {
                invalidateOptionsMenu()
            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            }

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        if (event.pic == "DP")
            Glide.with(activity!!).load(event.message).into(ivAvatar_!!)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    private val navigationBarHeight: Int
        get() {
            val resources = resources
            val resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android")
            return if (resourceId > 0) {
                resources.getDimensionPixelSize(resourceId)
            } else 0
        }

    val bubbleList: ArrayList<BubbleListItem>?
        get() {
            val gson = Gson()
            var bubbleList: ArrayList<BubbleListItem>
            val type = object : TypeToken<java.util.ArrayList<BubbleListItem>>() {

            }.type

            try {
                bubbleList = gson.fromJson(HooleyApp.db.getString(Constants.GSON_BUBBLE_LIST), type)
            } catch (e: IllegalStateException) {
                return ArrayList()
            }
            return bubbleList
        }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_hooley_main)
        HooleyApp.db.putBoolean(Constants.IS_LOGIN, true)
        HooleyApp.db.putBoolean(Constants.IS_GUEST_LOGIN, false)
        activity = appActivity
        Logger.d("--> Logged in")

        themeType = if (HooleyApp.db.getInt(Constants.TYPE_THEME) == MODE_NIGHT_YES) {
            AppCompatDelegate.MODE_NIGHT_YES
        } else {
            AppCompatDelegate.MODE_NIGHT_NO
        }
        //Setting Night Mode
        umm = getSystemService(Context.UI_MODE_SERVICE) as UiModeManager
        umm.nightMode = themeType
        AppCompatDelegate.setDefaultNightMode(themeType)
        delegate.setLocalNightMode(themeType)

        //Check For Permissions
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Permissions.checkPermission(this)
        }
        // HandleSystem Overlay Permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:$packageName"))
            startActivityForResult(intent, 786)
        } else {
            if (bubblesManager == null)
                Handler().postDelayed({ initializeBubblesManager() }, 1000)
        }
        //Register broadCast receiver
        LocalBroadcastManager.getInstance(activity!!).registerReceiver(mMessageReceiver,
                IntentFilter("Notification"))
        LocalBroadcastManager.getInstance(activity!!).registerReceiver(silentPushReceiver,
                IntentFilter("ChatStreamMessage"))


        getHashKey()
        setToolBar()
        updateCurrentLatLong()
        if (intent.extras != null) {
            if (intent.extras.get("tagger") != null) {
                loadNotificationFragment(true)
            } else {
                handleIntent(intent, true)
            }
        } else {
            loadFragmentInActivity()
        }
        setMenuItems()
        addBackStackListener()
        setUpLocationServices()
        handleNavigationBar()
    }

    override fun onPause() {
        super.onPause()
        (activity as HooleyMain).removeBubbleView()
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setToolBar() {
        setStatusBarGradiant(0)
        binding!!.toolbar.title = ""
        setSupportActionBar(binding!!.toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_menu)
        binding!!.ivHome.setOnClickListener(this)

    }

    override fun onNewIntent(intent: Intent) {
        val data = intent.data
        if (data != null && data.isHierarchical) {
            val uri = intent.dataString
            Logger.ex("MyApp Deep link clicked " + uri!!)
            gotoEventScreen(uri)
            return
        }
        if (intent.action != null && intent.action!!.contains("ChatBubble")) {
            activity!!.hideKeyboard()
            (activity as ActivityBase).callFragment(R.id.container, LiveEventFragment.newInstance(HooleyApp.db.getString(Constants.CHAT_HEAD_EVENT_ID)), "LiveEventFragment")
            hideDrawer(activity!!)
        } else {
            if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                activity!!.hideKeyboard()
                handleIntent(intent, false)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {

    }

    override fun onLocationChanged(location: Location) {
        Logger.d("Latitude : " + location.latitude + ", Longitude:" + location.longitude)
        HooleyApp.db.putDouble(Constants.CURRENT_LAT, location.latitude)
        HooleyApp.db.putDouble(Constants.CURRENT_LANG, location.longitude)
    }

    override fun onProviderDisabled(provider: String) {
        Logger.d("Latitude => " + "disable")
    }

    override fun onProviderEnabled(provider: String) {
        Logger.d("Latitude =>" + "enable")
    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
        Logger.d("Latitude => " + "status")
    }

    fun showHomeButton(isVisible: Boolean) {
        binding!!.ivHome.visibility = if (isVisible) {
            View.VISIBLE
        } else {
            View.GONE
        }
        invalidateOptionsMenu()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                if (supportFragmentManager.findFragmentById(R.id.container) is DashBoardFragment) {
                    resideMenu!!.openMenu(ResideLayout.DIRECTION_LEFT)
                } else {
                    onBackPressed()
                }
                return true
            }
            R.id.ic_search -> {
                activity!!.hideKeyboard()
                if (supportFragmentManager.findFragmentById(R.id.container) is SearchFilterFragment)
                    return false
                (activity as ActivityBase).addFragment(R.id.container, SearchFilterFragment.newInstance(), "SearchFilterFragment")
                hideDrawer(activity!!)
                return true
            }
            R.id.ic_notification -> {
                activity!!.hideKeyboard()
                if (supportFragmentManager.findFragmentById(R.id.container) is NotificationMainFragment)
                    return false
                (activity as ActivityBase).addFragment(R.id.container, NotificationMainFragment.newInstance(), "NotificationMainFragment")
                hideDrawer(activity!!)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        val f = supportFragmentManager.findFragmentById(R.id.container)
        when (f) {
            is MyBubbleListFragment -> {//the fragment on which you want to handle your back press
            }
            is CreateEventFragment -> {
                YesNoDialog("", "By exiting you will lose all of your information.\n Are you sure you want to exit?", null, object : onClickDialog {
                    override fun onClickYes() {
                        supportFragmentManager.popBackStackImmediate()
                    }
                })
            }
//            is PastGuestListFriendsFragment -> {
//
//            }super.onBackPressed()
//            is ShowSelectedUserFragment -> {//the fragment on which you want to handle your back press
//                val currFrag = f as ShowSelectedUserFragment
//                f.selectedUserList
//            }
            else -> super.onBackPressed()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            Permissions.PERMISSIONS_REQUEST_WRITE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (!Permissions.checkPermission(this)) {
                        Permissions.showSettings(this)
                    }
                } else {
                    if (!Permissions.checkPermission(this)) {
                        Permissions.showSettings(this)
                    }
                }
                return
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        optionMenu = menu
        menu.clear()

        menuInflater.inflate(R.menu.menu_main, menu)
        val menuItem = menu.findItem(R.id.ic_notification)
        menuItem.icon = buildCounterDrawable(HooleyApp.db.getInt(Constants.UNREAD_NOTIFICATION_COUNT, 0), R.drawable.ic_bell)
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
        if (bubblesManager != null) {
            bubblesManager!!.recycle()
            bubblesManager = null
        }

    }

    override fun onMenuItemClick(position: Int) {
        resideMenu!!.closeMenu()
        handler.postDelayed({ mPerformAction(position) }, 200)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tvEditProfile -> {
                resideMenu!!.closeMenu()
                handler.postDelayed({ callFragment(R.id.container, ProfileMainFragment.newInstance(false), "ProfileMainFragment") }, 200)
            }
            R.id.ivHome -> {
//                if (optionMenu != null)
//                    onCreateOptionsMenu(optionMenu)
                gotoHome()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_CANCELED && requestCode == 786) {
            //            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(this)) {
            //                initializeBubblesManager();
            ////                return;
            //            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
                //                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                //                        Uri.parse("package:" + getPackageName()));
                //                startActivityForResult(intent, 786);
                return
            } else {
                initializeBubblesManager()
                return
            }
        }
        if (resultCode == Activity.RESULT_OK && requestCode == 192) {
            return
        }
        val fragment = supportFragmentManager.findFragmentById(R.id.container)
        when (fragment) {
            is EditPersonalProfileFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is LiveEventFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is LivePostFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is EventDetailFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is EventGalleryFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is EventPagerFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is NearByFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is InviteFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is DashBoardFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is BookmarkEventsFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is ProfileMainFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is EventTicketSetupFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is AddEventAnnouncementFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is ChatWindowFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is EditBusinessProfileFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is UpdatePostFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is CreateEventFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is UpdateEventFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is MyTicketFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is BuyTicketFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            is HooleyWalletCardsFragment -> fragment.onActivityResult(requestCode, resultCode, data)
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun setUpLocationServices() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        try {
            locationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this)
            locationManager!!.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, this)
        } catch (ewz: SecurityException) {

        }

    }

    private fun handleNavigationBar() {
        // Set up UI according to resolution
        when {
            hasNavBar() -> binding!!.container.setPadding(0, 0, 0, navigationBarHeight)
            Util.hasSoftKeys(activity) -> binding!!.container.setPadding(0, 0, 0, navigationBarHeight)
            else -> binding!!.container.setPadding(0, 0, 0, 0)
        }
        val decorView = window.decorView
        decorView.setOnSystemUiVisibilityChangeListener { visibility ->
            when (visibility) {
                4 -> when {
                    hasNavBar() -> binding!!.container.setPadding(0, 0, 0, navigationBarHeight)
                    Util.hasSoftKeys(activity) -> binding!!.container.setPadding(0, 0, 0, navigationBarHeight)
                    else -> binding!!.container.setPadding(0, 0, 0, 0)
                }
                0 -> when {
                    hasNavBar() -> binding!!.container.setPadding(0, 0, 0, navigationBarHeight)
                    Util.hasSoftKeys(activity) -> binding!!.container.setPadding(0, 0, 0, navigationBarHeight)
                    else -> binding!!.container.setPadding(0, 0, 0, 0)
                }
                2 -> binding!!.container.setPadding(0, 0, 0, 0)
            }
        }
    }

    private fun handleIntent(intent: Intent?, fromCreate: Boolean) {
        if (intent != null) {
            if (intent.extras == null)
                return

            try {
                if (!TextUtils.isEmpty(intent.extras.get("body").toString()))
                    HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, true)
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }


            if (intent.extras.get("tag") != null) {
                tag = intent.extras.get("tag") as String
                HooleyApp.db.putString(Constants.TYPE_NOTIFICATION, tag)
                when (tag) {
                    Constants.TYPE_MESSAGES_ALERT -> {
                        var friendName: String = intent.extras.get("body") as String
                        val threadId: String = intent.extras.get("threadId") as String
                        val friendId: String = intent.extras.get("friendId") as String
                        val name = friendName.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                        friendName = name[0]
                        HooleyApp.db.putString(Constants.THREAD_ID, threadId)
                        HooleyApp.db.putString(Constants.FRIEND_NAME, friendName)
                        HooleyApp.db.putString(Constants.FRIEND_ID, friendId)
                        HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, true)
                        loadNotificationFragment(fromCreate)
                    }
                    Constants.TYPE_EVENT_ALERTS -> {
                        HooleyApp.db.putString(Constants.EVENT_ID, intent.extras.get("eventId") as String)
                        HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, true)
                        loadNotificationFragment(fromCreate)
                    }
                    Constants.TYPE_FRIENDS_ALERT -> {
                        HooleyApp.db.putString(Constants.FRIEND_UID, intent.extras.get("userId") as String)
                        HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, true)
                        loadNotificationFragment(fromCreate)
                    }
                    Constants.TYPE_MEDIA_ALERT -> {
                        HooleyApp.db.putString(Constants.EVENT_ID, intent.extras.get("eventId") as String)
                        HooleyApp.db.putString(Constants.MEDIA_ID, intent.extras.get("mediaId") as String)
                        HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, true)
                        loadNotificationFragment(fromCreate)
                    }
                    Constants.TYPE_TICKET_ALERT -> {
                        HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, true)
                        loadNotificationFragment(fromCreate)
                    }
                    Constants.TYPE_TICKETSALE_ALERT -> {
                        HooleyApp.db.putString(Constants.EVENT_ID, intent.extras.get("eventId") as String)
                        HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, true)
                        loadNotificationFragment(fromCreate)
                    }
                    Constants.TYPE_UPDATE_ANNOUNCEMENTS_ALERT -> {
                        HooleyApp.db.putString(Constants.EVENT_ID, intent.extras.get("eventId") as String)
                        HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, true)
                        loadNotificationFragment(fromCreate)
                    }
                    Constants.TYPE_ANNOUNCEMENTS_ALERT -> {
                        HooleyApp.db.putString(Constants.EVENT_ID, intent.extras.get("eventId") as String)
                        HooleyApp.db.putString(Constants.EVENT_NAME, intent.extras.get("eventName") as String)
                        HooleyApp.db.putString(Constants.EVENT_ADDRESS, intent.extras.get("address") as String)
                        if (intent.extras.get("imageUrl") != null)
                            HooleyApp.db.putString(Constants.EVENT_ANNOUNCEMENT_IMGURL, intent.extras.get("imageUrl") as String)
                        HooleyApp.db.putString(Constants.EVENT_ANNOUNCEMENT_MSG, intent.extras.get("detailMessage") as String)
                        HooleyApp.db.putString(Constants.EVENT_START_TIME, intent.extras.get("startTime") as String)
                        HooleyApp.db.putString(Constants.EVENT_END_TIME, intent.extras.get("endTime") as String)
                        HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, true)
                        loadNotificationFragment(fromCreate)
                    }
                    else -> {
                        (activity as ActivityBase).addFragment(R.id.container, NotificationMainFragment.newInstance(), "NotificationMainFragment")
                    }
                }
            } else {
                loadFragmentInActivity()
                val data = intent.data
                if (data != null && data.isHierarchical) {
                    val uri = intent.dataString
                    Logger.ex("MyApp Deep link clicked " + uri!!)
                    // store isDeeplink true
                    HooleyApp.db.putBoolean(Constants.IS_DEEPLINK, true)
                    HooleyApp.db.putString(Constants.DEEPLINK_URI, uri)
                    return
                }
            }
        }
    }

    private fun hasNavBar(): Boolean {
        val id = applicationContext.resources.getIdentifier("config_showNavigationBar", "bool", "android")
        return id > 0 && applicationContext.resources.getBoolean(id)
    }

    private fun addBackStackListener() {
        supportFragmentManager.addOnBackStackChangedListener {
            val manager = supportFragmentManager
            if (manager != null) {
                val container = manager.findFragmentById(R.id.container)
                val fragmentName = container!!.javaClass.name.split(".").last()
                // Setting toolbar titles
                if (fragmentName.getTitleName() == "") {
                    when (container) {
                        is LiveEventFragment -> {
                            container.setToolBar()
                        }
                        is SelectFriendFragment -> {
                            container.setView()
                        }
                        is CancelEventFragment -> {
                            container.setData()
                        }
                        is AllFeedsSinglePostCommentFragment -> {
                            container.setToolBar()
                        }
                        is ChatWindowFragment -> {
                            container.setToolbar()
                        }
                        is FriendsProfileFragment -> {
                            container.setToolbar()
                        }
                        is EventTicketSetupFragment -> {
                            container.setToolbar()
                        }
                        is ShowSelectedUserFragment -> {
                            container.setToolbar()
                        }
                        is HooleyWalletCardsFragment -> {
                            container.setToolbar()
                        }
                        is HooleyWalletFragment -> {
                            container.setToolbar()
                        }
                        is EventDetailAnnouncementFragment -> {
                            container.setToolbar()
                        }
                        is CreateEventFragment -> {
                            container.setToolbar()
                        }
                        is EventDetailGuestListFragment -> {
                            container.setToolbar()
                        }
                        is BuyTicketFragment -> {
                            container.setToolbar(1)
                        }
                    }
                } else {
                    setToolBarTitle(this, fragmentName.getTitleName())
                }
                // perform actions
                when (container) {
                    is InviteHooleyFriendsFragment -> {
                        container.onBackPressUpdateList()
                    }
                    is DashBoardFragment -> {
                        container.setToolbar()
//                        if (optionMenu != null)
//                            onCreateOptionsMenu(optionMenu)
                    }
//                    is TicketPagerFragment -> {
////                        supportActionBar?.show()
//                    }
                }
                when (container) {
                    is DashBoardFragment -> {
                        Glide.with(activity!!).load(HooleyApp.db.getString(Constants.USER_AVATAR)).into(ivAvatar_!!)
                        if (bubbleList!!.size > 0) {
                            if (!isBubbleShowing) {
                                HooleyApp.db.putBoolean(Constants.IS_BUBBLE_CLICKABLE, true)
                                addNewChatHead()
                            }
                        }
                    }
                    is MyEventMainFragment -> {
                        if (bubbleList!!.size > 0) {
                            if (!isBubbleShowing) {
                                HooleyApp.db.putBoolean(Constants.IS_BUBBLE_CLICKABLE, true)
                                addNewChatHead()
                            }
                        }
                    }
                    is LiveEventFragment -> {
                        hideDrawer(activity!!)
                        showHomeButton(true)
                    }
                    is NotificationMainFragment -> {
                        hideDrawer(activity!!)
                        showHomeButton(true)
                    }
                    is CreateEventFragment -> {
                        showHomeButton(false)
                    }
                    is UpdateEventFragment -> {
                        showHomeButton(false)
                    }
                    is CancelEventFragment -> {
                        showHomeButton(false)
                    }
                    is UpdateEventDataTimeFragment -> {
                        showHomeButton(false)
                    }
                    is EventTicketSetupFragment -> {
                        showHomeButton(false)
                    }
                    is PreviewEventFragment -> {
                        showHomeButton(false)
                    }
                    is ConfirmCreateEventFragment -> {
                        showHomeButton(true)
                    }
                    is GeofenceFragment -> {
                        showHomeButton(false)
                    }
                    is HooleyGalleryFragment -> {
                        showHomeButton(false)
                    }
                    is PastGuestListFriendsFragment -> {
                        showHomeButton(false)
                    }
                    is AddIncomeSharePartnerFragment -> {
                        showHomeButton(false)
                    }
                    is ViewSalesFragment -> {
                        showHomeButton(false)
                    }
                    else -> {
                        showHomeButton(true)
                    }
                }
            }
        }
    }

    private fun loadFragmentInActivity() {
        callFragment(R.id.container, DashBoardFragment.newInstance(), null)
    }

    private fun loadNotificationFragment(fromCreate: Boolean) {
        if (fromCreate)
            callFragment(R.id.container, DashBoardFragment.newInstance(), null)
        else {
            HooleyApp.db.putBoolean(Constants.IS_FROM_NOTIFICATION, false)
            when (HooleyApp.db.getString(Constants.TYPE_NOTIFICATION)) {
                Constants.TYPE_MESSAGES_ALERT -> callFragment(R.id.container, ChatWindowFragment(HooleyApp.db.getString(Constants.THREAD_ID), HooleyApp.db.getString(Constants.FRIEND_ID), HooleyApp.db.getString(Constants.FRIEND_NAME)), "ChatWindowFragment")
                Constants.TYPE_EVENT_ALERTS -> (activity as ActivityBase).callFragment(R.id.container, LiveEventFragment.newInstance(HooleyApp.db.getString(Constants.EVENT_ID)), "LiveEventFragment")
                Constants.TYPE_FRIENDS_ALERT -> {
                    val avatarfragment = ViewAvatarFragment(HooleyApp.db.getString(Constants.FRIEND_UID).toInt())
                    avatarfragment.show(activity!!.supportFragmentManager)
                }
                Constants.TYPE_MEDIA_ALERT -> (activity as ActivityBase).callFragment(R.id.container, AllFeedsSinglePostCommentFragment.newInstance(HooleyApp.db.getString(Constants.EVENT_ID), HooleyApp.db.getString(Constants.MEDIA_ID)), "AllFeedsSinglePostCommentFragment")
                Constants.TYPE_TICKET_ALERT -> {
                    (activity as ActivityBase).callFragment(R.id.container, MyTicketFragment(), "MyTicketFragment")
                }
                Constants.TYPE_TICKETSALE_ALERT -> {
                    (activity as ActivityBase).callFragment(R.id.container, ViewSalesFragment.newInstance(HooleyApp.db.getString(Constants.EVENT_ID)), "ViewSalesFragment")
                }
                Constants.TYPE_UPDATE_ANNOUNCEMENTS_ALERT -> {
                    (activity as ActivityBase).callFragment(R.id.container, EventDetailAnnouncementFragment(HooleyApp.db.getString(Constants.EVENT_ID), false), "EventDetailAnnouncementFragment")
                }
                Constants.TYPE_ANNOUNCEMENTS_ALERT -> {
                    activity!!.showAnnouncementDialog(HooleyApp.db.getString(Constants.EVENT_ANNOUNCEMENT_MSG),
                            HooleyApp.db.getString(Constants.EVENT_ANNOUNCEMENT_IMGURL),
                            HooleyApp.db.getString(Constants.EVENT_NAME),
                            HooleyApp.db.getString(Constants.EVENT_START_TIME),
                            HooleyApp.db.getString(Constants.EVENT_END_TIME),
                            HooleyApp.db.getString(Constants.EVENT_ADDRESS),
                            object : onClickDialog {
                                override fun onClickYes() {
                                    (activity as ActivityBase).callFragment(R.id.container, LiveEventFragment.newInstance(HooleyApp.db.getString(Constants.EVENT_ID)), "LiveEventFragment")
                                }
                            })
                }
            }
        }
    }

    private fun initializeBubblesManager() {
        bubblesManager = BubblesManager.Builder(activity)
//                .setTrashLayout(R.layout.bubble_trash_layout)
                .setInitializationCallback { Logger.ex("Service initialize") }
                .build()
        bubblesManager!!.initialize()
    }

    fun addNewChatHead() {
        isBubbleShowing = true

        bubbleView = LayoutInflater.from(this@HooleyMain).inflate(R.layout.bubble_layout, null) as BubbleLayout
        bubbleView!!.isBubbleUse(true)

        ivEventAvatar = bubbleView!!.findViewById<CircleImageView>(R.id.ivEventAvatar)
        val rlLive = bubbleView!!.findViewById<RelativeLayout>(R.id.rlLive)
        try {
            Glide.with(activity!!).load(HooleyApp.db.getString(Constants.CHAT_HEAD_EVENT_AVATAR)).into(ivEventAvatar)
        } catch (e: Exception) {
            e.printStackTrace()
            return
        }

        bubbleView!!.setOnBubbleRemoveListener {
            if (HooleyApp.db.getBoolean(Constants.BUBBLE_REMOVE_BY_USER)) {
                HooleyApp.db.putBoolean(Constants.HAS_BUBBLE_HISTORY, false)
                val bubbleList = bubbleList
                if (bubbleList != null && bubbleList.size > 0)
                    bubbleList.clear()
                val bubbleListString = gson.toJson(bubbleList)
                HooleyApp.db.putString(Constants.GSON_BUBBLE_LIST, bubbleListString)
                removeBubbleView()
            }
        }

        bubbleView!!.setOnBubbleLongClickListener {
            if (HooleyApp.db.getBoolean(Constants.HAS_BUBBLE_HISTORY)) {
                if (MoreMediaDialogFragment().isVisible) {
                    MoreMediaDialogFragment().dismiss()
                }
                vibrate()
                removeBubbleView()
                handler.postDelayed({
                    (activity as ActivityBase).addFragment(R.id.container, MyBubbleListFragment.newInstance(), "MyBubbleListFragment")
                }, 50)

            }
        }

        rlLive.setOnLongClickListener(View.OnLongClickListener {
            if (HooleyApp.db.getBoolean(Constants.HAS_BUBBLE_HISTORY)) {
                if (MoreMediaDialogFragment().isVisible) {
                    MoreMediaDialogFragment().dismiss()
                }
                vibrate()
                removeBubbleView()
                bubbleView!!.isBubbleUse(false)
                handler.postDelayed({ (activity as ActivityBase).addFragment(R.id.container, MyBubbleListFragment.newInstance(), "MyBubbleListFragment") }, 50)
                return@OnLongClickListener true
            }
            false
        })

        bubbleView!!.setOnBubbleClickListener {
            if (Util.isAppIsInBackground(applicationContext)) {
                val it = Intent("intent.action.ChatBubble")
                it.component = ComponentName(packageName, HooleyMain::class.java.name)
                //                    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(it)
                removeBubbleView()
            } else {
                var fragment = supportFragmentManager.findFragmentById(R.id.container)

                if (fragment is MyEventMainFragment) {
                    var pagerFragment = fragment.adapter!!.getItem(MyEventMainFragment.fragmentMyEventMainBinding.vpMyEvent.currentItem)
                    if (pagerFragment is MyHostEventFragment) {
                        if ((MyHostEventFragment).moreFilterDialogeFragment != null)
                            if ((MyHostEventFragment).moreFilterDialogeFragment!!.isVisible)
                                (MyHostEventFragment).moreFilterDialogeFragment!!.dismiss()
                    }
                }

                (activity as ActivityBase).callFragment(R.id.container, LiveEventFragment.newInstance(HooleyApp.db.getString(Constants.CHAT_HEAD_EVENT_ID)), "LiveEventFragment")
                hideDrawer(activity!!)
                removeBubbleView()
            }
        }

        bubbleView!!.setShouldStickToWall(true)

        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)

        if (bubblesManager != null) {
            bubblesManager!!.addBubble(bubbleView, size.x, size.y)
            HooleyApp.db.putBoolean(Constants.BUBBLE_REMOVE_BY_USER, false)
            HooleyApp.db.putBoolean(Constants.BUBBLE_REMOVE_BY_SYSTEM, false)
        }
    }

    fun removeBubbleView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (bubbleView != null && bubbleView!!.isAttachedToWindow)
                Handler().postDelayed({
                    HooleyApp.db.putBoolean(Constants.BUBBLE_REMOVE_BY_SYSTEM, true)
                    if (bubblesManager != null) {
                        isBubbleShowing = false
                        bubblesManager!!.removeBubble(bubbleView)
                    }
                    //                        bubblesManager.removeTrashView();
                }, 50)
            Handler().postDelayed({
                if (bubblesManager != null) {
                    isBubbleShowing = false
                    bubblesManager!!.removeAllBubble()
                }
            }, 100)
        }
    }

    private fun gotoEventScreen(uri: String) {
        val urlparts = uri.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        var id = ""
        if (urlparts[0].contains("EventPreview")) {
            id = urlparts[1]
            (activity as ActivityBase).callFragment(R.id.container, LiveEventFragment.newInstance(id), "NotificationMainFragment")
        } else {
            val ba = urlparts[1].split("&".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            id = ba[0]
            (activity as ActivityBase).callFragment(R.id.container, AllFeedsSinglePostCommentFragment.newInstance(urlparts[2], id), "AllFeedsSinglePostCommentFragment")
        }
        hideDrawer(activity!!)
    }

    fun setToolbarTitle(strTitle: String) {
        binding!!.toolbarTitle.text = strTitle
    }

    private fun setMenuItems() {
        resideMenu = ResideLayout(activity!!, R.layout.layout_side_menu)
        resideMenu!!.setBackground(R.drawable.bg_tour_header)
        resideMenu!!.attachToActivity(activity!!)
        resideMenu!!.setScaleValue(0.7f)
        resideMenu!!.setSwipeDirectionDisable(ResideLayout.DIRECTION_RIGHT)
        resideMenu!!.setSwipeDirectionDisable(ResideLayout.DIRECTION_LEFT)
        val leftMenu = resideMenu!!.leftMenuView!!
        ivAvatar_ = leftMenu.findViewById<ImageView>(R.id.ivAvatar)
        val ivClose = leftMenu.findViewById<ImageView>(R.id.ivClose)
        val tvUserName = leftMenu.findViewById<TextView>(R.id.tvUserName)
        val tvEditProfile = leftMenu.findViewById<TextView>(R.id.tvEditProfile)
        val rvMenuItems = leftMenu.findViewById<RecyclerView>(R.id.rvMenuItems)

        if (!TextUtils.isEmpty(HooleyApp.db.getString(Constants.USER_AVATAR)))
            Glide.with(appActivity).load(HooleyApp.db.getString(Constants.USER_AVATAR)).into(ivAvatar_!!)
        else
            Glide.with(appActivity).load(R.drawable.ic_avatar_place_holder).into(ivAvatar_!!)

        if (!TextUtils.isEmpty(HooleyApp.db.getString(Constants.FULL_NAME)))
            tvUserName.text = HooleyApp.db.getString(Constants.FULL_NAME)
        tvEditProfile.setOnClickListener(this)
        ivClose.setOnClickListener { resideMenu!!.closeMenu() }
        val llManager = LinearLayoutManager(applicationContext)
        rvMenuItems.layoutManager = llManager
        adapter = AdapterMenuItems(applicationContext)
        adapter!!.mListener = this
        rvMenuItems.adapter = adapter
    }

    private fun showLogOutDialog() {
        val alertDialog = AlertDialog.Builder(this@HooleyMain).create()
        alertDialog.setTitle("Are you sure")
        alertDialog.setMessage("you want to Logout")
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel"
        ) { dialog, _ -> dialog.dismiss() }
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK"
        ) { dialog, _ ->
            dialog.dismiss()
            logoutClick()
        }

        alertDialog.show()
    }

    private lateinit var notificationManager: NotificationManager

    private fun logoutClick() {
        if (!NetworkUtil.isInternetConnected(activity!!)) {
            Util.showToastMessage(activity, Constants.NO_NETWORK)
            return
        }
        activity!!.showDialog()
        service.logoutUser(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                notificationManager = activity!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.cancelAll()
                activity!!.removeDialog()
//                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                val token = HooleyApp.db.getString(Constants.GCM_REGISTRATION_TOKEN)
                HooleyApp.db.clear()
                HooleyApp.db.putString(Constants.GCM_REGISTRATION_TOKEN, token)
                SocialClass.logoutFaceBook()
                val intent = Intent(activity, HooleyAuth::class.java)
                startActivity(intent)
                finish()
            }

            override fun failure(message: String) {
                activity!!.removeDialog()
                try {
                    Util.showToastMessage(appActivity, message)
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }
        })
    }

    private fun mPerformAction(position: Int) {
        when (position) {
            0 -> {
            }
            1 -> {
                callFragment(R.id.container, MyGeoFenceFragment.newInstance(), "MyGeoFenceFragment")
                hideDrawer(activity!!)
            }
            2 -> {
                callFragment(R.id.container, ProfileMainFragment.newInstance(true), "ProfileMainFragment")
                HooleyApp.db.putBoolean(Constants.IS_FRIEND_CLICK, true)
                hideDrawer(activity!!)
            }
            3 -> {
                if (HooleyApp.db.getBoolean(Constants.IS_WALLET_SETUP)) {
                    callFragment(R.id.container, HooleyWalletCardsFragment(false, "Hooley Wallet"), "HooleyWalletCardsFragment")
                } else {
                    callFragment(R.id.container, HooleyWalletFragment(false, false, "Save Card"), "HooleyWalletFragment")
                }
                hideDrawer(activity!!)
            }
            4 -> {
                callFragment(R.id.container, TermsAndConditionFragment.newInstance(true, false), "TermsAndConditionFragment")
                hideDrawer(activity!!)
            }
            5 -> {
                callFragment(R.id.container, PrivacyPolicyFragment(), "PrivacyPolicyFragment")
                hideDrawer(activity!!)
            }
            6 -> {
                if (isBubbleShowing)
                    (activity as HooleyMain).removeBubbleView()
                hideDrawer(activity!!)
                if (themeType == AppCompatDelegate.MODE_NIGHT_YES) {
                    themeType = AppCompatDelegate.MODE_NIGHT_NO
                    HooleyApp.db.putInt(Constants.TYPE_THEME, themeType)
                    umm.nightMode = themeType
                    delegate.setLocalNightMode(themeType)
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                    recreate()
                    setDarkTheme(false)
                } else {
                    themeType = AppCompatDelegate.MODE_NIGHT_YES
                    HooleyApp.db.putInt(Constants.TYPE_THEME, themeType)
                    umm.nightMode = themeType
                    delegate.setLocalNightMode(themeType)
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                    recreate()
                    setDarkTheme(true)
                }
            }
            7 -> {
                callFragment(R.id.container, FeedbackFragment(), "FeedbackFragment")
                hideDrawer(activity!!)
            }
            8 -> {
                callFragment(R.id.container, FaqsFragment(), "FaqsFragment")
                hideDrawer(activity!!)
            }
            9 -> {
                try {
                    showShareDialog()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            10 -> {
                callFragment(R.id.container, AboutHooleyFragment(), "AboutHooleyFragment")
                hideDrawer(activity!!)
            }
            11 -> {
                (activity as HooleyMain).removeBubbleView()
                showLogOutDialog()
            }
        }
    }

    @Throws(IOException::class)
    private fun showShareDialog() {
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT,
                "Please install Hooley app using following link.\n" + "https://play.google.com/store/apps/details?id=com.hooleyapp.hooley")
        shareIntent.type = "text/plain"
        startActivity(Intent.createChooser(shareIntent, "Share Hooley"))
    }

    private fun getHashKey() {
        try {
            val info = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {

        } catch (e: NoSuchAlgorithmException) {

        }

    }

    private fun updateCurrentLatLong() {
        if (!NetworkUtil.isInternetConnected(activity!!))
            return
        val userID = HooleyApp.db.getString(Constants.USER_ID)
        val latitude = java.lang.Double.toString(HooleyApp.db.getDouble(Constants.CURRENT_LAT, 0.0))
        val longitude = java.lang.Double.toString(HooleyApp.db.getDouble(Constants.CURRENT_LANG, 0.0))
        val call = HooleyApp.apiService.updateCurrentLatLong(UpdateLatLongRequestModel(userID, latitude, longitude))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    Util.showToastMessage(activity, "Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {

                } else {
                    Util.showToastMessage(activity, response.body()!!.exception)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                Logger.ex("ERROR:" + t.message)
                Util.showToastMessage(activity, t.message)
            }
        })
    }

    private fun gotoHome() {
        val fm = supportFragmentManager
        /*   if (fm.getBackStackEntryCount() <= 1)
            return;*/
        for (i in 0 until fm.backStackEntryCount) {
            fm.popBackStack()
        }

    }

    private fun buildCounterDrawable(count: Int, backgroundImageId: Int): Drawable {
        val inflater = LayoutInflater.from(this)
        val view = inflater.inflate(R.layout.layout_notification_badge, null)
        view.setBackgroundResource(backgroundImageId)

        if (count == 0) {
            val counterTextPanel = view.findViewById<View>(R.id.counterValuePanel)
            counterTextPanel.visibility = View.GONE
        } else {
            val textView = view.findViewById<TextView>(R.id.count)
            textView.text = count.toString()
        }
        view.measure(
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
        view.layout(0, 0, view.measuredWidth, view.measuredHeight)

        view.isDrawingCacheEnabled = true
        view.drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
        val bitmap = Bitmap.createBitmap(view.drawingCache)
        view.isDrawingCacheEnabled = false

        return BitmapDrawable(resources, bitmap)
    }

    fun shareWithImage(bitmap: Bitmap, eventName: String, imgCaption: String) {
        val bitmapPath = MediaStore.Images.Media.insertImage(activity!!.contentResolver, bitmap, "Thumb", null)
        val bitmapUri = Uri.parse(bitmapPath)
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "image/png"
        val receiver = Intent(activity, ApplicationSelectorReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(activity, 0, receiver, PendingIntent.FLAG_UPDATE_CURRENT)
        intent.putExtra(Intent.EXTRA_STREAM, bitmapUri)
        intent.putExtra(Intent.EXTRA_TEXT, "Event Name = " + eventName
                + "\nMedia Caption = " + imgCaption)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            startActivity(Intent.createChooser(intent, null, pendingIntent.intentSender))
        } else {
            startActivity(Intent.createChooser(intent, "Share"))
        }
    }

    fun shareWithImage(bitmap: Bitmap, eventName: String, imgCaption: String, eventId: String, postId: String) {
        val bitmapPath = MediaStore.Images.Media.insertImage(activity!!.contentResolver, bitmap, "Thumb", null)
        val bitmapUri = Uri.parse(bitmapPath)
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "image/png"
        val receiver = Intent(activity, ApplicationSelectorReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(activity, 0, receiver, PendingIntent.FLAG_UPDATE_CURRENT)
        intent.putExtra(Intent.EXTRA_STREAM, bitmapUri)
        intent.putExtra(Intent.EXTRA_TEXT,
                imgCaption + "\n " +
                        Constants.POST_SHARE + postId + "&eventId=" + eventId)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            startActivity(Intent.createChooser(intent, null, pendingIntent.intentSender))
        } else {
            startActivity(Intent.createChooser(intent, "Share"))
        }
    }

    fun shareWithImage(bitmap: Bitmap, eventName: String, imgCaption: String, Date: String) {
        val bitmapPath = MediaStore.Images.Media.insertImage(activity!!.contentResolver, bitmap, "Thumb", null)
        val bitmapUri = Uri.parse(bitmapPath)
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "image/png"
        val receiver = Intent(activity, ApplicationSelectorReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(activity, 0, receiver, PendingIntent.FLAG_UPDATE_CURRENT)
        intent.putExtra(Intent.EXTRA_STREAM, bitmapUri)
        intent.putExtra(Intent.EXTRA_TEXT,
                imgCaption
                        + "\n" + eventName
                        + "\nStart Date = " + Date)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            startActivity(Intent.createChooser(intent, null, pendingIntent.intentSender))
        } else {
            startActivity(Intent.createChooser(intent, "Share"))
        }
    }

    fun vibrate() {
        val vibrator = applicationContext.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        vibrator.vibrate(VIBRATION_DURATION_IN_MS.toLong())
    }

    fun setDarkTheme(isDark: Boolean) {
        service.setDarkTheme(isDark, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
            }

            override fun failure(message: String) {

            }
        })
    }

    companion object {
        var activity: AppCompatActivity? = null
        const val VIBRATION_DURATION_IN_MS = 70
    }
}
