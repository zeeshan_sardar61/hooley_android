package com.hooleyapp.hooley.activites

//import com.afollestad.materialdialogs.MaterialDialog;
import android.annotation.TargetApi
import android.app.ProgressDialog
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.Nullable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.FragmentVideoPlayerBinding
import com.hooleyapp.hooley.fragments.BaseFragment

/**
 * Created by Nauman on 2/9/2018.
 */

class VideoPlayerActivity : BaseFragment() {

    private var binding: FragmentVideoPlayerBinding? = null
    //    private EasyVideoPlayer player;
    var pDialog: ProgressDialog? = null

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_video_player, container, false)
        //        getAllEventFeeds(LiveEventFragment.eventId)

        //        userSearchbinding.player.setCallback(this);
        //        userSearchbinding.player.setSource(Uri.parse(videoUrl));
        pDialog = ProgressDialog(HooleyMain.activity)
        pDialog?.setMessage("Buffering...")
        pDialog?.isIndeterminate = false
        pDialog?.setCancelable(true)
        // Show progressbar
        pDialog?.show()

        try {
//            Start the MediaController
            var mediaController = MediaController(HooleyMain.activity)
            mediaController.setAnchorView(binding!!.vidPlayer)

            val videoUri = Uri.parse(videoUrl)
            binding!!.vidPlayer.setMediaController(mediaController)
            binding!!.vidPlayer.setVideoURI(videoUri)

        } catch (e: Exception) {

            e.printStackTrace()
        }

        binding!!.vidPlayer.requestFocus()
        binding!!.vidPlayer.setOnPreparedListener {
            pDialog?.dismiss()
            binding!!.vidPlayer.start()
        }
        binding!!.vidPlayer.setOnCompletionListener {
            if (pDialog?.isShowing!!) {
                pDialog?.dismiss()
            }
        }



        return binding!!.root
    }

    companion object {
        lateinit var instance: VideoPlayerActivity
        var videoUrl: String = ""

        fun newInstance(url: String): VideoPlayerActivity {
            videoUrl = url
            instance = VideoPlayerActivity()
            return instance
        }
    }

    /*
    @Override
    public void onStarted(EasyVideoPlayer player) {
    }

    @Override
    public void onPaused(EasyVideoPlayer player) {
    }

    @Override
    public void onPreparing(EasyVideoPlayer player) {
        Log.d("EVP-Sample", "onPreparing()");
    }

    @Override
    public void onPrepared(EasyVideoPlayer player) {
        Log.d("EVP-Sample", "onPrepared()");
    }

    @Override
    public void onBuffering(int percent) {
        Log.d("EVP-Sample", "onBuffering(): " + percent + "%");
    }

    @Override
    public void onError(EasyVideoPlayer player, Exception e) {
        Log.d("EVP-Sample", "onError(): " + e.getMessage());
//        new MaterialDialog.Builder(this)
//                .title(R.string.error)
//                .content(e.getMessage())
//                .positiveText(android.R.string.ok)
//                .show();
    }

    @Override
    public void onCompletion(EasyVideoPlayer player) {
        Log.d("EVP-Sample", "onCompletion()");
    }

    @Override
    public void onRetry(EasyVideoPlayer player, Uri source) {
//        Toast.makeText(this, "Retry", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSubmit(EasyVideoPlayer player, Uri source) {
//        Toast.makeText(this, "Submit", Toast.LENGTH_SHORT).show();
    }
*/

}