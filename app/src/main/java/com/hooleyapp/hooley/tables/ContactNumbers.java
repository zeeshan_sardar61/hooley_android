package com.hooleyapp.hooley.tables;

import com.hooleyapp.hooley.db.AppDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.NotNull;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Nauman on 12/28/2017.
 */


@Table(database = AppDatabase.class, cachingEnabled = true)
public class ContactNumbers extends BaseModel {


    @PrimaryKey(autoincrement = true)
    public long id;

    @NotNull
    @Column
    public long contactDataID;

    @NotNull
    @Column
    public String contactNumber;  //Raw Number saved in DB.

    @Column
    public String contactE164Number;   // E164 Formatted Number.

    @Column
    public int contactNumberType;   // Work, Mobile, Home

    @Column(defaultValue = "0")
    public int contactIsAppUser;

    @Column
    public long contactID; // contactId

    @Column(defaultValue = "0")
    public int contactState;   //Sync State, Added, Synced etc

    @Column(defaultValue = "0")
    public int isNewAppUser;


//    @NotNull
//    @ForeignKey(saveForeignKeyModel = false, onDelete = ForeignKeyAction.CASCADE)
//    public ForeignKeyContainer<ContactNames> nameContactID;
//
//    public void associateContactNames(ContactNames contactNames) {
//        nameContactID = FlowManager.getContainerAdapter(ContactNames.class).toForeignKeyContainer(contactNames);
//    }
}
