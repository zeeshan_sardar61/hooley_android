package com.hooleyapp.hooley.tables;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.RequiresApi;

import com.hooleyapp.hooley.db.AppDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.NotNull;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.QueryModel;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.ArrayList;
import java.util.Comparator;


/**
 * Created by Nauman on 12/22/2017.
 */

@QueryModel(database = AppDatabase.class)
@Table(database = AppDatabase.class, cachingEnabled = true)
public class FriendsTable extends BaseModel implements Parcelable {

    public static final Creator<FriendsTable> CREATOR = new Creator<FriendsTable>() {
        @Override
        public FriendsTable createFromParcel(Parcel in) {
            return new FriendsTable(in);
        }

        @Override
        public FriendsTable[] newArray(int size) {
            return new FriendsTable[size];
        }
    };
    //Getter and setter methods same as the above examples
    /*Comparator for sorting the list by Student Name*/
    public static Comparator<FriendsTable> sortByNameComparator = new Comparator<FriendsTable>() {

        public int compare(FriendsTable s1, FriendsTable s2) {
            String StudentName1 = s1.fullName.toUpperCase();
            String StudentName2 = s2.fullName.toUpperCase();
            return StudentName1.compareTo(StudentName2);

        }
    };
    /*Comparator for sorting the list by roll no*/
    public static Comparator<FriendsTable> sortByHooleyFriend = new Comparator<FriendsTable>() {

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        public int compare(FriendsTable s1, FriendsTable s2) {
            return Boolean.compare(s2.isFriend, s1.isFriend);
        }
    };

    public static Comparator<FriendsTable> sortByCityComparator = new Comparator<FriendsTable>() {

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        public int compare(FriendsTable s1, FriendsTable s2) {
            String CityName1 = s1.cityName.toUpperCase();
            String CityName2 = s2.cityName.toUpperCase();
            return CityName1.compareTo(CityName2);
        }
    };

    /*Comparator for sorting the list by roll no*/
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static Comparator<FriendsTable> sortByShareInterest = new Comparator<FriendsTable>() {
        public int compare(FriendsTable s1, FriendsTable s2) {
            return Boolean.compare(s2.isSharedIntrest, s1.isSharedIntrest);
        }
    };

    public static Comparator<FriendsTable> sortByTimeComparator = new Comparator<FriendsTable>() {
        public int compare(FriendsTable s1, FriendsTable s2) {
            String s = s1.dateAndTime.toUpperCase();
            String s3 = s2.dateAndTime.toUpperCase();
            return s.compareTo(s3);
        }
    };

    @PrimaryKey(autoincrement = true)
    public int id;
    @Unique(onUniqueConflict = ConflictAction.REPLACE)
    @NotNull
    @Column
    public int userId;
    @Column
    public String profilePic;
    @NotNull
    @Column
    public String fullName;
    @Column
    public String homeTown;
    @Column
    public int gender;
    @Column
    public String imJid;
    @Column
    public String ageGroup;
    @Column
    public boolean isFriend = true;
    @Column
    public String countryName;
    @Column
    public String stateName;
    @Column
    public String cityName;
    public String dateAndTime;
    public String status;
    @Column
    public boolean isSharedIntrest;
    @Column
    public boolean isConnectedToStripe;
    public ArrayList<String> userIntrestList;
    public boolean isChecked = false;
    public String header;
    public int percent;
    public boolean isMessageAccessible;
    public boolean isFriendRequestAccessible;

    public FriendsTable() {

    }

    protected FriendsTable(Parcel in) {
        percent = in.readInt();
    }

    public int getUserId() {
        return userId;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public String getFullName() {
        return fullName;
    }

    public int getId() {
        return id;
    }

    public String getHomeTown() {
        return homeTown;
    }

    public void setHomeTown(String homeTown) {
        this.homeTown = homeTown;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(percent);
    }
}
