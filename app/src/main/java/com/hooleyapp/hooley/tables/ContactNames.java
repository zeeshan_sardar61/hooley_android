package com.hooleyapp.hooley.tables;

import com.hooleyapp.hooley.db.AppDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.NotNull;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.ArrayList;

/**
 * Created by Nauman on 12/28/2017.
 */


@Table(database = AppDatabase.class, cachingEnabled = true)
public class ContactNames extends BaseModel {

    @Unique(onUniqueConflict = ConflictAction.REPLACE)
    @PrimaryKey(autoincrement = true)
    public long id;

    @Unique(onUniqueConflict = ConflictAction.REPLACE)
    @NotNull
    @Column
    public long cnContactID;

    @Column
    public String cnName;

    @Column
    public String cnAppName;


    @NotNull
    @Column(defaultValue = "0")
    public int cnIsAppUser;

    @Column(defaultValue = "0")
    public int cnIsFavourite;

    ArrayList<ContactNumbers> contactNumbersList;

//    @OneToMany(methods = {OneToMany.Method.ALL}, variableName = "contactNumbersList")
//    public ArrayList<ContactNumbers> getContactNumbersList() {
//        if (contactNumbersList == null || contactNumbersList.isEmpty()) {
//            contactNumbersList = SQLite.select().from(ContactNumbers.class)
//                    .where(ContactNumbers_Table.nameContactID_id.eq(this.id))
//                    .queryList();
//        }
//        return contactNumbersList;
//    }

}

