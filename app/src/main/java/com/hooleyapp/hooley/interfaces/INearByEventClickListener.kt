package com.hooleyapp.hooley.interfaces

import com.hooleyapp.hooley.model.NearByEventsFinalModel

/**
 * Created by Zeeshan on 26-Feb-18.
 */

interface INearByEventClickListener {

    fun onClickLike(nearByEventsFinalModel: NearByEventsFinalModel, position: Int, mListPosition: Int)

    fun onClickFollow(nearByEventsFinalModel: NearByEventsFinalModel, position: Int, mListPosition: Int)

    fun onClickComment()

    fun onUpComingEvent()

    fun onPastEvent()

    fun onClickShare(nearByEventsFinalModel: NearByEventsFinalModel, position: Int)

}
