package com.hooleyapp.hooley.interfaces

/**
 * Created by Zeeshan on 23-Jan-18.
 */

interface IInterestDeleteClick {

    fun onClickItem(name: String, position: Int)
}
