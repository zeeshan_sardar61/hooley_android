package com.hooleyapp.hooley.interfaces

/**
 * Created by Zeeshan on 26-Feb-18.
 */

interface INearByEventHeaderClickListener {

    fun onClickHeaderLike(position: Int, listPosition: Int)

    fun onClickHeaderComment()

    fun onClickHeaderShare(position: Int, listPosition: Int)

    fun onClickHeaderFollow(postition: Int, listPosition: Int)

}
