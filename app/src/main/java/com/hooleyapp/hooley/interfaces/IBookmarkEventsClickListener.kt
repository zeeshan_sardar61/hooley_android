package com.hooleyapp.hooley.interfaces

import com.hooleyapp.hooley.model.BookmarkEventModel

/**
 * Created by Zeeshan on 21-Mar-18.
 */

interface IBookmarkEventsClickListener {

    fun onClickLike(eventObj: BookmarkEventModel.EventDetail, position: Int)

    fun onClickShare(eventObj: BookmarkEventModel.EventDetail, position: Int)

    fun onClickFollow(eventObj: BookmarkEventModel.EventDetail, position: Int)
}
