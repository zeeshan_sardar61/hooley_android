package com.hooleyapp.hooley.interfaces

import android.net.Uri

/**
 * Created by adilmalik on 19/06/2018.
 */
interface ICallBackListener<T> {
    fun success(result: T?)

    fun failure(message: String?)

}

interface ICallBackUri {
    fun imageUri(result: Uri?)
}


