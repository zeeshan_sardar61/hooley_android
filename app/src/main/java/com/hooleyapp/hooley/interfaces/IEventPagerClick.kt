package com.hooleyapp.hooley.interfaces

/**
 * Created by Zeeshan on 21-Feb-18.
 */

interface IEventPagerClick {

    fun onClickShare(eventId: String, eventCover: String, pos: Int)

    fun onClickFollow(eventId: String, isFollow: Boolean, pos: Int)

    fun onClickLiked(eventId: String, isLiked: Boolean, pos: Int)

    //void onClickMore(String eventId , int pos);

    fun onClickViewProfile()

    fun onClickViewEvent(eventId: String, pos: Int)

    fun onClickClose()

}
