package com.hooleyapp.hooley.interfaces

import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel

/**
 * Created by Zeeshan on 22-Jan-18.
 */

interface IEventAllFeedsClick {

    fun onClickPostComment(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, type: Int)

    fun onClickMore(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, position: Int, listPosition: Int, type: Int)

    fun onClickImageView(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo)

    fun onClickLikePost(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, position: Int, type: Int)

    fun onClickFav(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, position: Int, type: Int)

    fun onClickShare(imageId: String, position: Int, listPosition: Int, type: Int)
}
