package com.hooleyapp.hooley.interfaces

/**
 * Created by Nauman on 3/8/2018.
 */

interface GenericCallback<T> {
    fun success(result: T)
    fun failure(message: String)
}


