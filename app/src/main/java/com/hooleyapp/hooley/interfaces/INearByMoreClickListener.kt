package com.hooleyapp.hooley.interfaces

/**
 * Created by Zeeshan on 26-Feb-18.
 */

interface INearByMoreClickListener {

    fun onClickMoreLike(position: Int, type: Int, listPosition: Int)

}
