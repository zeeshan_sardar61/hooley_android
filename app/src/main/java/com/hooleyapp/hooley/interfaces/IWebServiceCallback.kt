package com.hooleyapp.hooley.interfaces

interface IWebServiceCallback<T> {
    fun success(result: T)

    fun failure(message: String)

    fun onTokenExpire()

}