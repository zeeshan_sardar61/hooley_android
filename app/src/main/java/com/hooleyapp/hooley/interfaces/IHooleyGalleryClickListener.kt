package com.hooleyapp.hooley.interfaces

import java.util.*

/**
 * Created by Nauman on 12/26/2017.
 */

interface IHooleyGalleryClickListener {
    fun onClickImage(imageUrl: String, isSelect: Boolean, imgPositions: ArrayList<Int>)
}
