package com.hooleyapp.hooley.interfaces

/**
 * Created by Nauman on 3/8/2018.
 */

interface FriendClickListener {

    fun onClickViewProfile(friendId: Int, friendName: String)

    fun addFriend(friendId: Int)

    fun AcceptRequest(position: Int, friendId: Int, value: Boolean)
}

interface FriendCardClickListener {

    fun onClickViewProfile(position: Int)

    fun onClickAddFriend(position: Int)

    fun onClickAcceptRequest(position: Int, value: Boolean)

    fun onClickMessage(position: Int)

    fun onClickClose()

    fun onClickMyProfile()


}
