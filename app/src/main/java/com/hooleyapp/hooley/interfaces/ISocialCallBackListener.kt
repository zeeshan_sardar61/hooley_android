package com.hooleyapp.hooley.interfaces

/**
 * Created by Nauman on 2/22/2018.
 */

interface ISocialCallBackListener {

    fun onCallBackFaceBook(isLogin: Boolean)

    fun onCallBackTwitter(isLogin: Boolean)

    fun onCallBackLinkedIn(isLogin: Boolean)

    fun onCallBackGooglePlus(isLogin: Boolean)

}
