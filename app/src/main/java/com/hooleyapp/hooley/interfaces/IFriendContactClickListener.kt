package com.hooleyapp.hooley.interfaces

interface IFriendContactClickListener {

    fun onClickAddFriend(position: Int)

    fun onClickAcceptFriend(position: Int)

    fun onClickItem(position: Int)
}