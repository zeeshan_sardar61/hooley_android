package com.hooleyapp.hooley.interfaces

/**
 * Created by Nauman on 9/11/2017.
 */

interface IMenuItem {
    fun onMenuItemClick(position: Int)
}
