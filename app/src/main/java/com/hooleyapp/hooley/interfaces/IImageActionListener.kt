package com.hooleyapp.hooley.interfaces

/**
 * Created by hp on 7/31/2017.
 */

interface IImageActionListener {

    fun deleteImage(ImageUrl: String)

    fun updateProfilePic(ImageUrl: String)
}

