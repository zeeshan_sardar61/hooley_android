package com.hooleyapp.hooley.contacts;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v4.content.LocalBroadcastManager;

import com.hooleyapp.hooley.HooleyApp;
import com.hooleyapp.hooley.db.AppDatabase;
import com.hooleyapp.hooley.db.DbManager;
import com.hooleyapp.hooley.interfaces.IWebServiceCallback;
import com.hooleyapp.hooley.log.Logger;
import com.hooleyapp.hooley.model.GeneralModel;
import com.hooleyapp.hooley.model.PhoneContactsModel;
import com.hooleyapp.hooley.others.Constants;
import com.hooleyapp.hooley.others.Util;
import com.hooleyapp.hooley.services.ContactWebService;
import com.hooleyapp.hooley.tables.ContactNames;
import com.hooleyapp.hooley.tables.ContactNumbers;
import com.raizlabs.android.dbflow.config.DatabaseDefinition;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ProcessModelTransaction;
import com.raizlabs.android.dbflow.structure.database.transaction.Transaction;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Nauman on 12/28/2017.
 */

public class ContactApi {
    public static int TRANSACTION_THRESHOLD = 100;
    public static long COPY_THRESHOLD = 60000;// 1 minute is enough to copy
    public static LocalBroadcastManager broadcaster;
    private static Context mContext;
    private static List<PhoneContactsModel> mlist;
    private static ContactWebService mService;

    public static void init(Activity activity, Context context) {

        ContactApi.mContext = context;
        // INFO: starting contact sync module
        broadcaster = LocalBroadcastManager.getInstance(context);

        //Add Account for contact syncing sync adapter
        addSyncAccount(activity);

        //register contact Observer
        try {
            registerForContactsChange(context);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        //start contactSync Module
        ContactSyncModule();

    }

    /**
     * Add an Account for contact syncing sync adapter..
     */
    public static void addSyncAccount(Activity activity) {
//        final AccountManager accountManager = AccountManager.get(mContext);
//        final String vopiumAccountType = mContext.getResources().getString(R.string.vopium_account_type);
//        Account[] accounts = accountManager.getAccountsByType(vopiumAccountType);
//        if (accounts.length < 1) {
//            accountManager.addAccount(vopiumAccountType, null, null, null, activity, null, null);
//        } else {
//            // already added
//        }
    }

    private static void registerForContactsChange(Context context) {
        if (HooleyApp.db.getBoolean(Constants.IS_LOGIN))
            context.startService(new Intent(context, ContactsObserverService.class));
    }

    /**
     * starts the contact syncing module
     */
    public static void ContactSyncModule() {

        // TODO: Comment out following initializing code

        // DBWrapper dbWrapper = DBWrapper.getInstance();
        // dbWrapper.clearAllDB();
        // PhoenixPreference.clearAllPrefs();

        boolean contacts_copied = HooleyApp.db.getBoolean(Constants.CONTACTS_COPIED, false);

        if (contacts_copied) {
            // its great that contacts have been copied.
            // lets c if contacts are synced also
            boolean contacts_synced = HooleyApp.db.getBoolean(Constants.CONTACTS_SYNCED, false);
            if (contacts_synced) {
                // contacts are also synced, lean back and enjoy :)
            } else {
                // contacts are not synced. check if contact sync is in progress
                boolean contacts_syncing = HooleyApp.db.getBoolean(Constants.CONTACTS_SYNCING, false);
                if (contacts_syncing) {
                    // contacts are getting synced. its ok !
                } else {
                    // use onContactsCopysuccessfully function

//                    onContactsCopiedSuccessfully();
                }
            }
        } else {

            // check if copying already started
            boolean isStarted = HooleyApp.db.getBoolean(Constants.CONTACTS_COPY_STARTED, false);

            if (isStarted) {
                // Just to be sure, check time.
                long start_time = HooleyApp.db.getLong(Constants.CONTACTS_COPY_START_TIME, 0);
                long diff = System.currentTimeMillis() - start_time;
                if (diff > COPY_THRESHOLD) {
                    // Looking suspicious
                    // lets start again
                    copyContactsAsync();
                }

                // don't do anything
            } else {

                // start copying the contacts
                copyContactsAsync();
            }
        }

    }


    /**
     * Start copying the native contacts to local db
     */
    private static void copyContactsAsync() {

        HooleyApp.db.putBoolean(Constants.CONTACTS_COPY_STARTED, true);
        HooleyApp.db.putLong(Constants.CONTACTS_COPY_START_TIME, System.currentTimeMillis());
        Thread t = new Thread() {

            public void run() {
//                android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                Logger.INSTANCE.i("Contacts Copy Started");
                long startTime = System.currentTimeMillis();


                int total_names_copied = 0;
                int total_num_copied = 0;

                int last_contact_id_inserted = Integer.MIN_VALUE;

                /**
                 * INFO: query in Data table................................
                 * with projection : Display_Name, Contact_ID
                 * Data1(number/email/etc),Data2(phone type etc)
                 * Mimetype(phone/email/etc)....... with selection :
                 * Has_Phone_Number != 0 AND Mimetype = Phone
                 */
                String[] projection = new String[]{ContactsContract.Data._ID, ContactsContract.Data.DISPLAY_NAME, ContactsContract.Data.CONTACT_ID, ContactsContract.Data.RAW_CONTACT_ID, ContactsContract.Data.DATA1,
                        ContactsContract.Data.DATA2, ContactsContract.Data.MIMETYPE};
                String selection = ContactsContract.Data.MIMETYPE + " =?";
                String[] selectionArgs = new String[]{ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE};

                Cursor cursor = null;
                try {
                    cursor = ContactApi.mContext.getContentResolver().query(
                            ContactsContract.Data.CONTENT_URI, projection, selection, selectionArgs, ContactsContract.Data.CONTACT_ID);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (cursor != null && cursor.moveToFirst()) {

                    try {

                        int number_count = 0;

                        ContactsManager.hmNumbers = new HashMap<>();

                        ContactNames contactNames = null;
                        ArrayList<ContactNames> contactNamesList = new ArrayList<>();
                        ArrayList<ContactNumbers> contactNumbersList = new ArrayList<>();
                        ContactNumbers contactNumbers;

                        do {

                            // Get number
                            String data = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DATA1));
                            // Remove non numbers
                            data = Util.excludeDashesFromNumber(data);
                            // Get contact id
                            int contact_id = cursor.getInt(cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID));
                            // Get raw_contact_id
                            int raw_contact_id = cursor.getInt(cursor.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID));
                            // Get row id
                            int contact_data_id = cursor.getInt(cursor.getColumnIndex(ContactsContract.Data._ID));
                            // Get phone type
                            int phone_type = cursor.getInt(cursor.getColumnIndex(ContactsContract.Data.DATA2));

                            // TODO: I think, we should use raw_contact_id

                            // check if last id is same to current
                            if (last_contact_id_inserted == contact_id) {
                                // This has already been put in AppContacts
                                // so ignore
                            } else {
                                last_contact_id_inserted = contact_id;
                                // this is a new entry, put in db
                                // in table AppContacts

                                // Get DisplayName
                                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));


                                //Create an item for batch insertion in DbFlow
                                contactNames = new ContactNames();
                                contactNames.cnContactID = (contact_id);
                                contactNames.cnName = (name);
                                contactNames.cnIsAppUser = (0);
                                contactNamesList.add(contactNames);

                                total_names_copied++;

                            }

                            // put number in hashmap to be used later
                            // for mainly posting to server
                            ContactsManager.hmNumbers.put(number_count, data);
                            number_count++;

                            // put in contacts data table
                            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
                            contactNumbers = new ContactNumbers();
                            contactNumbers.contactID = (contact_id);
                            contactNumbers.contactNumber = (data);
//                            contactNumbers.setContactName(name);
                            contactNumbers.contactE164Number = ((Util.convertToE164(data)));
                            contactNumbers.contactNumberType = (phone_type);
                            contactNumbers.contactDataID = (contact_data_id);

                            // create content value
                            contactNumbersList.add(contactNumbers);
                            // Put in db
                            total_num_copied++;

                            // if batch grows to 100, lets commit in db now
                            if (total_names_copied >= TRANSACTION_THRESHOLD || total_num_copied >= TRANSACTION_THRESHOLD) {

                                Logger.INSTANCE.i("commiting the batch");
                                total_names_copied = 0;
                                total_num_copied = 0;
                            }

                        } while (cursor.moveToNext());

                        if (cursor != null && !cursor.isClosed())
                            cursor.close();

                        ProcessModelTransaction<ContactNames> processModelTransaction =
                                new ProcessModelTransaction.Builder<>(new ProcessModelTransaction.ProcessModel<ContactNames>() {
                                    @Override
                                    public void processModel(ContactNames contactNames, DatabaseWrapper wrapper) {
                                        contactNames.save();
                                    }
                                }).addAll(contactNamesList).build();

                        ProcessModelTransaction<ContactNumbers> processModelContactNumbersTransaction =
                                new ProcessModelTransaction.Builder<>(new ProcessModelTransaction.ProcessModel<ContactNumbers>() {
                                    @Override
                                    public void processModel(ContactNumbers contactNumbers, DatabaseWrapper wrapper) {
                                        contactNumbers.save();

                                    }
                                }).addAll(contactNumbersList).build();
                        DatabaseDefinition database = FlowManager.getDatabase(AppDatabase.class);
                        database.reset();
                        Transaction transaction = database.beginTransactionAsync(processModelTransaction).build();
                        transaction.execute();
                        Transaction transactionContactNumbers = database.beginTransactionAsync(processModelContactNumbersTransaction).build();
                        transactionContactNumbers.execute();
                        DbManager.getInstance().exportDB();
                        //Put name and numbers in db using batch insertion
//                        DbManager.getInstance().saveNames(contactNamesList);
//                        DbManager.getInstance().saveNumbers(contactNumbersList);
//                        .getInstance().saveOnSaveQueue(contactNumbersList);

                        Logger.INSTANCE.i("Numbers inserted");

                        // Contacts copied successfully
//                        DbManager.getInstance().dumpDb();
                        HooleyApp.db.putBoolean(Constants.CONTACTS_COPIED, true);
                        Logger.INSTANCE.i("Contacts Copied successfully");

//                        onContactsCopiedSuccessfully();

                        // last batch didn't committed in above code, lets
                        // commit that now

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        HooleyApp.db.putBoolean(Constants.CONTACTS_COPY_STARTED, false);
                    }


                } else {
                    // cursor either empty or couldn't query. what to do?
                    // lets make sure, user opens app again and it all started
                    // again

                    Logger.INSTANCE.i("cursor either empty or couldn't query.");
                    HooleyApp.db.putBoolean(Constants.CONTACTS_COPY_STARTED, false);

                }
                long total = System.currentTimeMillis() - startTime;
                Logger.INSTANCE.i("Contacts Copy Ended");
                Logger.INSTANCE.i("Total Time::" + total);
                HooleyApp.db.putBoolean(Constants.SYNC_USER_CONTACTS, false);
                sendBroadcast();
//                sendContactToServer();

            }
        };
        t.start();
    }

    public static void sendBroadcast() {
        Intent intent = new Intent("Contacts");
        if (broadcaster != null)
            broadcaster.sendBroadcast(intent);
        Logger.INSTANCE.i("sending local sync completion broadcast");
    }


    public static void sendContactToServer() {
        if (mlist == null)
            mlist = DbManager.getContactList();
        if (mService == null)
            mService = new ContactWebService();
        mService.sendContactToServer(mlist, new IWebServiceCallback<GeneralModel>() {
            @Override
            public void success(GeneralModel result) {

            }

            @Override
            public void failure(@NotNull String message) {

            }

            @Override
            public void onTokenExpire() {

            }
        });
    }

}
