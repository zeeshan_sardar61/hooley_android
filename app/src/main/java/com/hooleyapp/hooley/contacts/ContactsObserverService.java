package com.hooleyapp.hooley.contacts;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.IBinder;
import android.provider.ContactsContract;

import com.hooleyapp.hooley.log.Logger;

/**
 * Created by Nauman on 12/28/2017.
 */

public class ContactsObserverService extends Service {

    ContactsObserver observer = null;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // If contacts are synced, register observer

        Logger.INSTANCE.i("ContactsObserverService::onStartCommand");

        // commented because we want to listen for changes even if sync has not been done yet
        //	if (VPPreference.getBoolean(VPPreferencesKeys.CONTACTS_SYNCED, false)) {


        observer = ContactsObserver.getInstance(getApplicationContext());

        // Just to make sure, our observer is registered only one time

        try {
            getContentResolver().unregisterContentObserver(observer);
        } catch (Exception e) {

        }

        Logger.INSTANCE.i("registering observer for contacts change");
        // register our observer
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, false, observer);
        }
        // restarts, if we get killed somehow
        return START_STICKY;
    }

}
