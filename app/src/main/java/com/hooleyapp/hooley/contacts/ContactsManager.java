package com.hooleyapp.hooley.contacts;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import com.hooleyapp.hooley.HooleyApp;
import com.hooleyapp.hooley.others.Constants;
import com.hooleyapp.hooley.tables.ContactNames;
import com.hooleyapp.hooley.tables.ContactNames_Table;
import com.hooleyapp.hooley.tables.ContactNumbers;
import com.hooleyapp.hooley.tables.ContactNumbers_Table;
import com.raizlabs.android.dbflow.sql.language.From;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.language.Where;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Nauman on 12/28/2017.
 */

public class ContactsManager {

    public static HashMap<Integer, String> hmNumbers = null;

    /**
     * This function will look for both local and native db, Updates the local
     * db (data) from native
     */
    public static boolean updateLocalDBNumbersFromNative(Context context) {
        boolean blContentChanged = false;

        boolean checkForDanglingNames = false;

        Where<ContactNumbers> joinQuery = SQLite.select(ContactNumbers_Table.id.as("id").withTable(), ContactNumbers_Table.contactDataID, ContactNumbers_Table.contactNumber
                , ContactNumbers_Table.contactState)
                .from(ContactNumbers.class)
                .where(ContactNumbers_Table.contactState.withTable().notEq(0));

        List<ContactNumbers> contactNumbersList = joinQuery.queryList();
        ContactNumbers contactNumbers;

        // Prepare hashmap for _id and number from local data table

        HashMap<Long, String> hmNumbers = new HashMap<Long, String>();
        ArrayList<ContactNumbers> lsAddedNumbers = new ArrayList<ContactNumbers>();
        ArrayList<ContactNumbers> lsDeletedNumbers = new ArrayList<ContactNumbers>();


        if (contactNumbersList != null && contactNumbersList.size() > 0) {

            for (int i = 0; i < contactNumbersList.size(); i++) {

                contactNumbers = contactNumbersList.get(i);

                String number = contactNumbers.contactNumber;
//                localPhoneRecord record = new localPhoneRecord();
//                record.contact_data_id = contactNumbers.contactDataID;
//                record.contact_id = contactNumbers.contactID;
//                record.data = contactNumbers.contactNumber;
//                record.state = contactNumbers.contactState;

                hmNumbers.put(contactNumbers.contactDataID, number);

            }
        }

        // ok, our hashmap is ready now

//        Logger.i("total numbers in hashmap:" + hmNumbers.size());

        // get numbers from native db now

        String[] projection = new String[]{ContactsContract.Data._ID, ContactsContract.Data.DATA1, ContactsContract.Data.DATA2,
                ContactsContract.Data.CONTACT_ID};
        String selection = ContactsContract.Contacts.Data.MIMETYPE + "=?";
        String[] args = new String[]{ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE};
//
//        Cursor cursor = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, projection, selection, args, null);
//        if (cursor != null && cursor.moveToFirst()) {
//
//            String added_numbers = "";
//
//            do {
//
//                long id = cursor.getLong(cursor.getColumnIndex("_id"));
//                String native_number = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DATA1));
//                native_number = Util.excludeDashesFromNumber(native_number);
//
//                if (hmNumbers.containsKey(id)) {
//                    // this native contact is present in local db, check for
//                    // edit scene
//
//                    String local_number = hmNumbers.get(id);
//
//                    if (native_number.equals(local_number)) {
//                        // numbers are same, no edit is required
//                    } else {
//                        // we got an edit, delete the old, add the new in
//                        // local data table
//
////                        Logger.i("New number found in edit scenario:" + native_number);
////                        Logger.i("Number to be deleted in edit scenario:" + local_number);
//
//                        long data_id = cursor.getLong(cursor.getColumnIndex(ContactsContract.Data._ID));
//
////                        Logger.i("data_id=" + data_id);
////                        Logger.i("hashmap id=" + id);
//
//                        long contact_id = cursor.getLong(cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID));
//                        int phone_type = cursor.getInt(cursor.getColumnIndex(ContactsContract.Data.DATA2));
//
//                        ContactNumbers contactNumbersAdded = new ContactNumbers();
//                        ContactNames contactNames = SQLite.select().from(ContactNames.class)
//                                .where(ContactNames_Table.cnContactID.eq(contact_id)).querySingle();
//
//                        contactNumbersAdded.contactID = contact_id;
//                        contactNumbersAdded.contactDataID = data_id;
//                        contactNumbersAdded.contactNumber = native_number;
//                        contactNumbersAdded.contactE164Number = Util.convertToE164(native_number);
//                        if (phone_type >= 0)
//                            contactNumbersAdded.contactNumberType = phone_type;
//                        contactNumbersAdded.contactState = ContactState.STATE_ADDED.ordinal();
//                        contactNumbersAdded.contactIsAppUser = VPUser.VOPIUM_NON_REG.ordinal();
//                        contactNumbersAdded.isNewAppUser = VPNewUser.VOPIUM_OLD.ordinal();
//                        if (contactNames != null)
//                            contactNumbersAdded.associateContactNames(contactNames);
//
//                        added_numbers = added_numbers + native_number + " | ";
//                        lsAddedNumbers.add(contactNumbersAdded);
//
//
//                        ContactNumbers contactNumbersDeleted = new ContactNumbers();
//                        contactNumbersDeleted.contactDataID = data_id;
//                        contactNumbersDeleted.contactNumber = local_number;
//                        contactNumbersDeleted.contactState = ContactState.STATE_DELETED.ordinal();
//                        lsDeletedNumbers.add(contactNumbersDeleted);
//
//                    }
//
//                    // clear this id from hashmap, this way we will get all
//                    // numbers deleted from native db.
//                    hmNumbers.remove(id);
//
//                } else {
//                    // new number found
//                    // may be its just a new number of old contact or
//                    // completely new contact
//                    // put in local data table
//
//                    Logger.i("New number found to be added in db and server:" + native_number);
//                    native_number = ContactsUtil.excludeDashesFromNumber(native_number);
//                    if (!TextUtils.isEmpty(native_number) && native_number.length() > 5) {
//
//                        long contact_id = cursor.getLong(cursor.getColumnIndex(Data.CONTACT_ID));
//                        ContactNumbers contactNumbersAdded = new ContactNumbers();
//                        contactNumbersAdded.contactID = contact_id;
//                        contactNumbersAdded.contactDataID = id;
//                        contactNumbersAdded.contactNumber = native_number;
//                        contactNumbersAdded.contactE164Number = IMUtils.convertToE164(native_number);
//                        contactNumbersAdded.contactState = ContactState.STATE_ADDED.ordinal();
//                        contactNumbersAdded.contactIsAppUser = VPUser.VOPIUM_NON_REG.ordinal();
//                        contactNumbersAdded.isNewAppUser = VPNewUser.VOPIUM_OLD.ordinal();
//                        ContactNames contactNames = SQLite.select().from(ContactNames.class)
//                                .where(ContactNames_Table.cnContactID.eq(contact_id)).querySingle();
//
//                        if (contactNames != null)
//                            contactNumbersAdded.associateContactNames(contactNames);
//                        added_numbers = added_numbers + native_number + " | ";
//                        lsAddedNumbers.add(contactNumbersAdded);
//                    }
//                }
//
//            } while (cursor.moveToNext());
//
//            Logger.i("Adding new numbers in local db ::" + added_numbers);
//
//            cursor.close();
//        }
//        // Adding all new numbers from native data table to local data
//        // table
//
//
////        int count = dbWrapper.getRowsCount(DBUtility.TABLE_NAME_CONTACT_DATA);
//
//        long count = ContactsApi.getContactNumbersCount();
//        Logger.i("Count before inserting new:" + count);
//
//        // check for state 'deleted' of newly added numbers found. Just
//        // delete them
//
//        Logger.i("checking for state 'deleted' of newly added numbers.");
//        for (int i = lsAddedNumbers.size() - 1; i >= 0; i--) {
//            ContactNumbers val = lsAddedNumbers.get(i);
//
//            long contact_data_row_id = ContactsApi.isContactInDeletedState(val.contactNumber);
//
//            if (contact_data_row_id > 0) {
//
//                Logger.i("Got a contact which is added and found deleted in local db" + val.contactNumber);
//
//
////                int deleted = dbWrapper.deleteRows(DBUtility.TABLE_NAME_CONTACT_DATA, AppContactData.PhoenixContactData.FIELD_ID + "=?",
////                        new String[]{contact_data_row_id + ""});
////                Where<ContactNumbers> query = SQLite.delete().from(ContactNumbers.class).where(ContactNumbers_Table.id.eq(contact_data_row_id));
////                query.query();
//
//                Where<ContactNumbers> query = SQLite.update(ContactNumbers.class)
//                        .set(ContactNumbers_Table.contactState.is(ContactState.STATE_ADDED.ordinal())
//                                , ContactNumbers_Table.contactIsAppUser.is(VPUser.VOPIUM_REG.ordinal()))
//                        .where(ContactNumbers_Table.id.eq(contact_data_row_id));
//                query.query();
//                lsAddedNumbers.remove(i);
//
////                if (deleted > 0)
////                checkForDanglingNames = true;
//
////                Logger.i("Deleting contact:" + deleted);
//
//            } else {
//
//                boolean appUser = ContactsApi.isContactAppUser(val.contactNumber);
//
//                if (appUser) {
//
//                    Logger.i("Already present in db and marked as app user, so no need to sync, just add it as app user");
//                    val.contactState = ContactState.STATE_SYNCED.ordinal();
//                    val.contactIsAppUser = VPUser.VOPIUM_REG.ordinal();
//                    lsAddedNumbers.set(i, val);
//                }
//            }
//
//        }
//
//        if (lsAddedNumbers.size() > 0) {
//
//            blContentChanged = true;
//
//            ContactsApi.insertInDB(lsAddedNumbers);
//
//
//            count = ContactsApi.getContactNamesCount();
//            Logger.i("Count after inserting new:" + count);
//        } else {
//            Logger.i("No Number to add in local db");
//        }

        // Put remaing numbers from hashmap to be marked deleted as they
        // were not found in native db
//        if (hmNumbers.size() > 0) {
//            Iterator<Long> iterator = hmNumbers.keySet().iterator();
//
//            while (iterator.hasNext()) {
//
//                long id = iterator.next();
//
//                String local_number = hmNumbers.get(id);
//
//                ContactNumbers val = new ContactNumbers();
//                val.contactDataID = id;
//                val.contactNumber = local_number;
//                val.contactState = ContactState.STATE_DELETED.ordinal();
//
//                lsDeletedNumbers.add(val);
//            }
//        }

        // check for state 'added' of newly deleted numbers found and remove
        // them
        // simply from local db, as they were added before and couldn't be sent
        // to server, now they got deleted so no need to tell server

//        Logger.i("checking for state 'added' of need to be deleted numbers.");
//        for (int i = lsDeletedNumbers.size() - 1; i >= 0; i--) {
//            ContactNumbers val = lsDeletedNumbers.get(i);
//
//            boolean inAddedState = ContactsApi.isContactInAddedState(val.contactNumber);
//
//            if (inAddedState) {
//
//                Logger.i("Got a contact which is deleted and found added in local db");
//
//                // No need to mark it deleted
//                lsDeletedNumbers.remove(i);
//                // delete from local db also
//
////                int deleted = dbWrapper.deleteRows(DBUtility.TABLE_NAME_CONTACT_DATA, AppContactData.PhoenixContactData.FIELD_DATA_ID
////                        + "=?", new String[]{val.getAsString(AppContactData.PhoenixContactData.FIELD_DATA_ID)});
//                Where<ContactNumbers> query = SQLite.delete()
//                        .from(ContactNumbers.class)
//                        .where(ContactNumbers_Table.contactNumber.eq(val.contactNumber));
//
//                query.query();
//
////                if (deleted > 0)
//                checkForDanglingNames = true;
//
////                Logger.i("Deleting contact:" + deleted);
//            }
//
//        }

//        if (lsDeletedNumbers.size() > 0) {
//
//            blContentChanged = true;
//
//            Logger.i("Marking deleted numbers to be deleted in db");
//
//            // updating all numbers to be mark delete from local db
////            String where = AppContactData.PhoenixContactData.FIELD_DATA;
//            ContactsApi.updateInDB(lsDeletedNumbers);
//        } else
//            Logger.i("No Number to mark delete in local db");

        // If we have made changes in the form of deletion in data table
//        if (checkForDanglingNames)
//            deleteDanglingNamesNNumbers();

        return blContentChanged;
    }


    /**
     * This function will look for both local and native db, Updates the local
     * db (names) from native
     */
    public static boolean updateLocalDBNamesFromNative(Context context) {

        boolean blContentChanged = false;

        ArrayList<ContactNames> lsAddedNames = new ArrayList<ContactNames>();
        ArrayList<ContactNames> lsUpdatedNames = new ArrayList<ContactNames>();
        ArrayList<ContactNames> lsDeletedNames = new ArrayList<ContactNames>();


        String[] projection = null;
        String selection = null;
        String[] args = null;

        HashMap<Long, String> hmNames = new HashMap<Long, String>();


        From<ContactNames> query = SQLite.select().from(ContactNames.class);

        List<ContactNames> contactNamesList = query.queryList();

        if (contactNamesList != null && !contactNamesList.isEmpty()) {

            for (int i = 0; i < contactNamesList.size(); i++) {
                ContactNames contactNames = contactNamesList.get(i);
                long id = contactNames.cnContactID;
                String name = contactNames.cnName;
                hmNames.put(id, name);

            }

        }


        projection = new String[]{ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.Contacts._ID};
        selection = ContactsContract.Contacts.HAS_PHONE_NUMBER + "!=?";
        args = new String[]{"0"};

        Cursor cursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, projection, selection, args, null);
        if (cursor != null && cursor.moveToFirst()) {

            do {

                long native_id = cursor.getLong(cursor.getColumnIndex(ContactsContract.Contacts._ID));

                String native_name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                if (hmNames.containsKey(native_id)) {

                    String local_name = hmNames.get(native_id);

                    if (native_name.equals(local_name)) {

                    } else {

                        long contact_id = cursor.getLong(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                        ContactNames contactNames = new ContactNames();
                        contactNames.cnName = (native_name);
                        contactNames.cnContactID = (contact_id);
                        lsUpdatedNames.add(contactNames);
                    }
                    hmNames.remove(native_id);
                } else {
                    long contact_id = cursor.getLong(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    ContactNames contactNames = new ContactNames();
                    contactNames.cnContactID = (contact_id);
                    contactNames.cnName = (native_name);
                    contactNames.cnIsAppUser = (0);
                    lsAddedNames.add(contactNames);

                }

            } while (cursor.moveToNext());

            cursor.close();
        }

        if (lsAddedNames.size() > 0) {
            blContentChanged = true;
            HooleyApp.db.putBoolean(Constants.CONTACTS_COPIED, false);
            ContactApi.ContactSyncModule();
//            insertInNameDB(lsAddedNames);
        }


        if (lsUpdatedNames.size() > 0) {
            blContentChanged = true;
            // update names in local db
            HooleyApp.db.putBoolean(Constants.CONTACTS_COPIED, false);
            ContactApi.ContactSyncModule();
//            updateInNameDB(lsUpdatedNames);
        }

        // delete names from local db
        if (hmNames.size() > 0) {
            Iterator<Long> iterator = hmNames.keySet().iterator();
            while (iterator.hasNext()) {
                ContactNames val = new ContactNames();
                long id = iterator.next();
                val.cnContactID = (id);
                lsDeletedNames.add(val);
            }
        }
        if (lsDeletedNames.size() > 0) {
            blContentChanged = true;
            HooleyApp.db.putBoolean(Constants.CONTACTS_COPIED, false);
            ContactApi.ContactSyncModule();
//            deleteInDB(lsDeletedNames);
        }

        return blContentChanged;
    }


    public static void insertInNameDB(ArrayList<ContactNames> list) {

        int size = list.size();
        try {
            for (int i = 0; i < size; i++) {
                try {
                    ContactNames contactNames = list.get(i);
                    contactNames.save();
                } catch (Exception exInsert) {
                }
            }

        } catch (Exception e) {

        } finally {
        }

    }


    public static void updateInNameDB(ArrayList<ContactNames> list) {
        long updated = 0;

        long ret = 0;

        int size = list.size();

        try {
            for (int i = 0; i < size; i++) {
                ContactNames val = list.get(i);
                Where<ContactNames> query = SQLite.update(ContactNames.class)
                        .set(ContactNames_Table.cnName.is(val.cnName)).where(ContactNames_Table.cnContactID.eq(val.cnContactID));
                query.query();
                if (ret > 0)
                    updated = updated + ret;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }


    public static long deleteInDB(ArrayList<ContactNames> list) {
        long ret = 0;
        int size = list.size();
        try {
            for (int i = 0; i < size; i++) {
                ContactNames val = list.get(i);
                Where<ContactNames> query = SQLite.delete().from(ContactNames.class)
                        .where(ContactNames_Table.cnContactID.eq(val.cnContactID));
                query.query();
            }
        } catch (Exception e) {

        } finally {
        }

        return ret;
    }


}
