package com.hooleyapp.hooley.contacts;

import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;

import com.hooleyapp.hooley.HooleyApp;
import com.hooleyapp.hooley.log.Logger;
import com.hooleyapp.hooley.others.Constants;

/**
 * Created by Nauman on 12/28/2017.
 */

public class ContactsObserver extends ContentObserver {

    private static final long THRESHOLD_CHANGES = 5000;// 5 seconds
    private static ContactsObserver instance;
    private final LocalBroadcastManager broadcaster;
    Context context;
    long previousTime = 0;
    private boolean inUse = false;
    private Intent intent;
//    private String AUTHORITY = "com.vopium.messenger.providers.AppContactDataContentProvider";

    private ContactsObserver(Context context, Handler handler) {
        super(handler);
        this.context = context;
        this.broadcaster = LocalBroadcastManager.getInstance(context);

    }

    public static ContactsObserver getInstance(Context context) {
        if (instance == null) {
            synchronized (ContactsObserver.class) {
                if (instance == null) {
                    instance = new ContactsObserver(context, null);
                }
            }
        }
        return instance;
    }

    @Override
    public boolean deliverSelfNotifications() {
        return true;
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);

        Logger.INSTANCE.i("change in contacts");


        long current = System.currentTimeMillis();

        if (current - previousTime >= THRESHOLD_CHANGES && !inUse) {
            // this is a new request, we should entertain this

            inUse = true;

            // update previous time
            previousTime = current;

            try {
                // local sync contact names now
                Logger.INSTANCE.i("updating names");

                boolean isNamesChanged = ContactsManager.updateLocalDBNamesFromNative(context);

                // prepare the local db to be synced by sync adapter
                Logger.INSTANCE.i("updating numbers");
                boolean isNumberChanged = ContactsManager.updateLocalDBNumbersFromNative(context);

                if (HooleyApp.db.getBoolean(Constants.IS_LOGIN, false) && isNumberChanged) {
                    // trigger sync adapter
                    triggerSyncAdapter();
                }


                // INFO: For Testing
//                if (Logger.showLogs)
//                    DBWrapper.getInstance().exportDatabse(DBUtility.DATABASE_NAME);

                // everything is synced from native to local
                // fire a broadcast to be used by contact list if needed
                if (isNamesChanged || isNumberChanged)
                    sendBroadcast();

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                inUse = false;
            }

        } else {
            Logger.INSTANCE.i("Ignoring the change request");
            // this is probably coming multiple times, or we are already in
            // business
        }
    }

    private void triggerSyncAdapter() {
//        final AccountManager accountManager = AccountManager.get(context);
//        final String vopiumAccountType = context.getResources().getString(R.string.vopium_account_type);
//        Account[] accounts = accountManager.getAccountsByType(vopiumAccountType);
//        if (accounts.length > 0) {
//
//            Bundle settingsBundle = new Bundle();
//            settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
//            settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
//            settingsBundle.putBoolean(SyncAdapter.SYNC_DB_PREPARED, true);
//            settingsBundle.putLong(SyncAdapter.SYNC_INITIATE_TIME, System.currentTimeMillis());
//            ContentResolver.setSyncAutomatically(accounts[0], AUTHORITY, true);
//            ContentResolver.setIsSyncable(accounts[0], AUTHORITY, 1);
//            ContentResolver.requestSync(accounts[0], AUTHORITY, settingsBundle);


//        }
    }


    private void sendBroadcast() {
        intent = new Intent("Contacts");
        broadcaster.sendBroadcast(intent);
        Logger.INSTANCE.i("sending local sync completion broadcast");
    }
}
