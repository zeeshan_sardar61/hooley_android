package com.hooleyapp.hooley.fcm

import android.app.*
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.content.LocalBroadcastManager
import android.text.TextUtils
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.others.Constants


/**
 * Created by Adil on 7/3/2017.
 */

class MyFcmListenerService : FirebaseMessagingService() {
    internal var title: String? = null
    internal var message: String? = null
    internal var tag: String = ""
    lateinit var notificationManager: NotificationManager
    private var broadcaster: LocalBroadcastManager? = null
    private var isShow = false
    private var friendId = ""

    override fun onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this)

    }

    override fun onNewToken(token: String?) {
        super.onNewToken(token)
        HooleyApp.db.putString(Constants.GCM_REGISTRATION_TOKEN, token.toString())
    }

    /**
     * Called when message is received.
     */

    fun handleIntent(intent: Intent) {
        if (intent.extras != null) {
            val builder = RemoteMessage.Builder("MyFcmListenerService")
            for (key in intent.extras!!.keySet()) {
                val value = intent.extras!!.get(key)
                builder.addData(key, intent.extras!!.get(key)!!.toString())
                if (key.equals("gcm.notification.body", ignoreCase = true) && value != null) {
                }
            }
            handleNotification(builder.build())
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        handleNotification(remoteMessage)
    }


    private fun handleNotification(mMessage: RemoteMessage) {
        Logger.ex("Json == >> " + mMessage.data.toString())
        var intent: Intent? = null
        intent = Intent(this, HooleyMain::class.java)
        // Check if message contains a notification payload.
        if (mMessage.notification != null) {
            Logger.ex("Notification Body: " + mMessage.notification!!.body!!)
            intent.putExtra("Title", mMessage.notification!!.title)
            intent.putExtra("Body", mMessage.notification!!.body)
            intent.putExtra("Tag", mMessage.notification!!.tag)
            // isslien push flag true
            title = mMessage.notification!!.title
            message = mMessage.notification!!.body
            tag = mMessage.notification!!.tag!!
            if (!TextUtils.isEmpty(tag)) {
                tag = ""
            }
            HooleyApp.db.putString(Constants.TYPE_NOTIFICATION, tag)
            val mIntent = Intent("Notification")
            if (mMessage.data["badge"] != null)
                HooleyApp.db.putInt(Constants.UNREAD_NOTIFICATION_COUNT, mMessage.data["badge"]!!.toInt())
            broadcaster!!.sendBroadcast(mIntent)
            isShow = true
        }
        if (mMessage.data.isNotEmpty()) {
            Log.e(TAG, "Data Payload: " + mMessage.data.toString())
            try {
                handleSilentPush(mMessage)
            } catch (e: Exception) {
                Log.e(TAG, "Exception: " + e.message)
            }

        } else {
            Log.e(TAG, " NO Data Payload: ")
        }
    }

    private fun handleSilentPush(mMessage: RemoteMessage) {
        var mIntent = Intent(this, HooleyMain::class.java)
        when (mMessage.data["tag"]) {
            Constants.TYPE_MESSAGES_ALERT -> {
                counter++
                if (mMessage.data["friendId"] != null) {
                    if (mMessage.data["friendId"].equals(HooleyApp.db.getString(Constants.RECEIVER_ID), ignoreCase = true)) {
                        mIntent = Intent("Message")
                    }
                    mIntent.putExtra("tag", mMessage.data["tag"])
                    friendId = mMessage.data["friendId"]!!
                    mIntent.putExtra("friendId", mMessage.data["friendId"])
                    mIntent.putExtra("friendName", mMessage.data["body"])
                    mIntent.putExtra("threadId", mMessage.data["threadId"])
                    mIntent.putExtra("badge", mMessage.data["badge"])
                    isShow = true
                } else {
                    isShow = false
                    mIntent = Intent("ChatStreamMessage")
                }
                mIntent.putExtra("userId", mMessage.data["userId"])
                mIntent.putExtra("body", mMessage.data["body"])
                mIntent.putExtra("tag", mMessage.data["tag"])
                mIntent.putExtra("badge", mMessage.data["badge"])
                HooleyApp.db.putString(Constants.TYPE_NOTIFICATION, mMessage.data["tag"]!!)
                mIntent.putExtra("sentTime", mMessage.data["sentTime"])
                if (mMessage.data["mediaUrl"] != null)
                    mIntent.putExtra("mediaUrl", mMessage.data["mediaUrl"])
                if (mMessage.data["thumbnailUrl"] != null)
                    mIntent.putExtra("thumbnailUrl", mMessage.data["thumbnailUrl"])
                if (mMessage.data["isVideo"] != null) {
                    if (mMessage.data["isImage"].equals("true", ignoreCase = true))
                        mIntent.putExtra("isImage", true)
                    else
                        mIntent.putExtra("isImage", false)
                }
                if (mMessage.data["isVideo"] != null) {
                    if (mMessage.data["isVideo"].equals("true", ignoreCase = true))
                        mIntent.putExtra("isVideo", true)
                    else
                        mIntent.putExtra("isVideo", false)
                }
                broadcaster!!.sendBroadcast(mIntent)
            }
            Constants.TYPE_EVENT_GALLERY_ALERT -> {
                counter++
                mIntent = Intent("ChatStreamMessage")
                mIntent.putExtra("tag", mMessage.data["tag"])
                mIntent.putExtra("badge", mMessage.data["badge"])
                broadcaster!!.sendBroadcast(mIntent)
            }
            Constants.TYPE_WHOS_HER_ALERT -> {
                counter++
                mIntent = Intent("ChatStreamMessage")
                mIntent.putExtra("tag", mMessage.data["tag"])
                mIntent.putExtra("badge", mMessage.data["badge"])
                broadcaster!!.sendBroadcast(mIntent)
            }
            Constants.TYPE_MEDIA_ALERT -> {
                counter++
                //                mIntent = new Intent("ChatStreamMessage");
                mIntent.putExtra("tag", mMessage.data["tag"])
                mIntent.putExtra("eventId", mMessage.data["eventId"])
                mIntent.putExtra("mediaId", mMessage.data["mediaId"])
                mIntent.putExtra("badge", mMessage.data["badge"])
                broadcaster!!.sendBroadcast(mIntent)
            }
            Constants.TYPE_FRIENDS_ALERT -> {
                counter++
                //                mIntent = new Intent("ChatStreamMessage");
                mIntent.putExtra("tag", mMessage.data["tag"])
                mIntent.putExtra("userId", mMessage.data["userId"])
                mIntent.putExtra("badge", mMessage.data["badge"])
                broadcaster!!.sendBroadcast(mIntent)
            }
            Constants.TYPE_EVENT_ALERTS -> {
                counter++
                //                mIntent = new Intent("ChatStreamMessage");
                mIntent.putExtra("tag", mMessage.data["tag"])
                mIntent.putExtra("eventId", mMessage.data["eventId"])
                mIntent.putExtra("badge", mMessage.data["badge"])
                broadcaster!!.sendBroadcast(mIntent)
            }
            Constants.TYPE_TICKET_ALERT -> {
                counter++
                mIntent.putExtra("tag", mMessage.data["tag"])
                mIntent.putExtra("eventId", mMessage.data["eventId"])
                mIntent.putExtra("badge", mMessage.data["badge"])
                broadcaster!!.sendBroadcast(mIntent)
            }
            Constants.TYPE_TICKETSALE_ALERT -> {
                counter++
                mIntent.putExtra("tag", mMessage.data["tag"])
                mIntent.putExtra("eventId", mMessage.data["eventId"])
                mIntent.putExtra("badge", mMessage.data["badge"])
                broadcaster!!.sendBroadcast(mIntent)
            }
            Constants.TYPE_UPDATE_ANNOUNCEMENTS_ALERT -> {
                counter++
                //                mIntent = new Intent("ChatStreamMessage");
                mIntent.putExtra("tag", mMessage.data["tag"])
                mIntent.putExtra("eventId", mMessage.data["eventId"])
                mIntent.putExtra("badge", mMessage.data["badge"])
                broadcaster!!.sendBroadcast(mIntent)
            }
            Constants.TYPE_ANNOUNCEMENTS_ALERT -> {
                counter++
                //                mIntent = new Intent("ChatStreamMessage");
                mIntent.putExtra("tag", mMessage.data["tag"])
                mIntent.putExtra("eventId", mMessage.data["eventId"])
                mIntent.putExtra("eventName", mMessage.data["eventName"])
                mIntent.putExtra("address", mMessage.data["address"])
                mIntent.putExtra("imageUrl", mMessage.data["imageUrl"])
                mIntent.putExtra("detailMessage", mMessage.data["detailMessage"])
                mIntent.putExtra("startTime", mMessage.data["startTime"])
                mIntent.putExtra("endTime", mMessage.data["endTime"])
                mIntent.putExtra("badge", mMessage.data["badge"])
                broadcaster!!.sendBroadcast(mIntent)
            }
        }

        val CHANNEL_ID = "Hooley_01"
        val name = getString(R.string.channel_name)
        val importance: Int = NotificationManager.IMPORTANCE_HIGH

        val pendingIntent = PendingIntent.getActivity(this, counter, mIntent,
                PendingIntent.FLAG_UPDATE_CURRENT)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notification = NotificationCompat.Builder(this)
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notification.setSmallIcon(R.drawable.icon_app)
//            notification.color = resources.getColor(R.color.white)
        } else {
            notification.setSmallIcon(R.drawable.icon_app)
        }
        notification.setContentTitle(title)
        notification.setContentText(message)
        notification.setAutoCancel(true)
        notification.priority = Notification.PRIORITY_MAX
        notification.setSound(defaultSoundUri)
        notification.setContentIntent(pendingIntent)
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notification.setChannelId(CHANNEL_ID)
            var mChannel = NotificationChannel(CHANNEL_ID, name, importance)
            notificationManager.createNotificationChannel(mChannel)
        }
        if (isShow) {
            if (!TextUtils.isEmpty(friendId)) {
                if (!friendId.equals(HooleyApp.db.getString(Constants.RECEIVER_ID), ignoreCase = true))
                    notificationManager.notify(counter, notification.build())
            } else
                notificationManager.notify(counter, notification.build())
        }
    }

    fun isAppIsInBackground(context: Context): Boolean {
        Log.d(TAG, "isAppIsInBackground: " + "show")
        var isInBackground = true
        val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            val runningProcesses = am.runningAppProcesses
            for (processInfo in runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && processInfo.processName == context.packageName) {
                    for (activeProcess in processInfo.pkgList) {
                        if (activeProcess == context.packageName) {
                            isInBackground = false
                        }
                    }
                }
            }
        } else {
            val taskInfo = am.getRunningTasks(1)
            val componentInfo = taskInfo[0].topActivity
            if (componentInfo.packageName == context.packageName) {
                isInBackground = false
            }
        }
        return isInBackground
    }

    companion object {

        private val TAG = "MyFcmListenerService"
        var counter = 0
    }
}