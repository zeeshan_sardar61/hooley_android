package com.hooleyapp.hooley.db;


import android.os.Process;

import com.hooleyapp.hooley.log.Logger;
import com.hooleyapp.hooley.model.PhoneContactsModel;
import com.hooleyapp.hooley.others.Util;
import com.hooleyapp.hooley.tables.ContactNames;
import com.hooleyapp.hooley.tables.ContactNames_Table;
import com.hooleyapp.hooley.tables.ContactNumbers;
import com.hooleyapp.hooley.tables.ContactNumbers_Table;
import com.hooleyapp.hooley.tables.FriendsTable;
import com.hooleyapp.hooley.tables.FriendsTable_Table;
import com.raizlabs.android.dbflow.config.DatabaseDefinition;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.From;
import com.raizlabs.android.dbflow.sql.language.Join;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.language.Where;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ProcessModelTransaction;
import com.raizlabs.android.dbflow.structure.database.transaction.Transaction;

import java.util.List;

/**
 * Created by Zeeshan on 19-Dec-17.
 */

public class DbManager {

    public static DbManager instance;

    public static DbManager getInstance() {
        instance = new DbManager();
        return instance;
    }

    public static List<PhoneContactsModel> getContactList() {

        Where<ContactNumbers> joinQuery = SQLite.select(ContactNames_Table.cnContactID, ContactNames_Table.cnName, ContactNumbers_Table.contactNumber
                , ContactNumbers_Table.contactE164Number, ContactNumbers_Table.contactIsAppUser, ContactNumbers_Table.contactState,
                ContactNumbers_Table.contactDataID, ContactNumbers_Table.contactIsAppUser)
                .from(ContactNumbers.class)
                .join(ContactNames.class, Join.JoinType.LEFT_OUTER)
                .on(ContactNumbers_Table.contactID.withTable().eq(ContactNames_Table.cnContactID.withTable()))
                .groupBy(ContactNumbers_Table.contactE164Number.withTable())
                .orderBy(ContactNames_Table.cnName.withTable(), true);

        return joinQuery.queryCustomList(PhoneContactsModel.class);

    }

    public void exportDB() {
        /* To dump DB on SD Card */
        if (Logger.INSTANCE.getExportDatabase()) {
            Thread dbExportThread = new Thread(new Runnable() {

                @Override
                public void run() {
                    Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                    Util.exportDatabse(AppDatabase.NAME);
                }
            });
            dbExportThread.start();

        }
    }

    public void exportRoom() {
        /* To dump DB on SD Card */
        if (Logger.INSTANCE.getExportDatabase()) {
            Thread dbExportThread = new Thread(new Runnable() {

                @Override
                public void run() {
                    Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                    Util.exportDatabse("hooley_database");
                }
            });
            dbExportThread.start();

        }
    }

    public List<FriendsTable> getFriendList() {
        From<FriendsTable> joinQuery = SQLite.select(FriendsTable_Table.ALL_COLUMN_PROPERTIES)
                .from(FriendsTable.class);
//                .orderBy(FriendsTable_Table.fullName)
        return joinQuery.queryCustomList(FriendsTable.class);
    }

    public void setFriendList(final List<FriendsTable> friendList) {
        ProcessModelTransaction<FriendsTable> processModelTransaction =
                new ProcessModelTransaction.Builder<>(new ProcessModelTransaction.ProcessModel<FriendsTable>() {
                    @Override
                    public void processModel(FriendsTable friendsTable, DatabaseWrapper wrapper) {
                        friendsTable.save();
                    }
                }).addAll(friendList).build();
        DatabaseDefinition database = FlowManager.getDatabase(AppDatabase.class);
        Transaction transaction = database.beginTransactionAsync(processModelTransaction).build();
        transaction.execute();
    }

}
