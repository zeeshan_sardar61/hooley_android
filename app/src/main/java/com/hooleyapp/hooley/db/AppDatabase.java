package com.hooleyapp.hooley.db;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by aadil on 3/18/16.
 */
@Database(name = AppDatabase.NAME, version = AppDatabase.VERSION)
public class AppDatabase {
    public static final String NAME = "hooley_db";
    public static final int VERSION = 2;

}
