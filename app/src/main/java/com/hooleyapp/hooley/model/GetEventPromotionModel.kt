package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 5/4/2018.
 */

class GetEventPromotionModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("minMaxRange")
    var minMaxRange: MyObj? = null

    @SerializedName("promoteAudiences")
    var mlist = ArrayList<PromoteAudiences>()

    inner class MyObj {

        @SerializedName("minAmount")
        var minAmount: String? = null

        @SerializedName("maxAmount")
        var maxAmount: String? = null

        @SerializedName("minAge")
        var minAge: String? = null

        @SerializedName("maxAge")
        var maxAge: String? = null

    }

    class PromoteAudiences(@field:SerializedName("id")
                           var id: String, @field:SerializedName("name")
                           var name: String)


}