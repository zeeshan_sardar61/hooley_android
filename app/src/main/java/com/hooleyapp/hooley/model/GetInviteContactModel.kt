package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

class GetInviteContactModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("contactNumbers")
    var contactNumbersList = ArrayList<String>()


}
