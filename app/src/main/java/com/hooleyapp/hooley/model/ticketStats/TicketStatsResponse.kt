package com.hooleyapp.hooley.model.ticketStats

import com.google.gson.annotations.SerializedName

data class TicketStatsResponse(

        @field:SerializedName("ppvSalesList")
        val ppvSalesList: List<PpvSalesListItem?>? = null,

        @field:SerializedName("incomeShareUsers")
        val incomeShareUsers: List<IncomeShareUsersItem?>? = null,

        @field:SerializedName("charity")
        val charity: Charity? = null,

        @field:SerializedName("latestSaleStats")
        val latestSaleStats: List<LatestSaleStatsItem?>? = null,

        @field:SerializedName("latestScanStats")
        val latestScanStats: List<LatestScanStatsItem?>? = null,

        @field:SerializedName("Exception")
        val exception: String? = null,

        @field:SerializedName("IsSuccessful")
        val isSuccessful: Boolean? = null
)