package com.hooleyapp.hooley.model

import android.annotation.SuppressLint
import android.os.Parcel
import android.os.Parcelable
import com.hooleyapp.hooley.tables.FriendsTable

/**
 * Created by Zeeshan on 06-Mar-18.
 */

class ShareIncomePartnerModel : Parcelable {

    lateinit var friendsTable: FriendsTable
    var percent: Int = 0

    constructor(friendsTable: FriendsTable, percent: Int) {
        this.friendsTable = friendsTable
        this.percent = percent
    }

    protected constructor(`in`: Parcel) {
        percent = `in`.readInt()
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(percent)
    }

    companion object {

        @SuppressLint("ParcelCreator")
        val CREATOR: Parcelable.Creator<ShareIncomePartnerModel> = object : Parcelable.Creator<ShareIncomePartnerModel> {
            override fun createFromParcel(`in`: Parcel): ShareIncomePartnerModel {
                return ShareIncomePartnerModel(`in`)
            }

            override fun newArray(size: Int): Array<ShareIncomePartnerModel?> {
                return arrayOfNulls(size)
            }
        }
    }
}
