package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName

class GetUpdateEventActionModel {

    @SerializedName("Exception")
    var exception: String = ""
    @SerializedName("IsSuccessful")
    var IsSuccessful: Boolean = false

    @SerializedName("termsAndConditionInfo")
    lateinit var termsAndConditionInfo: TermsAndConditionInfo

    inner class TermsAndConditionInfo {

        @SerializedName("termsAndConditionUrl")
        var termsAndConditionUrl: String = ""

        @SerializedName("termsAndConditionText")
        var termsAndConditionText: String = ""

    }

}