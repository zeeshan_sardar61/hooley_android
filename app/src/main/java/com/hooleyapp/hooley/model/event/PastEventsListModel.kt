package com.hooleyapp.hooley.model.event

import com.google.gson.annotations.SerializedName
import java.util.*

data class PastEventsListModel(

	@field:SerializedName("past_events")
	val pastEvents: ArrayList<PastEventsItem>? = null,

	@field:SerializedName("Exception")
	val exception: String? = null,

	@field:SerializedName("IsSuccessful")
	val isSuccessful: Boolean? = null
)