package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName

class UploadImageModel {
    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("url")
    var url: String? = null
}