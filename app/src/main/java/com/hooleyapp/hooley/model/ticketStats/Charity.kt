package com.hooleyapp.hooley.model.ticketStats


import com.google.gson.annotations.SerializedName


data class Charity(

        @field:SerializedName("remoteDonations")
        val remoteDonations: Int? = null,

        @field:SerializedName("charityTarget")
        val charityTarget: Int? = null
)