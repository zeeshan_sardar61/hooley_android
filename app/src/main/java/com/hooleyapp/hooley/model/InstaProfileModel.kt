package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Zeeshan on 13-Mar-18.
 */

class InstaProfileModel {


    @SerializedName("data")
    var data: Data? = null

    @SerializedName("meta")
    var meta: Meta? = null

    inner class Data {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("username")
        var username: String? = null

    }

    inner class Meta {

        @SerializedName("code")
        var code: String? = null


    }

}
