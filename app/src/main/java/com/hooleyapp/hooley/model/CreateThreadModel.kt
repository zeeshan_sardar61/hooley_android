package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Nauman on 4/24/2018.
 */

class CreateThreadModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("threadId")
    var threadId: String? = null


}