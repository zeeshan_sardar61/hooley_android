package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 12/13/2017.
 */

class PersonalProfileModel {
    @SerializedName("userInfo")
    var userInfo = UserInfo()
    @SerializedName("relationshipList")
    var relationShipsList = ArrayList<myObject>()
    @SerializedName("politicalViewsList")
    var politicalViewsList = ArrayList<myObject>()
    @SerializedName("religionList")
    var religionList = ArrayList<myObject>()
    @SerializedName("languagesList")
    var languagesList = ArrayList<myObject>()
    @SerializedName("ethnicBackgroundList")
    var ethnicBackgroundList = ArrayList<myObject>()
    @SerializedName("userInterestList")
    var userInterestList = ArrayList<myObject>()
    @SerializedName("astrologicalSigns")
    var astrologicalSignsList = ArrayList<myObject>()
    @SerializedName("userOccupation")
    var userOccupationList = ArrayList<myObject>()
    @SerializedName("userPrivacyList")
    var userPrivacyList = ArrayList<UserPrivacy>()
    @SerializedName("userGeofence")
    var userGeofence = ArrayList<GeoFence>()
    @SerializedName("userSocialMediasList")
    var socialMediasList = ArrayList<SocialMedia>()
    @SerializedName("collageMajorList")
    var collageMajorList = ArrayList<myObject>()

    class myObject {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("name")
        var name: String? = null

        var isActive = false


    }

    class GeoFence {

        @SerializedName("userId")
        var userId: String? = null

        @SerializedName("latitude")
        var latitude: String? = null

        @SerializedName("longitude")
        var longitude: String? = null

        @SerializedName("radius")
        var radius: String? = null

        @SerializedName("address")
        var address: String? = null

    }

    inner class UserInfo {

        @SerializedName("firstName")
        var firstName: String? = null

        @SerializedName("lastName")
        var lastName: String? = null

        @SerializedName("profilePic")
        var profilePicture: String? = null

        @SerializedName("aboutMe")
        var aboutMe: String? = null

        @SerializedName("email")
        var email: String? = null

        @SerializedName("phoneNo")
        var phoneNo: String? = null

        @SerializedName("website")
        var website: String? = null

        @SerializedName("studentOf")
        var studentOf: String? = null

        @SerializedName("gender")
        var gender: String? = null

        @SerializedName("birthday")
        var birthday: String? = null

        @SerializedName("relationshipId")
        var relationshipId: String? = null

        @SerializedName("hobbies")
        var hobbies: String? = null

        @SerializedName("homeTown")
        var homeTown: String? = null

        @SerializedName("politicalViewId")
        var politicalViewId: String? = null

        @SerializedName("religionId")
        var religionId: String? = null

        @SerializedName("languagesId")
        var languageId: String? = null

        @SerializedName("ethnicBackgroundId")
        var ethnicBackgroundId: String? = null

        @SerializedName("astrologicalSignId")
        var astrologicalSignId: String? = null

        @SerializedName("collageMajor")
        var collageMajor: String? = null

        @SerializedName("highSchool")
        var highSchool: String? = null

        @SerializedName("ageGroupId")
        var ageGroupId: String? = null

        @SerializedName("currentCity")
        var currentCity: String? = null

        @SerializedName("employer")
        var employer: String? = null

        @SerializedName("organization")
        var organization: String? = null

        @SerializedName("occupationId")
        var occupationId: String? = null

        @SerializedName("countryCode")
        var countryCode: String? = null

        @SerializedName("countryISO")
        var countryISO: String? = null

        @SerializedName("isPhoneVerified")
        var isPhoneVerified: Boolean = false

        @SerializedName("country")
        var country: String? = null

        @SerializedName("state")
        var state: String? = null

    }

    inner class PoliticalView {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("name")
        var name: String? = null

    }

    inner class Religion {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("name")
        var name: String? = null

    }

    inner class Languages {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("name")
        var name: String? = null

    }

    inner class EthinicBackground {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("name")
        var name: String? = null

    }

    inner class UserInterest {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("name")
        var name: String? = null

    }

    inner class UserPrivacy {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("name")
        var name: String? = null

        @SerializedName("privacyId")
        var privacyId: String? = null

        @SerializedName("isActive")
        var isActive: Boolean = false

    }


    inner class SocialMedia {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("name")
        var name: String? = null

        @SerializedName("socialUrl")
        var socialUrl: String? = null

        @SerializedName("isConnected")
        var isConnected: Boolean = false

    }

    companion object {

        var sortByNameComparator: Comparator<myObject> = Comparator { s1, s2 ->
            val StateName1 = s1.name!!.toUpperCase()
            val StateName2 = s2.name!!.toUpperCase()
            StateName1.compareTo(StateName2)
        }
    }

}