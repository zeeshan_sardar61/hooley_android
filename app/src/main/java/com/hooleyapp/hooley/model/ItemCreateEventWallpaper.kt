package com.hooleyapp.hooley.model

import android.net.Uri

data class ItemCreateEventWallpaper(var url: String, var uri: Uri?, var type: Int)

data class UpdatePostMediaItem(var mediaUrl: String, var mediaUri: Uri?, var type: Int, var isVideo: Boolean)

data class RemoveAllSearchesRequestModel(var userId: String)

data class AddSearchHistoryRequestModel(var userId: String, var searchText: String)
