package com.hooleyapp.hooley.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class GetUserSearchesHistory {
    @SerializedName("searchList")
    @Expose
    public ArrayList<String> searchList = null;
    @SerializedName("IsSuccessful")
    @Expose
    public Boolean isSuccessful;
    @SerializedName("Exception")
    @Expose
    public String exception;

    /**
     * No args constructor for use in serialization
     */
    public GetUserSearchesHistory() {
    }

    /**
     * @param isSuccessful
     * @param exception
     * @param searchList
     */
    public GetUserSearchesHistory(ArrayList<String> searchList, Boolean isSuccessful, String exception) {
        super();
        this.searchList = searchList;
        this.isSuccessful = isSuccessful;
        this.exception = exception;
    }

}