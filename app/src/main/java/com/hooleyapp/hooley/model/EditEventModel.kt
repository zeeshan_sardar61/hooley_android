package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

/**
 * Created by Zeeshan on 15-Feb-18.
 */

class EditEventModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("eventBasicInfo")
    var eventBasicInfo: EventBasicInfo? = null

    @SerializedName("eventHostProfile")
    var eventHostProfileList = ArrayList<EventHostProfile>()

    @SerializedName("eventCoHostProfile")
    var eventCoHostProfileList = ArrayList<FriendsTable>()

    @SerializedName("eventCovers")
    var eventCoverList = ArrayList<EventCover>()

    @SerializedName("eventCategories")
    var eventCategoryList = ArrayList<EventCategory>()

    @SerializedName("optionalEventCategories")
    var eventOptionalCategoryList = ArrayList<EventOptionalCategory>()

    @SerializedName("eventGeofance")
    var eventGeofenceList = ArrayList<PersonalProfileModel.GeoFence>()


    inner class EventBasicInfo {

        @SerializedName("eventHashTags")
        var eventHashs = ArrayList<String>()

        @SerializedName("eventType")
        val eventType: String? = null

        @SerializedName("eventName")
        var eventName: String? = null

        @SerializedName("eventDetails")
        var eventDetails: String? = null

        @SerializedName("eventStartTime")
        var eventStartTime: String? = null

        @SerializedName("eventEndTime")
        var eventEndTime: String? = null

        @SerializedName("isPrivate")
        var isPrivate: Boolean = false

        @SerializedName("isPaid")
        var isPaid: Boolean = false

        @SerializedName("isPublished")
        var isPublished: Boolean = false

        @SerializedName("minimumAgeId")
        var minimumAgeId: String? = null

    }


    class EventHostProfile {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("hostId")
        var hostId: String? = null

        @SerializedName("fullName")
        var fullName: String? = null

        @SerializedName("profilePic")
        var profilePic: String? = null

        var isDeleted = false
    }

    class EventCoHostProfile {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("coHostId")
        var coHostId: String? = null

        @SerializedName("fullName")
        var fullName: String? = null

        @SerializedName("profilePic")
        var profilePic: String? = null

        var isDeleted = false

    }

    class EventCover {

        @SerializedName("photo_url")
        var photo_url: String? = null
    }

    inner class EventCategory {
        @SerializedName("id")
        var Id: String? = null

        @SerializedName("name")
        var Name: String? = null

        @SerializedName("icon")
        var icon: String? = null

        @SerializedName("isAdded")
        var isAdded: Boolean = false
    }

    class EventOptionalCategory {

        @SerializedName("id")
        var Id: String? = null

        @SerializedName("categoryName")
        var categoryName: String? = null

        var isDeleted = false

    }
}
