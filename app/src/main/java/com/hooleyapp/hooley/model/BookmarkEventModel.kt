package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Zeeshan on 21-Mar-18.
 */

class BookmarkEventModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("myEventInfo")
    var bookmarkedEventsList = ArrayList<EventDetail>()


    class EventDetail {

        @SerializedName("eventId")
        var eventId: String? = null

        @SerializedName("coverPhoto")
        var coverPhoto: String? = null

        @SerializedName("startTime")
        var startTime: String? = null

        @SerializedName("endTime")
        var endTime: String? = null

        @SerializedName("eventName")
        var eventName: String? = null

        @SerializedName("address")
        var address: String? = null

        @SerializedName("eventLat")
        var eventLat: Double? = null

        @SerializedName("eventLong")
        var eventLong: Double? = null

        @SerializedName("likesCount")
        var likesCount: Int = 0

        @SerializedName("commentsCount")
        var commentsCount: Int = 0

        @SerializedName("sharesCounts")
        var sharesCount: Int = 0

        @SerializedName("isLiked")
        var isLiked: Boolean = false

        @SerializedName("isLive")
        var isLive: Boolean = false

        @SerializedName("isPast")
        var isPast: Boolean = false

        @SerializedName("isFollowed")
        var isFollowed: Boolean = false

        @SerializedName("followersCount")
        var followersCount: Int = 0
    }

    companion object {

        //Getter and setter methods same as the above examples
        /*Comparator for sorting the list by Student Name*/
        var sortByNameComparator: Comparator<EventDetail> = Comparator { s1, s2 ->
            val StudentName1 = s1.eventName!!.toUpperCase()
            val StudentName2 = s2.eventName!!.toUpperCase()
            StudentName1.compareTo(StudentName2)
        }
    }
}
