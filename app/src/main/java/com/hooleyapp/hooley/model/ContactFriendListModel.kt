package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.ArrayList
import kotlin.Comparator

class ContactFriendListModel {
    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("contactList")
    var contactListList = ArrayList<ContactModel>()

    class ContactModel {

        @SerializedName("userId")
        var userId: String = ""

        @SerializedName("profilePic")
        var profilePic: String = ""

        @SerializedName("fullName")
        var fullName: String = ""

        @SerializedName("homeTown")
        var homeTown: String = ""

        @SerializedName("status")
        var status: String = ""

        @SerializedName("countryName")
        var countryName: String = ""

        @SerializedName("stateName")
        var stateName: String = ""

        @SerializedName("cityName")
        var cityName: String = ""

        @SerializedName("contactNumber")
        var contactNumber: String = ""

        companion object {

            //Getter and setter methods same as the above examples
            /*Comparator for sorting the list by Student Name*/
            var sortByNameComparator: java.util.Comparator<ContactModel> = Comparator { s1, s2 ->
                val StudentName1 = s1.fullName.toUpperCase()
                val StudentName2 = s2.fullName.toUpperCase()
                StudentName1.compareTo(StudentName2)
            }

        }
    }

}