package com.hooleyapp.hooley.model

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root
import java.util.*

/**
 * Created by Nauman on 12/15/2017.
 */
@Root(strict = false)
class RootModel {

    @ElementList(entry = "object", inline = true, required = false)
    var `object`: ArrayList<MyObject>? = null


    @Root(name = "object", strict = false)
    class MyObject {

        @Element(name = "id", required = false)
        var id: String? = null

        @Element(name = "name", required = false)
        var name: String? = null

    }

}
