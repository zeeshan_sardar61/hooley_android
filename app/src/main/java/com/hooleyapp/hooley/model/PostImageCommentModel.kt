package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventDetailModel
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

/**
 * Created by Zeeshan on 22-Jan-18.
 */

class PostImageCommentModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("imageInfo")
    var imageInfo: EventGalleryFeedModel.EventMediaInfo? = null

    @SerializedName("showComments")
    var postComments = ArrayList<EventDetailModel.Comments>()


    @SerializedName("tagPeopleInfo")
    var tagFriends = ArrayList<FriendsTable>()


    //    public class ImageInfo {
    //
    //        @SerializedName("postId")
    //        public String imageId;
    //
    //        @SerializedName("imageUrl")
    //        public String imageUrl;
    //
    //        @SerializedName("eventId")
    //        public String eventId;
    //
    //        @SerializedName("userId")
    //        public String userId;
    //
    //        @SerializedName("profilePic")
    //        public String profilePic;
    //
    //        @SerializedName("fullName")
    //        public String fullName;
    //
    //        @SerializedName("eventName")
    //        public String eventName;
    //
    //        @SerializedName("videoUrl")
    //        public String videoUrl;
    //
    //        @SerializedName("thumbnailUrl")
    //        public String thumbnailUrl;
    //
    //        @SerializedName("postedDate")
    //        public String postedDate;
    //
    //        @SerializedName("likesCount")
    //        public int likesCount;
    //
    //        @SerializedName("commentsCount")
    //        public int commentsCount;
    //
    //        @SerializedName("sharesCounts")
    //        public int shareCount;
    //
    //        @SerializedName("imageCaption")
    //        public String imageCaption;
    //
    //        @SerializedName("isLiked")
    //        public boolean isLiked;
    //
    //        @SerializedName("isVideo")
    //        public boolean isVideo;
    //
    //        @SerializedName("isLive")
    //        public boolean isLive;
    //
    //        @SerializedName("isFavorite")
    //        public boolean isFavorited;
    //
    //        @SerializedName("favoriteCount")
    //        public int favoriteCount;
    //
    //        @SerializedName("mediaFiles")
    //        public ArrayList<EventGalleryFeedModel.MediaFile> mediaFiles = new ArrayList<>();
    //
    //    }
}
