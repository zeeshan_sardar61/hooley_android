package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Nauman on 12/7/2017.
 */

class LoginModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("isFirstLogin")
    var isFirstLogin: Boolean = false

    @SerializedName("isBusinessProfile")
    var isBusinessProfile: Boolean = false

    @SerializedName("isWalletSetup")
    var isWalletSetup: Boolean = false

    @SerializedName("isDark")
    var isDark: Boolean = false

    @SerializedName("userToken")
    var userToken: String? = null

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("userId")
    var userId: String? = null

    @SerializedName("generalProfile")
    var generalProfile: PersonalProfileModel? = null

    @SerializedName("businessProfile")
    var businessProfile: BusinessProfileModel? = null


}