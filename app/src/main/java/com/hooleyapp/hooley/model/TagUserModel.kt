package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 2/14/2018.
 */

class TagUserModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("tagList")
    var tagList = ArrayList<WhosHereModel.User>()


    //    public class TagUser {
    //
    //        @SerializedName("userId")
    //        public String userId;
    //
    //        @SerializedName("profilePic")
    //        public String profilePic;
    //
    //        @SerializedName("fullName")
    //        public String fullName;
    //
    //        @SerializedName("homeTown")
    //        public String homeTown;
    //
    //        @SerializedName("gender")
    //        public String gender;
    //
    //        @SerializedName("imJid")
    //        public String imJid;
    //
    //        @SerializedName("ageGroup")
    //        public String ageGroup;
    //
    //        @SerializedName("isfriend")
    //        public String isfriend;
    //
    //    }

}
