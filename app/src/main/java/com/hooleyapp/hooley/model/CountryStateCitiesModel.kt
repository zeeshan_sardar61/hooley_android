package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Zeeshan on 14-Mar-18.
 */

class CountryStateCitiesModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("countryStateCitiesList")
    var countryStateCitiesList = ArrayList<PersonalProfileModel.myObject>()

}
