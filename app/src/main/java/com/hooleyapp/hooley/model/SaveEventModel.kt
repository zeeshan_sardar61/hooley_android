package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Nauman on 12/29/2017.
 */

class SaveEventModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("event_obj")
    var eventObject = EventObj()

    inner class EventObj {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("photo_url")
        var photo_url: String? = null

        @SerializedName("name")
        var name: String? = null

        @SerializedName("is_published")
        var is_published: String? = null
    }


}
