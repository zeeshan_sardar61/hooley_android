package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Nauman on 12/29/2017.
 */

class PastGuestListModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String = ""

    @SerializedName("guestList")
    var guestArrayList = ArrayList<Guest>()

    inner class Guest {

        @SerializedName("eventId")
        var eventid: String = ""

        @SerializedName("coverPhoto")
        var coverPhoto: String = ""

        @SerializedName("eventName")
        var eventName: String = ""

        @SerializedName("invitGuestCount")
        var invitGuestCount: String = ""

        @SerializedName("eventAddress")
        var eventAddress: String = ""

        @SerializedName("startTime")
        var startTime: String = ""

        @SerializedName("endTime")
        var endTime: String = ""

        var isChecked = false

        var userIdList = ArrayList<Int>()

        var isSelectedAll = false

        /*
        0 = no select
        1 = all select
        2 = multi selection
         */
        var selection = 0
    }

}
