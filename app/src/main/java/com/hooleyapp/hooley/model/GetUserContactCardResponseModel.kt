package com.hooleyapp.hooley.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class GetUserContactCardResponseModel {

    @SerializedName("userContactCardList")
    @Expose
    var userContactCardList: ArrayList<UserContactCardList>? = null

    @SerializedName("IsSuccessful")
    @Expose
    var isSuccessful: Boolean = false

    @SerializedName("Exception")
    @Expose
    var exception: String = ""

    inner class UserContactCardList {

        @SerializedName("userId")
        @Expose
        var userId: String? = null

        @SerializedName("profilePic")
        @Expose
        var profilePic: String? = null

        @SerializedName("fullName")
        @Expose
        var fullName: String? = null

        @SerializedName("homeTown")
        @Expose
        var homeTown: String? = null

        @SerializedName("gender")
        @Expose
        var gender: Int? = null

        @SerializedName("ageGroup")
        @Expose
        var ageGroup: String? = null

        @SerializedName("isFriend")
        @Expose
        var isFriend: Boolean? = null

        @SerializedName("status")
        @Expose
        var status: String? = null

        @SerializedName("countryName")
        @Expose
        var countryName: String? = null

        @SerializedName("stateName")
        @Expose
        var stateName: String? = null

        @SerializedName("cityName")
        @Expose
        var cityName: String? = null

        @SerializedName("isMessageAccessible")
        @Expose
        var isMessageAccessible: Boolean = false

        @SerializedName("isFriendRequestAccessible")
        @Expose
        var isFriendRequestAccessible: Boolean = false

    }

}
