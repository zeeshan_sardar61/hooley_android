package com.hooleyapp.hooley.model.ticketStats


import com.google.gson.annotations.SerializedName


data class LatestScanStatsItem(

        @field:SerializedName("sold")
        val sold: Int? = null,

        @field:SerializedName("ticketCategory")
        val ticketCategory: String? = null,

        @field:SerializedName("left")
        val left: Int? = null,

        @field:SerializedName("scan")
        val scan: Int? = null
)