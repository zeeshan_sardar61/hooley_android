package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 1/18/2018.
 */

class MyHostEventModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("publishEvents")
    var publishEventsArrayList = ArrayList<HostEvents>()

    @SerializedName("unPublishEvents")
    var unPublishEventsArrayList = ArrayList<HostEvents>()

    inner class HostEvents {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("eventName")
        var eventName: String = ""

        @SerializedName("startTime")
        var startTime: String? = null

        @SerializedName("endTime")
        var endTime: String? = null

        @SerializedName("alertDate")
        var alertDate: String? = null

        @SerializedName("coverPhoto")
        var coverPhoto: String? = null

        @SerializedName("peopleCount")
        var peopleCount: String? = null

        @SerializedName("isDeleteAble")
        var isDeleteAble: Boolean = false

        @SerializedName("isPastEvent")
        var isPastEvent: Boolean = false

        @SerializedName("isAlertOn")
        var isAlertOn: Boolean = false

        @SerializedName("createdDate")
        var createdDate: String = ""

        @SerializedName("isIndefinetly")
        var isIndefinetly: Boolean = false


    }

}