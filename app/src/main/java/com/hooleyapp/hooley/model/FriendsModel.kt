package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

/**
 * Created by Nauman on 12/22/2017.
 */

class FriendsModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("friendList")
    var friendsList = ArrayList<FriendsTable>()

}