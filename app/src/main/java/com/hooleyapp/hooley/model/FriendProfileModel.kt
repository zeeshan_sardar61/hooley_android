package com.hooleyapp.hooley.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 3/6/2018.
 */

class FriendProfileModel {


    @SerializedName("isBusinessAvail")
    var isBusinessAvail: Boolean = false

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("friendGeneralProfile")
    var friendGeneralProfile: FriendGeneralProfile? = null

    @SerializedName("friendBusinessProfile")
    var friendBusinessProfile: FriendBusinessProfile? = null

    inner class FriendGeneralProfile {

        @SerializedName("friendBasicInfo")
        var friendBasicInfo: FriendBasicInfo? = null

        @SerializedName("friendContactInfo")
        var friendContactInfo: FriendContactInfo? = null

        @SerializedName("friendGeneralProfileInfo")
        var friendGeneralProfileInfo: FriendGeneralProfileInfo? = null

        @SerializedName("userSocialList")
        var userSocialList = ArrayList<PersonalSocialList>()

        @SerializedName("isMessageAccessible")
        @Expose
        var isMessageAccessible: Boolean = false

        @SerializedName("isFriendRequestAccessible")
        @Expose
        var isFriendRequestAccessible: Boolean = false


    }

    inner class FriendBasicInfo {

        @SerializedName("profilePic")
        var profilePic: String? = null

        @SerializedName("fullName")
        var fullName: String? = null

        @SerializedName("aboutMe")
        var aboutMe: String? = null

        @SerializedName("homeTown")
        var homeTown: String? = null

    }


    inner class FriendContactInfo {

        @SerializedName("email")
        var email: String? = null

        @SerializedName("phoneNo")
        var phoneNo: String? = null

        @SerializedName("website")
        var website: String? = null
        @SerializedName("isPhoneVerified")
        var isPhoneVerified: Boolean = false

    }

    inner class FriendGeneralProfileInfo {

        @SerializedName("gender")
        var gender: String? = null

        @SerializedName("hobbies")
        var hobbies: String? = null

        @SerializedName("collage")
        var collage: String? = null

        @SerializedName("collageMajor")
        var collageMajor: String? = null

        @SerializedName("highSchool")
        var highSchool: String? = null

        @SerializedName("politicalView")
        var politicalView: String? = null

        @SerializedName("religion")
        var religion: String? = null

        @SerializedName("relationship")
        var relationship: String? = null

        @SerializedName("ethinicBackground")
        var ethinicBackground: String? = null

        @SerializedName("zodiacSign")
        var zodiacSign: String? = null

        @SerializedName("currentCity")
        var currentCity: String? = null

        @SerializedName("occupation")
        var occupation: String? = null

        @SerializedName("employer")
        var employer: String? = null

        @SerializedName("countryName")
        var countryName: String? = null

        @SerializedName("stateName")
        var stateName: String? = null

        @SerializedName("cityName")
        var cityName: String? = null

        @SerializedName("organization")
        var organization: String? = null

        @SerializedName("languages")
        var languages: String? = null

        @SerializedName("homeTown")
        var homeTown: String? = null


        @SerializedName("friendInterests")
        var friendInterestList = ArrayList<FriendInterest>()

        @SerializedName("ageGroup")
        var ageGroup: String? = null

    }


    inner class FriendInterest {

        @SerializedName("name")
        var name: String? = null

    }

    inner class FriendBusinessProfile {

        @SerializedName("profilePic")
        var profilePic: String? = null

        @SerializedName("fullName")
        var fullName: String? = null

        @SerializedName("address")
        var address: String? = null

        @SerializedName("aboutBusiness")
        var aboutBusiness: String? = null

        @SerializedName("businessName")
        var businessName: String? = null


        @SerializedName("website")
        var website: String? = null

        @SerializedName("contactName")
        var contactName: String? = null


        @SerializedName("businessEmail")
        var businessEmail: String? = null

        @SerializedName("businessPhone")
        var businessPhone: String? = null

        @SerializedName("isPhoneVerified")
        var isPhoneVerified: Boolean = false


        @SerializedName("zipCode")
        var zipCode: String? = null

        @SerializedName("companyType")
        var companyType: String? = null


        @SerializedName("countryName")
        var countryName: String? = null

        @SerializedName("stateName")
        var stateName: String? = null

        @SerializedName("cityName")
        var cityName: String? = null

        @SerializedName("industry")
        var industry: String? = null

        @SerializedName("businessSocialList")
        var businessSocialLists = ArrayList<BusinessSocialList>()
    }


    inner class BusinessSocialList {

        @SerializedName("socialUrl")
        var socialUrl: String? = null

        @SerializedName("socialName")
        var socialName: String? = null
    }

    inner class PersonalSocialList {

        @SerializedName("socialMediaId")
        var socialMediaId: String? = null

        @SerializedName("socialUrl")
        var socialUrl: String? = null

        @SerializedName("name")
        var socialName: String? = null
    }

}