package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName

/**
 * Created by adilmalik on 09/05/2018.
 */

class SetAlertModel {


    @SerializedName("IsSuccessful")
    var IsSuccessful: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("alertText")
    var alertText: String? = null

}