package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName

class GetTermsConditionModel {

    @SerializedName("termsAndConditions")
    lateinit var termsAndConditions: TermsAndConditions
    @SerializedName("IsSuccessful")
    var isSuccessful: Boolean = false
    @SerializedName("Exception")
    var exception: String = ""

    @SerializedName("stripeEmail")
    var stripeEmail: String? = null

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param isSuccessful
     * @param exception
     * @param termsAndConditions
     */
    constructor(termsAndConditions: TermsAndConditions, isSuccessful: Boolean, exception: String) : super() {
        this.termsAndConditions = termsAndConditions
        this.isSuccessful = isSuccessful
        this.exception = exception
    }

    inner class TermsAndConditions {

        @SerializedName("termsAndConditionsText")
        var termsAndConditionsText: String = ""

        @SerializedName("url")
        var url: String = ""

        /**
         * No args constructor for use in serialization
         */
        constructor()

        /**
         * @param termsAndConditionsText
         * @param url
         */
        constructor(termsAndConditionsText: String, url: String) : super() {
            this.termsAndConditionsText = termsAndConditionsText
            this.url = url
        }

    }
}