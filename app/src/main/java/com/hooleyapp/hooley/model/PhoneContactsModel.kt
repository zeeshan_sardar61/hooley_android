package com.hooleyapp.hooley.model

import com.hooleyapp.hooley.db.AppDatabase
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.QueryModel
import com.raizlabs.android.dbflow.structure.BaseQueryModel

import java.io.Serializable

/**
 * Created by Nauman on 12/29/2017.
 */
@QueryModel(database = AppDatabase::class)
class PhoneContactsModel : BaseQueryModel(), Serializable {

    @Column
    var cnContactID: Long = 0

    @Column
    var cnName: String = ""

    @Column
    var contactNumber: String = ""

    @Column
    var contactE164Number: String = ""

    var checked: Boolean? = false

    var alreadySend: Boolean? = false
}
