package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 12/15/2017.
 */

class BusinessProfileModel {
    //    public static BusinessProfileModel _obj = null;
    //
    //    public BusinessProfileModel() {
    //
    //    }
    //
    //    public static BusinessProfileModel getInstance() {
    //        if (_obj == null) {
    //            _obj = new BusinessProfileModel();
    //        }
    //        return _obj;
    //    }
    //
    //    public void setObj(BusinessProfileModel obj) {
    //        _obj = obj;
    //    }
    //
    //
    //    @SerializedName("IsSuccessful")
    //    public boolean success;
    //
    //    @SerializedName("Exception")
    //    public String exception;


    @SerializedName("businessInformation")
    var businessInfo = BusinessInfo()


    @SerializedName("industryList")
    var industryList = ArrayList<PersonalProfileModel.myObject>()

    @SerializedName("companyTypeList")
    var companyTypeList = ArrayList<PersonalProfileModel.myObject>()

    @SerializedName("socialMediasList")
    var socialMediaArrayList = ArrayList<SocialMedia>()


    inner class BusinessInfo {

        @SerializedName("businessProfilePicture")
        var businessProfilePicture: String? = null

        @SerializedName("businessName")
        var businessName: String? = null

        @SerializedName("aboutBusiness")
        var aboutBusiness: String? = null

        @SerializedName("businessEmail")
        var businessEmail: String? = null

        @SerializedName("businessPhone")
        var businessPhone: String? = null

        @SerializedName("website")
        var website: String? = null

        @SerializedName("isPhoneVerified")
        var isPhoneVerified: Boolean = false

        @SerializedName("contactName")
        var contactName: String? = null

        @SerializedName("industryId")
        var industryId: String? = null

        @SerializedName("companyTypeId")
        var companyTypeId: String? = null

        @SerializedName("address")
        var address: String? = null

        @SerializedName("city")
        var city: String? = null

        @SerializedName("state")
        var state: String? = null

        @SerializedName("zipCode")
        var zipCode: String? = null

        @SerializedName("country")
        var country: String? = null

        @SerializedName("countryISO")
        var countryISO: String? = null

        @SerializedName("countryCode")
        var countryCode: String? = null

    }


    class SocialMedia {

        @SerializedName("id")
        var id: Int = 0

        @SerializedName("userId")
        var userId: String? = null

        @SerializedName("socialUrl")
        var socialUrl: String? = null

        @SerializedName("socialName")
        var socialName: String? = null

    }
}