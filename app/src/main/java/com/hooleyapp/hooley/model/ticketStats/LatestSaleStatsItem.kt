package com.hooleyapp.hooley.model.ticketStats


import com.google.gson.annotations.SerializedName


data class LatestSaleStatsItem(

        @field:SerializedName("sold")
        val sold: Int? = null,

        @field:SerializedName("total")
        val total: Int? = null,

        @field:SerializedName("revenue")
        val revenue: Int? = null,

        @field:SerializedName("ticketCategory")
        val ticketCategory: String? = null,

        @field:SerializedName("available")
        val available: Int? = null
)