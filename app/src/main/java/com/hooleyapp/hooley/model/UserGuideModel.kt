package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 12/5/2017.
 */

class UserGuideModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("userGuid")
    var userGuideArrayList = ArrayList<UserGuide>()

    inner class UserGuide {

        @SerializedName("imageUrl")
        var imageUrl: String? = null

        @SerializedName("guideText")
        var guideText: String? = null
    }


}