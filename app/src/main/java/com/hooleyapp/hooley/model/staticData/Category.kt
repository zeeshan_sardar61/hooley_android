package com.hooleyapp.hooley.model.staticData

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Category {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("icon_url")
    @Expose
    var iconUrl: String? = null

    var isActive = false


    constructor(id: Int?, name: String?, iconUrl: String?) {
        this.id = id
        this.name = name
        this.iconUrl = iconUrl
    }

    constructor()


}
