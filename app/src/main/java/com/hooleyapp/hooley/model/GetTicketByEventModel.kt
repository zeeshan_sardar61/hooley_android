package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Zeeshan on 07-Mar-18.
 */

class GetTicketByEventModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String = ""

    @SerializedName("charity")
    var charity: Charity? = null

    @SerializedName("generalTicket")
    var ticketObj: TicketObject? = null

    @SerializedName("payPerView")
    var payPerView: PayPerView? = null

    class PayPerView {
        @SerializedName("ppvAmount")
        var ppvAmount: Int = 0

        @SerializedName("isPpvAvailable")
        var isPpvAvailable: Boolean = false

    }

    class Charity {

        @SerializedName("charityTarget")
        var charityTarget: Int = 0

        @SerializedName("remoteDonations")
        var charitySoFar: Int = 0
    }

    class TicketObject {

        @SerializedName("onlineSaleStart")
        var onlineSaleStart: String = ""

        @SerializedName("onlineSaleEnd")
        var onlineSaleEnd: String = ""

        @SerializedName("ticketInfo")
        var ticketInfoList = ArrayList<TicketInfo>()

    }

    class TicketInfo {

        @SerializedName("ticketId")
        var ticketId: String = ""

        @SerializedName("ticketName")
        var ticketName: String = ""

        @SerializedName("quantity")
        var quantity: Int = 0

        @SerializedName("price")
        var price: Int = 0

        @SerializedName("available")
        var available: Int = 0

        @SerializedName("sold")
        var sold: Int = 0
    }

    @SerializedName("eventBasicInfo")
    lateinit var eventBasicInfo: EventBasicInfo

    class EventBasicInfo {

        @SerializedName("eventId")
        var eventId: String = ""

        @SerializedName("eventName")
        var eventName: String = ""

        @SerializedName("userId")
        var userId: String = ""

        @SerializedName("eventCover")
        var eventCover: String = ""

        @SerializedName("hostName")
        var hostname: String = ""

        @SerializedName("profilePicture")
        var profilePicture: String = ""

        @SerializedName("address")
        var address: String = ""

    }
}
