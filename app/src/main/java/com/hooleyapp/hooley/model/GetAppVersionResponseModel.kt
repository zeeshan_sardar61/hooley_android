package com.hooleyapp.hooley.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GetAppVersionResponseModel {

    @SerializedName("hooleyVersion")
    @Expose
    lateinit var hooleyVersion: HooleyVersion
    @SerializedName("IsSuccessful")
    @Expose
    var isSuccessful: Boolean = false
    @SerializedName("Exception")
    @Expose
    var exception: String = ""

    /**
     * No args constructor for use in serialization
     *
     */
    constructor()

    /**
     *
     * @param isSuccessful
     * @param exception
     * @param hooleyVersion
     */
    constructor(hooleyVersion: HooleyVersion, isSuccessful: Boolean, exception: String) : super() {
        this.hooleyVersion = hooleyVersion
        this.isSuccessful = isSuccessful
        this.exception = exception
    }


    inner class HooleyVersion {

        @SerializedName("versionName")
        @Expose
        var versionName: String = ""
        @SerializedName("versionCode")
        @Expose
        var versionCode: String = ""

        /**
         * No args constructor for use in serialization
         *
         */
        constructor()

        /**
         *
         * @param versionCode
         * @param versionName
         */
        constructor(versionName: String, versionCode: String) : super() {
            this.versionName = versionName
            this.versionCode = versionCode
        }

    }
}