package com.hooleyapp.hooley.model

import java.util.*

/**
 * Created by Zeeshan on 19-Feb-18.
 */

class NearByEventsFinalModel {

    var id: Int = 0
    var type: Int = 0
    var eventDetailList = ArrayList<NearByEventListFeedModel.EventDetail>()
    lateinit var eventDetail: NearByEventListFeedModel.EventDetail

    constructor(type: Int, checkId: Int) {
        this.id = checkId
        this.type = type
    }

    constructor(type: Int, eventDetailList: ArrayList<NearByEventListFeedModel.EventDetail>) {
        this.type = type
        this.eventDetailList = eventDetailList
    }

    constructor(type: Int, eventDetailModel: NearByEventListFeedModel.EventDetail) {
        this.type = type
        this.eventDetail = eventDetailModel
    }
}
