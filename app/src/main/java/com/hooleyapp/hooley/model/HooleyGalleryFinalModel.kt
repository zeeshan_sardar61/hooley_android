package com.hooleyapp.hooley.model

/**
 * Created by Zeeshan on 22-Dec-17.
 */

class HooleyGalleryFinalModel {

    var categoryid: String = ""
    var type: Int = 0
    var categoryName: String = ""
    var imageUrl: String = ""
    //Added by Shahzaib
    var isSelected = false

    constructor()

    constructor(categoryid: String, type: Int, categoryName: String, imageUrl: String) {
        this.categoryid = categoryid
        this.type = type
        this.categoryName = categoryName
        this.imageUrl = imageUrl
    }

    constructor(categoryid: String, type: Int, categoryName: String) {
        this.categoryid = categoryid
        this.type = type
        this.categoryName = categoryName
    }

    constructor(categoryid: String, imageUrl: String, type: Int) {
        this.categoryid = categoryid
        this.type = type
        this.imageUrl = imageUrl
    }
}
