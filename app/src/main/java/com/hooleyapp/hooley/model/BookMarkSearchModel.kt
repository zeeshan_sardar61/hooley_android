package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 4/4/2018.
 */

class BookMarkSearchModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("categories")
    var categoriesList = ArrayList<CreateEventModel.Category>()

    @SerializedName("searchbookMark")
    var searchbookMarkList = ArrayList<SearchbookMark>()


    inner class SearchbookMark {

        @SerializedName("isPaid")
        var isPaid: Boolean = false

        @SerializedName("isFree")
        var isFree: Boolean = false

        @SerializedName("searchText")
        var searchText: String? = null

        @SerializedName("miles")
        var miles: Double = 0.toDouble()

        @SerializedName("id")
        var id: String? = null

        @SerializedName("eventType")
        var eventTypeList = ArrayList<EventType>()

    }

    inner class EventType {

        @SerializedName("typeId")
        var typeId: String? = null
    }


}
