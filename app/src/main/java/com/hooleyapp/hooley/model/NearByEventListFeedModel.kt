package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Zeeshan on 19-Feb-18.
 */

class NearByEventListFeedModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("featuredEvents")
    var featuredEventsList = ArrayList<EventDetail>()

    @SerializedName("todayEvents")
    var todayEventsList = ArrayList<EventDetail>()

    @SerializedName("nextWeekEvents")
    var nextWeekEventsList = ArrayList<EventDetail>()

    @SerializedName("commingEvents")
    var commingEventsList = ArrayList<EventDetail>()

    class EventDetail {

        @SerializedName("hostedId")
        var hostedId: String? = null

        @SerializedName("eventId")
        var eventId: String? = null

        @SerializedName("coverPhoto")
        var coverPhoto: String? = null

        @SerializedName("startTime")
        var startTime: String? = null

        @SerializedName("endTime")
        var endTime: String? = null

        @SerializedName("eventName")
        var eventName: String? = null

        @SerializedName("address")
        var address: String? = null

        @SerializedName("eventLat")
        var eventLat: Double? = null

        @SerializedName("eventLong")
        var eventLong: Double? = null

        @SerializedName("likesCount")
        var likesCount: Int = 0

        @SerializedName("commentsCount")
        var commentsCount: Int = 0

        @SerializedName("sharesCounts")
        var sharesCount: Int = 0

        @SerializedName("isLiked")
        var isLiked: Boolean = false

        @SerializedName("profilePic")
        var profilePic: String? = null

        @SerializedName("fullName")
        var fullName: String? = null

        @SerializedName("attendCount")
        var attendCount: Int = 0

        @SerializedName("followersCount")
        var followersCount: Int = 0

        @SerializedName("isAttended")
        var isAttended: Boolean = false

        @SerializedName("isFollowed")
        var isFollowed: Boolean = false

        @SerializedName("isLive")
        var isLive: Boolean = false

    }

}
