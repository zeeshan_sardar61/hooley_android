package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 1/19/2018.
 */

class MyAttendingEventModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("attending")
    var attendingEventList = ArrayList<MyHostEventModel.HostEvents>()

}
