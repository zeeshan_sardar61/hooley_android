package com.hooleyapp.hooley.model.event

import com.google.gson.annotations.SerializedName
import com.hooleyapp.hooley.others.Constants
import java.util.*

class GetEditPostModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("caption")
    var caption: String? = null

    @SerializedName("mediaUrl")
    var mediaUrlList = ArrayList<PostMediaItem>()

    @SerializedName("tagList")
    var tagList = ArrayList<TagUserItem>()

    class PostMediaItem {

        @SerializedName("isVideo")
        var isVideo: Boolean = false

        @SerializedName("url")
        var url: String? = null

    }

    class TagUserItem {

        @SerializedName("userId")
        var userId: Int = 0

        @SerializedName("profilePic")
        var profilePic: String? = null

        var state = Constants.TYPE_TAG_NONE
    }

}