package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Zeeshan on 22-Dec-17.
 */

class HooleyGalleryModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("eventGalleryCategories")
    var eventGallryCategories = ArrayList<EventGallryCategories>()

    @SerializedName("eventGalleryList")
    var eventGalleryLists = ArrayList<EventGalleryList>()


    inner class EventGallryCategories {

        @SerializedName("categoryId")
        var categoryId: String? = null

        @SerializedName("categoryName")
        var ccategoryName: String? = null

    }

    inner class EventGalleryList {

        @SerializedName("categoryId")
        var categoryId: String? = null

        @SerializedName("imageUrl")
        var imageUrl: String? = null

    }

}
