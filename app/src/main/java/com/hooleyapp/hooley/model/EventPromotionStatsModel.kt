package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

class EventPromotionStatsModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("promotionStats")
    var promotionStats: PromotionStats? = null

    inner class PromotionStats {

        @SerializedName("coverPhoto")
        var coverPhoto: String? = null

        @SerializedName("eventName")
        var eventName: String? = null

        @SerializedName("address")
        var address: String? = null

        @SerializedName("profilePic")
        var profilePic: String? = null

        @SerializedName("fullName")
        var fullName: String? = null

        @SerializedName("latitude")
        var latitude: Double = 0.toDouble()

        @SerializedName("longitude")
        var longitude: Double = 0.toDouble()

        @SerializedName("startTime")
        var startTime: String? = null

        @SerializedName("endTime")
        var endTime: String? = null

        @SerializedName("totalViewCount")
        var totalViewCount: Int = 0

        @SerializedName("states")
        var statesList = ArrayList<StateCity>()
    }

    inner class StateCity {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("name")
        var name: String? = null

        @SerializedName("viewsCount")
        var viewCount: Int = 0

        @SerializedName("cities")
        var citiesList = ArrayList<City>()
    }


    inner class City {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("name")
        var name: String? = null

        @SerializedName("viewsCount")
        var viewCount: Int = 0
    }

}
