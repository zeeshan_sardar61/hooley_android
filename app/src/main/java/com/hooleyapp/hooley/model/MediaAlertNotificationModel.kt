package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 4/10/2018.
 */

class MediaAlertNotificationModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("likeCommentNotification")
    var mediaAlertNotificationList = ArrayList<MediaNotification>()

    class MediaNotification {

        @SerializedName("userId")
        var userId: String? = null

        @SerializedName("id")
        var id: String? = null

        @SerializedName("bodyText")
        var bodyText: String? = null

        @SerializedName("eventId")
        var eventId: String? = null

        @SerializedName("profilePic")
        var profilePic: String? = null

        @SerializedName("imageId")
        var imageId: String? = null


        @SerializedName("postId")
        var postId: String? = null

        @SerializedName("sentTime")
        var sentTime: String? = null

        @SerializedName("type")
        var type: String? = null

        @SerializedName("isSeen")
        var isSeen: Boolean = false

    }

}
