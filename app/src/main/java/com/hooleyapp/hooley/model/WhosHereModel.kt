package com.hooleyapp.hooley.model

import android.os.Build
import android.support.annotation.RequiresApi
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 2/15/2018.
 */

class WhosHereModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("whosHere")
    var whosHereList = ArrayList<User>()

    class User {

        @SerializedName("userId")
        var userId: String? = null

        @SerializedName("profilePic")
        var profilePic: String? = null

        @SerializedName("fullName")
        var fullName: String? = null

        @SerializedName("homeTown")
        var homeTown: String? = null

        @SerializedName("gender")
        var gender: Int = 0

        @SerializedName("ageGroup")
        var ageGroup: String? = null

        @SerializedName("countryName")
        var countryName: String? = null

        @SerializedName("stateName")
        var stateName: String? = null

        @SerializedName("status")
        var status: String? = null

        @SerializedName("cityName")
        var cityName: String? = null

        @SerializedName("isSharedIntrest")
        var isSharedIntrest: Boolean = false

        @SerializedName("mintuesAgo")
        var minutesAgo: String? = null

        @SerializedName("userIntrestList")
        var userIntrestList: ArrayList<String>? = null

        @SerializedName("isMessageAccessible")
        @Expose
        var isMessageAccessible: Boolean = false

        @SerializedName("isFriendRequestAccessible")
        @Expose
        var isFriendRequestAccessible: Boolean = false


        companion object {

            //Getter and setter methods same as the above examples
            /*Comparator for sorting the list by Student Name*/
            var sortByNameComparator: Comparator<User> = Comparator { s1, s2 ->
                val s = s1.fullName!!.toUpperCase()
                val s3 = s2.fullName!!.toUpperCase()
                s.compareTo(s3)
            }

            /*Comparator for sorting the list by Student Name*/
            var sortByTimeComparator: Comparator<User> = Comparator { s1, s2 ->
                val s = s1.minutesAgo!!.toUpperCase()
                val s3 = s2.minutesAgo!!.toUpperCase()
                s.compareTo(s3)
            }
            /*Comparator for sorting the list by Student Name*/
            var sortByCityComparator: Comparator<User> = Comparator { s1, s2 ->
                val s = s1.cityName!!.toUpperCase()
                val s3 = s2.cityName!!.toUpperCase()
                s.compareTo(s3)
            }
            /*Comparator for sorting the list by roll no*/
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            var sortByShareInterest: Comparator<User> = Comparator { s1, s2 -> java.lang.Boolean.compare(s2.isSharedIntrest, s1.isSharedIntrest) }
        }

    }

}
