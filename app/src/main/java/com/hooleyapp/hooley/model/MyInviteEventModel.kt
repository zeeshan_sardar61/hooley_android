package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 4/23/2018.
 */

class MyInviteEventModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("inviteEvents")
    var inviteEvents = ArrayList<MyHostEventModel.HostEvents>()

}
