package com.hooleyapp.hooley.model.event

import com.google.gson.annotations.SerializedName

data class PastEventsItem(

	@field:SerializedName("eventId")
	val eventId: Int? = null,

	@field:SerializedName("Address")
	val address: String? = null,

	@field:SerializedName("coverPhoto")
	val coverPhoto: String? = null,

	@field:SerializedName("eventName")
	val eventName: String? = null,

	@field:SerializedName("startTime")
	val startTime: String? = null,

	@field:SerializedName("endTime")
	val endTime: String? = null,

	@field:SerializedName("invitGuestCount")
	val invitGuestCount: Int? = null
)