package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

class UserPreferencesModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null
    @SerializedName("blockCount")
    var blockCount: String? = null

    @SerializedName("userInfoPrivacy")
    var userInfoPrivacy: ArrayList<UserPreferencesModel.PrivacyCheckModel>? = null

    class PrivacyCheckModel {
        @SerializedName("name")
        var name: String? = null

        @SerializedName("privacyId")
        var privacyId: Integer? = null

        @SerializedName("isActive")
        var isActive: Boolean? = false

        @SerializedName("accessPrivacyId")
        var accessPrivacyId: Int = 0

    }

    @SerializedName("userAlertsPrivacy")
    var userAlertsPrivacy: ArrayList<UserPreferencesModel.PrivacyCheckModel>? = null


    @SerializedName("userAccessPrivacy")
    var userAccessPrivacy: ArrayList<UserPreferencesModel.UserAccessPrivacy>? = null

    class UserAccessPrivacy {
        @SerializedName("name")
        var name: String? = null

        @SerializedName("privacyId")
        var privacyId: Integer? = null

        @SerializedName("accessPrivacyName")
        var accessPrivacyName: String? = null

        @SerializedName("isActive")
        var isActive: Boolean? = true

        @SerializedName("accessPrivacyId")
        var accessPrivacyId: Integer? = null

    }


}
