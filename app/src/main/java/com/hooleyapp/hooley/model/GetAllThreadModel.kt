package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 4/24/2018.
 */

class GetAllThreadModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("threadInfo")
    var messageHistoryList = ArrayList<ThreadInfo>()


    class ThreadInfo {

        @SerializedName("threadId")
        var threadId: String? = null

        @SerializedName("fullName")
        var fullName: String? = null

        @SerializedName("lastMessage")
        var lastMessage: String? = null

        @SerializedName("profilePic")
        var profilePic: String? = null

        @SerializedName("friendId")
        var friendId: String? = null

        @SerializedName("jid")
        var jid: String? = null

        @SerializedName("lastMessageTime")
        var lastMessageTime: String? = null


        @SerializedName("isRead")
        var isRead: Boolean = false

    }

}