package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

/**
 * Created by Nauman on 3/6/2018.
 */

class PendingFriendRequestModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("pendingRequest")
    var friendsList = ArrayList<FriendsTable>()

}