package com.hooleyapp.hooley.model

/**
 * Created by Nauman on 12/27/2017.
 */

class GeoFenceModel {

    var placeLat: Double = 0.toDouble()
    var placeLong: Double = 0.toDouble()
    var raduis: String = ""
    var address: String = ""

}
