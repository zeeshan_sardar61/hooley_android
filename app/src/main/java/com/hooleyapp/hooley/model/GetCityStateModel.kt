package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 5/3/2018.
 */

class GetCityStateModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("statesWithCities")
    var statesWithCitiesList = ArrayList<StateCity>()

    class StateCity(@field:SerializedName("id")
                    var id: String, @field:SerializedName("name")
                    var name: String, city: ArrayList<City>) {

        var isChecked: Boolean = false

        @SerializedName("cities")
        var citiesList = ArrayList<City>()

        init {
            citiesList = city
        }
    }

    class City(@field:SerializedName("id")
               var id: String, @field:SerializedName("name")
               var name: String, var isChecked: Boolean)


}
