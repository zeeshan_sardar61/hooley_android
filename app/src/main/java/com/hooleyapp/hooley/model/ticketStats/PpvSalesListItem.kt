package com.hooleyapp.hooley.model.ticketStats


import com.google.gson.annotations.SerializedName


data class PpvSalesListItem(

        @field:SerializedName("sold")
        val sold: Int? = null,

        @field:SerializedName("revenue")
        val revenue: Int? = null,

        @field:SerializedName("price")
        val price: Int? = null
)