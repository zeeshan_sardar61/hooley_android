package com.hooleyapp.hooley.model.friends

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.hooleyapp.hooley.tables.FriendsTable

class HooleyUserModel {

    @field:SerializedName("Exception")
    @field:Expose
    var exception: String = ""

    @field:SerializedName("IsSuccessful")
    @field:Expose
    var success: Boolean = false

    @field:SerializedName("inviteList")
    @field:Expose
    var inviteList: ArrayList<FriendsTable> = ArrayList()

//    class User {
//
//        @field:SerializedName("userId")
//        @field:Expose
//        var userId: String? = null
//
//        @field:SerializedName("profilePic")
//        @field:Expose
//        var profilePic: String? = null
//
//        @field:SerializedName("fullName")
//        @field:Expose
//        var fullName: String? = null
//
//        @field:SerializedName("homeTown")
//        @field:Expose
//        var homeTown: String? = null
//
//        @field:SerializedName("status")
//        @field:Expose
//        var status: String? = null
//
//        @field:SerializedName("countryName")
//        @field:Expose
//        var countryName: String? = null
//
//        @field:SerializedName("stateName")
//        @field:Expose
//        var stateName: String? = null
//
//        @field:SerializedName("cityName")
//        @field:Expose
//        var cityName: String? = null
//
//
//        companion object {
//
//            //Getter and setter methods same as the above examples
//            /*Comparator for sorting the list by Student Name*/
//            var sortByNameComparator: java.util.Comparator<User> = Comparator { s1, s2 ->
//                val StudentName1 = s1.fullName!!.toUpperCase()
//                val StudentName2 = s2.fullName!!.toUpperCase()
//                StudentName1.compareTo(StudentName2)
//            }
//
//        }
//
//    }


}