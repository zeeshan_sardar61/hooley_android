package com.hooleyapp.hooley.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CardDetailsModel {

    @SerializedName("last4")
    @Expose
    internal var last4: String? = null
    @SerializedName("name")
    @Expose
    internal var name: String? = null
    @SerializedName("expMonth")
    @Expose
    internal var expMonth: String? = null
    @SerializedName("expYear")
    @Expose
    internal var expYear: String? = null
    @SerializedName("brand")
    @Expose
    internal var brand: String? = null
    @SerializedName("IsSuccessful")
    @Expose
    internal var isSuccessful: Boolean? = null
    @SerializedName("Exception")
    @Expose
    internal var exception: String? = null
}
