package com.hooleyapp.hooley.model

import android.net.Uri

class ItemPostMedia(var mediaUri: Uri, var isVideo: Boolean)
