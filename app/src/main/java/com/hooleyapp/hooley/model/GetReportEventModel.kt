package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 1/24/2018.
 */

class GetReportEventModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null


    @SerializedName("eventReportReasons")
    var reportReasonsArrayList = ArrayList<PersonalProfileModel.myObject>()


    @SerializedName("getMediaReports")
    var mediaReportReasonsArrayList = ArrayList<PersonalProfileModel.myObject>()


}

