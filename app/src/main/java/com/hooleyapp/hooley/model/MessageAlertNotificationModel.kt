package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 4/11/2018.
 */

class MessageAlertNotificationModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("messagesNotifications")
    var mlist = ArrayList<MessageNotification>()

    class MessageNotification {

        @SerializedName("friendId")
        var friendId: String? = null

        @SerializedName("fullName")
        var fullName: String? = null

        @SerializedName("bodyText")
        var bodyText: String? = null

        @SerializedName("sentTime")
        var sentTime: String? = null

        @SerializedName("profilePic")
        var profilePic: String? = null

        @SerializedName("isSeen")
        var isSeen: Boolean = false

    }

}
