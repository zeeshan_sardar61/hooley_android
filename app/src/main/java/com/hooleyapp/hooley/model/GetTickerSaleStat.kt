package com.hooleyapp.hooley.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GetTickerSaleStat {

    @SerializedName("latestSaleStats")
    @Expose
    var latestSaleStats: List<LatestSaleStat>? = null
    @SerializedName("latestScanStats")
    @Expose
    var latestScanStats: List<LatestScanStat>? = null
    @SerializedName("IsSuccessful")
    @Expose
    var isSuccessful: Boolean? = null
    @SerializedName("Exception")
    @Expose
    var exception: String = ""

    /**
     * No args constructor for use in serialization
     *
     */
    constructor()

    /**
     *
     * @param latestScanStats
     * @param isSuccessful
     * @param latestSaleStats
     * @param exception
     */
    constructor(latestSaleStats: List<LatestSaleStat>, latestScanStats: List<LatestScanStat>, isSuccessful: Boolean?, exception: String) : super() {
        this.latestSaleStats = latestSaleStats
        this.latestScanStats = latestScanStats
        this.isSuccessful = isSuccessful
        this.exception = exception
    }

    inner class LatestSaleStat {

        @SerializedName("ticketCategory")
        @Expose
        var ticketCategory: String = ""
        @SerializedName("total")
        @Expose
        var total: String = ""
        @SerializedName("sold")
        @Expose
        var sold: String = ""
        @SerializedName("available")
        @Expose
        var available: String = ""

        /**
         * No args constructor for use in serialization
         *
         */
        constructor()

        /**
         *
         * @param total
         * @param ticketCategory
         * @param available
         * @param sold
         */
        constructor(ticketCategory: String, total: String, sold: String, available: String) : super() {
            this.ticketCategory = ticketCategory
            this.total = total
            this.sold = sold
            this.available = available
        }

    }

    inner class LatestScanStat {

        @SerializedName("ticketCategory")
        @Expose
        var ticketCategory: String = ""
        @SerializedName("sold")
        @Expose
        var sold: String = ""
        @SerializedName("scan")
        @Expose
        var scan: String = ""
        @SerializedName("left")
        @Expose
        var left: String = ""

        /**
         * No args constructor for use in serialization
         *
         */
        constructor()

        /**
         *
         * @param ticketCategory
         * @param left
         * @param scan
         * @param sold
         */
        constructor(ticketCategory: String, sold: String, scan: String, left: String) : super() {
            this.ticketCategory = ticketCategory
            this.sold = sold
            this.scan = scan
            this.left = left
        }

    }

}
