package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Zeeshan on 15-Jan-18.
 */

class PersonalGeofenceModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("myGeofence")
    var myGeofence = ArrayList<PersonalProfileModel.GeoFence>()

    inner class GeoFence {

        @SerializedName("id")
        var userId: String? = null

        @SerializedName("latitude")
        var latitude: String? = null

        @SerializedName("longitude")
        var longitude: String? = null

        @SerializedName("radius")
        var radius: String? = null

        @SerializedName("addressLocation")
        var addressLocation: String? = null

    }

}
