package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 4/10/2018.
 */

class EventInviteNotificationModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("eventInvites")
    var eventInvitesList = ArrayList<EventInvite>()

    class EventInvite {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("bodyText")
        var bodyText: String? = null

        @SerializedName("eventId")
        var eventId: String? = null

        @SerializedName("eventStartTime")
        var eventStartTime: String? = null

        @SerializedName("eventEndTime")
        var eventEndTime: String? = null

        @SerializedName("coverPhoto")
        var coverPhoto: String? = null

        @SerializedName("sentTime")
        var sentTime: String? = null

        @SerializedName("peopleCount")
        var peopleCount: String? = null

        @SerializedName("isSeen")
        var isSeen: Boolean = false

        @SerializedName("isDisable")
        var isDisable: Boolean = false

    }
}
