package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

/**
 * Created by Nauman on 5/18/2018.
 */

class GetPastEventGuestListModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("guestList")
    var guestList = ArrayList<FriendsTable>()


    class GuestList {

        @SerializedName("userId")
        var userId: Int = 0

        @SerializedName("fullName")
        var fullName: String? = null

        @SerializedName("homeTown")
        var homeTown: String? = null

        @SerializedName("status")
        var status: String? = null

        @SerializedName("profilePic")
        var profilePic: String? = null

        var isChecked: Boolean = false


    }

}
