package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Zeeshan on 29-Dec-17.
 */

class EventInviteNearByMeModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("userList")
    var userNearMeList = ArrayList<UserNearMe>()

    inner class UserNearMe {

        @SerializedName("userId")
        var userId: String = ""

        @SerializedName("fullName")
        var fullName: String? = null


        @SerializedName("latitude")
        var latitude: Double? = null

        @SerializedName("longitude")
        var longitude: Double? = null


        @SerializedName("homeTown")
        var homeTown: String? = null

        @SerializedName("profilePic")
        var profilePicUrl: String? = null


        @SerializedName("distance")
        var distance: Double? = null

        var isChecked: Boolean = false
    }

}
