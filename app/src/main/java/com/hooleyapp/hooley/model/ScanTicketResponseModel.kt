package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName

class ScanTicketResponseModel {


    @SerializedName("status")
    var status: String? = null

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null


}