package com.hooleyapp.hooley.model.staticData

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.hooleyapp.hooley.model.PersonalProfileModel

class StaticDataModel {

    @SerializedName("politicalViewsList")
    @Expose
    var politicalViewsList: ArrayList<PersonalProfileModel.myObject>? = null
    @SerializedName("religionList")
    @Expose
    var religionList: ArrayList<PersonalProfileModel.myObject>? = null
    @SerializedName("languagesList")
    @Expose
    var languagesList: ArrayList<PersonalProfileModel.myObject>? = null
    @SerializedName("ethnicBackgroundList")
    @Expose
    var ethnicBackgroundList: ArrayList<PersonalProfileModel.myObject>? = null
    @SerializedName("astrologicalSigns")
    @Expose
    var astrologicalSigns: ArrayList<PersonalProfileModel.myObject>? = null
    @SerializedName("collageMajorList")
    @Expose
    var collageMajorList: ArrayList<PersonalProfileModel.myObject>? = null
    @SerializedName("userOccupation")
    @Expose
    var userOccupation: ArrayList<PersonalProfileModel.myObject>? = null
    @SerializedName("paymentMethodList")
    @Expose
    var paymentMethodList: List<PersonalProfileModel.myObject>? = null
    @SerializedName("industryList")
    @Expose
    var industryList: ArrayList<PersonalProfileModel.myObject>? = null
    @SerializedName("companyTypeList")
    @Expose
    var companyTypeList: ArrayList<PersonalProfileModel.myObject>? = null
    @SerializedName("userOrganizationList")
    @Expose
    var userOrganizationList: ArrayList<PersonalProfileModel.myObject>? = null
    @SerializedName("minimumAgeList")
    @Expose
    var minimumAgeList: ArrayList<PersonalProfileModel.myObject>? = null
    @SerializedName("categories")
    @Expose
    var categories: List<Category>? = null
    @SerializedName("relationshipList")
    @Expose
    var relationshipList: ArrayList<PersonalProfileModel.myObject>? = null
    @SerializedName("IsSuccessful")
    @Expose
    var isSuccessful: Boolean? = null
    @SerializedName("Exception")
    @Expose
    var exception: String? = null

}
