package com.hooleyapp.hooley.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 5/7/2018.
 */

class SendGiftModel {

    @SerializedName("giftedTicketsList")
    @Expose
    var giftedTicketsList: ArrayList<MyTicketsList>? = null
    @SerializedName("IsSuccessful")
    @Expose
    var isSuccessful: Boolean? = null
    @SerializedName("Exception")
    @Expose
    var exception: String = ""

    /**
     * No args constructor for use in serialization
     */
    constructor()

    /**
     * @param isSuccessful
     * @param exception
     * @param myTicketsList
     */
    constructor(myTicketsList: ArrayList<MyTicketsList>, isSuccessful: Boolean?, exception: String) : super() {
        this.giftedTicketsList = myTicketsList
        this.isSuccessful = isSuccessful
        this.exception = exception
    }

    inner class MyTicketsList {

        @SerializedName("eventId")
        @Expose
        var eventId: Int? = null
        @SerializedName("coverPhoto")
        @Expose
        var coverPhoto: String = ""
        @SerializedName("eventName")
        @Expose
        var eventName: String = ""
        @SerializedName("ticketCategoryName")
        @Expose
        var ticketCategoryName: String = ""
        @SerializedName("eventAddress")
        @Expose
        var eventAddress: String = ""
        @SerializedName("latitude")
        @Expose
        var latitude: Int? = null
        @SerializedName("longitude")
        @Expose
        var longitude: Int? = null
        @SerializedName("startTime")
        @Expose
        var startTime: String = ""
        @SerializedName("endTime")
        @Expose
        var endTime: String = ""
        @SerializedName("alertOn")
        @Expose
        var alertOn: Boolean? = null
        @SerializedName("ticketList")
        @Expose
        var ticketList: ArrayList<TicketList>? = null
        @SerializedName("alertDate")
        @Expose
        var alertDate: String = ""
        @SerializedName("giftedFrom")
        @Expose
        var giftedFrom: String = ""
        @SerializedName("isGifted")
        @Expose
        var isGifted: Boolean? = null

        /**
         * No args constructor for use in serialization
         */
        constructor()

        /**
         * @param startTime
         * @param giftedFrom
         * @param eventAddress
         * @param eventId
         * @param ticketList
         * @param coverPhoto
         * @param isGifted
         * @param longitude
         * @param ticketCategoryName
         * @param eventName
         * @param latitude
         * @param endTime
         * @param alertOn
         * @param alertDate
         */
        constructor(eventId: Int?, coverPhoto: String, eventName: String, ticketCategoryName: String, eventAddress: String, latitude: Int?, longitude: Int?, startTime: String, endTime: String, alertOn: Boolean?, ticketList: ArrayList<TicketList>, alertDate: String, giftedFrom: String, isGifted: Boolean?) : super() {
            this.eventId = eventId
            this.coverPhoto = coverPhoto
            this.eventName = eventName
            this.ticketCategoryName = ticketCategoryName
            this.eventAddress = eventAddress
            this.latitude = latitude
            this.longitude = longitude
            this.startTime = startTime
            this.endTime = endTime
            this.alertOn = alertOn
            this.ticketList = ticketList
            this.alertDate = alertDate
            this.giftedFrom = giftedFrom
            this.isGifted = isGifted
        }

    }

    inner class TicketList {

        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("qrCodeUrl")
        @Expose
        var qrCodeUrl: String = ""
        @SerializedName("isValid")
        @Expose
        var isValid: Boolean? = null

        /**
         * No args constructor for use in serialization
         */
        constructor()

        /**
         * @param id
         * @param qrCodeUrl
         * @param isValid
         */
        constructor(id: Int?, qrCodeUrl: String, isValid: Boolean?) : super() {
            this.id = id
            this.qrCodeUrl = qrCodeUrl
            this.isValid = isValid
        }

    }

}