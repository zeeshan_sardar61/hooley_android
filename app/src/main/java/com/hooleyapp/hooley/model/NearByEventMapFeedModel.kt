package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Zeeshan on 20-Feb-18.
 */

class NearByEventMapFeedModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("paidEvents")
    var paidEventsList = ArrayList<NearByEventListFeedModel.EventDetail>()

    @SerializedName("freeEvents")
    var freeEventsList = ArrayList<NearByEventListFeedModel.EventDetail>()

}
