package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 12/19/2017.
 */

class CreateEventModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("categories")
    var categoriesList = ArrayList<Category>()


    class Category {

        @SerializedName("id")
        var id: String = ""

        @SerializedName("name")
        var name: String = ""

        @SerializedName("icon_url")
        var iconUrl: String = ""

        var isActive = false

    }


}
