package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName

class GetUserProfilesModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("generalProfile")
    var generalProfile: PersonalProfileModel? = null

    @SerializedName("businessProfile")
    var businessProfile: BusinessProfileModel? = null


}
