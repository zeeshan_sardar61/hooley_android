package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 4/4/2018.
 */

class SearchEventModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("featuredEvents")
    var featuredEventsList = ArrayList<NearByEventListFeedModel.EventDetail>()

    @SerializedName("todayEvents")
    var todayEventsList = ArrayList<NearByEventListFeedModel.EventDetail>()

    @SerializedName("nextWeekEvents")
    var nextWeekEventsList = ArrayList<NearByEventListFeedModel.EventDetail>()

    @SerializedName("commingEvents")
    var commingEventsList = ArrayList<NearByEventListFeedModel.EventDetail>()

    @SerializedName("pastEvents")
    var pastEventsList = ArrayList<NearByEventListFeedModel.EventDetail>()


}