package com.hooleyapp.hooley.model.friends

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.hooleyapp.hooley.tables.FriendsTable

class GetFriendOfFriendModel {

    @field:SerializedName("Exception")
    @field:Expose
    var exception: String = ""

    @field:SerializedName("IsSuccessful")
    @field:Expose
    var success: Boolean = false

    @field:SerializedName("guestList")
    @field:Expose
    var guestList: ArrayList<FriendsTable> = ArrayList()

}