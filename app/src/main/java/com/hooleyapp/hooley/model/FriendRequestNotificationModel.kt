package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 4/10/2018.
 */

class FriendRequestNotificationModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("friendRequestNotify")
    var mlist = ArrayList<FriendRequest>()

    class FriendRequest {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("friendRequestId")
        var FriendRequestId: Int = 0

        @SerializedName("senderId")
        var SenderId: Int = 0

        @SerializedName("bodyText")
        var bodyText: String? = null

        @SerializedName("sentTime")
        var sentTime: String? = null

        @SerializedName("fullName")
        var fullName: String? = null

        @SerializedName("homeTown")
        var homeTown: String? = null

        @SerializedName("ageGroup")
        var ageGroup: String? = null

        @SerializedName("imJid")
        var imJid: String? = null

        @SerializedName("isFriend")
        var isFriend: Boolean = false

        @SerializedName("profilePic")
        var profilePic: String? = null


        @SerializedName("status")
        var status: String? = null

        @SerializedName("isSeen")
        var isSeen: Boolean = false

    }

}
