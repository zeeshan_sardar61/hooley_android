package com.hooleyapp.hooley.model.ticketStats


import com.google.gson.annotations.SerializedName


data class IncomeShareUsersItem(

        @field:SerializedName("profilePic")
        val profilePic: String? = null,

        @field:SerializedName("percentage")
        val percentage: Int? = null,

        @field:SerializedName("fullName")
        val fullName: String? = null,

        @field:SerializedName("userId")
        val userId: Int? = null


)