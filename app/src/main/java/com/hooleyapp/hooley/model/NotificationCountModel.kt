package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Nauman on 4/12/2018.
 */

class NotificationCountModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("hooleyVersion")
    var hooleyVersion = HooleyVersion()

    @SerializedName("count")
    var count = Count()

    inner class Count {
        @SerializedName("eventInvitesCount")
        var eventInvitesCount: Int = 0

        @SerializedName("messagesCount")
        var messagesCount: Int = 0

        @SerializedName("mediaAlertsCount")
        var mediaAlertsCount: Int = 0

        @SerializedName("friendRequestCount")
        var friendRequestCount: Int = 0
    }

    inner class HooleyVersion {
        @SerializedName("versionName")
        var versionName: String? = null

        @SerializedName("versionCode")
        var versionCode: String? = null

    }

}