package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName

/**
 * Created by hp on 6/19/2017.
 */

class GeneralModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

}