package com.hooleyapp.hooley.model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 3/13/2018.
 */

class GetChatStreamModel {


    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("chatStream")
    var chatStreamList = ArrayList<ChatStream>()

    class ChatStream {

        @SerializedName("senderId")
        var senderId: String? = null

        @SerializedName("profilePic")
        var profilePic: String? = null

        @SerializedName("fullName")
        var fullName: String? = null

        @SerializedName("messageText")
        var messageText: String? = null

        @SerializedName("messageTime")
        var messageTime: String? = null

    }

}

