package com.hooleyapp.hooley.model.staticData

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserOrganizationList {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("name")
    @Expose
    var name: String? = null

}
