package com.hooleyapp.hooley

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import android.util.Log
import com.crashlytics.android.Crashlytics
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.iid.FirebaseInstanceId
import com.hooleyapp.hooley.apis.ApiClient
import com.hooleyapp.hooley.apis.ApiInterface
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.TinyDB
import com.hooleyapp.hooley.others.Util
import com.raizlabs.android.dbflow.config.FlowManager
import io.fabric.sdk.android.Fabric


/**
 * Created by Nauman on 11/28/2017.
 */

class HooleyApp : MultiDexApplication() {
    lateinit var appSharedPrefs: SharedPreferences
    lateinit var prefsEditor: SharedPreferences.Editor
    var SHARED_NAME = "com.hooleyapp.hooley"
    private lateinit var auth: FirebaseAuth

    override fun onCreate() {
        super.onCreate()
        instance = this
        Fabric.with(this, Crashlytics())
        auth = FirebaseAuth.getInstance()
        HooleyApp.context = applicationContext
        initPreferences()
        configRetrofit()
        Log.v(TAG, "public void onCreate()")
        FlowManager.init(context)
        Fresco.initialize(this)
        //        FirebaseApp.initializeApp(context);
        //        Realm.init(context);
        //        DbManager.getInstance().getRealmInstance();

        FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Log.w(TAG, "getInstanceId failed", task.exception)
                        return@OnCompleteListener
                    }

                    // Get new Instance ID token
                    val token = task.result?.token
                    // Log and toast
                    Log.d(TAG, "FCM TOKEN $token")
                    HooleyApp.db.putString(Constants.GCM_REGISTRATION_TOKEN, token.toString())
                })

    }

    fun initPreferences() {
        this.appSharedPrefs = getSharedPreferences(SHARED_NAME,
                Activity.MODE_PRIVATE)
        this.prefsEditor = appSharedPrefs.edit()
        db = TinyDB(applicationContext)
    }

    /**
     *
     */
    override fun onLowMemory() {
        super.onLowMemory()
        Log.i(TAG, "Freeing memory ...")
    }

    /**
     *
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        if (Util.LOG_ENABLED) {
            Log.v(TAG, "public void onTrimMemory (int level)")
        }
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)

    }

    companion object {
        lateinit var db: TinyDB
        var instance: HooleyApp? = null
            private set
        lateinit var context: Context
        private val TAG = HooleyApp::class.java.name
        lateinit var apiService: ApiInterface

        fun configRetrofit() {
            apiService = ApiClient.getClient().create(ApiInterface::class.java)
        }
    }


}
