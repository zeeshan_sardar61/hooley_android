package com.hooleyapp.hooley.apis

import android.text.TextUtils
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.*
import com.hooleyapp.hooley.model.staticData.Category
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.requestmodel.*
import com.hooleyapp.hooley.tables.FriendsTable

/**
 * Created by Nauman on 11/22/2017.
 */

class ApiRequestJson {


    /**
     * @param eventId
     * @param caption
     * @param list
     * @return
     */
    fun uploadEventMediaDataString(eventId: String, caption: String, list: ArrayList<FriendsTable>): String {
        val userID = HooleyApp.db.getString(Constants.USER_ID)
        val stringBuilder = StringBuilder()
        stringBuilder.append("{\"userId\":\"$userID\",")
        stringBuilder.append("\"eventId\":\"$eventId\",")
        stringBuilder.append("\"caption\":\"$caption\",")
        stringBuilder.append("\"userTagsList\":[")
        for (i in list.indices) {
            stringBuilder.append("{\"userId\":\"" + list[i].getUserId() + "\"},")
        }
        if (stringBuilder[stringBuilder.length - 1] == ',')
            stringBuilder.deleteCharAt(stringBuilder.length - 1)
        stringBuilder.append("]}")
        Logger.ex("uploadEventMediaDataString$stringBuilder")
        return stringBuilder.toString()
    }

    /**
     * @param mediaUri
     * @param eventName
     * @param eventDetail
     * @param eventStartTime
     * @param eventEndTime
     * @param isPublic
     * @param isPrivate
     * @param isPaid
     * @param isFree
     * @param inviteFriends
     * @param saveAsUnpublished
     * @param optionalCat
     * @param hstList
     * @param coHstList
     * @param catList
     * @param geofenceList
     * @return
     */
    fun saveEventDataString(mediaUri: ArrayList<ItemCreateEventWallpaper>,
                            eventId: String,
                            eventName: String,
                            eventType: String,
                            eventDetail: String,
                            eventHashs: ArrayList<String>,
                            eventStartTime: String,
                            eventEndTime: String,
                            isPublic: Boolean,
                            isPublish: Boolean,
                            optionalCat: ArrayList<String>?,
                            coHstList: ArrayList<FriendsTable>?,
                            catList: ArrayList<Category>,
                            geofenceList: ArrayList<GeoFenceModel>,
                            ageId: String): String {

        val userID = HooleyApp.db.getString(Constants.USER_ID)
        val CoHostList = ArrayList<String>()
        val CategoryList = ArrayList<String>()
        val OptionalCategoryList = ArrayList<String>()
        val eventCoverArrayList = ArrayList<UpdateEventRequestModel.EventCover>()
        val geofenceArrayList = ArrayList<UpdateEventRequestModel.Geofence>()
        if (coHstList != null) {
            for (i in coHstList.indices) {
                CoHostList.add(coHstList[i].getUserId().toString())
            }
        }

        for (i in catList.indices) {
            if (catList[i].isActive)
                CategoryList.add(catList[i].id.toString())
        }
        if (optionalCat != null) {
            for (i in optionalCat.indices) {
                OptionalCategoryList.add(optionalCat[i])
            }
        }


        for (i in geofenceList.indices) {
            geofenceArrayList.add(UpdateEventRequestModel.Geofence(geofenceList[i].placeLat.toString(),
                    geofenceList[i].placeLong.toString(), geofenceList[i].raduis, geofenceList[i].address))
        }

        for (i in mediaUri.indices) {
            if (mediaUri[i].type == Constants.TYPE_URL) {
                eventCoverArrayList.add(UpdateEventRequestModel.EventCover(mediaUri[i].url))
            }
        }
        val eventAttribute = UpdateEventRequestModel(userID, eventId, eventName, eventType, eventDetail, eventHashs,
                DateUtils.SendDateToDate(DateUtils.getDateInUtc(eventStartTime)), DateUtils.SendDateToDate(DateUtils.getDateInUtc(eventEndTime)), isPublic,
                isPublish, CoHostList,
                CategoryList, OptionalCategoryList, eventCoverArrayList, geofenceArrayList, ageId)
        //03035408639
        Logger.ex("Create Event" + Gson().toJson(eventAttribute))

        return Gson().toJson(eventAttribute)
    }

    /**
     * @param mediaUri
     * @param eventId
     * @param editEventModel
     * @param isPublish
     * @return
     */
    fun updateEventString(mediaUri: ArrayList<ItemCreateEventWallpaper>, eventId: String, editEventModel: EditEventModel, isPublish: Boolean): String {
        val userID = HooleyApp.db.getString(Constants.USER_ID)
        val CoHostList = ArrayList<String>()
        val CategoryList = ArrayList<String>()
        val OptionalCategoryList = ArrayList<String>()
        val eventCoverArrayList = ArrayList<UpdateEventRequestModel.EventCover>()
        val geofenceArrayList = ArrayList<UpdateEventRequestModel.Geofence>()
        if (editEventModel.eventCoHostProfileList != null) {
            for (i in editEventModel.eventCoHostProfileList.indices) {
                CoHostList.add(editEventModel.eventCoHostProfileList[i].userId.toString())
            }
        }

        for (i in editEventModel.eventCategoryList.indices) {
            if (editEventModel.eventCategoryList[i].isAdded)
                CategoryList.add(editEventModel.eventCategoryList[i].Id!!)
        }
        if (editEventModel.eventOptionalCategoryList != null) {
            for (i in editEventModel.eventOptionalCategoryList.indices) {
                OptionalCategoryList.add(editEventModel.eventOptionalCategoryList[i].categoryName!!)
            }
        }


        for (i in editEventModel.eventGeofenceList.indices) {
            geofenceArrayList.add(UpdateEventRequestModel.Geofence(editEventModel.eventGeofenceList[i].latitude!!,
                    editEventModel.eventGeofenceList[i].longitude!!, editEventModel.eventGeofenceList[i].radius!!, editEventModel.eventGeofenceList[i].address!!))
        }

        for (i in mediaUri.indices) {
            if (mediaUri[i].type == Constants.TYPE_URL) {
                eventCoverArrayList.add(UpdateEventRequestModel.EventCover(mediaUri[i].url))
            }
        }

        val requestModel = UpdateEventRequestModel(userID,
                eventId,
                editEventModel.eventBasicInfo!!.eventName!!,
                editEventModel.eventBasicInfo!!.eventType!!,
                editEventModel.eventBasicInfo!!.eventDetails!!,
                editEventModel.eventBasicInfo!!.eventHashs,
                editEventModel.eventBasicInfo!!.eventStartTime!!,
                editEventModel.eventBasicInfo!!.eventEndTime!!,
                editEventModel.eventBasicInfo!!.isPrivate,
                isPublish,
                CoHostList,
                CategoryList, OptionalCategoryList, eventCoverArrayList, geofenceArrayList, editEventModel.eventBasicInfo!!.minimumAgeId!!)
        return Gson().toJson(requestModel)
    }

    /**
     * @param profilePic
     * @param firstName
     * @param lastName
     * @param aboutMe
     * @param countryCode
     * @param countryISO
     * @param phoneNumber
     * @param homeTown
     * @param email
     * @param website
     * @param genderId
     * @param birthday
     * @param relationShipId
     * @param religionId
     * @param hobbies
     * @param college
     * @param highschool
     * @param collegeMajor
     * @param astrologicalSignId
     * @param policticalViewId
     * @param languageId
     * @param ethinicBackground
     * @param employer
     * @param organization
     * @param occupationId
     * @param cityId
     * @param countryId
     * @param stateId
     * @return
     */
    fun updatePersonalProfile(profilePic: String, firstName: String, lastName: String, aboutMe: String, countryCode: String, countryISO: String, phoneNumber: String, homeTown: String, email: String, website: String, genderId: String,
                              birthday: String, relationShipId: String, religionId: String, hobbies: String, college: String, highschool: String, collegeMajor: String, astrologicalSignId: String,
                              policticalViewId: String, languageId: String, ethinicBackground: String, employer: String,
                              organization: String, occupationId: String, cityId: String, countryId: String, stateId: String): String {
        var birthday = birthday

        if (TextUtils.isEmpty(birthday))
            birthday = "05-12-2010"
        val requestModel = UpdatePersonalProfileRequestModel(HooleyApp.db.getString(Constants.USER_ID),
                profilePic, firstName, lastName, aboutMe, phoneNumber, homeTown, email, website, genderId, birthday, relationShipId, religionId, hobbies, college, policticalViewId, languageId, ethinicBackground, astrologicalSignId, collegeMajor, highschool, cityId, employer, organization, occupationId, countryCode, countryISO, countryId, stateId)
        return Gson().toJson(requestModel)
    }

    fun updatePostMedia(eventId: String, postId: String, caption: String, tagUserList: ArrayList<UpdatePostMediaRequestModel.TagUserItem>, mediaArrayList: ArrayList<UpdatePostMediaItem>): String {
        var mediaList: ArrayList<UpdatePostMediaRequestModel.ImageUrlItem> = arrayListOf()
        if (mediaArrayList != null && mediaArrayList.size > 0) {
            for (i in 0 until mediaArrayList.size) {
                if (mediaArrayList[i].type == Constants.TYPE_URL)
                    mediaList.add(UpdatePostMediaRequestModel.ImageUrlItem(mediaArrayList[i].mediaUrl, mediaArrayList[i].isVideo))
            }
        }
        val requestModel = UpdatePostMediaRequestModel(HooleyApp.db.getString(Constants.USER_ID).toInt(), eventId, postId, caption, mediaList, tagUserList)
        return Gson().toJson(requestModel)
    }

    /**
     * @param profilePic
     * @param businessName
     * @param aboutBusiness
     * @param countryCode
     * @param countryISO
     * @param businessPhone
     * @param isPhoneVerified
     * @param businessEmail
     * @param businessWebsite
     * @param contactName
     * @param companyTypeId
     * @param industryId
     * @param address
     * @param cityId
     * @param stateId
     * @param countryId
     * @param zipCode
     * @param socialList
     * @return
     */
    fun updateBusinessProfile(profilePic: String, businessName: String, aboutBusiness: String, countryCode: String, countryISO: String, businessPhone: String, isPhoneVerified: Boolean, businessEmail: String, businessWebsite: String,
                              contactName: String, companyTypeId: String, industryId: String, address: String, cityId: String, stateId: String, countryId: String, zipCode: String, socialList: ArrayList<BusinessProfileModel.SocialMedia>): String {
        val userSocials = ArrayList<UpdateBusinessProfileRequestModel.UserSocial>()
        for (i in socialList.indices) {
            userSocials.add(UpdateBusinessProfileRequestModel.UserSocial(socialList[i].id, HooleyApp.db.getString(Constants.USER_ID), socialList[i].socialUrl!!, socialList[i].socialName!!))
        }
        val requestModel = UpdateBusinessProfileRequestModel(HooleyApp.db.getString(Constants.USER_ID), businessName, profilePic, aboutBusiness, businessPhone, businessEmail, businessWebsite, contactName, companyTypeId, industryId, address, cityId, stateId, zipCode, countryId, countryCode, countryISO, userSocials)
        return Gson().toJson(requestModel)
    }


    /**
     * @return
     */

    fun getUserNearByMeMapViewPolygonString(centerLat: Double, centerLong: Double, geofenceList: ArrayList<LatLng>): String {
        val userID = HooleyApp.db.getString(Constants.USER_ID)
        val stringBuilder = StringBuilder()
        stringBuilder.append("{\"userId\":\"$userID\",")
        stringBuilder.append("\"centriod\":")
        stringBuilder.append("{\"latitude\":\"$centerLat\",")
        stringBuilder.append("\"longitude\":\"$centerLong\"},")
        stringBuilder.append("\"geoFenceList\":[")
        for (i in geofenceList.indices) {
            stringBuilder.append("{\"latitude\":\"" + geofenceList[i].latitude + "\",")
            stringBuilder.append("\"longitude\":\"" + geofenceList[i].longitude + "\"}")
            if (i != geofenceList.size - 1)
                stringBuilder.append(",")
        }
        stringBuilder.append("]}")
        Logger.ex("getUserNearByMeMapViewPolygonString$stringBuilder")
        return stringBuilder.toString()
    }


    /**
     * @param countryId
     * @param phoneNo
     * @param birthday
     * @param countryISO
     * @param cityId
     * @param stateId
     * @param countryCode
     */
    fun profileSetup(birthday: String, phoneNo: String, countryCode: String, countryISO: String, countryId: String, stateId: String, cityId: String, msisdn: String, gender: Int): String {
        val requestModel = SetupProfileRequestModel(HooleyApp.db.getString(Constants.USER_ID), birthday, phoneNo, countryCode, countryISO, countryId, stateId, cityId, msisdn, gender)
        return Gson().toJson(requestModel)
    }

    companion object {

        private var instance: ApiRequestJson? = null

        fun getInstance(): ApiRequestJson {
            if (instance == null)
                instance = ApiRequestJson()
            return instance!!
        }
    }


}
