package com.hooleyapp.hooley.apis;


import com.hooleyapp.hooley.app.data.model.Friend.BlockUserListModel;
import com.hooleyapp.hooley.app.data.model.event.requestmodel.EditTicketRequest;
import com.hooleyapp.hooley.app.data.model.event.requestmodel.EnableDisablePPV;
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventAnnouncementModel;
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventDetailModel;
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventGuestListModel;
import com.hooleyapp.hooley.app.data.model.event.responseModel.GetEditTicketResponse;
import com.hooleyapp.hooley.app.data.model.event.responseModel.PopularInFriendsModel;
import com.hooleyapp.hooley.app.data.model.guestuser.GuestUserLoginModel;
import com.hooleyapp.hooley.app.data.model.guestuser.GuestUserLoginRequestModel;
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel;
import com.hooleyapp.hooley.app.data.model.media.MediaStreamDetailModel;
import com.hooleyapp.hooley.app.data.model.media.MediaStreamEventListModel;
import com.hooleyapp.hooley.app.data.model.media.MediaStreamTrendingModel;
import com.hooleyapp.hooley.app.data.model.media.MyMediaAlbumModel;
import com.hooleyapp.hooley.app.data.model.messages.GetAllMessageModel;
import com.hooleyapp.hooley.model.AddSearchHistoryRequestModel;
import com.hooleyapp.hooley.model.BookMarkSearchModel;
import com.hooleyapp.hooley.model.BookmarkEventModel;
import com.hooleyapp.hooley.model.CardDetailsModel;
import com.hooleyapp.hooley.model.ContactFriendListModel;
import com.hooleyapp.hooley.model.CountryStateCitiesModel;
import com.hooleyapp.hooley.model.CreateEventModel;
import com.hooleyapp.hooley.model.CreateThreadModel;
import com.hooleyapp.hooley.model.EditEventModel;
import com.hooleyapp.hooley.model.EventInviteNearByMeModel;
import com.hooleyapp.hooley.model.EventInviteNotificationModel;
import com.hooleyapp.hooley.model.EventPromotionStatsModel;
import com.hooleyapp.hooley.model.FriendProfileModel;
import com.hooleyapp.hooley.model.FriendRequestNotificationModel;
import com.hooleyapp.hooley.model.FriendsModel;
import com.hooleyapp.hooley.model.GeneralModel;
import com.hooleyapp.hooley.model.GetAllThreadModel;
import com.hooleyapp.hooley.model.GetAppVersionResponseModel;
import com.hooleyapp.hooley.model.GetChatStreamModel;
import com.hooleyapp.hooley.model.GetCityStateModel;
import com.hooleyapp.hooley.model.GetEventPromotionModel;
import com.hooleyapp.hooley.model.GetInviteContactModel;
import com.hooleyapp.hooley.model.GetPastEventGuestListModel;
import com.hooleyapp.hooley.model.GetReportEventModel;
import com.hooleyapp.hooley.model.GetTermsConditionModel;
import com.hooleyapp.hooley.model.GetTicketByEventModel;
import com.hooleyapp.hooley.model.GetUpdateEventActionModel;
import com.hooleyapp.hooley.model.GetUserContactCardResponseModel;
import com.hooleyapp.hooley.model.GetUserProfilesModel;
import com.hooleyapp.hooley.model.GetUserSearchesHistory;
import com.hooleyapp.hooley.model.HooleyGalleryModel;
import com.hooleyapp.hooley.model.InstaProfileModel;
import com.hooleyapp.hooley.model.LoginModel;
import com.hooleyapp.hooley.model.MediaAlertNotificationModel;
import com.hooleyapp.hooley.model.MessageAlertNotificationModel;
import com.hooleyapp.hooley.model.MyAttendingEventModel;
import com.hooleyapp.hooley.model.MyFollowingEventModel;
import com.hooleyapp.hooley.model.MyHostEventModel;
import com.hooleyapp.hooley.model.MyInviteEventModel;
import com.hooleyapp.hooley.model.MyTicketModel;
import com.hooleyapp.hooley.model.NearByEventListFeedModel;
import com.hooleyapp.hooley.model.NearByEventMapFeedModel;
import com.hooleyapp.hooley.model.NotificationCountModel;
import com.hooleyapp.hooley.model.PastGuestListModel;
import com.hooleyapp.hooley.model.PendingFriendRequestModel;
import com.hooleyapp.hooley.model.PersonalGeofenceModel;
import com.hooleyapp.hooley.model.PostImageCommentModel;
import com.hooleyapp.hooley.model.RemoveAllSearchesRequestModel;
import com.hooleyapp.hooley.model.RootModel;
import com.hooleyapp.hooley.model.SaveEventModel;
import com.hooleyapp.hooley.model.ScanTicketResponseModel;
import com.hooleyapp.hooley.model.SearchEventModel;
import com.hooleyapp.hooley.model.SendGiftModel;
import com.hooleyapp.hooley.model.SetAlertModel;
import com.hooleyapp.hooley.model.TagUserModel;
import com.hooleyapp.hooley.model.UploadImageModel;
import com.hooleyapp.hooley.model.UserGuideModel;
import com.hooleyapp.hooley.model.UserPreferencesModel;
import com.hooleyapp.hooley.model.WhosHereModel;
import com.hooleyapp.hooley.model.event.GetEditPostModel;
import com.hooleyapp.hooley.model.event.PastEventsListModel;
import com.hooleyapp.hooley.model.friends.GetFriendOfFriendModel;
import com.hooleyapp.hooley.model.friends.HooleyUserModel;
import com.hooleyapp.hooley.model.staticData.StaticDataModel;
import com.hooleyapp.hooley.model.ticketStats.TicketStatsResponse;
import com.hooleyapp.hooley.others.Constants;
import com.hooleyapp.hooley.requestmodel.AcceptDeclineFriendRequestModel;
import com.hooleyapp.hooley.requestmodel.AddCardRequest;
import com.hooleyapp.hooley.requestmodel.AddFriendRequestModel;
import com.hooleyapp.hooley.requestmodel.AddSearchBookmarkRequestModel;
import com.hooleyapp.hooley.requestmodel.AttendEventRequestModel;
import com.hooleyapp.hooley.requestmodel.BlockFriendrequestModel;
import com.hooleyapp.hooley.requestmodel.BuyTicketRequestModel;
import com.hooleyapp.hooley.requestmodel.ConnectStripeRequestModel;
import com.hooleyapp.hooley.requestmodel.ConstactSyncRequestModel;
import com.hooleyapp.hooley.requestmodel.CreateThreadRequestModel;
import com.hooleyapp.hooley.requestmodel.DeleMediaRequestModel;
import com.hooleyapp.hooley.requestmodel.EditTicketRequestModel;
import com.hooleyapp.hooley.requestmodel.EventBookMarkRequestModel;
import com.hooleyapp.hooley.requestmodel.EventCommentRequestModel;
import com.hooleyapp.hooley.requestmodel.EventImageShareRequestModel;
import com.hooleyapp.hooley.requestmodel.FavMediaRequestModel;
import com.hooleyapp.hooley.requestmodel.FeedBackRequestModel;
import com.hooleyapp.hooley.requestmodel.FollowEventRequestModel;
import com.hooleyapp.hooley.requestmodel.ForgetPasswordRequestModel;
import com.hooleyapp.hooley.requestmodel.InterestRequestModel;
import com.hooleyapp.hooley.requestmodel.InvitePastGuestListRequestModel;
import com.hooleyapp.hooley.requestmodel.InvitePeopleNearByMeRequestModel;
import com.hooleyapp.hooley.requestmodel.LikeEventRequestModel;
import com.hooleyapp.hooley.requestmodel.LoginUserRequestModel;
import com.hooleyapp.hooley.requestmodel.LogoutRequestModel;
import com.hooleyapp.hooley.requestmodel.PostCommentRequestModel;
import com.hooleyapp.hooley.requestmodel.PostLikeRequestModel;
import com.hooleyapp.hooley.requestmodel.PromoteEventRequestModel;
import com.hooleyapp.hooley.requestmodel.RefundTicketModel;
import com.hooleyapp.hooley.requestmodel.RegisterRequestModel;
import com.hooleyapp.hooley.requestmodel.RemoveAllSearchBookMarkRequestModel;
import com.hooleyapp.hooley.requestmodel.RemoveSingleSearchBookMarkRequestModel;
import com.hooleyapp.hooley.requestmodel.RemoveThreadRequestModel;
import com.hooleyapp.hooley.requestmodel.SaveTicketRequestModel;
import com.hooleyapp.hooley.requestmodel.ScanTicketRequestModel;
import com.hooleyapp.hooley.requestmodel.SearchEventRequestModel;
import com.hooleyapp.hooley.requestmodel.SeeAllNearByEventMapViewPolygonFeedsModel;
import com.hooleyapp.hooley.requestmodel.SeenNotificationRequestModel;
import com.hooleyapp.hooley.requestmodel.SendContactInviteRequestModel;
import com.hooleyapp.hooley.requestmodel.SendMessageRequestModel;
import com.hooleyapp.hooley.requestmodel.SendStreamMessageRequestModel;
import com.hooleyapp.hooley.requestmodel.SendTicketGiftRequestModel;
import com.hooleyapp.hooley.requestmodel.SetDarkThemeRequestModel;
import com.hooleyapp.hooley.requestmodel.SetEventAlertRequestModel;
import com.hooleyapp.hooley.requestmodel.SetTicketAlertRequestModel;
import com.hooleyapp.hooley.requestmodel.ShareEventRequestModel;
import com.hooleyapp.hooley.requestmodel.SocialLoginRequestModel;
import com.hooleyapp.hooley.requestmodel.SubmitPostReportRequestModel;
import com.hooleyapp.hooley.requestmodel.SubmitReportEventRequestModel;
import com.hooleyapp.hooley.requestmodel.UpdateEventActionRequestModel;
import com.hooleyapp.hooley.requestmodel.UpdateGeoFenceRequestModel;
import com.hooleyapp.hooley.requestmodel.UpdateLatLongRequestModel;
import com.hooleyapp.hooley.requestmodel.UpdatePersonalGeofenceRequestModel;
import com.hooleyapp.hooley.requestmodel.UpdateUserPrivacyRequestModel;
import com.hooleyapp.hooley.requestmodel.UpdateUserSocialMediaRequestModel;
import com.hooleyapp.hooley.requestmodel.VerifyCodeRequestModel;
import com.hooleyapp.hooley.requestmodel.VerifyNumberRequestModel;
import com.hooleyapp.hooley.requestmodel.VerifyPhoneNumberModel;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiInterface {

    @POST(Constants.REGISTER_USER_URL)
    Call<GeneralModel> registerUser(@Body RegisterRequestModel registerRequestModel);

    @POST(Constants.LOGIN_URL)
    Call<LoginModel> loginUser(@Body LoginUserRequestModel loginUserRequestModel);

    @POST(Constants.SOCIAL_LOGIN_URL)
    Call<LoginModel> loginWithSM(@Body SocialLoginRequestModel socialLoginRequestModel);

    @POST(Constants.FORGET_PASSWORD_URL)
    Call<GeneralModel> forgotPassword(@Body ForgetPasswordRequestModel forgetPasswordRequestModel);

    @GET(Constants.GET_USER_GUIDE_URL)
    Call<UserGuideModel> getUserGuideModel();

    @POST(Constants.LOG_OUT_URL)
    Call<GeneralModel> logOutUser(@Body LogoutRequestModel logoutRequestModel);

    @Multipart
    @POST(Constants.UPLOAD_PERSONAL_PROFILE_PIC_URL)
    Call<UploadImageModel> uploadPersonalProfileImage(@Part MultipartBody.Part file, @Part("DestinationFolder") RequestBody DestinationFolder, @Part("isVideo") RequestBody isVideo, @Part("userId") RequestBody userId);

    @Multipart
    @POST(Constants.SEND_MEDIA_MESSAGE_URL)
    Call<UploadImageModel> sendMediaMessage(@Part MultipartBody.Part file, @Part("userId") RequestBody userId, @Part("threadId") RequestBody threadId);

    @Multipart
    @POST(Constants.SAVE_EVENT_URL)
    Call<SaveEventModel> createEvent(@Part ArrayList<MultipartBody.Part> file, @Part("isUpdate") RequestBody isUpdate, @Part("eventAttribute") RequestBody eventAttribute);

    @Multipart
    @POST(Constants.UPLOAD_EVENT_IMAGES_URL)
    Call<GeneralModel> uploadEventMedia(@Part ArrayList<MultipartBody.Part> file, @Part("postAttributes") RequestBody postAttributes);

    @Multipart
    @POST(Constants.UPDATE_PERSONAL_PROFILE_URL)
    Call<GeneralModel> updatePersonalProfile(@Part ArrayList<MultipartBody.Part> file, @Part("generalProfileAttribute") RequestBody generalProfileAttribute);

    @POST(Constants.UPDATE_USER_PRIVACY_URL)
    Call<GeneralModel> updateUserPrivacy(@Body UpdateUserPrivacyRequestModel updateUserPrivacyRequestModel);

    @POST(Constants.SET_USER_THEME_URL)
    Call<GeneralModel> setDarkTheme(@Body SetDarkThemeRequestModel setDarkThemeRequestModel);

    @Multipart
    @POST(Constants.SAVE_PAID_EVENT)
    Call<SaveEventModel> savePaidEvent(@Part ArrayList<MultipartBody.Part> file, @Part("isUpdate") RequestBody isUpdate, @Part("eventAttribute") RequestBody eventAttribute);

    @Multipart
    @POST(Constants.UPDATE_BUSINESS_PROFILE_URL)
    Call<GeneralModel> updateBusinessProfile(@Part ArrayList<MultipartBody.Part> file, @Part("businessProfileAttribute") RequestBody businessProfileAttribute);

    @GET(Constants.EVENT_SCREEN_DATA_URL)
    Call<CreateEventModel> getCreateEventData(@Query("userId") String userId);

    @GET(Constants.GET_PAST_GUEST_URL)
    Call<PastGuestListModel> getPastGuestList(@Query("userId") String userId);


    @GET(Constants.GET_PAST_EVENTS_URL)
    Call<PastEventsListModel> getPastEventList(@Query("userId") String userId);

    @POST(Constants.SEND_INVITE_PAST_GUEST_URL)
    Call<GeneralModel> sendInvitePastGuest(@Body InvitePastGuestListRequestModel InvitePastGuestListRequestModel);

    @Multipart
    @POST(Constants.UPLOAD_PHOTO_FLICKR_URL)
    Call<ResponseBody> SharePhotoFlickr(
//            @Header("Authorization") String Authorization,
            @Part MultipartBody.Part file,
            @Part("oauth_consumer_key") RequestBody api_key,
            @Part("oauth_token") RequestBody oauth_token,
            @Part("oauth_signature_method ") RequestBody oauth_signature_method,
            @Part("oauth_timestamp ") RequestBody oauth_timestamp,
            @Part("oauth_nonce") RequestBody oauth_nonce,
            @Part("oauth_version") RequestBody oauth_version,
            @Part("oauth_signature") RequestBody oauth_signature

    );


    @FormUrlEncoded
    @POST(Constants.UPLOAD_PHOTO_FLICKR_URL)
    Call<RootModel> SharePhotoFlickr(@Field("photo") String photoUrl, @Field("api_key") String api_key, @Field("oauth_token") String oauth_token);

    @GET(Constants.EVENT_WALLPAPER_URL)
    Call<HooleyGalleryModel> getHooleyGalleryModel(@Query("userId") String userId);

    @GET(Constants.GET_EVENT_INVITE_NEAR_BY_ME_URL)
    Call<EventInviteNearByMeModel> getEventNearByMe(@Query("userId") String userId, @Query("latitude") Double latitude, @Query("longitude") Double longitude, @Query("miles") Float miles);

    @POST(Constants.SEND_INVITE_TO_GUEST_NEAR_BY_ME_URL)
    Call<GeneralModel> sendInvitePeopleNearBy(@Body InvitePeopleNearByMeRequestModel invitePeopleNearByMeRequestModel);

    @POST(Constants.SEND_INVITE_TO_HOOLEY_FRIENDS_URL)
    Call<GeneralModel> sendInviteToHooleyFriends(@Body InvitePeopleNearByMeRequestModel invitePeopleNearByMeRequestModel);

    @GET(Constants.GET_PERSONAL_GEOFENCEE_URL)
    Call<PersonalGeofenceModel> getPersonalGeofence(@Query("userId") String userId);

    @POST(Constants.UPDATE_PERSONAL_GEOFENCE_URL)
    Call<GeneralModel> updatePersonalGeofence(@Body UpdatePersonalGeofenceRequestModel updatePersonalGeofenceRequestModel);

    @GET(Constants.SEE_EVENT_DETAIL_BY_ID_URL)
    Call<EventDetailModel> seeEventDetail(@Query("userId") String userId,
                                          @Query("eventId") String eventId,
                                          @Query("isGuest") Boolean isGuest);

    @GET(Constants.GET_MY_HOST_EVENTS_URL)
    Call<MyHostEventModel> getMyHostEvents(@Query("userId") String userId);

    @GET(Constants.GET_MY_ATTEND_EVENTS_URL)
    Call<MyAttendingEventModel> getMyAttendEvents(@Query("userId") String userId);

    @GET(Constants.GET_MY_FOLLOW_EVENTS_URL)
    Call<MyFollowingEventModel> getMyFollowEvents(@Query("userId") String userId);

    @GET(Constants.SEE_EVENT_GALLERY_FEEDS_URL)
    Call<EventGalleryFeedModel> seeEventGalleryFeeds(@Query("userId") String userId, @Query("eventId") String eventId, @Query("type") String type);

    @POST(Constants.ADD_FOLLOWER_EVENT_URL)
    Call<GeneralModel> followEvent(@Body FollowEventRequestModel followEventRequestModel);

    @POST(Constants.ADD_ATTEND_EVENT_URL)
    Call<GeneralModel> attendEvent(@Body AttendEventRequestModel attendEventRequestModel);

    @GET(Constants.SHOW_IMAGE_COMMENTS_URL)
    Call<PostImageCommentModel> showSinglePostComments(@Query("userId") String userId, @Query("postId") String imageId, @Query("eventId") String eventId);

    @POST(Constants.POST_COMMENT_ON_EVENT_URL)
    Call<GeneralModel> postCommentOnEvent(@Body EventCommentRequestModel eventCommentRequestModel);

    @GET(Constants.GET_REPORT_EVENT_URL)
    Call<GetReportEventModel> getReportEvent(@Query("userId") String userId);

    @POST(Constants.SUBMIT_REPORT_EVENT_URL)
    Call<GeneralModel> submitReportEvent(@Body SubmitReportEventRequestModel submitReportEventRequestModel);

    @GET(Constants.GET_MEDIA_REPORT_EVENT_URL)
    Call<GetReportEventModel> getMediaReportEvent(@Query("userId") String userId);

    @POST(Constants.SUBMIT_MEDIA_REPORT_EVENT_URL)
    Call<GeneralModel> submitMediaReportEvent(@Body SubmitPostReportRequestModel submitPostReportRequestModel);

    @POST(Constants.DELETE_MEDIA_URL)
    Call<GeneralModel> deleteMedia(@Body DeleMediaRequestModel deleMediaRequestModel);

    @POST(Constants.EVENT_IMAGE_COMMENT_URL)
    Call<GeneralModel> commentOnPost(@Body PostCommentRequestModel postCommentRequestModel);

    @POST(Constants.DELETE_USER_INTERESTS_URL)
    Call<GeneralModel> deleteUserInerests(@Body InterestRequestModel interestRequestModel);

    @POST(Constants.ADD_USER_INTERESTS_URL)
    Call<GeneralModel> addUserInerests(@Body InterestRequestModel interestRequestModel);

    @POST(Constants.VERIFY_PHONE_NUMBER_URL)
    Call<VerifyPhoneNumberModel> verifyPhoneNumber(@Body VerifyNumberRequestModel verifyNumberRequestModel);

    @POST(Constants.VERIFY_PHONE_NUMBER_CODE_URL)
    Call<GeneralModel> verifyCode(@Body VerifyCodeRequestModel verifyCodeRequestModel);

    @POST(Constants.EVENT_IMAGE_LIKE_URL)
    Call<GeneralModel> addPostLike(@Body PostLikeRequestModel postLikeRequestModel);

    @POST(Constants.ADD_MEDIA_TO_FAVORITES_URL)
    Call<GeneralModel> addMediaToFav(@Body FavMediaRequestModel favMediaRequestModel);

    @GET(Constants.GET_TAG_USER_URL)
    Call<TagUserModel> getTagUser(@Query("userId") String userId, @Query("eventId") String eventId, @Query("imageId") String imageId);

    @GET(Constants.GET_WHOS_HERE_URL)
    Call<WhosHereModel> getWhosHere(@Query("userId") String userId, @Query("eventId") String eventId, @Query("type") String type);

    @GET(Constants.GET_EDIT_EVENT_URL)
    Call<EditEventModel> getEditEvent(@Query("userId") String userId, @Query("eventId") String eventId);

    @GET(Constants.GET_STATIC_DATA_URL)
    Call<StaticDataModel> getStaticData(@Query("userId") String userId, @Query("version") String version);

    @Multipart
    @POST(Constants.SAVE_EVENT_URL)
    Call<GeneralModel> updateEvent(@Part ArrayList<MultipartBody.Part> file, @Part("isUpdate") RequestBody isUpdate, @Part("eventAttribute") RequestBody eventAttribute);

    //    @Headers("Cache-Control: max-age=640000")
    @GET(Constants.NEAR_BY_EVENT_LIST_VIEW_FEED_URL)
    Call<NearByEventListFeedModel> seeAllNearByEventListViewFeeds(@Query("userId") String data, @Query("latitude") String latitude, @Query("longitude") String longitude, @Query("radius") String radius, @Query("isGuest") Boolean isGuest);

    @GET(Constants.NEAR_BY_EVENT_MAP_VIEW_FEED_URL)
    Call<NearByEventMapFeedModel> seeAllNearByEventMapViewFeeds(@Query("userId") String data, @Query("latitude") Double latitude, @Query("longitude") Double longitude, @Query("radius") Float miles, @Query("isGuest") Boolean isGuest);

    @POST(Constants.ADD_LIKE_EVENT_URL)
    Call<GeneralModel> likeEvent(@Body LikeEventRequestModel likeEventRequestModel);

    @POST(Constants.ADD_EVENT_SHARE_URL)
    Call<GeneralModel> shareEvent(@Body ShareEventRequestModel shareEventRequestModel);

    @POST(Constants.ADD_EVENT_IMAGE_SHARE_URL)
    Call<GeneralModel> eventImageShare(@Body EventImageShareRequestModel eventImageShareRequestModel);

    @POST(Constants.NEAR_BY_EVENTS_MAP_VIEW_FEED_POLYGON_URL)
    Call<NearByEventMapFeedModel> seeAllNearByEventMapViewPolygonFeeds(@Body SeeAllNearByEventMapViewPolygonFeedsModel seeAllNearByEventMapViewPolygonFeedsModel);

    @POST(Constants.GET_USER_NEAR_BY_ME_MAP_VIEW_POLYGON_URL)
    Call<EventInviteNearByMeModel> getUserNearByMePolygon(@Field("data") String data);

    @POST(Constants.ADD_BOOK_MARK_EVENT_URL)
    Call<GeneralModel> addBookMarkEvent(@Body EventBookMarkRequestModel eventBookMarkRequestModel);


    @GET(Constants.SEE_FRIEND_PROFILES_URL)
    Call<FriendProfileModel> seeFriendProfile(@Query("userId") String userId, @Query("friendId") String friendId);

    @GET(Constants.GET_MY_FRIEND_LIST_URL)
    Call<FriendsModel> getMyFriendsList(@Query("userId") String data);

    @POST(Constants.ACCEPT_DECLINE_FRIEND_REQUEST_URL)
    Call<GeneralModel> acceptDeclineFriendRequest(@Body AcceptDeclineFriendRequestModel acceptDeclineFriendRequestModel);

    @POST(Constants.ADD_FRIEND_REQUEST_URL)
    Call<GeneralModel> sendFriendRequest(@Body AddFriendRequestModel addFriendRequestModel);

    @GET(Constants.GET_MY_PENDING_FRIEND_REQUEST_URL)
    Call<PendingFriendRequestModel> getMYPendingFreindRequest(@Query("userId") String data);

    @POST(Constants.SAVE_TICKET_URL)
    Call<GeneralModel> saveTicket(@Body SaveTicketRequestModel saveTicketRequestModel);

    @GET(Constants.GET_TICKET_BY_EVENT_URL)
    Call<GetTicketByEventModel> getTicketByEvent(@Query("userId") String userId, @Query("eventId") String eventId);

    @POST(Constants.CONNECT_STRIPE_URL)
    Call<GeneralModel> connectStripe(@Body ConnectStripeRequestModel connectStripeRequestModel);

    @POST(Constants.EDIT_TICKET_URL)
    Call<GeneralModel> editTicket(@Body EditTicketRequestModel editTicketRequestModel);

    @POST(Constants.UN_FRIEND_URL)
    Call<GeneralModel> unFriend(@Body CreateThreadRequestModel createThreadRequestModel);

    @GET(Constants.GET_CHAT_STREAM_URL)
    Call<GetChatStreamModel> getChatStream(@Query("userId") String data, @Query("eventId") String eventId);

    @POST(Constants.SEND_TEXT_EVENT_CHAT_STREAM_URL)
    Call<GeneralModel> sendTextEventChatStream(@Body SendStreamMessageRequestModel sendStreamMessageRequestModel);

    @POST(Constants.UPDATE_USER_SOCIAL_MEDIA_URL)
    Call<GeneralModel> updateUserSocialMedia(@Body UpdateUserSocialMediaRequestModel updateUserSocialMediaRequestModel);

    @GET(Constants.GET_USER_INSTA_DATA_URL)
    Call<InstaProfileModel> getUserInstaData(@Query("access_token") String accessToken);

    @GET(Constants.GET_COUNTRY_STATE_CITIES_URL)
    Call<CountryStateCitiesModel> getCountryStateCities(@Query("userId") String userId, @Query("area") String area, @Query("countryId") String countryId, @Query("stateId") String stateId);

    @POST(Constants.BUY_TICKET_URL)
    Call<GeneralModel> buyTicket(@Body BuyTicketRequestModel BuyTicketRequestModel);

    @GET(Constants.GET_MEDIA_STREAM_URL)
    Call<MediaStreamEventListModel> getMediaStream(@Query("userId") String userId, @Query("type") String type);

    @GET(Constants.GET_MEDIA_STREAM_TRENDING_URL)
    Call<MediaStreamTrendingModel> getMediaStreamTreeding(@Query("userId") String data, @Query("type") String type);

    @GET(Constants.SHOW_EVENT_BOOKMARKS_URL)
    Call<BookmarkEventModel> showEventBookmark(@Query("userId") String data, @Query("isGuest") Boolean isGuest);

    @GET(Constants.GET_MEDIA_BY_EVENT_URL)
    Call<MediaStreamDetailModel> getMediaByEvent(@Query("userId") String data, @Query("eventId") String eventId, @Query("type") String type);

    @GET(Constants.GET_FEATURE_EVENTS_URL)
    Call<BookmarkEventModel> getfeatureEvent(@Query("userId") String userId, @Query("type") String type);

    @GET(Constants.POPULAR_IN_FRIENDS_EVENTS_URL)
    Call<PopularInFriendsModel> getPopularInFriendsEvent(@Query("userId") String data);

    @GET(Constants.MY_PROFILE_MEDIA_STREAM_URL)
    Call<MediaStreamEventListModel> getMyMedia(@Query("userId") String data);

    @GET(Constants.MY_PROFILE_MEDIA_ALBUM_VIEW_URL)
    Call<MyMediaAlbumModel> getMyMediaAlbum(@Query("userId") String data);

    @POST(Constants.ADD_SEARCH_BOOKMARK_URL)
    Call<GeneralModel> addBookMarkSearch(@Body AddSearchBookmarkRequestModel addSearchBookmarkRequestModel);

    @GET(Constants.GET_SEARCH_BOOKMARK_URL)
    Call<BookMarkSearchModel> getBookMarkSearch(@Query("userId") String data);

    @POST(Constants.REMOVE_SINGLE_SEARCH_BOOKMARK_URL)
    Call<GeneralModel> removeBookMarkSearch(@Body RemoveSingleSearchBookMarkRequestModel removeSingleSearchBookMarkRequestModel);

    @POST(Constants.REMOVE_ALL_SEARCH_BOOKMARK_URL)
    Call<GeneralModel> removeAllBookMarkSearch(@Body RemoveAllSearchBookMarkRequestModel removeAllSearchBookMarkRequestModel);

    @POST(Constants.SEARCH_EVENT_URL)
    Call<SearchEventModel> searchEvent(@Body SearchEventRequestModel searchEventRequestModel);

    @GET(Constants.GET_EVENT_INVITES_NOTIFICATIONS_URL)
    Call<EventInviteNotificationModel> getEventInvitesNotification(@Query("userId") String data);

    @GET(Constants.LIKE_COMMENT_NOTIFICATION_URL)
    Call<MediaAlertNotificationModel> getMediaAlterNotification(@Query("userId") String data);

    @GET(Constants.GET_FRIEND_REQUEST_NOTIFICATIONS_URL)
    Call<FriendRequestNotificationModel> getFriendRequestNotification(@Query("userId") String data);

    @GET(Constants.GET_MESSAGES_REQUEST_NOTIFICATIONS_URL)
    Call<MessageAlertNotificationModel> getMessageAlterNotification(@Query("userId") String data);

    @POST(Constants.MESSAGE_NOTIFICATIONS_SEEN_URL)
    Call<GeneralModel> setSeenCount(@Body SeenNotificationRequestModel seenNotificationRequestModel);

    @GET(Constants.GET_NOTIFICATIONS_COUNT_URL)
    Call<NotificationCountModel> getNotificationCount(@Query("userId") String data);

    @GET(Constants.GET_RECENT_DATA_URL)
    Call<NotificationCountModel> getRecentData(@Query("userId") String data, @Query("osType") String osType);

    @GET(Constants.GET_MY_EVENT_INVITE_URL)
    Call<MyInviteEventModel> getMyEventInvite(@Query("userId") String userId);

    @POST(Constants.CREATE_THREAD_URL)
    Call<CreateThreadModel> createThread(@Body CreateThreadRequestModel createThreadRequestModel);

    @GET(Constants.GET_ALL_THREAD_URL)
    Call<GetAllThreadModel> getAllThread(@Query("userId") String data);

    @POST(Constants.SEND_MESSAGE_URL)
    Call<GeneralModel> sendIMMessage(@Body SendMessageRequestModel sendMessageRequestModel);

    @GET(Constants.GET_MESSAGES_URL)
    Call<GetAllMessageModel> getMessages(@Query("userId") String data, @Query("threadId") String threadId);

    @POST(Constants.REMOVE_THREAD_URL)
    Call<GeneralModel> removeThread(@Body RemoveThreadRequestModel removeThreadRequestModel);

    @GET(Constants.GET_ALL_STATES_CITIES_URL)
    Call<GetCityStateModel> getAllStatesCities(@Query("userId") String data);

    @GET(Constants.GET_EVENT_PROMOTION_URL)
    Call<GetEventPromotionModel> getEventPromotionData(@Query("userId") String userId, @Query("eventId") String eventId);

    @POST(Constants.SAVE_EVENT_PROMOTION_URL)
    Call<GeneralModel> promoteEvent(@Body PromoteEventRequestModel promoteEventRequestModel);

    @GET(Constants.EVENT_PROMOTION_STATS_URL)
    Call<EventPromotionStatsModel> getEventPromotionStats(@Query("userId") String data, @Query("eventId") String eventId);

    @POST(Constants.SET_ALERT_URL)
    Call<SetAlertModel> setTicketAlert(@Body SetTicketAlertRequestModel setTicketAlertRequestModel);

    @GET(Constants.GET_MY_TICKETS_URL)
    Call<MyTicketModel> getMyTickets(@Query("userId") String userId);

    @POST(Constants.SEND_GIFT_TICKET_URL)
    Call<GeneralModel> sendGiftTicket(@Body SendTicketGiftRequestModel sendTicketGiftRequestModel);

    @POST(Constants.UPDATE_CURRENT_LATLONG_URL)
    Call<GeneralModel> updateCurrentLatLong(@Body UpdateLatLongRequestModel updateLatLongRequestModel);

    @POST(Constants.ADD_USER_FEEDBACK_URL)
    Call<GeneralModel> addUserFeedBack(@Body FeedBackRequestModel feedBackRequestModel);

    @GET(Constants.GET_PAST_EVENT_GUEST_LIST_URL)
    Call<GetPastEventGuestListModel> getPastEventGuestList(@Query("userId") String userId, @Query("eventId") String eventId);

    @GET(Constants.GET_SEND_GIFT_URL)
    Call<SendGiftModel> getSendGift(@Query("userId") String userId);

    @GET(Constants.GET_USER_PROFILES_URL)
    Call<GetUserProfilesModel> getUserProfiles(@Query("userId") String userId);

    @GET(Constants.GET_CONTACT_INVITES_URL)
    Call<GetInviteContactModel> getContactInviteList(@Query("userId") String userId, @Query("eventId") String eventId);

    @POST(Constants.SYNC_USER_CONTACTS_URL)
    Call<GeneralModel> sycUserContact(@Body ConstactSyncRequestModel constactSyncRequestModel);

    @POST(Constants.ADD_EVENT_INVITE_BY_SMS_URL)
    Call<GeneralModel> sendInviteToContact(@Body SendContactInviteRequestModel sendContactInviteRequestModel);

    @GET(Constants.GET_USER_CONTACT_CARD_URL)
    Call<GetUserContactCardResponseModel> getUserContactCard(@Query("userId") String userId, @Query("users") String users);

    @GET(Constants.GET_APP_VERSION_URL)
    Call<GetAppVersionResponseModel> getAppVersion(@Query("osType") String osType, @Query("userId") String userId);

    @GET(Constants.GET_EVENT_UPDATE_ACTION_URL)
    Call<GetUpdateEventActionModel> getEventUpdateAction(@Query("userId") String userId, @Query("eventId") String eventId, @Query("updateActionId") int updateActionId);

    @POST(Constants.UPDATE_EVENT_BY_ACTION_URL)
    Call<GeneralModel> updateEventByActionId(@Body UpdateEventActionRequestModel updateEventActionRequestModel);

    @GET(Constants.GET_BUY_TICKET_TERMS_COND_URL)
    Call<GetTermsConditionModel> getTicketTermsCondition(@Query("userId") String userId);

    @POST(Constants.SCAN_TICKET_URL)
    Call<ScanTicketResponseModel> scanTicket(@Body ScanTicketRequestModel scanTicketFragment);

    @GET(Constants.GET_TICKET_SALES_STAT_URL)
    Call<TicketStatsResponse> viewTicketSales(@Query("userId") String userId, @Query("eventId") String eventId);

    @POST(Constants.ADD_MARK_ON_THE_WAY_URL)
    Call<GeneralModel> addMarkOnTheWay(@Body LikeEventRequestModel eventRequestModel);

    @Multipart
    @POST(Constants.PROFILE_SETUP_URL)
    Call<VerifyPhoneNumberModel> profileSetup(@Part ArrayList<MultipartBody.Part> file, @Part("profileSetup") RequestBody profileSetup);

    @GET(Constants.ADD_FRIENDS_FROM_CONTACT_LIST_URL)
    Call<ContactFriendListModel> addFriendFromContact(@Query("userId") String userId);

    @GET(Constants.GET_MORE_PEOPLE_INVITE_URL)
    Call<HooleyUserModel> getMoreInvitePeople(@Query("userId") String userId, @Query("searchString") String searchString);

    @GET(Constants.GET_FRIENDS_OF_FRIENDS_URL)
    Call<GetFriendOfFriendModel> getFriendsOfFriends(@Query("userId") String userId);

    @POST(Constants.SET_EVENT_ALERT_URL)
    Call<SetAlertModel> setEventAlert(@Body SetEventAlertRequestModel setEventAlertRequestModel);

    @POST(Constants.ADD_REMOVE_FRIEND_TO_BLOCK_LIST_URL)
    Call<GeneralModel> blockFriend(@Body BlockFriendrequestModel blockFriendrequestModel);

    @Multipart
    @POST(Constants.ADD_EVENT_ANNOUNCEMENT_URL)
    Call<GeneralModel> addEventAnnouncement(@Part ArrayList<MultipartBody.Part> file, @Part("announcement") RequestBody setEventAnnouncementRequestModel);

    @FormUrlEncoded
    @POST(Constants.DELETE_EVENT_ANNOUNCEMENT_URL)
    Call<GeneralModel> deleteEventAnnouncement(@Field("userId") String userId, @Field("id") String id);

    @GET(Constants.GET_EDIT_URL)
    Call<GetEditPostModel> getEditPost(@Query("userId") String data, @Query("eventId") String eventId, @Query("postId") String postId);

    @Multipart
    @POST(Constants.UPDATE_POST_URL)
    Call<GeneralModel> updatePost(@Part ArrayList<MultipartBody.Part> file, @Part("postAttributes") RequestBody postAttributes);

    @POST(Constants.REFUND_TICKET_URL)
    Call<GeneralModel> refundTicket(@Body RefundTicketModel refundTicketModel);

    @POST(Constants.REMOVE_ALL_USER_SEARCHES_URL)
    Call<GeneralModel> removeAllUserSearches(@Body RemoveAllSearchesRequestModel removeAllSearchesRequestModel);

    @POST(Constants.ADD_USER_SEARCH_HISTORY_URL)
    Call<GeneralModel> addUserSearchHistory(@Body AddSearchHistoryRequestModel addSearchHistoryRequestModel);

    @GET(Constants.GET_USER_SEARCH_HISTORY_URL)
    Call<GetUserSearchesHistory> getUserSearchHistory(@Query("userId") String userId);

    @GET(Constants.GET_USER_PRIVACY_URL)
    Call<UserPreferencesModel> getUserPrivacy(@Query("userId") String userId);

    @POST(Constants.ADD_CARD)
    Call<GeneralModel> addCard(@Body AddCardRequest addCardRequest);

    @GET(Constants.CARD_DETAILS)
    Call<CardDetailsModel> geCardDetails(@Query("userId") String userId);

    @GET(Constants.GET_EVENT_GUEST_LIST_URL)
    Call<EventGuestListModel> getEventGuestList(@Query("userId") String userId, @Query("eventId") String eventId, @Query("type") String postId);

    @GET(Constants.GET_EVENT_ANNOUCEMENT_URL)
    Call<EventAnnouncementModel> getEventAnnouncement(@Query("userId") String userId, @Query("eventId") String eventId, @Query("isGuest") Boolean isGuest);

    @POST(Constants.DELETE_CARD)
    Call<GeneralModel> deleteCard(@Body LogoutRequestModel logoutRequestModel);

    // Guest USER
    @POST(Constants.GUEST_LOGIN_URL)
    Call<GuestUserLoginModel> guestUserLogin(@Body GuestUserLoginRequestModel guestUserLoginRequestModel);

    @GET(Constants.GUEST_NEAR_BY_EVENTS_LIST_VIEW_FEED_URL)
    Call<NearByEventListFeedModel> seeGuestUserAllNearByEventList(@Query("userId") String userId, @Query("latitude") String latitude, @Query("longitude") String longitude, @Query("radius") String radius);

    @GET(Constants.GUEST_NEAR_BY_EVENTS_MAP_VIEW_FEED_URL)
    Call<NearByEventMapFeedModel> seeGuestUserAllNearByEventMap(@Query("userId") String userId, @Query("latitude") Double latitude, @Query("longitude") Double longitude, @Query("radius") Float miles);

    @GET(Constants.SEE_GUEST_EVENT_DETAILS_BY_ID_URL)
    Call<EventDetailModel> seeGuestUserEventDetail(@Query("userId") String userId, @Query("eventId") String eventId);

    @GET(Constants.SEE_GUEST_EVENT_GALLERY_FEEDS_URL)
    Call<EventGalleryFeedModel> seeGuestUserEventGalleryFeeds(@Query("userId") String userId, @Query("eventId") String eventId, @Query("type") String type);

    @POST(Constants.GUEST_SEARCH_EVENTS_URL)
    Call<SearchEventModel> guestUsersearchEvent(@Body SearchEventRequestModel searchEventRequestModel);

    @POST(Constants.GUEST_ADD_REMOVE_BOOKMARKS_EVENT_URL)
    Call<GeneralModel> addGuestUserBookMarkEvent(@Body EventBookMarkRequestModel eventBookMarkRequestModel);

    @POST(Constants.GUEST_ADD_USER_FEEDBACK_URL)
    Call<GeneralModel> addGuestUserFeedBack(@Body FeedBackRequestModel feedBackRequestModel);

    @GET(Constants.GUEST_SHOW_EVENT_BOOKMARKS_URL)
    Call<BookmarkEventModel> showGuestUserEventBookmark(@Query("userId") String data);

    @POST(Constants.UPDATE_EVENT_GEOFENCE_URL)
    Call<GeneralModel> updateEventGeoFence(@Body UpdateGeoFenceRequestModel modelGeofence);

    @GET(Constants.GET_GUEST_EVENT_ANNOUCEMENT_URL)
    Call<EventAnnouncementModel> getGuestEventAnnouncement(@Query("userId") String userId, @Query("eventId") String eventId);

    @GET(Constants.GET_BLOCKED_USERS_URL)
    Call<BlockUserListModel> getBlockUserList(@Query("userId") String userId);

    @POST(Constants.UN_BLOCKE_USER_URL)
    Call<GeneralModel> unBlockUser(@Body CreateThreadRequestModel modelGeofence);

    @GET(Constants.GET_EDIT_TICKET_URL)
    Call<GetEditTicketResponse> getEditTicket(@Query("userId") String userId, @Query("eventId") String eventId);

    @POST(Constants.SAVE_EDIT_TICKET_URL)
    Call<GeneralModel> saveEditTicket(@Body EditTicketRequest editTicketRequest);

    @POST(Constants.ENABLE_DISABLE_PPV_URL)
    Call<GeneralModel> enableDisablePPV(@Body EnableDisablePPV enableDisablePPV);

}
