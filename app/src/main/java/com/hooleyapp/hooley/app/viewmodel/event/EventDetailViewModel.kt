package com.hooleyapp.hooley.app.viewmodel.event

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventDetailModel
import com.hooleyapp.hooley.app.data.repository.event.EventDetailRepository
import com.hooleyapp.hooley.helper.SingleLiveEvent
import com.hooleyapp.hooley.model.PersonalProfileModel


class EventDetailViewModel(application: Application) : AndroidViewModel(application) {

    var repository = EventDetailRepository()
    var failureMessage: SingleLiveEvent<String> = SingleLiveEvent()
    var sessionExpire: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var loading: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var response: SingleLiveEvent<EventDetailModel> = SingleLiveEvent()
    var isSectionUpdate: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var reportEventList: SingleLiveEvent<List<PersonalProfileModel.myObject>> = SingleLiveEvent()
    var actionType: SingleLiveEvent<String> = SingleLiveEvent()

    init {
        failureMessage = repository.failureMessage
        sessionExpire = repository.sessionExpire
        loading = repository.loading
        response = repository.response
        isSectionUpdate = repository.isSectionUpdate
        reportEventList = repository.reportEventList
        actionType = repository.actionType
    }

    fun getEventDetail(eventId: String) {
        repository.getEventDetails(eventId)
    }

    fun likeEvent(eventId: String) {
        repository.likeEvent(eventId)
    }

    fun followEvent(eventId: String, isFollow: Boolean) {
        repository.followEvent(eventId, isFollow)
    }

    fun attendEvent(eventId: String, isAttended: Boolean) {
        repository.attendEvent(eventId, isAttended)
    }

    fun getEventReport() {
        repository.getReportEvent()
    }

    fun bookMarkEvent(eventId: String, isRemoved: Boolean) {
        repository.bookMarkEvent(eventId, isRemoved)
    }

    fun addMarkOnWay(eventId: String, type: String) {
        repository.addMarkOnWay(eventId, type)
    }

    fun submitReport(eventId: String, reasonId: String, reportMessage: String) {
        repository.submitReport(eventId, reasonId, reportMessage)
    }

    fun addGuestUserBookMarkEvent(eventId: String, isRemoved: Boolean) {
        repository.addGuestUserBookMarkEvent(eventId, isRemoved)
    }

    fun shareEvent(shareMedia: String, eventId: String) {
        repository.shareEvent(shareMedia, eventId)
    }
}
