package com.hooleyapp.hooley.app.data.repository.others

import android.arch.lifecycle.MutableLiveData
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.GetAppVersionResponseModel
import com.hooleyapp.hooley.services.HooleyWebService

class AboutHooleyRepository {

    var service = HooleyWebService()
    var failureMessage: MutableLiveData<String> = MutableLiveData()
    var sessionExpire: MutableLiveData<Boolean> = MutableLiveData()
    var loading: MutableLiveData<Boolean> = MutableLiveData()
    var versionObject: MutableLiveData<GetAppVersionResponseModel.HooleyVersion> = MutableLiveData()

    fun getAppVersion() {
        loading.postValue(true)
        service.getAppVersion(object : IWebServiceCallback<GetAppVersionResponseModel> {
            override fun success(result: GetAppVersionResponseModel) {
                loading.postValue(false)
                versionObject.postValue(result.hooleyVersion)
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }
        })
    }
}