package com.hooleyapp.hooley.app.data.model.guestuser

import com.google.gson.annotations.SerializedName

class GuestUserLoginModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("guestUserInfo")
    var guestUserInfo = GuestUserInfo()

    class GuestUserInfo {

        @SerializedName("guestId")
        var guestId: String? = null

        @SerializedName("guestToken")
        var guestToken: String? = null
    }
}