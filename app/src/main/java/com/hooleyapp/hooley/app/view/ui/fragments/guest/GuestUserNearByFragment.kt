package com.hooleyapp.hooley.app.view.ui.fragments.guest

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.UiModeManager
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.content.res.Resources
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.AnimationUtils
import android.widget.*
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.adapters.AdapterNearByEvents
import com.hooleyapp.hooley.app.view.ui.activites.HooleyGuestUser
import com.hooleyapp.hooley.databinding.FragmentGuestUserNearByBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.event.EventPagerFragment
import com.hooleyapp.hooley.helper.DialogGuestUser
import com.hooleyapp.hooley.interfaces.INearByEventClickListener
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.MapListFinalModel
import com.hooleyapp.hooley.model.NearByEventListFeedModel
import com.hooleyapp.hooley.model.NearByEventMapFeedModel
import com.hooleyapp.hooley.model.NearByEventsFinalModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.EventWebService
import com.hooleyapp.hooley.services.GeoFenceWebService
import java.text.DecimalFormat
import java.util.*

class GuestUserNearByFragment : BaseFragment(), RadioGroup.OnCheckedChangeListener, GoogleApiClient.ConnectionCallbacks, OnMapReadyCallback, SeekBar.OnSeekBarChangeListener, CompoundButton.OnCheckedChangeListener, View.OnClickListener, GoogleMap.OnMarkerClickListener, GoogleApiClient.OnConnectionFailedListener, LocationListener, ResultCallback<LocationSettingsResult>, INearByEventClickListener, SwipeRefreshLayout.OnRefreshListener, GoogleMap.OnMapLongClickListener {

    private var list = listOf<Place.Field>()
    private val AUTOCOMPLETE_REQUEST_CODE: Int = 506
    lateinit var binding: FragmentGuestUserNearByBinding
    var mGoogleApiClient: GoogleApiClient? = null
    lateinit var mLocationRequest: LocationRequest
    lateinit var mLocationSettingsRequest: LocationSettingsRequest
    var mCurrentLocation: Location? = null
    lateinit var objectResponse: NearByEventListFeedModel
    var objectMapResponse: NearByEventMapFeedModel? = null
    internal var mList: ArrayList<NearByEventsFinalModel>? = ArrayList()
    internal var isSwiped = false
    lateinit var adapter: AdapterNearByEvents
    var mRequestingLocationUpdates: Boolean? = true
    internal var mGooglemap: GoogleMap? = null
    lateinit var locationManager: LocationManager
    lateinit var mapFragment: SupportMapFragment
    var ct = "CT_" + javaClass.simpleName
    var mapFinalList: ArrayList<MapListFinalModel>? = ArrayList()
    internal var bold: Typeface? = null
    internal var regular: Typeface? = null
    private var mapCircle: Circle? = null
    private var markerOption: MarkerOptions? = null
    private var showInfoDialog: Dialog? = null
    private var circleLat: Double = 0.toDouble()
    private var circleLong: Double = 0.toDouble()
    private var isCircle = true
    private var currentProgress = 8046.72f
    private val geoFenceWebService = GeoFenceWebService()
    private var displayInfoDialog = true
    private val gson = Gson()
    var intent: Intent? = null
    var type = Constants.TYPE_LIST_VIEW
    var service = EventWebService()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_guest_user_near_by, container, false)
        Util.hideKeyboard(HooleyGuestUser.activity)
        bold = ResourcesCompat.getFont(HooleyGuestUser.activity, R.font.roboto_bold)
        regular = ResourcesCompat.getFont(HooleyGuestUser.activity, R.font.roboto)
        retainInstance = true
        initMap()
        initPlaces()
        buildGoogleApiClient()
        createLocationRequest()
        buildLocationSettingsRequest()
        checkLocationSettings()
        setListener()
        getNearByEventsListFeeds()
        setToolbar()
        binding.rbListView.isChecked = true
        binding.rbListView.typeface = bold
        binding.rbMapView.typeface = regular
        return binding.root
    }

    fun setToolbar() {
        (HooleyGuestUser.activity as HooleyGuestUser).setToolbarTitle("Nearby Events")
        HooleyApp.db.putBoolean(Constants.OPEN_DRAWER, true)
        try {
            HooleyGuestUser.activity.supportActionBar!!.show()
            if (HooleyGuestUser.activity.supportFragmentManager.findFragmentById(R.id.container) is GuestUserNearByFragment)
                HooleyGuestUser.activity.supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_menu)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setSavedData() = if (HooleyApp.db.getString(Constants.EVENT_FEEDS).isNotEmpty()) {
        if (isSwiped) {
            binding.sLRecycler.isRefreshing = false
            isSwiped = false
        }

        objectResponse = gson.fromJson(HooleyApp.db.getString(Constants.EVENT_FEEDS), NearByEventListFeedModel::class.java)
        addAllDataInList()
        binding.tvNoData.visibility = View.GONE
        binding.pbNearBy.visibility = View.GONE
    } else {
        Toast.makeText(HooleyGuestUser.activity, "No Internet Connection Available", Toast.LENGTH_SHORT).show()
    }

    @RequiresApi(Build.VERSION_CODES.CUPCAKE)
    private fun setListener() {
        binding.nearByTab.setOnCheckedChangeListener(this)
        binding.sbGeofence.setOnSeekBarChangeListener(this)
        binding.tbPaidEvent.setOnCheckedChangeListener(this)
        binding.tbfreeEvent.setOnCheckedChangeListener(this)
        binding.ivMyLocation.setOnClickListener(this)
        binding.sLRecycler.setOnRefreshListener(this)
        binding.ivMapInfo.setOnClickListener(this)
        binding.ivMinus.setOnClickListener(this)
        binding.ivPlus.setOnClickListener(this)
        binding.rlHeader.setOnClickListener(this)

        binding.ivMinus.setOnLongClickListener {
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        if (binding.ivMinus.isPressed) {
                            if (currentProgress > 100.0f) {
                                var temp = currentProgress
                                temp -= 100f
                                if (temp <= 100.0f) {
                                    currentProgress = 100.0f
                                } else {
                                    currentProgress -= 100f
                                }
                                binding.sbGeofence.progress = currentProgress.toInt()
                            }
                        } else {
                            timer.cancel()
                            updateUI()
                        }
                    } else {
                        settingsRequest()
                        Util.showToastMessage(HooleyGuestUser.activity, "Please turn on your location!")
                    }
                }
            }, 100, 200)
            true
        }

        binding.ivPlus.setOnLongClickListener {
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        if (binding.ivPlus.isPressed) {
                            if (currentProgress < 64373) {
                                currentProgress += 100f
                                binding.sbGeofence.progress = currentProgress.toInt()
                            }
                        } else {
                            timer.cancel()
                            updateUI()
                        }
                    } else {
                        settingsRequest()
                        Util.showToastMessage(HooleyGuestUser.activity, "Please turn on your location!")
                    }
                }
            }, 100, 200)

            true
        }
    }

    private fun updateUI() {
        Handler(Looper.getMainLooper()).post {
            if (mapCircle != null) {
                if (currentProgress > 100) {
                    mapCircle!!.radius = currentProgress.toDouble()
                    val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
                    val animateZomm = currentZoomLevel + 5
                    mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat, circleLong), animateZomm))
                    mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
                    getNearByEventsMapFeeds(circleLat, circleLong, (currentProgress / 1609.34).toFloat())
                }
            }
        }
    }

    private fun initPlaces() {
        try {
            // Initialize place API
            if (!Places.isInitialized()) {
                Places.initialize(HooleyGuestUser.activity, HooleyGuestUser.activity.getString(R.string.google_maps_key))
            }
            var placesClient: PlacesClient = Places.createClient(HooleyGuestUser.activity)

            // Create a new token for the autocomplete session. Pass this to FindAutocompletePredictionsRequest,
            // and once again when the user makes a selection (for example when calling fetchPlace()).
            var token: AutocompleteSessionToken = AutocompleteSessionToken.newInstance()
            list = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)
            binding.tvLocationName.setOnClickListener {
                intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, list).build(HooleyGuestUser.activity)
                HooleyGuestUser.activity.startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
            }
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

    }

    private fun setRecyclerView() {
        binding.rbListView.isChecked = true
        binding.rbListView.typeface = bold
        binding.rbMapView.typeface = regular
        binding.rvNearBy.visibility = View.VISIBLE
        binding.sLRecycler.visibility = View.VISIBLE
        binding.rlMapView.visibility = View.GONE
        val manager = LinearLayoutManager(HooleyGuestUser.activity, LinearLayoutManager.VERTICAL, false)
        val animation = AnimationUtils.loadLayoutAnimation(HooleyGuestUser.activity, R.anim.layout_animation_from_right)
        binding.rvNearBy.layoutAnimation = animation
        binding.rvNearBy.layoutManager = manager
        binding.rvNearBy.setEmptyView(binding.tvNoData)
        adapter = AdapterNearByEvents(HooleyGuestUser.activity, mList!!)
        binding.rvNearBy.adapter = adapter
        adapter.setListener(this)

    }

    private fun getNearByEventsListFeeds() {
        if (NetworkUtil.isInternetConnected(HooleyGuestUser.activity)) {
            if (binding != null && !isSwiped)
                binding.pbNearBy.visibility = View.VISIBLE
            service.seeAllNearByEventListViewFeeds(object : IWebServiceCallback<NearByEventListFeedModel> {
                override fun success(result: NearByEventListFeedModel) {
                    if (isSwiped) {
                        binding.sLRecycler.isRefreshing = false
                        isSwiped = false
                    }
                    objectResponse = result
                    val data = gson.toJson(objectResponse)
                    HooleyApp.db.putString(Constants.EVENT_FEEDS, data)
                    addAllDataInList()
                    binding.tvNoData.visibility = View.GONE
                    binding.pbNearBy.visibility = View.GONE
                }

                override fun failure(message: String) {
                    try {
                        if (isSwiped) {
                            binding.sLRecycler.isRefreshing = false
                            isSwiped = false
                        }
                        binding.rlMapView.visibility = View.GONE
                        binding.tvNoData.visibility = View.VISIBLE
                        //                        userSearchbinding.rvNearBy.setVisibility(View.GONE);
                        binding.pbNearBy.visibility = View.GONE
                        binding.tvNoData.text = message
                    } catch (ex: NullPointerException) {
                        ex.printStackTrace()
                    }
                }

                override fun onTokenExpire() {
                    onTokenExpireLogOut()
                }
            })
        } else {
            setSavedData()
        }

    }

    private fun addAllDataInList() {
        if (mList != null)
            mList!!.clear()
        if (objectResponse.featuredEventsList.size > 0)
            mList!!.add(NearByEventsFinalModel(Constants.TYPE_FEATURE_EVENT, objectResponse.featuredEventsList))
        if (objectResponse.todayEventsList.size > 0)
            mList!!.add(NearByEventsFinalModel(Constants.TYPE_TODAY_EVENT, objectResponse.todayEventsList))
        if (objectResponse.nextWeekEventsList.size > 0)
            mList!!.add(NearByEventsFinalModel(Constants.TYPE_NEXT_WEEK_EVENT, objectResponse.nextWeekEventsList))
        if (objectResponse.commingEventsList.size > 0)
            mList!!.add(NearByEventsFinalModel(Constants.TYPE_COMMING_EVENT, objectResponse.commingEventsList))
        setRecyclerView()
    }


    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(context!!)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        settingsRequest()
    }

    private fun settingsRequest() {
        try {
            mLocationRequest = LocationRequest.create()
            mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            mLocationRequest.interval = (30 * 1000).toLong()
            mLocationRequest.fastestInterval = (5 * 1000).toLong()
            val builder = LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest)
            mLocationSettingsRequest = builder.build()
            builder.setAlwaysShow(true) //this is the key ingredient
            val result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, mLocationSettingsRequest)
            result.setResultCallback { result ->
                val status = result.status
                val state = result.locationSettingsStates
                when (status.statusCode) {
                    LocationSettingsStatusCodes.SUCCESS -> startLocationUpdates()
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                        status.startResolutionForResult(HooleyGuestUser.activity, REQUEST_CHECK_SETTINGS)
                    } catch (e: IntentSender.SendIntentException) {
                    }

                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                    }
                }
            }
        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
        }
    }

    private fun initMap() {
        locationManager = HooleyGuestUser.activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!googleServicesAvailable()) {
            Toast.makeText(HooleyGuestUser.activity, "google service not available", Toast.LENGTH_SHORT).show()
            return
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission()
        } else {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                settingsRequest()
            }
        }
        mapFragment = childFragmentManager.findFragmentById(R.id.mpGeofence) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun googleServicesAvailable(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val isAvailable = apiAvailability.isGooglePlayServicesAvailable(context!!)
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true
        } else if (apiAvailability.isUserResolvableError(isAvailable)) {
            val dialog = apiAvailability.getErrorDialog(HooleyGuestUser.activity, isAvailable, 0)
            dialog.show()
        } else {
            Toast.makeText(HooleyGuestUser.activity, "Can't connect ", Toast.LENGTH_SHORT).show()
        }
        return false
    }

    private fun checkLocationPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(HooleyGuestUser.activity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(HooleyGuestUser.activity, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(HooleyGuestUser.activity, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS_REQUEST_LOCATION)
            } else {
                ActivityCompat.requestPermissions(HooleyGuestUser.activity, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS_REQUEST_LOCATION)
            }
            return false
        } else {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (mGoogleApiClient != null) {
                    settingsRequest()
                } else {
                    buildGoogleApiClient()
                }
            }
            return true
        }
    }

    private fun startLocationUpdates() {
        try {
            if (ActivityCompat.checkSelfPermission(HooleyGuestUser.activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return
            } else {
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        mGoogleApiClient,
                        mLocationRequest,
                        this
                ).setResultCallback { status ->
                    mRequestingLocationUpdates = true
                    if (status.isSuccess) {
                        Log.i(ct, "Status true! Going to load!")
                    } else {
                        Log.e(ct, "There is a problem")
                    }
                    getLastLocation()
                }
            }
        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
        }

    }

    private fun stopLocationUpdates() {
        try {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient,
                    this
            ).setResultCallback { status ->
                mRequestingLocationUpdates = false
                if (status.isSuccess) {
                    Log.i(ct, "stopLocationUpdates-> Status true! Going to load!")
                } else {
                    Log.e(ct, "stopLocationUpdates-> There is a problem")
                }
            }
        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
        }

    }

    fun getLastLocation() {
        if (mRequestingLocationUpdates!!) {
            if (mCurrentLocation != null) {
                Log.e(ct, "getLastLocation-> Now fetched!")
                Log.d(ct, "getLastLocation-> Lat is: " + mCurrentLocation!!.latitude)
                Log.d(ct, "getLastLocation-> Lon is: " + mCurrentLocation!!.longitude)
                stopLocationUpdates()
            } else {
                Log.i(ct, "getLastLocation-> Cannot fetch! (mCurrentLocation)")
            }
        } else {
            Log.i(ct, "getLastLocation-> Cannot fetch! (mRequestingLocationUpdates)")
        }
    }

    override fun onConnected(bundle: Bundle?) {
        if (mGooglemap != null)
            mGooglemap!!.uiSettings.isMyLocationButtonEnabled = false
    }

    override fun onConnectionSuspended(i: Int) {}

    override fun onMapReady(googleMap: GoogleMap) {
        mGooglemap = googleMap
        mGooglemap!!.uiSettings.isMapToolbarEnabled = false
        mGooglemap!!.uiSettings.isZoomControlsEnabled = false
        mGooglemap!!.setOnMapLongClickListener(this)

        if (HooleyApp.db.getInt(Constants.TYPE_THEME) == UiModeManager.MODE_NIGHT_YES) {
            try {
                // Customise the styling of the base map using a JSON object defined
                // in a raw resource file.
                val success = mGooglemap!!.setMapStyle(MapStyleOptions(resources
                        .getString(R.string.style_json)))

                if (!success) {
                    Log.e("", "Style parsing failed.")
                }
            } catch (e: Resources.NotFoundException) {
                Log.e("", "Can't find style. Error: ", e)
            }

        }

        //        try {
        //            userSearchbinding.sbGeofence.setProgress(Integer.parseInt(HooleyApp.db.getString(Constants.PERSONAL_RADIUS)));
        //        } catch (NumberFormatException ex) {
        //            ex.printStackTrace();
        //        }


        moveToCurrentLocation()
        addCircle(circleLat, circleLong)

//        moveToLockLocation()
        try {
            binding.tvLocationName.text = (HooleyGuestUser.activity as ActivityBase).getCompleteAddressString(circleLat, circleLong)
        } catch (e: KotlinNullPointerException) {
        }
        addCircle(circleLat, circleLong)
        getNearByEventsMapFeeds(circleLat, circleLong, (currentProgress / 1609.34f))
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

    }

    override fun onLocationChanged(location: Location) {
        mCurrentLocation = location
        getLastLocation()
    }

    protected fun checkLocationSettings() {
        try {
            val result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, mLocationSettingsRequest)
            result.setResultCallback(this)
        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(HooleyGuestUser.activity, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                        }
                    }
                } else {

                    Toast.makeText(HooleyGuestUser.activity, "permission denied", Toast.LENGTH_LONG).show()
                }
                return
            }
        }
    }

    private fun moveToCurrentLocation() {
        try {
            if (mCurrentLocation == null) {
                if (HooleyGuestUser.activity != null) {
                    if (ActivityCompat.checkSelfPermission(HooleyGuestUser.activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(HooleyGuestUser.activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    }
                }
                mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
                if (mCurrentLocation != null) {
                    circleLat = mCurrentLocation!!.latitude
                    circleLong = mCurrentLocation!!.longitude
                    val loc = LatLng(mCurrentLocation!!.latitude, mCurrentLocation!!.longitude)
                    if (mGooglemap != null) {
                        mGooglemap!!.clear()
                        addCircle(circleLat, circleLong)
                        val markerOption = MarkerOptions().position(loc).flat(true).draggable(false)
                        markerOption.icon(bitmapDescriptorFromVector(HooleyGuestUser.activity, R.drawable.ic_map_cirlce_centre))
                        mGooglemap!!.addMarker(markerOption)
                        val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
                        val animateZomm = currentZoomLevel + 5
                        mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, animateZomm))
                        mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
                    }
                } else {
                    circleLat = HooleyApp.db.getDouble(Constants.CURRENT_LAT, 31.470319395591418)
                    circleLong = HooleyApp.db.getDouble(Constants.CURRENT_LANG, 74.36357019476067)
                    val loc = LatLng(circleLat, circleLong)
                    if (mGooglemap != null) {
                        mGooglemap!!.clear()
                        addCircle(circleLat, circleLong)
                        val markerOption = MarkerOptions().position(loc).flat(true).draggable(false)
                        markerOption.icon(bitmapDescriptorFromVector(HooleyGuestUser.activity, R.drawable.ic_map_cirlce_centre))
                        mGooglemap!!.addMarker(markerOption)
                        val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
                        val animateZomm = currentZoomLevel + 5
                        mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, animateZomm))
                        mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
                    }
                }
            } else {
                circleLat = mCurrentLocation!!.latitude
                circleLong = mCurrentLocation!!.longitude
                val loc = LatLng(mCurrentLocation!!.latitude, mCurrentLocation!!.longitude)
                if (mGooglemap != null) {
                    mGooglemap!!.clear()
                    addCircle(circleLat, circleLong)
                    val markerOption = MarkerOptions().position(loc).flat(true).draggable(false)
                    markerOption.icon(bitmapDescriptorFromVector(HooleyGuestUser.activity, R.drawable.ic_map_cirlce_centre))
                    mGooglemap!!.addMarker(markerOption)
                    val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
                    val animateZomm = currentZoomLevel + 5
                    mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat, circleLong), animateZomm))
                    mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)

                }
                binding.tvLocationName.text = (HooleyGuestUser.activity as ActivityBase).getCompleteAddressString(mCurrentLocation!!.latitude, mCurrentLocation!!.longitude)
            }
            getNearByEventsMapFeeds(circleLat, circleLong, currentProgress / 1609.34f)
        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                AUTOCOMPLETE_REQUEST_CODE -> {
                    if (resultCode == Activity.RESULT_OK) {
                        val place: Place = Autocomplete.getPlaceFromIntent(data!!)
                        Log.i("TAG", "Place: " + place.name + ", " + place.id)
                        circleLat = place.latLng!!.latitude
                        circleLong = place.latLng!!.longitude
                        binding.tvLocationName.text = (HooleyGuestUser.activity as ActivityBase).getCompleteAddressString(circleLat, circleLong)
                        val loc = LatLng(circleLat, circleLong)
                        if (mGooglemap != null) {
                            mGooglemap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 11.5f))
                            mGooglemap!!.clear()
                            addCircle(circleLat, circleLong)
                            getNearByEventsMapFeeds(circleLat, circleLong, binding.sbGeofence.progress / 100.0f)
                        }
                    } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                        // TODO: Handle the error.
                        val status: Status = Autocomplete.getStatusFromIntent(data!!)
                        Log.i("TAG", status.statusMessage)
                    } else if (resultCode == Activity.RESULT_CANCELED) {
                        // The user canceled the operation.
                    }

                }
                REQUEST_CHECK_SETTINGS -> when (resultCode) {
                    Activity.RESULT_OK -> {
                        Log.i(ct, "User agreed to make required location settings changes.")
                        startLocationUpdates()
                    }
                    Activity.RESULT_CANCELED -> {
                        Toast.makeText(context, "Please allow your location!", Toast.LENGTH_SHORT).show()
                        Log.i(ct, "User chose not to make required location settings changes.")
                    }
                }
            }
        }
        Log.d(ct, "onActivityResult-> $resultCode")
    }

    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun buildLocationSettingsRequest() {
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        mLocationSettingsRequest = builder.build()
    }

    override fun onResult(locationSettingsResult: LocationSettingsResult) {
        val status = locationSettingsResult.status
        Log.d(ct, "onResult-> " + status.statusCode)
        Log.i(ct, "onResult-> " + status.statusMessage!!)
        when (status.statusCode) {
            LocationSettingsStatusCodes.CANCELED -> Log.i(ct, "Location handling cancelled!")
            LocationSettingsStatusCodes.SUCCESS -> {
                Log.i(ct, "All location settings are satisfied.")
                startLocationUpdates()
            }
            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                Log.i(ct, "Location settings are not satisfied. Show the user a dialog to" + "upgrade location settings ")

                try {
                    status.startResolutionForResult(HooleyGuestUser.activity, REQUEST_CHECK_SETTINGS)
                } catch (e: IntentSender.SendIntentException) {
                    Log.i(ct, "PendingIntent unable to execute request.")
                }

            }
            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(ct, "Location settings are inadequate, and cannot be fixed here. Dialog " + "not created.")
        }

    }

    fun addCircle(cLat: Double, cLong: Double) {
        isCircle = true
        mGooglemap!!.uiSettings.isZoomGesturesEnabled = true
        mGooglemap!!.uiSettings.setAllGesturesEnabled(true)
        if (mapCircle != null) {
            mapCircle!!.remove()
        }
        mapCircle = mGooglemap!!.addCircle(CircleOptions()
                .center(LatLng(cLat, cLong))
                .radius(currentProgress.toDouble())
                .fillColor(resources.getColor(R.color.app_purple_light_color))
                .strokeColor(resources.getColor(R.color.app_purple_color))
                .strokeWidth(2f)
                .clickable(true))
        mapCircle!!.isVisible = true

        val markerOption = MarkerOptions().position(LatLng(cLat, cLong)).flat(true).draggable(false)
        markerOption.icon(bitmapDescriptorFromVector(HooleyGuestUser.activity, R.drawable.ic_map_cirlce_centre))
        mGooglemap!!.addMarker(markerOption)
    }

    @SuppressLint("MissingPermission")
    private fun moveToLockLocation() {
        if (mGooglemap != null) {
            mGooglemap!!.isMyLocationEnabled = false
            val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
            val animateZomm = currentZoomLevel + 5
            mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat, circleLong), animateZomm))
            mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
            addCircle(circleLat, circleLong)
        }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        if (mapCircle != null) {
            mapCircle!!.radius = progress.toDouble()
        }
        val f = DecimalFormat("##.00")
        currentProgress = seekBar!!.progress.toFloat()
        try {
            if (currentProgress < 1609.344) {
                if (progress <= 100) {
                    binding.tvSbProgress.text = 100.toString() + " meters"
                } else {
                    binding.tvSbProgress.text = f.format(currentProgress) + " meters"
                }
            } else {
                binding.tvSbProgress.text = "${f.format(currentProgress / 1609.344)} miles"
            }
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

    }

    override fun onStartTrackingTouch(seekBar: SeekBar) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar) {
        mGooglemap!!.clear()
        addCircle(circleLat, circleLong)
        val currentZoomLevel = getZoomLevel(mapCircle).toFloat()
        val animateZomm = currentZoomLevel + 5
        mGooglemap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(circleLat, circleLong), animateZomm))
        mGooglemap!!.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel), 2000, null)
        getNearByEventsMapFeeds(circleLat, circleLong, binding.sbGeofence.progress / 1609.34f)
    }

    private fun getNearByEventsMapFeeds(lat: Double, lang: Double, radius: Float) {
        geoFenceWebService.seeAllNearByEventMapViewFeeds(lat, lang, radius, object : IWebServiceCallback<NearByEventMapFeedModel> {
            override fun success(result: NearByEventMapFeedModel) {
                objectMapResponse = result
                addFreePaidEventInFinalList()
                if (binding.tbPaidEvent.isChecked) {
                    addPaidEventMarker()
                }
                if (binding.tbfreeEvent.isChecked) {
                    addFreeEventsMarker()
                }
                mGooglemap!!.setOnMarkerClickListener(this@GuestUserNearByFragment)
            }

            override fun failure(message: String) {
                try {
                    //                    Util.showToastMessage(HooleyGuestUser.Companion.getActivity(), message);
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    private fun addFreeEventsMarker() {
        if (mapFinalList != null && mapFinalList!!.size > 0) {
            for (i in mapFinalList!!.indices) {
                if (mapFinalList!![i].type == Constants.TYPE_FREE_EVENT) {
                    val loc = LatLng(mapFinalList!![i].eventDetail.eventLat!!, mapFinalList!![i].eventDetail.eventLong!!)
                    markerOption = MarkerOptions().position(loc).flat(false).draggable(false)
                    markerOption!!.icon(bitmapDescriptorFromVector(HooleyGuestUser.activity, R.drawable.ic_free_event))
                    val marker = mGooglemap!!.addMarker(markerOption)
                    marker.tag = i
                }

            }
        }
    }

    private fun addPaidEventMarker() {
        if (mapFinalList != null && mapFinalList!!.size > 0) {
            for (i in mapFinalList!!.indices) {
                if (mapFinalList!![i].type == Constants.TYPE_PAID_EVENT) {
                    val loc = LatLng(mapFinalList!![i].eventDetail.eventLat!!, mapFinalList!![i].eventDetail.eventLong!!)
                    markerOption = MarkerOptions().position(loc).flat(false).draggable(false)
                    markerOption!!.icon(bitmapDescriptorFromVector(HooleyGuestUser.activity, R.drawable.ic_paid_event))
                    val marker = mGooglemap!!.addMarker(markerOption)
                    marker.tag = i
                }
            }
        }
    }

    private fun addFreePaidEventInFinalList() {
        if (mapFinalList != null)
            mapFinalList!!.clear()
        if (binding.tbfreeEvent.isChecked) {
            if (objectMapResponse != null) {
                if (objectMapResponse!!.freeEventsList.size > 0) {
                    for (i in objectMapResponse!!.freeEventsList.indices) {
                        mapFinalList!!.add(MapListFinalModel(objectMapResponse!!.freeEventsList[i], Constants.TYPE_FREE_EVENT))
                    }
                }
            } else {
                getNearByEventsMapFeeds(circleLat, circleLong, binding.sbGeofence.progress / 1609.34f)
            }
        }
        if (binding.tbPaidEvent.isChecked) {
            if (objectMapResponse != null) {
                if (objectMapResponse!!.paidEventsList.size > 0) {
                    for (i in objectMapResponse!!.paidEventsList.indices) {
                        mapFinalList!!.add(MapListFinalModel(objectMapResponse!!.paidEventsList[i], Constants.TYPE_PAID_EVENT))
                    }
                }
            } else {
                getNearByEventsMapFeeds(circleLat, circleLong, binding.sbGeofence.progress / 1609.34f)
            }
        }
    }

    private fun bitmapDescriptorFromVector(context: Context?, vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context!!, vectorResId)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        when (buttonView.id) {
            R.id.tbPaidEvent -> if (isChecked) {
                binding.tbPaidEvent.setTextColor(ContextCompat.getColor(HooleyGuestUser.activity, R.color.simple_white))
                binding.tbPaidEvent.typeface = bold
                mGooglemap!!.clear()
                addCircle(circleLat, circleLong)
                addFreePaidEventInFinalList()
                if (binding.tbfreeEvent.isChecked) {
                    addPaidEventMarker()
                    addFreeEventsMarker()
                } else {
                    addPaidEventMarker()
                }

            } else {
                binding.tbPaidEvent.setTextColor(ContextCompat.getColor(HooleyGuestUser.activity, R.color.app_purple_color))
                binding.tbPaidEvent.typeface = regular
                if (binding.tbfreeEvent.isChecked) {
                    mGooglemap!!.clear()
                    addFreeEventsMarker()
                    addCircle(circleLat, circleLong)
                } else {
                    mGooglemap!!.clear()
                    addCircle(circleLat, circleLong)
                }
                addFreePaidEventInFinalList()
            }
            R.id.tbfreeEvent -> if (isChecked) {
                binding.tbfreeEvent.setTextColor(ContextCompat.getColor(HooleyGuestUser.activity, R.color.simple_white))
                binding.tbfreeEvent.typeface = bold
                mGooglemap!!.clear()
                addCircle(circleLat, circleLong)
                addFreePaidEventInFinalList()
                if (binding.tbPaidEvent.isChecked) {
                    addPaidEventMarker()
                    addFreeEventsMarker()
                } else {
                    addFreeEventsMarker()
                }

            } else {
                binding.tbfreeEvent.setTextColor(ContextCompat.getColor(HooleyGuestUser.activity, R.color.app_purple_color))
                binding.tbfreeEvent.typeface = regular
                addFreePaidEventInFinalList()

                if (binding.tbPaidEvent.isChecked) {
                    mGooglemap!!.clear()
                    addPaidEventMarker()
                    addCircle(circleLat, circleLong)
                } else {
                    mGooglemap!!.clear()
                    addCircle(circleLat, circleLong)
                }
            }
        }
    }


    override fun onResume() {
        super.onResume()
        if (mGoogleApiClient!!.isConnected) {
            startLocationUpdates()
        }
        if (mGooglemap != null)
            mGooglemap!!.clear()
        binding.sbGeofence.progress = 8046
        binding.tvSbProgress.text = "5 miles"
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivMyLocation -> {
                binding.rlTop.visibility = View.VISIBLE
                moveToCurrentLocation()
            }

            R.id.ivDrawCircle -> {
                binding.rlTop.visibility = View.VISIBLE
                mGooglemap!!.clear()
                addCircle(circleLat, circleLong)
                val loc = LatLng(circleLat, circleLong)
                if (mGooglemap != null) {
                    mGooglemap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, mGooglemap!!.cameraPosition.zoom))
                }
                getNearByEventsMapFeeds(circleLat, circleLong, binding.sbGeofence.progress / 100.0f)
            }
            R.id.ivMapInfo -> showInfoDialog()
            R.id.rlHeader -> HooleyGuestUser.activity.DialogGuestUser()

            R.id.ivMinus -> if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (currentProgress > 100.0f) {
                    var temp = currentProgress
                    temp -= 100f
                    if (temp <= 100.0f) {
                        currentProgress = 100.0f
                    } else {
                        currentProgress -= 100f
                    }
                    binding.sbGeofence.progress = currentProgress.toInt()
                    updateUI()
                }
            } else {
                settingsRequest()
                Util.showToastMessage(HooleyGuestUser.activity, "Please turn on your location!")
            }
            R.id.ivPlus -> if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (currentProgress < 64373) {
                    currentProgress += 100f
                    binding.sbGeofence.progress = currentProgress.toInt()
                    updateUI()
                }
            } else {
                settingsRequest()
                Util.showToastMessage(HooleyGuestUser.activity, "Please turn on your location!")
            }
        }
    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        when (checkedId) {
            R.id.rbListView -> {
                type = Constants.TYPE_LIST_VIEW
                setTabs()
            }
            R.id.rbMapView -> {
                type = Constants.TYPE_MAP_VIEW
                setTabs()
            }
        }
    }

    private fun setTabs() {
        when (type) {
            Constants.TYPE_LIST_VIEW -> {
                binding.rlHeader.visibility = View.VISIBLE
                binding.sLRecycler.visibility = View.VISIBLE
                binding.rbListView.typeface = bold
                binding.rbMapView.typeface = regular
                binding.rbListView.setTextColor(ContextCompat.getColor(HooleyGuestUser.activity, android.R.color.white))
                binding.rbListView.background = ContextCompat.getDrawable(HooleyGuestUser.activity, R.drawable.bg_btn_left_radio)
                binding.rbMapView.setTextColor(ContextCompat.getColor(HooleyGuestUser.activity, R.color.app_purple_color))
                binding.rbMapView.setBackgroundColor(ContextCompat.getColor(HooleyGuestUser.activity, android.R.color.transparent))
                binding.rvNearBy.visibility = View.VISIBLE
                binding.rlMapView.visibility = View.GONE
            }
            Constants.TYPE_MAP_VIEW -> {
                binding.rlHeader.visibility = View.GONE
                binding.sLRecycler.visibility = View.GONE
                binding.rbListView.typeface = regular
                binding.rbMapView.typeface = bold
                binding.rbMapView.setTextColor(ContextCompat.getColor(HooleyGuestUser.activity, android.R.color.white))
                binding.rbMapView.background = ContextCompat.getDrawable(HooleyGuestUser.activity, R.drawable.bg_btn_right_radio)
                binding.rbListView.setTextColor(ContextCompat.getColor(HooleyGuestUser.activity, R.color.app_purple_color))
                binding.rbListView.setBackgroundColor(ContextCompat.getColor(HooleyGuestUser.activity, android.R.color.transparent))
                binding.rvNearBy.visibility = View.GONE
                binding.rlMapView.visibility = View.VISIBLE
                binding.tvNoData.visibility = View.GONE
                if (displayInfoDialog) {
                    displayInfoDialog = false
                    showInfoDialog()
                }
            }
        }

    }

    override fun onMarkerClick(marker: Marker): Boolean {
        var pos = if (marker.tag == null)
            0
        else
            marker.tag as Int
//        (HooleyGuestUser.activity as ActivityBase).addFragment(R.id.container, EventPagerFragment.newInstance(mapFinalList!!, pos), "EventPagerFragment")
        var eventsCardFragment = EventPagerFragment.newInstance(mapFinalList!!, pos)
        eventsCardFragment.show(HooleyGuestUser.activity.supportFragmentManager)
//        hideDrawer(HooleyGuestUser.activity)
        return false
    }

    override fun onStart() {
        super.onStart()
        mGoogleApiClient!!.connect()

    }

    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient!!.isConnected) {
            mGoogleApiClient!!.disconnect()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
    }

    override fun onClickLike(nearByEventsFinalModel: NearByEventsFinalModel, pos: Int, listPosition: Int) {
        HooleyGuestUser.activity.DialogGuestUser()

    }

    override fun onClickComment() {}

    override fun onClickFollow(nearByEventsFinalModel: NearByEventsFinalModel, pos: Int, listPosition: Int) {
        HooleyGuestUser.activity.DialogGuestUser()
    }

    override fun onClickShare(nearByEventsFinalModel: NearByEventsFinalModel, pos: Int) {
        HooleyGuestUser.activity.DialogGuestUser()
    }


    private fun showInfoDialog() {
        showInfoDialog = Dialog(HooleyGuestUser.activity)
        showInfoDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        showInfoDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        showInfoDialog!!.setContentView(R.layout.dialog_map_info)
        val ivClose = showInfoDialog!!.findViewById<ImageView>(R.id.ivClose)
        val tvText = showInfoDialog!!.findViewById<TextView>(R.id.tvText)
        tvText.text = HooleyGuestUser.activity.getString(R.string.str_map_near_by)
        ivClose.setOnClickListener { showInfoDialog!!.dismiss() }
        showInfoDialog!!.show()
    }

    override fun onUpComingEvent() {
        // unused

    }

    override fun onPastEvent() {
        // unused

    }

    override fun onMapLongClick(latLng: LatLng) {
        circleLat = latLng.latitude
        circleLong = latLng.longitude
        mGooglemap!!.clear()
        addCircle(circleLat, circleLong)
        mGooglemap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11.5f))
        getNearByEventsMapFeeds(circleLat, circleLong, binding.sbGeofence.progress / 1609.34f)
        binding.tvLocationName.text = ((HooleyGuestUser.activity as ActivityBase).getCompleteAddressString(circleLat, circleLong))
    }

    override fun onRefresh() {
        if (binding.sLRecycler != null) {
            binding.sLRecycler.isRefreshing = true
            binding.pbNearBy.visibility = View.GONE
        }
        isSwiped = true
        getNearByEventsListFeeds()
    }

    companion object {
        val MY_PERMISSIONS_REQUEST_LOCATION = 99
        val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 1000
        val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2
        protected val REQUEST_CHECK_SETTINGS = 0x1
    }
}
