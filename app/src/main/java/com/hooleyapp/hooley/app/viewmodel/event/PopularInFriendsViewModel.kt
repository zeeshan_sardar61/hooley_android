package com.hooleyapp.hooley.app.viewmodel.event

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.hooleyapp.hooley.app.data.model.event.responseModel.PopularInFriendsModel
import com.hooleyapp.hooley.app.data.repository.event.PopularInFriendsRepository
import com.hooleyapp.hooley.app.persistance.local.HooleyDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext


class PopularInFriendsViewModel(application: Application) : AndroidViewModel(application) {
    private var parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    private val repository: PopularInFriendsRepository
    var popularInFriendsEventList: MutableLiveData<List<PopularInFriendsModel.PopularInFriends>>
    var failureMessage: MutableLiveData<String>
    var sessionExpire: MutableLiveData<Boolean>
    var loading: MutableLiveData<Boolean> = MutableLiveData()

    init {
        val poplarInFriendsDao = HooleyDatabase.getDatabase(application).popularInFriendsDao()
        repository = PopularInFriendsRepository(poplarInFriendsDao)
        popularInFriendsEventList = repository.allevents
        failureMessage = repository.failureMessage
        sessionExpire = repository.sessionExpire
        loading = repository.loading
        repository.getPopularInFriends(false)
    }

    fun insert(popularInFriends: PopularInFriendsModel.PopularInFriends) = scope.launch(Dispatchers.IO) {
        repository.insert(popularInFriends)
    }

    fun filterListByAll() {
        var filterList = repository.tempFilterList.value?.filterNot {
            it.isPast
        }
        popularInFriendsEventList.postValue(null)
        popularInFriendsEventList.postValue(filterList)
    }

    fun filterListPast() {
        var filterList = repository.tempFilterList.value?.filter {
            it.isPast
        }
        popularInFriendsEventList.postValue(null)
        popularInFriendsEventList.postValue(filterList)
    }

    fun onRefreshList() {
        repository.getPopularInFriends(true)
    }

    fun likeEvent(position: Int) {
        repository.likeEvent(position)
    }

    fun followEvent(position: Int) {
        repository.followEvent(position)
    }

    fun setSelectetab(value: Int) {
        repository.selectedTab = value
    }

    fun shareEvent(shareMedia: String, position: Int) {
        repository.shareEvent(shareMedia, position)
    }

    override fun onCleared() {
        super.onCleared()
        parentJob.cancel()
    }

}