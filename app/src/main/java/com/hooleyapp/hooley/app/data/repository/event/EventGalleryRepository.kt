package com.hooleyapp.hooley.app.data.repository.event

import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.helper.SingleLiveEvent
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GetReportEventModel
import com.hooleyapp.hooley.model.PersonalProfileModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.GuestUserWebService
import com.hooleyapp.hooley.services.MediaWebService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException
import java.util.*

class EventGalleryRepository {

    var mList: SingleLiveEvent<ArrayList<EventGalleryFeedModel.EventMediaInfo>> = SingleLiveEvent()
    var tempList: SingleLiveEvent<ArrayList<EventGalleryFeedModel.EventMediaInfo>> = SingleLiveEvent()
    var failureMessage: SingleLiveEvent<String> = SingleLiveEvent()
    var sessionExpire: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var reloadApi: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var loading: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var reportEventList: SingleLiveEvent<List<PersonalProfileModel.myObject>> = SingleLiveEvent()
    var mediaService = MediaWebService()
    var mService = GuestUserWebService()
    private var apiCall: Call<EventGalleryFeedModel>? = null

    // NetWork Request
    fun getEventGalleryFeeds(eventId: String, type: String) {
        loading.postValue(true)
        getEventGalleryFeeds(eventId, type, object : IWebServiceCallback<EventGalleryFeedModel> {
            override fun success(result: EventGalleryFeedModel) {
                loading.postValue(false)
                mList.postValue(null)
                mList.postValue(result.eventMediaInfoList)
                tempList.postValue(null)
                tempList.postValue(result.eventMediaInfoList)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                if (message.isNotBlank()) {
                    failureMessage.postValue(null)
                    failureMessage.postValue(message)
                }
                mList.postValue(null)
                mList.postValue(arrayListOf())
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })
    }

    fun seeGuestUserEventGalleryFeeds(eventId: String, type: String) {
        loading.postValue(true)
        mService.seeGuestUserEventGalleryFeeds(eventId, type, object : IWebServiceCallback<EventGalleryFeedModel> {
            override fun success(result: EventGalleryFeedModel) {
                loading.postValue(false)
                mList.postValue(null)
                mList.postValue(result.eventMediaInfoList)
                tempList.postValue(null)
                tempList.postValue(result.eventMediaInfoList)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                if (message.isNotBlank())
                    failureMessage.postValue(message)
                mList.postValue(null)
                mList.postValue(arrayListOf())
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })
    }

    fun likePost(eventId: String, imageId: String) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        mediaService.likePost(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                reloadApi.postValue(true)
            }

            override fun failure(message: String) {
                failureMessage.postValue(message)
            }
        }, eventId, imageId)
    }

    fun mediaFav(eventId: String, imageId: String, isFav: Boolean) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        mediaService.mediaFav(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                reloadApi.postValue(true)
            }

            override fun failure(message: String) {
                failureMessage.postValue(message)

            }
        }, eventId, imageId, isFav)
    }

    fun deleteMedia(imageId: String, eventId: String, position: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        mediaService.deleteMedia(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                reloadApi.postValue(true)
            }

            override fun failure(message: String) {
                failureMessage.postValue(message)
            }
        }, imageId, eventId)
    }

    fun submitReport(eventId: String, reasonId: String, reportMessage: String, mediaId: String) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        loading.postValue(true)
        mediaService.submitReport(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                loading.postValue(false)
            }

            override fun failure(message: String) {
                loading.postValue(false)
            }
        }, reasonId, reportMessage, mediaId, eventId)
    }

    fun getMediaReportEvent() {
        loading.postValue(true)
        mediaService.getMediaReportEvent(object : GenericCallback<GetReportEventModel> {
            override fun success(result: GetReportEventModel) {
                reportEventList.postValue(result.mediaReportReasonsArrayList)
            }

            override fun failure(message: String) {
                failureMessage.postValue(message)
            }
        })
    }

    fun shareEventMedia(eventId: String?, imageId: String, shareMedia: String, allFeedItemPosition: Int) {
        if (!NetworkUtil.isInternetConnected(ActivityBase.activity)) {
            Util.showToastMessage(ActivityBase.activity, Constants.NO_NETWORK)
            return
        }
        mediaService.shareEventMedia(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                tempList.value!![allFeedItemPosition].shareCounts = tempList.value!![allFeedItemPosition].shareCounts + 1
                mList.postValue(null)
                mList.postValue(tempList.value)

            }

            override fun failure(message: String) {
                failureMessage.postValue(message)
            }
        }, eventId!!, imageId, shareMedia)

    }

    // retofit call
    fun getEventGalleryFeeds(eventId: String, type: String, callback: IWebServiceCallback<EventGalleryFeedModel>) {
        if (apiCall != null)
            apiCall?.cancel()
        apiCall = HooleyApp.apiService.seeEventGalleryFeeds(HooleyApp.db.getString(Constants.USER_ID), eventId, type)
        apiCall?.enqueue(object : Callback<EventGalleryFeedModel> {
            override fun onResponse(call: Call<EventGalleryFeedModel>, response: Response<EventGalleryFeedModel>) {
                if (response.body() == null) {
                    if (response.errorBody() != null)
                        callback.failure(response.errorBody().toString())
                    else
                        callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<EventGalleryFeedModel>, t: Throwable) {
                if (call.isCanceled) {
                    callback.failure("")
                    return
                }
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

}