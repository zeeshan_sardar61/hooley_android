package com.hooleyapp.hooley.app.viewmodel.event

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.app.data.repository.event.EventGalleryRepository
import com.hooleyapp.hooley.helper.SingleLiveEvent
import com.hooleyapp.hooley.model.PersonalProfileModel

class EventGalleryViewModel(application: Application) : AndroidViewModel(application) {

    var repository = EventGalleryRepository()

    var mList: SingleLiveEvent<ArrayList<EventGalleryFeedModel.EventMediaInfo>> = SingleLiveEvent()
    var failureMessage: SingleLiveEvent<String> = SingleLiveEvent()
    var sessionExpire: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var reloadApi: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var loading: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var reportEventList: SingleLiveEvent<List<PersonalProfileModel.myObject>> = SingleLiveEvent()
    var type: String = ""

    init {
        mList = repository.mList
        failureMessage = repository.failureMessage
        sessionExpire = repository.sessionExpire
        reloadApi = repository.reloadApi
        loading = repository.loading
        reportEventList = repository.reportEventList
    }

    fun getEventGalleryFeeds(eventId: String, type_: String) {
        repository.getEventGalleryFeeds(eventId, type_)
        type = type_
    }

    fun getGuestUserEventGalleryFeeds(eventId: String, type_: String) {
        repository.seeGuestUserEventGalleryFeeds(eventId, type_)
        type = type_
    }

    fun likePost(eventId: String, imageId: String) {
        repository.likePost(eventId, imageId)
    }

    fun mediaFav(eventId: String, imageId: String, isFav: Boolean) {
        repository.mediaFav(eventId, imageId, isFav)
        getEventGalleryFeeds(eventId, type)
    }

    fun deleteMedia(imageId: String, eventId: String, position: Int) {
        repository.deleteMedia(imageId, eventId, position)
    }

    fun submitReport(eventId: String, reasonId: String, reportMessage: String, mediaId: String) {
        repository.submitReport(eventId, reasonId, reportMessage, mediaId)
    }


    fun shareEventMedia(eventId: String?, imageId: String, shareMedia: String, allFeedItemPosition: Int) {
        repository.shareEventMedia(eventId, imageId, shareMedia, allFeedItemPosition)
    }

    fun getMediaReportEvent() {
        repository.getMediaReportEvent()
    }

    // filter
    fun filterByAll() {

        mList.postValue(repository.tempList.value)
    }

    fun filterByFav() {
        var list = repository.tempList.value?.filter {
            it.isFavorited
        }

        mList.postValue(ArrayList(list))
    }

    fun filterByPhotos() {
        var mainList = arrayListOf<EventGalleryFeedModel.EventMediaInfo>()
        for (i in 0 until repository.tempList.value!!.size) {
            var mediaList = repository.tempList.value!![i].mediaFiles.filterNot { it.isVideo!! }
            if (mediaList.isNotEmpty()) {
                var item = EventGalleryFeedModel.EventMediaInfo()
                item.imageId = repository.tempList.value!![i].imageId
                item.userId = repository.tempList.value!![i].userId
                item.fullName = repository.tempList.value!![i].fullName
                item.eventName = repository.tempList.value!![i].eventName
                item.imageCaption = repository.tempList.value!![i].imageCaption
                item.postedTime = repository.tempList.value!![i].postedTime
                item.profilePic = repository.tempList.value!![i].profilePic
                item.eventId = repository.tempList.value!![i].eventId
                item.commentsCount = repository.tempList.value!![i].commentsCount
                item.favoriteCount = repository.tempList.value!![i].favoriteCount
                item.userId = repository.tempList.value!![i].userId
                item.likesCount = repository.tempList.value!![i].likesCount
                item.isLiked = repository.tempList.value!![i].isLiked
                item.isLive = repository.tempList.value!![i].isLive
                item.isFavorited = repository.tempList.value!![i].isFavorited
                item.shareCounts = repository.tempList.value!![i].shareCounts
                item.partialComments.addAll(repository.tempList.value!![i].partialComments)
                item.mediaFiles.addAll((ArrayList(mediaList)))
                mainList.add(item)
            }
        }
        mList.postValue(ArrayList(mainList))
    }

    fun filterByVideo() {
        var mainList = arrayListOf<EventGalleryFeedModel.EventMediaInfo>()
        for (i in 0 until repository.tempList.value!!.size) {
            var mediaList = repository.tempList.value!![i].mediaFiles.filter { it.isVideo!! }
            if (mediaList.isNotEmpty()) {
                var item = EventGalleryFeedModel.EventMediaInfo()
                item.imageId = repository.tempList.value!![i].imageId
                item.userId = repository.tempList.value!![i].userId
                item.fullName = repository.tempList.value!![i].fullName
                item.eventName = repository.tempList.value!![i].eventName
                item.imageCaption = repository.tempList.value!![i].imageCaption
                item.postedTime = repository.tempList.value!![i].postedTime
                item.profilePic = repository.tempList.value!![i].profilePic
                item.eventId = repository.tempList.value!![i].eventId
                item.commentsCount = repository.tempList.value!![i].commentsCount
                item.favoriteCount = repository.tempList.value!![i].favoriteCount
                item.userId = repository.tempList.value!![i].userId
                item.likesCount = repository.tempList.value!![i].likesCount
                item.isLiked = repository.tempList.value!![i].isLiked
                item.isLive = repository.tempList.value!![i].isLive
                item.isFavorited = repository.tempList.value!![i].isFavorited
                item.shareCounts = repository.tempList.value!![i].shareCounts
                item.partialComments.addAll(repository.tempList.value!![i].partialComments)
                item.mediaFiles.addAll((ArrayList(mediaList)))
                mainList.add(item)
            }
        }

        mList.postValue(ArrayList(mainList))
    }
}