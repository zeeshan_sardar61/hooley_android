package com.hooleyapp.hooley.app.data.repository.event

import android.arch.lifecycle.MutableLiveData
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventGuestListModel
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.EventWebService
import com.hooleyapp.hooley.services.FriendWebService
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

class EventDetailGuestListRepository {

    var failureMessage: MutableLiveData<String> = MutableLiveData()
    var sessionExpire: MutableLiveData<Boolean> = MutableLiveData()
    var loading: MutableLiveData<Boolean> = MutableLiveData()
    var reloadApi: MutableLiveData<Boolean> = MutableLiveData()
    var attendingAndInvitedList: MutableLiveData<List<FriendsTable>> = MutableLiveData()
    var attendingAndInvitedTempList: MutableLiveData<List<FriendsTable>> = MutableLiveData()
    var service = EventWebService()
    var friendService = FriendWebService()


    // Network Request
    fun getEventGuestList(eventId: String, type: String) {
        loading.postValue(true)
        service.getEventGuestList(eventId, type, object : IWebServiceCallback<EventGuestListModel> {
            override fun success(result: EventGuestListModel) {
                loading.postValue(false)
                when (type) {
                    Constants.TYPE_ATTENDING -> {
                        Collections.sort(result.attendingAndInvitedList, FriendsTable.sortByNameComparator)
                        attendingAndInvitedList.postValue(null)
                        attendingAndInvitedList.postValue(result.attendingAndInvitedList)
                    }
                    Constants.TYPE_FOLLOWING -> {
                        Collections.sort(result.attendingAndInvitedList, FriendsTable.sortByNameComparator)
                        attendingAndInvitedList.postValue(null)
                        attendingAndInvitedList.postValue(result.attendingAndInvitedList)
                    }
                    Constants.TYPE_INVITED -> {
                        var userList = result.attendingAndInvitedList.filterNot {
                            it.status == Constants.NO_HOOLEY_USER
                        }
                        var contactList = result.attendingAndInvitedList.filter {
                            it.status == Constants.NO_HOOLEY_USER
                        }
                        Collections.sort(userList, FriendsTable.sortByNameComparator)
                        Collections.sort(contactList, FriendsTable.sortByNameComparator)
                        var list = ArrayList<FriendsTable>()
                        list.addAll(ArrayList(userList))
                        list.addAll(ArrayList(contactList))
                        attendingAndInvitedList.postValue(null)
                        attendingAndInvitedList.postValue(list)
                    }
                    else -> {
                        Collections.sort(result.attendingAndInvitedList, FriendsTable.sortByNameComparator)
                        attendingAndInvitedList.postValue(null)
                        attendingAndInvitedList.postValue(result.attendingAndInvitedList)
                    }
                }

                attendingAndInvitedTempList.postValue(null)
                attendingAndInvitedTempList.postValue(result.attendingAndInvitedList)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })
    }

    fun sendFriendRequest(friendId: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        val friendList = java.util.ArrayList<Int>()
        friendList.add(friendId)
        friendService.sendFriendRequest(friendList, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                Util.showToastMessage(HooleyMain.activity!!, "Friend Request Sent")
                reloadApi.postValue(true)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }
        })
    }

    fun acceptIgnoreFriendRequest(userId: Int, isAccepted: Boolean) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        friendService.acceptIgnoreFriendRequest(userId, isAccepted, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                loading.postValue(false)
                reloadApi.postValue(true)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }
        })
    }

}