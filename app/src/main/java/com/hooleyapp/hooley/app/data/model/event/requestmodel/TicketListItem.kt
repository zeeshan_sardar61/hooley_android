package com.hooleyapp.hooley.app.data.model.event.requestmodel

import com.google.gson.annotations.SerializedName

data class TicketListItem(

        @field:SerializedName("quantity")
        val quantity: Int? = null,

        @field:SerializedName("price")
        val price: Int? = null,

        @field:SerializedName("ticketId")
        val ticketId: Int? = null,

        @field:SerializedName("ticketName")
        val ticketName: String? = null
)