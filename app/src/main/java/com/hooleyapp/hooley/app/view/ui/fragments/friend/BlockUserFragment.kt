package com.hooleyapp.hooley.app.view.ui.fragments.friend

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.view.adapter.BlockUserAdapter
import com.hooleyapp.hooley.app.view.callback.IBlockClickListener
import com.hooleyapp.hooley.app.viewmodel.friend.BlockUserViewModel
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.showSuccessDialog

class BlockUserFragment : BaseFragment(), IBlockClickListener {

    private var adapterBlockUser: BlockUserAdapter? = null
    lateinit var binding: com.hooleyapp.hooley.databinding.FragmentBlockUserBinding
    lateinit var blockUserViewModel: BlockUserViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_block_user, container, false)
        blockUserViewModel = ViewModelProviders.of(this).get(BlockUserViewModel::class.java)
        setUiObserver()
        setAdapter()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        blockUserViewModel.getBlockUserList()
    }

    fun setUiObserver() {
        blockUserViewModel.loading.observe(this, Observer {
            it?.let { showLoader ->
                if (showLoader)
                    binding.pbBlockUser.visibility = View.VISIBLE
                else
                    binding.pbBlockUser.visibility = View.GONE
            }
        })

        blockUserViewModel.failureMessage.observe(this, Observer {
            it?.let { message ->
                ActivityBase.activity.showSuccessDialog(message)
            }
        })
        blockUserViewModel.reloadApi.observe(this, Observer {
            it?.let { reload ->
                if (reload)
                    blockUserViewModel.getBlockUserList()
            }
        })

        blockUserViewModel.mList.observe(this, Observer {
            it?.let { list ->
                adapterBlockUser?.setList(list)
            }
        })
    }

    override fun onClickUnBlockUser(position: Int) {

        blockUserViewModel.unBlockUser(blockUserViewModel.mList.value!![position].userId!!)
    }

    fun setAdapter() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity)
        binding.rvBlockUser.layoutAnimation = animation
        binding.rvBlockUser.layoutManager = manager
        binding.rvBlockUser.setEmptyView(binding.tvNoData)
        adapterBlockUser = BlockUserAdapter(HooleyMain.activity!!)
        adapterBlockUser?.mListener = this
        binding.rvBlockUser.adapter = adapterBlockUser
    }
}