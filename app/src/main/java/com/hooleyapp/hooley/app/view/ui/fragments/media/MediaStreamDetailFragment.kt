package com.hooleyapp.hooley.app.view.ui.fragments.media

import android.annotation.SuppressLint
import android.app.Dialog
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.AnimationUtils
import android.widget.*
import com.felipecsl.asymmetricgridview.AsymmetricRecyclerViewAdapter
import com.felipecsl.asymmetricgridview.Utils
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterMoreMediaDialoge
import com.hooleyapp.hooley.adapters.AdapterSpinner
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.app.view.adapter.EventFeedTrendingAdapter
import com.hooleyapp.hooley.app.view.adapter.MediaStreamAdapter
import com.hooleyapp.hooley.app.viewmodel.media.MediaStreamDetailViewModel
import com.hooleyapp.hooley.databinding.FragmentMediaStreamDetailBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.event.LiveEventFragment
import com.hooleyapp.hooley.fragments.event.UpdatePostFragment
import com.hooleyapp.hooley.fragments.media.AllFeedsSinglePostCommentFragment
import com.hooleyapp.hooley.fragments.media.ViewPostMediaFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.fragments.others.MoreMediaDialogFragment
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.interfaces.IEventAllFeedsClick
import com.hooleyapp.hooley.model.PersonalProfileModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.GetBitmapTask
import com.hooleyapp.hooley.others.MessageEvent
import com.hooleyapp.hooley.others.SpacesItemDecoration
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*

/**
 * Created by Adil Malik on 12-Mar-18.
 */

@SuppressLint("ValidFragment")
class MediaStreamDetailFragment constructor(var eventId: String, var eventName: String, var isLive: Boolean) : BaseFragment(), RadioGroup.OnCheckedChangeListener, View.OnClickListener, IEventAllFeedsClick {
    internal lateinit var binding: FragmentMediaStreamDetailBinding
    lateinit var mediaStreamDetailViewModel: MediaStreamDetailViewModel
    private var adapterMediaStreamTrending: EventFeedTrendingAdapter? = null
    private var moreTrendingDialogeFragment: MoreMediaDialogFragment? = null
    private var adapterMediaStream: MediaStreamAdapter? = null
    private var morePostDialogeFragment: MoreDialogeFragment? = null
    private var showMediaReportEventDialog: Dialog? = null
    private var spinnerAdapter: AdapterSpinner? = null
    private var reasonMediaItemPosition = 0
    private var pagerPosition = 0
    private var allFeedItemPosition: Int = 0
    private lateinit var imageId: String
    internal val mediaList = ArrayList<AdapterMoreMediaDialoge.MoreItem>()
    var type = Constants.MEDIA_STREAM_POST
    var boldFont: Typeface
    var regularFont: Typeface

    init {
        boldFont = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)!!
        regularFont = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_media_stream_detail, container, false)
        mediaStreamDetailViewModel = ViewModelProviders.of(this).get(MediaStreamDetailViewModel::class.java)
        setUiObserver()
        setListener()
        setTabs()
        setHeader()
        return binding.root
    }

    private fun setListener() {
        binding.myEventsTab.setOnCheckedChangeListener(this)
        binding.ivFilter.setOnClickListener(this)
        binding.rlLive.setOnClickListener(this)
    }

    fun setUiObserver() {
        mediaStreamDetailViewModel.sessionExpire.observe(this, android.arch.lifecycle.Observer { onTokenExpireLogOut() })
        mediaStreamDetailViewModel.failureMessage.observe(this, android.arch.lifecycle.Observer { it?.let { text -> { binding.tvNoData.text = text } } })
        mediaStreamDetailViewModel.loading.observe(this, android.arch.lifecycle.Observer { if (it!!) binding.pbMediaStream.visibility = View.VISIBLE else binding.pbMediaStream.visibility = View.GONE })
        mediaStreamDetailViewModel.mList.observe(this, android.arch.lifecycle.Observer {
            it?.let { list ->
                when (type) {
                    Constants.MEDIA_STREAM_POST -> {
                        setMediaStreamAdapter(list)
                    }
                    Constants.MEDIA_STREAM_TRENDING -> {
                        setMediaStreamTrendingAdapter(list)
                    }
                }
            }
        })
        mediaStreamDetailViewModel.reportEventList.observe(this, android.arch.lifecycle.Observer {
            it?.let { list ->
                showReportEventDialog(ArrayList(list))
            }
        })
    }

    fun setHeader() {
        if (isLive) {
            binding.ivLiveCircle.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_circle_live)
            binding.rlLive.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.live_orange))
            binding.tvLiveStatus.text = "CLICK TO LIVE EVENT"
        } else {
            binding.ivLiveCircle.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_circle_orange)
            binding.rlLive.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.live_red))
            binding.tvLiveStatus.text = "Event Ended - Tap to View"
        }
    }

    private fun setMediaStreamAdapter(list: ArrayList<EventGalleryFeedModel.EventMediaInfo>) {
        binding.rvMediaStream.visibility = View.VISIBLE
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding.rvMediaStream.layoutManager = manager
        binding.rvMediaStream.setEmptyView(binding.tvNoData)
        binding.rvMediaStream.layoutAnimation = animation
        adapterMediaStream = MediaStreamAdapter(HooleyMain.activity!!, list)
        adapterMediaStream?.mListener = this
        binding.rvMediaStream.adapter = adapterMediaStream
    }

    private fun setMediaStreamTrendingAdapter(list: ArrayList<EventGalleryFeedModel.EventMediaInfo>) {
        binding.rvTrending.visibility = View.VISIBLE
        binding.rvTrending.setRequestedColumnCount(3)
        binding.rvTrending.isDebugging = false
        binding.rvTrending.requestedHorizontalSpacing = Utils.dpToPx(HooleyMain.activity!!, 3f)
        if (adapterMediaStreamTrending == null)
            binding.rvTrending.addItemDecoration(SpacesItemDecoration(HooleyMain.activity!!.resources.getDimensionPixelSize(R.dimen.recycler_padding)))
        adapterMediaStreamTrending = EventFeedTrendingAdapter(list)
//        adapterMediaStreamTrending!!.setmListener(this)
        binding.rvTrending.adapter = AsymmetricRecyclerViewAdapter<RecyclerView.ViewHolder>(HooleyMain.activity, binding.rvTrending, adapterMediaStreamTrending!!)
    }

    private fun setMoreCallerBackListener() {
        if (mediaList.size > 0)
            mediaList.clear()
        val itemAll = AdapterMoreMediaDialoge.MoreItem("All", true)
        val itemFavorites = AdapterMoreMediaDialoge.MoreItem("Favorites", false)
        val itemPhotos = AdapterMoreMediaDialoge.MoreItem("Photos", false)
        val itemVideos = AdapterMoreMediaDialoge.MoreItem("Videos", false)
        val viewDetails = AdapterMoreMediaDialoge.MoreItem("View Details", false)
        mediaList.add(itemAll)
        mediaList.add(itemFavorites)
        mediaList.add(itemPhotos)
        mediaList.add(itemVideos)
        mediaList.add(viewDetails)
        moreTrendingDialogeFragment = MoreMediaDialogFragment.Builder(HooleyMain.activity!!).setTitle("Filter by").setType(Constants.TYPE_MORE_80).setList(mediaList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    mediaStreamDetailViewModel.filterByAll()
                    mediaList[0].isCheck = (true)
                    mediaList[1].isCheck = (false)
                    mediaList[2].isCheck = (false)
                    mediaList[3].isCheck = (false)
                    mediaList[4].isCheck = (false)
                    moreTrendingDialogeFragment!!.dismiss()
                }
                1 -> {
                    mediaStreamDetailViewModel.filterByFav()
                    mediaList[0].isCheck = (false)
                    mediaList[1].isCheck = (true)
                    mediaList[2].isCheck = (false)
                    mediaList[3].isCheck = (false)
                    mediaList[4].isCheck = (false)
                    moreTrendingDialogeFragment!!.dismiss()
                }
                2 -> {
                    mediaStreamDetailViewModel.filterByPhotos()
                    mediaList[0].isCheck = (false)
                    mediaList[1].isCheck = (false)
                    mediaList[2].isCheck = (true)
                    mediaList[3].isCheck = (false)
                    mediaList[4].isCheck = (false)
                    moreTrendingDialogeFragment!!.dismiss()
                }
                3 -> {
                    mediaStreamDetailViewModel.filterByVideo()
                    mediaList[0].isCheck = (false)
                    mediaList[1].isCheck = (false)
                    mediaList[2].isCheck = (false)
                    mediaList[3].isCheck = (true)
                    mediaList[4].isCheck = (false)
                    moreTrendingDialogeFragment!!.dismiss()
                }
                4 -> {
                    mediaList[0].isCheck = (false)
                    mediaList[1].isCheck = (false)
                    mediaList[2].isCheck = (false)
                    mediaList[3].isCheck = (false)
                    mediaList[4].isCheck = (true)
                    moreTrendingDialogeFragment!!.dismiss()
                    (HooleyMain.activity as ActivityBase).addFragment(R.id.container, LiveEventFragment
                            .newInstance(eventId), "LiveEventFragment")
                    hideDrawer(HooleyMain.activity!!)
                }
            }
        }.create()
    }

    private fun showReportEventDialog(list: ArrayList<PersonalProfileModel.myObject>) {
        showMediaReportEventDialog = Dialog(HooleyMain.activity!!)
        showMediaReportEventDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        showMediaReportEventDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        showMediaReportEventDialog!!.setContentView(R.layout.dialog_report_event)
        val btnReport = showMediaReportEventDialog!!.findViewById<TextView>(R.id.btnReport)
        val edtMessage = showMediaReportEventDialog!!.findViewById<EditText>(R.id.edtMessage)
        val spSelectReason = showMediaReportEventDialog!!.findViewById<Spinner>(R.id.spSelectReason)
        spinnerAdapter = AdapterSpinner(activity, R.layout.sp_gender_group_item, list)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spSelectReason.adapter = spinnerAdapter
        spSelectReason.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                reasonMediaItemPosition = position
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
        btnReport.setOnClickListener {
            showMediaReportEventDialog!!.dismiss()
            mediaStreamDetailViewModel.submitReport(eventId, list[reasonMediaItemPosition].id!!, edtMessage.text.toString(), imageId)
        }
        showMediaReportEventDialog!!.show()
    }

    fun setTabs() {
        when (type) {
            Constants.MEDIA_STREAM_POST -> {
                setMoreCallerBackListener()
                binding.rvMediaStream.visibility = View.GONE
                binding.rvTrending.visibility = View.GONE
                binding.rbMediaList.typeface = boldFont
                binding.rbTrending.typeface = regularFont
                binding.rbMediaList.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding.rbMediaList.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_rb_default)
                binding.rbTrending.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbTrending.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                mediaStreamDetailViewModel.getMediaStream(eventId, type)
            }
            Constants.MEDIA_STREAM_TRENDING -> {
                setMoreCallerBackListener()
                binding.rvMediaStream.visibility = View.GONE
                binding.rvTrending.visibility = View.GONE
                binding.rbMediaList.typeface = regularFont
                binding.rbTrending.typeface = boldFont
                binding.rbTrending.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding.rbTrending.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_rb_default)
                binding.rbMediaList.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbMediaList.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                mediaStreamDetailViewModel.getMediaStream(eventId, type)
            }
        }
    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        when (checkedId) {
            R.id.rbMediaList -> {
                type = Constants.MEDIA_STREAM_POST
                setTabs()
            }
            R.id.rbTrending -> {
                type = Constants.MEDIA_STREAM_TRENDING
                setTabs()
            }
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivFilter -> {
                if (!moreTrendingDialogeFragment!!.isAdded)
                    moreTrendingDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
            }
            R.id.rlLive -> {
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment
                        .newInstance(eventId), "LiveEventFragment")
                hideDrawer(HooleyMain.activity!!)
            }
        }
    }

    private fun showShareEventDialog() {
        object : GetBitmapTask() {
            override fun onPostExecute(bmp: Bitmap) {
                super.onPostExecute(bmp)
                (HooleyMain.activity as HooleyMain).shareWithImage(bmp, mediaStreamDetailViewModel.mList.value!![allFeedItemPosition].eventName!!, mediaStreamDetailViewModel.mList.value!![allFeedItemPosition].imageCaption, mediaStreamDetailViewModel.mList.value!![allFeedItemPosition].eventId!!, mediaStreamDetailViewModel.mList.value!![allFeedItemPosition].imageId)
            }
        }.execute(mediaStreamDetailViewModel.mList.value!![allFeedItemPosition].mediaFiles[pagerPosition].thumbnailUrl)
    }

    override fun onClickPostComment(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, type: Int) {
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, AllFeedsSinglePostCommentFragment.newInstance(eventMediaInfo.eventId!!, eventMediaInfo.imageId), "AllFeedsSinglePostCommentFragment")
    }

    override fun onClickMore(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, pos: Int, listPosition: Int, mType: Int) {
        val arrayList = ArrayList<String>()
        if (eventMediaInfo.isFavorited)
            arrayList.add("Remove From Favorite")
        else
            arrayList.add("Add to Favorite")

        if (eventMediaInfo.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
            arrayList.add("Edit Photo")

        arrayList.add("Download/Share Photo")

        if (eventMediaInfo.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
            arrayList.add("Delete Photo")
        else
            arrayList.add("Report Photo")
        morePostDialogeFragment = MoreDialogeFragment.Builder(activity!!).setTitle("More").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    if (eventMediaInfo.isFavorited) {
                        mediaStreamDetailViewModel.mList.value!![pos].favoriteCount++
                        mediaStreamDetailViewModel.mList.value!![pos].isFavorited = true
                    } else {
                        mediaStreamDetailViewModel.mList.value!![pos].favoriteCount--
                        mediaStreamDetailViewModel.mList.value!![pos].isFavorited = false
                    }
                    if (adapterMediaStream != null)
                        adapterMediaStream?.notifyDataSetChanged()
                    mediaStreamDetailViewModel.mediaFav(eventMediaInfo.eventId!!, eventMediaInfo.imageId, !eventMediaInfo.isFavorited)
                    morePostDialogeFragment!!.dismiss()
                }
                1 -> {
                    morePostDialogeFragment!!.dismiss()
                    if (eventMediaInfo.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
                        (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, UpdatePostFragment(eventMediaInfo.eventId!!, eventMediaInfo.imageId), "UpdatePostFragment")
                    else {
                        imageId = eventMediaInfo.imageId
                        eventId = eventMediaInfo.eventId!!
                        mediaStreamDetailViewModel.getMediaReportEvent()
                    }
                }
                2 -> {
                    morePostDialogeFragment!!.dismiss()
                    onClickShare(eventMediaInfo.imageId, position, listPosition, mType)
                }
                3 -> {
                    morePostDialogeFragment!!.dismiss()
                    if (eventMediaInfo.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
                        mediaStreamDetailViewModel.deleteMedia(eventMediaInfo.imageId, eventMediaInfo.eventId!!)
                    else {
                        imageId = eventMediaInfo.imageId
                        eventId = eventMediaInfo.eventId!!
                        mediaStreamDetailViewModel.getMediaReportEvent()
                    }
                }
            }
        }.create()

        if (!morePostDialogeFragment!!.isAdded)
            morePostDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
    }

    override fun onClickImageView(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo) {
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ViewPostMediaFragment.newInstance(eventMediaInfo), "ViewPostMediaFragment")
    }

    override fun onClickLikePost(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, position: Int, type: Int) {
        mediaStreamDetailViewModel.likePost(eventMediaInfo.eventId!!, eventMediaInfo.imageId)
    }

    override fun onClickFav(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, position: Int, type: Int) {
        mediaStreamDetailViewModel.mediaFav(eventMediaInfo.eventId!!, eventMediaInfo.imageId, !eventMediaInfo.isFavorited)

    }

    override fun onClickShare(imageId: String, position: Int, listPosition: Int, type: Int) {
        allFeedItemPosition = position
        pagerPosition = listPosition
        showShareEventDialog()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        Toast.makeText(HooleyMain.activity, event.message, Toast.LENGTH_SHORT).show()
        mediaStreamDetailViewModel.shareEventMedia(mediaStreamDetailViewModel.mList.value!![allFeedItemPosition].eventId, mediaStreamDetailViewModel.mList.value!![allFeedItemPosition].imageId, event.message, allFeedItemPosition)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        setTabs()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
    }
}
