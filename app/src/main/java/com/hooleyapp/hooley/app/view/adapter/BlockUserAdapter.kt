package com.hooleyapp.hooley.app.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.Friend.BlockUserListModel
import com.hooleyapp.hooley.app.view.callback.IBlockClickListener
import com.hooleyapp.hooley.databinding.AdapterBlockUserBinding
import com.hooleyapp.hooley.helper.YesNoDialogBlock
import com.hooleyapp.hooley.helper.load
import com.hooleyapp.hooley.helper.onClickDialog
import com.hooleyapp.hooley.others.DateUtils

class BlockUserAdapter(private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal lateinit var binding: AdapterBlockUserBinding
    private val viewBinderHelper = ViewBinderHelper()
    internal var mList = listOf<BlockUserListModel.BlockUser>()
    lateinit var mListener: IBlockClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_block_user, parent, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            holder.bindWithHolder(mList[position])
        } catch (e: Exception) {
            e.printStackTrace()
        }
        // setting click listener
        holder.binding.rlBlock.setOnClickListener {
            HooleyMain.activity!!.YesNoDialogBlock("Are you sure you want to unblock", mList[position].fullName!!, object : onClickDialog {
                override fun onClickYes() {
                    mListener.onClickUnBlockUser(position)
                }
            })
        }

    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var binding: AdapterBlockUserBinding = DataBindingUtil.bind(v)!!

        @Throws(NullPointerException::class)
        @SuppressLint("SetTextI18n")
        fun bindWithHolder(friendTable: BlockUserListModel.BlockUser) {
            binding.tvFriendName.text = friendTable.fullName
            viewBinderHelper.bind(binding.srLayout, friendTable.userId.toString())
            if (!TextUtils.isEmpty(friendTable.profilePic))
                binding.civFriendAvatar.load(friendTable.profilePic!!)
            else
                Glide.with(context).load(R.drawable.ic_avatar_place_holder).into(binding.civFriendAvatar)
            binding.tvMinAgo.text = DateUtils.displayDateOnlyForBlockUser(DateUtils.getLocalDate(friendTable.blockedOn!!))

        }
    }

    internal fun setList(list: List<BlockUserListModel.BlockUser>) {
        this.mList = list
        notifyDataSetChanged()
    }
}