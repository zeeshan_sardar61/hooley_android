package com.hooleyapp.hooley.app.viewmodel.event

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.hooleyapp.hooley.app.data.repository.event.EventDetailGuestListRepository
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*
import kotlin.collections.ArrayList

class EventDetailGuestListViewModel(application: Application) : AndroidViewModel(application) {

    var repository = EventDetailGuestListRepository()
    var failureMessage: MutableLiveData<String> = MutableLiveData()
    var sessionExpire: MutableLiveData<Boolean> = MutableLiveData()
    var reloadApi: MutableLiveData<Boolean> = MutableLiveData()
    var loading: MutableLiveData<Boolean> = MutableLiveData()
    var attendingAndInvitedList: MutableLiveData<List<FriendsTable>> = MutableLiveData()

    init {
        failureMessage = repository.failureMessage
        sessionExpire = repository.sessionExpire
        loading = repository.loading
        attendingAndInvitedList = repository.attendingAndInvitedList
        reloadApi = repository.reloadApi
    }

    fun getEventGuestList(eventId: String, type: String) {
        repository.getEventGuestList(eventId, type)
    }

    fun sendFriendRequest(friendId: Int) {
        repository.sendFriendRequest(friendId)
    }

    fun acceptIgnoreFriendRequest(userId: Int, isAccepted: Boolean) {
        repository.acceptIgnoreFriendRequest(userId, isAccepted)
    }


    fun applyGenderFilter(type: String, isMaleChecked: Boolean, isFemaleChecked: Boolean, isOtherChecked: Boolean) {
        var sortingList = arrayListOf<FriendsTable>()
        when (type) {
            Constants.TYPE_ATTENDING -> {
                if (isMaleChecked || isFemaleChecked || isOtherChecked) {
                    sortingList.addAll(if (isMaleChecked) ArrayList(maleFilterList(type)) else ArrayList())
                    sortingList.addAll(if (isFemaleChecked) ArrayList(femaleFilterList(type)) else ArrayList())
                    sortingList.addAll(if (isOtherChecked) ArrayList(otherFilterList(type)) else ArrayList())
                } else {
                    sortingList.addAll(repository.attendingAndInvitedTempList.value!!)
                }
                attendingAndInvitedList.postValue(null)
                attendingAndInvitedList.postValue(sortingList)
            }
            Constants.TYPE_FOLLOWING -> {
                if (isMaleChecked || isFemaleChecked || isOtherChecked) {
                    sortingList.addAll(if (isMaleChecked) ArrayList(maleFilterList(type)) else ArrayList())
                    sortingList.addAll(if (isFemaleChecked) ArrayList(femaleFilterList(type)) else ArrayList())
                    sortingList.addAll(if (isOtherChecked) ArrayList(otherFilterList(type)) else ArrayList())
                } else {
                    sortingList.addAll(repository.attendingAndInvitedTempList.value!!)
                }
                attendingAndInvitedList.postValue(null)
                attendingAndInvitedList.postValue(sortingList)
            }
            Constants.TYPE_INVITED -> {
                if (isMaleChecked || isFemaleChecked || isOtherChecked) {
                    sortingList.addAll(if (isMaleChecked) ArrayList(maleFilterList(type)) else ArrayList())
                    sortingList.addAll(if (isFemaleChecked) ArrayList(femaleFilterList(type)) else ArrayList())
                    sortingList.addAll(if (isOtherChecked) ArrayList(otherFilterList(type)) else ArrayList())
                } else {
                    sortingList.addAll(repository.attendingAndInvitedTempList.value!!)
                }
                attendingAndInvitedList.postValue(null)
                attendingAndInvitedList.postValue(sortingList)
            }
        }
    }

    fun searchingList(type: String, s: String) {
        when (type) {
            Constants.TYPE_INVITED -> {
                var list = repository.attendingAndInvitedTempList.value?.filter { table ->
                    table.fullName.contains(s, true) || (table.userIntrestList.any { it.contains(s, true) })
                }
                attendingAndInvitedList.postValue(null)
                attendingAndInvitedList.postValue(list)
            }
            Constants.TYPE_ATTENDING -> {
                var list = repository.attendingAndInvitedTempList.value?.filter { table ->
                    table.fullName.contains(s, true) || (table.userIntrestList.any { it.contains(s, true) })
                }
                attendingAndInvitedList.postValue(null)
                attendingAndInvitedList.postValue(list)
            }
            Constants.TYPE_FOLLOWING -> {
                var list = repository.attendingAndInvitedTempList.value?.filter { table ->
                    table.fullName.contains(s, true) || (table.userIntrestList.any { it.contains(s, true) })
                }
                attendingAndInvitedList.postValue(null)
                attendingAndInvitedList.postValue(list)
            }
            else -> {
                attendingAndInvitedList.postValue(null)
                attendingAndInvitedList.postValue(listOf())
            }
        }
    }

    // filtering
    fun otherFilterList(type: String): List<FriendsTable>? {
        return when (type) {
            Constants.TYPE_INVITED -> {
                repository.attendingAndInvitedTempList.value?.filter {
                    it.gender == Constants.GENDER_OTHER
                }
            }
            Constants.TYPE_ATTENDING -> {
                repository.attendingAndInvitedTempList.value?.filter {
                    it.gender == Constants.GENDER_OTHER
                }
            }
            Constants.TYPE_FOLLOWING -> {
                repository.attendingAndInvitedTempList.value?.filter {
                    it.gender == Constants.GENDER_OTHER
                }
            }
            else ->
                listOf()
        }

    }

    fun femaleFilterList(type: String): List<FriendsTable>? {
        return when (type) {
            Constants.TYPE_INVITED -> {
                repository.attendingAndInvitedTempList.value?.filter {
                    it.gender == Constants.GENDER_FEMALE
                }
            }
            Constants.TYPE_ATTENDING -> {
                repository.attendingAndInvitedTempList.value?.filter {
                    it.gender == Constants.GENDER_FEMALE
                }
            }
            Constants.TYPE_FOLLOWING -> {
                repository.attendingAndInvitedTempList.value?.filter {
                    it.gender == Constants.GENDER_FEMALE
                }
            }
            else ->
                listOf()
        }
    }

    fun maleFilterList(type: String): List<FriendsTable>? {
        return when (type) {
            Constants.TYPE_INVITED -> {
                repository.attendingAndInvitedTempList.value?.filter {
                    it.gender == Constants.GENDER_MALE
                }
            }
            Constants.TYPE_ATTENDING -> {
                repository.attendingAndInvitedTempList.value?.filter {
                    it.gender == Constants.GENDER_MALE
                }
            }
            Constants.TYPE_FOLLOWING -> {
                repository.attendingAndInvitedTempList.value?.filter {
                    it.gender == Constants.GENDER_MALE
                }
            }
            else ->
                listOf()
        }
    }

    // Sorting
    fun sortByAlphabetical(type: String) {
        when (type) {
            Constants.TYPE_ATTENDING -> {
                Collections.sort(repository.attendingAndInvitedTempList.value, FriendsTable.sortByNameComparator)
                attendingAndInvitedList.postValue(null)
                attendingAndInvitedList.postValue(repository.attendingAndInvitedTempList.value)
            }
            Constants.TYPE_FOLLOWING -> {
                Collections.sort(repository.attendingAndInvitedTempList.value, FriendsTable.sortByNameComparator)
                attendingAndInvitedList.postValue(null)
                attendingAndInvitedList.postValue(repository.attendingAndInvitedTempList.value)
            }
            Constants.TYPE_INVITED -> {
                var userList = repository.attendingAndInvitedTempList.value?.filterNot {
                    it.status == Constants.NO_HOOLEY_USER
                }
                var contactList = repository.attendingAndInvitedTempList.value?.filter {
                    it.status == Constants.NO_HOOLEY_USER
                }
                Collections.sort(userList, FriendsTable.sortByNameComparator)
                Collections.sort(contactList, FriendsTable.sortByNameComparator)
                var list = ArrayList<FriendsTable>()
                list.addAll(ArrayList(userList))
                list.addAll(ArrayList(contactList))
                attendingAndInvitedList.postValue(null)
                attendingAndInvitedList.postValue(list)
            }
        }
    }

    fun sortByHooleyFriends(type: String) {
        when (type) {
            Constants.TYPE_INVITED -> {
                Collections.sort(repository.attendingAndInvitedTempList.value, FriendsTable.sortByHooleyFriend)
                attendingAndInvitedList.postValue(null)
                attendingAndInvitedList.postValue(repository.attendingAndInvitedTempList.value)
            }
            Constants.TYPE_ATTENDING -> {
                Collections.sort(repository.attendingAndInvitedTempList.value, FriendsTable.sortByHooleyFriend)
                attendingAndInvitedList.postValue(null)
                attendingAndInvitedList.postValue(repository.attendingAndInvitedTempList.value)
            }
            Constants.TYPE_FOLLOWING -> {
                Collections.sort(repository.attendingAndInvitedTempList.value, FriendsTable.sortByHooleyFriend)
                attendingAndInvitedList.postValue(null)
                attendingAndInvitedList.postValue(repository.attendingAndInvitedTempList.value)
            }
        }
    }

    fun sortByShareInterest(type: String) {
        when (type) {
            Constants.TYPE_INVITED -> {
                Collections.sort(repository.attendingAndInvitedTempList.value, FriendsTable.sortByShareInterest)
                attendingAndInvitedList.postValue(null)
                attendingAndInvitedList.postValue(repository.attendingAndInvitedTempList.value)
            }
            Constants.TYPE_ATTENDING -> {
                Collections.sort(repository.attendingAndInvitedTempList.value, FriendsTable.sortByShareInterest)
                attendingAndInvitedList.postValue(null)
                attendingAndInvitedList.postValue(repository.attendingAndInvitedTempList.value)
            }
            Constants.TYPE_FOLLOWING -> {
                Collections.sort(repository.attendingAndInvitedTempList.value, FriendsTable.sortByShareInterest)
                attendingAndInvitedList.postValue(null)
                attendingAndInvitedList.postValue(repository.attendingAndInvitedTempList.value)
            }
        }
    }
}