package com.hooleyapp.hooley.app.view.callback

interface IAnnouncementClickListener {
    fun onImageClick(position: Int)
    fun onMoreClick(position: Int)
}