package com.hooleyapp.hooley.app.viewmodel.others

import android.app.Application
import android.app.UiModeManager
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.app.data.repository.others.AboutHooleyRepository
import com.hooleyapp.hooley.model.GetAppVersionResponseModel
import com.hooleyapp.hooley.others.Constants

class AboutHooleyViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: AboutHooleyRepository = AboutHooleyRepository()
    var pageUrl: MutableLiveData<String> = MutableLiveData()
    var versionObject: MutableLiveData<GetAppVersionResponseModel.HooleyVersion> = MutableLiveData()
    var sessionExpire: MutableLiveData<Boolean> = MutableLiveData()
    var failureMessage: MutableLiveData<String> = MutableLiveData()

    init {
        var url = if (HooleyApp.db.getInt(Constants.TYPE_THEME) == UiModeManager.MODE_NIGHT_YES) {
            Constants.ABOUT_HOLLEY_URL + "true"
        } else {
            Constants.ABOUT_HOLLEY_URL + "false"
        }
        pageUrl.postValue(url)
        versionObject = repository.versionObject
        sessionExpire = repository.sessionExpire
        failureMessage = repository.failureMessage
    }

    fun getAppVersion() {
        repository.getAppVersion()
    }
}