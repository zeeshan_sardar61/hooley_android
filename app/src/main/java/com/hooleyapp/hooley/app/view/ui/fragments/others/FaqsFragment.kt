package com.hooleyapp.hooley.app.view.ui.fragments.others

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.app.viewmodel.others.FaqsViewModel
import com.hooleyapp.hooley.databinding.FragmentFaqsBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.others.Constants

/**
 * Created by adil on 3/1/v 2018.
 */

class FaqsFragment : BaseFragment() {

    private lateinit var binding: FragmentFaqsBinding
    private lateinit var faqsViewModel: FaqsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_faqs, container, false)
        faqsViewModel = ViewModelProviders.of(this).get(FaqsViewModel::class.java)
        HooleyApp.db.putBoolean(Constants.OPEN_DRAWER, false)
        ActivityBase.setStatusBarGradiant(0)
        return binding.root
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.wvFaqs.settings.javaScriptEnabled = true
        setUiObservers()
    }

    private fun setUiObservers() {
        faqsViewModel.pageUrl.observe(this, Observer { it.let { its -> binding.wvFaqs.loadUrl(its!!) } })
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
    }
}
