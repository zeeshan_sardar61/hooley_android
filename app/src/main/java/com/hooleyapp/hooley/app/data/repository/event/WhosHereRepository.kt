package com.hooleyapp.hooley.app.data.repository.event

import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.helper.SingleLiveEvent
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.WhosHereModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.EventWebService
import com.hooleyapp.hooley.services.FriendWebService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException
import java.util.*

class WhosHereRepository {

    var mList: SingleLiveEvent<ArrayList<WhosHereModel.User>> = SingleLiveEvent()
    var tempList: SingleLiveEvent<ArrayList<WhosHereModel.User>> = SingleLiveEvent()
    var failureMessage: SingleLiveEvent<String> = SingleLiveEvent()
    var sessionExpire: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var reloadApi: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var loading: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var eventWebService = EventWebService()
    var friendService = FriendWebService()
    private var apiCall: Call<WhosHereModel>? = null

    // Network Request
    fun getWhosHere(eventId: String, type: String) {
        loading.postValue(true)
        getWhosHere(eventId, type, object : IWebServiceCallback<WhosHereModel> {
            override fun success(result: WhosHereModel) {
                loading.postValue(false)
                mList.postValue(null)
                Collections.sort<WhosHereModel.User>(result.whosHereList, WhosHereModel.User.sortByNameComparator)
                mList.postValue(result.whosHereList)
                tempList.postValue(null)
                tempList.postValue(result.whosHereList)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                if (message.isNotBlank())
                    failureMessage.postValue(message)
                mList.postValue(null)
                mList.postValue(arrayListOf())
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })
    }

    fun sendFriendRequest(friendId: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        val friendList = java.util.ArrayList<Int>()
        friendList.add(friendId)
        friendService.sendFriendRequest(friendList, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                Util.showToastMessage(HooleyMain.activity!!, "Friend Request Sent")
                reloadApi.postValue(true)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }
        })
    }

    fun acceptIgnoreFriendRequest(userId: Int, isAccepted: Boolean) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        friendService.acceptIgnoreFriendRequest(userId, isAccepted, object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                loading.postValue(false)
                reloadApi.postValue(true)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }
        })
    }

    fun getWhosHere(eventId: String, type: String, callback: IWebServiceCallback<WhosHereModel>) {
        if (apiCall != null)
            apiCall?.cancel()
        apiCall = HooleyApp.apiService.getWhosHere(HooleyApp.db.getString(Constants.USER_ID), eventId, type)
        apiCall?.enqueue(object : Callback<WhosHereModel> {
            override fun onResponse(call: Call<WhosHereModel>, response: Response<WhosHereModel>) {
                if (response.body() == null) {
                    if (response.errorBody() != null)
                        callback.failure(response.errorBody().toString())
                    else
                        callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<WhosHereModel>, t: Throwable) {
                if (call.isCanceled) {
                    return
                }
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


}