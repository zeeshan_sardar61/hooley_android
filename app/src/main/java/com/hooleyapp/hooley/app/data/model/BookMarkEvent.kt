package com.hooleyapp.hooley.app.data.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "BookmarkTable")
data class BookMarkEvent(

        @PrimaryKey
        @ColumnInfo(name = "id")
        var id: Int = 0,

        @ColumnInfo(name = "eventId")
        @SerializedName("eventId")
        var eventId: String? = null,

        @ColumnInfo(name = "coverPhoto")
        @SerializedName("coverPhoto")
        var coverPhoto: String? = null,

        @ColumnInfo(name = "startTime")
        @SerializedName("startTime")
        var startTime: String? = null,

        @ColumnInfo(name = "endTime")
        @SerializedName("endTime")
        var endTime: String? = null,

        @ColumnInfo(name = "eventName")
        @SerializedName("eventName")
        var eventName: String? = null,

        @ColumnInfo(name = "address")
        @SerializedName("address")
        var address: String? = null,

        @ColumnInfo(name = "eventLat")
        @SerializedName("eventLat")
        var eventLat: Double? = null,

        @ColumnInfo(name = "eventLong")
        @SerializedName("eventLong")
        var eventLong: Double? = null,

        @ColumnInfo(name = "likesCount")
        @SerializedName("likesCount")
        var likesCount: Int = 0,

        @ColumnInfo(name = "commentsCount")
        @SerializedName("commentsCount")
        var commentsCount: Int = 0,

        @ColumnInfo(name = "sharesCounts")
        @SerializedName("sharesCounts")
        var sharesCount: Int = 0,

        @ColumnInfo(name = "isLiked")
        @SerializedName("isLiked")
        var isLiked: Boolean = false,

        @ColumnInfo(name = "isLive")
        @SerializedName("isLive")
        var isLive: Boolean = false,

        @ColumnInfo(name = "isPast")
        @SerializedName("isPast")
        var isPast: Boolean = false,

        @ColumnInfo(name = "isFollowed")
        @SerializedName("isFollowed")
        var isFollowed: Boolean = false,

        @ColumnInfo(name = "followersCount")
        @SerializedName("followersCount")
        var followersCount: Int = 0
)