package com.hooleyapp.hooley.app.data.model.event.responseModel.eventDetailNew

data class EventDetailNewResponse(
        val coHosts: List<Any?>? = null,
        val reasons: Reasons? = null,
        val geofence: List<GeofenceItem?>? = null,
        val counts: Counts? = null,
        val exception: String? = null,
        val eventHashTags: List<Any?>? = null,
        val basicInfo: BasicInfo? = null,
        val checks: Checks? = null,
        val optionalCategories: List<Any?>? = null,
        val host: Host? = null,
        val coverPhotos: List<Any?>? = null,
        val categories: List<Any?>? = null,
        val isSuccessful: Boolean? = null
)
