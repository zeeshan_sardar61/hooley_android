package com.hooleyapp.hooley.app.data.repository.event

import android.arch.lifecycle.MutableLiveData
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventAnnouncementModel
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.services.EventWebService
import com.hooleyapp.hooley.services.GuestUserWebService

class EventAnnouncementRepository {

    var mlist: MutableLiveData<ArrayList<EventAnnouncementModel.Announcement>> = MutableLiveData()
    var tempList: MutableLiveData<ArrayList<EventAnnouncementModel.Announcement>> = MutableLiveData()
    var failureMessage: MutableLiveData<String> = MutableLiveData()
    var sessionExpire: MutableLiveData<Boolean> = MutableLiveData()
    var loading: MutableLiveData<Boolean> = MutableLiveData()
    var eventWebService = EventWebService()
    var guestWebService = GuestUserWebService()

    // NetWorkRequest
    fun getEventAnnouncement(eventId: String) {
        loading.postValue(true)
        eventWebService.getEventAnnouncement(eventId, object : IWebServiceCallback<EventAnnouncementModel> {
            override fun success(result: EventAnnouncementModel) {
                loading.postValue(false)
                mlist.postValue(null)
                mlist.postValue(result.eventDisplayAnnoucment)
                tempList.postValue(null)
                tempList.postValue(result.eventDisplayAnnoucment)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })
    }

    fun deleteEventAnnouncement(positionId: Int) {
        loading.postValue(true)
        eventWebService.deleteEventAnnouncement(mlist.value!![positionId].id.toString(), object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                loading.postValue(false)
                tempList.value!!.remove(tempList.value!![positionId])
                mlist.postValue(null)
                mlist.postValue(tempList.value)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)

            }
        })
    }
}