package com.hooleyapp.hooley.app.viewmodel.others

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.hooleyapp.hooley.app.data.repository.others.FeedBackRepository

class FeedBackViewModel(application: Application) : AndroidViewModel(application) {

    var repository = FeedBackRepository()
    var failureMessage: MutableLiveData<String> = MutableLiveData()
    var sessionExpire: MutableLiveData<Boolean> = MutableLiveData()
    var loading: MutableLiveData<Boolean> = MutableLiveData()
    var success: MutableLiveData<Boolean> = MutableLiveData()

    init {
        failureMessage = repository.failureMessage
        sessionExpire = repository.sessionExpire
        loading = repository.loading
        success = repository.success
    }

    fun submitFeedBack(feedBackMessage: String, rating: String) {
        repository.addFeedBack(feedBackMessage, rating)
    }

    fun addGuestUserFeedBack(feedBackMessage: String, rating: String) {
        repository.addGuestUserFeedBack(feedBackMessage, rating)
    }
}