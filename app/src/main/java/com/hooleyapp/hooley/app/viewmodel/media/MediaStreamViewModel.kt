package com.hooleyapp.hooley.app.viewmodel.media

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.app.data.repository.media.MediaStreamRepository
import com.hooleyapp.hooley.model.PersonalProfileModel

class MediaStreamViewModel(application: Application) : AndroidViewModel(application) {

    var repository = MediaStreamRepository()
    var mList: MutableLiveData<ArrayList<EventGalleryFeedModel.EventMediaInfo>> = MutableLiveData()
    var failureMessage: MutableLiveData<String> = MutableLiveData()
    var sessionExpire: MutableLiveData<Boolean> = MutableLiveData()
    var reloadApi: MutableLiveData<Boolean> = MutableLiveData()
    var loading: MutableLiveData<Boolean> = MutableLiveData()
    var reportEventList: MutableLiveData<List<PersonalProfileModel.myObject>> = MutableLiveData()

    init {
        mList = repository.mList
        failureMessage = repository.failureMessage
        sessionExpire = repository.sessionExpire
        reloadApi = repository.reloadApi
        loading = repository.loading
        reportEventList = repository.reportEventList
    }

    fun getMediaStream(type: String) {
        repository.getMediaStream(type)
    }

    fun getMediaStreamTrending(type: String) {
        repository.getMediaStreamTrending(type)
    }

    fun likePost(eventId: String, imageId: String) {
        repository.likePost(eventId, imageId)
    }

    fun mediaFav(eventId: String, imageId: String, isFav: Boolean) {
        repository.mediaFav(eventId, imageId, isFav)
    }

    fun deleteMedia(imageId: String, eventId: String) {
        repository.deleteMedia(imageId, eventId)
    }

    fun submitReport(eventId: String, reasonId: String, reportMessage: String, mediaId: String) {
        repository.submitReport(eventId, reasonId, reportMessage, mediaId)
    }

    fun shareEventMedia(eventId: String?, imageId: String, shareMedia: String, allFeedItemPosition: Int) {
        repository.shareEventMedia(eventId, imageId, shareMedia, allFeedItemPosition)
    }

    fun getMediaReportEvent() {
        repository.getMediaReportEvent()
    }

    fun searchString(text: String) {
        if (repository.tempList.value == null)
            return
        var searchList = repository.tempList.value!!.filter { item ->
            item.fullName.toLowerCase().contains(text.toLowerCase()) || item.eventName!!.toLowerCase().contains(text.toLowerCase()) || item.imageCaption.toLowerCase().contains(text.toLowerCase())
        }
        mList.postValue(null)
        mList.postValue(ArrayList(searchList))
    }

    // filter
    fun filterByAll() {
        mList.postValue(null)
        mList.postValue(repository.tempList.value)
    }

    fun filterByFav() {
        var list = repository.tempList.value?.filter {
            it.isFavorited
        }
        mList.postValue(null)
        mList.postValue(ArrayList(list))
    }

    fun filterByPhotos() {
        var mainList = arrayListOf<EventGalleryFeedModel.EventMediaInfo>()
        for (i in 0 until repository.tempList.value!!.size) {
            var mediaList = repository.tempList.value!![i].mediaFiles.filterNot { it.isVideo!! }
            if (mediaList.isNotEmpty()) {
                var item = EventGalleryFeedModel.EventMediaInfo()
                item.imageId = repository.tempList.value!![i].imageId
                item.userId = repository.tempList.value!![i].userId
                item.fullName = repository.tempList.value!![i].fullName
                item.eventName = repository.tempList.value!![i].eventName
                item.imageCaption = repository.tempList.value!![i].imageCaption
                item.postedTime = repository.tempList.value!![i].postedTime
                item.profilePic = repository.tempList.value!![i].profilePic
                item.eventId = repository.tempList.value!![i].eventId
                item.commentsCount = repository.tempList.value!![i].commentsCount
                item.favoriteCount = repository.tempList.value!![i].favoriteCount
                item.userId = repository.tempList.value!![i].userId
                item.likesCount = repository.tempList.value!![i].likesCount
                item.isLiked = repository.tempList.value!![i].isLiked
                item.isLive = repository.tempList.value!![i].isLive
                item.isFavorited = repository.tempList.value!![i].isFavorited
                item.shareCounts = repository.tempList.value!![i].shareCounts
                item.partialComments.addAll(repository.tempList.value!![i].partialComments)
                item.mediaFiles.addAll((ArrayList(mediaList)))
                mainList.add(item)
            }
        }
        mList.postValue(null)
        mList.postValue(ArrayList(mainList))
    }

    fun filterByVideo() {
        var mainList = arrayListOf<EventGalleryFeedModel.EventMediaInfo>()
        for (i in 0 until repository.tempList.value!!.size) {
            var mediaList = repository.tempList.value!![i].mediaFiles.filter { it.isVideo!! }
            if (mediaList.isNotEmpty()) {
                var item = EventGalleryFeedModel.EventMediaInfo()
                item.imageId = repository.tempList.value!![i].imageId
                item.userId = repository.tempList.value!![i].userId
                item.fullName = repository.tempList.value!![i].fullName
                item.eventName = repository.tempList.value!![i].eventName
                item.imageCaption = repository.tempList.value!![i].imageCaption
                item.postedTime = repository.tempList.value!![i].postedTime
                item.profilePic = repository.tempList.value!![i].profilePic
                item.eventId = repository.tempList.value!![i].eventId
                item.commentsCount = repository.tempList.value!![i].commentsCount
                item.favoriteCount = repository.tempList.value!![i].favoriteCount
                item.userId = repository.tempList.value!![i].userId
                item.likesCount = repository.tempList.value!![i].likesCount
                item.isLiked = repository.tempList.value!![i].isLiked
                item.isLive = repository.tempList.value!![i].isLive
                item.isFavorited = repository.tempList.value!![i].isFavorited
                item.shareCounts = repository.tempList.value!![i].shareCounts
                item.partialComments.addAll(repository.tempList.value!![i].partialComments)
                item.mediaFiles.addAll((ArrayList(mediaList)))
                mainList.add(item)
            }
        }
        mList.postValue(null)
        mList.postValue(ArrayList(mainList))
    }
}