package com.hooleyapp.hooley.app.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.event.responseModel.PopularInFriendsModel
import com.hooleyapp.hooley.app.view.callback.IEventClickListener
import com.hooleyapp.hooley.databinding.AdapterPopularInFriendsBinding
import com.hooleyapp.hooley.helper.load
import com.hooleyapp.hooley.others.DateUtils

class PopularInFriendsAdapter(private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var mListener: IEventClickListener? = null
    internal var mList = listOf<PopularInFriendsModel.PopularInFriends>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding: AdapterPopularInFriendsBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_popular_in_friends, parent, false)
        return ItemViewHolder(binding.root)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder

        // setting cell data
        holder.bindHolderViewData(position, mList[position])

        // setting listener
        holder.binding.rlEventMainCover.setOnClickListener { mListener?.onClickItem(position) }
        holder.binding.tvEventAddress.setOnClickListener { mListener?.onMapClick(position) }
        holder.binding.llLike.setOnClickListener { mListener!!.onClickLike(position) }
        holder.binding.llShare.setOnClickListener { mListener!!.onClickShare(position) }
        holder.binding.llFollow.setOnClickListener { mListener!!.onClickFollow(position) }
        holder.binding.llInterestedPeopleCount.setOnClickListener { mListener!!.onClickInterestedPoeople(position) }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var binding: AdapterPopularInFriendsBinding = DataBindingUtil.bind(v)!!

        @SuppressLint("SetTextI18n")
        fun bindHolderViewData(itemPosition: Int, popularInFriendModel: PopularInFriendsModel.PopularInFriends) {
            if (itemPosition == 0)
                binding.vSeprator.visibility = View.GONE
            else
                binding.vSeprator.visibility = View.VISIBLE

            popularInFriendModel.coverPhoto?.let {
                binding.ivEventCover.load(it)
            }

            binding.tvEventDate.text = DateUtils.displayDateOnly(DateUtils.getLocalDate(popularInFriendModel.startTime!!))
            binding.tvLike.text = popularInFriendModel.likesCount.toString()
            binding.tvFollow.text = popularInFriendModel.followersCount.toString()
            binding.tvShare.text = popularInFriendModel.sharesCount.toString()
            binding.tvEventName.text = popularInFriendModel.eventName
            binding.tvEventAddress.text = popularInFriendModel.address
            if (popularInFriendModel.isLiked) {
                binding.ivLike.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_red_heart)
            } else {
                binding.ivLike.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_purple_heart)
            }
            if (popularInFriendModel.isFollowed) {
                binding.ivFollow.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_filled_star)
            } else {
                binding.ivFollow.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_follow_it)
            }
            if (popularInFriendModel.isLive)
                binding.rlLive.visibility = View.VISIBLE
            else
                binding.rlLive.visibility = View.GONE
            if (popularInFriendModel.friendList.size > 0) {
                binding.llInterestedPeopleCount.visibility = View.VISIBLE
                binding.tvInterestedPeople.visibility = View.VISIBLE
                if (popularInFriendModel.friendList.size > 1) {
                    binding.tvOthers.text = "other friends are"
                    binding.tvInterestedPeople.text = "${popularInFriendModel.friendList[0].fullName} & ${popularInFriendModel.friendList.size - 1}"
                } else {
                    binding.tvInterestedPeople.text = popularInFriendModel.friendList[0].fullName
                    binding.tvOthers.text = "is interested"
                    binding.tvInterested.visibility = GONE
                }
            } else {
                binding.llInterestedPeopleCount.visibility = View.GONE
            }
        }
    }

    internal fun setList(list: List<PopularInFriendsModel.PopularInFriends>) {
        this.mList = list
        notifyDataSetChanged()
    }

}
