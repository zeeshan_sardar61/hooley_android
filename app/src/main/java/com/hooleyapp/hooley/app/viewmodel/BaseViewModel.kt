package com.hooleyapp.hooley.app.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.hooleyapp.hooley.app.data.repository.BaseRepository
import com.hooleyapp.hooley.helper.SingleLiveEvent

open class BaseViewModel(application: Application) : AndroidViewModel(application) {

    var mRepository = BaseRepository()
    var failureMessage: SingleLiveEvent<String> = SingleLiveEvent()
    var sessionExpire: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var reloadApi: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var loading: SingleLiveEvent<Boolean> = SingleLiveEvent()

//    init {
//        this.failureMessage = mRepository.failureMessage
//        this.sessionExpire = mRepository.sessionExpire
//        this.reloadApi = mRepository.reloadApi
//        this.loading = mRepository.loading
//    }

}
