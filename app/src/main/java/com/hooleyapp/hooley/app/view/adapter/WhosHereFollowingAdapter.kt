package com.hooleyapp.hooley.app.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.view.callback.IEventGuestListClickListener
import com.hooleyapp.hooley.databinding.AdapterItemWhosHereBinding
import com.hooleyapp.hooley.helper.load
import com.hooleyapp.hooley.model.WhosHereModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils

/**
 *
 * Created by Nauman on 2/15/2018.
 */

class WhosHereFollowingAdapter(private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal lateinit var binding: AdapterItemWhosHereBinding
    private val viewBinderHelper = ViewBinderHelper()
    lateinit var mListener: IEventGuestListClickListener
    internal var mList = listOf<WhosHereModel.User>()
    internal var type: String = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_item_whos_here, parent, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder

        if (type == Constants.WHOS_HERE_NOW || type == Constants.WHOS_HERE_LEFT) {
            binding.tvMinAgo.visibility = View.VISIBLE
            binding.tvMinAgo.text = DateUtils.convertDateToTimeAgo(DateUtils.getLocalDate(mList[position].minutesAgo!!))
        } else {
            binding.tvMinAgo.visibility = View.GONE
        }

        try {
            holder.bindWithHolder(mList[position])
        } catch (e: Exception) {
            e.printStackTrace()
        }

        // setting click listener
        holder.binding.rlAddFriend.setOnClickListener { mListener.onClickAddFriend(position) }
        holder.binding.rlViewProfile.setOnClickListener { mListener.onClickFriendProfile(position) }
        holder.binding.rlItem.setOnClickListener { mListener.onClickItem(position) }
        holder.binding.rlMessage.setOnClickListener { mListener.onClickMessage(position) }
        holder.binding.rlMyProfile.setOnClickListener { mListener.onClickMyProfile(position) }
        holder.binding.rlAccept.setOnClickListener { mListener.onClickAcceptRequest(position, true) }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var binding: AdapterItemWhosHereBinding = DataBindingUtil.bind(v)!!

        @Throws(NullPointerException::class)
        @SuppressLint("SetTextI18n")
        fun bindWithHolder(friendTable: WhosHereModel.User) {
            binding.tvFriendName.text = friendTable.fullName
            viewBinderHelper.bind(binding.srLayout, friendTable.userId.toString())
            if (!TextUtils.isEmpty(friendTable.profilePic))
                binding.civFriendAvatar.load(friendTable.profilePic!!)
            else
                Glide.with(context).load(R.drawable.ic_avatar_place_holder).into(binding.civFriendAvatar)
            binding.tvAddress.text = friendTable.cityName + ", " + friendTable.stateName

            if (friendTable.userId == HooleyApp.db.getString(Constants.USER_ID)) {
                binding.rlAddFriend.visibility = View.GONE
                binding.rlViewProfile.visibility = View.GONE
                binding.rlMyProfile.visibility = View.VISIBLE
                binding.rlMessage.visibility = View.GONE
                binding.rlAccept.visibility = View.GONE
                binding.rlIgnore.visibility = View.GONE
            } else {
                when (friendTable.status) {
                    Constants.TYPE_NONE -> {
                        binding.rlAddFriend.visibility = View.VISIBLE
                        binding.rlMessage.visibility = View.VISIBLE
                        binding.rlViewProfile.visibility = View.GONE
                        binding.rlMyProfile.visibility = View.GONE
                        binding.rlAccept.visibility = View.GONE
                        binding.rlIgnore.visibility = View.GONE
                        binding.rlRequested.visibility = View.GONE
                        isFriendRequestAccessible(friendTable, binding)
                        isMessageAccessible(friendTable, binding)
                    }
                    Constants.TYPE_PENDING_SENT -> {
                        binding.rlMessage.visibility = View.VISIBLE
                        binding.rlAddFriend.visibility = View.GONE
                        binding.rlRequested.visibility = View.VISIBLE
                        binding.rlViewProfile.visibility = View.GONE
                        binding.rlMyProfile.visibility = View.GONE
                        binding.rlAccept.visibility = View.GONE
                        binding.rlIgnore.visibility = View.GONE
                        isMessageAccessible(friendTable, binding)
                    }
                    Constants.TYPE_FRIEND -> {
                        binding.rlMessage.visibility = View.VISIBLE
                        binding.rlAddFriend.visibility = View.GONE
                        binding.rlViewProfile.visibility = View.VISIBLE
                        binding.rlMyProfile.visibility = View.GONE
                        binding.rlAccept.visibility = View.GONE
                        binding.rlIgnore.visibility = View.GONE
                        binding.rlRequested.visibility = View.GONE
                        isMessageAccessible(friendTable, binding)
                    }
                    Constants.TYPE_PENDING_RECEIVED -> {
                        binding.rlAddFriend.visibility = View.GONE
                        binding.rlMessage.visibility = View.GONE
                        binding.rlViewProfile.visibility = View.GONE
                        binding.rlMyProfile.visibility = View.GONE
                        binding.rlAccept.visibility = View.VISIBLE
                        binding.rlIgnore.visibility = View.VISIBLE
                        binding.rlRequested.visibility = View.GONE
                    }
                }
            }
        }
    }

    internal fun isFriendRequestAccessible(friendTable: WhosHereModel.User, binding: AdapterItemWhosHereBinding) {
        if (!friendTable.isFriendRequestAccessible) {
            binding.rlAddFriend.isClickable = false
            binding.rlAddFriend.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_grey_gradiant)
        }
    }

    internal fun isMessageAccessible(friendTable: WhosHereModel.User, binding: AdapterItemWhosHereBinding) {
        if (!friendTable.isMessageAccessible) {
            binding.rlMessage.isClickable = false
            binding.rlMessage.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_grey_gradiant)
        }
    }

    internal fun setList(list: ArrayList<WhosHereModel.User>, type: String) {
        this.mList = list
        this.type = type
        notifyDataSetChanged()
    }

}