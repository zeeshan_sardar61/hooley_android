package com.hooleyapp.hooley.app.data.repository.messages

import android.arch.lifecycle.MutableLiveData
import android.net.Uri
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.messages.GetAllMessageModel
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.model.CreateThreadModel
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.UploadImageModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.ImageUploadWebService
import com.hooleyapp.hooley.services.MessageService
import java.util.*

class ChatWindowRepository {

    var mList: MutableLiveData<ArrayList<GetAllMessageModel.MessageInfo>> = MutableLiveData()
    var tempList: MutableLiveData<ArrayList<GetAllMessageModel.MessageInfo>> = MutableLiveData()
    var failureMessage: MutableLiveData<String> = MutableLiveData()
    var threadId: MutableLiveData<String> = MutableLiveData()
    var sessionExpire: MutableLiveData<Boolean> = MutableLiveData()
    var reloadApi: MutableLiveData<Boolean> = MutableLiveData()
    var loading: MutableLiveData<Boolean> = MutableLiveData()
    var canMessage: MutableLiveData<Boolean> = MutableLiveData()
    var service = MessageService()
    var imageUploadWebService = ImageUploadWebService()

    // Network Request
    fun getAllMessages(threadId: String) {
        loading.postValue(true)
        service.getAllMessage(object : GenericCallback<GetAllMessageModel> {
            override fun success(result: GetAllMessageModel) {
                loading.postValue(false)
                if (result.isBlocked || !result.isMessageAccessible)
                    canMessage.postValue(true)
                else
                    canMessage.postValue(false)
                mList.postValue(null)
                mList.postValue(result.messagesList)
                tempList.postValue(null)
                tempList.postValue(result.messagesList)

            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }
        }, threadId)
    }

    fun createThread(friendId: String) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        loading.postValue(true)
        service.createThread(object : GenericCallback<CreateThreadModel> {
            override fun success(result: CreateThreadModel) {
                loading.postValue(false)
                threadId.postValue(result.threadId!!)
            }

            override fun failure(message: String) {
                loading.postValue(false)
            }
        }, friendId)
    }

    fun sendTextMessage(threadId: String, msg: String, msgType: String, position: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        service.sendIMMessage(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
//                tempList.value!![position].isLoaderShow = false
//                mList.postValue(null)
//                mList.postValue(tempList.value!!)
            }

            override fun failure(message: String) {
                failureMessage.postValue(message)
            }
        }, threadId, msg, msgType)
    }

    fun sendMediaMessage(uri: Uri?, isVideo: Boolean, threadId: String, position: Int) {
        tempList.value!![position].isLoaderShow = true
        mList.postValue(null)
        mList.postValue(tempList.value!!)
        imageUploadWebService.sendMediaMessage(uri!!, threadId, isVideo, object : GenericCallback<UploadImageModel> {
            override fun success(result: UploadImageModel) {
                tempList.value!![position].isLoaderShow = false
                mList.postValue(null)
                mList.postValue(tempList.value!!)
            }

            override fun failure(message: String) {
                failureMessage.postValue(message)
            }
        })
    }
}
