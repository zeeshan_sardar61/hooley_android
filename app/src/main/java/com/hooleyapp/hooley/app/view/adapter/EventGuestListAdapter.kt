package com.hooleyapp.hooley.app.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.view.callback.IEventGuestListClickListener
import com.hooleyapp.hooley.databinding.AdapterMyFriendsBinding
import com.hooleyapp.hooley.helper.load
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.tables.FriendsTable

class EventGuestListAdapter(private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    internal lateinit var binding: AdapterMyFriendsBinding
    private val viewBinderHelper = ViewBinderHelper()
    internal var mList = listOf<FriendsTable>()
    var mListener: IEventGuestListClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_my_friends, parent, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder

        try {
            holder.bindHolderViewData(mList[position])
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

        // setting Listener
        holder.binding.rlAddFriend.setOnClickListener { mListener?.onClickAddFriend(position) }
        holder.binding.rlMyProfile.setOnClickListener { mListener?.onClickMyProfile(position) }
        holder.binding.rlItem.setOnClickListener { mListener?.onClickItem(position) }
        holder.binding.rlViewProfile.setOnClickListener { mListener?.onClickFriendProfile(position) }
        holder.binding.rlMessage.setOnClickListener { mListener?.onClickMessage(position) }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var binding: AdapterMyFriendsBinding = DataBindingUtil.bind(v)!!

        @Throws(NullPointerException::class)
        @SuppressLint("SetTextI18n")
        fun bindHolderViewData(friendTable: FriendsTable) {
            viewBinderHelper.bind(binding.srLayout, friendTable.userId.toString())
            if (!TextUtils.isEmpty(friendTable.profilePic)) {
                binding.civFriendAvatar.load(friendTable.profilePic)
            } else {
                Glide.with(context).load(R.drawable.ic_avatar_place_holder).into(binding.civFriendAvatar)
            }
            binding.tvFriendName.text = friendTable.fullName
            binding.tvAddress.text = friendTable.cityName + ", " + friendTable.stateName
            if (friendTable.userId == HooleyApp.db.getString(Constants.USER_ID).toInt()) {
                binding.rlAddFriend.visibility = View.GONE
                binding.rlViewProfile.visibility = View.GONE
                binding.rlMyProfile.visibility = View.VISIBLE
                binding.rlMessage.visibility = View.GONE
                binding.rlAccept.visibility = View.GONE
                binding.rlIgnore.visibility = View.GONE
            } else {
                when (friendTable.status) {
                    Constants.TYPE_NONE -> {
                        if (friendTable.isFriendRequestAccessible) {
                            binding.rlAddFriend.visibility = View.VISIBLE
                        } else {
                            binding.rlAddFriend.visibility = View.VISIBLE
                            binding.rlAddFriend.isClickable = false
                            binding.rlAddFriend.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_grey_gradiant)
                        }
                        if (friendTable.isMessageAccessible) {
                            binding.rlMessage.visibility = View.VISIBLE
                        } else {
                            binding.rlMessage.isClickable = false
                            binding.rlMessage.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_grey_gradiant)
                        }
                        binding.rlViewProfile.visibility = View.GONE
                        binding.rlMyProfile.visibility = View.GONE
                        binding.rlAccept.visibility = View.GONE
                        binding.rlIgnore.visibility = View.GONE
                        binding.rlRequested.visibility = View.GONE
                    }
                    Constants.TYPE_PENDING_SENT -> {
                        if (friendTable.isMessageAccessible) {
                            binding.rlMessage.visibility = View.VISIBLE
                        } else {
                            binding.rlMessage.isClickable = false
                            binding.rlMessage.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_grey_gradiant)
                        }
                        binding.rlAddFriend.visibility = View.GONE
                        binding.rlRequested.visibility = View.VISIBLE
                        binding.rlViewProfile.visibility = View.GONE
                        binding.rlMyProfile.visibility = View.GONE
                        binding.rlAccept.visibility = View.GONE
                        binding.rlIgnore.visibility = View.GONE
                    }
                    Constants.TYPE_FRIEND -> {
                        if (friendTable.isMessageAccessible) {
                            binding.rlMessage.visibility = View.VISIBLE
                        } else {
                            binding.rlMessage.isClickable = false
                            binding.rlMessage.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_grey_gradiant)
                        }
                        binding.rlAddFriend.visibility = View.GONE
                        binding.rlViewProfile.visibility = View.VISIBLE
                        binding.rlMyProfile.visibility = View.GONE
                        binding.rlAccept.visibility = View.GONE
                        binding.rlIgnore.visibility = View.GONE
                        binding.rlRequested.visibility = View.GONE
                    }
                    Constants.TYPE_PENDING_RECEIVED -> {
                        binding.rlAddFriend.visibility = View.GONE
                        binding.rlMessage.visibility = View.GONE
                        binding.rlViewProfile.visibility = View.GONE
                        binding.rlMyProfile.visibility = View.GONE
                        binding.rlAccept.visibility = View.VISIBLE
                        binding.rlIgnore.visibility = View.VISIBLE
                        binding.rlRequested.visibility = View.GONE
                    }
                }
            }
        }
    }

    internal fun setList(list: List<FriendsTable>) {
        this.mList = list
        notifyDataSetChanged()
    }
}