package com.hooleyapp.hooley.app.view.ui.fragments.event

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterMoreMediaDialoge
import com.hooleyapp.hooley.app.view.adapter.EventGuestListAdapter
import com.hooleyapp.hooley.app.view.adapter.InvitedGuestListAdapter
import com.hooleyapp.hooley.app.view.callback.IEventGuestListClickListener
import com.hooleyapp.hooley.app.viewmodel.event.EventDetailGuestListViewModel
import com.hooleyapp.hooley.databinding.FragmentEventDetailGuestListBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.friends.FriendCardPagerFragment
import com.hooleyapp.hooley.fragments.friends.FriendsProfileFragment
import com.hooleyapp.hooley.fragments.messaging.ChatWindowFragment
import com.hooleyapp.hooley.fragments.others.MoreMediaDialogFragment
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.fragments.profile.ProfileMainFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.others.Constants

@SuppressLint("ValidFragment")
class EventDetailGuestListFragment constructor(var eventId: String, var type: String) : BaseFragment(), TextWatcher, View.OnClickListener, IEventGuestListClickListener, ViewAvatarFragment.CardCallBackListener {

    internal val arrayList = ArrayList<AdapterMoreMediaDialoge.MoreItem>()
    lateinit var binding: FragmentEventDetailGuestListBinding
    lateinit var eventDetailGuestListViewModel: EventDetailGuestListViewModel
    private var adapterAttending: EventGuestListAdapter? = null
    private var adapterInvited: InvitedGuestListAdapter? = null
    private var moreWhosHereDialogFragment: MoreMediaDialogFragment? = null
    var isMaleChecked = false
    var isFemaleChecked = false
    var isOtherChecked = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbar()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_event_detail_guest_list, container, false)
        eventDetailGuestListViewModel = ViewModelProviders.of(this).get(EventDetailGuestListViewModel::class.java)
        setUiObservers()
        eventDetailGuestListViewModel.getEventGuestList(eventId, type)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
        setFilterMoreCallBackListener()
        when (type) {
            Constants.TYPE_ATTENDING -> {
                setAttending()
            }
            Constants.TYPE_FOLLOWING -> {
                setAttending()
            }
            Constants.TYPE_INVITED -> {
                setInvited()
            }
        }
    }

    private fun setUiObservers() {
        eventDetailGuestListViewModel.loading.observe(this, android.arch.lifecycle.Observer {
            it.let { t ->
                if (t!!)
                    HooleyMain.activity!!.showDialog()
                else
                    HooleyMain.activity!!.removeDialog()
            }
        })
        eventDetailGuestListViewModel.reloadApi.observe(this, android.arch.lifecycle.Observer { eventDetailGuestListViewModel.getEventGuestList(eventId, type) })
        eventDetailGuestListViewModel.sessionExpire.observe(this, android.arch.lifecycle.Observer { onTokenExpireLogOut() })
        eventDetailGuestListViewModel.failureMessage.observe(this, android.arch.lifecycle.Observer { it.let { t -> HooleyMain.activity!!.showSuccessDialog(t!!) } })
        eventDetailGuestListViewModel.attendingAndInvitedList.observe(this, android.arch.lifecycle.Observer {
            it.let { t ->
                when (type) {
                    Constants.TYPE_ATTENDING -> {
                        adapterAttending?.setList(t!!)
                        binding.tvUserCount.text = if (t!!.size == 1) {
                            "${t.size} Person "
                        } else {
                            "${t.size} People"
                        }
                    }
                    Constants.TYPE_FOLLOWING -> {
                        adapterAttending?.setList(t!!)
                        binding.tvUserCount.text = if (t!!.size == 1) {
                            "${t.size} Person "
                        } else {
                            "${t.size} People"
                        }
                    }
                    Constants.TYPE_INVITED -> {
                        adapterInvited?.setList(t!!)
                        binding.tvUserCount.text = if (t!!.size == 1) {
                            "${t.size} Person "
                        } else {
                            "${t.size} People"
                        }
                    }
                }
            }
        })
    }

    fun setListener() {
        binding.edtSearch.addTextChangedListener(this)
        binding.ivFilter.setOnClickListener(this)
        binding.rbMale.setOnClickListener(this)
        binding.rbFemale.setOnClickListener(this)
        binding.rbOther.setOnClickListener(this)
        binding.ivClose.setOnClickListener(this)
    }

    fun setToolbar() {
        when (type) {
            Constants.TYPE_ATTENDING -> {
                setToolBarTitle(HooleyMain.activity!!, "Attending")
            }
            Constants.TYPE_FOLLOWING -> {
                setToolBarTitle(HooleyMain.activity!!, "Following")
            }
            Constants.TYPE_INVITED -> {
                setToolBarTitle(HooleyMain.activity!!, "Invited")
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setAttending() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.rvAttending.layoutManager = manager
        binding.rvAttending.layoutAnimation = animation
        binding.rvAttending.setEmptyView(binding.tvNoData)
        adapterAttending = EventGuestListAdapter(HooleyMain.activity!!)
        adapterAttending?.mListener = this
        binding.rvAttending.adapter = adapterAttending
        binding.tvUserCount.text = "0 People"
    }

    @SuppressLint("SetTextI18n")
    private fun setInvited() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.rvInvited.layoutManager = manager
        binding.rvInvited.layoutAnimation = animation
        binding.rvInvited.setEmptyView(binding.tvNoData)
        adapterInvited = InvitedGuestListAdapter(HooleyMain.activity!!)
        adapterInvited?.mListener = this
        binding.rvInvited.adapter = adapterInvited
        binding.tvUserCount.text = "0 People"
    }

    private fun setFilterMoreCallBackListener() {
        if (arrayList.size > 0)
            arrayList.clear()
        val itemAtoZ = AdapterMoreMediaDialoge.MoreItem("A - Z", true)
        val itemCurrentCity = AdapterMoreMediaDialoge.MoreItem("Current City", false)
        val itemHooleyFriends = AdapterMoreMediaDialoge.MoreItem("Hooley Friends", false)
        val itemShareInterest = AdapterMoreMediaDialoge.MoreItem("Shared Interests", false)
        arrayList.add(itemAtoZ)
        arrayList.add(itemCurrentCity)
        arrayList.add(itemHooleyFriends)
        arrayList.add(itemShareInterest)
        moreWhosHereDialogFragment = MoreMediaDialogFragment.Builder(HooleyMain.activity!!).setTitle("Sort by").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, _ ->
            when (position) {
                0 -> {
                    eventDetailGuestListViewModel.sortByAlphabetical(type)
                    arrayList[0].isCheck = (true)
                    arrayList[1].isCheck = (false)
                    arrayList[2].isCheck = (false)
                    arrayList[3].isCheck = (false)
                    moreWhosHereDialogFragment!!.dismiss()
                }
                1 -> {
                    arrayList[0].isCheck = (false)
                    arrayList[1].isCheck = (true)
                    arrayList[2].isCheck = (false)
                    arrayList[3].isCheck = (false)
                    moreWhosHereDialogFragment!!.dismiss()
                }
                2 -> {
                    eventDetailGuestListViewModel.sortByHooleyFriends(type)
                    arrayList[0].isCheck = (false)
                    arrayList[1].isCheck = (false)
                    arrayList[2].isCheck = (true)
                    arrayList[3].isCheck = (false)
                    moreWhosHereDialogFragment!!.dismiss()
                }
                3 -> {
                    eventDetailGuestListViewModel.sortByShareInterest(type)
                    arrayList[0].isCheck = (false)
                    arrayList[1].isCheck = (false)
                    arrayList[2].isCheck = (false)
                    arrayList[3].isCheck = (true)
                    moreWhosHereDialogFragment!!.dismiss()
                }
            }
        }.create()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivFilter -> {
                if (!moreWhosHereDialogFragment!!.isAdded)
                    moreWhosHereDialogFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
            }
            R.id.rbMale -> {
                isMaleChecked = !isMaleChecked
                if (isMaleChecked) {
                    binding.rlMale.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_selected_field)
                    binding.rbMale.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_male_white)
                } else {
                    binding.rlMale.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                    binding.rbMale.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_male)
                }
                eventDetailGuestListViewModel.applyGenderFilter(type, isMaleChecked, isFemaleChecked, isOtherChecked)
            }
            R.id.rbFemale -> {
                isFemaleChecked = !isFemaleChecked
                if (isFemaleChecked) {
                    binding.rlFemale.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_rb_center)
                    binding.rbFemale.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_female_white)
                } else {
                    binding.rlFemale.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                    binding.rbFemale.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_female)
                }
                eventDetailGuestListViewModel.applyGenderFilter(type, isMaleChecked, isFemaleChecked, isOtherChecked)
            }
            R.id.rbOther -> {
                isOtherChecked = !isOtherChecked
                if (isOtherChecked) {
                    binding.rlOther.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_rb_center)
                    binding.rbOther.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_other_white)
                } else {
                    binding.rlOther.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                    binding.rbOther.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_other)
                }
                eventDetailGuestListViewModel.applyGenderFilter(type, isMaleChecked, isFemaleChecked, isOtherChecked)
            }
            R.id.ivClose -> {
                binding.edtSearch.setText("")
            }
        }
    }

    override fun reloadFragment(flag: Boolean) {
        if (flag)
            eventDetailGuestListViewModel.getEventGuestList(eventId, type)
    }

    override fun onClickAddFriend(position: Int) {
        eventDetailGuestListViewModel.sendFriendRequest(eventDetailGuestListViewModel.attendingAndInvitedList.value!![position].userId)
    }

    override fun onClickItem(position: Int) {
        var friendCardFragment = FriendCardPagerFragment(position, ArrayList(eventDetailGuestListViewModel.attendingAndInvitedList.value))
        friendCardFragment.mListener = this
        friendCardFragment.show(HooleyMain.activity!!.supportFragmentManager)

    }

    override fun onClickMyProfile(position: Int) {
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ProfileMainFragment.newInstance(false), "ProfileMainFragment")
        hideDrawer(HooleyMain.activity!!)
    }

    override fun onClickFriendProfile(position: Int) {
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, FriendsProfileFragment.newInstance(eventDetailGuestListViewModel.attendingAndInvitedList.value!![position].userId, eventDetailGuestListViewModel.attendingAndInvitedList.value!![position].fullName), "FriendsProfileFragment")
    }

    override fun onClickMessage(position: Int) {
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ChatWindowFragment("", eventDetailGuestListViewModel.attendingAndInvitedList.value!![position].userId.toString(), eventDetailGuestListViewModel.attendingAndInvitedList.value!![position].fullName), "ChatWindowFragment")
    }

    override fun onClickAcceptRequest(position: Int, value: Boolean) {
        eventDetailGuestListViewModel.acceptIgnoreFriendRequest(eventDetailGuestListViewModel.attendingAndInvitedList.value!![position].userId, value)
    }

    override fun afterTextChanged(s: Editable?) {
        eventDetailGuestListViewModel.searchingList(type, s.toString())
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
    }
}
