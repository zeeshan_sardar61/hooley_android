package com.hooleyapp.hooley.app.data.repository.media

import android.arch.lifecycle.MutableLiveData
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.app.data.model.media.MediaStreamEventListModel
import com.hooleyapp.hooley.app.data.model.media.MediaStreamTrendingModel
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GetReportEventModel
import com.hooleyapp.hooley.model.PersonalProfileModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.MediaWebService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException
import java.util.*

class MediaStreamRepository {

    var mList: MutableLiveData<ArrayList<EventGalleryFeedModel.EventMediaInfo>> = MutableLiveData()
    var tempList: MutableLiveData<ArrayList<EventGalleryFeedModel.EventMediaInfo>> = MutableLiveData()
    var failureMessage: MutableLiveData<String> = MutableLiveData()
    var sessionExpire: MutableLiveData<Boolean> = MutableLiveData()
    var reloadApi: MutableLiveData<Boolean> = MutableLiveData()
    var loading: MutableLiveData<Boolean> = MutableLiveData()
    var reportEventList: MutableLiveData<List<PersonalProfileModel.myObject>> = MutableLiveData()
    var mediaService = MediaWebService()
    private var apiCall: Call<MediaStreamEventListModel>? = null
    private var apiTrendingCall: Call<MediaStreamTrendingModel>? = null

    // calling web services
    fun getMediaStream(type: String) {
        loading.postValue(true)
        getMediaStreamAllEvents(type, object : IWebServiceCallback<MediaStreamEventListModel> {
            override fun success(result: MediaStreamEventListModel) {
                loading.postValue(false)
                mList.postValue(null)
                mList.postValue(result.myEventInfoList)
                tempList.postValue(null)
                tempList.postValue(result.myEventInfoList)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }

        })
    }

    fun getMediaStreamTrending(type: String) {
        loading.postValue(true)
        getMediaStreamTrendingEvents(type, object : IWebServiceCallback<MediaStreamTrendingModel> {
            override fun success(result: MediaStreamTrendingModel) {
                loading.postValue(false)
                mList.postValue(null)
                mList.postValue(result.mediaStreamTrendingList)
                tempList.postValue(null)
                tempList.postValue(result.mediaStreamTrendingList)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }

        })
    }

    fun likePost(eventId: String, imageId: String) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        mediaService.likePost(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
            }

            override fun failure(message: String) {
                failureMessage.postValue(message)
            }
        }, eventId, imageId)
    }

    fun mediaFav(eventId: String, imageId: String, isFav: Boolean) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        mediaService.mediaFav(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
            }

            override fun failure(message: String) {
                failureMessage.postValue(message)

            }
        }, eventId, imageId, isFav)
    }

    fun deleteMedia(imageId: String, eventId: String) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        mediaService.deleteMedia(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                reloadApi.postValue(true)
            }

            override fun failure(message: String) {
                failureMessage.postValue(message)
            }
        }, imageId, eventId)
    }

    fun submitReport(eventId: String, reasonId: String, reportMessage: String, mediaId: String) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        loading.postValue(true)
        mediaService.submitReport(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                loading.postValue(false)
            }

            override fun failure(message: String) {
                loading.postValue(false)
            }
        }, reasonId, reportMessage, mediaId, eventId)
    }

    fun getMediaReportEvent() {
        mediaService.getMediaReportEvent(object : GenericCallback<GetReportEventModel> {
            override fun success(result: GetReportEventModel) {
                reportEventList.postValue(null)
                reportEventList.postValue(result.mediaReportReasonsArrayList)
            }

            override fun failure(message: String) {
                failureMessage.postValue(message)
            }
        })
    }

    fun shareEventMedia(eventId: String?, imageId: String, shareMedia: String, allFeedItemPosition: Int) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            Util.showToastMessage(HooleyMain.activity!!, Constants.NO_NETWORK)
            return
        }
        mediaService.shareEventMedia(object : GenericCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                tempList.value!![allFeedItemPosition].shareCounts = tempList.value!![allFeedItemPosition].shareCounts + 1
                mList.postValue(null)
                mList.postValue(tempList.value)

            }

            override fun failure(message: String) {
                failureMessage.postValue(message)
            }
        }, eventId!!, imageId, shareMedia)

    }

    // RetorFit call
    private fun getMediaStreamAllEvents(type: String, callback: IWebServiceCallback<MediaStreamEventListModel>) {
        if (apiCall != null)
            apiCall?.cancel()
        if (apiTrendingCall != null)
            apiTrendingCall?.cancel()
        apiCall = HooleyApp.apiService.getMediaStream(HooleyApp.db.getString(Constants.USER_ID), type)
        apiCall?.enqueue(object : Callback<MediaStreamEventListModel> {
            override fun onResponse(call: Call<MediaStreamEventListModel>, response: Response<MediaStreamEventListModel>) {
                if (response.body() == null) {
                    if (response.errorBody() != null)
                        callback.failure(response.errorBody().toString())
                    else
                        callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<MediaStreamEventListModel>, t: Throwable) {
                if (call.isCanceled) {
                    return
                }
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    private fun getMediaStreamTrendingEvents(type: String, callback: IWebServiceCallback<MediaStreamTrendingModel>) {
        if (apiTrendingCall != null)
            apiTrendingCall?.cancel()
        if (apiCall != null)
            apiCall?.cancel()
        apiTrendingCall = HooleyApp.apiService.getMediaStreamTreeding(HooleyApp.db.getString(Constants.USER_ID), type)
        apiTrendingCall?.enqueue(object : Callback<MediaStreamTrendingModel> {
            override fun onResponse(call: Call<MediaStreamTrendingModel>, response: Response<MediaStreamTrendingModel>) {
                if (response.body() == null) {
                    if (response.errorBody() != null)
                        callback.failure(response.errorBody().toString())
                    else
                        callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<MediaStreamTrendingModel>, t: Throwable) {
                if (call.isCanceled) {
                    return
                }
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

}