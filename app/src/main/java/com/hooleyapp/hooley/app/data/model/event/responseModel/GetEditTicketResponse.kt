package com.hooleyapp.hooley.app.data.model.event.responseModel

import com.google.gson.annotations.SerializedName

data class GetEditTicketResponse(

        @field:SerializedName("ppvTicket")
        val ppvTicket: PpvTicket? = null,

        @field:SerializedName("ticketDetail")
        val ticketDetail: ArrayList<TicketDetail?>? = null,

        @field:SerializedName("donationTarget")
        val donationTarget: Int? = null,

        @field:SerializedName("Exception")
        val exception: String? = null,

        @field:SerializedName("IsSuccessful")
        val isSuccessful: Boolean? = null
)