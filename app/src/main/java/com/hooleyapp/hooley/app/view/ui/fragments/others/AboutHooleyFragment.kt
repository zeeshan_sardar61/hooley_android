package com.hooleyapp.hooley.app.view.ui.fragments.others

import android.annotation.SuppressLint
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.app.viewmodel.others.AboutHooleyViewModel
import com.hooleyapp.hooley.databinding.FragmentAboutHooleyBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.showSuccessDialog


/**
 * Created by Nauman on 3/1/2018.
 */

class AboutHooleyFragment : BaseFragment(), View.OnClickListener {
    private lateinit var binding: FragmentAboutHooleyBinding
    private lateinit var aboutHooleyViewModel: AboutHooleyViewModel
    private var updateVersionDialog: Dialog? = null
    private lateinit var updateVersion: TextView
    private lateinit var updateTxt: TextView
    private lateinit var rlCircle: RelativeLayout
    private lateinit var updateIcon: ImageView
    private lateinit var btnUpdate: TextView
    private lateinit var version: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_about_hooley, container, false)
        aboutHooleyViewModel = ViewModelProviders.of(this).get(AboutHooleyViewModel::class.java)
        setListener()
        setupDialog()
        return binding.root
    }

    private fun setListener() {
        binding.tvURL.setOnClickListener(this)
        binding.btnCheckUpdates.setOnClickListener(this)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.wvAboutHooley.settings.javaScriptEnabled = true
        setUiObserver()
    }

    private fun setUiObserver() {
        aboutHooleyViewModel.pageUrl.observe(this, Observer {
            it.let { t -> binding.wvAboutHooley.loadUrl(t) }
        })
        aboutHooleyViewModel.sessionExpire.observe(this, Observer { onTokenExpireLogOut() })
        aboutHooleyViewModel.failureMessage.observe(this, Observer { it.let { t -> activity!!.showSuccessDialog(t!!) } })

        aboutHooleyViewModel.versionObject.observe(this, Observer {
            it.let { t ->
                val newVersion = t!!.versionCode
                if (newVersion == version) {
                    rlCircle.background = ContextCompat.getDrawable(activity!!, R.drawable.bg_updated)
                    updateIcon.background = ContextCompat.getDrawable(activity!!, R.drawable.ic_updated)
                    updateTxt.text = "App is up to date"
                    btnUpdate.visibility = View.GONE
                } else {
                    rlCircle.background = ContextCompat.getDrawable(activity!!, R.drawable.bg_update_available_circle)
                    updateTxt.text = "Updates Available"
                    btnUpdate.background = ContextCompat.getDrawable(activity!!, R.drawable.bg_btn_add)
                    btnUpdate.setOnClickListener {
                        val appPackageName = activity!!.packageName
                        try {
                            startActivity(Intent(ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
                        } catch (ange: android.content.ActivityNotFoundException) {
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
                        }
                    }
                }
            }
        })
    }

    private fun setupDialog() {
        try {
            val pInfo = activity!!.packageManager.getPackageInfo(activity!!.packageName, 0)
            version = pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        updateVersionDialog = Dialog(activity)
        updateVersionDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        updateVersionDialog!!.setCancelable(true)
        updateVersionDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        updateVersionDialog!!.setContentView(R.layout.dialog_check_updates)
        updateVersion = updateVersionDialog!!.findViewById(R.id.tvUpdatesVersion)
        updateTxt = updateVersionDialog!!.findViewById(R.id.tvUpdates)
        btnUpdate = updateVersionDialog!!.findViewById(R.id.btnCheckForUpdates)
        rlCircle = updateVersionDialog!!.findViewById(R.id.rlUpdate)
        updateIcon = updateVersionDialog!!.findViewById(R.id.ivCheckUpdate)
        updateVersion.text = "Version no: $version"
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnCheckUpdates -> {
                updateVersionDialog!!.show()
                aboutHooleyViewModel.getAppVersion()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
    }

}
