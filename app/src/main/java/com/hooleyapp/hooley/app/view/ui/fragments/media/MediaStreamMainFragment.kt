package com.hooleyapp.hooley.app.view.ui.fragments.media

import android.app.Dialog
import android.arch.lifecycle.ViewModelProviders
import android.content.res.ColorStateList
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.widget.ImageViewCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.widget.*
import com.felipecsl.asymmetricgridview.AsymmetricRecyclerViewAdapter
import com.felipecsl.asymmetricgridview.Utils
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterMoreMediaDialoge
import com.hooleyapp.hooley.adapters.AdapterSpinner
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.app.view.adapter.EventFeedTrendingAdapter
import com.hooleyapp.hooley.app.view.adapter.MediaStreamAdapter
import com.hooleyapp.hooley.app.viewmodel.media.MediaStreamViewModel
import com.hooleyapp.hooley.databinding.FragmentMediaStreamMainBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.event.UpdatePostFragment
import com.hooleyapp.hooley.fragments.media.AllFeedsSinglePostCommentFragment
import com.hooleyapp.hooley.fragments.media.ViewPostMediaFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.fragments.others.MoreMediaDialogFragment
import com.hooleyapp.hooley.interfaces.IEventAllFeedsClick
import com.hooleyapp.hooley.model.PersonalProfileModel
import com.hooleyapp.hooley.others.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*


/**
 * Created by Adil Malik on 12-Mar-18.
 */

class MediaStreamMainFragment : BaseFragment(), View.OnClickListener, RadioGroup.OnCheckedChangeListener, IEventAllFeedsClick, TextView.OnEditorActionListener, TextWatcher {

    lateinit var binding: FragmentMediaStreamMainBinding
    lateinit var mediaStreamViewModel: MediaStreamViewModel
    internal val arrayList = ArrayList<AdapterMoreMediaDialoge.MoreItem>()
    private var moreTrendingDialogeFragment: MoreMediaDialogFragment? = null
    var adapterMediaStream: MediaStreamAdapter? = null
    var adapterMediaStreamTrending: EventFeedTrendingAdapter? = null
    private var morePostDialogeFragment: MoreDialogeFragment? = null
    private var showMediaReportEventDialog: Dialog? = null
    private var spinnerAdapter: AdapterSpinner? = null
    private var reasonMediaItemPosition = 0
    private var allFeedItemPosition = 0
    private var pagerPosition = 0
    private var isClicked: Boolean = false
    private lateinit var imageId: String
    private lateinit var eventId: String
    var topTabType = Constants.MEDIA_STREAM_MY_EVENTS
    var tabType = Constants.EVENT_GALLERY_ALL
    var boldFont: Typeface
    var regularFont: Typeface


    init {
        boldFont = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)!!
        regularFont = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbar()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_media_stream_main, container, false)
        mediaStreamViewModel = ViewModelProviders.of(this).get(MediaStreamViewModel::class.java)
        setListener()
        setUiObserver()
        return binding.root
    }

    private fun setUiObserver() {
        mediaStreamViewModel.reloadApi.observe(this, android.arch.lifecycle.Observer { reloadFragment() })
        mediaStreamViewModel.sessionExpire.observe(this, android.arch.lifecycle.Observer { onTokenExpireLogOut() })
        mediaStreamViewModel.failureMessage.observe(this, android.arch.lifecycle.Observer { it?.let { text -> { binding.tvNoData.text = text } } })
        mediaStreamViewModel.loading.observe(this, android.arch.lifecycle.Observer { if (it!!) binding.pbmediaStream.visibility = View.VISIBLE else binding.pbmediaStream.visibility = View.GONE })
        mediaStreamViewModel.mList.observe(this, android.arch.lifecycle.Observer {
            it?.let { list ->
                when (tabType) {
                    Constants.EVENT_GALLERY_ALL -> {
                        setMediaStreamAdapter(list)
                    }
                    Constants.EVENT_GALLERY_TRENDING -> {
                        setMediaStreamTrendingAdapter(list)
                    }
                }
            }
        })
        mediaStreamViewModel.reportEventList.observe(this, android.arch.lifecycle.Observer {
            it?.let { list ->
                showReportEventDialog(ArrayList(list))
            }
        })
    }

    private fun setToolbar() {
        HooleyMain.activity!!.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        HooleyMain.activity!!.supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
    }

    private fun setListener() {
        binding.llMyEvents.setOnClickListener(this)
        binding.llMore.setOnClickListener(this)
        binding.llExplore.setOnClickListener(this)
        binding.llFriends.setOnClickListener(this)
        binding.rlSearch.setOnClickListener(this)
        binding.etSearch.addTextChangedListener(this)
        binding.rgMediaStream.setOnCheckedChangeListener(this)
        binding.etSearch.setOnEditorActionListener(this)

    }

    fun setMoreTrendingCallerBackListener() {
        if (arrayList.size > 0)
            arrayList.clear()
        val itemAll = AdapterMoreMediaDialoge.MoreItem("All", true)
        val itemFavorites = AdapterMoreMediaDialoge.MoreItem("Favorites", false)
        val itemPhotos = AdapterMoreMediaDialoge.MoreItem("Photos", false)
        val itemVideos = AdapterMoreMediaDialoge.MoreItem("Videos", false)
        arrayList.add(itemAll)
        arrayList.add(itemFavorites)
        arrayList.add(itemPhotos)
        arrayList.add(itemVideos)
        moreTrendingDialogeFragment = MoreMediaDialogFragment.Builder(HooleyMain.activity!!).setTitle("Filter by").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    mediaStreamViewModel.filterByAll()
                    arrayList[0].isCheck = (true)
                    arrayList[1].isCheck = (false)
                    arrayList[2].isCheck = (false)
                    arrayList[3].isCheck = (false)
                    moreTrendingDialogeFragment!!.dismiss()
                }
                1 -> {
                    mediaStreamViewModel.filterByFav()
                    arrayList[0].isCheck = (false)
                    arrayList[1].isCheck = (true)
                    arrayList[2].isCheck = (false)
                    arrayList[3].isCheck = (false)
                    moreTrendingDialogeFragment!!.dismiss()
                }
                2 -> {
                    mediaStreamViewModel.filterByPhotos()
                    arrayList[0].isCheck = (false)
                    arrayList[1].isCheck = (false)
                    arrayList[2].isCheck = (true)
                    arrayList[3].isCheck = (false)
                    moreTrendingDialogeFragment!!.dismiss()
                }
                3 -> {
                    mediaStreamViewModel.filterByVideo()
                    arrayList[0].isCheck = (false)
                    arrayList[1].isCheck = (false)
                    arrayList[2].isCheck = (false)
                    arrayList[3].isCheck = (true)
                    moreTrendingDialogeFragment!!.dismiss()
                }
            }
        }.create()

    }

    fun reloadFragment() {
        when (tabType) {
            Constants.EVENT_GALLERY_ALL -> {
                mediaStreamViewModel.getMediaStream(topTabType)
            }
            Constants.EVENT_GALLERY_TRENDING -> {
                mediaStreamViewModel.getMediaStreamTrending(topTabType)
            }
        }
    }

    // setting adapters
    private fun setMediaStreamAdapter(list: ArrayList<EventGalleryFeedModel.EventMediaInfo>) {
        binding.rvMediaStream.visibility = View.VISIBLE
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_from_right)
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding.rvMediaStream.setEmptyView(binding.tvNoData)
        binding.rvMediaStream.layoutManager = manager
        binding.rvMediaStream.layoutAnimation = animation
        adapterMediaStream = MediaStreamAdapter(HooleyMain.activity!!, list)
        adapterMediaStream?.mListener = this
        binding.rvMediaStream.adapter = adapterMediaStream
    }

    private fun setMediaStreamTrendingAdapter(list: ArrayList<EventGalleryFeedModel.EventMediaInfo>) {
        binding.rvTrending.visibility = View.VISIBLE
        binding.rvTrending.setRequestedColumnCount(3)
        binding.rvTrending.isDebugging = false
        binding.rvTrending.requestedHorizontalSpacing = Utils.dpToPx(HooleyMain.activity!!, 3f)
        if (adapterMediaStreamTrending == null)
            binding.rvTrending.addItemDecoration(SpacesItemDecoration(HooleyMain.activity!!.resources.getDimensionPixelSize(R.dimen.recycler_padding)))
        adapterMediaStreamTrending = EventFeedTrendingAdapter(list)
        adapterMediaStreamTrending!!.mListener = this
        binding.rvTrending.adapter = AsymmetricRecyclerViewAdapter<RecyclerView.ViewHolder>(HooleyMain.activity, binding.rvTrending, adapterMediaStreamTrending!!)
    }

    private fun setTopTabs() {
        when (topTabType) {
            Constants.MEDIA_STREAM_MY_EVENTS -> {
                binding.etSearch.visibility = View.GONE
                binding.rlSearch.visibility = View.GONE
                binding.llMyEvents.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                binding.llMore.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding.llExplore.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding.llFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                setMoreTrendingCallerBackListener()
            }
            Constants.MEDIA_STREAM_EXPLORE -> {
                binding.etSearch.visibility = View.GONE
                binding.rlSearch.visibility = View.VISIBLE
                binding.llMyEvents.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding.llMore.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding.llExplore.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                binding.llFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                setMoreTrendingCallerBackListener()
            }
            Constants.MEDIA_STREAM_FRIENDS -> {
                binding.etSearch.visibility = View.GONE
                binding.rlSearch.visibility = View.GONE
                binding.llMyEvents.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding.llMore.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding.llExplore.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding.llFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_box_selected_color))
                setMoreTrendingCallerBackListener()
            }
            else -> {
                binding.llMyEvents.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding.llExplore.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
                binding.llFriends.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_bg_layout))
            }
        }
    }

    private fun setTabs() {
        when (tabType) {
            Constants.EVENT_GALLERY_ALL -> {
                binding.rvMediaStream.visibility = View.GONE
                binding.rvTrending.visibility = View.GONE
                binding.tvNoData.visibility = View.GONE
                binding.rbEventList.typeface = boldFont
                binding.rbTrending.typeface = regularFont
                binding.rbEventList.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding.rbEventList.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_left_radio)
                binding.rbTrending.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbTrending.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                mediaStreamViewModel.getMediaStream(topTabType)
            }
            Constants.EVENT_GALLERY_TRENDING -> {
                binding.rvMediaStream.visibility = View.GONE
                binding.rvTrending.visibility = View.GONE
                binding.tvNoData.visibility = View.GONE
                binding.rbEventList.typeface = regularFont
                binding.rbTrending.typeface = boldFont
                binding.rbTrending.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding.rbTrending.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_right_radio)
                binding.rbEventList.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbEventList.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                mediaStreamViewModel.getMediaStreamTrending(topTabType)
            }
        }
    }

    private fun showShareEventDialog() {
        object : GetBitmapTask() {
            override fun onPostExecute(bmp: Bitmap) {
                super.onPostExecute(bmp)
                if (bmp != null)
                    (HooleyMain.activity as HooleyMain).shareWithImage(bmp, mediaStreamViewModel.mList.value!![allFeedItemPosition].eventName!!, mediaStreamViewModel.mList.value!![allFeedItemPosition].imageCaption, mediaStreamViewModel.mList.value!![allFeedItemPosition].eventId!!, mediaStreamViewModel.mList.value!![allFeedItemPosition].imageId)
                else
                    Util.showToastMessage(HooleyMain.activity!!, "Image is not valid")
            }
        }.execute(mediaStreamViewModel.mList.value!![allFeedItemPosition].mediaFiles[pagerPosition].mediaUrl)
    }

    private fun showReportEventDialog(list: ArrayList<PersonalProfileModel.myObject>) {
        showMediaReportEventDialog = Dialog(HooleyMain.activity!!)
        showMediaReportEventDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        showMediaReportEventDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        showMediaReportEventDialog!!.setContentView(R.layout.dialog_report_event)
        val btnReport = showMediaReportEventDialog!!.findViewById<TextView>(R.id.btnReport)
        val edtMessage = showMediaReportEventDialog!!.findViewById<EditText>(R.id.edtMessage)
        val spSelectReason = showMediaReportEventDialog!!.findViewById<Spinner>(R.id.spSelectReason)
        spinnerAdapter = AdapterSpinner(activity, R.layout.sp_gender_group_item, list)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spSelectReason.adapter = spinnerAdapter
        spSelectReason.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                reasonMediaItemPosition = position
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
        btnReport.setOnClickListener {
            showMediaReportEventDialog!!.dismiss()
            mediaStreamViewModel.submitReport(eventId, list[reasonMediaItemPosition].id!!, edtMessage.text.toString(), imageId)
        }
        showMediaReportEventDialog!!.show()
    }

    override fun onClickPostComment(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, type: Int) {
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, AllFeedsSinglePostCommentFragment.newInstance(eventMediaInfo.eventId!!, eventMediaInfo.imageId), "AllFeedsSinglePostCommentFragment")
    }

    override fun onClickMore(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, pos: Int, listPosition: Int, mType: Int) {
        val arrayList = ArrayList<String>()
        if (eventMediaInfo.isFavorited)
            arrayList.add("Remove From Favorite")
        else
            arrayList.add("Add to Favorite")

        if (eventMediaInfo.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
            arrayList.add("Edit Post")

        arrayList.add("Download/Share Photo")

        if (eventMediaInfo.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
            arrayList.add("Delete Photo")
        else
            arrayList.add("Report Photo")
        morePostDialogeFragment = MoreDialogeFragment.Builder(activity!!).setTitle("More").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    if (eventMediaInfo.isFavorited) {
                        mediaStreamViewModel.mList.value!![pos].favoriteCount++
                        mediaStreamViewModel.mList.value!![pos].isFavorited = true
                    } else {
                        mediaStreamViewModel.mList.value!![pos].favoriteCount--
                        mediaStreamViewModel.mList.value!![pos].isFavorited = false
                    }
                    if (adapterMediaStream != null)
                        adapterMediaStream?.notifyDataSetChanged()
                    mediaStreamViewModel.mediaFav(eventMediaInfo.eventId!!, eventMediaInfo.imageId, !eventMediaInfo.isFavorited)
                    morePostDialogeFragment!!.dismiss()
                }
                1 -> {
                    morePostDialogeFragment!!.dismiss()
                    if (eventMediaInfo.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
                        (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, UpdatePostFragment(eventMediaInfo.eventId!!, eventMediaInfo.imageId), "UpdatePostFragment")
                    else {
                        onClickShare(eventMediaInfo.imageId, pos, listPosition, mType)
                    }
                }
                2 -> {
                    morePostDialogeFragment!!.dismiss()
                    if (arrayList.size == 3) {
                        if (eventMediaInfo.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
                            mediaStreamViewModel.deleteMedia(eventMediaInfo.imageId, eventMediaInfo.eventId!!)
                        else {
                            imageId = eventMediaInfo.imageId
                            eventId = eventMediaInfo.eventId!!
                            mediaStreamViewModel.getMediaReportEvent()
                        }
                    } else {
                        onClickShare(eventMediaInfo.imageId, pos, listPosition, mType)
                    }
                }
                3 -> {
                    morePostDialogeFragment!!.dismiss()
                    if (eventMediaInfo.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
                        mediaStreamViewModel.deleteMedia(eventMediaInfo.imageId, eventMediaInfo.eventId!!)
                    else {
                        imageId = eventMediaInfo.imageId
                        eventId = eventMediaInfo.eventId!!
                        mediaStreamViewModel.getMediaReportEvent()
                    }
                }
            }
        }.create()

        if (!morePostDialogeFragment!!.isAdded)
            morePostDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)

    }

    override fun onClickImageView(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo) {
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ViewPostMediaFragment.newInstance(eventMediaInfo), "ViewPostMediaFragment")
    }

    override fun onClickLikePost(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, position: Int, type: Int) {
        mediaStreamViewModel.likePost(eventMediaInfo.eventId!!, eventMediaInfo.imageId)
    }

    override fun onClickFav(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, position: Int, type: Int) {
        mediaStreamViewModel.mediaFav(eventMediaInfo.eventId!!, eventMediaInfo.imageId, !eventMediaInfo.isFavorited)

    }

    override fun onClickShare(imageId: String, position: Int, listPosition: Int, type: Int) {
        allFeedItemPosition = position
        pagerPosition = listPosition
        showShareEventDialog()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.llMyEvents -> {
                topTabType = Constants.MEDIA_STREAM_MY_EVENTS
                setTopTabs()
                setTabs()
            }
            R.id.llExplore -> {
                topTabType = Constants.MEDIA_STREAM_EXPLORE
                setTopTabs()
                setTabs()
            }
            R.id.llFriends -> {
                topTabType = Constants.MEDIA_STREAM_FRIENDS
                setTopTabs()
                setTabs()
            }
            R.id.llMore -> {

                if (!moreTrendingDialogeFragment!!.isAdded)
                    moreTrendingDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)

            }
            R.id.rlSearch -> {
                if (isClicked) {
                    isClicked = false
                    binding.etSearch.visibility = View.GONE
                    binding.rlSearch.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_radio_group)
                    ImageViewCompat.setImageTintList(binding.ivSearch, ColorStateList.valueOf(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color)))
                    Util.hideKeyboard(HooleyMain.activity!!)
                } else {
                    isClicked = true
                    binding.etSearch.visibility = View.VISIBLE
                    binding.rlSearch.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_search)
                    ImageViewCompat.setImageTintList(binding.ivSearch, ColorStateList.valueOf(ContextCompat.getColor(HooleyMain.activity!!, R.color.simple_white)))
                }
            }
        }
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            if (binding.etSearch.text.toString().isNotEmpty()) {
                Util.hideKeyboard(HooleyMain.activity!!)
                mediaStreamViewModel.searchString(binding.etSearch.text.toString())
            } else {
                Util.hideKeyboard(HooleyMain.activity!!)
            }
            return true
        }
        return false
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when (checkedId) {
            R.id.rbEventList -> {
                tabType = Constants.EVENT_GALLERY_ALL
                setTabs()
            }
            R.id.rbTrending -> {
                tabType = Constants.EVENT_GALLERY_TRENDING
                setTabs()
            }
        }
    }

    override fun afterTextChanged(s: Editable?) {
        try {
            mediaStreamViewModel.searchString(s.toString())
        } catch (e: Exception) {
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        Toast.makeText(HooleyMain.activity, event.message, Toast.LENGTH_SHORT).show()
        mediaStreamViewModel.shareEventMedia(mediaStreamViewModel.mList.value!![allFeedItemPosition].eventId, mediaStreamViewModel.mList.value!![allFeedItemPosition].imageId, event.message, allFeedItemPosition)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        setTopTabs()
        setTabs()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
    }

}
