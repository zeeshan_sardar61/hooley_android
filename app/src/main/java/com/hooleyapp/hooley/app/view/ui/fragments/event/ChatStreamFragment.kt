package com.hooleyapp.hooley.app.view.ui.fragments.event

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat.getColor
import android.support.v4.content.ContextCompat.getDrawable
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.widget.RadioGroup
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterChatStream
import com.hooleyapp.hooley.app.viewmodel.event.ChatStreamViewModel
import com.hooleyapp.hooley.databinding.FragmentChatStreamBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.event.LiveEventFragment
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.model.GetChatStreamModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.Util


/**
 * Created by Nauman on 3/13/2018.
 */

class ChatStreamFragment : BaseFragment(), View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    lateinit var binding: FragmentChatStreamBinding
    lateinit var chatStreamViewModel: ChatStreamViewModel
    private var adapterChatStream: AdapterChatStream? = null
    private val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.extras!!.getString("tag") == Constants.TYPE_MESSAGES_ALERT) {
                val streamMsg = GetChatStreamModel.ChatStream()
                streamMsg.profilePic = intent.extras!!.getString("profilePic")
                streamMsg.senderId = intent.extras!!.getString("userId")
                streamMsg.messageText = intent.extras!!.getString("body")
                streamMsg.messageTime = "Just Now"
                chatStreamViewModel.mList.value!!.add(chatStreamViewModel.mList.value!!.size, streamMsg)
                adapterChatStream!!.notifyDataSetChanged()
                binding.rvChatStream.scrollToPosition(chatStreamViewModel.mList.value!!.size - 1)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("SetJavaScriptEnabled", "PrivateApi")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat_stream, container, false)
        chatStreamViewModel = ViewModelProviders.of(this).get(ChatStreamViewModel::class.java)
        setToolbar()
        binding.rgTab.setOnCheckedChangeListener(this)
        chatStreamViewModel.getMyChatStreamMessages(eventId!!)
        binding.wvTwitter.settings.javaScriptEnabled = true
        binding.wvTwitter.settings.domStorageEnabled = true
        binding.wvTwitter.settings.setAppCacheEnabled(true)
        binding.wvTwitter.settings.loadsImagesAutomatically = true
        binding.wvTwitter.settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        try {
            val clsWebSettingsClassic = HooleyMain.activity!!.classLoader.loadClass("android.webkit.WebSettingsClassic")
            val md = clsWebSettingsClassic.getMethod(
                    "setProperty", String::class.java, String::class.java)
            md.invoke(binding.wvTwitter.settings, "inverted", "true")
            md.invoke(binding.wvTwitter.settings, "inverted_contrast", "1")
        } catch (e: Exception) {
        }

        chatStreamViewModel.getTwitterTweets(eventId!!)
        LocalBroadcastManager.getInstance(HooleyMain.activity!!).registerReceiver(mMessageReceiver,
                IntentFilter("ChatStreamMessage"))
        if (!LiveEventFragment.isPostAllow) {
            binding.rlChat.visibility = View.GONE
        }
        setUiObserver()
        if (!canChat) {
            binding.edtMessage.isEnabled = false
            binding.edtMessage.setText("You must attend or follow the event before chat")
            binding.btnSendMsg.isEnabled = false
        }
        return binding.root
    }

    fun setUiObserver() {
        chatStreamViewModel.loading.observe(this, Observer {
            it?.let { showProgress ->
                if (showProgress)
                    binding.pbChatStream.visibility = View.VISIBLE
                else
                    binding.pbChatStream.visibility = View.GONE
            }
        })
        chatStreamViewModel.sessionExpire.observe(this, Observer {
            it?.let {
                onTokenExpireLogOut()
            }
        })
        chatStreamViewModel.failureMessage.observe(this, Observer {
            it?.let { message ->
                HooleyMain.activity!!.showSuccessDialog(message)
            }
        })
        chatStreamViewModel.mList.observe(this, Observer {
            it?.let { list ->
                setChatStreamRv(list)
                setListener()
            }
        })
        chatStreamViewModel.pageUrl.observe(this, Observer {
            it?.let { url ->
                binding.wvTwitter.loadUrl(url)
            }
        })
    }

    private fun setTabs(value: Boolean) {
        if (value) {
            binding.rbChat.typeface = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)
            binding.rbSocialFeed.typeface = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)
            binding.rbChat.setTextColor(getColor(HooleyMain.activity!!, android.R.color.white))
            binding.rbChat.background = getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_left_radio)
            binding.rbSocialFeed.setTextColor(getColor(HooleyMain.activity!!, R.color.app_purple_color))
            binding.rbSocialFeed.setBackgroundColor(getColor(HooleyMain.activity!!, android.R.color.transparent))
            binding.rlChatStream.visibility = View.VISIBLE
            binding.wvTwitter.visibility = View.GONE
        } else {
            binding.rlChatStream.visibility = View.GONE
            binding.wvTwitter.visibility = View.VISIBLE
            binding.rbChat.typeface = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)
            binding.rbSocialFeed.typeface = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)
            binding.rbChat.setTextColor(getColor(HooleyMain.activity!!, R.color.app_purple_color))
            binding.rbChat.setBackgroundColor(getColor(HooleyMain.activity!!, android.R.color.transparent))
            binding.rbSocialFeed.setTextColor(getColor(HooleyMain.activity!!, android.R.color.white))
            binding.rbSocialFeed.background = getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_right_radio)
        }
    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        when (checkedId) {
            R.id.rbChat -> {
                setTabs(true)

            }
            R.id.rbSocialFeed -> {
                setTabs(false)
            }
        }
    }

    private fun setToolbar() {
        HooleyMain.activity!!.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        HooleyMain.activity!!.supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
    }

    private fun setListener() {
        binding.btnSendMsg.setOnClickListener(this)
    }

    private fun setChatStreamRv(list: ArrayList<GetChatStreamModel.ChatStream>) {
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding.rvChatStream.layoutManager = manager
        adapterChatStream = AdapterChatStream(HooleyMain.activity!!, list)
        binding.rvChatStream.adapter = adapterChatStream
        binding.rvChatStream.scrollToPosition(list.size - 1)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
        LocalBroadcastManager.getInstance(HooleyMain.activity!!).unregisterReceiver(mMessageReceiver)
        Util.hideKeyboard(HooleyMain.activity!!)

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnSendMsg -> if (binding.edtMessage.text.toString().trim { it <= ' ' }.isEmpty()) {
                binding.edtMessage.requestFocus()
                binding.edtMessage.error = getString(R.string.required)
                return
            } else {
                val msg = binding.edtMessage.text.toString()
                chatStreamViewModel.sendTextStreamMessage(eventId!!, msg)
                binding.edtMessage.setText("")
                val streamMsg = GetChatStreamModel.ChatStream()
                streamMsg.profilePic = HooleyApp.db.getString(Constants.USER_AVATAR)
                streamMsg.senderId = HooleyApp.db.getString(Constants.USER_ID)
                streamMsg.messageText = msg
                streamMsg.messageTime = "Just Now"
                chatStreamViewModel.mList.value!!.add(chatStreamViewModel.mList.value!!.size, streamMsg)
                adapterChatStream!!.notifyDataSetChanged()
                binding.rvChatStream.scrollToPosition(chatStreamViewModel.mList.value!!.size - 1)
            }
        }
    }


    companion object {

        @SuppressLint("StaticFieldLeak")
        lateinit var instance: ChatStreamFragment
        private var eventId: String? = null
        private var canChat = false

        fun newInstance(id: String, isAttended: Boolean): ChatStreamFragment {
            eventId = id
            canChat = isAttended
            instance = ChatStreamFragment()
            return instance

        }
    }
}
