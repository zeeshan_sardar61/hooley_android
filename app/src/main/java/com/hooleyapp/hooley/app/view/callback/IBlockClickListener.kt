package com.hooleyapp.hooley.app.view.callback

interface IBlockClickListener {
    fun onClickUnBlockUser(position: Int)
}