package com.hooleyapp.hooley.app.view.ui.fragments.guest

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventDetailModel
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.app.view.adapter.EventGalleryAdapter
import com.hooleyapp.hooley.app.view.ui.activites.HooleyGuestUser
import com.hooleyapp.hooley.app.viewmodel.event.EventGalleryViewModel
import com.hooleyapp.hooley.databinding.FragmentEventGalleryBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.event.GuestUserLiveEventFragment
import com.hooleyapp.hooley.helper.DialogGuestUser
import com.hooleyapp.hooley.interfaces.IEventAllFeedsClick
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.GetBitmapTask
import com.hooleyapp.hooley.others.MessageEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


@SuppressLint("ValidFragment")
class GuestUserEventGalleryFragment constructor(var objectRespons: EventDetailModel) : BaseFragment(), IEventAllFeedsClick, SwipeRefreshLayout.OnRefreshListener {

    lateinit var binding: FragmentEventGalleryBinding
    lateinit var eventGalleryViewModel: EventGalleryViewModel
    private var adapterAllEventFeed: EventGalleryAdapter? = null
    private var allFeedItemPosition = 0
    private var pagerPosition = 0
    private lateinit var imageId: String

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser && isResumed)
            if (::eventGalleryViewModel.isInitialized)
                eventGalleryViewModel.getGuestUserEventGalleryFeeds(GuestUserLiveEventFragment.eventId, Constants.EVENT_GALLERY_ALL)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_event_gallery, container, false)
        eventGalleryViewModel = ViewModelProviders.of(this).get(EventGalleryViewModel::class.java)
        binding.rlTabs.visibility = View.GONE
        setListener()
        setUiObserver()

        if (!objectRespons.isPpvPurchased && objectRespons.ppvPrice > 0.0) {
            binding.llppvView.visibility = View.VISIBLE
            binding.tvEventPrice.text = "$" + objectRespons.ppvPrice.toInt().toString()
        } else {
            binding.llppvView.visibility = View.GONE
        }

        binding.btnBuyNow.setOnClickListener {
            HooleyGuestUser.activity.DialogGuestUser()
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdapter()
    }


    fun setUiObserver() {
        eventGalleryViewModel.sessionExpire.observe(this, android.arch.lifecycle.Observer { onTokenExpireLogOut() })
        eventGalleryViewModel.failureMessage.observe(this, android.arch.lifecycle.Observer { binding.tvNoData.text = it!! })
        eventGalleryViewModel.loading.observe(this, android.arch.lifecycle.Observer { if (it!!) binding.pbAll.visibility = View.VISIBLE else binding.pbAll.visibility = View.GONE })
        eventGalleryViewModel.mList.observe(this, android.arch.lifecycle.Observer {
            if (binding.slGallery.isRefreshing)
                binding.slGallery.isRefreshing = false

            it.let { mList ->
                adapterAllEventFeed?.setList(mList!!)
            }
        })
    }

    private fun setListener() {
        binding.slGallery.setOnRefreshListener(this)
    }

    fun setAdapter() {
        binding.rvAll.visibility = View.VISIBLE
        binding.rvTrending.visibility = View.GONE
        binding.rvFriends.visibility = View.GONE
        val animation = AnimationUtils.loadLayoutAnimation(HooleyGuestUser.activity, R.anim.layout_animation_from_right)
        val manager = LinearLayoutManager(HooleyGuestUser.activity, LinearLayoutManager.VERTICAL, false)
        binding.rvAll.layoutAnimation = animation
        binding.rvAll.layoutManager = manager
        binding.rvAll.setEmptyView(binding.tvNoData)
        adapterAllEventFeed = EventGalleryAdapter()
        adapterAllEventFeed!!.mListener = this
        binding.rvAll.adapter = adapterAllEventFeed
    }

    override fun onClickPostComment(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, type: Int) {
        HooleyGuestUser.activity.DialogGuestUser()
    }

    override fun onClickMore(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, mpos: Int, mlistPosition: Int, mType: Int) {
        HooleyGuestUser.activity.DialogGuestUser()
    }

    override fun onClickImageView(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo) {
        HooleyGuestUser.activity.DialogGuestUser()
    }

    override fun onClickLikePost(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, position: Int, type: Int) {
        HooleyGuestUser.activity.DialogGuestUser()
    }

    override fun onClickFav(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, position: Int, type: Int) {
        HooleyGuestUser.activity.DialogGuestUser()
    }

    override fun onClickShare(imId: String, pos: Int, pagerPos: Int, type: Int) {
        imageId = imId
        allFeedItemPosition = pos
        pagerPosition = pagerPos
        showShareEventDialog()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        eventGalleryViewModel.shareEventMedia(GuestUserLiveEventFragment.eventId, imageId, event.message, allFeedItemPosition)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }


    private fun showShareEventDialog() {
        object : GetBitmapTask() {
            override fun onPostExecute(bmp: Bitmap) {
                super.onPostExecute(bmp)
                (HooleyGuestUser.activity as HooleyGuestUser).shareWithImage(bmp, eventGalleryViewModel.mList.value!![allFeedItemPosition].eventName!!, eventGalleryViewModel.mList.value!![allFeedItemPosition].imageCaption, eventGalleryViewModel.mList.value!![allFeedItemPosition].eventId!!, eventGalleryViewModel.mList.value!![allFeedItemPosition].imageId)
            }
        }.execute(eventGalleryViewModel.mList.value!![allFeedItemPosition].mediaFiles[pagerPosition].thumbnailUrl)
    }

    override fun onRefresh() {
        binding.slGallery.isRefreshing = true
        binding.pbAll.visibility = View.GONE
        HooleyApp.db.putBoolean(Constants.SHOW_GALLERY_ALERT, false)
        eventGalleryViewModel.getGuestUserEventGalleryFeeds(GuestUserLiveEventFragment.eventId, Constants.EVENT_GALLERY_ALL)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
    }
}
