package com.hooleyapp.hooley.app.view.adapter

import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.felipecsl.asymmetricgridview.AGVRecyclerViewAdapter
import com.felipecsl.asymmetricgridview.AsymmetricItem
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.databinding.AdapterEventFeedTrendingBinding
import com.hooleyapp.hooley.fragments.media.ViewPostMediaFragment
import com.hooleyapp.hooley.helper.load
import com.hooleyapp.hooley.interfaces.IEventAllFeedsClick
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.GalleryItem
import com.hooleyapp.hooley.others.Util
import java.util.*

/**
 * Created by Zeeshan on 19-Jan-18.
 */

class EventFeedTrendingAdapter(var mList: ArrayList<EventGalleryFeedModel.EventMediaInfo>) : AGVRecyclerViewAdapter<RecyclerView.ViewHolder>() {
    var mListener: IEventAllFeedsClick? = null
    var items: List<GalleryItem> = Util.moarItems(mList.size)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding: AdapterEventFeedTrendingBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_event_feed_trending, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        holder.bindDataWithHolder(mList[position])
        holder.binding.sdvPostImage.setOnClickListener { mListener!!.onClickImageView(mList[position]) }
        holder.binding.ivFav.setOnClickListener {
            mListener!!.onClickFav(mList[position], position, Constants.TYPE_EVENT_FEED_TRENDING)
            if (mList[position].isFavorited) {
                mList[position].isFavorited = false
                mList[position].favoriteCount--
            } else {
                mList[position].isFavorited = true
                mList[position].favoriteCount++
            }
            notifyDataSetChanged()
        }
        holder.binding.ivTrending.setOnClickListener {
            (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ViewPostMediaFragment.newInstance(mList[position]), "ViewPostMediaFragment")
            //            (HooleyMain.activity as ActivityBase).callFragment(R.id.container, VideoPlayerActivity.newInstance(mList[position].videoUrl), "VideoPlayerActivity")
        }
    }

    override fun getItem(position: Int): AsymmetricItem {
        return items[position]
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var binding: AdapterEventFeedTrendingBinding = DataBindingUtil.bind(v)!!

        fun bindDataWithHolder(item: EventGalleryFeedModel.EventMediaInfo) {
            if (item.mediaFiles.size > 1) {
                binding.rlMultiple.visibility = View.VISIBLE
            } else {
                binding.rlMultiple.visibility = View.GONE
            }
            try {
                binding.ivTrending.load(item.mediaFiles[0].thumbnailUrl!!)
            } catch (e: IndexOutOfBoundsException) {
                e.printStackTrace()
            }
            if (item.isFavorited) {
                binding.ivFav.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_fav_orrange)
            } else {
                binding.ivFav.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_unfav_orange)
            }
        }
    }

    internal fun setList(list: ArrayList<EventGalleryFeedModel.EventMediaInfo>) {
        this.mList = list
        items = Util.moarItems(list.size)
        notifyDataSetChanged()
        notifyItemChanged(list.size)
    }
}
