package com.hooleyapp.hooley.app.view.ui.fragments.event

import android.app.PendingIntent
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.RadioGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.view.adapter.PopularInFriendsAdapter
import com.hooleyapp.hooley.app.view.callback.IEventClickListener
import com.hooleyapp.hooley.app.viewmodel.event.PopularInFriendsViewModel
import com.hooleyapp.hooley.databinding.FragmentBookmarkEventsBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.event.LiveEventFragment
import com.hooleyapp.hooley.fragments.invites.ShowSelectedUserFragment
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.others.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * Created by Nauman on 3/29/2018.
 */

class PopularInFriendsFragment : BaseFragment(), RadioGroup.OnCheckedChangeListener, SwipeRefreshLayout.OnRefreshListener, IEventClickListener {

    internal lateinit var binding: FragmentBookmarkEventsBinding
    internal var adapter: PopularInFriendsAdapter? = null
    private lateinit var popularInFriendsViewModel: PopularInFriendsViewModel
    private var pos: Int = -1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bookmark_events, container, false)
        popularInFriendsViewModel = ViewModelProviders.of(this).get(PopularInFriendsViewModel::class.java)
        setBookmarkRv()
        setUiObservers()
        setListener()
        return binding.root
    }

    private fun setUiObservers() {
        popularInFriendsViewModel.popularInFriendsEventList.observe(this, android.arch.lifecycle.Observer { item ->
            item?.let {
                if (binding.slBookmarks.isRefreshing) {
                    binding.slBookmarks.isRefreshing = false
                    binding.pbBookmark.visibility = View.GONE
                }
                adapter?.setList(it)
            }
        })
        popularInFriendsViewModel.failureMessage.observe(this,
                android.arch.lifecycle.Observer {
                    it?.let { text ->
                        binding.tvNoData.text = text
                    }
                })
        popularInFriendsViewModel.sessionExpire.observe(this,
                android.arch.lifecycle.Observer {
                    onTokenExpireLogOut()
                })
        popularInFriendsViewModel.loading.observe(this, Observer {
            if (it!!)
                binding.pbBookmark.visibility = View.VISIBLE
            else
                binding.pbBookmark.visibility = View.GONE
        })
    }

    private fun setListener() {
        binding.rgTab.setOnCheckedChangeListener(this)
        binding.slBookmarks.setOnRefreshListener(this)
    }

    private fun setTabs(value: Boolean) {
        if (value) {
            binding.rbAllEvent.typeface = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)
            binding.rbPastEvents.typeface = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)
            binding.rbAllEvent.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
            binding.rbAllEvent.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_left_radio)
            binding.rbPastEvents.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
            binding.rbPastEvents.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
            binding.tvNoData.text = "No Upcoming Events"
        } else {
            binding.rbAllEvent.typeface = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)
            binding.rbPastEvents.typeface = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)
            binding.rbAllEvent.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
            binding.rbAllEvent.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
            binding.rbPastEvents.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
            binding.rbPastEvents.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_right_radio)
            binding.tvNoData.text = "No Past Events"
        }
    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        when (checkedId) {
            R.id.rbAllEvent -> {
                popularInFriendsViewModel.setSelectetab(1)
                popularInFriendsViewModel.filterListByAll()
                setTabs(true)

            }
            R.id.rbPastEvents -> {
                popularInFriendsViewModel.setSelectetab(2)
                popularInFriendsViewModel.filterListPast()
                setTabs(false)
            }
        }
    }

    override fun onClickItem(position: Int) {
        (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment
                .newInstance(popularInFriendsViewModel.popularInFriendsEventList.value!![position].eventId!!), "LiveEventFragment")
        hideDrawer(HooleyMain.activity!!)
    }

    override fun onClickLike(position: Int) {
        popularInFriendsViewModel.likeEvent(position)
    }

    override fun onClickShare(position: Int) {
        pos = position
        object : GetBitmapTask() {
            override fun onPostExecute(bmp: Bitmap) {
                super.onPostExecute(bmp)
                //                HooleyMain.removeDialog();
                showShareEventDialog(bmp)
            }
        }.execute(popularInFriendsViewModel.popularInFriendsEventList.value!![position].coverPhoto)
    }

    override fun onClickFollow(position: Int) {
        popularInFriendsViewModel.followEvent(position)
    }

    override fun onMapClick(position: Int) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + popularInFriendsViewModel.popularInFriendsEventList.value!![position].eventLat + "," + popularInFriendsViewModel.popularInFriendsEventList.value!![position].eventLong))
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        HooleyMain.activity!!.startActivity(intent)
    }

    override fun onClickInterestedPoeople(position: Int) {
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ShowSelectedUserFragment(popularInFriendsViewModel.popularInFriendsEventList.value!![position].friendList, false), "ShowSelectedUserFragment")

    }

    override fun onRefresh() {
        binding.slBookmarks.isRefreshing = true
        binding.pbBookmark.visibility = View.GONE
        popularInFriendsViewModel.onRefreshList()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        popularInFriendsViewModel.shareEvent(event.message, pos)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    private fun setBookmarkRv() {
        val manager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_from_right)
        binding.rvBookmarkEvent.layoutAnimation = animation
        binding.rvBookmarkEvent.layoutManager = manager
        binding.rvBookmarkEvent.setEmptyView(binding.tvNoData)
        adapter = PopularInFriendsAdapter(HooleyMain.activity!!)
        adapter!!.mListener = this
        binding.rvBookmarkEvent.adapter = adapter
    }

    private fun showShareEventDialog(bitmap: Bitmap) {
        val bitmapPath = MediaStore.Images.Media.insertImage(HooleyMain.activity!!.contentResolver, bitmap, "Thumb", null)
        val bitmapUri = Uri.parse(bitmapPath)
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "image/png"
        intent.type = "text/plain"
        val receiver = Intent(HooleyMain.activity, ApplicationSelectorReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(HooleyMain.activity, 0, receiver, PendingIntent.FLAG_UPDATE_CURRENT)
        intent.putExtra(Intent.EXTRA_STREAM, bitmapUri)
        intent.putExtra(Intent.EXTRA_TEXT,
                Constants.EVENT_SHARE + popularInFriendsViewModel.popularInFriendsEventList.value!![pos].eventId
                        + "\n" + popularInFriendsViewModel.popularInFriendsEventList.value!![pos].eventName
                        + "\nStart Date = " + DateUtils.formatDateNew(popularInFriendsViewModel.popularInFriendsEventList.value!![pos].startTime!!))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            startActivityForResult(Intent.createChooser(intent, null, pendingIntent.intentSender), 7)
        } else {
            startActivityForResult(Intent.createChooser(intent, "Share"), 7)
        }
    }
}
