package com.hooleyapp.hooley.app.data.repository.others

import android.arch.lifecycle.MutableLiveData
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.services.HooleyWebService

class FeedBackRepository {
    var service = HooleyWebService()
    var failureMessage: MutableLiveData<String> = MutableLiveData()
    var sessionExpire: MutableLiveData<Boolean> = MutableLiveData()
    var loading: MutableLiveData<Boolean> = MutableLiveData()
    var success: MutableLiveData<Boolean> = MutableLiveData()


    fun addFeedBack(feedBackMessage: String, rating: String) {
        loading.postValue(true)
        service.addUserFeedBack(feedBackMessage, rating, object : IWebServiceCallback<GeneralModel> {

            override fun success(result: GeneralModel) {
                loading.postValue(false)
                success.postValue(true)
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }
        })
    }

    fun addGuestUserFeedBack(feedBackMessage: String, rating: String) {
        loading.postValue(true)
        service.addUserFeedBack(feedBackMessage, rating, object : IWebServiceCallback<GeneralModel> {

            override fun success(result: GeneralModel) {
                loading.postValue(false)
                success.postValue(true)
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }
        })
    }
}