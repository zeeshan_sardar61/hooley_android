package com.hooleyapp.hooley.app.data.model.guestuser

data class GuestUserLoginRequestModel(var deviceId: String? = null, var deviceToken: String? = null, var deviceType: String? = null, var latitude: String? = null, var longitude: String? = null)