package com.hooleyapp.hooley.app.view.ui.fragments.event

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventAnnouncementModel
import com.hooleyapp.hooley.app.view.adapter.AnnouncementsAdapter
import com.hooleyapp.hooley.app.view.callback.IAnnouncementClickListener
import com.hooleyapp.hooley.app.viewmodel.event.EventAnnouncementViewModel
import com.hooleyapp.hooley.databinding.FragmentEventDetailAnnouncementBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.event.AddEventAnnouncementFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.helper.ChatImageOverlayView
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.helper.setToolBarTitle
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.others.Constants
import com.stfalcon.frescoimageviewer.ImageViewer
import java.util.*

@SuppressLint("ValidFragment")
class EventDetailAnnouncementFragment constructor(var eventId: String, var isMyEvent: Boolean) : BaseFragment(), IAnnouncementClickListener {

    lateinit var binding: FragmentEventDetailAnnouncementBinding
    lateinit var eventDetailAnnouncementViewModel: EventAnnouncementViewModel
    private var adapterAnnouncements: AnnouncementsAdapter? = null
    private var moreDialogFragment: MoreDialogeFragment? = null
    private var overlayView: ChatImageOverlayView? = null
    private var imageViewer: ImageViewer? = null

    private val customFormatter: ImageViewer.Formatter<String>
        get() = ImageViewer.Formatter { customImage -> customImage }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        hideDrawer(HooleyMain.activity!!)
        setToolBarTitle(HooleyMain.activity!!, "Announcements")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_event_detail_announcement, container, false)
        eventDetailAnnouncementViewModel = ViewModelProviders.of(this).get(EventAnnouncementViewModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
        if (!isMyEvent) {
            binding.rlParent.visibility = View.GONE
        }
        setUiObservers()
        eventDetailAnnouncementViewModel.getEventAnnouncement(eventId)
    }

    private fun setUiObservers() {
        eventDetailAnnouncementViewModel.failureMessage.observe(this,
                android.arch.lifecycle.Observer {
                    it?.let { text ->
                        HooleyMain.activity!!.showSuccessDialog(text)
                    }
                })
        eventDetailAnnouncementViewModel.sessionExpire.observe(this,
                android.arch.lifecycle.Observer {
                    onTokenExpireLogOut()
                })
        eventDetailAnnouncementViewModel.loading.observe(this, android.arch.lifecycle.Observer {
            if (it!!)
                binding.pbAnnouncement.visibility = View.VISIBLE
            else
                binding.pbAnnouncement.visibility = View.GONE
        })
        eventDetailAnnouncementViewModel.mlist.observe(this, android.arch.lifecycle.Observer {
            it?.let { list ->
                setAnnouncementRv(list)

//                adapterAnnouncements?.setList(list)
            }
        })
    }

    private fun setListener() {
        binding.rlAddAnnouncement.setOnClickListener {
            (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, AddEventAnnouncementFragment(eventId, null, false), "EventAnnouncementFragment")
        }
    }

    private fun setAnnouncementRv(mList: List<EventAnnouncementModel.Announcement>) {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding.rvAnnouncements.layoutManager = manager
        binding.rvAnnouncements.layoutAnimation = animation
        adapterAnnouncements = AnnouncementsAdapter(isMyEvent, mList)
        adapterAnnouncements!!.mListener = this
        binding.rvAnnouncements.adapter = adapterAnnouncements
        binding.rvAnnouncements.setEmptyView(binding.tvNoData)
    }

    private fun setMoreCallerBackListener(positionId: Int) {
        val arrayList = ArrayList<String>()
        arrayList.add("Edit")
        arrayList.add("Delete")
        moreDialogFragment = MoreDialogeFragment.Builder(activity!!).setTitle("More").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    moreDialogFragment!!.dismiss()
                    (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, AddEventAnnouncementFragment(eventId, eventDetailAnnouncementViewModel.mlist.value!![positionId], true), "EventAnnouncementFragment")
                }
                1 -> {
                    moreDialogFragment!!.dismiss()
                    eventDetailAnnouncementViewModel.deleteEventAnnouncement(positionId)
                }
            }
        }.create()


        if (!moreDialogFragment!!.isAdded)
            moreDialogFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
    }

    override fun onMoreClick(position: Int) {
        setMoreCallerBackListener(position)
    }

    override fun onImageClick(position: Int) {
        overlayView = ChatImageOverlayView(HooleyMain.activity!!)
        val imageArray = ArrayList<String>()
        imageArray.add(eventDetailAnnouncementViewModel.mlist.value!![position].imageUrl!!)
        imageViewer = ImageViewer.Builder(HooleyMain.activity, imageArray)
                .setFormatter(customFormatter)
                .setStartPosition(0)
                .setOverlayView(overlayView)
                .show()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
    }

    fun setToolbar() {
        setToolBarTitle(HooleyMain.activity!!, "Announcements")
    }

}
