package com.hooleyapp.hooley.app.data.model.event.responseModel.eventDetailNew

data class GeofenceItem(
        val address: String? = null,
        val latitude: Double? = null,
        val radius: Double? = null,
        val longitude: Double? = null
)
