package com.hooleyapp.hooley.app.data.repository.friend

import android.arch.lifecycle.MutableLiveData
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.app.data.model.Friend.BlockUserListModel
import com.hooleyapp.hooley.app.data.repository.BaseRepository
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.requestmodel.CreateThreadRequestModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException

class BlockUserRepository : BaseRepository() {

    var mList: MutableLiveData<ArrayList<BlockUserListModel.BlockUser>> = MutableLiveData()

    private var apiCall: Call<BlockUserListModel>? = null

    fun getBlockUserList() {
        loading.postValue(true)
        getBlockUserList(object : IWebServiceCallback<BlockUserListModel> {
            override fun success(result: BlockUserListModel) {
                loading.postValue(false)
                mList.postValue(null)
                mList.postValue(result.blockedUsersList)
            }

            override fun failure(message: String) {
                loading.postValue(null)
                loading.postValue(false)
                if (message.isNotBlank())
                    failureMessage.postValue(message)
                mList.postValue(null)
                mList.postValue(arrayListOf())
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })
    }


    fun unBlockUser(friendId: String) {
        loading.postValue(true)
        unBlockUser(friendId, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                loading.postValue(false)
                reloadApi.postValue(true)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })

    }

    //Retorfit call
    private fun getBlockUserList(callback: IWebServiceCallback<BlockUserListModel>) {
        if (apiCall != null)
            apiCall?.cancel()
        apiCall = HooleyApp.apiService.getBlockUserList(HooleyApp.db.getString(Constants.USER_ID))
        apiCall?.enqueue(object : Callback<BlockUserListModel> {
            override fun onResponse(call: Call<BlockUserListModel>, response: Response<BlockUserListModel>) {
                if (response.body() == null) {
                    if (response.errorBody() != null)
                        callback.failure(response.errorBody().toString())
                    else
                        callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<BlockUserListModel>, t: Throwable) {
                if (call.isCanceled) {
                    return
                }
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    //Retorfit call
    private fun unBlockUser(friendId: String, callback: IWebServiceCallback<GeneralModel>) {
        var call = HooleyApp.apiService.unBlockUser(CreateThreadRequestModel(HooleyApp.db.getString(Constants.USER_ID), friendId))
        call?.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    if (response.errorBody() != null)
                        callback.failure(response.errorBody().toString())
                    else
                        callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (call.isCanceled) {
                    return
                }
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

}