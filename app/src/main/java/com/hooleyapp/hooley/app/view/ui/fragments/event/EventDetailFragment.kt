package com.hooleyapp.hooley.app.view.ui.fragments.event

import android.annotation.SuppressLint
import android.app.Dialog
import android.arch.lifecycle.ViewModelProviders
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context.CLIPBOARD_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.text.Html
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.*
import android.view.View.GONE
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import com.bumptech.glide.Glide
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.Indicators.PagerIndicator
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.DefaultSliderView
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterSpinner
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventDetailModel
import com.hooleyapp.hooley.app.viewmodel.event.EventDetailViewModel
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.event.*
import com.hooleyapp.hooley.fragments.invites.InviteFragment
import com.hooleyapp.hooley.fragments.messaging.ChatWindowFragment
import com.hooleyapp.hooley.fragments.nearby.GeofenceFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.fragments.ticket.BuyTicketFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.model.PersonalProfileModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.others.GetBitmapTask
import com.lyft.deeplink.DeepLinkParams
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


/**
 * @author Adil Malik
 * Updated by Adil on 2-April-19.
 */

@SuppressLint("ValidFragment")
class EventDetailFragment constructor(var eventId: String, var objectRespons: EventDetailModel) : BaseFragment(), View.OnClickListener {

    internal val handler = Handler()
    internal var timer: Timer? = null
    lateinit var timerTask: TimerTask
    private lateinit var binding: com.hooleyapp.hooley.databinding.FragmentEventDetailBinding
    lateinit var eventDetailViewModel: EventDetailViewModel
    private var moreMyEventDialogFragment = MoreDialogeFragment()
    private var moreDialogFragment: MoreDialogeFragment? = null
    private var moreCarDialogFragment: MoreDialogeFragment? = null
    private var moreEventEditDialogFragment = MoreDialogeFragment()
    private var showReportEventDialog: Dialog? = null
    private var spinnerAdapter: AdapterSpinner? = null
    private var reasonItemPosition = 0
    private var fullDetail: String? = null
    private var eventPayment: String? = null
    private var myClipboard: ClipboardManager? = null
    private var myClip: ClipData? = null
    lateinit var countDownTimer: CountDownTimer

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser && isResumed) {
            if (::eventDetailViewModel.isInitialized)
                eventDetailViewModel.getEventDetail(eventId)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_event_detail, container, false)
        HooleyMain.activity!!.YesNoDialog("", "", "dismiss", object : onClickDialog {
            override fun onClickYes() {

            }
        })

        eventDetailViewModel = ViewModelProviders.of(this).get(EventDetailViewModel::
        class.java)
        setUiObserver()
        eventDetailViewModel.response.postValue(objectRespons)
        eventDetailViewModel.isSectionUpdate.postValue(false)
        if (userVisibleHint) {
            binding.btnEditEvent.isEnabled = false
            eventDetailViewModel.getEventDetail(eventId)
        }
        retainInstance = true
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        myClipboard = HooleyMain.activity!!.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager?
    }

    // setting observer
    private fun setUiObserver() {
        eventDetailViewModel.failureMessage.observe(this,
                android.arch.lifecycle.Observer {
                    it?.let { text ->
                        HooleyMain.activity!!.showSuccessDialog(text)
                    }
                })
        eventDetailViewModel.sessionExpire.observe(this,
                android.arch.lifecycle.Observer {
                    onTokenExpireLogOut()
                })
        eventDetailViewModel.loading.observe(this, android.arch.lifecycle.Observer {
            if (it!!)
                binding.pbEventDetail.visibility = View.VISIBLE
            else
                binding.pbEventDetail.visibility = View.GONE
        })
        eventDetailViewModel.isSectionUpdate.observe(this, android.arch.lifecycle.Observer {
            if (it!!) {
                binding.btnEditEvent.isEnabled = true
                updateSectionUi()
            } else {
                binding.btnEditEvent.isEnabled = true
                updateSectionUi()
                setData()
            }
        })
        eventDetailViewModel.reportEventList.observe(this, android.arch.lifecycle.Observer {
            it.let { list ->
                showReportEventDialog(ArrayList(list!!))
            }
        })
        eventDetailViewModel.actionType.observe(this, android.arch.lifecycle.Observer {
            it.let { type ->
                when (type) {
                    Constants.TYPE_LYFT -> {
                        openLyftIntent()
                    }
                    Constants.TYPE_UBER -> {
                        openUberIntent()
                    }
                    Constants.TYPE_MAP -> {
                        openMapIntent()
                    }
                }
            }
        })
    }

    // Setting Listener
    private fun setListener() {
        binding.llFollowing.setOnClickListener(this)
        binding.llFollowEvent.setOnClickListener(this)
        binding.llAttendEvent.setOnClickListener(this)
        binding.ivMoreOption.setOnClickListener(this)
        binding.llShareEvent.setOnClickListener(this)
        binding.btnEditEvent.setOnClickListener(this)
        binding.btnBookUber.setOnClickListener(this)
        binding.ivMarker.setOnClickListener(this)
        binding.ivLikeEvent.setOnClickListener(this)
        binding.ivBookMarkEvent.setOnClickListener(this)
        binding.ivUnBookMarkEvent.setOnClickListener(this)
        binding.llLikeEvent.setOnClickListener(this)
        binding.ivHost.setOnClickListener(this)
        binding.btnBuyTickets.setOnClickListener(this)
        binding.llInvited.setOnClickListener(this)
        binding.llAttending.setOnClickListener(this)
        binding.llAnnouncements.setOnClickListener(this)
        binding.tvCohost1.setOnClickListener(this)
        binding.tvCohost2.setOnClickListener(this)
        binding.tvCohost3.setOnClickListener(this)
    }

    @SuppressLint("SetTextI18n")
    private fun updateSectionUi() {
        binding.tvInterested.text = "${eventDetailViewModel.response.value!!.interestedCount} Interested"
        binding.tvInvited.text = "${eventDetailViewModel.response.value!!.invitedCount} Invited"
        binding.tvAttending.text = "${eventDetailViewModel.response.value!!.attendCount} Attending"
        binding.tvFollowing.text = "${eventDetailViewModel.response.value!!.followersCount} Following"
        binding.tvAnnouncements.text = "Announcements ( ${eventDetailViewModel.response.value!!.announcementCount} )"
        LiveEventFragment.objectResult.isAttended = eventDetailViewModel.response.value!!.isAttended
        LiveEventFragment.objectResult.isFollow = eventDetailViewModel.response.value!!.isFollow
        if (eventDetailViewModel.response.value!!.eventHashs.size > 0) {
            binding.llHash.visibility = View.VISIBLE
            for (i in eventDetailViewModel.response.value!!.eventHashs.indices) {
                eventDetailViewModel.response.value!!.eventHashs[i] = "#" + eventDetailViewModel.response.value!!.eventHashs[i]
            }
            binding.tvHash.text = eventDetailViewModel.response.value!!.eventHashs.joinToString().replace(",", "")
        } else {
            binding.llHash.visibility = GONE
        }
        if (eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.isLiked) {
            binding.ivLikeEvent.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_red_heart)
            binding.llLikeEvent.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.event_tabs_selection))
        } else {
            binding.ivLikeEvent.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_purple_heart)
            binding.llLikeEvent.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.like_layer_color))
        }
        if (eventDetailViewModel.response.value!!.isFollow) {
            binding.llFollowEvent.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.event_tabs_selection))
            binding.ivFollowEvent.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_filled_star)
        } else {
            binding.llFollowEvent.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.like_layer_color))
            binding.ivFollowEvent.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_follow_it)
        }
        if (eventDetailViewModel.response.value!!.isAttended) {
            binding.llAttendEvent.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.event_tabs_selection))
            binding.ivAttendEvent.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_attended)
        } else {
            binding.llAttendEvent.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.like_layer_color))
            binding.ivAttendEvent.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_attend)
        }
        if (eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.isBookmarked) {
            binding.ivBookMarkEvent.visibility = View.GONE
            binding.ivUnBookMarkEvent.visibility = View.VISIBLE
        } else {
            binding.ivBookMarkEvent.visibility = View.VISIBLE
            binding.ivUnBookMarkEvent.visibility = View.GONE
        }

        binding.tvEventAddress.text = eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.address
    }

    fun makeTextViewResizable(tv: TextView, maxLine: Int, expandText: String, viewMore: Boolean) {

        if (tv.tag == null) {
            tv.tag = tv.text
        }
        var vto: ViewTreeObserver = tv.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                var text: String
                var lineEndIndex: Int
                var obs: ViewTreeObserver = tv.viewTreeObserver
                obs.removeGlobalOnLayoutListener(this)
                if (maxLine == 0) {
                    lineEndIndex = tv.layout.getLineEnd(0)
                    text = tv.text.subSequence(0, lineEndIndex - expandText.length + 1).toString() + " " + expandText
                } else if (maxLine > 0 && tv.lineCount >= maxLine) {
                    lineEndIndex = tv.layout.getLineEnd(maxLine - 1)
                    text = tv.text.subSequence(0, lineEndIndex - expandText.length + 1).toString() + " " + expandText
                } else {
                    lineEndIndex = tv.layout.getLineEnd(tv.layout.lineCount - 1)
                    text = tv.text.subSequence(0, lineEndIndex).toString() + " " + expandText
                }
                tv.text = text
                tv.movementMethod = LinkMovementMethod.getInstance()
                tv.setText(
                        addClickablePartTextViewResizable(Html.fromHtml(tv.text.toString()), tv, lineEndIndex, expandText,
                                viewMore), TextView.BufferType.SPANNABLE)
            }

        })

    }

    private fun addClickablePartTextViewResizable(strSpanned: Spanned, tv: TextView, maxLine: Int, spanableText: String, viewMore: Boolean): SpannableStringBuilder {
        var str: String = strSpanned.toString()
        var ssb: SpannableStringBuilder = SpannableStringBuilder(strSpanned)
        if (str.contains(spanableText)) {
            ssb.setSpan(object : ClickableSpan() {
                override fun onClick(widget: View) {
                    tv.layoutParams = tv.layoutParams
                    tv.setText(tv.tag.toString(), TextView.BufferType.SPANNABLE)
                    tv.invalidate()
                    if (viewMore) {
                        makeTextViewResizable(tv, -1, "View Less", false)
                    } else {
                        makeTextViewResizable(tv, 3, "View More", true)
                    }
                }

            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length, 0)
        }
        return ssb

    }


    @SuppressLint("SetTextI18n")
    private fun setData() {
        binding.tvEventName.text = eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventName
        if (eventDetailViewModel.response.value!!.eventHostList!!.size > 0) {
            binding.tvHostName.text = eventDetailViewModel.response.value!!.eventHostList!![0].fullName
        } else {
            binding.tvHostName.text = "NONAME"
        }

        binding.tvAgeGroup.text = "Minimum Age ${eventDetailViewModel.response.value!!.minimumAge}"
        if (eventDetailViewModel.response.value!!.eventCoversList.size > 0)
            Glide.with(HooleyMain.activity!!).load(eventDetailViewModel.response.value!!.eventCoversList[0]).into(binding.ivEventCover)
        if (!TextUtils.isEmpty(eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.profilePic))
            Glide.with(HooleyMain.activity!!).load(eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.profilePic).into(binding.ivHost)
        else
            Glide.with(HooleyMain.activity!!).load(R.drawable.ic_avatar_place_holder).into(binding.ivHost)
        if (eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventDetails!!.isNotEmpty()) {
            binding.tvEventDetail.text = eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventDetails
            if (binding.tvEventDetail.layout != null)
                if (binding.tvEventDetail.layout.lineCount > 3)
                    makeTextViewResizable(binding.tvEventDetail, 3, "Read more...", true)
        }

        fullDetail = if (eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.isPublic) {
            "Public Event"
        } else {
            "Private Event"
        }
        binding.tveventType.text = fullDetail
        eventPayment = if (eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventType == "1") {
            "Free"
        } else if (eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventType == "2") {
            if (eventDetailViewModel.response.value!!.ppvPrice > 0.0) {
                "Paid - PPV"
            } else {
                "Paid"
            }
        } else {
            "Paid - Charity"
        }
        if (eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventType != "1") {
            binding.tveventPaid.isEnabled = true
        } else {
            binding.tveventPaid.isEnabled = false
            binding.tveventPaid.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.text_color))
        }
        binding.tveventPaid.text = eventPayment
        binding.tveventPaid.setOnClickListener {
            if (!eventDetailViewModel.response.value!!.isMyEvent)
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, BuyTicketFragment.newInstance(eventId), "BuyTicketFragment")
        }
        if (eventDetailViewModel.response.value!!.isMyEvent) {
            binding.ivMoreOption.visibility = View.VISIBLE
        }
        if (eventDetailViewModel.response.value!!.isLive) {
//            startTimer()
            startReverseTime()
            binding.rlLive.visibility = View.VISIBLE
        } else {
            binding.rlLive.visibility = View.GONE
        }

        if (!eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.isPastEvent) {
            if (eventDetailViewModel.response.value!!.isMyEvent) {
                binding.btnBookUber.visibility = View.GONE
                binding.btnEditEvent.visibility = View.VISIBLE
            } else {
                binding.btnBookUber.visibility = View.VISIBLE
                binding.btnEditEvent.visibility = View.GONE
            }
            if (eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventType != "1" && !eventDetailViewModel.response.value!!.isMyEvent) {
                if (eventDetailViewModel.response.value!!.isTicketSetup)
                    binding.btnBuyTickets.visibility = View.VISIBLE
                else
                    binding.btnBuyTickets.visibility = View.GONE
            }
        } else {
            if (eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.isPastEvent) {
                binding.btnBuyTickets.visibility = GONE
                binding.btnEditEvent.visibility = View.GONE
                binding.btnBookUber.visibility = View.GONE
                binding.tveventPaid.isEnabled = false
            }

            if (eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.isPublished) {
                if (eventDetailViewModel.response.value!!.isMyEvent) {
                    binding.btnBookUber.visibility = View.GONE
                } else {
                    binding.btnBookUber.visibility = View.VISIBLE
                }
            } else {
                binding.btnBookUber.visibility = View.GONE
                if (eventDetailViewModel.response.value!!.isMyEvent) {
                    binding.btnEditEvent.visibility = View.VISIBLE
                } else {
                    binding.btnBookUber.visibility = View.VISIBLE
                }
            }
        }

        if (eventDetailViewModel.response.value!!.eventCoversList.size > 0)
            initSlider()

        if (!eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.isPublished) {
            binding.ivMoreOption.visibility = View.GONE
            binding.ivBookMarkEvent.visibility = GONE
            binding.ivUnBookMarkEvent.visibility = GONE
        }

        setEventCategory()
        setTimeStamp()
        setCoHostRv()
        setListener()
        setOptCat()
    }

    private fun setOptCat() {
        val builder = StringBuilder()
        if (eventDetailViewModel.response.value!!.eventOptionalCategoryList.size > 0) {

            if (eventDetailViewModel.response.value!!.eventOptionalCategoryList.size == 1) {
                builder.append(eventDetailViewModel.response.value!!.eventOptionalCategoryList[0])
            } else {
                for (i in eventDetailViewModel.response.value!!.eventOptionalCategoryList.indices) {
                    if (i == eventDetailViewModel.response.value!!.eventCategoryList!!.size - 1) {
                        builder.append(eventDetailViewModel.response.value!!.eventOptionalCategoryList[i])
                    } else {
                        builder.append(eventDetailViewModel.response.value!!.eventOptionalCategoryList[i] + ",")
                    }
                }
            }
            binding.tvOptCat.text = builder.toString()
            binding.llOptCat.visibility = View.VISIBLE
        } else {
            binding.llOptCat.visibility = View.GONE
        }

    }

    private fun initSlider() {
        binding.EvenCoverSlider.stopAutoCycle()
        val urlMaps = HashMap<String, String>()
        for (loop in eventDetailViewModel.response.value!!.eventCoversList.indices) {
            urlMaps["" + loop] = eventDetailViewModel.response.value!!.eventCoversList[loop].photoUrl!!
        }
        for (name in urlMaps.keys) {
            val textSliderView = DefaultSliderView(HooleyMain.activity)
            textSliderView
                    .description("")
                    .image(urlMaps[name]).scaleType = BaseSliderView.ScaleType.CenterCrop
            textSliderView.bundle(Bundle())
            textSliderView.bundle.putString("extra", name)
            binding.EvenCoverSlider.addSlider(textSliderView)
        }

        val handler = Handler()
        handler.postDelayed({
            binding.EvenCoverSlider.startAutoCycle()
            binding.EvenCoverSlider.indicatorVisibility = PagerIndicator.IndicatorVisibility.Invisible
            binding.EvenCoverSlider.setPresetTransformer(SliderLayout.Transformer.Fade)
            binding.EvenCoverSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom)
            binding.EvenCoverSlider.setCustomAnimation(DescriptionAnimation())
            binding.EvenCoverSlider.setDuration(3000)
        }, 1500)

    }

    private fun startTimer() {
        timer = Timer()
        initializeTimerTask()
        timer!!.schedule(timerTask, 100, 1000) //
    }

    private fun stopTimerTask() {
        if (timer != null) {
            timer!!.cancel()
            timer = null
        }
    }

    private fun startReverseTime() {
        if (::countDownTimer.isInitialized)
            countDownTimer.cancel()
        val testMilemillis = DateUtils.GetTimeForLiveEvent(DateUtils.getLocalDate(eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.endTime!!))
        countDownTimer = object : CountDownTimer((testMilemillis), 100) {
            override fun onTick(leftTimeInMilliseconds: Long) {
                var millis = leftTimeInMilliseconds
                var hms = if (TimeUnit.MILLISECONDS.toDays(millis) > 0) {
                    String.format("%2d", TimeUnit.MILLISECONDS.toDays(millis)) + "d - " +
                            String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)))
                } else {
                    String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)))
                }
                binding.tvLiveTimer.text = hms
            }

            override fun onFinish() {
                binding.tvLiveTimer.text = "00:00"
            }
        }
        countDownTimer.start()
    }

    private fun initializeTimerTask() {
        timerTask = object : TimerTask() {
            override fun run() {
                //use a handler to run a toast that shows the current timestamp
                handler.post {
                    val millis = DateUtils.GetTimeForLiveEvent(DateUtils.getLocalDate(eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.endTime!!))
                    val hms: String
                    hms = if (TimeUnit.MILLISECONDS.toDays(millis) > 0) {
                        String.format("%2d", TimeUnit.MILLISECONDS.toDays(millis)) + "d - " +
                                String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)))
                    } else {
                        String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)))

                    }
                    binding.tvLiveTimer.text = hms
                }
            }
        }
    }

    private fun setCoHostRv() {
        if (eventDetailViewModel.response.value!!.eventCoHostList != null && eventDetailViewModel.response.value!!.eventCoHostList!!.size > 0) {
            binding.llCoHosts.visibility = View.VISIBLE

            for (i in eventDetailViewModel.response.value!!.eventCoHostList!!.indices) {
                when (i) {
                    0 -> binding.tvCohost1.text = eventDetailViewModel.response.value!!.eventCoHostList!![i].fullName
                    1 -> {
                        binding.tvCohost2.visibility = View.VISIBLE
                        binding.tvCohost2.text = ", " + eventDetailViewModel.response.value!!.eventCoHostList!![i].fullName
                    }
                    2 -> {
                        binding.tvCohost3.visibility = View.VISIBLE
                        binding.tvCohost3.text = ", " + eventDetailViewModel.response.value!!.eventCoHostList!![i].fullName
                    }
                }
            }
        } else {
            binding.llCoHosts.visibility = View.GONE
        }
    }

    private fun setTimeStamp() {
        binding.tvEventDate.text = DateUtils.convertDateNew(DateUtils.getLocalDate(eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.startTime!!), DateUtils.getLocalDate(eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.endTime!!))
    }

    private fun setEventCategory() {
        var category: String? = null
        for (i in eventDetailViewModel.response.value!!.eventCategoryList!!.indices) {
            category = if (i == 0) {
                eventDetailViewModel.response.value!!.eventCategoryList!![i].categoryName
            } else {
                category + ", " + eventDetailViewModel.response.value!!.eventCategoryList!![i].categoryName
            }
        }
        binding.tvCategory.text = category
    }

    private fun showReportEventDialog(list: ArrayList<PersonalProfileModel.myObject>) {
        showReportEventDialog = Dialog(HooleyMain.activity!!)
        showReportEventDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        showReportEventDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        showReportEventDialog?.setContentView(R.layout.dialog_report_event)
        val btnReport = showReportEventDialog!!.findViewById<TextView>(R.id.btnReport)
        val edtMessage = showReportEventDialog!!.findViewById<EditText>(R.id.edtMessage)
        val spSelectReason = showReportEventDialog!!.findViewById<Spinner>(R.id.spSelectReason)
        spinnerAdapter = AdapterSpinner(activity, R.layout.sp_gender_group_item, list)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spSelectReason.adapter = spinnerAdapter
        spSelectReason.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                reasonItemPosition = position
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
        btnReport.setOnClickListener {
            if (edtMessage.text.toString().isNotEmpty()) {
                HooleyMain.activity!!.hideKeyboard()
                showReportEventDialog!!.dismiss()
                eventDetailViewModel.submitReport(eventId, list[reasonItemPosition].id!!, edtMessage.text.toString())
            } else {
                edtMessage.error = "Please enter the reason to report"
            }
        }
        showReportEventDialog!!.show()
    }

    private fun setMoreMyEventCallBackListener() {
        val arrayList = ArrayList<String>()
        arrayList.add("Copy Event Link")
        arrayList.add("Invite Guests")
        if (!eventDetailViewModel.response.value!!.isPromoted) {
            arrayList.add("Promote Event")
        } else {
            arrayList.add("View Stats")
        }
        if (eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventType != "1") {
            arrayList.add("Scan Stats / Scan Tickets")
            arrayList.add("View Sales / Edit Tickets")
        }

        moreMyEventDialogFragment = MoreDialogeFragment.Builder(HooleyMain.activity!!).setTitle("More").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, _ ->
            when (position) {
                0 -> {
                    var text: String = Constants.EVENT_SHARE + eventId
                    myClip = ClipData.newPlainText("text", text)
                    myClipboard?.primaryClip = myClip
                    HooleyMain.activity!!.showToastMessage("Text Copied")

                    if (moreMyEventDialogFragment != null)
                        moreMyEventDialogFragment.dismiss()
                }
                1 -> {
                    (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, InviteFragment.newInstance(eventId, eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventName!!), "InviteFragment")
                    if (moreMyEventDialogFragment != null)
                        moreMyEventDialogFragment.dismiss()
                }
                2 -> {
                    if (!eventDetailViewModel.response.value!!.isPromoted)
                        (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, PromoteEventFragment.newInstance(eventId, DateUtils.getLocalDate(eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.startTime!!), DateUtils.getLocalDate(eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.endTime!!)), "PromoteEventFragment")
                    else
                        (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, PromotionStatsFragment.newInstance(eventId), "PromotionStatsFragment")
                    if (moreMyEventDialogFragment != null)
                        moreMyEventDialogFragment.dismiss()
                }
                3 -> {
                    (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, ViewTickerSalesFragment.newInstance(eventId, eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventName!!, eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.address!!, DateUtils.displayDateOnly(DateUtils.getLocalDate(eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.startTime!!))), "ViewTickerSalesFragment")
                    if (moreMyEventDialogFragment != null)
                        moreMyEventDialogFragment.dismiss()
                }
                4 -> {
                    (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, ViewSalesFragment.newInstance(eventId), "ViewSalesFragment")
                    if (moreMyEventDialogFragment != null)
                        moreMyEventDialogFragment.dismiss()
                }

            }
        }.create()
        moreMyEventDialogFragment.show(HooleyMain.activity!!.supportFragmentManager)
    }

    private fun setMoreCallerBackListener() {
        val arrayList = ArrayList<String>()
        if (eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventType != "1" && eventDetailViewModel.response.value!!.isTicketSetup && !eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.isPastEvent) {
            arrayList.add("Buy Tickets")
        }
        arrayList.add("Copy Event Link")
        if (!eventDetailViewModel.response.value!!.isMyEvent) {
            arrayList.add("Message Host")
            arrayList.add("Report Event")
        }

        moreDialogFragment = MoreDialogeFragment.Builder(HooleyMain.activity!!).setTitle("More").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, _ ->
            when (position) {
                0 -> {
                    if (eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventType != "1" && eventDetailViewModel.response.value!!.isTicketSetup && !eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.isPastEvent) {
                        if (eventDetailViewModel.response.value!!.isMyEvent) {
//                            (HooleyMain.activity as ActivityBase).callFragment(R.id.container, UpdateEventFragment.newInstance(eventId), "UpdateEventFragment")
                            var text: String = Constants.EVENT_SHARE + eventId
                            myClip = ClipData.newPlainText("text", text)
                            myClipboard?.primaryClip = myClip

                            HooleyMain.activity!!.showToastMessage("Text Copied")
                        } else {
                            if (eventDetailViewModel.response.value != null)
                                if (eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventType != "1")
                                    (HooleyMain.activity as ActivityBase).addFragment(R.id.container, BuyTicketFragment.newInstance(eventId), "BuyTicketFragment")
                        }
                    } else {
                        var text: String = Constants.EVENT_SHARE + eventId
                        myClip = ClipData.newPlainText("text", text)
                        myClipboard?.primaryClip = myClip

                        HooleyMain.activity!!.showToastMessage("Text Copied")
                    }
                    if (moreDialogFragment != null)
                        moreDialogFragment!!.dismiss()
                }
                1 -> {
                    if (eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventType != "1" && eventDetailViewModel.response.value!!.isTicketSetup && !eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.isPastEvent) {
                        var text: String = Constants.EVENT_SHARE + eventId
                        myClip = ClipData.newPlainText("text", text)
                        myClipboard?.primaryClip = myClip

                        HooleyMain.activity!!.showToastMessage("Text Copied")
                    } else {
                        if (eventDetailViewModel.response.value!!.eventHostList != null && eventDetailViewModel.response.value!!.eventHostList!!.size > 0)
                            if (!TextUtils.isEmpty(eventDetailViewModel.response.value!!.eventHostList!![0].userId)) {
                                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, ChatWindowFragment("", eventDetailViewModel.response.value!!.eventHostList!![0].userId!!, eventDetailViewModel.response.value!!.eventHostList!![0].fullName!!), "ChatWindowFragment")
                            }
                    }
                    if (moreDialogFragment != null)
                        moreDialogFragment!!.dismiss()
                }
                2 -> {
                    if (eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventType != "1" && eventDetailViewModel.response.value!!.isTicketSetup) {
                        if (eventDetailViewModel.response.value!!.eventHostList != null && eventDetailViewModel.response.value!!.eventHostList!!.size > 0)
                            if (!TextUtils.isEmpty(eventDetailViewModel.response.value!!.eventHostList!![0].userId)) {
                                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, ChatWindowFragment("", eventDetailViewModel.response.value!!.eventHostList!![0].userId!!, eventDetailViewModel.response.value!!.eventHostList!![0].fullName!!), "ChatWindowFragment")
                            }
                    } else {
                        eventDetailViewModel.getEventReport()
                    }
                    if (moreDialogFragment != null)
                        moreDialogFragment!!.dismiss()
                }
                3 -> {
                    eventDetailViewModel.getEventReport()
                    if (moreDialogFragment != null)
                        moreDialogFragment!!.dismiss()
                }
            }
        }.create()
        moreDialogFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
    }

    private fun showShareEventDialog() {
        object : GetBitmapTask() {
            override fun onPostExecute(bmp: Bitmap) {
                super.onPostExecute(bmp)
                //                HooleyMain.removeDialog();
                (HooleyMain.activity as HooleyMain).shareWithImage(bmp, eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventName!!, Constants.EVENT_SHARE + eventId, DateUtils.formatDateNew(eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.startTime!!))
            }
        }.execute(eventDetailViewModel.response.value!!.eventCoversList[0].photoUrl)
    }

    private fun createDeepLinkString(deepLinkParams: DeepLinkParams): String {
        val sb = StringBuilder()
        sb.append("lyft://ridetype?id=")
        sb.append("lyft")

        if (deepLinkParams.isPickupLatLngSet) {
            sb.append("&pickup[latitude]=")
            sb.append(HooleyApp.db.getString(Constants.CURRENT_LAT))
            sb.append("&pickup[longitude]=")
            sb.append(HooleyApp.db.getString(Constants.CURRENT_LANG))
        }

        if (deepLinkParams.isPickupAddressSet) {
            sb.append("&pickup[address]=")
            sb.append(deepLinkParams.pickupAddr)
        }

        if (deepLinkParams.isDropoffLatLngSet) {
            sb.append("&destination[latitude]=")
            sb.append(eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventLat.toString())
            sb.append("&destination[longitude]=")
            sb.append(eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventLong.toString())
        }

        if (deepLinkParams.isDropoffAddressSet) {
            sb.append("&destination[address]=")
            sb.append(deepLinkParams.dropoffAddr)
        }

        if (deepLinkParams.clientId != null) {
            sb.append("&partner=")
            sb.append(deepLinkParams.clientId)
        }

        if (deepLinkParams.promoCode != null) {
            sb.append("&credits=")
            sb.append(deepLinkParams.promoCode)
        }

        return sb.toString()
    }

    private fun setCarBookingCallerBackListener() {
        val arrayList = ArrayList<String>()
        arrayList.add(Constants.TYPE_LYFT)
        arrayList.add(Constants.TYPE_UBER)
        moreCarDialogFragment = MoreDialogeFragment.Builder(HooleyMain.activity!!).setTitle("Book a Driver").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, _ ->
            when (position) {
                0 -> {
                    moreCarDialogFragment!!.dismiss()
//                    eventDetailViewModel.addMarkOnWay(eventId, Constants.TYPE_LYFT)
                    openLyftIntent()
                }
                1 -> {
                    moreCarDialogFragment!!.dismiss()
//                    eventDetailViewModel.addMarkOnWay(eventId, Constants.TYPE_UBER)
                    openUberIntent()
                }
            }
        }.create()

        moreCarDialogFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
    }

    private fun setEditEventCallerBackListener() {
        val arrayList = ArrayList<String>()
        arrayList.add("Edit Basic Info")
        arrayList.add("Edit Date & Time")
        arrayList.add("Edit Location & Geofence")
        arrayList.add("Cancel Event")
        moreEventEditDialogFragment = MoreDialogeFragment.Builder(HooleyMain.activity!!).setTitle("Edit Event").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, _ ->
            when (position) {
                0 -> {
                    (HooleyMain.activity as ActivityBase).callFragment(R.id.container, UpdateEventFragment.newInstance(eventId), "updateEventFragment")
                    if (moreEventEditDialogFragment != null)
                        moreEventEditDialogFragment.dismiss()
                }
                1 -> {
                    var canUpdate = eventDetailViewModel.response.value!!.timeUpdatedCount!! <= 0
                    (HooleyMain.activity as ActivityBase).addFragment(R.id.container, UpdateEventDataTimeFragment.newInstance(eventId, eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.startTime!!, eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.endTime!!, true /* pely ye that yahan "canUpdate"*/, eventDetailViewModel.response.value!!.timeUpdateReason), "updateEventFragment")
                    if (moreEventEditDialogFragment != null)
                        moreEventEditDialogFragment.dismiss()
                }
                2 -> {
                    (HooleyMain.activity as ActivityBase).callFragment(R.id.container, GeofenceFragment.newInstance(eventDetailViewModel.response.value!!.eventGeofenceList, true, eventId), "GeofenceFragment")
                    if (moreEventEditDialogFragment != null)
                        moreEventEditDialogFragment.dismiss()
                }
                3 -> {
                    var isPaid = when (eventDetailViewModel.response.value!!.eventDisplayBasicInfo?.eventType) {
                        "2", "3" ->
                            true
                        else ->
                            false
                    }
                    (HooleyMain.activity as ActivityBase).addFragment(R.id.container, CancelEventFragment.newInstance(eventId, true, isPaid, Constants.TYPE_CANCEL_EVENT), "CancelEventFragment")
                    if (moreEventEditDialogFragment != null)
                        moreEventEditDialogFragment.dismiss()
                }
            }
        }.create()

        moreEventEditDialogFragment.show(HooleyMain.activity!!.supportFragmentManager)

    }

    private fun openLyftIntent() {
        val pm = HooleyMain.activity!!.packageManager
        try {
            //https://www.lyft.com/developers/apps/
            //Login --> +923214148671
            pm.getPackageInfo("me.lyft.android", PackageManager.GET_ACTIVITIES)
            val deepLinkParams = DeepLinkParams.Builder()
                    .setClientId("IYyHssX_-xd8")
                    .setRideType("lyft")
                    .setPickupLocation(java.lang.Double.parseDouble(HooleyApp.db.getString(Constants.CURRENT_LAT)), java.lang.Double.parseDouble(HooleyApp.db.getString(Constants.CURRENT_LANG)))
                    .setDropoffLocation(eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventLat, eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventLong)
                    .build()

            val deepLinkString = createDeepLinkString(deepLinkParams)
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(deepLinkString)
            startActivity(intent)
        } catch (e: PackageManager.NameNotFoundException) {
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=me.lyft.android")))
            } catch (anfe: android.content.ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=me.lyft.android")))
            }

        }
    }

    private fun openUberIntent() {
        val pm = HooleyMain.activity!!.packageManager
        try {
            pm.getPackageInfo("com.ubercab", PackageManager.GET_ACTIVITIES)
            val uri = "uber://?client_id=yY5Z0sVlsH7kM8yugdvRxUHhkGuco9eF&action=setPickup&pickup=my_location&dropoff[latitude]=" + eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventLat + "&dropoff[longitude]=" + eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventLong
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(uri)
            startActivity(intent)
        } catch (e: PackageManager.NameNotFoundException) {
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.ubercab")))
            } catch (anfe: android.content.ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.ubercab")))
            }
        }
    }

    private fun openMapIntent() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventLat + "," + eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.eventLong))
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        HooleyMain.activity!!.startActivity(intent)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.llFollowEvent -> {
                eventDetailViewModel.followEvent(eventId, !eventDetailViewModel.response.value!!.isFollow)
            }
            R.id.llAttendEvent -> {
                try {
                    eventDetailViewModel.attendEvent(eventId, !eventDetailViewModel.response.value!!.isAttended)
                } catch (e: Exception) {

                }
            }
            R.id.ivMoreOption -> if (eventDetailViewModel.response.value!!.isMyEvent && eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.isPublished && !eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.isPastEvent) {
                setMoreMyEventCallBackListener()
            } else {
                setMoreCallerBackListener()
            }
            R.id.llShareEvent -> showShareEventDialog()
            R.id.btnEditEvent -> if (eventDetailViewModel.response.value!!.eventDisplayBasicInfo!!.isPublished) {
                setEditEventCallerBackListener()
            } else {
                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, UpdateEventFragment.newInstance(eventId), "updateEventFragment")
            }
            R.id.ivMarker -> {
//                eventDetailViewModel.addMarkOnWay(eventId, Constants.TYPE_MAP)
                openMapIntent()
            }
            R.id.llLikeEvent -> eventDetailViewModel.likeEvent(eventId)
            R.id.ivBookMarkEvent -> eventDetailViewModel.bookMarkEvent(eventId, false)
            R.id.ivUnBookMarkEvent -> eventDetailViewModel.bookMarkEvent(eventId, true)
            R.id.btnBuyTickets -> (HooleyMain.activity as ActivityBase).addFragment(R.id.container, BuyTicketFragment.newInstance(eventId), "BuyTicketFragment")
            R.id.llAttending -> (HooleyMain.activity as ActivityBase).addFragment(R.id.container, EventDetailGuestListFragment(eventId, Constants.TYPE_ATTENDING), "EventDetailGuestListFragment")
            R.id.llInvited -> (HooleyMain.activity as ActivityBase).addFragment(R.id.container, EventDetailGuestListFragment(eventId, Constants.TYPE_INVITED), "EventDetailGuestListFragment")
            R.id.llFollowing -> (HooleyMain.activity as ActivityBase).addFragment(R.id.container, EventDetailGuestListFragment(eventId, Constants.TYPE_FOLLOWING), "EventDetailGuestListFragment")
            R.id.llAnnouncements -> (HooleyMain.activity as ActivityBase).addFragment(R.id.container, EventDetailAnnouncementFragment(eventId, eventDetailViewModel.response.value!!.isMyEvent), "EventDetailAnnouncementFragment")
            R.id.btnBookUber -> if (eventDetailViewModel.response.value!!.isAttended) {
                setCarBookingCallerBackListener()
            } else {
                HooleyMain.activity!!.showSuccessDialog("Please mark the event attend first.")
            }
            R.id.ivHost -> if (eventDetailViewModel.response.value!!.eventHostList != null && eventDetailViewModel.response.value!!.eventHostList!!.size > 0) {
                var avatarfragment = ViewAvatarFragment(eventDetailViewModel.response.value!!.eventHostList!![0].userId!!.toInt())
                avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
            }
            R.id.tvCohost1 -> {
                var avatarfragment = ViewAvatarFragment(eventDetailViewModel.response.value!!.eventCoHostList!![0].userId!!.toInt())
                avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
            }
            R.id.tvCohost2 -> {
                var avatarfragment = ViewAvatarFragment(eventDetailViewModel.response.value!!.eventCoHostList!![1].userId!!.toInt())
                avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
            }
            R.id.tvCohost3 -> {
                var avatarfragment = ViewAvatarFragment(eventDetailViewModel.response.value!!.eventCoHostList!![2].userId!!.toInt())
                avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
            }

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
//        stopTimerTask()
        if (::countDownTimer.isInitialized)
            countDownTimer.cancel()
    }

}