package com.hooleyapp.hooley.app.data.model.event.responseModel

import com.google.gson.annotations.SerializedName
import com.hooleyapp.hooley.model.PersonalProfileModel
import java.util.*

/**
 * Created by Zeeshan on 17-Jan-18.
 */


class EventDetailModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null


    @SerializedName("isLive")
    var isLive: Boolean = false

    @SerializedName("likes")
    var likes: Int = 0

    @SerializedName("shares")
    var shares: Int = 0

    @SerializedName("followersCount")
    var followersCount: Int = 0

    @SerializedName("attendCount")
    var attendCount: Int = 0

    @SerializedName("announcementCount")
    var announcementCount: Int = 0

    @SerializedName("invitedCount")
    var invitedCount: Int = 0

    @SerializedName("interestedCount")
    var interestedCount: Int = 0

    @SerializedName("minimumAge")
    var minimumAge: String = ""

    @SerializedName("isFollow")
    var isFollow: Boolean = false

    @SerializedName("isAttended")
    var isAttended: Boolean = false

    @SerializedName("isMyEvent")
    var isMyEvent: Boolean = false

    @SerializedName("isInvite")
    var isInvite: Boolean = false

    @SerializedName("isPromoted")
    var isPromoted: Boolean = false

    @SerializedName("isTicketSetup")
    var isTicketSetup: Boolean = false

    @SerializedName("isCanceledEvent")
    var isCanceledEvent: Boolean = false

    @SerializedName("cancelReason")
    var cancelReason: String = ""

    @SerializedName("ppvPrice")
    var ppvPrice: Double = 0.0

    @SerializedName("isPpvPurchased")
    var isPpvPurchased: Boolean = false

    @SerializedName("timeUpdateReason")
    var timeUpdateReason: String = ""

    @SerializedName("timeUpdatedCount")
    var timeUpdatedCount: Int? = null

    @SerializedName("eventDisplayBasicInfo")
    var eventDisplayBasicInfo: EventDisplayBasicInfo? = null

    @SerializedName("eventDisplayHost")
    var eventHostList: ArrayList<EventHost>? = null

    @SerializedName("eventDisplayCoHost")
    var eventCoHostList: ArrayList<EventHost>? = null

    @SerializedName("eventDisplayCategories")
    var eventCategoryList: ArrayList<EventCategory>? = null

    @SerializedName("eventCovers")
    var eventCoversList = ArrayList<EventCover>()

    @SerializedName("eventGeofance")
    var eventGeofenceList = ArrayList<PersonalProfileModel.GeoFence>()

    @SerializedName("optionalCategories")
    var eventOptionalCategoryList = ArrayList<String>()

    inner class EventDisplayBasicInfo {

        @SerializedName("hostName")
        var hostName: String? = null

        @SerializedName("profilePic")
        var profilePic: String? = null

        @SerializedName("eventName")
        var eventName: String? = null

        @SerializedName("startTime")
        var startTime: String? = null

        @SerializedName("endTime")
        var endTime: String? = null

        @SerializedName("address")
        var address: String? = null

        @SerializedName("eventDetails")
        var eventDetails: String? = null

        @SerializedName("isPublic")
        var isPublic: Boolean = false

        @SerializedName("isPrivate")
        var isPrivate: Boolean = false

//        @SerializedName("isPaid")
//        var isPaid: Boolean = false

        @SerializedName("isInviteFriend")
        var isInviteFriend: Boolean = false

        @SerializedName("isLiked")
        var isLiked: Boolean = false

        @SerializedName("isBookmarked")
        var isBookmarked: Boolean = false

        @SerializedName("latitude")
        var eventLat: Double = 0.toDouble()

        @SerializedName("longitude")
        var eventLong: Double = 0.toDouble()

        @SerializedName("isPastEvent")
        var isPastEvent: Boolean = false

        @SerializedName("isPostAllow")
        var isPostAllow: Boolean = false

        @SerializedName("isPublished")
        var isPublished: Boolean = false

        @SerializedName("eventType")
        var eventType: String? = null


    }

    inner class EventHost {

        @SerializedName("userId")
        var userId: String? = null

        @SerializedName("profilePic")
        var profilePic: String? = null

        @SerializedName("fullName")
        var fullName: String? = null

    }

    inner class EventCategory {
        @SerializedName("id")
        var id: String? = null

        @SerializedName("categoryName")
        var categoryName: String? = null
    }

    class Comments {

        @SerializedName("userId")
        var userId: String? = null

        @SerializedName("fullName")
        var fullName: String? = null

        @SerializedName("profilePic")
        var profilePic: String? = null

        @SerializedName("commentText")
        var commentText: String? = null

        @SerializedName("commentTime")
        var commentTime: String? = null
    }

    inner class EventGuest {
        @SerializedName("userId")
        var userId: String? = null

        @SerializedName("profilePic")
        var profilePic: String? = null
    }

    inner class EventCover {
        @SerializedName("photo_url")
        var photoUrl: String? = null

    }

    @SerializedName("eventHashTags")
    var eventHashs = ArrayList<String>()


}
