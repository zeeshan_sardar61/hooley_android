package com.hooleyapp.hooley.app.viewmodel.event

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventAnnouncementModel
import com.hooleyapp.hooley.app.data.repository.event.EventAnnouncementRepository

class EventAnnouncementViewModel(application: Application) : AndroidViewModel(application) {

    var repository = EventAnnouncementRepository()
    var mlist: MutableLiveData<ArrayList<EventAnnouncementModel.Announcement>> = MutableLiveData()
    var failureMessage: MutableLiveData<String> = MutableLiveData()
    var sessionExpire: MutableLiveData<Boolean> = MutableLiveData()
    var loading: MutableLiveData<Boolean> = MutableLiveData()

    init {
        mlist = repository.mlist
        failureMessage = repository.failureMessage
        sessionExpire = repository.sessionExpire
        loading = repository.loading
    }

    fun getEventAnnouncement(eventId: String) {
        repository.getEventAnnouncement(eventId)
    }

    fun deleteEventAnnouncement(positionId: Int) {
        repository.deleteEventAnnouncement(positionId)
    }

}