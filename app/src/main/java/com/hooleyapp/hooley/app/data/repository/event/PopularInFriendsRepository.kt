package com.hooleyapp.hooley.app.data.repository.event

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.support.annotation.WorkerThread
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.event.responseModel.PopularInFriendsModel
import com.hooleyapp.hooley.app.persistance.dao.PopularInFriendsDao
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.others.NetworkUtil
import com.hooleyapp.hooley.services.EventWebService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class PopularInFriendsRepository(private val popularInFriendsDao: PopularInFriendsDao) {

    var allevents: MutableLiveData<List<PopularInFriendsModel.PopularInFriends>> = MutableLiveData()
    var tempFilterList: MutableLiveData<List<PopularInFriendsModel.PopularInFriends>> = MutableLiveData()
    var failureMessage: MutableLiveData<String> = MutableLiveData()
    var sessionExpire: MutableLiveData<Boolean> = MutableLiveData()
    var loading: MutableLiveData<Boolean> = MutableLiveData()
    var selectedTab = 1
    var eventWebService = EventWebService()

    private var parentJob = Job()

    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    init {
        scope.launch(Dispatchers.IO) {
            allevents.postValue(getPopularInFriendsEvent().value)
            tempFilterList.postValue(getPopularInFriendsEvent().value)
        }
    }

    @WorkerThread
    suspend fun insert(item: PopularInFriendsModel.PopularInFriends) {
        popularInFriendsDao.insert(item)
    }

    @WorkerThread
    suspend fun getPopularInFriendsEvent(): LiveData<List<PopularInFriendsModel.PopularInFriends>> {
        return popularInFriendsDao.getPopularInFriendsEvent()
    }

    @WorkerThread
    suspend fun likeUnLikeCount(item: PopularInFriendsModel.PopularInFriends) {
        popularInFriendsDao.likeUnLikeCount(item)
    }

    @WorkerThread
    suspend fun insertList(list: List<PopularInFriendsModel.PopularInFriends>) {
        popularInFriendsDao.insertAllEvents(list)
    }

    @WorkerThread
    suspend fun deleteAll() {
        popularInFriendsDao.deleteAll()
    }


    // Network Request
    fun getPopularInFriends(refreshData: Boolean) {
        if (!NetworkUtil.isInternetConnected(HooleyMain.activity!!)) {
            scope.launch(Dispatchers.IO) {
                allevents.postValue(null)
                allevents.postValue(getPopularInFriendsEvent().value)
            }
            return
        }
        if (!refreshData)
            loading.postValue(true)
        eventWebService.getPopularInFriendsEvent(object : IWebServiceCallback<PopularInFriendsModel> {
            override fun success(result: PopularInFriendsModel) {
                loading.postValue(false)

                scope.launch(Dispatchers.IO) {
                    if (refreshData)
                        deleteAll()
                    for (item in 0 until result.popularInFriendList.size) {
                        insert(result.popularInFriendList[item])
                    }
//                    insertList(result.popularInFriendList)
                }
                tempFilterList.postValue(null)
                tempFilterList.postValue(result.popularInFriendList)
                var filterList: List<PopularInFriendsModel.PopularInFriends> = if (selectedTab == 1) {
                    result.popularInFriendList.filterNot {
                        it.isPast
                    }
                } else {
                    result.popularInFriendList.filter {
                        it.isPast
                    }
                }
                allevents.postValue(null)
                allevents.postValue(filterList)
//                scope.launch(Dispatchers.IO) {
//                    allevents.postValue(null)
//                    allevents.postValue(getPopularInFriendsEvent().value)
//                }
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })
    }

    fun followEvent(listPosition: Int) {
//        loading.postValue(true)
        eventWebService.followEvent(allevents.value!![listPosition].eventId!!, !allevents.value!![listPosition].isFollowed, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                var listitem = allevents.value
                if (listitem!![listPosition].isFollowed) {
                    listitem[listPosition].isFollowed = false
                    listitem[listPosition].followersCount = listitem[listPosition].followersCount - 1
                } else {
                    listitem[listPosition].isFollowed = true
                    listitem[listPosition].followersCount = listitem[listPosition].followersCount + 1
                }
                allevents.postValue(null)
                allevents.postValue(listitem)
            }

            override fun failure(message: String) {
//                loading.postValue(false)
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
//                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })
    }

    fun likeEvent(listPosition: Int) {
//        loading.postValue(true)
        eventWebService.likeEvent(allevents.value!![listPosition].eventId!!, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
//                loading.postValue(false)
                var listitem = allevents.value
                if (listitem!![listPosition].isLiked) {
                    listitem[listPosition].isLiked = false
                    listitem[listPosition].likesCount--
                } else {
                    listitem[listPosition].isLiked = true
                    listitem[listPosition].likesCount++
                }
                allevents.postValue(null)
                allevents.postValue(listitem)

//                if (allevents.value!![listPosition].isLiked) {
//                    allevents.value!![listPosition].isLiked = false
//                    allevents.value!![listPosition].likesCount = allevents.value!![listPosition].likesCount - 1
//                } else {
//                    allevents.value!![listPosition].isLiked = true
//                    allevents.value!![listPosition].likesCount = allevents.value!![listPosition].likesCount + 1
//                }
//                scope.launch(Dispatchers.IO) {
//                    likeUnLikeCount(allevents.value!![listPosition])
//                }
            }

            override fun failure(message: String) {
//                loading.postValue(false)
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
//                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })
    }

    fun shareEvent(shareMedia: String, listPosition: Int) {
//        loading.postValue(true)
        eventWebService.shareEvent(allevents.value!![listPosition].eventId!!, shareMedia, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
//                loading.postValue(false)
                var listitems = allevents.value
                listitems!![listPosition].sharesCount++
                allevents.postValue(null)
                allevents.postValue(listitems)
            }

            override fun failure(message: String) {
//                loading.postValue(false)
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
//                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })
    }
}