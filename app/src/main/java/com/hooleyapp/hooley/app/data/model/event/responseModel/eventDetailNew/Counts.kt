package com.hooleyapp.hooley.app.data.model.event.responseModel.eventDetailNew

data class Counts(
        val followers: Int? = null,
        val attend: Int? = null,
        val timeUpdated: Int? = null,
        val invited: Int? = null,
        val interested: Int? = null,
        val announcement: Int? = null
)
