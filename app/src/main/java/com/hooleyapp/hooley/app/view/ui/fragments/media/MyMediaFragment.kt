package com.hooleyapp.hooley.app.view.ui.fragments.media

import android.app.Dialog
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.AnimationUtils
import android.widget.*
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterMediaAlbum
import com.hooleyapp.hooley.adapters.AdapterMoreMediaDialoge
import com.hooleyapp.hooley.adapters.AdapterSpinner
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.app.data.model.media.MyMediaAlbumModel
import com.hooleyapp.hooley.app.view.adapter.MediaStreamAdapter
import com.hooleyapp.hooley.app.viewmodel.media.MyMediaViewModel
import com.hooleyapp.hooley.databinding.FragmentMyMediaBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.event.UpdatePostFragment
import com.hooleyapp.hooley.fragments.media.AllFeedsSinglePostCommentFragment
import com.hooleyapp.hooley.fragments.media.ViewPostMediaFragment
import com.hooleyapp.hooley.fragments.others.MoreDialogeFragment
import com.hooleyapp.hooley.fragments.others.MoreMediaDialogFragment
import com.hooleyapp.hooley.interfaces.IEventAllFeedsClick
import com.hooleyapp.hooley.model.PersonalProfileModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.GetBitmapTask
import com.hooleyapp.hooley.others.MessageEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*

/**
 * Created by Nauman on 4/2/2018.
 */

class MyMediaFragment : BaseFragment(), View.OnClickListener, RadioGroup.OnCheckedChangeListener, IEventAllFeedsClick {

    lateinit var binding: FragmentMyMediaBinding
    lateinit var myMediaViewModel: MyMediaViewModel
    internal val mediaList = ArrayList<AdapterMoreMediaDialoge.MoreItem>()
    internal val albumList = ArrayList<AdapterMoreMediaDialoge.MoreItem>()
    private var adapterMyMedia: MediaStreamAdapter? = null
    private var moreTrendingDialogeFragment: MoreMediaDialogFragment? = null
    private var moreDialogAlbumFragment: MoreMediaDialogFragment? = null
    private var adapterMyMediaAlbum: AdapterMediaAlbum? = null
    private var morePostDialogeFragment: MoreDialogeFragment? = null
    private var showMediaReportEventDialog: Dialog? = null
    private var spinnerAdapter: AdapterSpinner? = null
    private var reasonMediaItemPosition = 0
    private var allFeedItemPosition = 0
    private var pagerPosition = 0
    private lateinit var imageId: String
    private lateinit var eventId: String
    var type = Constants.TYPE_LIST_VIEW
    var boldFont: Typeface
    var regularFont: Typeface

    init {
        boldFont = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)!!
        regularFont = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)!!
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser && isResumed) {
            if (::myMediaViewModel.isInitialized)
                myMediaViewModel.getMyMedia()
        } else {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_media, container, false)
        myMediaViewModel = ViewModelProviders.of(this).get(MyMediaViewModel::class.java)
        if (userVisibleHint)
            myMediaViewModel.getMyMedia()
        setUiObserver()
        setTab()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setMoreAlbumCallerBackListener()
        setMoreTrendingCallerBackListener()
        setListener()
    }

    private fun setUiObserver() {
//        myMediaViewModel.reloadApi.observe(this, android.arch.lifecycle.Observer { reloadFragment() })
        myMediaViewModel.sessionExpire.observe(this, android.arch.lifecycle.Observer { onTokenExpireLogOut() })
        myMediaViewModel.failureMessage.observe(this, android.arch.lifecycle.Observer { it?.let { text -> { binding.tvNoData.text = text } } })
        myMediaViewModel.loading.observe(this, android.arch.lifecycle.Observer { if (it!!) binding.pbMyMedia.visibility = View.VISIBLE else binding.pbMyMedia.visibility = View.GONE })
        myMediaViewModel.reportEventList.observe(this, android.arch.lifecycle.Observer {
            it?.let { list ->
                showReportEventDialog(ArrayList(list))
            }
        })
        myMediaViewModel.mList.observe(this, android.arch.lifecycle.Observer {
            it?.let { list ->
                setMediaStreamAdapter(list)
            }
        })
        myMediaViewModel.mAblumList.observe(this, android.arch.lifecycle.Observer {
            it?.let { list ->
                setMediaStreamAlbumAdapter(list)
            }
        })
    }

    private fun setListener() {
        binding.ivFilter.setOnClickListener(this)
        binding.rgMediaStream.setOnCheckedChangeListener(this)
    }

    private fun setMoreAlbumCallerBackListener() {
        if (albumList.size > 0)
            albumList.clear()
        val itemAtoZ = AdapterMoreMediaDialoge.MoreItem("A - Z", false)
        val itemTime = AdapterMoreMediaDialoge.MoreItem("Time", false)
        albumList.add(itemAtoZ)
        albumList.add(itemTime)
        val arrayList = ArrayList<String>()
        arrayList.add("A - Z")
        arrayList.add("Time")

        moreDialogAlbumFragment = MoreMediaDialogFragment.Builder(HooleyMain.activity!!).setTitle("Sort by").setType(Constants.TYPE_MORE_80).setList(albumList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    myMediaViewModel.sortByAlphabetical()
                    albumList[0].isCheck = (true)
                    albumList[1].isCheck = (false)
                    moreDialogAlbumFragment!!.dismiss()
                }
                1 -> {
                    myMediaViewModel.sortByCurrentTime()
                    albumList[0].isCheck = (false)
                    albumList[1].isCheck = (true)
                    moreDialogAlbumFragment!!.dismiss()
                }
            }
        }.create()

    }

    private fun setMoreTrendingCallerBackListener() {
        if (mediaList.size > 0)
            mediaList.clear()
        val itemAll = AdapterMoreMediaDialoge.MoreItem("All", true)
        val itemFavorites = AdapterMoreMediaDialoge.MoreItem("Favorites", false)
        val itemPhotos = AdapterMoreMediaDialoge.MoreItem("Photos", false)
        val itemVideos = AdapterMoreMediaDialoge.MoreItem("Videos", false)
        mediaList.add(itemAll)
        mediaList.add(itemFavorites)
        mediaList.add(itemPhotos)
        mediaList.add(itemVideos)
        moreTrendingDialogeFragment = MoreMediaDialogFragment.Builder(HooleyMain.activity!!).setTitle("Filter by").setType(Constants.TYPE_MORE_80).setList(mediaList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    myMediaViewModel.filterByAll()
                    mediaList[0].isCheck = (true)
                    mediaList[1].isCheck = (false)
                    mediaList[2].isCheck = (false)
                    mediaList[3].isCheck = (false)
                    moreTrendingDialogeFragment!!.dismiss()
                }
                1 -> {
                    mediaList[0].isCheck = (false)
                    mediaList[1].isCheck = (true)
                    mediaList[2].isCheck = (false)
                    mediaList[3].isCheck = (false)
                    myMediaViewModel.filterByFav()
                    moreTrendingDialogeFragment!!.dismiss()
                }
                2 -> {
                    mediaList[0].isCheck = (false)
                    mediaList[1].isCheck = (false)
                    mediaList[2].isCheck = (true)
                    mediaList[3].isCheck = (false)
                    myMediaViewModel.filterByPhotos()
                    moreTrendingDialogeFragment!!.dismiss()
                }
                3 -> {
                    mediaList[0].isCheck = (false)
                    mediaList[1].isCheck = (false)
                    mediaList[2].isCheck = (false)
                    mediaList[3].isCheck = (true)
                    myMediaViewModel.filterByVideo()
                    moreTrendingDialogeFragment!!.dismiss()
                }
            }
        }.create()


    }

    private fun setMediaStreamAdapter(list: ArrayList<EventGalleryFeedModel.EventMediaInfo>) {
        binding.rvMyMedia.visibility = View.VISIBLE
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_from_right)
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding.rvMyMedia.layoutAnimation = animation
        binding.rvMyMedia.setEmptyView(binding.tvNoData)
        binding.rvMyMedia.layoutManager = manager
        adapterMyMedia = MediaStreamAdapter(HooleyMain.activity!!, list)
        adapterMyMedia?.mListener = this
        binding.rvMyMedia.adapter = adapterMyMedia
    }

    private fun setMediaStreamAlbumAdapter(list: ArrayList<MyMediaAlbumModel.EventAlbum>) {
        binding.rvMyMedia.visibility = View.VISIBLE
        val glManager = GridLayoutManager(HooleyMain.activity, 2)
        binding.rvMyMedia.setEmptyView(binding.tvNoData)
        binding.rvMyMedia.layoutManager = glManager
        adapterMyMediaAlbum = AdapterMediaAlbum(HooleyMain.activity!!, list)
        binding.rvMyMedia.adapter = adapterMyMediaAlbum
    }

    private fun showReportEventDialog(list: ArrayList<PersonalProfileModel.myObject>) {
        showMediaReportEventDialog = Dialog(HooleyMain.activity!!)
        showMediaReportEventDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        showMediaReportEventDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        showMediaReportEventDialog!!.setContentView(R.layout.dialog_report_event)
        val btnReport = showMediaReportEventDialog!!.findViewById<TextView>(R.id.btnReport)
        val edtMessage = showMediaReportEventDialog!!.findViewById<EditText>(R.id.edtMessage)
        val spSelectReason = showMediaReportEventDialog!!.findViewById<Spinner>(R.id.spSelectReason)
        spinnerAdapter = AdapterSpinner(activity, R.layout.sp_gender_group_item, list)
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spSelectReason.adapter = spinnerAdapter
        spSelectReason.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                reasonMediaItemPosition = position
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
        btnReport.setOnClickListener {
            showMediaReportEventDialog!!.dismiss()
            myMediaViewModel.submitReport(eventId, list[reasonMediaItemPosition].id!!, edtMessage.text.toString(), imageId)
        }
        showMediaReportEventDialog!!.show()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivFilter -> when (binding.rgMediaStream.checkedRadioButtonId) {
                R.id.rbEventList -> {

                    if (!moreTrendingDialogeFragment!!.isAdded)
                        moreTrendingDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)
                }
                R.id.rbAlbumView -> {
                    if (!moreDialogAlbumFragment!!.isAdded)
                        moreDialogAlbumFragment!!.show(HooleyMain.activity!!.supportFragmentManager)

                }
            }
        }
    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        when (checkedId) {
            R.id.rbEventList -> {
                type = Constants.TYPE_LIST_VIEW
                setTab()
            }
            R.id.rbAlbumView -> {
                type = Constants.TYPE_ALBUM_VIEW
                setTab()
            }
        }
    }

    private fun setTab() {
        when (type) {
            Constants.TYPE_LIST_VIEW -> {
                binding.rvMyMedia.visibility = View.GONE
                binding.rbEventList.typeface = boldFont
                binding.rbAlbumView.typeface = regularFont
                binding.rbEventList.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding.rbEventList.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_left_radio)
                binding.rbAlbumView.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbAlbumView.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                myMediaViewModel.getMyMedia()
            }
            Constants.TYPE_ALBUM_VIEW -> {
                binding.rvMyMedia.visibility = View.GONE
                binding.rbEventList.typeface = regularFont
                binding.rbAlbumView.typeface = boldFont
                binding.rbAlbumView.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding.rbAlbumView.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_right_radio)
                binding.rbEventList.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbEventList.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                myMediaViewModel.getMyMediaAlbumView()
            }
        }
    }

    private fun showShareEventDialog() {
        object : GetBitmapTask() {
            override fun onPostExecute(bmp: Bitmap) {
                super.onPostExecute(bmp)
                (HooleyMain.activity as HooleyMain).shareWithImage(bmp, myMediaViewModel.mList.value!![allFeedItemPosition].eventName!!, myMediaViewModel.mList.value!![allFeedItemPosition].imageCaption,
                        myMediaViewModel.mList.value!![allFeedItemPosition].eventId!!, myMediaViewModel.mList.value!![allFeedItemPosition].imageId)
            }
        }.execute(myMediaViewModel.mList.value!![allFeedItemPosition].mediaFiles[pagerPosition].thumbnailUrl)
    }

    override fun onClickPostComment(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, type: Int) {
        (HooleyMain.activity as ActivityBase).callFragment(R.id.container, AllFeedsSinglePostCommentFragment.newInstance(eventMediaInfo.eventId!!, eventMediaInfo.imageId), "AllFeedsSinglePostCommentFragment")
    }

    override fun onClickMore(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, pos: Int, listPosition: Int, mType: Int) {
        val arrayList = ArrayList<String>()
        if (eventMediaInfo.isFavorited)
            arrayList.add("Remove From Favorite")
        else
            arrayList.add("Add to Favorite")

        if (eventMediaInfo.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
            arrayList.add("Edit Post")

        arrayList.add("Download/Share Photo")

        if (eventMediaInfo.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
            arrayList.add("Delete Photo")
        else
            arrayList.add("Report Photo")
        morePostDialogeFragment = MoreDialogeFragment.Builder(activity!!).setTitle("More").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    if (eventMediaInfo.isFavorited) {
                        myMediaViewModel.mList.value!![pos].favoriteCount++
                        myMediaViewModel.mList.value!![pos].isFavorited = true
                    } else {
                        myMediaViewModel.mList.value!![pos].favoriteCount--
                        myMediaViewModel.mList.value!![pos].isFavorited = false
                    }
                    if (adapterMyMedia != null)
                        adapterMyMedia?.notifyDataSetChanged()
                    myMediaViewModel.mediaFav(eventMediaInfo.eventId!!, eventMediaInfo.imageId, !eventMediaInfo.isFavorited)
                    morePostDialogeFragment!!.dismiss()
                }
                1 -> {
                    morePostDialogeFragment!!.dismiss()
                    if (eventMediaInfo.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
                        (HooleyMain.activity as ActivityBase).callFragmentWithReplace(R.id.container, UpdatePostFragment(eventMediaInfo.eventId!!, eventMediaInfo.imageId), "UpdatePostFragment")
                    else {
                        onClickShare(eventMediaInfo.imageId, pos, listPosition, mType)
                    }
                }
                2 -> {
                    morePostDialogeFragment!!.dismiss()
                    if (arrayList.size == 3) {
                        if (eventMediaInfo.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
                            myMediaViewModel.deleteMedia(eventMediaInfo.imageId, eventMediaInfo.eventId!!)
                        else {
                            imageId = eventMediaInfo.imageId
                            eventId = eventMediaInfo.eventId!!
                            myMediaViewModel.getMediaReportEvent()
                        }
                    } else {
                        onClickShare(eventMediaInfo.imageId, pos, listPosition, mType)
                    }
                }
                3 -> {
                    morePostDialogeFragment!!.dismiss()
                    if (eventMediaInfo.userId!!.equals(HooleyApp.db.getString(Constants.USER_ID), ignoreCase = true))
                        myMediaViewModel.deleteMedia(eventMediaInfo.imageId, eventMediaInfo.eventId!!)
                    else {
                        imageId = eventMediaInfo.imageId
                        eventId = eventMediaInfo.eventId!!
                        myMediaViewModel.getMediaReportEvent()
                    }
                }
            }
        }.create()


        if (!morePostDialogeFragment!!.isAdded)
            morePostDialogeFragment!!.show(HooleyMain.activity!!.supportFragmentManager)

    }

    override fun onClickImageView(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo) {
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ViewPostMediaFragment.newInstance(eventMediaInfo), "ViewPostMediaFragment")
    }

    override fun onClickLikePost(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, position: Int, type: Int) {
        myMediaViewModel.likePost(eventMediaInfo.eventId!!, eventMediaInfo.imageId)
    }

    override fun onClickFav(eventMediaInfo: EventGalleryFeedModel.EventMediaInfo, position: Int, type: Int) {
        myMediaViewModel.mediaFav(eventMediaInfo.eventId!!, eventMediaInfo.imageId, !eventMediaInfo.isFavorited)
    }

    override fun onClickShare(imageId: String, pos: Int, pagerPos: Int, type: Int) {
        allFeedItemPosition = pos
        pagerPosition = pagerPos
        showShareEventDialog()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        myMediaViewModel.shareEventMedia(myMediaViewModel.mList.value!![allFeedItemPosition].eventId, myMediaViewModel.mList.value!![allFeedItemPosition].imageId, event.message, allFeedItemPosition)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        setTab()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
    }


}
