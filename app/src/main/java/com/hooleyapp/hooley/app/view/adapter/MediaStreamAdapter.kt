package com.hooleyapp.hooley.app.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterPostMediaPager
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.databinding.AdapterMediaStreamBinding
import com.hooleyapp.hooley.fragments.event.LiveEventFragment
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.interfaces.IEventAllFeedsClick
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import java.util.*

/**
 * Created by Nauman on 3/22/2018.
 */

class MediaStreamAdapter(private val context: Context, mList: ArrayList<EventGalleryFeedModel.EventMediaInfo>) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), AdapterPostMediaPager.IClickPostListener {
    internal var mList = ArrayList<EventGalleryFeedModel.EventMediaInfo>()
    lateinit var mListener: IEventAllFeedsClick
    private var adapter: AdapterPostMediaPager? = null

    init {
        this.mList = mList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<AdapterMediaStreamBinding>(LayoutInflater.from(parent.context), R.layout.adapter_media_stream, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            holder.bindHolderViewData(mList[position])
            initPager(holder, mList[position].mediaFiles, position)
        } catch (e: Exception) {
        }

        // setting listener
        holder.binding.ivUserProfile.setOnClickListener {
            var avatarfragment = ViewAvatarFragment(mList[position].userId!!.toInt())
            avatarfragment.show(HooleyMain.activity!!.supportFragmentManager)
        }
        holder.binding.ivPostMore.setOnClickListener { mListener.onClickMore(mList[position], position, holder.binding.vpMediaPost.currentItem, Constants.TYPE_EVENT_FEED_ALL) }
        holder.binding.llComment.setOnClickListener { mListener.onClickPostComment(mList[position], Constants.TYPE_EVENT_FEED_ALL) }
        holder.binding.llFav.setOnClickListener {
            mListener.onClickFav(mList[position], position, Constants.TYPE_EVENT_FEED_ALL)
            if (mList[position].isFavorited) {
                mList[position].isFavorited = false
                mList[position].favoriteCount--
            } else {
                mList[position].isFavorited = true
                mList[position].favoriteCount++
            }
            notifyDataSetChanged()
        }
        holder.binding.llLike.setOnClickListener {
            mListener.onClickLikePost(mList[position], position, Constants.TYPE_EVENT_FEED_ALL)
            if (mList[position].isLiked) {
                mList[position].isLiked = false
                mList[position].likesCount--
            } else {
                mList[position].isLiked = true
                mList[position].likesCount++
            }
            notifyDataSetChanged()
        }
        holder.binding.llShare.setOnClickListener { mListener.onClickShare(mList[position].imageId, position, holder.binding.vpMediaPost.currentItem, Constants.TYPE_EVENT_FEED_ALL) }
    }

    private fun initPager(holder: ItemViewHolder, mediaFiles: ArrayList<EventGalleryFeedModel.MediaFile>, position: Int) {
        adapter = AdapterPostMediaPager(mediaFiles, position)
        adapter!!.setMListener(this)
        holder.binding.vpMediaPost.adapter = adapter

        holder.binding.tlMediaPost.setupWithViewPager(holder.binding.vpMediaPost, true)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onClickPost(position: Int, listPosition: Int) {
        mListener.onClickImageView(mList[listPosition])
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var binding: AdapterMediaStreamBinding = DataBindingUtil.bind(v)!!
        @Throws(NullPointerException::class)
        @SuppressLint("SetTextI18n")
        fun bindHolderViewData(eventInfo: EventGalleryFeedModel.EventMediaInfo) {
            binding.tvEventName.text = eventInfo.eventName

            binding.tvEventName.setOnClickListener {
                (HooleyMain.activity as ActivityBase).addFragment(R.id.container, LiveEventFragment.newInstance(eventInfo.eventId!!), "LiveEventFragment")
            }
            binding.tvFullName.text = eventInfo.fullName
            if (!TextUtils.isEmpty(eventInfo.imageCaption)) {
                binding.tvPostDetail.visibility = View.VISIBLE
                binding.tvPostDetail.text = eventInfo.imageCaption
            } else {
                binding.tvPostDetail.visibility = View.GONE
            }
            binding.tvPostTime.text = "(" + DateUtils.convertDateToTimeAgo(DateUtils.getLocalDate(eventInfo.postedTime)) + ")"
            binding.tvLike.text = Integer.toString(eventInfo.likesCount)
            binding.tvComment.text = Integer.toString(eventInfo.commentsCount)
            binding.tvShare.text = Integer.toString(eventInfo.shareCounts)
            binding.tvFav.text = Integer.toString(eventInfo.favoriteCount)
            if (!TextUtils.isEmpty(eventInfo.profilePic))
                Glide.with(context).load(eventInfo.profilePic).into(binding.ivUserProfile)

            if (eventInfo.mediaFiles.size == 1) {
                binding.tlMediaPost.visibility = View.GONE
            } else {
                binding.tlMediaPost.visibility = View.VISIBLE
            }

            if (eventInfo.isLiked) {
                binding.ivLike.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_red_heart)
            } else {
                binding.ivLike.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_purple_heart)
            }
            if (eventInfo.isFavorited) {
                binding.ivFav.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_fav)
            } else {
                binding.ivFav.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_un_fav)
            }

            if (eventInfo.isLive)
                binding.ivLiveCircle.visibility = View.VISIBLE
            else
                binding.ivLiveCircle.visibility = View.GONE

            if (eventInfo.partialComments.size > 0) {
                binding.tvSingleComment.visibility = View.VISIBLE
                val sourceString = "<b>" + eventInfo.partialComments[0].fullName + "</b> " + eventInfo.partialComments[0].commentText
                binding.tvSingleComment.text = Html.fromHtml(sourceString)
                binding.tvSingleComment.setOnClickListener { mListener.onClickPostComment(eventInfo, Constants.TYPE_EVENT_FEED_ALL) }
            } else {
                binding.tvSingleComment.visibility = View.GONE
            }
        }
    }
}
