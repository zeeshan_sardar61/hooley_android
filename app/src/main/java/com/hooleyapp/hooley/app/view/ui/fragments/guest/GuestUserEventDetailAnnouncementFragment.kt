package com.hooleyapp.hooley.app.view.ui.fragments.guest

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventAnnouncementModel
import com.hooleyapp.hooley.app.view.adapter.AnnouncementsAdapter
import com.hooleyapp.hooley.app.view.callback.IAnnouncementClickListener
import com.hooleyapp.hooley.app.view.ui.activites.HooleyGuestUser
import com.hooleyapp.hooley.app.viewmodel.event.EventAnnouncementViewModel
import com.hooleyapp.hooley.databinding.FragmentEventDetailAnnouncementBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.ChatImageOverlayView
import com.hooleyapp.hooley.helper.setToolBarTitle
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.stfalcon.frescoimageviewer.ImageViewer

@SuppressLint("ValidFragment")
class GuestUserEventDetailAnnouncementFragment constructor(var eventId: String, var isMyEvent: Boolean) : BaseFragment(), IAnnouncementClickListener {

    lateinit var binding: FragmentEventDetailAnnouncementBinding
    lateinit var eventDetailAnnouncementViewModel: EventAnnouncementViewModel
    private var adapterAnnouncements: AnnouncementsAdapter? = null
    private var overlayView: ChatImageOverlayView? = null
    private var imageViewer: ImageViewer? = null

    private val customFormatter: ImageViewer.Formatter<String>
        get() = ImageViewer.Formatter { customImage -> customImage }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_event_detail_announcement, container, false)
        eventDetailAnnouncementViewModel = ViewModelProviders.of(this).get(EventAnnouncementViewModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rlParent.visibility = View.GONE
        setUiObservers()
        eventDetailAnnouncementViewModel.getEventAnnouncement(eventId)
    }

    private fun setUiObservers() {
        eventDetailAnnouncementViewModel.failureMessage.observe(this,
                android.arch.lifecycle.Observer {
                    it?.let { text ->
                        if (!text.contains("No annoucment"))
                            HooleyGuestUser.activity.showSuccessDialog(text)
                    }
                })
        eventDetailAnnouncementViewModel.sessionExpire.observe(this,
                android.arch.lifecycle.Observer {
                    onTokenExpireLogOut()
                })
        eventDetailAnnouncementViewModel.loading.observe(this, android.arch.lifecycle.Observer {
            if (it!!)
                binding.pbAnnouncement.visibility = View.VISIBLE
            else
                binding.pbAnnouncement.visibility = View.GONE
        })
        eventDetailAnnouncementViewModel.mlist.observe(this, android.arch.lifecycle.Observer {
            it?.let { list ->
                setAnnouncementRv(list)
//                adapterAnnouncements?.setList(list)
            }
        })
    }

    private fun setAnnouncementRv(list: ArrayList<EventAnnouncementModel.Announcement>) {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyGuestUser.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyGuestUser.activity, LinearLayoutManager.VERTICAL, false)
        binding.rvAnnouncements.layoutManager = manager
        binding.rvAnnouncements.layoutAnimation = animation
        adapterAnnouncements = AnnouncementsAdapter(isMyEvent, list)
//        adapterAnnouncements?.setList(list)
        adapterAnnouncements!!.mListener = this
        binding.rvAnnouncements.adapter = adapterAnnouncements
        binding.rvAnnouncements.setEmptyView(binding.tvNoData)
    }


    override fun onMoreClick(position: Int) {
    }

    override fun onImageClick(position: Int) {
        overlayView = ChatImageOverlayView(HooleyGuestUser.activity)
        val imageArray = ArrayList<String>()
        imageArray.add(eventDetailAnnouncementViewModel.mlist.value!![position].imageUrl!!)
        imageViewer = ImageViewer.Builder(HooleyGuestUser.activity, imageArray)
                .setFormatter(customFormatter)
                .setStartPosition(0)
                .setOverlayView(overlayView)
                .show()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
    }

    fun setToolbar() {
        setToolBarTitle(HooleyGuestUser.activity, "Announcements")
    }

}
