package com.hooleyapp.hooley.app.viewmodel.messages

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.net.Uri
import com.hooleyapp.hooley.app.data.model.messages.GetAllMessageModel
import com.hooleyapp.hooley.app.data.repository.messages.ChatWindowRepository
import java.util.*

class ChatWindowViewModel(application: Application) : AndroidViewModel(application) {

    var repository = ChatWindowRepository()
    var mList: MutableLiveData<ArrayList<GetAllMessageModel.MessageInfo>> = MutableLiveData()
    var failureMessage: MutableLiveData<String> = MutableLiveData()
    var threadId: MutableLiveData<String> = MutableLiveData()
    var sessionExpire: MutableLiveData<Boolean> = MutableLiveData()
    var reloadApi: MutableLiveData<Boolean> = MutableLiveData()
    var loading: MutableLiveData<Boolean> = MutableLiveData()
    var canMessage: MutableLiveData<Boolean> = MutableLiveData()

    init {
        this.mList = repository.mList
        this.failureMessage = repository.failureMessage
        this.threadId = repository.threadId
        this.sessionExpire = repository.sessionExpire
        this.reloadApi = repository.reloadApi
        this.loading = repository.loading
        this.canMessage = repository.canMessage
    }

    fun getAllMessages(threadId: String) {
        repository.getAllMessages(threadId)
    }

    fun createThread(friendId: String) {
        repository.createThread(friendId)
    }

    fun sendTextMessage(threadId: String, msg: String, msgType: String, position: Int) {
        repository.sendTextMessage(threadId, msg, msgType, position)
    }

    fun sendMediaMessage(uri: Uri?, isVideo: Boolean, threadId: String, position: Int) {
        repository.sendMediaMessage(uri, isVideo, threadId, position)
    }
}