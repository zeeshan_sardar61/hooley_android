package com.hooleyapp.hooley.app.data.model.media

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 4/3/2018.
 */

class MyMediaAlbumModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("eventAlbums")
    var eventAlbumsList = ArrayList<EventAlbum>()

    class EventAlbum {

        @SerializedName("eventId")
        var eventId: String? = null

        @SerializedName("imageUrl")
        var imageUrl: String? = null

        @SerializedName("address")
        var address: String? = null

        @SerializedName("coverPhoto")
        var coverPhoto: String? = null

        @SerializedName("startTime")
        var startTime: String? = null

        @SerializedName("endTime")
        var endTime: String? = null

        @SerializedName("eventName")
        var eventName: String? = null

        @SerializedName("videoUrl")
        var videoUrl: String? = null

        @SerializedName("thumbnailUrl")
        var thumbnailUrl: String? = null

        @SerializedName("mediaCount")
        var mediaCount: Int = 0

        @SerializedName("isVideo")
        var isVideo: Boolean = false

        @SerializedName("isLive")
        var isLive: Boolean = false

        @SerializedName("isPast")
        var isPast: Boolean = false

    }

    companion object {
        var _obj: MyMediaAlbumModel? = null

        //Getter and setter methods same as the above examples
        /*Comparator for sorting the list by Student Name*/
        var sortByNameComparator: Comparator<EventAlbum> = Comparator { s1, s2 ->
            val s = s1.eventName!!.toUpperCase()
            val s3 = s2.eventName!!.toUpperCase()
            s.compareTo(s3)
        }

        /*Comparator for sorting the list by Student Name*/
        var sortByTimeComparator: Comparator<EventAlbum> = Comparator { s1, s2 ->
            val s = s1.startTime!!.toUpperCase()
            val s3 = s2.startTime!!.toUpperCase()
            s.compareTo(s3)
        }
    }

}
