package com.hooleyapp.hooley.app.view.callback

interface IEventClickListener {
    fun onClickItem(position: Int)
    fun onClickLike(position: Int)
    fun onClickShare(position: Int)
    fun onClickFollow(position: Int)
    fun onMapClick(position: Int)
    fun onClickInterestedPoeople(position: Int)
}
