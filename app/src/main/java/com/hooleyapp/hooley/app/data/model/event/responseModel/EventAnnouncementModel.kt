package com.hooleyapp.hooley.app.data.model.event.responseModel

import com.google.gson.annotations.SerializedName
import java.util.*

class EventAnnouncementModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null


    @SerializedName("eventDisplayAnnoucmentList")
    var eventDisplayAnnoucment = ArrayList<Announcement>()


    class Announcement {
        @SerializedName("id")
        var id: Int? = null

        @SerializedName("postedDate")
        var postedDate: String? = null

        @SerializedName("type")
        var type: Int? = null

        @SerializedName("imageUrl")
        var imageUrl: String? = null

        @SerializedName("description")
        var description: String? = null

        @SerializedName("oldStartDate")
        var oldStartDate: String? = null

        @SerializedName("oldEndDate")
        var oldEndDate: String? = null

        @SerializedName("newStartDate")
        var newStartDate: String? = null

        @SerializedName("newEndDate")
        var newEndDate: String? = null

        @SerializedName("oldLocation")
        var oldLocation: String? = null

        @SerializedName("newLocation")
        var newLocation: String? = null

//        @SerializedName("announcement")
//        var announcementText: String? = null

        @SerializedName("broadcastAll")
        var broadcastAll: Boolean = false

        @SerializedName("broadcastHereNow")
        var broadcastHereNow: Boolean = false

    }


}