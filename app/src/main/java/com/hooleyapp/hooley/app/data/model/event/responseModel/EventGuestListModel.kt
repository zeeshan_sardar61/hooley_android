package com.hooleyapp.hooley.app.data.model.event.responseModel

import com.google.gson.annotations.SerializedName
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.*

class EventGuestListModel {
    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("guestList")
    var attendingAndInvitedList = ArrayList<FriendsTable>()

}