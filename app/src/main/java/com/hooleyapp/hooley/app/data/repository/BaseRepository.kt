package com.hooleyapp.hooley.app.data.repository

import com.hooleyapp.hooley.helper.SingleLiveEvent

open class BaseRepository {
    var failureMessage: SingleLiveEvent<String> = SingleLiveEvent()
    var sessionExpire: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var reloadApi: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var loading: SingleLiveEvent<Boolean> = SingleLiveEvent()

}