package com.hooleyapp.hooley.app.view.ui.fragments.guest

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.adapters.AdapterSearchEvent
import com.hooleyapp.hooley.app.view.ui.activites.HooleyGuestUser
import com.hooleyapp.hooley.databinding.FragmentSearchResultBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.DialogGuestUser
import com.hooleyapp.hooley.interfaces.INearByEventClickListener
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.NearByEventsFinalModel
import com.hooleyapp.hooley.model.SearchEventModel
import com.hooleyapp.hooley.model.staticData.Category
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.services.EventWebService
import java.util.*

class GuestUserSearchResultFragment : BaseFragment(), INearByEventClickListener {
    internal var mList: ArrayList<NearByEventsFinalModel>? = ArrayList()
    var binding: FragmentSearchResultBinding? = null
    private var objectResponse: SearchEventModel? = null
    private var adapter: AdapterSearchEvent? = null
    lateinit var nearByEvents: NearByEventsFinalModel
    private var position = 0
    private var eventService = EventWebService()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_result, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchEvent()
        (HooleyGuestUser.activity as HooleyGuestUser).setToolbarTitle("Search Results")

    }

    private fun searchEvent() {
        binding!!.pbSearchEvent.visibility = View.VISIBLE
        eventService.searchEvent(searchString, raduis, searchDate!!, catList!!, isPaid, isFree, object : IWebServiceCallback<SearchEventModel> {
            override fun success(result: SearchEventModel) {
                binding!!.pbSearchEvent.visibility = View.GONE
                objectResponse = result
                addAllDataInList()
                binding!!.tvNoData.visibility = View.GONE
            }

            override fun failure(message: String) {
                try {
                    binding!!.pbSearchEvent.visibility = View.GONE
                    binding!!.tvNoData.visibility = View.VISIBLE
                    binding!!.rvSearchResult.visibility = View.GONE
                    binding!!.tvNoData.text = message
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    private fun addAllDataInList() {
        if (mList != null)
            mList!!.clear()
        if (objectResponse!!.featuredEventsList.size > 0)
            mList!!.add(NearByEventsFinalModel(Constants.TYPE_FEATURE_EVENT, objectResponse!!.featuredEventsList))
//        mList!!.add(NearByEventsFinalModel(Constants.TYPE_MENU_EVENT, Constants.TYPE_UPCOMMING_EVENT))
        if (objectResponse!!.todayEventsList.size > 0)
            mList!!.add(NearByEventsFinalModel(Constants.TYPE_TODAY_EVENT, objectResponse!!.todayEventsList))
        if (objectResponse!!.nextWeekEventsList.size > 0)
            mList!!.add(NearByEventsFinalModel(Constants.TYPE_NEXT_WEEK_EVENT, objectResponse!!.nextWeekEventsList))
        if (objectResponse!!.commingEventsList.size > 0)
            mList!!.add(NearByEventsFinalModel(Constants.TYPE_COMMING_EVENT, objectResponse!!.commingEventsList))
        setRecylerView()
    }

    private fun setRecylerView() {
        binding!!.rvSearchResult.visibility = View.VISIBLE
        val manager = LinearLayoutManager(ActivityBase.activity, LinearLayoutManager.VERTICAL, false)
        binding!!.rvSearchResult.layoutManager = manager
        binding!!.rvSearchResult.setEmptyView(binding!!.tvNoData)
        adapter = AdapterSearchEvent(ActivityBase.activity, mList!!)
        binding!!.rvSearchResult.adapter = adapter
        adapter!!.setListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null)
            binding!!.unbind()
    }

    override fun onClickLike(nearByEventsFinalModel: NearByEventsFinalModel, pos: Int, mListPosition: Int) {
        HooleyGuestUser.activity.DialogGuestUser()
    }

    override fun onClickComment() {
        // unused
    }

    override fun onUpComingEvent() {
        addAllDataInList()
    }

    override fun onPastEvent() {
    }

    override fun onClickShare(nearByEventsFinalModel: NearByEventsFinalModel, pos: Int) {
        HooleyGuestUser.activity.DialogGuestUser()
    }

    override fun onClickFollow(nearByEventsFinalModel: NearByEventsFinalModel, pos: Int, listPosition: Int) {
        HooleyGuestUser.activity.DialogGuestUser()
    }


    companion object {
        lateinit var instance: GuestUserSearchResultFragment
        private var raduis: Double = 0.toDouble()
        private var searchString = ""
        private var searchDate: String? = null
        private var catList: ArrayList<Category>? = null
        private var isPaid = false
        private var isFree = false

        fun newInstance(miles: Double, paid: Boolean, free: Boolean, searchText: String, date: String, list: ArrayList<Category>): GuestUserSearchResultFragment {
            raduis = miles
            isPaid = paid
            isFree = free
            searchString = searchText
            searchDate = date
            catList = list
            instance = GuestUserSearchResultFragment()
            return instance
        }
    }

}
