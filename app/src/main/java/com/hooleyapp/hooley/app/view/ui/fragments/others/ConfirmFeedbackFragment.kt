package com.hooleyapp.hooley.app.view.ui.fragments.others

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.databinding.FragmentConfirmFeedBackBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.others.Util

/**
 * Created by Nauman on 5/17/2018.
 */

class ConfirmFeedbackFragment : BaseFragment() {

    lateinit var binding: FragmentConfirmFeedBackBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_confirm_feed_back, container, false)
        Util.hideKeyboard(activity!!)
        return binding.root
    }
}
