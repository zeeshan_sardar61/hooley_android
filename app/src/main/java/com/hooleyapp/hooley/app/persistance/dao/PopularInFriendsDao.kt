package com.hooleyapp.hooley.app.persistance.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.hooleyapp.hooley.app.data.model.event.responseModel.PopularInFriendsModel

@Dao
interface PopularInFriendsDao {

    @Query("SELECT * from popular_in_friend")
    fun getPopularInFriendsEvent(): LiveData<List<PopularInFriendsModel.PopularInFriends>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllEvents(popularInFriendList: List<PopularInFriendsModel.PopularInFriends>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(item: PopularInFriendsModel.PopularInFriends)

    @Query("DELETE FROM popular_in_friend ")
    fun deleteAll()

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun likeUnLikeCount(item: PopularInFriendsModel.PopularInFriends)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateList(popularInFriendList: List<PopularInFriendsModel.PopularInFriends>)

}