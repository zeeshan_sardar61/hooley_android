package com.hooleyapp.hooley.app.data.repository.event

import com.hooleyapp.hooley.app.data.model.event.responseModel.EventDetailModel
import com.hooleyapp.hooley.helper.SingleLiveEvent
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GetReportEventModel
import com.hooleyapp.hooley.model.PersonalProfileModel
import com.hooleyapp.hooley.services.EventWebService
import com.hooleyapp.hooley.services.GuestUserWebService

class EventDetailRepository {

    var failureMessage: SingleLiveEvent<String> = SingleLiveEvent()
    var sessionExpire: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var loading: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var response: SingleLiveEvent<EventDetailModel> = SingleLiveEvent()
    var isSectionUpdate: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var actionType: SingleLiveEvent<String> = SingleLiveEvent()
    var reportEventList: SingleLiveEvent<List<PersonalProfileModel.myObject>> = SingleLiveEvent()
    var service = EventWebService()
    var guestservice = GuestUserWebService()

    // NetWorkRequest
    fun getEventDetails(eventId: String) {
        loading.postValue(true)
        service.seeEventDetail(eventId, object : IWebServiceCallback<EventDetailModel> {
            override fun success(result: EventDetailModel) {
                loading.postValue(false)
                response.postValue(result)
                isSectionUpdate.postValue(false)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })
    }


    fun followEvent(eventId: String, isFollow: Boolean) {
        loading.postValue(true)
        service.followEvent(eventId, isFollow, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                loading.postValue(false)
                isSectionUpdate.postValue(true)
                if (response.value!!.isFollow) {
                    response.value!!.followersCount--
                    response.value!!.isFollow = false

                } else {
                    response.value!!.followersCount++
                    response.value!!.isFollow = true
                }
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })

    }

    fun attendEvent(eventId: String, isAttend: Boolean) {
        loading.postValue(true)
        service.attendEvent(eventId, isAttend, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                loading.postValue(false)
                isSectionUpdate.postValue(true)
                if (isAttend) {
                    response.value!!.attendCount += 1
                    response.value!!.isAttended = true
                } else {
                    response.value!!.attendCount -= 1
                    response.value!!.isAttended = false
                }
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })
    }

    fun bookMarkEvent(eventId: String?, isRemove: Boolean) {
        loading.postValue(true)
        service.bookmarkEvent(eventId!!, isRemove, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                loading.postValue(false)
                isSectionUpdate.postValue(true)
                response.value!!.eventDisplayBasicInfo!!.isBookmarked = !isRemove
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })
    }

    fun likeEvent(eventId: String?) {
        loading.postValue(true)
        service.likeEvent(eventId!!, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                loading.postValue(false)
                isSectionUpdate.postValue(true)
                if (response.value!!.eventDisplayBasicInfo!!.isLiked) {
                    response.value!!.likes -= 1
                    response.value!!.eventDisplayBasicInfo!!.isLiked = false
                } else {
                    response.value!!.likes += 1
                    response.value!!.eventDisplayBasicInfo!!.isLiked = true
                }
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })
    }

    fun getReportEvent() {
        loading.postValue(true)
        service.getReportEvent(object : IWebServiceCallback<GetReportEventModel> {
            override fun success(result: GetReportEventModel) {
                loading.postValue(false)
                reportEventList.postValue(result.reportReasonsArrayList)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })

    }

    fun submitReport(eventId: String, reasonId: String?, reportMessage: String) {
        loading.postValue(true)
        service.submitReport(eventId, reasonId!!, reportMessage, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                loading.postValue(false)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })
    }

    fun addMarkOnWay(eventId: String, type: String) {
        loading.postValue(true)
        service.addMarkOnWay(eventId, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                loading.postValue(false)
                actionType.postValue(type)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })
    }

    fun addGuestUserBookMarkEvent(eventId: String?, isRemove: Boolean) {
        loading.postValue(true)
        guestservice.addGuestUserBookMarkEvent(eventId!!, isRemove, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
                loading.postValue(false)
                isSectionUpdate.postValue(true)
                response.value!!.eventDisplayBasicInfo!!.isBookmarked = !isRemove
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
                loading.postValue(false)
                sessionExpire.postValue(true)
            }
        })
    }

    fun shareEvent(shareMedia: String, eventId: String) {
        service.shareEvent(eventId, shareMedia, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
            }

            override fun failure(message: String) {
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
                sessionExpire.postValue(true)
            }
        })
    }
}