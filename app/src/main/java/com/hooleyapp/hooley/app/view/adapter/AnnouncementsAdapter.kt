package com.hooleyapp.hooley.app.view.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.databinding.DataBindingUtil
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventAnnouncementModel
import com.hooleyapp.hooley.app.view.callback.IAnnouncementClickListener
import com.hooleyapp.hooley.databinding.AdapterAnnouncementBinding
import com.hooleyapp.hooley.databinding.ItemAnnoucementLocationBinding
import com.hooleyapp.hooley.databinding.ItemAnnoucementTimeStampBinding
import com.hooleyapp.hooley.fragments.event.LiveEventFragment
import com.hooleyapp.hooley.helper.load
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import me.saket.bettermovementmethod.BetterLinkMovementMethod


/**
 * Created by Zeeshan on 17-Jan-18.
 */

class AnnouncementsAdapter(val myEvent: Boolean, var mList: List<EventAnnouncementModel.Announcement>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val VIEW_ANNOUCMENT = 0
    private val VIEW_LOCATION = 1
    private val VIEW_TIME_STAMP = 2

    //    internal var mList = listOf<EventAnnouncementModel.Announcement>()
    lateinit var mListener: IAnnouncementClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_ANNOUCMENT -> {
                val binding: AdapterAnnouncementBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_announcement, parent, false)
                BetterLinkMovementMethod.linkify(Linkify.WEB_URLS, binding.root as ViewGroup)
                ItemViewHolder(binding.root)
            }
            VIEW_LOCATION -> {
                val binding: ItemAnnoucementLocationBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_annoucement_location, parent, false)
                ItemLocationViewHolder(binding.root)
            }
            VIEW_TIME_STAMP -> {
                val binding: ItemAnnoucementTimeStampBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_annoucement_time_stamp, parent, false)
                ItemTimeStampViewHolder(binding.root)
            }
            else -> {
                val binding: AdapterAnnouncementBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_announcement, parent, false)
                BetterLinkMovementMethod.linkify(Linkify.WEB_URLS, binding.root as ViewGroup)
                ItemViewHolder(binding.root)
            }
        }
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            VIEW_ANNOUCMENT -> {
                val holder = mHolder as ItemViewHolder
                holder.bindHolderViewData(mList[position])
                //Setting listener
                holder.binding.ivMore.setOnClickListener { mListener.onMoreClick(position) }
                holder.binding.ivAnnouncement.setOnClickListener { mListener.onImageClick(position) }
                BetterLinkMovementMethod.linkifyHtml(holder.binding.tvDescription).setOnLinkClickListener { _, url ->
                    if (url.contains(Constants.EVENT_SHARE)) {
                        val eventIdString = url.split("eventId=")
                        (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment
                                .newInstance(eventIdString[1]), "LiveEventFragment")
                    } else {
                        val browserIntent = Intent(ACTION_VIEW)
                        browserIntent.data = Uri.parse(url)
                        HooleyMain.activity!!.startActivity(browserIntent)
                    }
                    true
                }
            }
            VIEW_LOCATION -> {
                val holder = mHolder as ItemLocationViewHolder
                holder.bindHolderViewData(mList[position])
            }
            VIEW_TIME_STAMP -> {
                val holder = mHolder as ItemTimeStampViewHolder
                holder.bindHolderViewData(mList[position])
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (mList[position].type!!) {
            1 -> VIEW_ANNOUCMENT
            2 -> VIEW_LOCATION
            3 -> VIEW_TIME_STAMP
            else -> VIEW_ANNOUCMENT
        }
    }


    override fun getItemCount(): Int {
        return mList.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var binding: AdapterAnnouncementBinding = DataBindingUtil.bind(v)!!

        @Throws(NullPointerException::class)
        @SuppressLint("SetTextI18n")
        fun bindHolderViewData(announcements: EventAnnouncementModel.Announcement) {
            binding.tvTime.text = DateUtils.displayDateOnlyForAnnocument(DateUtils.getLocalDate(announcements.postedDate!!))
            binding.tvDescription.text = announcements.description!!
            if (!announcements.imageUrl!!.isEmpty()) {
                binding.ivBackgroundAnn.load(announcements.imageUrl!!)
                binding.ivAnnouncement.load(announcements.imageUrl!!)
                binding.ivBackgroundAnn.visibility = View.VISIBLE
                binding.ivAnnouncement.visibility = View.VISIBLE
                binding.bvView.visibility = View.VISIBLE
            } else {
                binding.ivBackgroundAnn.visibility = View.GONE
                binding.ivAnnouncement.visibility = View.GONE
                binding.bvView.visibility = View.GONE
            }
            if (!myEvent) {
                binding.ivMore.visibility = View.GONE
            } else
                binding.ivMore.visibility = View.VISIBLE
        }
    }

    inner class ItemLocationViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var binding: ItemAnnoucementLocationBinding = DataBindingUtil.bind(v)!!

        @Throws(NullPointerException::class)
        @SuppressLint("SetTextI18n")
        fun bindHolderViewData(announcements: EventAnnouncementModel.Announcement) {
            binding.tvTime.text = DateUtils.displayDateOnlyForAnnocument(DateUtils.getLocalDate(announcements.postedDate!!))
            binding.tvNewLocation.text = announcements.newLocation
            binding.tvOldLocation.text = announcements.oldLocation
        }
    }

    inner class ItemTimeStampViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var binding: ItemAnnoucementTimeStampBinding = DataBindingUtil.bind(v)!!

        @Throws(NullPointerException::class)
        @SuppressLint("SetTextI18n")
        fun bindHolderViewData(announcements: EventAnnouncementModel.Announcement) {
            binding.tvTime.text = DateUtils.displayDateOnlyForAnnocument(DateUtils.getLocalDate(announcements.postedDate!!))
            binding.tvDescription.text = announcements.description!!
            binding.tvNewTime.text = DateUtils.convertDateNew(DateUtils.getLocalDate(announcements.newStartDate!!), DateUtils.getLocalDate(announcements.newEndDate!!))
            binding.tvOldTime.text = DateUtils.convertDateNew(DateUtils.getLocalDate(announcements.oldStartDate!!), DateUtils.getLocalDate(announcements.oldEndDate!!))
        }
    }

//    internal fun setList(list: List<EventAnnouncementModel.Announcement>) {
//        this.mList = list
//        notifyDataSetChanged()
//    }
}
