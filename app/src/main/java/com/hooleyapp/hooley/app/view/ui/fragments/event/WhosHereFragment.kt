package com.hooleyapp.hooley.app.view.ui.fragments.event

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.RadioGroup
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.adapters.AdapterMoreMediaDialoge
import com.hooleyapp.hooley.app.view.adapter.WhosHereFollowingAdapter
import com.hooleyapp.hooley.app.view.callback.IEventGuestListClickListener
import com.hooleyapp.hooley.app.viewmodel.event.WhosHereViewModel
import com.hooleyapp.hooley.databinding.FragmentWhosHereBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.event.LiveEventFragment
import com.hooleyapp.hooley.fragments.friends.CardPagerFragment
import com.hooleyapp.hooley.fragments.friends.FriendsProfileFragment
import com.hooleyapp.hooley.fragments.messaging.ChatWindowFragment
import com.hooleyapp.hooley.fragments.others.MoreMediaDialogFragment
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.fragments.profile.ProfileMainFragment
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.interfaces.IBuyPPV
import com.hooleyapp.hooley.model.WhosHereModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.Util

/**
 * Created by Adil Malik on 2/14/2018.
 */

class WhosHereFragment : BaseFragment(), RadioGroup.OnCheckedChangeListener, View.OnClickListener, TextWatcher, SwipeRefreshLayout.OnRefreshListener, IEventGuestListClickListener, ViewAvatarFragment.CardCallBackListener {

    lateinit var binding: FragmentWhosHereBinding
    lateinit var whosHerViewModel: WhosHereViewModel
    var handler = Handler()
    var type = Constants.WHOS_HERE_FOLLOWING
    internal val arrayList = ArrayList<AdapterMoreMediaDialoge.MoreItem>()
    private var moreWhosHereDialogeFragmen: MoreMediaDialogFragment? = null
    private var adapterWhosHere: WhosHereFollowingAdapter? = null
    var isMaleChecked = false
    var isFemaleChecked = false
    var isOtherChecked = false
    var boldFont: Typeface
    var regularFont: Typeface
    lateinit var mListener: IBuyPPV
    var mList_ = ArrayList<WhosHereModel.User>()

    init {
        boldFont = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto_bold)!!
        regularFont = ResourcesCompat.getFont(HooleyMain.activity!!, R.font.roboto)!!
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser && isResumed) {
            if (::whosHerViewModel.isInitialized)
                whosHerViewModel.getWhosHere(LiveEventFragment.eventId, Constants.WHOS_HERE_FOLLOWING)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_whos_here, container, false)
        whosHerViewModel = ViewModelProviders.of(this).get(WhosHereViewModel::class.java)
        setRecyclerView()
        setUiObserver()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            setMoreCallerBackListener()
        }
        setListener()
        if (!LiveEventFragment.isPostAllow) {
            binding.rbOnWay.visibility = View.GONE
            binding.rbHereNow.visibility = View.GONE
            binding.rbFollowing.text = "Followed"
            binding.rbLeft.text = "Attended"
        }


        if (!LiveEventFragment.objectResult.isPpvPurchased && LiveEventFragment.objectResult.ppvPrice > 0.0) {
            if (!LiveEventFragment.objectResult.isMyEvent) {
                binding.llppvView.visibility = View.VISIBLE
                binding.tvEventPrice.text = "$" + LiveEventFragment.objectResult.ppvPrice.toInt().toString()
            } else {
                binding.llppvView.visibility = View.GONE
            }
        } else {
            binding.llppvView.visibility = View.GONE
        }

        binding.btnBuyNow.setOnClickListener {
            mListener.onBuyPPV()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setUiObserver() {
        whosHerViewModel.sessionExpire.observe(this, android.arch.lifecycle.Observer { onTokenExpireLogOut() })
        whosHerViewModel.reloadApi.observe(this, android.arch.lifecycle.Observer {
            whosHerViewModel.getWhosHere(LiveEventFragment.eventId, type)
        })
        whosHerViewModel.loading.observe(this, android.arch.lifecycle.Observer { it.let { isLoading -> updateProgress(isLoading!!) } })
        whosHerViewModel.failureMessage.observe(this, android.arch.lifecycle.Observer { it.let { message -> binding.tvNoData.text = message!! } })
        whosHerViewModel.mList.observe(this, android.arch.lifecycle.Observer {
            if (binding.slWhosHere.isRefreshing)
                binding.slWhosHere.isRefreshing = false
            it?.let { list ->
                if (adapterWhosHere == null)
                    return@let
                mList_.clear()
                mList_.addAll(list)
                adapterWhosHere?.setList(mList_, type)
                binding.rvFollowing.visibility = View.VISIBLE
                binding.tvPeopleCount.text = if (mList_.size == 1) {
                    "${mList_.size} Person "
                } else {
                    "${mList_.size} People"
                }
            }
        })
    }

    private fun updateProgress(isLoading: Boolean) {
        if (binding.slWhosHere.isRefreshing)
            return
        if (isLoading) {
            binding.rvFollowing.visibility = View.GONE
            when (type) {
                Constants.WHOS_HERE_FOLLOWING -> {
                    binding.pbFollowing.visibility = View.VISIBLE
                    binding.pbOnWay.visibility = View.GONE
                    binding.pbOnWay.visibility = View.GONE
                    binding.pbOnWay.visibility = View.GONE
                }

                Constants.WHOS_HERE_ONTHEWAY -> {
                    binding.pbFollowing.visibility = View.GONE
                    binding.pbOnWay.visibility = View.VISIBLE
                    binding.pbHereNow.visibility = View.GONE
                    binding.pbLeft.visibility = View.GONE

                }
                Constants.WHOS_HERE_NOW -> {
                    binding.pbFollowing.visibility = View.GONE
                    binding.pbOnWay.visibility = View.GONE
                    binding.pbHereNow.visibility = View.VISIBLE
                    binding.pbLeft.visibility = View.GONE
                }
                Constants.WHOS_HERE_LEFT -> {
                    binding.pbFollowing.visibility = View.GONE
                    binding.pbOnWay.visibility = View.GONE
                    binding.pbHereNow.visibility = View.GONE
                    binding.pbLeft.visibility = View.VISIBLE
                }
            }
        } else {
            binding.pbFollowing.visibility = View.GONE
            binding.pbOnWay.visibility = View.GONE
            binding.pbHereNow.visibility = View.GONE
            binding.pbLeft.visibility = View.GONE
        }
    }

    private fun setListener() {
        binding.edtSearch.addTextChangedListener(this)
        binding.rgWhosHere.setOnCheckedChangeListener(this)
        binding.ivFilter.setOnClickListener(this)
        binding.rbMale.setOnClickListener(this)
        binding.rbFemale.setOnClickListener(this)
        binding.rbOther.setOnClickListener(this)
        binding.ivClose.setOnClickListener(this)
        binding.slWhosHere.setOnRefreshListener(this)
    }

    private fun setRecyclerView() {
        val animation = AnimationUtils.loadLayoutAnimation(HooleyMain.activity, R.anim.layout_animation_fall_down)
        val manager = LinearLayoutManager(HooleyMain.activity, LinearLayoutManager.VERTICAL, false)
        binding.rvFollowing.layoutAnimation = animation
        binding.rvFollowing.layoutManager = manager
        binding.rvFollowing.setEmptyView(binding.tvNoData)
        adapterWhosHere = WhosHereFollowingAdapter(HooleyMain.activity!!)
        adapterWhosHere?.mListener = this
        binding.rvFollowing.adapter = adapterWhosHere
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private fun setMoreCallerBackListener() {
        if (arrayList.size > 0)
            arrayList.clear()
        val itemAtoZ = AdapterMoreMediaDialoge.MoreItem("A - Z", false)
//        val itemTime = AdapterMoreMediaDialoge.MoreItem("Time", false)
        val itemCurrentCity = AdapterMoreMediaDialoge.MoreItem("Current City", false)
        val itemHooleyFriends = AdapterMoreMediaDialoge.MoreItem("Hooley Friends", false)
        val itemShareInterest = AdapterMoreMediaDialoge.MoreItem("Shared Interests", false)
        arrayList.add(itemAtoZ)
//        arrayList.add(itemTime)
        arrayList.add(itemCurrentCity)
        arrayList.add(itemHooleyFriends)
        arrayList.add(itemShareInterest)
        arrayList[0].isCheck = true
        moreWhosHereDialogeFragmen = MoreMediaDialogFragment.Builder(HooleyMain.activity!!).setTitle("Sort By").setType(Constants.TYPE_MORE_80).setList(arrayList).setOnImageSelectedListener { position, type ->
            when (position) {
                0 -> {
                    moreWhosHereDialogeFragmen!!.dismiss()
                    if (mList_.size > 0) {
                        whosHerViewModel.sortByAlphabetical()
                        arrayList[0].isCheck = true
                        arrayList[1].isCheck = false
                        arrayList[2].isCheck = false
                        arrayList[3].isCheck = false
                    }
                }
                1 -> {
                    moreWhosHereDialogeFragmen!!.dismiss()
                    if (mList_.size > 0) {
                        whosHerViewModel.sortByCity()
                        arrayList[0].isCheck = false
                        arrayList[1].isCheck = true
                        arrayList[2].isCheck = false
                        arrayList[3].isCheck = false
                    }
                }
                2 -> {
                    moreWhosHereDialogeFragmen!!.dismiss()
                    if (mList_.size > 0) {
                        arrayList[0].isCheck = false
                        arrayList[1].isCheck = false
                        arrayList[2].isCheck = true
                        arrayList[3].isCheck = false
                        whosHerViewModel.sortByHooleyFriends()
                    }
                }
                3 -> {
                    moreWhosHereDialogeFragmen!!.dismiss()
                    if (mList_.size > 0) {
                        arrayList[0].isCheck = false
                        arrayList[1].isCheck = false
                        arrayList[2].isCheck = false
                        arrayList[3].isCheck = true
                        whosHerViewModel.sortByShareInterest()
                    }
                }

            }
        }.create()
    }

    private fun setTabs() {
        when (type) {
            Constants.WHOS_HERE_FOLLOWING -> {
                binding.rbFollowing.typeface = boldFont
                binding.rbOnWay.typeface = regularFont
                binding.rbHereNow.typeface = regularFont
                binding.rbLeft.typeface = regularFont
//                binding.tvNoData.visibility = View.GONE
                binding.rbFollowing.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding.rbFollowing.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_left_radio)
                binding.rbOnWay.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbOnWay.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding.rbHereNow.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbHereNow.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding.rbLeft.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbLeft.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                if (!LiveEventFragment.isPostAllow) {
                    binding.rbOnWay.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_gray_color))
                    binding.rbHereNow.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_gray_color))
                }
                if (userVisibleHint)
                    whosHerViewModel.getWhosHere(LiveEventFragment.eventId, Constants.WHOS_HERE_FOLLOWING)
            }
            Constants.WHOS_HERE_ONTHEWAY -> {
                arrayList[0].isCheck = true
                arrayList[1].isCheck = false
                arrayList[2].isCheck = false
                arrayList[3].isCheck = false
//                arrayList[4].isCheck = false
                binding.tvNoData.visibility = View.GONE
                binding.rbFollowing.typeface = regularFont
                binding.rbOnWay.typeface = boldFont
                binding.rbHereNow.typeface = regularFont
                binding.rbLeft.typeface = regularFont
                if (!LiveEventFragment.isPostAllow)
                    return
                whosHerViewModel.getWhosHere(LiveEventFragment.eventId, Constants.WHOS_HERE_ONTHEWAY)
                binding.rbFollowing.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbFollowing.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding.rbOnWay.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding.rbOnWay.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_rb_center)
                binding.rbHereNow.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbHereNow.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding.rbLeft.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbLeft.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
            }
            Constants.WHOS_HERE_NOW -> {
                arrayList[0].isCheck = true
                arrayList[1].isCheck = false
                arrayList[2].isCheck = false
                arrayList[3].isCheck = false
//                arrayList[4].isCheck = false
                binding.tvNoData.visibility = View.GONE
                binding.rbFollowing.typeface = regularFont
                binding.rbOnWay.typeface = regularFont
                binding.rbHereNow.typeface = boldFont
                binding.rbLeft.typeface = regularFont
                if (!LiveEventFragment.isPostAllow)
                    return
                whosHerViewModel.getWhosHere(LiveEventFragment.eventId, Constants.WHOS_HERE_NOW)
                binding.rbFollowing.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbFollowing.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding.rbOnWay.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbOnWay.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding.rbHereNow.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding.rbHereNow.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_rb_default)
                binding.rbLeft.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbLeft.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
            }
            Constants.WHOS_HERE_LEFT -> {
                arrayList[0].isCheck = true
                arrayList[1].isCheck = false
                arrayList[2].isCheck = false
                arrayList[3].isCheck = false
//                arrayList[4].isCheck = false
                binding.tvNoData.visibility = View.GONE
                binding.rbFollowing.typeface = regularFont
                binding.rbOnWay.typeface = regularFont
                binding.rbHereNow.typeface = regularFont
                binding.rbLeft.typeface = boldFont
                whosHerViewModel.getWhosHere(LiveEventFragment.eventId, Constants.WHOS_HERE_LEFT)
                binding.rbFollowing.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbFollowing.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding.rbOnWay.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbOnWay.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding.rbHereNow.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_purple_color))
                binding.rbHereNow.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                binding.rbLeft.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.white))
                binding.rbLeft.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_btn_right_radio)
                if (!LiveEventFragment.isPostAllow) {
                    binding.rbOnWay.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_gray_color))
                    binding.rbHereNow.setTextColor(ContextCompat.getColor(HooleyMain.activity!!, R.color.app_gray_color))
                }
            }
        }
    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        when (checkedId) {
            R.id.rbFollowing -> {
                type = Constants.WHOS_HERE_FOLLOWING
                setTabs()
            }
            R.id.rbOnWay -> {
                type = Constants.WHOS_HERE_ONTHEWAY
                setTabs()
            }
            R.id.rbHereNow -> {
                type = Constants.WHOS_HERE_NOW
                setTabs()
            }
            R.id.rbLeft -> {
                type = Constants.WHOS_HERE_LEFT
                setTabs()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (::binding.isInitialized) {
            binding.rbFollowing.isChecked = true
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivFilter -> {
                if (!moreWhosHereDialogeFragmen!!.isAdded)
                    moreWhosHereDialogeFragmen!!.show(HooleyMain.activity!!.supportFragmentManager)
            }
            R.id.rbMale -> {
                if (whosHerViewModel.mList.value!!.size > 0) {
                    isMaleChecked = !isMaleChecked
                    if (isMaleChecked) {
                        binding.rlMale.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_selected_field)
                        binding.rbMale.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_male_white)
                    } else {
                        binding.rlMale.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                        binding.rbMale.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_male)
                    }
                    whosHerViewModel.applyGenderFilter(isMaleChecked, isFemaleChecked, isOtherChecked)

                }
            }
            R.id.rbFemale -> {
                if (whosHerViewModel.mList.value!!.size > 0) {
                    isFemaleChecked = !isFemaleChecked
                    if (isFemaleChecked) {
                        binding.rlFemale.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_rb_center)
                        binding.rbFemale.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_female_white)
                    } else {
                        binding.rlFemale.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                        binding.rbFemale.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_female)
                    }
                    whosHerViewModel.applyGenderFilter(isMaleChecked, isFemaleChecked, isOtherChecked)
                }
            }
            R.id.rbOther -> {
                if (whosHerViewModel.mList.value!!.size > 0) {
                    isOtherChecked = !isOtherChecked
                    if (isOtherChecked) {
                        binding.rlOther.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.bg_rb_center)
                        binding.rbOther.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_other_white)
                    } else {
                        binding.rlOther.setBackgroundColor(ContextCompat.getColor(HooleyMain.activity!!, android.R.color.transparent))
                        binding.rbOther.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_other)
                    }
                    whosHerViewModel.applyGenderFilter(isMaleChecked, isFemaleChecked, isOtherChecked)
                }
            }
            R.id.ivClose -> {
                binding.edtSearch.setText("")
                Util.hideKeyboard(HooleyMain.activity!!)
            }
        }

    }

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

    }

    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
    }

    override fun afterTextChanged(s: Editable) {
        try {
            whosHerViewModel.searchInList(s.toString())
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }
    }

    override fun onRefresh() {
        binding.slWhosHere.isRefreshing = true
        binding.pbFollowing.visibility = View.GONE
        binding.pbHereNow.visibility = View.GONE
        binding.pbLeft.visibility = View.GONE
        binding.pbOnWay.visibility = View.GONE
        when {
            binding.rbFollowing.isChecked -> whosHerViewModel.getWhosHere(LiveEventFragment.eventId, Constants.WHOS_HERE_FOLLOWING)
            binding.rbOnWay.isChecked -> whosHerViewModel.getWhosHere(LiveEventFragment.eventId, Constants.WHOS_HERE_ONTHEWAY)
            binding.rbHereNow.isChecked -> whosHerViewModel.getWhosHere(LiveEventFragment.eventId, Constants.WHOS_HERE_NOW)
            else -> whosHerViewModel.getWhosHere(LiveEventFragment.eventId, Constants.WHOS_HERE_LEFT)
        }
    }

    override fun onClickAddFriend(position: Int) {
        whosHerViewModel.sendFriendRequest(whosHerViewModel.mList.value!![position].userId!!.toInt())
    }

    override fun onClickItem(position: Int) {
        var friendCardFragment = CardPagerFragment(position, ArrayList(whosHerViewModel.mList.value!!))
        friendCardFragment.mListener = this
        friendCardFragment.show(HooleyMain.activity!!.supportFragmentManager)
    }

    override fun onClickMyProfile(position: Int) {
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ProfileMainFragment.newInstance(false), "ProfileMainFragment")
        hideDrawer(HooleyMain.activity!!)
    }

    override fun onClickFriendProfile(position: Int) {
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, FriendsProfileFragment.newInstance(whosHerViewModel.mList.value!![position].userId!!.toInt(), whosHerViewModel.mList.value!![position].fullName!!), "FriendsProfileFragment")
    }

    override fun onClickMessage(position: Int) {
        (HooleyMain.activity as ActivityBase).addFragment(R.id.container, ChatWindowFragment("", whosHerViewModel.mList.value!![position].userId!!, whosHerViewModel.mList.value!![position].fullName!!), "ChatWindowFragment")
    }

    override fun onClickAcceptRequest(position: Int, value: Boolean) {
        whosHerViewModel.acceptIgnoreFriendRequest(whosHerViewModel.mList.value!![position].userId!!.toInt(), value)
    }

    override fun reloadFragment(flag: Boolean) {
        whosHerViewModel.getWhosHere(LiveEventFragment.eventId, type)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
    }

}