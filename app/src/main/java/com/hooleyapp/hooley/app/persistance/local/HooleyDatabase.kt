package com.hooleyapp.hooley.app.persistance.local


import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.hooleyapp.hooley.app.data.model.BookMarkEvent
import com.hooleyapp.hooley.app.data.model.event.responseModel.PopularInFriendsModel
import com.hooleyapp.hooley.app.persistance.dao.PopularInFriendsDao

@Database(entities = [(PopularInFriendsModel.PopularInFriends::class), (BookMarkEvent::class)], version = 2, exportSchema = false)
abstract class HooleyDatabase : RoomDatabase() {

    abstract fun popularInFriendsDao(): PopularInFriendsDao
//    abstract fun (): PopularInFriendsDao


    companion object {
        @Volatile
        private var INSTANCE: HooleyDatabase? = null

        fun getDatabase(context: Context): HooleyDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        HooleyDatabase::class.java,
                        "hooley_database.db"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}