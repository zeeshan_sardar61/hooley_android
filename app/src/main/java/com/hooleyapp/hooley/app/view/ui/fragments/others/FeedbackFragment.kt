package com.hooleyapp.hooley.app.view.ui.fragments.others

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.app.viewmodel.others.FeedBackViewModel
import com.hooleyapp.hooley.databinding.FragmentFeedBackBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.removeDialog
import com.hooleyapp.hooley.helper.showDialog
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.Util

/**
 * Created by Nauman on 5/17/2018.
 */

class FeedbackFragment : BaseFragment(), View.OnClickListener {

    lateinit var binding: FragmentFeedBackBinding
    lateinit var feedBackViewModel: FeedBackViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_feed_back, container, false)
        feedBackViewModel = ViewModelProviders.of(this).get(FeedBackViewModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnSendFeedBack.setOnClickListener(this)
        setUiObservers()
    }

    private fun setUiObservers() {
        feedBackViewModel.sessionExpire.observe(this, Observer { it.let { onTokenExpireLogOut() } })

        feedBackViewModel.failureMessage.observe(this, Observer {
            it.let { t -> activity!!.showSuccessDialog(t!!) }
        })

        feedBackViewModel.loading.observe(this, Observer {
            it.let { t ->
                if (t!!)
                    activity!!.showDialog()
                else
                    activity!!.removeDialog()
            }
        })
        feedBackViewModel.success.observe(this, Observer {
            it.let { t ->
                if (t!!)
                    (activity as ActivityBase).callFragmentWithReplace(R.id.container, ConfirmFeedbackFragment(), null)
            }
        })
    }

    private fun validInput(): Boolean {
        return when {
            Util.isContentNull(binding.edtMessage.text.toString()) -> {
                binding.edtMessage.requestFocus()
                binding.edtMessage.error = getString(R.string.required)
                false
            }
            binding.rbHooley.rating == 0f -> {
                Util.showToastMessage(activity!!, "Please rate first")
                false
            }
            else -> true
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnSendFeedBack -> if (validInput()) {
                if (HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN))
                    feedBackViewModel.addGuestUserFeedBack(binding.edtMessage.text.toString(), Math.round(binding.rbHooley.rating).toString())
                else
                    feedBackViewModel.submitFeedBack(binding.edtMessage.text.toString(), Math.round(binding.rbHooley.rating).toString())
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
    }
}



