package com.hooleyapp.hooley.app.view.ui.fragments.guest

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.Indicators.PagerIndicator
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.DefaultSliderView
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventDetailModel
import com.hooleyapp.hooley.app.view.ui.activites.HooleyGuestUser
import com.hooleyapp.hooley.app.viewmodel.event.EventDetailViewModel
import com.hooleyapp.hooley.databinding.FragmentEventDetailBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.fragments.event.GuestUserLiveEventFragment
import com.hooleyapp.hooley.helper.DialogGuestUser
import com.hooleyapp.hooley.helper.showSuccessDialog
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.others.GetBitmapTask
import com.hooleyapp.hooley.others.MessageEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*
import java.util.concurrent.TimeUnit

@SuppressLint("ValidFragment")
class GuestUserEventDetailFragment constructor(var eventId: String, var objectRespons: EventDetailModel) : BaseFragment(), View.OnClickListener {

    private var eventPayment: String? = null
    internal val handler = Handler()
    internal var timer: Timer? = null
    lateinit var timerTask: TimerTask
    private lateinit var binding: FragmentEventDetailBinding
    lateinit var eventDetailViewmodel: EventDetailViewModel
    private var fullDetail: String? = null
    lateinit var countDownTimer: CountDownTimer

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser && isResumed) {
            if (::eventDetailViewmodel.isInitialized)
                eventDetailViewmodel.getEventDetail(eventId)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_event_detail, container, false)
        eventDetailViewmodel = ViewModelProviders.of(this).get(EventDetailViewModel::class.java)
        setUiObserver()
        eventDetailViewmodel.response.postValue(objectRespons)
        eventDetailViewmodel.isSectionUpdate.postValue(false)
        if (userVisibleHint)
            eventDetailViewmodel.getEventDetail(eventId)
        retainInstance = true
        binding.tvInvited.setTextColor(ContextCompat.getColor(ActivityBase.activity, R.color.text_color))
        binding.tvAttending.setTextColor(ContextCompat.getColor(ActivityBase.activity, R.color.text_color))
        binding.tveventType.setTextColor(ContextCompat.getColor(ActivityBase.activity, R.color.text_color))
        return binding.root
    }

    // setting observer
    private fun setUiObserver() {
        eventDetailViewmodel.failureMessage.observe(this,
                android.arch.lifecycle.Observer {
                    it?.let { text ->
                        ActivityBase.activity.showSuccessDialog(text)
                    }
                })
        eventDetailViewmodel.sessionExpire.observe(this,
                android.arch.lifecycle.Observer {
                    onTokenExpireLogOut()
                })
        eventDetailViewmodel.loading.observe(this, android.arch.lifecycle.Observer {
            if (it!!)
                binding.pbEventDetail.visibility = View.VISIBLE
            else
                binding.pbEventDetail.visibility = View.GONE
        })
        eventDetailViewmodel.isSectionUpdate.observe(this, android.arch.lifecycle.Observer {
            if (it!!) {
                updateSectionUi()
            } else {
                updateSectionUi()
                setData()
            }
        })
    }

    // Setting Listener
    private fun setListener() {
        binding.llFollowEvent.setOnClickListener(this)
        binding.llAttendEvent.setOnClickListener(this)
        binding.llShareEvent.setOnClickListener(this)
        binding.btnEditEvent.setOnClickListener(this)
        binding.ivMarker.setOnClickListener(this)
        binding.ivLikeEvent.setOnClickListener(this)
        binding.ivBookMarkEvent.setOnClickListener(this)
        binding.ivUnBookMarkEvent.setOnClickListener(this)
        binding.llLikeEvent.setOnClickListener(this)
        binding.ivHost.setOnClickListener(this)
        binding.llInvited.setOnClickListener(this)
        binding.llAttending.setOnClickListener(this)
        binding.llAnnouncements.setOnClickListener(this)
    }

    @SuppressLint("SetTextI18n")
    private fun updateSectionUi() {
        binding.tvInterested.text = "${eventDetailViewmodel.response.value!!.interestedCount} Interested"
        binding.tvInvited.text = "${eventDetailViewmodel.response.value!!.invitedCount} Invited"
        binding.tvAttending.text = "${eventDetailViewmodel.response.value!!.attendCount} Attending"
        binding.tvFollowing.text = "${eventDetailViewmodel.response.value!!.followersCount} Following"
        binding.tvAnnouncements.text = "Announcements ( ${eventDetailViewmodel.response.value!!.announcementCount} )"
        GuestUserLiveEventFragment.objectResult.isAttended = eventDetailViewmodel.response.value!!.isAttended
        if (eventDetailViewmodel.response.value!!.eventHashs.size > 0) {
            binding.llHash.visibility = View.VISIBLE
            for (i in eventDetailViewmodel.response.value!!.eventHashs.indices) {
                eventDetailViewmodel.response.value!!.eventHashs[i] = "#" + eventDetailViewmodel.response.value!!.eventHashs[i]
            }
            binding.tvHash.text = eventDetailViewmodel.response.value!!.eventHashs.joinToString().replace(",", "")
        } else {
            binding.llHash.visibility = View.GONE
        }
        if (eventDetailViewmodel.response.value!!.eventDisplayBasicInfo!!.isLiked) {
            binding.ivLikeEvent.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_red_heart)
            binding.llLikeEvent.setBackgroundColor(ContextCompat.getColor(ActivityBase.activity, R.color.event_tabs_selection))
        } else {
            binding.ivLikeEvent.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_purple_heart)
            binding.llLikeEvent.setBackgroundColor(ContextCompat.getColor(ActivityBase.activity, R.color.like_layer_color))
        }
        if (eventDetailViewmodel.response.value!!.isFollow) {
            binding.llFollowEvent.setBackgroundColor(ContextCompat.getColor(ActivityBase.activity, R.color.event_tabs_selection))
            binding.ivFollowEvent.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_filled_star)
        } else {
            binding.llFollowEvent.setBackgroundColor(ContextCompat.getColor(ActivityBase.activity, R.color.like_layer_color))
            binding.ivFollowEvent.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_follow_it)
        }
        if (eventDetailViewmodel.response.value!!.isAttended) {
            binding.llAttendEvent.setBackgroundColor(ContextCompat.getColor(ActivityBase.activity, R.color.event_tabs_selection))
            binding.ivAttendEvent.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_attended)
        } else {
            binding.llAttendEvent.setBackgroundColor(ContextCompat.getColor(ActivityBase.activity, R.color.like_layer_color))
            binding.ivAttendEvent.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_attend)
        }
        if (eventDetailViewmodel.response.value!!.eventDisplayBasicInfo!!.isBookmarked) {
            binding.ivBookMarkEvent.visibility = View.GONE
            binding.ivUnBookMarkEvent.visibility = View.VISIBLE
        } else {
            binding.ivBookMarkEvent.visibility = View.VISIBLE
            binding.ivUnBookMarkEvent.visibility = View.GONE
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
        binding.tvEventName.text = eventDetailViewmodel.response.value!!.eventDisplayBasicInfo!!.eventName
        if (eventDetailViewmodel.response.value!!.eventHostList!!.size > 0) {
            binding.tvHostName.text = eventDetailViewmodel.response.value!!.eventHostList!![0].fullName
        } else {
            binding.tvHostName.text = "NONAME"
        }
        binding.tvEventAddress.text = eventDetailViewmodel.response.value!!.eventDisplayBasicInfo!!.address
        binding.tvAgeGroup.text = "Minimum Age ${eventDetailViewmodel.response.value!!.minimumAge}"
        binding.ivMoreOption.visibility = View.GONE
        Glide.with(ActivityBase.activity).load(eventDetailViewmodel.response.value!!.eventCoversList[0]).into(binding.ivEventCover)
        if (!TextUtils.isEmpty(eventDetailViewmodel.response.value!!.eventDisplayBasicInfo!!.profilePic))
            Glide.with(ActivityBase.activity).load(eventDetailViewmodel.response.value!!.eventDisplayBasicInfo!!.profilePic).into(binding.ivHost)
        binding.tvEventDetail.text = eventDetailViewmodel.response.value!!.eventDisplayBasicInfo!!.eventDetails
        fullDetail = if (eventDetailViewmodel.response.value!!.eventDisplayBasicInfo!!.isPublic) {
            "Public Event"
        } else {
            "Private Event"
        }

        binding.tveventType.text = fullDetail

        eventPayment = if (eventDetailViewmodel.response.value!!.eventDisplayBasicInfo!!.eventType == "1") {
            "Free"
        } else if (eventDetailViewmodel.response.value!!.eventDisplayBasicInfo!!.eventType == "2") {
            if (eventDetailViewmodel.response.value!!.ppvPrice > 0.0) {
                "Paid - PPV"
            } else {
                "Paid"
            }
        } else {
            "Paid - Charity"
        }

        if (eventDetailViewmodel.response.value!!.eventDisplayBasicInfo!!.eventType != "1") {
            binding.tveventPaid.isEnabled = true
        } else {
            binding.tveventPaid.isEnabled = false
            binding.tveventPaid.setTextColor(ContextCompat.getColor(activity!!, R.color.text_color))
        }

        binding.tveventPaid.text = eventPayment

        binding.tveventPaid.setOnClickListener {
            HooleyGuestUser.activity.DialogGuestUser()
        }
        if (eventDetailViewmodel.response.value!!.isLive) {
            startReverseTime()
            binding.rlLive.visibility = View.VISIBLE
        } else {
            binding.rlLive.visibility = View.GONE
        }

        if (eventDetailViewmodel.response.value!!.eventCoversList.size > 0)
            initSlider()
        setEventCategory()
        setTimeStamp()
        setCoHostRv()
        setListener()
    }

    private fun initSlider() {
        binding.EvenCoverSlider.stopAutoCycle()
        val urlMaps = HashMap<String, String>()
        for (loop in eventDetailViewmodel.response.value!!.eventCoversList.indices) {
            urlMaps["" + loop] = eventDetailViewmodel.response.value!!.eventCoversList[loop].photoUrl!!
        }
        for (name in urlMaps.keys) {
            val textSliderView = DefaultSliderView(ActivityBase.activity)
            textSliderView
                    .description("")
                    .image(urlMaps[name]).scaleType = BaseSliderView.ScaleType.CenterCrop
//                    .setOnSliderClickListener(this)
            //add your extra information
            textSliderView.bundle(Bundle())
            textSliderView.bundle.putString("extra", name)
            binding.EvenCoverSlider.addSlider(textSliderView)
        }

        val handler = Handler()
        handler.postDelayed({
            binding.EvenCoverSlider.startAutoCycle()
            binding.EvenCoverSlider.indicatorVisibility = PagerIndicator.IndicatorVisibility.Invisible
            binding.EvenCoverSlider.setPresetTransformer(SliderLayout.Transformer.Fade)
            binding.EvenCoverSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom)
            binding.EvenCoverSlider.setCustomAnimation(DescriptionAnimation())
            binding.EvenCoverSlider.setDuration(3000)
        }, 1500)

    }

    private fun startReverseTime() {
        if (::countDownTimer.isInitialized)
            countDownTimer.cancel()
        val testMilemillis = DateUtils.GetTimeForLiveEvent(DateUtils.getLocalDate(eventDetailViewmodel.response.value!!.eventDisplayBasicInfo!!.endTime!!))
        countDownTimer = object : CountDownTimer((testMilemillis), 100) {
            override fun onTick(leftTimeInMilliseconds: Long) {
                var millis = leftTimeInMilliseconds
                var hms = if (TimeUnit.MILLISECONDS.toDays(millis) > 0) {
                    String.format("%2d", TimeUnit.MILLISECONDS.toDays(millis)) + "d - " +
                            String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)))
                } else {
                    String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)))
                }
                binding.tvLiveTimer.text = hms
            }

            override fun onFinish() {
                binding.tvLiveTimer.text = "00:00"
            }
        }
        countDownTimer.start()
    }

    private fun startTimer() {
        timer = Timer()
        initializeTimerTask()
        timer!!.schedule(timerTask, 100, 1000) //
    }

    private fun stopTimerTask() {
        if (timer != null) {
            timer!!.cancel()
            timer = null
        }
    }

    private fun initializeTimerTask() {
        timerTask = object : TimerTask() {
            override fun run() {
                //use a handler to run a toast that shows the current timestamp
                handler.post {
                    val millis = DateUtils.GetTimeForLiveEvent(DateUtils.getLocalDate(eventDetailViewmodel.response.value!!.eventDisplayBasicInfo!!.startTime!!))
                    val hms: String
                    hms = if (TimeUnit.MILLISECONDS.toDays(millis) > 0) {
                        String.format("%2d", TimeUnit.MILLISECONDS.toDays(millis)) + "d - " +
                                String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)))
                    } else {
                        String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)))

                    }
                    binding.tvLiveTimer.text = hms
                }
            }
        }
    }

    private fun setCoHostRv() {
        if (eventDetailViewmodel.response.value!!.eventCoHostList != null && eventDetailViewmodel.response.value!!.eventCoHostList!!.size > 0) {
            binding.llCoHosts.visibility = View.VISIBLE

            for (i in eventDetailViewmodel.response.value!!.eventCoHostList!!.indices) {
                when (i) {
                    0 -> binding.tvCohost1.text = eventDetailViewmodel.response.value!!.eventCoHostList!![i].fullName
                    1 -> {
                        binding.tvCohost2.visibility = View.VISIBLE
                        binding.tvCohost2.text = ", " + eventDetailViewmodel.response.value!!.eventCoHostList!![i].fullName
                    }
                    2 -> {
                        binding.tvCohost3.visibility = View.VISIBLE
                        binding.tvCohost3.text = ", " + eventDetailViewmodel.response.value!!.eventCoHostList!![i].fullName
                    }
                }
            }
        } else {
            binding.llCoHosts.visibility = View.GONE
        }
    }

    private fun setTimeStamp() {
        binding.tvEventDate.text = DateUtils.convertDateNew(DateUtils.getLocalDate(eventDetailViewmodel.response.value!!.eventDisplayBasicInfo!!.startTime!!), DateUtils.getLocalDate(eventDetailViewmodel.response.value!!.eventDisplayBasicInfo!!.endTime!!))
    }

    private fun setEventCategory() {
        var category: String? = null
        for (i in eventDetailViewmodel.response.value!!.eventCategoryList!!.indices) {
            category = if (i == 0) {
                eventDetailViewmodel.response.value!!.eventCategoryList!![i].categoryName
            } else {
                category + ", " + eventDetailViewmodel.response.value!!.eventCategoryList!![i].categoryName
            }
        }
        binding.tvCategory.text = category
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.llFollowEvent -> HooleyGuestUser.activity.DialogGuestUser()
            R.id.llAttendEvent -> HooleyGuestUser.activity.DialogGuestUser()
            R.id.llShareEvent -> showShareEventDialog()
            R.id.btnEditEvent -> HooleyGuestUser.activity.DialogGuestUser()
            R.id.ivMarker -> HooleyGuestUser.activity.DialogGuestUser()
            R.id.llLikeEvent -> HooleyGuestUser.activity.DialogGuestUser()
            R.id.ivBookMarkEvent -> eventDetailViewmodel.addGuestUserBookMarkEvent(eventId, false)
            R.id.ivUnBookMarkEvent -> eventDetailViewmodel.addGuestUserBookMarkEvent(eventId, true)
            R.id.llAnnouncements -> (HooleyGuestUser.activity as ActivityBase).callFragmentWithReplace(R.id.container, GuestUserEventDetailAnnouncementFragment(eventId, eventDetailViewmodel.response.value!!.isMyEvent), "EventDetailAnnouncementFragment")

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        eventDetailViewmodel.shareEvent(event.message, GuestUserLiveEventFragment.eventId)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    private fun showShareEventDialog() {
        object : GetBitmapTask() {
            override fun onPostExecute(bmp: Bitmap) {
                super.onPostExecute(bmp)
                //                HooleyMain.removeDialog();
                (HooleyGuestUser.activity as HooleyGuestUser).shareWithImage(bmp, eventDetailViewmodel.response.value!!.eventDisplayBasicInfo!!.eventName!!, Constants.EVENT_SHARE + eventId, DateUtils.formatDateNew(eventDetailViewmodel.response.value!!.eventDisplayBasicInfo!!.startTime!!))
            }
        }.execute(eventDetailViewmodel.response.value!!.eventCoversList[0].photoUrl)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
//        stopTimerTask()
        if (::countDownTimer.isInitialized)
            countDownTimer.cancel()
    }
}
