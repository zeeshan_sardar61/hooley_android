package com.hooleyapp.hooley.app.data.model.event.responseModel.eventDetailNew

data class Checks(
        val isBookmarked: Boolean? = null,
        val isPublished: Boolean? = null,
        val isLiked: Boolean? = null,
        val isPostAllow: Boolean? = null,
        val isInvite: Boolean? = null,
        val isFollow: Boolean? = null,
        val isPromoted: Boolean? = null,
        val isAttended: Boolean? = null,
        val isMyEvent: Boolean? = null,
        val isLive: Boolean? = null,
        val isCanceledEvent: Boolean? = null,
        val isTicketSetup: Boolean? = null,
        val isPublic: Boolean? = null,
        val isPastEvent: Boolean? = null,
        val isPpvPurchased: Boolean? = null
)
