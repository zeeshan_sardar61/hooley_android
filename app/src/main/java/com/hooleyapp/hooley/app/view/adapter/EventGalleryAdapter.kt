package com.hooleyapp.hooley.app.view.adapter

import android.annotation.SuppressLint
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.adapters.AdapterPostMediaPager
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.databinding.AdapterAllEventFeedsBinding
import com.hooleyapp.hooley.fragments.others.ViewAvatarFragment
import com.hooleyapp.hooley.helper.load
import com.hooleyapp.hooley.interfaces.IEventAllFeedsClick
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils

/**
 * Created by Zeeshan on 18-Jan-18.
 */

class EventGalleryAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(), AdapterPostMediaPager.IClickPostListener {
    internal var mList = arrayListOf<EventGalleryFeedModel.EventMediaInfo>()
    var mListener: IEventAllFeedsClick? = null
    private var adapter: AdapterPostMediaPager? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding: AdapterAllEventFeedsBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_all_event_feeds, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            holder.bindHolderViewData(mList[position])
            initPager(holder, mList[position].mediaFiles, position)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        //Setting Listener
        holder.binding.ivPostMore.setOnClickListener { mListener!!.onClickMore(mList[position], position, holder.binding.vpMediaPost.currentItem, Constants.TYPE_EVENT_FEED_ALL) }
        holder.binding.llComment.setOnClickListener { mListener!!.onClickPostComment(mList[position], Constants.TYPE_EVENT_FEED_ALL) }
        holder.binding.llFav.setOnClickListener {
            mListener!!.onClickFav(mList[position], position, Constants.TYPE_EVENT_FEED_ALL)
            if (HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN))
                return@setOnClickListener
            if (mList[position].isFavorited) {
                mList[position].isFavorited = false
                mList[position].favoriteCount--
            } else {
                mList[position].isFavorited = true
                mList[position].favoriteCount++
            }
            notifyDataSetChanged()
        }
        holder.binding.llLike.setOnClickListener {
            mListener!!.onClickLikePost(mList[position], position, Constants.TYPE_EVENT_FEED_ALL)
            if (HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN))
                return@setOnClickListener
            if (mList[position].isLiked) {
                mList[position].isLiked = false
                mList[position].likesCount--
            } else {
                mList[position].isLiked = true
                mList[position].likesCount++
            }
            notifyDataSetChanged()
        }
        holder.binding.llShare.setOnClickListener { mListener!!.onClickShare(mList[position].imageId, position, holder.binding.vpMediaPost.currentItem, Constants.TYPE_EVENT_FEED_ALL) }
        holder.binding.ivUserProfile.setOnClickListener {
            if (HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN))
                return@setOnClickListener
            var avatarfragment = ViewAvatarFragment(mList[position].userId!!.toInt())
            avatarfragment.show(ActivityBase.activity.supportFragmentManager)
        }

    }

    private fun initPager(holder: ItemViewHolder, mediaFiles: ArrayList<EventGalleryFeedModel.MediaFile>, position: Int) {
        adapter = AdapterPostMediaPager(mediaFiles, position)
        adapter!!.setMListener(this)
        holder.binding.vpMediaPost.adapter = adapter
        holder.binding.tlMediaPost.setupWithViewPager(holder.binding.vpMediaPost, true)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onClickPost(position: Int, listPosition: Int) {
        mListener!!.onClickImageView(mList[listPosition])
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var binding: AdapterAllEventFeedsBinding = DataBindingUtil.bind(v)!!

        @Throws(NullPointerException::class)
        @SuppressLint("SetTextI18n")
        fun bindHolderViewData(eventInfo: EventGalleryFeedModel.EventMediaInfo) {
            if (!TextUtils.isEmpty(eventInfo.profilePic))
                binding.ivUserProfile.load(eventInfo.profilePic)
            binding.tvFullname.text = eventInfo.fullName
            if (TextUtils.isEmpty(eventInfo.imageCaption)) {
                binding.tvPostDetail.visibility = View.GONE
            } else {
                binding.tvPostDetail.visibility = View.VISIBLE
                binding.tvPostDetail.text = eventInfo.imageCaption
            }
            if (eventInfo.mediaFiles.size > 1) {
                binding.tlMediaPost.visibility = View.VISIBLE
            } else {
                binding.tlMediaPost.visibility = View.GONE
            }
            binding.tvPostTime.text = DateUtils.convertDateToTimeAgo(DateUtils.getLocalDate(eventInfo.postedTime))
            binding.tvLike.text = Integer.toString(eventInfo.likesCount)
            binding.tvComment.text = Integer.toString(eventInfo.commentsCount)
            binding.tvShare.text = Integer.toString(eventInfo.shareCounts)
            binding.tvFav.text = Integer.toString(eventInfo.favoriteCount)

            if (eventInfo.isLiked) {
                binding.ivLike.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_red_heart)
            } else {
                binding.ivLike.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_purple_heart)
            }

            if (eventInfo.isFavorited) {
                binding.ivFav.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_fav)
            } else {
                binding.ivFav.background = ContextCompat.getDrawable(ActivityBase.activity, R.drawable.ic_un_fav)
            }

            if (eventInfo.partialComments.size > 0) {
                binding.tvSingleComment.visibility = View.VISIBLE
                val sourceString = "<b>" + eventInfo.partialComments[0].fullName + "</b> " + eventInfo.partialComments[0].commentText
                binding.tvSingleComment.text = Html.fromHtml(sourceString)
                binding.tvSingleComment.setOnClickListener { mListener!!.onClickPostComment(eventInfo, Constants.TYPE_EVENT_FEED_ALL) }
            } else {
                binding.tvSingleComment.visibility = View.GONE
            }
        }
    }

    internal fun setList(list: ArrayList<EventGalleryFeedModel.EventMediaInfo>) {
        this.mList = list
        notifyDataSetChanged()
    }

}
