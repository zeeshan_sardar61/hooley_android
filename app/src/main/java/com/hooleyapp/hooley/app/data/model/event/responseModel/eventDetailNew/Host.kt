package com.hooleyapp.hooley.app.data.model.event.responseModel.eventDetailNew

data class Host(
        val profilePic: String? = null,
        val fullName: String? = null,
        val userId: Int? = null
)
