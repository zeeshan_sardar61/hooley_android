package com.hooleyapp.hooley.app.data.repository.event

import android.arch.lifecycle.MutableLiveData
import com.hooleyapp.hooley.app.data.repository.BaseRepository
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GetChatStreamModel
import com.hooleyapp.hooley.services.EventWebService

class ChatStreamRepository : BaseRepository() {

    var mList: MutableLiveData<ArrayList<GetChatStreamModel.ChatStream>> = MutableLiveData()
    var service = EventWebService()

    fun getMyChatStreamMessages(eventId: String) {
        loading.postValue(true)
        service.getMyChatStreamMessages(eventId, object : GenericCallback<GetChatStreamModel> {
            override fun success(result: GetChatStreamModel) {
                loading.postValue(false)
                mList.postValue(null)
                mList.postValue(result.chatStreamList)
            }

            override fun failure(message: String) {
                loading.postValue(false)
                failureMessage.postValue(message)
            }
        })
    }

    fun sendTextStreamMessage(eventId: String, msg: String) {
        service.sendStreamMessage(eventId, msg, object : IWebServiceCallback<GeneralModel> {
            override fun success(result: GeneralModel) {
            }

            override fun failure(message: String) {
                failureMessage.postValue(message)
            }

            override fun onTokenExpire() {
                sessionExpire.postValue(true)
            }
        })
    }

}