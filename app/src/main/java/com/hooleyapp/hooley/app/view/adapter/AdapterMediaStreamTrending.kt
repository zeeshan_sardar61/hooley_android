package com.hooleyapp.hooley.app.view.adapter

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.felipecsl.asymmetricgridview.AGVRecyclerViewAdapter
import com.felipecsl.asymmetricgridview.AsymmetricItem
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.databinding.AdapterEventFeedTrendingBinding
import com.hooleyapp.hooley.interfaces.IEventAllFeedsClick
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.GalleryItem
import java.util.*

/**
 * Created by Nauman on 3/22/2018.
 */

class AdapterMediaStreamTrending(private val context: Context, internal var mList: ArrayList<EventGalleryFeedModel.EventMediaInfo>?, private val items: List<GalleryItem>) : AGVRecyclerViewAdapter<RecyclerView.ViewHolder>() {
    private var mListener: IEventAllFeedsClick? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<AdapterEventFeedTrendingBinding>(LayoutInflater.from(parent.context), R.layout.adapter_event_feed_trending, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder

        if (mList!![position].mediaFiles.size > 1) {
            holder.binding.rlMultiple.visibility = View.VISIBLE
        } else {
            holder.binding.rlMultiple.visibility = View.GONE
        }

//        if (mList!![position].isVideo) {
//            if (mList!![position].thumbnailUrl != null)
//                Glide.with(context).load(mList!![position].thumbnailUrl).into(holder.userSearchbinding!!.ivTrending)
//            holder.userSearchbinding!!.sdvPostImage.visibility = View.GONE
//            holder.userSearchbinding!!.ivTrending.visibility = View.VISIBLE
//            holder.userSearchbinding!!.ivMovie.visibility = View.VISIBLE
//            holder.userSearchbinding!!.ivPicture.visibility = View.GONE
//        } else {
//            if (mList!![position].thumbnailUrl != null) {
//                val imageUri = Uri.parse(mList!![position].thumbnailUrl)
//                holder.userSearchbinding!!.sdvPostImage.setImageURI(imageUri)
//            } else if (mList!![position].imageUrl != null) {
//                val imageUri = Uri.parse(mList!![position].imageUrl)
//                holder.userSearchbinding!!.sdvPostImage.setImageURI(imageUri)
//            }
//            holder.userSearchbinding!!.ivTrending.visibility = View.GONE
//            holder.userSearchbinding!!.sdvPostImage.visibility = View.VISIBLE
//            holder.userSearchbinding!!.ivPicture.visibility = View.VISIBLE
//            holder.userSearchbinding!!.ivMovie.visibility = View.GONE
//        }
        if (mList!![position].isFavorited) {
            holder.binding.ivFav.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_fav_orrange)
        } else {
            holder.binding.ivFav.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_unfav_orange)
        }

        holder.binding.sdvPostImage.setOnClickListener { mListener!!.onClickImageView(mList!![position]) }

        holder.binding.ivFav.setOnClickListener { mListener!!.onClickFav(mList!![position], position, Constants.TYPE_EVENT_FEED_TRENDING) }

        holder.binding.ivTrending.setOnClickListener {

            //            if (mList!![position].videoUrl != null)
//                (HooleyMain.activity as ActivityBase).callFragment(R.id.container, VideoPlayerActivity.newInstance(mList!![position].videoUrl), "VideoPlayerActivity")
        }


    }


    //    @Override
    //    public int getItemViewType(int position) {
    //        return position % 2 == 0 ? 1 : 0;
    //    }

    override fun getItem(position: Int): AsymmetricItem {
        return items[position]
    }

    override fun getItemCount(): Int {
        return if (mList == null) 0 else mList!!.size
    }

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var binding: AdapterEventFeedTrendingBinding = DataBindingUtil.bind(v)!!
    }

    fun setmListener(listener: IEventAllFeedsClick) {
        this.mListener = listener
    }
}
