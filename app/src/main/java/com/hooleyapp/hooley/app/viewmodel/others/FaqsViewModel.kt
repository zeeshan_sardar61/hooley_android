package com.hooleyapp.hooley.app.viewmodel.others

import android.app.Application
import android.app.UiModeManager
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.others.Constants

class FaqsViewModel(application: Application) : AndroidViewModel(application) {

    var pageUrl: MutableLiveData<String> = MutableLiveData()

    init {
        var url = if (HooleyApp.db.getInt(Constants.TYPE_THEME) == UiModeManager.MODE_NIGHT_YES) {
            Constants.FAQS_URL + "true"
        } else {
            Constants.FAQS_URL + "false"
        }
        pageUrl.postValue(url)
    }

}