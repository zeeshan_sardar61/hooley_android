package com.hooleyapp.hooley.app.viewmodel.event

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.hooleyapp.hooley.app.data.repository.event.WhosHereRepository
import com.hooleyapp.hooley.helper.SingleLiveEvent
import com.hooleyapp.hooley.model.WhosHereModel
import com.hooleyapp.hooley.others.Constants
import java.util.*
import kotlin.collections.ArrayList

class WhosHereViewModel(application: Application) : AndroidViewModel(application) {

    var repository = WhosHereRepository()
    var mList: SingleLiveEvent<ArrayList<WhosHereModel.User>> = SingleLiveEvent()
    var failureMessage: SingleLiveEvent<String> = SingleLiveEvent()
    var sessionExpire: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var loading: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var reloadApi: SingleLiveEvent<Boolean> = SingleLiveEvent()

    init {
        mList = repository.mList
        failureMessage = repository.failureMessage
        sessionExpire = repository.sessionExpire
        loading = repository.loading
        reloadApi = repository.reloadApi
    }

    /**
     *
     * @param eventId
     * @param type
     */
    fun getWhosHere(eventId: String, type: String) {
        repository.getWhosHere(eventId, type)
    }

    fun sendFriendRequest(friendId: Int) {
        repository.sendFriendRequest(friendId)
    }

    fun acceptIgnoreFriendRequest(userId: Int, isAccepted: Boolean) {
        repository.acceptIgnoreFriendRequest(userId, isAccepted)
    }


    fun applyGenderFilter(isMaleChecked: Boolean, isFemaleChecked: Boolean, isOtherChecked: Boolean) {
        var sortingList = arrayListOf<WhosHereModel.User>()
        if (isMaleChecked || isFemaleChecked || isOtherChecked) {
            sortingList.addAll(if (isMaleChecked) ArrayList(maleFilterList()) else ArrayList())
            sortingList.addAll(if (isFemaleChecked) ArrayList(femaleFilterList()) else ArrayList())
            sortingList.addAll(if (isOtherChecked) ArrayList(otherFilterList()) else ArrayList())
        } else {
            sortingList.addAll(repository.tempList.value!!)
        }
        mList.postValue(sortingList)
    }


    /**
     * Searching String in List
     * @param s
     * @param type
     */
    fun searchInList(s: String) {
        if (repository.tempList.value == null)
            return
        var list = repository.tempList.value?.filter {
            it.fullName!!.contains(s, true) || (it.userIntrestList!!.any { it.contains(s, true) })
        }
        mList.postValue(ArrayList(list))
    }

    // filtering
    fun otherFilterList(): List<WhosHereModel.User>? {
        return repository.tempList.value?.filter {
            it.gender == Constants.GENDER_OTHER
        }
    }

    fun femaleFilterList(): List<WhosHereModel.User>? {
        return repository.tempList.value?.filter { it.gender == Constants.GENDER_FEMALE }
    }

    fun maleFilterList(): List<WhosHereModel.User>? {
        return repository.tempList.value?.filter {
            it.gender == Constants.GENDER_MALE
        }
    }

    // Sorting
    fun sortByAlphabetical() {
        if (repository.tempList.value != null) {
            Collections.sort(repository.tempList.value, WhosHereModel.User.sortByNameComparator)
            mList.postValue(repository.tempList.value)
        }
    }

    fun sortByHooleyFriends() {
//        Collections.sort(repository.tempList.value, WhosHereModel.User.sortByHooleyFriend)
        if (repository.tempList.value != null) {
            repository.tempList.value!!.sortByDescending { it.status.equals("friend") }
            mList.postValue(repository.tempList.value)
        }
    }

    fun sortByShareInterest() {
        if (repository.tempList.value != null) {
            Collections.sort(repository.tempList.value, WhosHereModel.User.sortByShareInterest)
            mList.postValue(repository.tempList.value)
        }
    }

    fun sortByCurrentTime() {
        Collections.sort(repository.tempList.value, WhosHereModel.User.sortByTimeComparator)
        mList.postValue(repository.tempList.value)
    }

    fun sortByCity() {
        if (repository.tempList.value != null) {
            Collections.sort(repository.tempList.value, WhosHereModel.User.sortByCityComparator)
            mList.postValue(repository.tempList.value)
        }
    }

}