package com.hooleyapp.hooley.app.view.ui.activites

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.UiModeManager
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.app.view.adapter.GuestUserMenuItemsAdapter
import com.hooleyapp.hooley.app.view.ui.fragments.guest.GuestUserBookmarkEventsFragment
import com.hooleyapp.hooley.app.view.ui.fragments.guest.GuestUserNearByFragment
import com.hooleyapp.hooley.app.view.ui.fragments.guest.GuestUserSearchFilterFragment
import com.hooleyapp.hooley.app.view.ui.fragments.guest.GuestUserSearchResultFragment
import com.hooleyapp.hooley.app.view.ui.fragments.others.AboutHooleyFragment
import com.hooleyapp.hooley.app.view.ui.fragments.others.FaqsFragment
import com.hooleyapp.hooley.app.view.ui.fragments.others.FeedbackFragment
import com.hooleyapp.hooley.app.view.ui.fragments.others.PrivacyPolicyFragment
import com.hooleyapp.hooley.fragments.event.EventPagerFragment
import com.hooleyapp.hooley.fragments.event.GuestUserLiveEventFragment
import com.hooleyapp.hooley.fragments.others.TermsAndConditionFragment
import com.hooleyapp.hooley.helper.*
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IMenuItem
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.staticData.StaticDataModel
import com.hooleyapp.hooley.others.ApplicationSelectorReceiver
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.services.AuthWebService

class HooleyGuestUser : ActivityBase(), IMenuItem, LocationListener {

    lateinit var binding: com.hooleyapp.hooley.databinding.ActivityHooleyGuestBinding
    private var resideMenu: ResideLayout? = null
    private var adapter: GuestUserMenuItemsAdapter? = null
    internal var handler = Handler()
    private var locationManager: LocationManager? = null
    private var gson = Gson()
    lateinit var notificationManager: NotificationManager
    var service = AuthWebService()
    internal lateinit var umm: UiModeManager

    private val navigationBarHeight: Int
        get() {
            val resources = resources
            val resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android")
            return if (resourceId > 0) {
                resources.getDimensionPixelSize(resourceId)
            } else 0
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        umm = getSystemService(Context.UI_MODE_SERVICE) as UiModeManager
//        umm.nightMode = HooleyApp.db.getInt(Constants.TYPE_THEME, 2)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_hooley_guest)
        activity = appActivity
        setToolBar()
        setMenuItems()
        handleNavigationBar()
        setUpLocationServices()
        addBackStackListener()
        getStaticData()
        callFragment(R.id.container, GuestUserNearByFragment(), null)

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setToolBar() {
        setStatusBarGradiant(0)
        binding.toolbar.title = ""
        setSupportActionBar(binding.toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_menu)

    }

    fun setToolbarTitle(strTitle: String) {
        binding.toolbarTitle.text = strTitle
    }

    private fun setUpLocationServices() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        try {
            locationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this)
            locationManager!!.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, this)
        } catch (ewz: SecurityException) {

        }

    }


    override fun onBackPressed() {
        val f = supportFragmentManager.findFragmentById(R.id.container)
        when (f) {
            is EventPagerFragment -> {//the fragment on which you want to handle your back press
                HooleyApp.db.putBoolean(Constants.OPEN_DRAWER, true)
                try {
                    supportActionBar?.show()
                    supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_menu)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                super.onBackPressed()
            }
            is GuestUserLiveEventFragment -> {//the fragment on which you want to handle your back press
                HooleyApp.db.putBoolean(Constants.OPEN_DRAWER, true)
                try {
//                    supportActionBar?.show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                super.onBackPressed()
            }
            else -> super.onBackPressed()
        }
    }

    private fun addBackStackListener() {
        supportFragmentManager.addOnBackStackChangedListener {
            val manager = supportFragmentManager
            if (manager != null) {
                val container = manager.findFragmentById(R.id.container)
                val fragmentName = container!!.javaClass.name.split(".").last()
                // Setting toolbar titles
                if (fragmentName.getTitleName() == "") {
                    when (container) {
                        is GuestUserNearByFragment -> {
                            container.setToolbar()
                        }
                        is GuestUserLiveEventFragment -> {
                            supportActionBar?.show()
                            container.setToolBar()
                        }
                        is EventPagerFragment -> {
                            supportActionBar?.hide()
                        }
                    }
                } else {
                    setToolbarTitle(fragmentName.getTitleName())
                }
                // perform actions
                when (container) {
//                    is TicketPagerFragment -> {
////                        supportActionBar?.show()
//                    }
                }
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menu.clear()
        menuInflater.inflate(R.menu.menu_guest_user, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                if (supportFragmentManager.findFragmentById(R.id.container) is GuestUserNearByFragment) {
                    resideMenu!!.openMenu(ResideLayout.DIRECTION_LEFT)
                } else {
                    onBackPressed()
                }
                return true
            }
            R.id.ic_search -> {
                activity.hideKeyboard()
                if (supportFragmentManager.findFragmentById(R.id.container) is GuestUserSearchFilterFragment)
                    return false
                if (supportFragmentManager.findFragmentById(R.id.container) is GuestUserSearchResultFragment)
                    onBackPressed()
                (activity as ActivityBase).addFragment(R.id.container, GuestUserSearchFilterFragment.newInstance(), "GuestUserSearchFilterFragment")
                hideDrawer(activity)
//                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onLocationChanged(location: Location) {
        Logger.d("Latitude : " + location.latitude + ", Longitude:" + location.longitude)
        HooleyApp.db.putDouble(Constants.CURRENT_LAT, location.latitude)
        HooleyApp.db.putDouble(Constants.CURRENT_LANG, location.longitude)
    }

    override fun onProviderDisabled(provider: String) {
        Logger.d("Latitude => " + "disable")
    }

    override fun onProviderEnabled(provider: String) {
        Logger.d("Latitude =>" + "enable")
    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
        Logger.d("Latitude => " + "status")
    }

    private fun handleNavigationBar() {
        // Set up UI according to resolution
        when {
            hasNavBar() -> binding.container.setPadding(0, 0, 0, navigationBarHeight)
            Util.hasSoftKeys(activity) -> binding.container.setPadding(0, 0, 0, navigationBarHeight)
            else -> binding.container.setPadding(0, 0, 0, 0)
        }
        val decorView = window.decorView
        decorView.setOnSystemUiVisibilityChangeListener { visibility ->
            when (visibility) {
                4 -> when {
                    hasNavBar() -> binding.container.setPadding(0, 0, 0, navigationBarHeight)
                    Util.hasSoftKeys(activity) -> binding.container.setPadding(0, 0, 0, navigationBarHeight)
                    else -> binding.container.setPadding(0, 0, 0, 0)
                }
                0 -> when {
                    hasNavBar() -> binding.container.setPadding(0, 0, 0, navigationBarHeight)
                    Util.hasSoftKeys(activity) -> binding.container.setPadding(0, 0, 0, navigationBarHeight)
                    else -> binding.container.setPadding(0, 0, 0, 0)
                }
                2 -> binding.container.setPadding(0, 0, 0, 0)
            }
        }
    }

    private fun hasNavBar(): Boolean {
        val id = applicationContext.resources.getIdentifier("config_showNavigationBar", "bool", "android")
        return id > 0 && applicationContext.resources.getBoolean(id)
    }

    private fun setMenuItems() {
        resideMenu = ResideLayout(activity, R.layout.layout_guest_user_side_menu)
        resideMenu!!.setBackground(R.drawable.bg_tour_header)
        resideMenu!!.attachToActivity(activity)
        resideMenu!!.setScaleValue(0.7f)
        resideMenu!!.setSwipeDirectionDisable(ResideLayout.DIRECTION_RIGHT)
        resideMenu!!.setSwipeDirectionDisable(ResideLayout.DIRECTION_LEFT)
        val leftMenu = resideMenu!!.leftMenuView!!
        val ivClose = leftMenu.findViewById<ImageView>(R.id.ivClose)
        val tvLogin = leftMenu.findViewById<TextView>(R.id.tvLogin)
        val rvMenuItems = leftMenu.findViewById<RecyclerView>(R.id.rvMenuItems)
        tvLogin.setOnClickListener {
            HooleyGuestUser.activity.DialogGuestUser()
        }
        ivClose.setOnClickListener { resideMenu!!.closeMenu() }
        val llManager = LinearLayoutManager(applicationContext)
        rvMenuItems.layoutManager = llManager
        adapter = GuestUserMenuItemsAdapter(applicationContext)
        adapter!!.mListener = this
        rvMenuItems.adapter = adapter
    }

    override fun onMenuItemClick(position: Int) {
        resideMenu!!.closeMenu()
        handler.postDelayed({ mPerformAction(position) }, 200)
    }


    private fun mPerformAction(position: Int) {
        when (position) {
            0 -> {
                callFragment(R.id.container, GuestUserBookmarkEventsFragment.newInstance(), "GuestUserBookmarkEventsFragment")
                hideDrawer(activity)
            }

            1 -> {
                callFragment(R.id.container, TermsAndConditionFragment.newInstance(true, false), "TermsAndConditionFragment")
                hideDrawer(activity)
            }
            2 -> {
                callFragment(R.id.container, PrivacyPolicyFragment(), "PrivacyPolicyFragment")
                hideDrawer(activity)
            }

            3 -> {
                callFragment(R.id.container, FeedbackFragment(), "FeedbackFragment")
                hideDrawer(activity)
            }
            4 -> {
                callFragment(R.id.container, FaqsFragment(), "FaqsFragment")
                hideDrawer(activity)
            }
            5 -> {
                callFragment(R.id.container, AboutHooleyFragment(), "AboutHooleyFragment")
                hideDrawer(activity)
            }
        }
    }

    private fun getStaticData() {
        notificationManager = activity.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
        service.getStaticData(HooleyApp.db.getString(Constants.USER_ID), "2", object : GenericCallback<StaticDataModel> {
            override fun success(data: StaticDataModel) {
                HooleyApp.db.putString(Constants.STATIC_MODEL, gson.toJson(data))
            }

            override fun failure(message: String) {
            }
        })
    }

    fun shareWithImage(bitmap: Bitmap, eventName: String, imgCaption: String, Date: String) {
        val bitmapPath = MediaStore.Images.Media.insertImage(activity.contentResolver, bitmap, "Thumb", null)
        val bitmapUri = Uri.parse(bitmapPath)
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "image/png"
        val receiver = Intent(activity, ApplicationSelectorReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(activity, 0, receiver, PendingIntent.FLAG_UPDATE_CURRENT)
        intent.putExtra(Intent.EXTRA_STREAM, bitmapUri)
        intent.putExtra(Intent.EXTRA_TEXT,
                imgCaption
                        + "\n" + eventName
                        + "\nStart Date = " + Date)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            startActivity(Intent.createChooser(intent, null, pendingIntent.intentSender))
        } else {
            startActivity(Intent.createChooser(intent, "Share"))
        }
    }

    fun shareWithImage(bitmap: Bitmap, eventName: String, imgCaption: String, eventId: String, postId: String) {
        val bitmapPath = MediaStore.Images.Media.insertImage(activity.contentResolver, bitmap, "Thumb", null)
        val bitmapUri = Uri.parse(bitmapPath)
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "image/png"
        val receiver = Intent(activity, ApplicationSelectorReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(activity, 0, receiver, PendingIntent.FLAG_UPDATE_CURRENT)
        intent.putExtra(Intent.EXTRA_STREAM, bitmapUri)
        intent.putExtra(Intent.EXTRA_TEXT,
                imgCaption + "\n " +
                        Constants.POST_SHARE + postId + "&eventId=" + eventId)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            startActivity(Intent.createChooser(intent, null, pendingIntent.intentSender))
        } else {
            startActivity(Intent.createChooser(intent, "Share"))
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()

    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var activity: AppCompatActivity
    }
}