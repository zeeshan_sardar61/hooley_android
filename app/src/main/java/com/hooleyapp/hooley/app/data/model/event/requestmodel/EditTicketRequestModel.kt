package com.hooleyapp.hooley.app.data.model.event.requestmodel

import com.google.gson.annotations.SerializedName

data class EditTicketRequest(

        @field:SerializedName("eventId")
        val eventId: String? = null,

        @field:SerializedName("ticketList")
        val ticketList: List<TicketListItem?>? = null,

        @field:SerializedName("ppvPrice")
        val ppvPrice: String? = null,

        @field:SerializedName("userId")
        val userId: String? = null
)

data class EnableDisablePPV(

        @field:SerializedName("eventId")
        val eventId: String? = null,

        @field:SerializedName("isEnable")
        val isEnable: Boolean? = null,

        @field:SerializedName("userId")
        val userId: String? = null
)