package com.hooleyapp.hooley.app.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.interfaces.IMenuItem

class GuestUserMenuItemsAdapter(private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var view: View? = null

    lateinit var mListener: IMenuItem
    internal var menuItemsTitles: Array<String>? = arrayOf("Bookmarks", "Terms of use", "Privacy Policy ", "FeedBack", "FAQ's", "About")
    internal var icon = intArrayOf(R.drawable.ic_menu_book_mark, R.drawable.ic_menu_terms, R.drawable.ic_privacy_policy, R.drawable.ic_feedback_tab, R.drawable.ic_menu_guide, R.drawable.ic_menu_about)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        try {
            view = LayoutInflater.from(context).inflate(R.layout.adapter_menu, parent, false)
        } catch (e: InflateException) {
            /* map is already there, just return view as it is */
        }

        return ItemViewHolder(view!!)
    }

    override fun onBindViewHolder(mHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = mHolder as ItemViewHolder
        try {
            holder.ivIcon.setImageResource(icon[position])
            holder.tvTitle.text = menuItemsTitles!![position]
            holder.tvTitle.setOnClickListener { mListener.onMenuItemClick(position) }
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

    }


    override fun getItemCount(): Int {
        return if (menuItemsTitles == null) 0 else menuItemsTitles!!.size
    }


    inner

    class ItemViewHolder(var mItemView: View) : RecyclerView.ViewHolder(mItemView) {
        var ivIcon: ImageView
        var tvTitle: TextView

        init {
            ivIcon = mItemView.findViewById(R.id.ivIcon)
            tvTitle = mItemView.findViewById(R.id.tvTitle)
        }
    }

}
