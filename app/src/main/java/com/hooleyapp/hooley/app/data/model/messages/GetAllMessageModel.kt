package com.hooleyapp.hooley.app.data.model.messages

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 4/24/2018.
 */

class GetAllMessageModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("isBlocked")
    var isBlocked: Boolean = false

    @SerializedName("isMessageAccessible")
    var isMessageAccessible: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("messagesList")
    var messagesList = ArrayList<MessageInfo>()

    class MessageInfo {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("senderId")
        var senderId: String? = null

        @SerializedName("messageText")
        var messageText: String = ""

        @SerializedName("fullName")
        var fullName: String? = null

        @SerializedName("sentTime")
        var sentTime: String? = null

        @SerializedName("profilePic")
        var profilePic: String? = null

        @SerializedName("type")
        var type: String? = null

        @SerializedName("mediaUrl")
        var mediaUrl: String = ""

        @SerializedName("thumbnailUrl")
        var thumbnailUrl: String? = null

        var isLoaderShow: Boolean = false

    }

}