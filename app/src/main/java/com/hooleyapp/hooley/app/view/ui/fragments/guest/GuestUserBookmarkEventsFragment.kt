package com.hooleyapp.hooley.app.view.ui.fragments.guest

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.adapters.AdapterBookmarkEvents
import com.hooleyapp.hooley.app.view.ui.activites.HooleyGuestUser
import com.hooleyapp.hooley.databinding.FragmentBookmarkEventsBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.DialogGuestUser
import com.hooleyapp.hooley.interfaces.IBookmarkEventsClickListener
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.model.BookmarkEventModel
import com.hooleyapp.hooley.services.EventWebService

class GuestUserBookmarkEventsFragment : BaseFragment(), IBookmarkEventsClickListener {
    lateinit var binding: FragmentBookmarkEventsBinding
    internal var objectResponse: BookmarkEventModel? = null
    internal var adapter: AdapterBookmarkEvents? = null
    var eventWebService = EventWebService()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bookmark_events, container, false)
        binding.rlTabs.visibility = View.GONE
        showEventBookmark()
        return binding.root
    }


    override fun onDestroy() {
        super.onDestroy()
        if (::binding.isInitialized)
            binding.unbind()
    }

    override fun onClickLike(eventObj: BookmarkEventModel.EventDetail, pos: Int) {
        HooleyGuestUser.activity.DialogGuestUser()
    }

    override fun onClickShare(eventObj: BookmarkEventModel.EventDetail, pos: Int) {
        HooleyGuestUser.activity.DialogGuestUser()
    }

    override fun onClickFollow(eventObj: BookmarkEventModel.EventDetail, pos: Int) {
        HooleyGuestUser.activity.DialogGuestUser()
    }


    private fun setBookmarkRv() {
        val animation = AnimationUtils.loadLayoutAnimation(ActivityBase.activity, R.anim.layout_animation_from_right)
        val manager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.rvBookmarkEvent.layoutAnimation = animation
        binding.rvBookmarkEvent.layoutManager = manager
        binding.rvBookmarkEvent.setEmptyView(binding.tvNoData)
        adapter = AdapterBookmarkEvents(ActivityBase.activity, objectResponse!!.bookmarkedEventsList)
        adapter!!.setListener(this)
        binding.rvBookmarkEvent.adapter = adapter

    }


    private fun showEventBookmark() {
        binding.pbBookmark.visibility = View.VISIBLE
        eventWebService.getBookMarkEvent(object : IWebServiceCallback<BookmarkEventModel> {
            override fun success(result: BookmarkEventModel) {
                objectResponse = result
                binding.pbBookmark.visibility = View.GONE
                binding.tvNoData.visibility = View.GONE
                binding.rvBookmarkEvent.visibility = View.VISIBLE
                setBookmarkRv()
            }

            override fun failure(message: String) {
                binding.pbBookmark.visibility = View.GONE
                try {
                    binding.tvNoData.visibility = View.VISIBLE
                    binding.rvBookmarkEvent.visibility = View.GONE
                    binding.tvNoData.text = message
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                }

            }

            override fun onTokenExpire() {
                onTokenExpireLogOut()
            }
        })
    }

    companion object {
        lateinit var instance: GuestUserBookmarkEventsFragment
        fun newInstance(): GuestUserBookmarkEventsFragment {
            instance = GuestUserBookmarkEventsFragment()
            return instance
        }
    }
}
