package com.hooleyapp.hooley.app.viewmodel.event

import android.app.Application
import android.app.UiModeManager
import android.arch.lifecycle.MutableLiveData
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.app.data.repository.event.ChatStreamRepository
import com.hooleyapp.hooley.app.viewmodel.BaseViewModel
import com.hooleyapp.hooley.model.GetChatStreamModel
import com.hooleyapp.hooley.others.Constants

class ChatStreamViewModel(application: Application) : BaseViewModel(application) {
    var repository = ChatStreamRepository()
    var mList: MutableLiveData<ArrayList<GetChatStreamModel.ChatStream>> = MutableLiveData()
    var pageUrl: MutableLiveData<String> = MutableLiveData()

    init {
        this.mList = repository.mList
    }

    fun getMyChatStreamMessages(eventId: String) {
        repository.getMyChatStreamMessages(eventId)
    }

    fun sendTextStreamMessage(eventId: String, msgText: String) {
        repository.sendTextStreamMessage(eventId, msgText)
    }

    fun getTwitterTweets(eventId: String) {
        var url = if (HooleyApp.db.getInt(Constants.TYPE_THEME) == UiModeManager.MODE_NIGHT_YES) {
            Constants.TWITTER_URL + eventId + "&isDark=true"
        } else {
            Constants.TWITTER_URL + eventId + "&isDark=false"
        }
        pageUrl.postValue(url)
    }
}