package com.hooleyapp.hooley.app.data.model.event.responseModel.eventDetailNew

data class BasicInfo(
        val ppvPrice: Double? = null,
        val profilePic: String? = null,
        val name: String? = null,
        val minimumAge: String? = null,
        val startTime: String? = null,
        val endTime: String? = null,
        val detail: String? = null,
        val type: Int? = null
)
