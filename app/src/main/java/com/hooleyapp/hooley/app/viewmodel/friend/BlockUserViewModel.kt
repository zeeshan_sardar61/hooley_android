package com.hooleyapp.hooley.app.viewmodel.friend

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.hooleyapp.hooley.app.data.model.Friend.BlockUserListModel
import com.hooleyapp.hooley.app.data.repository.friend.BlockUserRepository
import com.hooleyapp.hooley.helper.SingleLiveEvent

class BlockUserViewModel(application: Application) : AndroidViewModel(application) {

    var repository = BlockUserRepository()
    var failureMessage: SingleLiveEvent<String> = SingleLiveEvent()
    var sessionExpire: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var reloadApi: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var loading: SingleLiveEvent<Boolean> = SingleLiveEvent()

    init {
        this.failureMessage = repository.failureMessage
        this.sessionExpire = repository.sessionExpire
        this.reloadApi = repository.reloadApi
        this.loading = repository.loading
    }

    var mList: MutableLiveData<ArrayList<BlockUserListModel.BlockUser>> = MutableLiveData()

    init {
        this.mList = repository.mList
    }

    fun getBlockUserList() {
        repository.getBlockUserList()
    }

    fun unBlockUser(friendId: String) {
        repository.unBlockUser(friendId)
    }
}