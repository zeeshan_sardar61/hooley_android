package com.hooleyapp.hooley.app.data.model.Friend

import com.google.gson.annotations.SerializedName
import java.util.*

class BlockUserListModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("blockedUsersList")
    var blockedUsersList = ArrayList<BlockUser>()

    inner class BlockUser {

        @SerializedName("userId")
        var userId: String? = null

        @SerializedName("fullName")
        var fullName: String? = null

        @SerializedName("profilePic")
        var profilePic: String? = null

        @SerializedName("blockedOn")
        var blockedOn: String? = null
    }

}