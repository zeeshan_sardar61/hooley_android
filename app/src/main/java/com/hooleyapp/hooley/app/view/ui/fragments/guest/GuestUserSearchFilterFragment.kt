package com.hooleyapp.hooley.app.view.ui.fragments.guest

import android.app.Dialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import android.view.animation.Animation
import android.view.animation.Transformation
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.CompoundButton
import android.widget.SeekBar
import android.widget.TextView
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.adapters.AdapterEventCategory
import com.hooleyapp.hooley.app.view.ui.activites.HooleyGuestUser
import com.hooleyapp.hooley.databinding.FragmentGuestUserSearchFilterFragmentBinding
import com.hooleyapp.hooley.fragments.BaseFragment
import com.hooleyapp.hooley.helper.CustomDateTimePicker
import com.hooleyapp.hooley.helper.hideDrawer
import com.hooleyapp.hooley.helper.hideKeyboard
import com.hooleyapp.hooley.model.staticData.Category
import com.hooleyapp.hooley.model.staticData.StaticDataModel
import com.hooleyapp.hooley.others.Constants
import java.text.DecimalFormat
import java.util.*

class GuestUserSearchFilterFragment : BaseFragment(), View.OnClickListener, CompoundButton.OnCheckedChangeListener, SeekBar.OnSeekBarChangeListener, AdapterView.OnItemClickListener {


    internal lateinit var binding: FragmentGuestUserSearchFilterFragmentBinding
    private var list: ArrayList<Category> = ArrayList()
    private var adapterCategory: AdapterEventCategory? = null
    private var customDialog: CustomDateTimePicker? = null
    private var currentProgress = 8046.72f
    private var eventData = ""
    private var searchText = ""
    private val gson = Gson()
    var model = StaticDataModel()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_guest_user_search_filter_fragment, container, false)
        getEventData()
        return binding.root
    }


    companion object {

        lateinit var instance: GuestUserSearchFilterFragment
        private var isPaid = false
        private var isFree = false

        fun newInstance(): GuestUserSearchFilterFragment {
            instance = GuestUserSearchFilterFragment()
            return instance
        }
    }

    private fun setListener() {
        binding.ivCollapseEvent.setOnClickListener(this)
        binding.ivExpandEvent.setOnClickListener(this)
        binding.llDate.setOnClickListener(this)
        binding.btnShowResult.setOnClickListener(this)
        binding.cbFree.setOnCheckedChangeListener(this)
        binding.cbPaid.setOnCheckedChangeListener(this)
        binding.sbGeofence.setOnSeekBarChangeListener(this)
        binding.ivPlus.setOnClickListener(this)
        binding.ivMinus.setOnClickListener(this)
        binding.edtSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (!binding.edtSearch.text!!.isEmpty()) {
                    ActivityBase.activity.hideKeyboard()
                }
            }
            true
        }


        binding.ivClose.setOnClickListener {
            if (binding.edtSearch.text.toString().isNotEmpty()) {
                binding.edtSearch.setText("")
            }
        }

        binding.ivMinus.setOnLongClickListener {
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    if (binding.ivMinus.isPressed) {
                        if (currentProgress >= 100.0f) {
                            var temp = currentProgress
                            temp -= 100f
                            if (temp <= 100.0f) {
                                currentProgress = 100.0f
                            } else {
                                currentProgress -= 100f
                            }
                            binding.sbGeofence.progress = currentProgress.toInt()
                        }
                    } else {
                        timer.cancel()
                    }
                }
            }, 100, 200)
            true
        }
        binding.ivPlus.setOnLongClickListener {
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    if (binding.ivPlus.isPressed) {
                        if (currentProgress < 64373) {
                            currentProgress += 100f
                            binding.sbGeofence.progress = currentProgress.toInt()
                        }
                    } else {
                        timer.cancel()
                    }
                }
            }, 100, 200)

            true
        }


    }

    fun expand(v: View) {
        v.measure(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        val targetHeight = v.measuredHeight

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.layoutParams.height = 1
        v.visibility = View.VISIBLE
        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                v.layoutParams.height = if (interpolatedTime == 1f)
                    WindowManager.LayoutParams.WRAP_CONTENT
                else
                    (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // 1dp/ms
        a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
        binding.tvEventCategory.visibility = View.GONE
    }

    fun collapse(v: View) {
        val initialHeight = v.measuredHeight

        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // 1dp/ms
        a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
        setEventCategoryLabel()
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        when (buttonView.id) {
            R.id.cbPaid -> isPaid = isChecked
            R.id.cbFree -> isFree = isChecked
        }
    }

    override fun onResume() {
        super.onResume()
        binding.sbGeofence.progress = 8046
        binding.tvSbProgress.text = "5 miles"
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.unbind()
        HooleyGuestUser.activity.hideKeyboard()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
    }

    private fun setEventCategoryLabel() {
        var cattegoryName: String? = null
        for (i in list.indices) {
            if (list[i].isActive) {
                if (cattegoryName == null) {
                    cattegoryName = list[i].name
                } else {
                    cattegoryName = cattegoryName + ", " + list[i].name
                }
            }
        }
        binding.tvEventCategory.text = cattegoryName
        if (cattegoryName != null)
            binding.tvEventCategory.visibility = View.VISIBLE
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivCollapseEvent -> {
                collapse(binding.rlSelectEvent)
                binding.ivCollapseEvent.visibility = View.GONE
                binding.ivExpandEvent.visibility = View.VISIBLE
            }
            R.id.ivExpandEvent -> {
                expand(binding.rlSelectEvent)
                binding.ivCollapseEvent.visibility = View.VISIBLE
                binding.ivExpandEvent.visibility = View.GONE
            }
            R.id.llDate -> showCustomDialog(binding.tvEventDate)
            R.id.btnShowResult -> showResultFragment(currentProgress.toDouble(), isPaid, isFree, searchText, eventData, list)
            R.id.ivMinus -> if (currentProgress >= 100.0f) {
                var temp = currentProgress
                temp -= 100f
                if (temp <= 100.0f) {
                    currentProgress = 100.0f
                } else {
                    currentProgress -= 100f
                }
                binding.sbGeofence.progress = currentProgress.toInt()
            }
            R.id.ivPlus -> if (currentProgress < 64373) {
                currentProgress += 100f
                binding.sbGeofence.progress = currentProgress.toInt()
            }
        }
    }

    private fun getEventData() {
        model = gson.fromJson(HooleyApp.db.getString(Constants.STATIC_MODEL), StaticDataModel::class.java)
        list.addAll(model.categories!!)
        setGridEvent()
    }

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        list[position].isActive = !list[position].isActive
        adapterCategory!!.notifyDataSetChanged()
    }

    private fun showResultFragment(mile: Double, paid: Boolean, free: Boolean, text: String?, date: String, mlist: ArrayList<Category>) {
        HooleyGuestUser.activity.hideKeyboard()
        var date = date
        if (!TextUtils.isEmpty(binding.tvEventDate.text))
            eventData = binding.tvEventDate.text.toString()
        if (!TextUtils.isEmpty(binding.edtSearch.text))
            searchText = binding.edtSearch.text.toString()
        if (!TextUtils.isEmpty(binding.tvDate.text))
            date = binding.tvEventDate.text.toString()
        (ActivityBase.activity as ActivityBase).addFragment(R.id.container, GuestUserSearchResultFragment.newInstance(mile / 1609.34, paid, free, searchText, date, mlist), "GuestUserSearchResultFragment")
        hideDrawer(ActivityBase.activity)
    }


    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
        val f = DecimalFormat("##.00")
        currentProgress = seekBar.progress.toFloat()
        try {
            if (currentProgress < 1609.344) {
                if (progress <= 100) {
                    currentProgress = 100.0f
                    binding.tvSbProgress.text = 100.toString() + " meters"
                } else {
                    binding.tvSbProgress.text = f.format(currentProgress) + " meters"
                }
            } else {
                binding.tvSbProgress.text = "${f.format(currentProgress / 1609.344)} miles"
            }
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar) {

    }

    private fun showCustomDialog(view: TextView) {
        customDialog = CustomDateTimePicker(ActivityBase.activity,
                object : CustomDateTimePicker.ICustomDateTimeListener {
                    override fun onSet(dialog: Dialog, calendarSelected: Calendar,
                                       dateSelected: Date, year: Int, monthFullName: String,
                                       monthShortName: String, monthNumber: Int, date: Int,
                                       weekDayFullName: String, weekDayShortName: String,
                                       hour24: Int, hour12: Int, min: Int, sec: Int,
                                       AM_PM: String) {
                        view.text = ""
                        view.text = String.format("%02d-%02d-%04d", monthNumber + 1, calendarSelected.get(Calendar.DAY_OF_MONTH), year)

                    }

                    override fun onCancel() {

                    }
                })

        customDialog!!.set24HourFormat(true)
        customDialog!!.setDate(Calendar.getInstance())
        customDialog!!.showDialog()
    }

    @Throws(NullPointerException::class)
    private fun setGridEvent() {
        adapterCategory = AdapterEventCategory(ActivityBase.activity, list)
        binding.gvEventType.adapter = adapterCategory
        binding.gvEventType.onItemClickListener = this
    }


}
