package com.hooleyapp.hooley.app.data.model.event.responseModel

import android.arch.persistence.room.*
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import com.hooleyapp.hooley.tables.FriendsTable
import java.util.ArrayList
import kotlin.Comparator


class PopularInFriendsModel {
    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("myEventInfo")
    var popularInFriendList = ArrayList<PopularInFriends>()

    @Entity(tableName = "popular_in_friend")
    class PopularInFriends {

        @PrimaryKey
        @ColumnInfo(name = "id")
        var id: Int = 0

        @ColumnInfo(name = "eventId")
        @SerializedName("eventId")
        var eventId: String? = null

        @ColumnInfo(name = "coverPhoto")
        @SerializedName("coverPhoto")
        var coverPhoto: String? = null

        @ColumnInfo(name = "startTime")
        @SerializedName("startTime")
        var startTime: String? = null

        @ColumnInfo(name = "endTime")
        @SerializedName("endTime")
        var endTime: String? = null

        @ColumnInfo(name = "eventName")
        @SerializedName("eventName")
        var eventName: String? = null

        @ColumnInfo(name = "address")
        @SerializedName("address")
        var address: String? = null

        @ColumnInfo(name = "eventLat")
        @SerializedName("eventLat")
        var eventLat: Double? = null

        @ColumnInfo(name = "eventLong")
        @SerializedName("eventLong")
        var eventLong: Double? = null

        @ColumnInfo(name = "likesCount")
        @SerializedName("likesCount")
        var likesCount: Int = 0

        @ColumnInfo(name = "commentsCount")
        @SerializedName("commentsCount")
        var commentsCount: Int = 0

        @ColumnInfo(name = "sharesCounts")
        @SerializedName("sharesCounts")
        var sharesCount: Int = 0

        @ColumnInfo(name = "isLiked")
        @SerializedName("isLiked")
        var isLiked: Boolean = false

        @ColumnInfo(name = "isLive")
        @SerializedName("isLive")
        var isLive: Boolean = false

        @ColumnInfo(name = "isPast")
        @SerializedName("isPast")
        var isPast: Boolean = false

        @ColumnInfo(name = "isFollowed")
        @SerializedName("isFollowed")
        var isFollowed: Boolean = false

        @ColumnInfo(name = "followersCount")
        @SerializedName("followersCount")
        var followersCount: Int = 0

        @TypeConverters(FriendsTypeConverter::class)
        @ColumnInfo(name = "friendList")
        @SerializedName("friendList")
        var friendList: ArrayList<FriendsTable> = arrayListOf()
    }

    class FriendsTypeConverter {
        companion object {
            var gson = Gson()
            @TypeConverter
            @JvmStatic
            fun stringToList(value: String): ArrayList<FriendsTable> {
                return if (value.isBlank())
                    arrayListOf()
                else {
                    val listType = object : TypeToken<ArrayList<FriendsTable>>() {}.type
                    gson.fromJson(value, listType)
                }
            }

            @TypeConverter
            @JvmStatic
            fun listToString(list: ArrayList<FriendsTable>): String {
                return gson.toJson(list)
            }
        }
    }

    companion object {

        //Getter and setter methods same as the above examples
        /*Comparator for sorting the list by Student Name*/
        var sortByNameComparator: java.util.Comparator<PopularInFriends> = Comparator { s1, s2 ->
            val StudentName1 = s1.eventName!!.toUpperCase()
            val StudentName2 = s2.eventName!!.toUpperCase()
            StudentName1.compareTo(StudentName2)
        }
    }
}
