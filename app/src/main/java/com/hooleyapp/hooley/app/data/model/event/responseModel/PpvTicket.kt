package com.hooleyapp.hooley.app.data.model.event.responseModel

import com.google.gson.annotations.SerializedName

data class PpvTicket(

        @field:SerializedName("sold")
        val sold: Int? = null,

        @field:SerializedName("price")
        val price: Int? = null,

        @field:SerializedName("isEnable")
        val isEnable: Boolean? = null
)