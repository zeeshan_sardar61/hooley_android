package com.hooleyapp.hooley.app.view.callback

interface IEventGuestListClickListener {
    fun onClickAddFriend(position: Int)
    fun onClickItem(position: Int)
    fun onClickMyProfile(position: Int)
    fun onClickFriendProfile(position: Int)
    fun onClickMessage(position: Int)
    fun onClickAcceptRequest(position: Int, value: Boolean)
}