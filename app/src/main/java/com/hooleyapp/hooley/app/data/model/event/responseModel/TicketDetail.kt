package com.hooleyapp.hooley.app.data.model.event.responseModel

import com.google.gson.annotations.SerializedName

data class TicketDetail(

        @field:SerializedName("ticketId")
        val ticketId: Int? = null,

        @field:SerializedName("ticketName")
        val ticketName: String? = null,

        @field:SerializedName("sold")
        val sold: Int? = null,

        @field:SerializedName("price")
        val price: Int? = null,

        @field:SerializedName("quantity")
        val quantity: Int? = null,

        @field:SerializedName("available")
        val available: Int? = null
)