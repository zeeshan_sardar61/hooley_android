package com.hooleyapp.hooley.app.data.model.media

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by adil on 18-Jan-18.
 */

class EventGalleryFeedModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("eventMediaInfoList")
    var eventMediaInfoList = ArrayList<EventMediaInfo>()

    class EventMediaInfo {

        @SerializedName("postId")
        var imageId: String = ""

        @SerializedName("userId")
        var userId: String? = null

        @SerializedName("fullName")
        var fullName: String = ""

        @SerializedName("profilePic")
        var profilePic: String = ""

        @SerializedName("imageCaption")
        var imageCaption: String = ""

        @SerializedName("postedTime")
        var postedTime: String = ""

        @SerializedName("imageUrl")
        var imageUrl: String? = null

        @SerializedName("likesCount")
        var likesCount: Int = 0

        @SerializedName("commentsCount")
        var commentsCount: Int = 0

        @SerializedName("sharesCounts")
        var shareCounts: Int = 0

        @SerializedName("favoriteCount")
        var favoriteCount: Int = 0

        @SerializedName("isLiked")
        var isLiked: Boolean = false

        @SerializedName("isFavorite")
        var isFavorited: Boolean = false

        @SerializedName("eventId")
        var eventId: String? = null

        @SerializedName("eventName")
        var eventName: String? = null

        @SerializedName("isLive")
        var isLive: Boolean = false

        @SerializedName("mediaFiles")
        var mediaFiles = ArrayList<MediaFile>()

        @SerializedName("partialComments")
        var partialComments = ArrayList<CommentObj>()

    }

    inner class CommentObj {

        @SerializedName("userId")
        var userId: String? = null

        @SerializedName("profilePic")
        var profilePic: String? = null

        @SerializedName("fullName")
        var fullName: String? = null

        @SerializedName("commentText")
        var commentText: String? = null

    }

    inner class MediaFile {

        @SerializedName("mediaId")
        var mediaId: String? = null

        @SerializedName("mediaUrl")
        var mediaUrl: String? = null

        @SerializedName("thumbnailUrl")
        var thumbnailUrl: String? = null

        @SerializedName("isVideo")
        var isVideo: Boolean? = null

    }

}
