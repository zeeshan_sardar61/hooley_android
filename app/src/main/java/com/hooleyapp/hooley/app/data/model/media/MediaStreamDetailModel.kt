package com.hooleyapp.hooley.app.data.model.media

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Nauman on 3/22/2018.
 */

class MediaStreamDetailModel {

    @SerializedName("IsSuccessful")
    var success: Boolean = false

    @SerializedName("Exception")
    var exception: String? = null

    @SerializedName("eventMediaInfoList")
    var eventMediaInfoArrayList = ArrayList<EventGalleryFeedModel.EventMediaInfo>()

}
