package com.hooleyapp.hooley.app.data.model.event.responseModel.eventDetailNew

data class Reasons(
        val cancel: String? = null,
        val timeUpdate: String? = null
)
