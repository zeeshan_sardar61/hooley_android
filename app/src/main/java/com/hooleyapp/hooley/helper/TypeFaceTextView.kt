package com.hooleyapp.hooley.helper

import android.content.Context
import android.graphics.Typeface
import android.support.v4.view.TintableBackgroundView
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import com.hooleyapp.hooley.R


/**
 * Created by android on 11/17/15.
 */
class TypeFaceTextView : AppCompatTextView, TintableBackgroundView {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setCustomFont(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        setCustomFont(context, attrs)
    }

    private fun setCustomFont(ctx: Context, attrs: AttributeSet) {
        val a = ctx.obtainStyledAttributes(attrs, R.styleable.TextViewPlus)
        val customFont = a.getString(R.styleable.TextViewPlus_customFont)
        setCustomFont(ctx, customFont)
        a.recycle()
    }

    fun setCustomFont(ctx: Context, asset: String?): Boolean {
        var tf: Typeface? = null
        try {
            tf = Typeface.createFromAsset(ctx.assets, asset)
        } catch (e: Exception) {
            // Log.e(TAG, "Could not get typeface: " + e.getMessage());
            return false
        }

        typeface = tf
        return true
    }

    companion object {
        private val TAG = "TextView"
    }

}