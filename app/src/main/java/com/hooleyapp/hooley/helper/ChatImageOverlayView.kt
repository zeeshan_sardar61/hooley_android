package com.hooleyapp.hooley.helper

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout

import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.interfaces.IImageActionListener

/**
 * Created by Nauman on 5/10/2018.
 */

class ChatImageOverlayView : RelativeLayout {


    private var SelectImageUrl: String? = null
    var mListener: IImageActionListener? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }


    fun setSelectedImageUrl(imageUrl: String) {
        this.SelectImageUrl = imageUrl
    }


    private fun init() {
        val view = View.inflate(context, R.layout.view_image_overlay, this)
        view.findViewById<View>(R.id.btnDeletePicture).setOnClickListener {
            //                Toast.makeText(SpottieMain.activity, "Image url = > " + SelectImageUrl, Toast.LENGTH_SHORT).show();
            //                mListener.deleteImage(SelectImageUrl);
        }
        view.findViewById<View>(R.id.btnSetProfilePic).setOnClickListener {
            //                Toast.makeText(SpottieMain.activity, "Image url = > " + SelectImageUrl, Toast.LENGTH_SHORT).show();
            //                mListener.updateProfilePic(SelectImageUrl);
        }
    }

//    fun setMListener(listener: IImageActionListener) {
//        this.mListener = listener
//    }
}