package com.hooleyapp.hooley.helper

import android.content.Context
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.interfaces.IEventAllFeedsClick
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils

/**
 * Created by Nauman on 2/9/2018.
 */


class ImageOverlayView : RelativeLayout {

    var selectedImageUrl: String? = null
        get() = eventMediaInfo!!.imageUrl
    var eventMediaInfo: EventGalleryFeedModel.EventMediaInfo? = null
    private var ivHost: ImageView? = null
    private var ivLike: ImageView? = null
    private var tvImageCaption: TextView? = null
    private var tvPostDate: TextView? = null
    private var tvPostTime: TextView? = null
    private var tvLike: TextView? = null
    private var tvComment: TextView? = null
    private var tvShare: TextView? = null
    private var tvHostName: TextView? = null
    lateinit var mContext: Context
    private var mListener: IEventAllFeedsClick? = null

    constructor(mcontext: Context, eventMedia: EventGalleryFeedModel.EventMediaInfo) : super(mcontext) {
        mContext = mcontext
        eventMediaInfo = eventMedia
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    fun mediaLiked(value: Boolean) {
        if (value) {
            ivLike!!.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_red_heart)
            eventMediaInfo!!.likesCount = eventMediaInfo!!.likesCount + 1
            tvLike!!.text = eventMediaInfo!!.likesCount.toString()
        } else {
            ivLike!!.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_purple_heart)
            eventMediaInfo!!.likesCount = eventMediaInfo!!.likesCount - 1
            tvLike!!.text = eventMediaInfo!!.likesCount.toString()

        }
    }

    fun mediaShared() {
        eventMediaInfo!!.shareCounts = eventMediaInfo!!.shareCounts + 1
        tvShare!!.text = eventMediaInfo!!.shareCounts.toString()
    }

    fun mediaFav(value: Boolean) {
        eventMediaInfo!!.isFavorited = value
    }


    private fun init() {
        val view = View.inflate(mContext, R.layout.layout_image_view, this)
        ivHost = view.findViewById(R.id.ivHost)
        ivLike = view.findViewById(R.id.ivLike)
        tvImageCaption = view.findViewById(R.id.tvImageCaption)
        tvPostDate = view.findViewById(R.id.tvPostDate)
        tvPostTime = view.findViewById(R.id.tvPostTime)
        tvLike = view.findViewById(R.id.tvLike)
        tvComment = view.findViewById(R.id.tvComment)
        tvShare = view.findViewById(R.id.tvShare)
        tvHostName = view.findViewById(R.id.tvHostName)

        if (eventMediaInfo != null) {
            if (!TextUtils.isEmpty(eventMediaInfo!!.profilePic))
                Glide.with(HooleyMain.activity!!).load(eventMediaInfo!!.profilePic).into(ivHost!!)
            if (eventMediaInfo!!.isLiked) {
                ivLike!!.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_red_heart)
            } else {
                ivLike!!.background = ContextCompat.getDrawable(HooleyMain.activity!!, R.drawable.ic_purple_heart)
            }
            tvLike!!.text = eventMediaInfo!!.likesCount.toString()
            tvComment!!.text = eventMediaInfo!!.commentsCount.toString()
            tvShare!!.text = eventMediaInfo!!.shareCounts.toString()
            tvImageCaption!!.text = eventMediaInfo!!.fullName + " - " + DateUtils.convertSingleDate(DateUtils.getLocalDate(eventMediaInfo!!.postedTime))
            /*tvPostTime.setText(DateUtils.convertSingleDate(DateUtils.getLocalDate(eventMediaInfo.postedTime)));*/
            tvPostTime!!.visibility = View.GONE
            tvHostName!!.text = eventMediaInfo!!.eventName.toString()

        }

        view.findViewById<View>(R.id.rlComments).setOnClickListener { mListener!!.onClickPostComment(eventMediaInfo!!, Constants.TYPE_EVENT_POST_VIEW) }

        view.findViewById<View>(R.id.rlLike).setOnClickListener { mListener!!.onClickLikePost(eventMediaInfo!!, 0, Constants.TYPE_EVENT_POST_VIEW) }

        view.findViewById<View>(R.id.btnMore).setOnClickListener { mListener!!.onClickMore(eventMediaInfo!!, 0, 0, Constants.TYPE_EVENT_POST_VIEW) }

        view.findViewById<View>(R.id.rlShare).setOnClickListener { mListener!!.onClickShare(eventMediaInfo!!.imageId, 0, 0, Constants.TYPE_EVENT_POST_VIEW) }

    }

    fun setMListener(listener: IEventAllFeedsClick) {
        this.mListener = listener
    }
}