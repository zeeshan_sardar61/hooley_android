package com.hooleyapp.hooley.helper


import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.database.CursorIndexOutOfBoundsException
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.drawable.ColorDrawable
import android.media.ExifInterface
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.text.Html
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.view.Window
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.activites.HooleyAuth
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.fragments.event.LiveEventFragment
import com.hooleyapp.hooley.interfaces.ICallBackUri
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.others.HooleyToolbarTitles
import com.hooleyapp.hooley.others.RealPathUtil
import de.hdodenhof.circleimageview.CircleImageView
import me.saket.bettermovementmethod.BetterLinkMovementMethod
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


var dialog: Dialog? = null
var fileUri: Uri? = null

fun Context.showToastMessage(message: CharSequence) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

fun Context.showDialog() {
    dialog = ProgressDialog.show(this, "", "Loading. Please wait...", true)
}

fun Context.removeDialog() {
    if (dialog != null)
        dialog!!.dismiss()
}

val Context.glide: RequestManager?
    get() = Glide.with(this)

fun ImageView.load(path: String) {
    try {
        context.glide!!.load(path).thumbnail(0.1f).into(this)
    } catch (ex: IllegalArgumentException) {

    }
}

fun View.clearAllFocus() {
    val group = this as ViewGroup
    val count = group.childCount
    for (i in 0 until count) {
        val view = group.getChildAt(i)
        if (view is ViewGroup) {
            view.clearAllFocus()
        } else if (view is EditText) {
            view.clearFocus()
        }
    }
}

fun hideDrawer(activity: AppCompatActivity) {
    HooleyApp.db.putBoolean(Constants.OPEN_DRAWER, false)
    activity.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    activity.supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
}

fun AppCompatActivity.hideToolbar() {
    supportActionBar?.hide()
}


fun AppCompatActivity.showToolbar() {
    supportActionBar?.show()
}


fun AppCompatActivity.checkIfName(name: String): Boolean {
    val pattern = Pattern.compile(".*[a-zA-Z]+.*")
    val matcher = pattern.matcher(name)
    return matcher.matches()
}


fun Activity.startCamera(requestCode: Int) {
    val values = ContentValues(1)
    values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg")
    fileUri = contentResolver
            .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    values)
    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
    if (intent.resolveActivity(packageManager) != null) {
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        startActivityForResult(intent, requestCode)
    }
}

fun Activity.startVideoCamera(requestCode: Int) {
    val values = ContentValues(1)
    values.put(MediaStore.Images.Media.MIME_TYPE, "video/*")
    fileUri = contentResolver
            .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    values)
    val intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
    if (intent.resolveActivity(packageManager) != null) {
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        startActivityForResult(intent, requestCode)
    }
}

fun Context.compressFile(fileUri: Uri): Uri {

    var image = this.compressBitmap(this.handleSamplingAndRotationBitmap(fileUri)!!)
    val file = File(this.cacheDir, "${getFileName(fileUri.path)}")
    try {
        file.createNewFile()
    } catch (e: IOException) {
        e.printStackTrace()
    }
    val bitmap = Bitmap.createScaledBitmap(image, image.width, image.height, true)
    val bos = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos)
    val bitmapdata = bos.toByteArray()
    try {
        val fos = FileOutputStream(file)
        fos.write(bitmapdata)
        fos.flush()
        fos.close()
    } catch (e: IOException) {

        e.printStackTrace()
    }

    return Uri.fromFile(file)
}


fun Activity.showGalleryVideo(requestCode: Int) {
    val galleryIntent: Intent?
    galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
    galleryIntent.type = "video/*"
    if (galleryIntent.resolveActivity(packageManager) == null) {
        showToastMessage("This Application do not have Gallery Application")
        return
    }
    startActivityForResult(galleryIntent, requestCode)
}

fun Activity.getUriFrombitmap(inImage: Bitmap): Uri {
    val bytes = ByteArrayOutputStream()
    inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
    val path = MediaStore.Images.Media.insertImage(contentResolver, inImage, "Title", null)
    return Uri.parse(path)
}

fun Activity.getFilePathFromContentUri(selectedVideoUri: Uri?,
                                       contentResolver: ContentResolver): String {
    val filePath: String
    val filePathColumn = arrayOf(MediaStore.MediaColumns.DATA)
    val cursor = contentResolver.query(selectedVideoUri!!, filePathColumn, null, null, null)
    cursor!!.moveToFirst()

    val columnIndex = cursor.getColumnIndex(filePathColumn[0])
    filePath = cursor.getString(columnIndex)
    cursor.close()
    return filePath
}


fun Activity.showGalleryImage(requestCode: Int) {
    val galleryIntent: Intent?
    galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
    galleryIntent.type = "image/*"
//    galleryIntent.type = "image/* video/*"
    if (galleryIntent.resolveActivity(packageManager) == null) {
        showToastMessage("This Application do not have Gallery Application")
        return
    }
    startActivityForResult(galleryIntent, requestCode)
}


fun Activity.showCameraGalleryImage(requestCode: Int) {
    val galleryIntent: Intent?
    galleryIntent = Intent()
//    galleryIntent.action = Intent.ACTION_PICK
//    galleryIntent.type = "image/*"
    galleryIntent.setDataAndType(Uri.parse(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).toString()), "image/*")
    galleryIntent.action = Intent.ACTION_PICK

    if (galleryIntent.resolveActivity(packageManager) == null) {
        showToastMessage("This Application do not have Gallery Application")
        return
    }
//    startActivityForResult(galleryIntent, requestCode)
    startActivityForResult(Intent.createChooser(galleryIntent, "Camera"), requestCode)

}


fun Activity.showGallery(requestCode: Int) {
    val galleryIntent: Intent?
    galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
//    galleryIntent.type = "image/*"
    galleryIntent.type = "image/* video/*"
    if (galleryIntent.resolveActivity(packageManager) == null) {
        showToastMessage("This Application do not have Gallery Application")
        return
    }
    startActivityForResult(galleryIntent, requestCode)
}

fun Activity.showMusicFiles(requestCode: Int) {
    val audioIntent: Intent?
    audioIntent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI)
    startActivityForResult(Intent.createChooser(audioIntent, "Gallery"), requestCode)
}

fun Activity.processCapturedPhoto(callback: ICallBackUri) {
    try {
        val cursor = contentResolver.query(Uri.parse(fileUri.toString()),
                Array(1) { android.provider.MediaStore.Images.ImageColumns.DATA },
                null, null, null)
        cursor.moveToFirst()
        val photoPath = cursor.getString(0)
        cursor.close()
        val file = File(photoPath)
        callback.imageUri(Uri.fromFile(file))
    } catch (ex: CursorIndexOutOfBoundsException) {
        ex.printStackTrace()
        var columns: Array<String> = arrayOf(MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DATE_ADDED)
        val cursor = contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, columns, null, null,
                "${MediaStore.MediaColumns.DATE_ADDED} DESC")
        var coloumnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val photoPath = cursor.getString(coloumnIndex)
        cursor.close()
        val file = File(photoPath)
        callback.imageUri(Uri.fromFile(file))
    }

}

fun Activity.processCapturedVideo(callback: ICallBackUri) {
    try {
        var columns: Array<String> = arrayOf(MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DATE_ADDED)
        val cursor = contentResolver.query(Uri.parse(fileUri.toString()), columns, null, null,
//        val cursor = contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, columns, null, null,
                "${MediaStore.MediaColumns.DATE_ADDED} DESC")

        var coloumnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val photoPath = cursor.getString(coloumnIndex)
        cursor.close()
        val file = File(photoPath)
        callback.imageUri(Uri.fromFile(file))
    } catch (ex: CursorIndexOutOfBoundsException) {
        ex.printStackTrace()
    }

}


fun Activity.hideKeyboard() {
    val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    //Find the currently focused view, so we can grab the correct window token from it.
    var view = currentFocus
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    if (view == null) {
        view = View(this)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

@Throws(IOException::class)
fun Context.handleSamplingAndRotationBitmap(selectedImage: Uri): Bitmap? {
    val MAX_HEIGHT = 1024
    val MAX_WIDTH = 1024

    // First decode with inJustDecodeBounds=true to check dimensions
    val options = BitmapFactory.Options()
    options.inJustDecodeBounds = true
    var imageStream = contentResolver.openInputStream(selectedImage)
    BitmapFactory.decodeStream(imageStream, null, options)
    imageStream!!.close()

    // Calculate inSampleSize
    options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT)

    // Decode bitmap with inSampleSize set
    options.inJustDecodeBounds = false
    imageStream = contentResolver.openInputStream(selectedImage)
    var img = BitmapFactory.decodeStream(imageStream, null, options)
    if (img != null)
        img = rotateImageIfRequired(img, selectedImage)
    else {
        showToastMessage("Image not available")
        return null
    }

    return img
}

@Throws(IOException::class)

private fun rotateImageIfRequired(img: Bitmap, selectedImage: Uri): Bitmap {
    var ei = ExifInterface(selectedImage.path)
    var orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
    return when (orientation) {
        ExifInterface.ORIENTATION_ROTATE_90 ->
            rotateImage(img, 90)
        ExifInterface.ORIENTATION_ROTATE_180 ->
            rotateImage(img, 180)
        ExifInterface.ORIENTATION_ROTATE_270 ->
            rotateImage(img, 270)
        else -> {
            img
        }
    }
}

private fun calculateInSampleSize(options: BitmapFactory.Options,
                                  reqWidth: Int, reqHeight: Int): Int {
    // Raw height and width of image
    val height = options.outHeight
    val width = options.outWidth
    var inSampleSize = 1

    if (height > reqHeight || width > reqWidth) {

        // Calculate ratios of height and width to requested height and width
        val heightRatio = Math.round(height.toFloat() / reqHeight.toFloat())
        val widthRatio = Math.round(width.toFloat() / reqWidth.toFloat())

        // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
        // with both dimensions larger than or equal to the requested height and width.
        inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio

        // This offers some additional logic in case the image has a strange
        // aspect ratio. For example, a panorama may have a much larger
        // width than height. In these cases the total pixels might still
        // end up being too large to fit comfortably in memory, so we should
        // be more aggressive with sample down the image (=larger inSampleSize).

        val totalPixels = (width * height).toFloat()

        // Anything more than 2x the requested pixels we'll sample down further
        val totalReqPixelsCap = (reqWidth * reqHeight * 2).toFloat()

        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++
        }
    }
    return inSampleSize
}


private fun rotateImage(img: Bitmap, degree: Int): Bitmap {
    val matrix = Matrix()
    matrix.postRotate(degree.toFloat())
    val rotatedImg = Bitmap.createBitmap(img, 0, 0, img.width, img.height, matrix, true)
    img.recycle()
    return rotatedImg
}

fun Activity.processGalleryPhoto(data: Intent, callback: ICallBackUri) {
    val realPath = RealPathUtil.getRealPath(this, data.data)
    fileUri = try {
        Uri.fromFile(File(realPath))
    } catch (ex: Exception) {
        Uri.parse(realPath)
    }
    callback.imageUri(fileUri)
}

fun EditText.isEmailValid(): Boolean {
    return this.text.contains("@") && this.text.contains(".com")
}


fun Context.showSuccessDialog(description: String) {
    var successDialog = Dialog(this, R.style.Theme_Dialog)
    successDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    successDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    successDialog.setContentView(R.layout.dialoge_success)
    val btnOkay = successDialog.findViewById<TextView>(R.id.tvOkay)
    var tvDescription = successDialog.findViewById<TextView>(R.id.tvContent)
    if (description.contains("Phone")) {
        tvDescription.text = Html.fromHtml(description)
    } else {
        tvDescription.text = description
    }
    btnOkay.setOnClickListener {
        successDialog.dismiss()
    }
    successDialog.show()
}

fun Context.showInfoDialog(title: String, description: String) {
    var successDialog = Dialog(this, R.style.Theme_Dialog)
    successDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    successDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    successDialog.setContentView(R.layout.dialog_info)
    val tvTitle = successDialog.findViewById<TextView>(R.id.tvTitle)
    var tvDescription = successDialog.findViewById<TextView>(R.id.tvContent)
    var ivCancel = successDialog.findViewById<ImageView>(R.id.ivCancel)
    tvTitle.text = title
    tvDescription.text = description
    ivCancel.setOnClickListener {
        successDialog.dismiss()
    }
    successDialog.show()
}

fun Context.showUpdateDialog(listener: onClickDialog) {
    var successDialog = Dialog(this, R.style.Theme_Dialog)
    successDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    successDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    successDialog.setContentView(R.layout.dialoge_success)
    val btnOkay = successDialog.findViewById<TextView>(R.id.tvOkay)
    var tvDescription = successDialog.findViewById<TextView>(R.id.tvContent)
    tvDescription.text = "Your app is outdated, Kindly update your app"
    btnOkay.text = "Update Now"
    btnOkay.setOnClickListener {
        listener.onClickYes()
    }
    successDialog.setCancelable(false)
    successDialog.show()
}


fun Context.phonoExistsDialog(phone: String, profile_pic: String, name: String, email: String, listener: onClickPhoneDialog) {
    var successDialog = Dialog(this, R.style.Theme_Dialog)
    successDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    successDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    successDialog.setContentView(R.layout.phone_already_exist)

    val tvUserPhone = successDialog.findViewById<TextView>(R.id.tvUserPhone)
    var ivAvatar = successDialog.findViewById<CircleImageView>(R.id.ivAvatar)
    var tvUserName = successDialog.findViewById<TextView>(R.id.tvUserName)
    var tvUserEmail = successDialog.findViewById<TextView>(R.id.tvUserEmail)
    var tvUserAccount = successDialog.findViewById<TextView>(R.id.tvLogin)
    var tvAnother = successDialog.findViewById<TextView>(R.id.tvAnother)

    tvUserPhone.text = phone
    ivAvatar.load(profile_pic)
    tvUserName.text = name
    tvUserEmail.text = email

    tvUserAccount.setOnClickListener {
        successDialog.dismiss()
        listener.onClickYes()
    }

    tvAnother.setOnClickListener {
        successDialog.dismiss()
        listener.onTryClick()
    }
    successDialog.show()
}

fun Context.showAnnouncementDialog(description: String, imgUrl: String, eventName: String, eventStartTime: String, eventEndTime: String, eventAddress: String, listener: onClickDialog) {


    var successDialog = Dialog(this, R.style.Theme_Dialog)
    successDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    successDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    successDialog.setContentView(R.layout.dialoge_announcement)
    var ivCancel = successDialog.findViewById<ImageView>(R.id.ivCancel)
    var ivAnnouncement = successDialog.findViewById<ImageView>(R.id.ivAnnouncement)
    var tvDescription = successDialog.findViewById<TextView>(R.id.tvContent)
    var tvEventTitle = successDialog.findViewById<TextView>(R.id.tvEventTitle)
    var tvEventTime = successDialog.findViewById<TextView>(R.id.tvEventTime)
    var tvEventAddress = successDialog.findViewById<TextView>(R.id.tvEventAddress)
    var btnViewEvent = successDialog.findViewById<TextView>(R.id.btnViewEvent)

    if (imgUrl.isEmpty()) {
        ivAnnouncement.visibility = GONE
    } else {
        ivAnnouncement.load(imgUrl)
    }
    tvDescription.text = description
    tvEventTitle.text = eventName
    var start = eventStartTime.replace("/Date(", "").replace(")/", "")
    var end = eventEndTime.replace("/Date(", "").replace(")/", "")
    tvEventTime.text = DateUtils.displayDateOnly(DateUtils.convertNotificationTime(DateUtils.dateToUTC(Date(start.toLong())).toString()))
    tvEventAddress.text = eventAddress

    ivCancel.setOnClickListener {
        successDialog.dismiss()
    }

    btnViewEvent.setOnClickListener {
        successDialog.dismiss()
        listener.onClickYes()
    }

    BetterLinkMovementMethod.linkifyHtml(tvDescription).setOnLinkClickListener { _, url ->
        if (url.contains(Constants.EVENT_SHARE)) {
            successDialog.dismiss()
            val eventIdString = url.split("eventId=")
            (HooleyMain.activity as ActivityBase).callFragment(R.id.container, LiveEventFragment
                    .newInstance(eventIdString[1]), "LiveEventFragment")
        } else {
            successDialog.dismiss()
            val browserIntent = Intent(Intent.ACTION_VIEW)
            browserIntent.data = Uri.parse(url)
            HooleyMain.activity!!.startActivity(browserIntent)

        }
        true
    }

    successDialog.show()
}

fun setToolBarTitle(mActivity: Activity, title: String) {
    (mActivity as HooleyMain).setToolbarTitle(title)
}

fun String.getTitleName(): String {
    return HooleyToolbarTitles.getNotificationTitle(this)
}

fun Context.showErrorDialog(title: String, description: String) {
//    var successDialog = Dialog(this, R.style.Theme_Dialog)
//    successDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//    successDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//    successDialog.setContentView(R.layout.dialoge_success)
//    val btnOkay = successDialog.findViewById<Button>(R.id.btnOkay)
//    val ivClose = successDialog.findViewById<ImageView>(R.id.ivClose)
//    var tvTitle = successDialog.findViewById<TextView>(R.id.tvTitle)
//    var tvDescription = successDialog.findViewById<TextView>(R.id.tvDescription)
//    tvTitle.text = title
//    tvDescription.text = description
//    ivClose.setOnClickListener {
//        successDialog.dismiss()
//    }
//    btnOkay.setOnClickListener {
//        successDialog.dismiss()
//    }
//    successDialog.show()
}

fun Context.YesNoDialog(title: String, description: String, dismiss: String?, listener: onClickDialog) {
    var shareDialog = Dialog(this, R.style.Theme_Dialog)
    shareDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    shareDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    shareDialog.setContentView(R.layout.dialog_discard_confirmation)
    val tvYes = shareDialog.findViewById<TextView>(R.id.tvYes)
    val tvCancel = shareDialog.findViewById<TextView>(R.id.tvCancel)
    val tvTitle = shareDialog.findViewById<TextView>(R.id.tvTitle)
    val tvDescription = shareDialog.findViewById<TextView>(R.id.tvDescription)

    tvTitle.text = title
    tvDescription.text = description
    tvCancel.setOnClickListener {
        shareDialog.dismiss()
    }
    tvYes.setOnClickListener {
        shareDialog.dismiss()
        listener.onClickYes()
    }
    if (dismiss != null) {
        shareDialog.dismiss()
    } else {
        shareDialog.show()
    }
}

fun Context.YesNoDialogBlock(title: String, name: String, listener: onClickDialog) {
    var shareDialog = Dialog(this, R.style.Theme_Dialog)
    shareDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    shareDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    shareDialog.setContentView(R.layout.dialog_block_confirmation)
    val tvYes = shareDialog.findViewById<TextView>(R.id.tvYes)
    val tvCancel = shareDialog.findViewById<TextView>(R.id.tvCancel)
    val tvTitle = shareDialog.findViewById<TextView>(R.id.tvTitle)
    val tvBlockName = shareDialog.findViewById<TextView>(R.id.tvBlockName)
    tvTitle.text = title
    tvBlockName.text = name
    tvCancel.setOnClickListener {
        shareDialog.dismiss()
    }
    tvYes.setOnClickListener {
        shareDialog.dismiss()
        listener.onClickYes()
    }
    shareDialog.show()
}

fun Context.addCommaInCurrency(values: Int): String? {
    val myFormat = NumberFormat.getInstance()
    myFormat.isGroupingUsed = true
    val decimalFormat = DecimalFormat("#.##")
    decimalFormat.isGroupingUsed = true
    decimalFormat.groupingSize = 3
    return decimalFormat.format(values)
}


fun Activity.DialogGuestUser() {
//    var listener: onClickGuestDialog
    var shareDialog = Dialog(this, R.style.Theme_Dialog)
    shareDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    shareDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    shareDialog.setContentView(R.layout.dialog_login)
    val btnSignUp = shareDialog.findViewById<TextView>(R.id.btnSignUp)
    val ivCancel = shareDialog.findViewById<ImageView>(R.id.ivCancel)
    val btnSignIn = shareDialog.findViewById<TextView>(R.id.btnSignIn)
    ivCancel.setOnClickListener {
        shareDialog.dismiss()
    }
    btnSignUp.setOnClickListener {
        shareDialog.dismiss()
        var intent = Intent(this, HooleyAuth::class.java)
        intent.putExtra("Type", "Register")
        startActivity(intent)
        finish()
    }
    btnSignIn.setOnClickListener {
        shareDialog.dismiss()
        var intent = Intent(this, HooleyAuth::class.java)
        intent.putExtra("Type", "Login")
        startActivity(intent)
        finish()
    }
    shareDialog.show()
}

interface onClickDialog {
    fun onClickYes()
}


interface onClickPhoneDialog {
    fun onClickYes()

    fun onTryClick()
}

interface onClickGuestDialog {
    fun onClickSignUP()
    fun onClickLogin()
}


fun Context.compressBitmap(image: Bitmap): Bitmap {

    var finalImage: Bitmap? = null
    if (image.width > image.height) {
        var ratio = (Constants.CONST_DEFAULT_IMAGE_WIDTH.toFloat() / image.width.toFloat())
        finalImage = Bitmap.createScaledBitmap(image, Constants.CONST_DEFAULT_IMAGE_WIDTH, (image.height * ratio).toInt(), true)

    } else if (image.height > image.width) {

        var ratio = (Constants.CONST_DEFAULT_IMAGE_WIDTH.toFloat() / image.height.toFloat())
        finalImage = Bitmap.createScaledBitmap(image, (image.width * ratio).toInt(), Constants.CONST_DEFAULT_IMAGE_WIDTH, true)

    } else {
        if (image.width > Constants.CONST_DEFAULT_IMAGE_WIDTH) {
            finalImage = Bitmap.createScaledBitmap(image, Constants.CONST_DEFAULT_IMAGE_WIDTH, Constants.CONST_DEFAULT_IMAGE_WIDTH, true)
        } else {
            finalImage = image
        }
    }

    return finalImage!!
}

fun Context.getRootParentFragment(fragment: Fragment): Fragment {
    val parent = fragment.parentFragment
    return if (parent == null)
        fragment
    else
        getRootParentFragment(parent)
}

fun convertCountToMonth(month: Int): String {
    return when (month) {
        1 -> "January"
        2 -> "February"
        3 -> "March"
        4 -> "April"
        5 -> "May"
        6 -> "June"
        7 -> "July"
        8 -> "August"
        9 -> "September"
        10 -> "October"
        11 -> "November"
        12 -> "December"
        else -> ""
    }

}


fun Context.isEmailValid(email: String): Boolean {
    return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
}

fun getBitmap(v: View): Bitmap? {

    v.clearFocus()
    v.isPressed = false

    var willNotCache = v.willNotCacheDrawing()
    v.setWillNotCacheDrawing(false)
    var color = v.drawingCacheBackgroundColor

    v.drawingCacheBackgroundColor = 0

    if (color != 0) {
        v.destroyDrawingCache()
    }


    v.isDrawingCacheEnabled = true

    // this is the important code :)
    // Without it the view will have a dimension of 0,0 and the bitmap will be null
/*    v.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
    v.layout(0, 0, v.measuredWidth, v.measuredHeight)*/

    v.buildDrawingCache(true)
    var bitmap = Bitmap.createBitmap(v.drawingCache)
    v.isDrawingCacheEnabled = false


    //var cacheBitmap: Bitmap? = v.drawingCache ?: return null
    //var bitmap = Bitmap.createBitmap(cacheBitmap)
    v.destroyDrawingCache()
    v.setWillNotCacheDrawing(willNotCache)
    v.drawingCacheBackgroundColor = color
    return bitmap

}

fun getComingDates(days: Int): String {
    var calendar: Calendar = Calendar.getInstance()
    calendar.add(Calendar.DAY_OF_YEAR, days)
    var dateFormat = SimpleDateFormat("dd-MMM-yyyy")
    return convertDate(dateFormat.format(calendar.time))
}


fun convertDate(date: String): String {
    var splitedDate = date.split("-")
    var mYear = splitedDate[2]
    var mMonth = splitedDate[1]
    var mDay = splitedDate[0]

    return "$mMonth $mDay, $mYear"

}


fun Context.getDuration(uri: Uri): Long {
    var retriever = MediaMetadataRetriever()
    retriever.setDataSource(this, uri)
    var time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
    var timeInMillisec = time.toLong()
    retriever.release()
    return timeInMillisec
}

fun getTimeForSong(durationInMillis: Long): String {
    var millis = durationInMillis % 1000
    var second = (durationInMillis / 1000) % 60
    var minute = (durationInMillis / (1000 * 60)) % 60
    var hour = (durationInMillis / (1000 * 60 * 60)) % 24
    return String.format("%d:%02d", minute, second)

}


fun noTrailingwhiteLines(text: CharSequence): CharSequence {
    var stText = text
    while (stText.elementAt(stText.length - 1) == '\n') stText = stText.subSequence(0, stText.length - 1)
    while (stText.elementAt(stText.length - 2) == '\n') stText = stText.subSequence(0, stText.length - 2)
    return stText
}

fun onTokenExpiredLogout() {
//    SocialClass.logoutFaceBook()
//    var token = YNMSApp.db.getString(Constants.DEVICE_ID)
//    YNMSApp.db.clear()
//    YNMSApp.db.putString(Constants.DEVICE_ID, token!!)
//    var intent = Intent(YNMSMain.mActivity, YNMSAuth::class.java)
//    YNMSMain.mActivity.startActivity(intent)
//    YNMSApp.db.putBoolean(Constants.IS_LOGIN, false)
//    YNMSMain.mActivity.finish()
}


fun Context.showTokenExpireDialog(title: String, description: String) {
//    var successDialog = Dialog(this, R.style.Theme_Dialog)
//    successDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//    successDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//    successDialog.setContentView(R.layout.dialoge_success)
////    val btnOkay = successDialog.findViewById<Button>(R.id.btnOkay)
//    val ivClose = successDialog.findViewById<ImageView>(R.id.ivClose)
//    var tvTitle = successDialog.findViewById<TextView>(R.id.tvTitle)
//    var tvDescription = successDialog.findViewById<TextView>(R.id.tvDescription)
//    tvTitle.text = title
//    tvDescription.text = description
//    ivClose.setOnClickListener {
//        successDialog.dismiss()
//    }
//    btnOkay.setOnClickListener {
//        onTokenExpiredLogout()
//        successDialog.dismiss()
//    }
//    successDialog.show()
}

fun getFileName(wholePath: String): String {
    var name: String? = null
    val start: Int = wholePath.lastIndexOf('/')
    val end: Int = wholePath.length
    //lastIndexOf('.');
    name = wholePath.substring((start + 1), end)
    return name
}

fun getPrivateStorageDir(context: Context, folderName: String): File? {

    val file = File(context.filesDir, folderName)
    //val file = File(context.getExternalFilesDir(Environment.DIRECTORY_DCIM), albumName)
    if (!file.exists()) {
        if (!file.mkdir()) {
        }
    }
    return file
}

fun isFileExist(path: String): Boolean {
    val file = File(path)
    return file.exists()

}

fun fadOutHideView(view: View) {
    var fadeout = AlphaAnimation(1f, 0f)
    fadeout.interpolator = AccelerateInterpolator()
    fadeout.duration = 1000
    fadeout.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(animation: Animation?) {}
        override fun onAnimationEnd(animation: Animation?) {
            view.visibility = View.INVISIBLE
        }

        override fun onAnimationStart(animation: Animation?) {}
    })
    view.startAnimation(fadeout)
}

fun getColorWithAlpha(color: Int, ratio: Float): Int {
    return Color.argb(Math.round(Color.alpha(color) * ratio), Color.red(color), Color.green(color), Color.blue(color))
}

fun TextView.setSize() {
    this.textSize = 12f
}