package com.hooleyapp.hooley.others

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

open class GetBitmapTask : AsyncTask<String, Void, Bitmap>() {


    override fun doInBackground(vararg params: String): Bitmap? {

        return try {
            val url = URL(params[0])
            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input = connection.inputStream
            BitmapFactory.decodeStream(input)
        } catch (e: IOException) {
            e.printStackTrace()
            null
        }

    }
}
