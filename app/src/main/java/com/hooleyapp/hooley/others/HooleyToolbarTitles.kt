package com.hooleyapp.hooley.others

object HooleyToolbarTitles {


    //Fragment Names
    const val FRAGMENT_NOTIFICATION = "NotificationMainFragment"
    const val FRAGMENT_AboutHooleyFragment = "AboutHooleyFragment"


    //TOOL BAR TITILES
    const val TITLE_NOTIFICATION = "Alerts"


    fun getNotificationTitle(s: String): String {
        return when (s) {
            HooleyToolbarTitles.FRAGMENT_NOTIFICATION -> HooleyToolbarTitles.TITLE_NOTIFICATION
            "GuestUserEventDetailAnnouncementFragment" -> "Announcements"
            "DashBoardFragment" -> "Nearby Events"
            "BlockUserFragment" -> "Blocked Users"
            "GuestUserSearchFilterFragment" -> "Search"
            "SearchFilterFragment" -> "Search"
            "DashBoardFragment" -> "Nearby Events"
            "AboutHooleyFragment" -> "About"
            "CreateEventOptionsFragment" -> "Create Event"
            "AddFriendFromContactFragment" -> "Add Friends"
            "AddFriendFromHooley" -> "Add Friends"
            "GuestUserBookmarkEventsFragment" -> "Bookmark Event"
            "BookmarkEventsFragment" -> "Bookmark Event"
            "ViewTickerSalesFragment" -> "Scan Stats / Scan Tickets"
            "ProfileMainFragment" -> "My Profile"
            "EditPersonalProfileFragment" -> "Edit Personal Profile"
            "EditBusinessProfileFragment" -> "Edit Business Profile"
//            "BuyTicketFragment" -> "Buy Tickets"
            "ConfirmCreateEventFragment" -> "Hooley"
            "SuccessPaymentFragment" -> "Hooley"
            "ConfirmFeedbackFragment" -> "Feedback"
            "FeedbackFragment" -> "Feedback"
            "FaqsFragment" -> "FAQ's"
            "FeatureEventFragment" -> "Feature Event"
            "GeofenceFragment" -> "Set Geofence"
            "HooleyGalleryFragment" -> "Add Cover Photo"
            "InstagramAuthenticationFragment" -> "Instagram Authentication"
            "FlickrAuthFragment" -> "Flickr Authentication"
            "MyTicketFragment" -> "My Tickets"
            "LivePostFragment" -> "Upload Media"
            "InviteFragment" -> "Invite Guests"
            "PastGuestListFriendsFragment" -> "Invite Guests"
            "InviteHooleyFriendsFragment" -> "Invite Guests"
            "SendInvitePeopleNearbyMeFragment" -> "Invite Guests"
//            "ViewSalesFragment" -> "Ticket & PPV Sales"
            "PopularInFriendsFragment" -> "Popular in Friends Event"
            "MyEventMainFragment" -> "My Events"
            "PrivacyPolicyFragment" -> "Privacy Policy"
            "PastEventListFragment" -> "Add Friends"
            "PastEventListFriendFragment" -> "Add Friends"
            "StripeConnectFragment" -> "Connect to Stripe"
            "UpdateEventFragment" -> "Edit Basic Info"
            "TermsAndConditionFragment" -> "Terms of Use"
            "PreviewEventFragment" -> "Event Preview"
            "PromotionStatsFragment" -> "Promotion Stats"
            "PostPoneEventFragment" -> "Postpone Event"
            "MediaStreamMainFragment" -> "Media Stream"
            "MyGeoFenceFragment" -> "My Geofence"
            "AddEventAnnouncementFragment" -> "Add Announcement"
            "MyTicketRefundFragment" -> "Get Ticket Refund"
            "UpdatePostFragment" -> "Edit Post"
            else -> ""
        }
    }
}