package com.hooleyapp.hooley.others

import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.util.Log
import org.greenrobot.eventbus.EventBus
import java.util.*

class ApplicationSelectorReceiver : BroadcastReceiver() {
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    override fun onReceive(context: Context, intent: Intent) {
        for (key in Objects.requireNonNull<Bundle>(intent.extras).keySet()) {
            try {
                val componentInfo = intent.extras!!.get(key) as ComponentName
                val packageManager = context.packageManager
                assert(componentInfo != null)
                val appName = packageManager.getApplicationLabel(packageManager.getApplicationInfo(componentInfo.packageName, PackageManager.GET_META_DATA)) as String
                Log.i("SELECTED_APP", appName)
                EventBus.getDefault().post(MessageEvent(appName))
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }
}
