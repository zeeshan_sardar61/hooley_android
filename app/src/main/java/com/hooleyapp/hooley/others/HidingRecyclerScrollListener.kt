package com.hooleyapp.hooley.others

import android.content.Context
import android.support.v7.widget.RecyclerView

import com.hooleyapp.hooley.log.Logger

/**
 * Created by Nauman on 1/17/2018.
 */

abstract class HidingRecyclerScrollListener(context: Context, private val mHaderHight: Int)//        mHaderHight = (int) (HooleyMain.activity.getResources().getDimension(R.dimen.action_bar_btn_height));
    : RecyclerView.OnScrollListener() {

    private var mHeaderOffset = 0
    private var mControlsVisible = true

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)

        if (newState == RecyclerView.SCROLL_STATE_IDLE) {
            if (mControlsVisible) {
                if (mHeaderOffset > HIDE_THRESHOLD) {
                    setInvisible()
                } else {
                    setVisible()
                }
            } else {
                if (mHaderHight - mHeaderOffset > SHOW_THRESHOLD) {
                    setVisible()
                } else {
                    setInvisible()
                }
            }
        }

    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        Logger.e("scroll Y " + dy + "mheaderOffset " + mHeaderOffset)

        clipToolbarOffset()
        onMoved(mHeaderOffset)

        if (mHeaderOffset < mHaderHight && dy > 0 || mHeaderOffset > 0 && dy < 0) {
            mHeaderOffset += dy
        }

    }

    private fun clipToolbarOffset() {
        if (mHeaderOffset > mHaderHight) {
            mHeaderOffset = mHaderHight
        } else if (mHeaderOffset < 0) {
            mHeaderOffset = 0
        }
    }

    private fun setVisible() {
        if (mHeaderOffset > 0) {
            onShow()
            mHeaderOffset = 0
        }
        mControlsVisible = true
    }

    private fun setInvisible() {
        if (mHeaderOffset < mHaderHight) {
            onHide()
            mHeaderOffset = mHaderHight
        }
        mControlsVisible = false
    }

    abstract fun onMoved(distance: Int)

    abstract fun onShow()

    abstract fun onHide()

    companion object {

        private val HIDE_THRESHOLD = 10f
        private val SHOW_THRESHOLD = 70f
    }
}