package com.hooleyapp.hooley.others;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;

import com.hooleyapp.hooley.R;
import com.hooleyapp.hooley.activites.HooleyMain;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadTask extends AsyncTask<String, Integer, String> {

    String CHANNEL_ID = "Hooley_01";// The id of the channel.
    CharSequence name = HooleyMain.Companion.getActivity().getString(R.string.channel_name);
    int importance = NotificationManager.IMPORTANCE_HIGH;
    private Context context;
    private PowerManager.WakeLock mWakeLock;
    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;

    public DownloadTask(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... sUrl) {


        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(sUrl[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage();
            }

            int fileLength = connection.getContentLength();
            input = connection.getInputStream();
            String[] parts = sUrl[0].split("/");
            File saveFolder = new File(Environment.getExternalStorageDirectory(), "Hooley");
            if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                //RUNTIME PERMISSION Android M
                if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(HooleyMain.Companion.getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    saveFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Hooley");
                }
            }
            output = new FileOutputStream(saveFolder.getPath() + "/" + parts[parts.length - 1]);
            byte[] data = new byte[4096];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                if (isCancelled()) {
                    input.close();
                    return null;
                }
                total += count;
                if (fileLength > 0) // only if total length is known
                    publishProgress((int) (total * 100 / fileLength));
                output.write(data, 0, count);
            }
        } catch (Exception e) {
            return e.toString();
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // take CPU lock to prevent CPU from going off if the user
        // presses the power button during download
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                getClass().getName());
        mWakeLock.acquire();
        Intent intent = new Intent(context, HooleyMain.class);

        final PendingIntent pendingIntent = PendingIntent.getActivity(
                context, 0, intent, 0);
        mNotifyManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setContentTitle("Hooley")
                .setContentText("Downloading in progress...")
                .setSmallIcon(R.mipmap.app_icon)
                .setContentIntent(pendingIntent)
                .setChannelId(CHANNEL_ID);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mNotifyManager.createNotificationChannel(mChannel);

        }
        mNotifyManager.notify(0, mBuilder.build());

    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
    }

    @Override
    protected void onPostExecute(String result) {
        mWakeLock.release();
        if (result != null) {
            /*Toast.makeText(context,"Download error: "+result, Toast.LENGTH_LONG).show();*/
            if (mBuilder != null) {
                mBuilder.setContentText("Download Failed")
                        // Removes the progress bar
                        .setProgress(0, 0, false);
                mNotifyManager.notify(0, mBuilder.build());
            } else {
                mNotifyManager =
                        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                mBuilder = new NotificationCompat.Builder(context);
                mBuilder.setContentTitle("Hooley")
                        .setSmallIcon(R.mipmap.app_icon)
                        .setChannelId(CHANNEL_ID);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                    mNotifyManager.createNotificationChannel(mChannel);
                }
                mBuilder.setContentText("Download Failed")
                        // Removes the progress bar
                        .setProgress(0, 0, false);
                mNotifyManager.notify(0, mBuilder.build());
            }
        } else {
            /* Toast.makeText(context,"File downloaded", Toast.LENGTH_SHORT).show();*/
            if (mBuilder != null) {
                mBuilder.setContentText("Download complete")
                        .setProgress(0, 0, false);
                mNotifyManager.notify(0, mBuilder.build());
            } else {
                mNotifyManager =
                        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                mBuilder = new NotificationCompat.Builder(context);
                mBuilder.setContentTitle("Hooley")
                        .setSmallIcon(R.mipmap.app_icon)
                        .setChannelId(CHANNEL_ID);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                    mNotifyManager.createNotificationChannel(mChannel);
                }
                mBuilder.setContentText("Download complete")
                        // Removes the progress bar
                        .setProgress(0, 0, false);
                mNotifyManager.notify(0, mBuilder.build());

            }

        }
    }

}
