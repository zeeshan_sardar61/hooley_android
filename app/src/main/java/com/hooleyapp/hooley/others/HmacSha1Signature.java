package com.hooleyapp.hooley.others;

import android.util.Base64;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Formatter;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Nauman on 2/21/2018.
 */

public class HmacSha1Signature {
    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";
    private final static char[] hexArray = "0123456789abcdef".toCharArray();

    private static String toHexString(byte[] bytes) {
        Formatter formatter = new Formatter();

        for (byte b : bytes) {
            formatter.format("%02x", b);
        }

        return formatter.toString();
    }

//    public static void main(String[] args) throws Exception {
//        String hmac = calculateRFC2104HMAC("data", "key");
//
//        System.out.println(hmac);
//        assert hmac.equals("104152c5bfdca07bc633eebd46199f0255c9f49d");
//    }

    public static String calculateRFC2104HMAC(String data, String key)
            throws NoSuchAlgorithmException, InvalidKeyException {
        SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);
        Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
        mac.init(signingKey);
        return toHexString(mac.doFinal(data.getBytes()));
    }

    public static String hmacSha1(String value, String key)
            throws NoSuchAlgorithmException,
            InvalidKeyException {
        String type = "HmacSHA1";
        SecretKeySpec secret = new SecretKeySpec(key.getBytes(), type);
        Mac mac = Mac.getInstance(type);
        mac.init(secret);
        byte[] bytes = mac.doFinal(value.getBytes());
        return bytesToHex(bytes);
    }

    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }


    public static String signatureEncoding(String data, String key) throws java.security.SignatureException {
        String HMAC_SHA1_ALGORITHM = "HmacSHA1";
        String result;
        try {
            SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);
            Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(data.getBytes());
            result = Base64.encodeToString(rawHmac, Base64.URL_SAFE);
        } catch (Exception e) {
            throw new SignatureException("Failed to generate HMAC : " + e.getMessage());
        }
        return result;
    }


    static String hash_hmac(String type, String value, String key) {
//        try {
//            javax.crypto.Mac mac = javax.crypto.Mac.getInstance(type);
//            javax.crypto.spec.SecretKeySpec secret = new javax.crypto.spec.SecretKeySpec(key.getBytes(), type);
//            mac.init(secret);
//            byte[] digest = mac.doFinal(value.getBytes());
//            StringBuilder sb = new StringBuilder(digest.length * 2);
//            String s;
//            for (byte b : digest) {
////                s = Integer.toHexString(intval(b));
//                if (s.length() == 1) sb.append('0');
//                sb.append(s);
//            }
//            return sb.toString();
//        } catch (Exception e) {
//            android.util.Log.v("TAG", "Exception [" + e.getMessage() + "]", e);
//        }
        return "";
    }

}