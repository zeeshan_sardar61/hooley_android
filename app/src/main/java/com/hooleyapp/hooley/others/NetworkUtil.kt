package com.hooleyapp.hooley.others

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.net.ConnectivityManager
import java.io.IOException

@SuppressLint("StaticFieldLeak")
object NetworkUtil {
    private val mContext: Activity? = null
    private var mAlertDialogBuilder: AlertDialog.Builder? = null
    private var mDialog: Dialog? = null

    /**
     * Checks if internet is connected/available by pinning google.com
     *
     * @return boolean i.e. true if internet is connected and false otherwise.
     */
    val isOnline: Boolean
        get() {
            val runtime = Runtime.getRuntime()
            try {

                val ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8")
                val exitValue = ipProcess.waitFor()
                return exitValue == 0

            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }

            return false

        }

    fun isInternetConnected(context: Activity): Boolean {
        var status = false
        try {
            val cm = context
                    .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            if (null != activeNetwork) {

                if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                    status = true
                }
                if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                    status = true
                }
                status = activeNetwork != null && activeNetwork.isConnected
            }
        } catch (e: Exception) {
            status = false
        }

        return status
    }

    fun isInternetConnected(context: Context): Boolean {
        var status = false
        try {
            val cm = context
                    .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            if (null != activeNetwork) {

                if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                    status = true
                }
                if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                    status = true
                }
                status = activeNetwork != null && activeNetwork.isConnected
            }
        } catch (e: Exception) {
            status = false
        }

        return status
    }

    /**
     * @param context
     */
    fun internetFailedDialog(context: Context) {
        try {
            if (mAlertDialogBuilder != null && mDialog != null && mDialog!!.isShowing) {
                mDialog!!.cancel()
                mDialog!!.dismiss()
                mDialog = null
                mAlertDialogBuilder = null
            }
            mAlertDialogBuilder = AlertDialog.Builder(
                    context)
            mAlertDialogBuilder!!.setTitle("Internet Problem")
            mAlertDialogBuilder!!
                    .setMessage("Please check your internet connection than try again!")
            mAlertDialogBuilder!!.setIcon(android.R.drawable.ic_dialog_alert)
            mAlertDialogBuilder!!.setPositiveButton("OK") { dialog, which ->
                dialog.dismiss()
                dialog.cancel()
                mAlertDialogBuilder = null
            }
            mDialog = mAlertDialogBuilder!!.create()
            mDialog!!.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * @param act
     * @param message
     * @param
     */
    fun showStatusDialog(act: Activity, title: String, message: String, isToken: Boolean) {

        val alertDialogBuilder = AlertDialog.Builder(
                act)
        alertDialogBuilder.setTitle(title)
        alertDialogBuilder.setMessage(message)
        alertDialogBuilder.setIcon(android.R.drawable.ic_dialog_alert)
        alertDialogBuilder.setPositiveButton("OK") { dialog, which ->
            dialog.dismiss()
            dialog.cancel()
        }

        alertDialogBuilder.create().show()

    }
}
