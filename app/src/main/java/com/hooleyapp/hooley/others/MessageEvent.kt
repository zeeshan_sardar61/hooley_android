package com.hooleyapp.hooley.others

class MessageEvent {
    var message: String = ""
    var fromNotification: Boolean = false
    var pic: String = "PIC"

    constructor(messageF: String) {
        message = messageF
    }

    constructor(messageF: String, pic_: String) {
        message = messageF
        pic = pic_
    }


    constructor(messageF: String, fBool: Boolean) {
        message = messageF
        fromNotification = fBool
    }

}
