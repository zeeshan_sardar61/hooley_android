package com.hooleyapp.hooley.services

import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GetInviteContactModel
import com.hooleyapp.hooley.model.PhoneContactsModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.requestmodel.ConstactSyncRequestModel
import com.hooleyapp.hooley.requestmodel.SendContactInviteRequestModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException
import java.util.*

class ContactWebService {

    /**
     * @param eventId
     * @param callback
     */
    fun getInviteContactList(eventId: String, callback: IWebServiceCallback<GetInviteContactModel>) {
        val call = HooleyApp.apiService.getContactInviteList(HooleyApp.db.getString(Constants.USER_ID), eventId)
        call.enqueue(object : Callback<GetInviteContactModel> {
            override fun onResponse(call: Call<GetInviteContactModel>, response: Response<GetInviteContactModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getInviteContactList:" + Gson().toJson(response))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    Logger.ex("getInviteContactList : Server Exception:" + response.body()!!.exception)
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true))
                        callback.onTokenExpire()
                    else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GetInviteContactModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }

            }
        })
    }

    /**
     * @param eventId
     * @param mlist
     * @param callback
     */

    fun sendInviteToContact(eventId: String, mlist: List<PhoneContactsModel>?, callback: IWebServiceCallback<GeneralModel>) {
        val contactNumberList = ArrayList<SendContactInviteRequestModel.myObject>()
        if (mlist != null && mlist.size > 0) {
            for (i in mlist.indices) {
                if (mlist[i].checked!!)
                    contactNumberList.add(SendContactInviteRequestModel.myObject(mlist[i].cnName, mlist[i].contactE164Number))
            }
        }
        val call = HooleyApp.apiService.sendInviteToContact(SendContactInviteRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId, contactNumberList))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("sendInviteToContact:" + Gson().toJson(response))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    Logger.ex("sendInviteToContact : Server Exception:" + response.body()!!.exception)
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true))
                        callback.onTokenExpire()
                    else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })

    }

    /**
     * @param mlist
     * @param callback
     */
    fun sendContactToServer(mlist: List<PhoneContactsModel>?, callback: IWebServiceCallback<GeneralModel>) {
        val contactArrayList = ArrayList<ConstactSyncRequestModel.UserContact>()
        if (mlist != null && mlist.size > 0) {
            for (i in mlist.indices) {
                contactArrayList.add(ConstactSyncRequestModel.UserContact(mlist[i].cnName, mlist[i].contactE164Number))
            }
        }
        val call = HooleyApp.apiService.sycUserContact(ConstactSyncRequestModel(HooleyApp.db.getString(Constants.USER_ID), contactArrayList))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("sendContactToServer:" + Gson().toJson(response))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    Logger.ex("sendContactToServer : Server Exception:" + response.body()!!.exception)
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true))
                        callback.onTokenExpire()
                    else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

}
