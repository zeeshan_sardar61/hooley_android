package com.hooleyapp.hooley.services

import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.app.data.model.event.requestmodel.EditTicketRequest
import com.hooleyapp.hooley.app.data.model.event.requestmodel.EnableDisablePPV
import com.hooleyapp.hooley.app.data.model.event.requestmodel.TicketListItem
import com.hooleyapp.hooley.app.data.model.event.responseModel.GetEditTicketResponse
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.*
import com.hooleyapp.hooley.model.ticketStats.TicketStatsResponse
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.requestmodel.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException
import java.util.*


class TicketWebService {

    fun buyTicket(eventId: String, totalTickets: Int, stripeToken: String, eventTicketId: String, type: String, donateAmount: Int, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.buyTicket(BuyTicketRequestModel(
                HooleyApp.db.getString(Constants.USER_ID), eventId, totalTickets, stripeToken, eventTicketId, type, donateAmount))

        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("buyTicket " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun sendTicketGift(friendId: Int, eventTicketId: ArrayList<Int>, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.sendGiftTicket(SendTicketGiftRequestModel(friendId, HooleyApp.db.getString(Constants.USER_ID), eventTicketId))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("sendTicketGift " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun refundTicket(eventId: String, eventTicketId: Int?, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.refundTicket(RefundTicketModel(HooleyApp.db.getString(Constants.USER_ID), eventId, eventTicketId.toString()))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("sendTicketGift " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getTicketByEvent(eventId: String, callback: GenericCallback<GetTicketByEventModel>) {
        val call = HooleyApp.apiService.getTicketByEvent(HooleyApp.db.getString(Constants.USER_ID), eventId)
        call.enqueue(object : Callback<GetTicketByEventModel> {
            override fun onResponse(call: Call<GetTicketByEventModel>, response: Response<GetTicketByEventModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getTicketByEvent " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception)
                }
            }

            override fun onFailure(call: Call<GetTicketByEventModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

//    fun saveTicket(eventId: String,
//                   shareIncomePartnerList: ArrayList<ShareIncomePartnerModel>,
//                   ticketCategoriesList: ArrayList<TicketCategoriesModel>, callback: GenericCallback<GeneralModel>) {
//
//        val ticketInfoArrayList = ArrayList<SaveTicketRequestModel.TicketObj.TicketInfo>()
//        val incomeShareArrayList = ArrayList<SaveTicketRequestModel.TicketObj.IncomeShare>()
//        for (i in ticketCategoriesList.indices) {
//            ticketInfoArrayList.add(SaveTicketRequestModel.TicketObj.TicketInfo(ticketCategoriesList[i].ticketName, ticketCategoriesList[i].tickeQuantity, ticketCategoriesList[i].ticketPrice))
//        }
//        for (i in shareIncomePartnerList.indices) {
//            if (shareIncomePartnerList[i].percent != 0)
//                incomeShareArrayList.add(SaveTicketRequestModel.TicketObj.IncomeShare(shareIncomePartnerList[i].friendsTable.userId.toString(), shareIncomePartnerList[i].percent))
//        }
//        val ticketObj = SaveTicketRequestModel.TicketObj(DateUtils.SendDateToDate(DateUtils.getDateInUtc(onlineSaleStart)), DateUtils.SendDateToDate(DateUtils.getDateInUtc(onlineSaleEnd)), ticketInfoArrayList, incomeShareArrayList)
//
//        val call = HooleyApp.apiService.saveTicket(SaveTicketRequestModel(ticketObj))
//        call.enqueue(object : Callback<GeneralModel> {
//            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
//                if (response.body() == null) {
//                    callback.failure("Server not responding Please try again later")
//                    return
//                }
//                Logger.ex("saveTicket " + Gson().toJson(response.body()!!))
//                if (response.body()!!.success) {
//                    callback.success(response.body()!!)
//                } else {
//                    callback.failure(response.body()!!.exception!!)
//                }
//            }
//
//            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
//                if (t is UnknownHostException || t is IOException) {
//                    if (t.message.equals("timeout", ignoreCase = true)) {
//                        callback.failure(Constants.TIMEOUT)
//                    } else
//                        callback.failure(Constants.NO_NETWORK)
//                } else {
//                    callback.failure(t.message!!)
//                }
//            }
//        })
//    }

//    fun editTicket(ticketModel: GetTicketByEventModel, callback: GenericCallback<GeneralModel>) {
//        val ticketLists = ArrayList<EditTicketRequestModel.TicketList>()
//        for (i in ticketModel.ticketObj!!!!.ticketInfoList.indices) {
//            ticketLists.add(EditTicketRequestModel.TicketList(ticketModel.ticketObj!!!!.ticketInfoList[i].ticketId, ticketModel.ticketObj!!.ticketInfoList[i].quantity, ticketModel.ticketObj!!.ticketInfoList[i].price))
//        }
//        val call = HooleyApp.apiService.editTicket(EditTicketRequestModel(ticketModel.ticketObj!!.eventId, ticketModel.ticketObj!!.onlineSaleStart,
//                ticketModel.ticketObj!!.onlineSaleEnd, HooleyApp.db.getString(Constants.USER_ID), ticketLists))
//        call.enqueue(object : Callback<GeneralModel> {
//            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
//                if (response.body() == null) {
//                    callback.failure("Server not responding Please try again later")
//                    return
//                }
//                Logger.ex("editTicket " + Gson().toJson(response.body()!!))
//                if (response.body()!!.success) {
//                    callback.success(response.body()!!)
//                } else {
//                    callback.failure(response.body()!!.exception!!)
//                }
//            }
//
//            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
//                if (t is UnknownHostException || t is IOException) {
//                    if (t.message.equals("timeout", ignoreCase = true)) {
//                        callback.failure(Constants.TIMEOUT)
//                    } else
//                        callback.failure(Constants.NO_NETWORK)
//                } else {
//                    callback.failure(t.message!!)
//                }
//            }
//        })
//    }

    fun getMyTickets(callback: GenericCallback<MyTicketModel>) {
        val call = HooleyApp.apiService.getMyTickets(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<MyTicketModel> {
            override fun onResponse(call: Call<MyTicketModel>, response: Response<MyTicketModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getTicketByEvent " + Gson().toJson(response.body()!!))
                if (response.body()?.isSuccessful!!) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception)
                }
            }

            override fun onFailure(call: Call<MyTicketModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getSendGift(callback: GenericCallback<SendGiftModel>) {
        val call = HooleyApp.apiService.getSendGift(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<SendGiftModel> {
            override fun onResponse(call: Call<SendGiftModel>, response: Response<SendGiftModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getSendGift " + Gson().toJson(response.body()!!))
                if (response.body()?.isSuccessful!!) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception)
                }
            }

            override fun onFailure(call: Call<SendGiftModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun setTicketAlert(ticketId: Int, eventId: Int, date: String, isAlert: Boolean, callback: GenericCallback<SetAlertModel>) {
        val call = HooleyApp.apiService.setTicketAlert(SetTicketAlertRequestModel(HooleyApp.db.getString(Constants.USER_ID), ticketId, eventId, DateUtils.getDateInUtc(date), isAlert))
        call.enqueue(object : Callback<SetAlertModel> {
            override fun onResponse(call: Call<SetAlertModel>, response: Response<SetAlertModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("setTicketAlert " + Gson().toJson(response.body()!!))
                if (response.body()!!.IsSuccessful) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<SetAlertModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getTicketTerms(callback: GenericCallback<GetTermsConditionModel>) {
        val call = HooleyApp.apiService.getTicketTermsCondition(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<GetTermsConditionModel> {
            override fun onResponse(call: Call<GetTermsConditionModel>, response: Response<GetTermsConditionModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getTicketTerms " + Gson().toJson(response.body()!!))
                if (response.body()!!.isSuccessful) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception)
                }
            }

            override fun onFailure(call: Call<GetTermsConditionModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun scanTicket(eventId: String, eventName: String, price: String, startTime: String, endTime: String, ticketId: String, callback: GenericCallback<ScanTicketResponseModel>) {
        val call = HooleyApp.apiService.scanTicket(ScanTicketRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId,
                ScanTicketRequestModel.QrScanInfo(eventName, price, startTime, endTime, ticketId)))
        call.enqueue(object : Callback<ScanTicketResponseModel> {
            override fun onResponse(call: Call<ScanTicketResponseModel>, response: Response<ScanTicketResponseModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getTicketTerms " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<ScanTicketResponseModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun viewTicketSales(eventId: String, callback: GenericCallback<TicketStatsResponse>) {
        val call = HooleyApp.apiService.viewTicketSales(HooleyApp.db.getString(Constants.USER_ID), eventId)
        call.enqueue(object : Callback<TicketStatsResponse> {
            override fun onResponse(call: Call<TicketStatsResponse>, response: Response<TicketStatsResponse>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("viewTicketSales " + Gson().toJson(response.body()!!))
                if (response.body()?.isSuccessful!!) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<TicketStatsResponse>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun addCard(stripeToken: String, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.addCard(AddCardRequest(HooleyApp.db.getString(Constants.USER_ID), stripeToken))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }

            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()?.success!!) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }
        })

    }

    fun getCards(callback: GenericCallback<CardDetailsModel>) {
        val call = HooleyApp.apiService.geCardDetails(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<CardDetailsModel> {
            override fun onFailure(call: Call<CardDetailsModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }

            override fun onResponse(call: Call<CardDetailsModel>, response: Response<CardDetailsModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()?.isSuccessful!!) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }
        })

    }

    fun deleteCard(callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.deleteCard(LogoutRequestModel(HooleyApp.db.getString(Constants.USER_ID)))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }

            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()?.success!!) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }
        })

    }

    fun getEditTicket(eventId: String, callback: GenericCallback<GetEditTicketResponse>) {
        val call = HooleyApp.apiService.getEditTicket(HooleyApp.db.getString(Constants.USER_ID), eventId)
        call.enqueue(object : Callback<GetEditTicketResponse> {
            override fun onResponse(call: Call<GetEditTicketResponse>, response: Response<GetEditTicketResponse>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getEditTicket " + Gson().toJson(response.body()!!))
                if (response.body()!!.isSuccessful!!) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GetEditTicketResponse>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun saveEditTicket(eventId: String, list: ArrayList<TicketListItem?>, ppv: String, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.saveEditTicket(EditTicketRequest(eventId, list, ppv, HooleyApp.db.getString(Constants.USER_ID)))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }

            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()?.success!!) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }
        })

    }


    fun enableDisablePPV(eventId: String, isEnable: Boolean, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.enableDisablePPV(EnableDisablePPV(eventId, isEnable, HooleyApp.db.getString(Constants.USER_ID)))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }

            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()?.success!!) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }
        })

    }

}
