package com.hooleyapp.hooley.services

import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.NearByEventMapFeedModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.requestmodel.SeeAllNearByEventMapViewPolygonFeedsModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException
import java.util.*

class GeoFenceWebService {


    /**
     * @param lat
     * @param lang
     * @param radius
     * @param callback
     */
    fun seeAllNearByEventMapViewFeeds(lat: Double, lang: Double, radius: Float, callback: IWebServiceCallback<NearByEventMapFeedModel>) {
        val call = HooleyApp.apiService.seeAllNearByEventMapViewFeeds(HooleyApp.db.getString(Constants.USER_ID), lat, lang, radius, HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN))
        call.enqueue(object : Callback<NearByEventMapFeedModel> {
            override fun onResponse(call: Call<NearByEventMapFeedModel>, response: Response<NearByEventMapFeedModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("seeAllNearByEventMapViewFeeds : " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<NearByEventMapFeedModel>, t: Throwable) {
                try {
                    if (t is UnknownHostException || t is IOException) {
                        if (t.message.equals("timeout", ignoreCase = true)) {
                            callback.failure(Constants.TIMEOUT)
                        } else
                            callback.failure(Constants.NO_NETWORK)
                    } else {
                        callback.failure(t.message!!)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        })
    }

    fun seeAllNearByEventMapViewPolygonFeeds(mLatLongList: ArrayList<LatLng>, callback: GenericCallback<NearByEventMapFeedModel>) {
        val mList = ArrayList<SeeAllNearByEventMapViewPolygonFeedsModel.GeoFenceList>()
        for (i in mLatLongList.indices) {
            mList.add(SeeAllNearByEventMapViewPolygonFeedsModel.GeoFenceList(mLatLongList[i].latitude, mLatLongList[i].longitude))
        }
        val call = HooleyApp.apiService.seeAllNearByEventMapViewPolygonFeeds(SeeAllNearByEventMapViewPolygonFeedsModel(HooleyApp.db.getString(Constants.USER_ID), mList))
        call.enqueue(object : Callback<NearByEventMapFeedModel> {
            override fun onResponse(call: Call<NearByEventMapFeedModel>, response: Response<NearByEventMapFeedModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("seeAllNearByEventMapViewPolygonFeeds : " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<NearByEventMapFeedModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


}


