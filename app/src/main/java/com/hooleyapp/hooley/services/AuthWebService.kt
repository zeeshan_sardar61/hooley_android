package com.hooleyapp.hooley.services

import android.text.TextUtils
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.LoginModel
import com.hooleyapp.hooley.model.staticData.StaticDataModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.requestmodel.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException

class AuthWebService {

    /**
     * @param callback
     */
    fun logoutUser(callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.logOutUser(LogoutRequestModel(HooleyApp.db.getString(Constants.USER_ID)))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("logoutUser: " + Gson().toJson(response.body()))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param loginMedia
     * @param callback
     */
    fun loginWithSMUser(loginMedia: String, callback: GenericCallback<LoginModel>) {
        val isParamCompleted: Boolean
        val token = HooleyApp.db.getString(Constants.GCM_REGISTRATION_TOKEN)
        val email = HooleyApp.db.getString(Constants.SOCIAL_USER_EMAIL)
        val userId = HooleyApp.db.getString(Constants.SOCIAL_ID)
        val firstName = HooleyApp.db.getString(Constants.SOCIAL_USER_FIRST_NAME)
        val lastName = HooleyApp.db.getString(Constants.SOCIAL_USER_LAST_NAME)
        val profilePic = HooleyApp.db.getString(Constants.SOCIAL_USER_PROFILE_PIC)
        val deviceType = Constants.DEVICE_TYPE
        isParamCompleted = !TextUtils.isEmpty(email)
        val call = HooleyApp.apiService.loginWithSM(SocialLoginRequestModel(email, userId, token, firstName, lastName, profilePic, deviceType, loginMedia, HooleyApp.db.getString(Constants.SOCIAL_ID), isParamCompleted))
        call.enqueue(object : Callback<LoginModel> {
            override fun onResponse(call: Call<LoginModel>, response: Response<LoginModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("loginWithSMUser: " + Gson().toJson(response.body()))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<LoginModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param email
     * @param password
     * @param callback
     */
    fun loginUser(email: String, password: String, callback: GenericCallback<LoginModel>) {
        val token = HooleyApp.db.getString(Constants.GCM_REGISTRATION_TOKEN)
        val deviceType = Constants.DEVICE_TYPE

        val call = HooleyApp.apiService.loginUser(LoginUserRequestModel(email, password, token, deviceType))
        call.enqueue(object : Callback<LoginModel> {
            override fun onResponse(call: Call<LoginModel>, response: Response<LoginModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("loginUser: " + Gson().toJson(response.body()))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<LoginModel>, t: Throwable) {

                if (t is UnknownHostException || t is IOException) {
                    if (t.message == null) {
                        callback.failure(Constants.TIMEOUT)
                    } else if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    /**
     * @param email
     * @param callback
     */
    fun forgetPassword(email: String, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.forgotPassword(ForgetPasswordRequestModel(email))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("forgetPassword: " + Gson().toJson(response.body()))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getStaticData(userId: String, version: String, callback: GenericCallback<StaticDataModel>) {
        val call = HooleyApp.apiService.getStaticData(userId, version)
        call.enqueue(object : Callback<StaticDataModel> {
            override fun onFailure(call: Call<StaticDataModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }

            override fun onResponse(call: Call<StaticDataModel>, response: Response<StaticDataModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("registerUser: " + Gson().toJson(response.body()))
                if (response.body()!!.isSuccessful!!) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

        })
    }

    /**
     * @param firstName
     * @param lastName
     * @param email
     * @param password
     * @param gender
     * @param callback
     */
    fun registerUser(firstName: String, lastName: String, email: String, password: String, profilePic: String, loginMedia: String, isEmailFound: Boolean, socialId: String, socialToken: String, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.registerUser(RegisterRequestModel(firstName, lastName, email, password, profilePic, loginMedia, isEmailFound, socialId, socialToken))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("registerUser: " + Gson().toJson(response.body()))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun setDarkTheme(isDark: Boolean, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.setDarkTheme(SetDarkThemeRequestModel(HooleyApp.db.getString(Constants.USER_ID), isDark))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("setDarkTheme :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

}
