package com.hooleyapp.hooley.services

import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.*
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.requestmodel.SeenNotificationRequestModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException

class NotificationWebService {


    fun getMessageAlterNotification(callback: GenericCallback<MessageAlertNotificationModel>) {
        val call = HooleyApp.apiService.getMessageAlterNotification(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<MessageAlertNotificationModel> {
            override fun onResponse(call: Call<MessageAlertNotificationModel>, response: Response<MessageAlertNotificationModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("get Message Alter Notification: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<MessageAlertNotificationModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     *
     * @param callback
     */
    fun getEventInvitesNotification(callback: GenericCallback<EventInviteNotificationModel>) {
        val call = HooleyApp.apiService.getEventInvitesNotification(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<EventInviteNotificationModel> {
            override fun onResponse(call: Call<EventInviteNotificationModel>, response: Response<EventInviteNotificationModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("get Event Invites Notification: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<EventInviteNotificationModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     *
     * @param callback
     */
    fun getMediaAlterNotification(callback: GenericCallback<MediaAlertNotificationModel>) {
        val call = HooleyApp.apiService.getMediaAlterNotification(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<MediaAlertNotificationModel> {
            override fun onResponse(call: Call<MediaAlertNotificationModel>, response: Response<MediaAlertNotificationModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("get Media Alter Notification " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<MediaAlertNotificationModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     *
     * @param callback
     */
    fun getFriendRequestNotification(callback: GenericCallback<FriendRequestNotificationModel>) {
        val call = HooleyApp.apiService.getFriendRequestNotification(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<FriendRequestNotificationModel> {
            override fun onResponse(call: Call<FriendRequestNotificationModel>, response: Response<FriendRequestNotificationModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("get Friend Request Notification: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<FriendRequestNotificationModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     *
     * @param type
     * @param callback
     */
    fun setSeenCount(type: String, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.setSeenCount(SeenNotificationRequestModel(HooleyApp.db.getString(Constants.USER_ID), type))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("set Seen Count: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getRecentData(callback: GenericCallback<NotificationCountModel>) {
        val call = HooleyApp.apiService.getRecentData(HooleyApp.db.getString(Constants.USER_ID), Constants.DEVICE_TYPE)
        call.enqueue(object : Callback<NotificationCountModel> {
            override fun onResponse(call: Call<NotificationCountModel>, response: Response<NotificationCountModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("get Notification Count " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<NotificationCountModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }
}
