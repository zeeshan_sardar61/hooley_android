package com.hooleyapp.hooley.services

import android.net.Uri
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.helper.getFilePathFromContentUri
import com.hooleyapp.hooley.helper.getUriFrombitmap
import com.hooleyapp.hooley.helper.handleSamplingAndRotationBitmap
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.UploadImageModel
import com.hooleyapp.hooley.others.Constants
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.net.UnknownHostException

/**
 * Created by adilmalik on 10/05/2018.
 */

class ImageUploadWebService {


    /**
     * @param callback
     */
    fun uploadImage(mediaUri: Uri, desFolder: String, isVideo: Boolean, callback: GenericCallback<UploadImageModel>) {
        var video: String = if (isVideo) "True" else "False"
        var mediaType: String = if (isVideo) "video/mp4" else "image/jpeg"
        var file: File = if (isVideo) {
            File(mediaUri.path)
        } else {
            File(HooleyMain.activity!!.getFilePathFromContentUri(HooleyMain.activity!!.getUriFrombitmap(HooleyMain.activity!!.handleSamplingAndRotationBitmap(mediaUri)!!), HooleyMain.activity!!.contentResolver))
        }
        val reqFile = RequestBody.create(MediaType.parse(mediaType), file)
        val fileBody = MultipartBody.Part.createFormData("file", file.name, reqFile)
        val destinationFolder = RequestBody.create(MediaType.parse("text/plain"), desFolder)
        val flag = RequestBody.create(MediaType.parse("text/plain"), video)
        val userId = RequestBody.create(MediaType.parse("text/plain"), HooleyApp.db.getString(Constants.USER_ID))
        val call = HooleyApp.apiService.uploadPersonalProfileImage(fileBody, destinationFolder, flag, userId)
        call.enqueue(object : Callback<UploadImageModel> {
            override fun onResponse(call: Call<UploadImageModel>, response: Response<UploadImageModel>) {

                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("uploadImage:" + Gson().toJson(response.body()))
                callback.success(response.body()!!)

            }

            override fun onFailure(call: Call<UploadImageModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun sendMediaMessage(mediaUri: Uri, threadId: String, isVideo: Boolean, callback: GenericCallback<UploadImageModel>) {
        var mediaType: String = if (isVideo) "video/mp4" else "image/jpeg"
        var fileName: String = if (isVideo) "video.mp4" else "photo.jpg"
        var file: File = if (isVideo) {
            File(mediaUri.path)
        } else {
            File(HooleyMain.activity!!.getFilePathFromContentUri(HooleyMain.activity!!.getUriFrombitmap(HooleyMain.activity!!.handleSamplingAndRotationBitmap(mediaUri)!!), HooleyMain.activity!!.contentResolver))
        }
        val reqFile = RequestBody.create(MediaType.parse(mediaType), file)
        val fileBody = MultipartBody.Part.createFormData("files", fileName, reqFile)
        val threadId = RequestBody.create(MediaType.parse("text/plain"), threadId)
        val userId = RequestBody.create(MediaType.parse("text/plain"), HooleyApp.db.getString(Constants.USER_ID))
        val call = HooleyApp.apiService.sendMediaMessage(fileBody, userId, threadId)
        call.enqueue(object : Callback<UploadImageModel> {
            override fun onResponse(call: Call<UploadImageModel>, response: Response<UploadImageModel>) {

                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("sendMediaMessage:" + Gson().toJson(response.body()))
                callback.success(response.body()!!)

            }

            override fun onFailure(call: Call<UploadImageModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

}
