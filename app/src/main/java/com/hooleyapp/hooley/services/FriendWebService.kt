package com.hooleyapp.hooley.services

import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.*
import com.hooleyapp.hooley.model.friends.GetFriendOfFriendModel
import com.hooleyapp.hooley.model.friends.HooleyUserModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.requestmodel.AcceptDeclineFriendRequestModel
import com.hooleyapp.hooley.requestmodel.AddFriendRequestModel
import com.hooleyapp.hooley.requestmodel.BlockFriendrequestModel
import com.hooleyapp.hooley.requestmodel.CreateThreadRequestModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException
import java.util.*

/**
 * Created by Nauman on 3/8/2018.
 */

class FriendWebService {

    /**
     * @param callback
     */
    fun getMyFriendsList(callback: GenericCallback<FriendsModel>) {
        val call = HooleyApp.apiService.getMyFriendsList(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<FriendsModel> {
            override fun onResponse(call: Call<FriendsModel>, response: Response<FriendsModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("objectResponseFriends:" + Gson().toJson(response))
                if (response.body()!!.success) {
                    Logger.ex("objectResponseFriends:" + Gson().toJson(response))
                    callback.success(response.body()!!)
                } else {
                    Logger.ex("getMyFriendsList : Server Exception:" + response.body()!!.exception!!)
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<FriendsModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    try {
                        if (t.message.equals("timeout", ignoreCase = true)) {
                            callback.failure(Constants.TIMEOUT)
                        } else
                            callback.failure(Constants.NO_NETWORK)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {
                    callback.failure(t.message!!)
                }

            }
        })
    }

    /**
     * @param callback
     */
    fun getMyPendingFriendRequest(callback: GenericCallback<PendingFriendRequestModel>) {
        val call = HooleyApp.apiService.getMYPendingFreindRequest(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<PendingFriendRequestModel> {
            override fun onResponse(call: Call<PendingFriendRequestModel>, response: Response<PendingFriendRequestModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                    Logger.ex("SeeAllEventFeeds:" + Gson().toJson(response))
                } else {
                    Logger.ex("getMyPendingFriendRequest : Server Exception:" + response.body()!!.exception!!)
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<PendingFriendRequestModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param callback
     */
    fun acceptIgnoreFriendRequest(friendId: Int, isAccept: Boolean, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.acceptDeclineFriendRequest(AcceptDeclineFriendRequestModel(HooleyApp.db.getString(Constants.USER_ID), friendId, isAccept))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                    Logger.ex("acceptIgnoreFriendRequest:" + Gson().toJson(response))
                } else {
                    Logger.ex("Server Exception:" + response.body()!!.exception!!)
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param callback
     */
    fun unFriend(friendId: Int, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.unFriend(CreateThreadRequestModel(HooleyApp.db.getString(Constants.USER_ID), friendId.toString()))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                    Logger.ex("unFriend:" + Gson().toJson(response))
                } else {
                    Logger.ex("Server Exception:" + response.body()!!.exception!!)
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param callback
     */
    fun sendFriendRequest(friendId: ArrayList<Int>, callback: GenericCallback<GeneralModel>) {
        val addFriends = ArrayList<AddFriendRequestModel.AddFriend>()
        for (i in friendId.indices) {
            val addFriend = AddFriendRequestModel.AddFriend()
            addFriend.friendId = friendId[i]
            addFriends.add(addFriend)

        }

        val call = HooleyApp.apiService.sendFriendRequest(AddFriendRequestModel(HooleyApp.db.getString(Constants.USER_ID), addFriends))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                    Logger.ex("unFriend:" + Gson().toJson(response))
                } else {
                    Logger.ex("Server Exception:" + response.body()!!.exception!!)
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    /**
     * @param eventId
     * @param callback
     */
    fun getPastGuestList(eventId: String, callback: GenericCallback<GetPastEventGuestListModel>) {
        val call = HooleyApp.apiService.getPastEventGuestList(HooleyApp.db.getString(Constants.USER_ID), eventId)
        call.enqueue(object : Callback<GetPastEventGuestListModel> {
            override fun onResponse(call: Call<GetPastEventGuestListModel>, response: Response<GetPastEventGuestListModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                    Logger.ex("getPastGuestList Friends:" + Gson().toJson(response))
                } else {
                    Logger.ex("Server Exception:" + response.body()!!.exception!!)
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GetPastEventGuestListModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param friendId
     * @param callback
     */
    fun seeFriendProfile(friendId: Int, callback: GenericCallback<FriendProfileModel>) {
        val call = HooleyApp.apiService.seeFriendProfile(HooleyApp.db.getString(Constants.USER_ID), Integer.toString(friendId))
        call.enqueue(object : Callback<FriendProfileModel> {
            override fun onResponse(call: Call<FriendProfileModel>, response: Response<FriendProfileModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                    Logger.ex("getPastGuestList Friends:" + Gson().toJson(response))
                } else {
                    Logger.ex("Server Exception:" + response.body()!!.exception!!)
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<FriendProfileModel>, t: Throwable) {
                Logger.ex("ERROR:" + t.message!!)
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    /**
     * @param friendId
     * @param callback
     */
    fun getTagUser(eventId: String, imageId: String, callback: GenericCallback<TagUserModel>) {
        val call = HooleyApp.apiService.getTagUser(HooleyApp.db.getString(Constants.USER_ID), eventId, imageId)
        call.enqueue(object : Callback<TagUserModel> {
            override fun onResponse(call: Call<TagUserModel>, response: Response<TagUserModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                    Logger.ex("getTagUser :" + Gson().toJson(response))
                } else {
                    Logger.ex("Server Exception:" + response.body()!!.exception!!)
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<TagUserModel>, t: Throwable) {
                Logger.ex("ERROR:" + t.message!!)
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param friendId
     * @param callback
     */
    fun addFriendFromContact(callback: GenericCallback<ContactFriendListModel>) {
        val call = HooleyApp.apiService.addFriendFromContact(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<ContactFriendListModel> {
            override fun onResponse(call: Call<ContactFriendListModel>, response: Response<ContactFriendListModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                    Logger.ex("addFriendFromContact :" + Gson().toJson(response))
                } else {
                    Logger.ex("Server Exception:" + response.body()!!.exception!!)
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<ContactFriendListModel>, t: Throwable) {
                Logger.ex("ERROR:" + t.message!!)
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param friendId
     * @param callback
     */
    fun getMoreInvitePeople(searchString: String, callback: GenericCallback<HooleyUserModel>) {
        val call = HooleyApp.apiService.getMoreInvitePeople(HooleyApp.db.getString(Constants.USER_ID), searchString)
        call.enqueue(object : Callback<HooleyUserModel> {
            override fun onResponse(call: Call<HooleyUserModel>, response: Response<HooleyUserModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                    Logger.ex("getMoreInvitePeople :" + Gson().toJson(response))
                } else {
                    Logger.ex("Server Exception:" + response.body()!!.exception)
                    callback.failure(response.body()!!.exception)
                }
            }

            override fun onFailure(call: Call<HooleyUserModel>, t: Throwable) {
                Logger.ex("ERROR:" + t.message!!)
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param friendId
     * @param callback
     */
    fun getFriendsOfFriends(callback: GenericCallback<GetFriendOfFriendModel>) {
        val call = HooleyApp.apiService.getFriendsOfFriends(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<GetFriendOfFriendModel> {
            override fun onResponse(call: Call<GetFriendOfFriendModel>, response: Response<GetFriendOfFriendModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                    Logger.ex("getFriendsOfFriends :" + Gson().toJson(response))
                } else {
                    Logger.ex("Server Exception:" + response.body()!!.exception)
                    callback.failure(response.body()!!.exception)
                }
            }

            override fun onFailure(call: Call<GetFriendOfFriendModel>, t: Throwable) {
                Logger.ex("ERROR:" + t.message!!)
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * true
     * @param callback
     */
    fun blockFriend(friendId: Int, isBlocked: Boolean, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.blockFriend(BlockFriendrequestModel(HooleyApp.db.getString(Constants.USER_ID), friendId.toString(), isBlocked))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                    Logger.ex("blockFriend:" + Gson().toJson(response))
                } else {
                    Logger.ex("Server Exception:" + response.body()!!.exception!!)
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * true
     * @param callback
     */
    fun getUserSearchHistory(callback: GenericCallback<GetUserSearchesHistory>) {
        val call = HooleyApp.apiService.getUserSearchHistory((HooleyApp.db.getString(Constants.USER_ID)))
        call.enqueue(object : Callback<GetUserSearchesHistory> {
            override fun onResponse(call: Call<GetUserSearchesHistory>, response: Response<GetUserSearchesHistory>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.isSuccessful) {
                    callback.success(response.body()!!)
                    Logger.ex("getUserSearchHistory:" + Gson().toJson(response))
                } else {
                    Logger.ex("Server Exception:" + response.body()!!.exception!!)
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GetUserSearchesHistory>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * true
     * @param callback
     */
    fun addUserSearchHistory(searchString: String, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.addUserSearchHistory(AddSearchHistoryRequestModel(HooleyApp.db.getString(Constants.USER_ID), searchString))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                    Logger.ex("addUserSearchHistory:" + Gson().toJson(response))
                } else {
                    Logger.ex("Server Exception:" + response.body()!!.exception!!)
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * true
     * @param callback
     */
    fun removeAllUserSearches(callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.removeAllUserSearches(RemoveAllSearchesRequestModel(HooleyApp.db.getString(Constants.USER_ID)))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                    Logger.ex("removeAllUserSearches:" + Gson().toJson(response))
                } else {
                    Logger.ex("Server Exception:" + response.body()!!.exception!!)
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }
}
