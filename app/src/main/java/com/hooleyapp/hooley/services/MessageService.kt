package com.hooleyapp.hooley.services

import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.app.data.model.messages.GetAllMessageModel
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.CreateThreadModel
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GetAllThreadModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.requestmodel.CreateThreadRequestModel
import com.hooleyapp.hooley.requestmodel.RemoveThreadRequestModel
import com.hooleyapp.hooley.requestmodel.SendMessageRequestModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException


/**
 * Created by Nauman on 4/24/2018.
 */

class MessageService {

    /**
     * @param callback
     */
    fun getMessageHistory(callback: GenericCallback<GetAllThreadModel>) {
        val call = HooleyApp.apiService.getAllThread(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<GetAllThreadModel> {
            override fun onResponse(call: Call<GetAllThreadModel>, response: Response<GetAllThreadModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    Logger.ex("objectResponseMessageHistory:" + Gson().toJson(response))
                    callback.success(response.body()!!)
                } else {
                    Logger.ex("getMessageHistory : Server Exception:" + response.body()!!.exception!!)
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GetAllThreadModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param callback
     */
    fun createThread(callback: GenericCallback<CreateThreadModel>, friendId: String) {
        val call = HooleyApp.apiService.createThread(CreateThreadRequestModel(HooleyApp.db.getString(Constants.USER_ID), friendId))
        call.enqueue(object : Callback<CreateThreadModel> {
            override fun onResponse(call: Call<CreateThreadModel>, response: Response<CreateThreadModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    Logger.ex("objectResponseCreateThread:" + Gson().toJson(response))
                    callback.success(response.body()!!)
                } else {
                    Logger.ex("Server Exception:" + response.body()!!.exception!!)
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<CreateThreadModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param callback
     */
    fun getAllMessage(callback: GenericCallback<GetAllMessageModel>, threadId: String) {
        val call = HooleyApp.apiService.getMessages(HooleyApp.db.getString(Constants.USER_ID), threadId)
        call.enqueue(object : Callback<GetAllMessageModel> {
            override fun onResponse(call: Call<GetAllMessageModel>, response: Response<GetAllMessageModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    Logger.ex("objectResponseMessageHistory:" + Gson().toJson(response))
                    callback.success(response.body()!!)
                } else {
                    Logger.ex("Server Exception:" + response.body()!!.exception!!)
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GetAllMessageModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }

            }
        })
    }

    /**
     * @param callback
     */
    fun sendIMMessage(callback: GenericCallback<GeneralModel>, threadId: String, msg: String, type: String) {
        val call = HooleyApp.apiService.sendIMMessage(SendMessageRequestModel(HooleyApp.db.getString(Constants.USER_ID), threadId, msg, type))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    Logger.ex("sendIMMessage:" + Gson().toJson(response))
                    callback.success(response.body()!!)
                } else {
                    Logger.ex("Server Exception:" + response.body()!!.exception!!)
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param callback
     */
    fun removeThread(callback: GenericCallback<GeneralModel>, threadId: String) {
        val call = HooleyApp.apiService.removeThread(RemoveThreadRequestModel(threadId, HooleyApp.db.getString(Constants.USER_ID)))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    Logger.ex("removeThread:" + Gson().toJson(response))
                    callback.success(response.body()!!)
                } else {
                    Logger.ex("Server Exception:" + response.body()!!.exception!!)
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }

            }
        })
    }


}
