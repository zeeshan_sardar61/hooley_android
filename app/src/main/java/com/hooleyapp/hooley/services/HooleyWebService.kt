package com.hooleyapp.hooley.services

import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.GetAppVersionResponseModel
import com.hooleyapp.hooley.model.GetUserContactCardResponseModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.requestmodel.FeedBackRequestModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException

class HooleyWebService {

    /**
     * @param feedBackMessage
     * @param rating
     * @param callback
     */
    fun addUserFeedBack(feedBackMessage: String, rating: String, callback: IWebServiceCallback<GeneralModel>) {
        val call = HooleyApp.apiService.addUserFeedBack(FeedBackRequestModel(HooleyApp.db.getString(Constants.USER_ID), feedBackMessage, rating, HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("addUserFeedBack: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getUserContactCard(HooleyUserId: String, callback: GenericCallback<GetUserContactCardResponseModel>) {
        val call = HooleyApp.apiService.getUserContactCard(HooleyApp.db.getString(Constants.USER_ID), HooleyUserId)
        call.enqueue(object : Callback<GetUserContactCardResponseModel> {
            override fun onResponse(call: Call<GetUserContactCardResponseModel>, response: Response<GetUserContactCardResponseModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getUserContactCard: " + Gson().toJson(response.body()!!))
                if (response.body()!!.isSuccessful) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception)
                }
            }

            override fun onFailure(call: Call<GetUserContactCardResponseModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getAppVersion(callback: IWebServiceCallback<GetAppVersionResponseModel>) {
        val call = HooleyApp.apiService.getAppVersion(Constants.DEVICE_TYPE, HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<GetAppVersionResponseModel> {
            override fun onResponse(call: Call<GetAppVersionResponseModel>, response: Response<GetAppVersionResponseModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.isSuccessful) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception)
                }
            }

            override fun onFailure(call: Call<GetAppVersionResponseModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }
}
