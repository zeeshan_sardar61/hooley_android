package com.hooleyapp.hooley.services

import android.annotation.SuppressLint
import android.provider.Settings
import android.text.TextUtils
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.activites.ActivityBase
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventAnnouncementModel
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventDetailModel
import com.hooleyapp.hooley.app.data.model.guestuser.GuestUserLoginModel
import com.hooleyapp.hooley.app.data.model.guestuser.GuestUserLoginRequestModel
import com.hooleyapp.hooley.app.data.model.media.EventGalleryFeedModel
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.*
import com.hooleyapp.hooley.model.staticData.Category
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.requestmodel.EventBookMarkRequestModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException
import java.util.*

class GuestUserWebService {

    private var eventGalleryapiCall: Call<EventGalleryFeedModel>? = null

    /**
     * @param callback
     */
    @SuppressLint("HardwareIds")
    fun guestUserLogin(callback: IWebServiceCallback<GuestUserLoginModel>) {
        var androidId = Settings.Secure.getString(ActivityBase.activity.contentResolver, Settings.Secure.ANDROID_ID)
        val call = HooleyApp.apiService.guestUserLogin(GuestUserLoginRequestModel(androidId, HooleyApp.db.getString(Constants.GCM_REGISTRATION_TOKEN), Constants.DEVICE_TYPE, HooleyApp.db.getString(Constants.CURRENT_LAT), HooleyApp.db.getString(Constants.CURRENT_LANG)))
        call.enqueue(object : Callback<GuestUserLoginModel> {
            override fun onResponse(call: Call<GuestUserLoginModel>, response: Response<GuestUserLoginModel>) {
                if (response.body() == null) {
                    if (response.errorBody() != null)
                        callback.failure(response.errorBody().toString())
                    else
                        callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GuestUserLoginModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param callback
     */
    fun seeGuestUserAllNearByEventList(callback: IWebServiceCallback<NearByEventListFeedModel>) {
        val latitude: String
        val longitude: String
        val personalLatitude = java.lang.Double.toString(HooleyApp.db.getDouble(Constants.PERSONAL_LAT, 0.0))
        val personalLongitude = java.lang.Double.toString(HooleyApp.db.getDouble(Constants.PERSONAL_LANG, 0.0))

        val currentLatitude = java.lang.Double.toString(HooleyApp.db.getDouble(Constants.CURRENT_LAT, 0.0))
        val currentLongitude = java.lang.Double.toString(HooleyApp.db.getDouble(Constants.CURRENT_LANG, 0.0))

        if (TextUtils.isEmpty(personalLatitude) || personalLatitude.equals("0.0", ignoreCase = true) || TextUtils.isEmpty(personalLongitude) || personalLongitude.equals("0.0", ignoreCase = true)) {
            latitude = currentLatitude
            longitude = currentLongitude
        } else {
            latitude = personalLatitude
            longitude = personalLongitude
        }
        val radius = "30"
        val call = HooleyApp.apiService.seeGuestUserAllNearByEventList(HooleyApp.db.getString(Constants.USER_ID), latitude, longitude, radius)
        call.enqueue(object : Callback<NearByEventListFeedModel> {
            override fun onResponse(call: Call<NearByEventListFeedModel>, response: Response<NearByEventListFeedModel>) {
                if (response.body() == null) {
                    if (response.errorBody() != null)
                        callback.failure(response.errorBody().toString())
                    else
                        callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<NearByEventListFeedModel>, t: Throwable) {
                try {
                    if (t is UnknownHostException || t is IOException) {
                        if (t.message.equals("timeout", ignoreCase = true)) {
                            callback.failure(Constants.TIMEOUT)
                        } else
                            callback.failure(Constants.NO_NETWORK)
                    } else {
                        callback.failure(t.message!!)
                    }
                } catch (e: NullPointerException) {

                }

            }
        })
    }

    /**
     * @param callback
     * @param lang
     * @param lat
     * @param radius
     */

    fun seeGuestUserAllNearByEventMap(lat: Double, lang: Double, radius: Float, callback: IWebServiceCallback<NearByEventMapFeedModel>) {
        val call = HooleyApp.apiService.seeGuestUserAllNearByEventMap(HooleyApp.db.getString(Constants.USER_ID), lat, lang, radius)
        call.enqueue(object : Callback<NearByEventMapFeedModel> {
            override fun onResponse(call: Call<NearByEventMapFeedModel>, response: Response<NearByEventMapFeedModel>) {
                if (response.body() == null) {
                    if (response.errorBody() != null)
                        callback.failure(response.errorBody().toString())
                    else
                        callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<NearByEventMapFeedModel>, t: Throwable) {
                try {
                    if (t is UnknownHostException || t is IOException) {
                        if (t.message.equals("timeout", ignoreCase = true)) {
                            callback.failure(Constants.TIMEOUT)
                        } else
                            callback.failure(Constants.NO_NETWORK)
                    } else {
                        callback.failure(t.message!!)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        })
    }


    fun seeGuestUserEventDetail(eventId: String, callback: IWebServiceCallback<EventDetailModel>) {
        val call = HooleyApp.apiService.seeGuestUserEventDetail(HooleyApp.db.getString(Constants.USER_ID), eventId)
        call.enqueue(object : Callback<EventDetailModel> {
            override fun onResponse(call: Call<EventDetailModel>, response: Response<EventDetailModel>) {
                if (response.body() == null) {
                    if (response.errorBody() != null)
                        callback.failure(response.errorBody().toString())
                    else
                        callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<EventDetailModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * wait to change
     */
    fun seeGuestUserEventGalleryFeeds(eventId: String, type: String, callback: IWebServiceCallback<EventGalleryFeedModel>) {
        if (eventGalleryapiCall != null)
            eventGalleryapiCall?.cancel()
        eventGalleryapiCall = HooleyApp.apiService.seeGuestUserEventGalleryFeeds(HooleyApp.db.getString(Constants.USER_ID), eventId, type)
        eventGalleryapiCall?.enqueue(object : Callback<EventGalleryFeedModel> {
            override fun onResponse(call: Call<EventGalleryFeedModel>, response: Response<EventGalleryFeedModel>) {
                if (response.body() == null) {
                    if (response.errorBody() != null)
                        callback.failure(response.errorBody().toString())
                    else
                        callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<EventGalleryFeedModel>, t: Throwable) {
                if (call.isCanceled) {
                    callback.failure("")
                    return
                }
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    fun guestUsersearchEvent(searchString: String, radius: Double?, searchDate: String, catList: ArrayList<Category>,
                             isPaid: Boolean?, isFree: Boolean?, callback: IWebServiceCallback<SearchEventModel>) {
        val userId = HooleyApp.db.getString(Constants.USER_ID)
        val latitude = HooleyApp.db.getDouble(Constants.CURRENT_LAT, 0.0)
        val longitude = HooleyApp.db.getDouble(Constants.CURRENT_LANG, 0.0)
        val eventTypes = ArrayList<Int>()
        var ids: Int
        for (i in catList.indices) {
            if (catList[i].isActive) {
                ids = catList[i].id!!
                eventTypes.add(ids)
            }
        }
//        val call = HooleyApp.apiService.guestUsersearchEvent(SearchEventRequestModel(searchString, radius!!, eventTypes, isPaid!!, isFree!!, latitude, longitude, userId, searchDate))
//        call.enqueue(object : Callback<SearchEventModel> {
//            override fun onResponse(call: Call<SearchEventModel>, response: Response<SearchEventModel>) {
//                if (response.body() == null) {
//                    if (response.errorBody() != null)
//                        callback.failure(response.errorBody().toString())
//                    else
//                        callback.failure("Server not responding Please try again later")
//                    return
//                }
//                if (response.body()!!.success) {
//                    callback.success(response.body()!!)
//                } else {
//                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
//                        callback.onTokenExpire()
//                    } else
//                        callback.failure(response.body()!!.exception!!)
//                }
//            }
//
//            override fun onFailure(call: Call<SearchEventModel>, t: Throwable) {
//                if (t is UnknownHostException || t is IOException) {
//                    if (t.message.equals("timeout", ignoreCase = true)) {
//                        callback.failure(Constants.TIMEOUT)
//                    } else
//                        callback.failure(Constants.NO_NETWORK)
//                } else {
//                    callback.failure(t.message!!)
//                }
//            }
//        })
    }

    fun addGuestUserBookMarkEvent(eventId: String, isRemove: Boolean, callback: IWebServiceCallback<GeneralModel>) {
        val call = HooleyApp.apiService.addGuestUserBookMarkEvent(EventBookMarkRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId, isRemove))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    if (response.errorBody() != null)
                        callback.failure(response.errorBody().toString())
                    else
                        callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    fun addGuestUserFeedBack(feedBackMessage: String, rating: String, callback: IWebServiceCallback<GeneralModel>) {
//        val call = HooleyApp.apiService.addGuestUserFeedBack(FeedBackRequestModel(HooleyApp.db.getString(Constants.USER_ID), feedBackMessage, rating))
//        call.enqueue(object : Callback<GeneralModel> {
//            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
//                if (response.body() == null) {
//                    if (response.errorBody() != null)
//                        callback.failure(response.errorBody().toString())
//                    else
//                        callback.failure("Server not responding Please try again later")
//                    return
//                }
//                if (response.body()!!.success) {
//                    callback.success(response.body()!!)
//                } else {
//                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
//                        callback.onTokenExpire()
//                    } else
//                        callback.failure(response.body()!!.exception!!)
//                }
//            }
//
//            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
//                if (t is UnknownHostException || t is IOException) {
//                    if (t.message.equals("timeout", ignoreCase = true)) {
//                        callback.failure(Constants.TIMEOUT)
//                    } else
//                        callback.failure(Constants.NO_NETWORK)
//                } else {
//                    callback.failure(t.message!!)
//                }
//            }
//        })
    }


    fun getGuestUserBookMarkEvent(callback: IWebServiceCallback<BookmarkEventModel>) {
        val call = HooleyApp.apiService.showGuestUserEventBookmark(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<BookmarkEventModel> {
            override fun onResponse(call: Call<BookmarkEventModel>, response: Response<BookmarkEventModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getGuestUserBookMarkEvent :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<BookmarkEventModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getGuestEventAnnouncement(eventId: String, callback: IWebServiceCallback<EventAnnouncementModel>) {
        val call = HooleyApp.apiService.getGuestEventAnnouncement(HooleyApp.db.getString(Constants.USER_ID), eventId)
        call.enqueue(object : Callback<EventAnnouncementModel> {
            override fun onResponse(call: Call<EventAnnouncementModel>, response: Response<EventAnnouncementModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getEventAnnouncement :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<EventAnnouncementModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

}