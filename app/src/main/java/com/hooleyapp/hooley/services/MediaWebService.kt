package com.hooleyapp.hooley.services

import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.app.data.model.media.MediaStreamDetailModel
import com.hooleyapp.hooley.app.data.model.media.MediaStreamEventListModel
import com.hooleyapp.hooley.app.data.model.media.MediaStreamTrendingModel
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.*
import com.hooleyapp.hooley.model.event.GetEditPostModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.requestmodel.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.net.UnknownHostException
import java.util.*

/**
 * Created by Nauman on 3/26/2018.
 */

/**
 * constructor
 */
class MediaWebService {

    /**
     * @param mediaArrayList
     * @param postAttribute
     * @param callback
     */
    fun postMedia(mediaArrayList: ArrayList<ItemPostMedia>?, postAttribute: String, callback: GenericCallback<GeneralModel>) {
        val mlist = ArrayList<MultipartBody.Part>()
        if (mediaArrayList != null && mediaArrayList.size > 0) {
            for (i in mediaArrayList.indices) {
                if (mediaArrayList[i].isVideo) {
                    val file = File(mediaArrayList[i].mediaUri.path!!)
                    val reqFile = RequestBody.create(MediaType.parse("video/*"), file)
                    val fileBody = MultipartBody.Part.createFormData("files", "video.mp4", reqFile)
                    mlist.add(fileBody)
                } else {
                    val file = File(mediaArrayList[i].mediaUri.path!!)
                    val reqFile = RequestBody.create(MediaType.parse("image/jpeg"), file)
                    val fileBody = MultipartBody.Part.createFormData("files", file.name, reqFile)
                    mlist.add(fileBody)
                }
            }
        }
        val postAttributes = RequestBody.create(MediaType.parse("text/plain"), postAttribute)
        val call = HooleyApp.apiService.uploadEventMedia(mlist, postAttributes)
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("createEvent:" + Gson().toJson(response.body()))
                callback.success(response.body()!!)
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })

    }

    /**
     * @param callback
     * @param eventId
     * @param imageId
     */
    fun likePost(callback: GenericCallback<GeneralModel>, eventId: String, imageId: String) {
        val call = HooleyApp.apiService.addPostLike(PostLikeRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId, imageId))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("likePost:" + Gson().toJson(response))
                if (response.body()!!.success)
                    callback.success(response.body()!!)
                else
                    callback.failure(response.body()!!.exception!!)
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param callback
     * @param eventId
     * @param imageId
     * @param isFav
     */
    fun mediaFav(callback: GenericCallback<GeneralModel>, eventId: String, imageId: String, isFav: Boolean) {
        val call = HooleyApp.apiService.addMediaToFav(FavMediaRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId, imageId, isFav))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("mediaFav:" + Gson().toJson(response))
                if (response.body()!!.success)
                    callback.success(response.body()!!)
                else
                    callback.failure(response.body()!!.exception!!)
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })

    }

    /**
     * @param callback
     * @param eventId
     * @param imageId
     * @param shareMedia
     */
    fun shareEventMedia(callback: GenericCallback<GeneralModel>, eventId: String, imageId: String, shareMedia: String) {
        val call = HooleyApp.apiService.eventImageShare(EventImageShareRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId, imageId, shareMedia))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("mediaFav:" + Gson().toJson(response))
                if (response.body()!!.success)
                    callback.success(response.body()!!)
                else
                    callback.failure(response.body()!!.exception!!)

            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param callback
     * @param reasonId
     * @param reportMessage
     * @param mediaId
     * @param eventId
     */
    fun submitReport(callback: GenericCallback<GeneralModel>, reasonId: String, reportMessage: String, mediaId: String, eventId: String) {
        val call = HooleyApp.apiService.submitMediaReportEvent(SubmitPostReportRequestModel(mediaId, eventId, HooleyApp.db.getString(Constants.USER_ID), reasonId, reportMessage))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("SubmitMediaReport:" + Gson().toJson(response))
                if (response.body()!!.success)
                    callback.success(response.body()!!)
                else
                    callback.failure(response.body()!!.exception!!)
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param callback
     * @param mediaId
     * @param eventId
     */
    fun deleteMedia(callback: GenericCallback<GeneralModel>, mediaId: String, eventId: String) {
        val call = HooleyApp.apiService.deleteMedia(DeleMediaRequestModel(mediaId, HooleyApp.db.getString(Constants.USER_ID), eventId))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("deleteMedia:" + Gson().toJson(response))
                if (response.body()!!.success)
                    callback.success(response.body()!!)
                else
                    callback.failure(response.body()!!.exception!!)
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param callback
     */
    fun getMediaReportEvent(callback: GenericCallback<GetReportEventModel>) {
        val call = HooleyApp.apiService.getMediaReportEvent(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<GetReportEventModel> {
            override fun onResponse(call: Call<GetReportEventModel>, response: Response<GetReportEventModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getMediaReportEvent:" + Gson().toJson(response))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GetReportEventModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })

    }


    fun getMediaStreamAllEvents(type: String, callback: GenericCallback<MediaStreamEventListModel>) {
        val call = HooleyApp.apiService.getMediaStream(HooleyApp.db.getString(Constants.USER_ID), type)
        call.enqueue(object : Callback<MediaStreamEventListModel> {
            override fun onResponse(call: Call<MediaStreamEventListModel>, response: Response<MediaStreamEventListModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getMediaStream: TYPE >>> $type")
                Logger.ex("getMediaStreamAllEvents : " + Gson().toJson(response))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<MediaStreamEventListModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getMediaStreamTrendingEvents(type: String, callback: GenericCallback<MediaStreamTrendingModel>) {
        val call = HooleyApp.apiService.getMediaStreamTreeding(HooleyApp.db.getString(Constants.USER_ID), type)
        call.enqueue(object : Callback<MediaStreamTrendingModel> {
            override fun onResponse(call: Call<MediaStreamTrendingModel>, response: Response<MediaStreamTrendingModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getMediaStream: TYPE >>> $type")
                Logger.ex("getMediaStreamTrendingEvents : " + Gson().toJson(response))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<MediaStreamTrendingModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    fun getMediaByEvent(eventId: String, type: String, callback: GenericCallback<MediaStreamDetailModel>) {
        val call = HooleyApp.apiService.getMediaByEvent(HooleyApp.db.getString(Constants.USER_ID), eventId, type)
        call.enqueue(object : Callback<MediaStreamDetailModel> {
            override fun onResponse(call: Call<MediaStreamDetailModel>, response: Response<MediaStreamDetailModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getMediaByEvent : " + Gson().toJson(response))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<MediaStreamDetailModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    fun getPostComments(eventId: String, imageId: String, callback: GenericCallback<PostImageCommentModel>) {
        val call = HooleyApp.apiService.showSinglePostComments(HooleyApp.db.getString(Constants.USER_ID), imageId, eventId)
        call.enqueue(object : Callback<PostImageCommentModel> {
            override fun onResponse(call: Call<PostImageCommentModel>, response: Response<PostImageCommentModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getPostComments : " + Gson().toJson(response))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<PostImageCommentModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun commentOnPost(eventId: String, imageId: String, commentText: String, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.commentOnPost(PostCommentRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId, imageId, commentText))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("commentOnPost : " + Gson().toJson(response))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getEditPost(eventId: String, postId: String, callback: GenericCallback<GetEditPostModel>) {
        val call = HooleyApp.apiService.getEditPost(HooleyApp.db.getString(Constants.USER_ID), eventId, postId)
        call.enqueue(object : Callback<GetEditPostModel> {
            override fun onResponse(call: Call<GetEditPostModel>, response: Response<GetEditPostModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getEditPost : " + Gson().toJson(response))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GetEditPostModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun updatePost(mediaArrayList: ArrayList<UpdatePostMediaItem>?, postAttribute: String, callback: GenericCallback<GeneralModel>) {
        val mlist = ArrayList<MultipartBody.Part>()
        if (mediaArrayList != null && mediaArrayList.size > 0) {
            for (i in mediaArrayList.indices) {
                if (mediaArrayList[i].isVideo) {
                    if (mediaArrayList[i].type == Constants.TYPE_URI) {
                        val file = File(mediaArrayList[i].mediaUri?.path!!)
                        val reqFile = RequestBody.create(MediaType.parse("video/*"), file)
                        val fileBody = MultipartBody.Part.createFormData("files", "video.mp4", reqFile)
                        mlist.add(fileBody)
                    }
                } else {
                    if (mediaArrayList[i].type == Constants.TYPE_URI) {
                        val file = File(mediaArrayList[i].mediaUri?.path!!)
                        val reqFile = RequestBody.create(MediaType.parse("image/jpeg"), file)
                        val fileBody = MultipartBody.Part.createFormData("files", file.name, reqFile)
                        mlist.add(fileBody)
                    }
                }
            }
        }
        val postAttributes = RequestBody.create(MediaType.parse("text/plain"), postAttribute)
        val call = HooleyApp.apiService.updatePost(mlist, postAttributes)
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("createEvent:" + Gson().toJson(response.body()))
                callback.success(response.body()!!)
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })

    }


}
