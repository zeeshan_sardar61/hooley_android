package com.hooleyapp.hooley.services

import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.EventInviteNearByMeModel
import com.hooleyapp.hooley.model.GeneralModel
import com.hooleyapp.hooley.model.PastGuestListModel
import com.hooleyapp.hooley.model.event.PastEventsListModel
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.requestmodel.InvitePastGuestListRequestModel
import com.hooleyapp.hooley.requestmodel.InvitePeopleNearByMeRequestModel
import com.hooleyapp.hooley.tables.FriendsTable
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException
import java.util.*

class InviteWebService {


    /**
     * @param callback
     */
    fun getPastGuestList(callback: GenericCallback<PastGuestListModel>) {
        val call = HooleyApp.apiService.getPastGuestList(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<PastGuestListModel> {
            override fun onResponse(call: Call<PastGuestListModel>, response: Response<PastGuestListModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getPastGuestList : " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception)
                }
            }

            override fun onFailure(call: Call<PastGuestListModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getPastEventsList(callback: GenericCallback<PastEventsListModel>) {
        val call = HooleyApp.apiService.getPastEventList(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<PastEventsListModel> {
            override fun onResponse(call: Call<PastEventsListModel>, response: Response<PastEventsListModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getPastGuestList : " + Gson().toJson(response.body()!!))
                if (response.body()!!.isSuccessful!!) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<PastEventsListModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    /**
     * @param callback
     */
    fun invitePastGuestList(eventId: String, eventName: String, list: ArrayList<PastGuestListModel.Guest>, callback: GenericCallback<GeneralModel>) {
        var mList = ArrayList<InvitePastGuestListRequestModel.EventList>()
        for (i in list.indices) {
            if (list[i].isChecked) {
                if (list[i].isSelectedAll) {
                    list[i].userIdList.clear()
                }
                mList.add(InvitePastGuestListRequestModel.EventList(list[i].eventid, list[i].userIdList))
            }
        }
        val call = HooleyApp.apiService.sendInvitePastGuest(InvitePastGuestListRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId, eventName, mList))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("invitePastGuestList" +
                        ": " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param callback
     */
    fun getEventNearByMe(latitude: Double?, longitude: Double?, miles: Float?,
                         callback: GenericCallback<EventInviteNearByMeModel>) {
        var radius = 0f
        if (miles!! > 0f)
            radius = miles
        val call = HooleyApp.apiService.getEventNearByMe(HooleyApp.db.getString(Constants.USER_ID), latitude, longitude, radius)
        call.enqueue(object : Callback<EventInviteNearByMeModel> {
            override fun onResponse(call: Call<EventInviteNearByMeModel>, response: Response<EventInviteNearByMeModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getEventNearByMe : " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<EventInviteNearByMeModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    /**
     * @param callback
     */
    fun sendInvitePeopleNearBy(eventId: String, eventName: String, list: ArrayList<EventInviteNearByMeModel.UserNearMe>,
                               callback: GenericCallback<GeneralModel>) {
        val mList = ArrayList<InvitePeopleNearByMeRequestModel.Userslist>()
        for (i in list.indices) {
            if (list[i].isChecked) {
                mList.add(InvitePeopleNearByMeRequestModel.Userslist(list[i].userId))
            }
        }
        val call = HooleyApp.apiService.sendInvitePeopleNearBy(InvitePeopleNearByMeRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId, eventName, mList))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("sendInvitePeopleNearBy : " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param callback
     */
    fun sendInviteToHooleyFriends(eventId: String, eventName: String, list: ArrayList<FriendsTable>,
                                  callback: GenericCallback<GeneralModel>) {
        val mList = ArrayList<InvitePeopleNearByMeRequestModel.Userslist>()
        for (i in list.indices) {
//            if (list[i].isChecked) {
            mList.add(InvitePeopleNearByMeRequestModel.Userslist(list[i].userId.toString()))
//            }
        }
        val call = HooleyApp.apiService.sendInviteToHooleyFriends(InvitePeopleNearByMeRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId, eventName, mList))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("sendInviteToHooleyFriends : " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


}
