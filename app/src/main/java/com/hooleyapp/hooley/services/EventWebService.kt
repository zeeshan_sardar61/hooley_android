package com.hooleyapp.hooley.services

import android.net.Uri
import android.text.TextUtils
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.apis.ApiRequestJson
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventAnnouncementModel
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventDetailModel
import com.hooleyapp.hooley.app.data.model.event.responseModel.EventGuestListModel
import com.hooleyapp.hooley.app.data.model.event.responseModel.PopularInFriendsModel
import com.hooleyapp.hooley.helper.getFilePathFromContentUri
import com.hooleyapp.hooley.helper.getUriFrombitmap
import com.hooleyapp.hooley.helper.handleSamplingAndRotationBitmap
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.interfaces.IWebServiceCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.*
import com.hooleyapp.hooley.model.staticData.Category
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.DateUtils
import com.hooleyapp.hooley.requestmodel.*
import com.hooleyapp.hooley.tables.FriendsTable
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.net.UnknownHostException
import java.util.*

/**
 * Created by Zeeshan on 26-Mar-18.
 */

class EventWebService {


    /**
     * @param callback
     */
    fun shareEvent(eventId: String, shareMedia: String, callback: IWebServiceCallback<GeneralModel>) {
        val call = HooleyApp.apiService.shareEvent(ShareEventRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId, shareMedia))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    /**
     * @param callback
     */
    fun followEvent(eventId: String, isFollow: Boolean, callback: IWebServiceCallback<GeneralModel>) {
        val call = HooleyApp.apiService.followEvent(FollowEventRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId, isFollow))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("followEvent:" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param eventId
     * @param isAttend
     * @param callback
     */
    fun attendEvent(eventId: String, isAttend: Boolean, callback: IWebServiceCallback<GeneralModel>) {
        val call = HooleyApp.apiService.attendEvent(AttendEventRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId, isAttend))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("attendEvent:" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param eventId
     * @param isRemove
     * @param callback
     */
    fun bookmarkEvent(eventId: String, isRemove: Boolean, callback: IWebServiceCallback<GeneralModel>) {
        val call = HooleyApp.apiService.addBookMarkEvent(EventBookMarkRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId, isRemove))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("bookmarkEvent:" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    /**
     * @param eventId
     * @param callback
     */
    fun likeEvent(eventId: String, callback: IWebServiceCallback<GeneralModel>) {
        val call = HooleyApp.apiService.likeEvent(LikeEventRequestModel(eventId, HooleyApp.db.getString(Constants.USER_ID)))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("likeEvent:" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    /**
     * @param eventId
     * @param comment
     * @param callback
     */
    fun postCommentOnEvent(eventId: String, comment: String, callback: IWebServiceCallback<GeneralModel>) {
        val call = HooleyApp.apiService.postCommentOnEvent(EventCommentRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId, comment))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("postCommentOnEvent:" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    /**
     * @param callback
     */
    fun getReportEvent(callback: IWebServiceCallback<GetReportEventModel>) {
        val call = HooleyApp.apiService.getReportEvent(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<GetReportEventModel> {
            override fun onResponse(call: Call<GetReportEventModel>, response: Response<GetReportEventModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                //                Logger.ex("getReportEvent:" + new Gson().toJson(response.body()!!));
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GetReportEventModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param eventId
     * @param reasonId
     * @param reportMessage
     * @param callback
     */
    fun submitReport(eventId: String, reasonId: String, reportMessage: String, callback: IWebServiceCallback<GeneralModel>) {
        val call = HooleyApp.apiService.submitReportEvent(SubmitReportEventRequestModel(eventId, reasonId, HooleyApp.db.getString(Constants.USER_ID), reportMessage))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                //                Logger.ex("submitReport:" + new Gson().toJson(response.body()!!));
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param eventId
     * @param callback
     */
    fun seeEventDetail(eventId: String, callback: IWebServiceCallback<EventDetailModel>) {
        val call = HooleyApp.apiService.seeEventDetail(HooleyApp.db.getString(Constants.USER_ID), eventId, HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN))
        call.enqueue(object : Callback<EventDetailModel> {
            override fun onResponse(call: Call<EventDetailModel>, response: Response<EventDetailModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                //                Logger.ex("seeEventDetail:" + new Gson().toJson(response.body()!!));
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<EventDetailModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param mediaUri
     * @param eventName
     * @param eventDetail
     * @param eventStartTime
     * @param eventEndTime
     * @param isPublic
     * @param isPrivate
     * @param isPaid
     * @param isFree
     * @param inviteFriends
     * @param saveAsUnpublished
     * @param optionalCat
     * @param hostList
     * @param coHOstList
     * @param catList
     * @param geofenceList
     * @param ageId
     * @param callback
     */
    fun createEvent(mediaUri: ArrayList<ItemCreateEventWallpaper>, eventName: String, evenType: String, eventDetail: String, eventHashs: ArrayList<String>,
                    eventStartTime: String, eventEndTime: String, isPublic: Boolean, isPublish: Boolean,
                    optionalCat: ArrayList<String>, coHOstList: ArrayList<FriendsTable>, catList: ArrayList<Category>,
                    geofenceList: ArrayList<GeoFenceModel>, ageId: String, update: String, callback: IWebServiceCallback<SaveEventModel>) {
        val mlist = ArrayList<MultipartBody.Part>()
        for (i in mediaUri.indices) {
            if (mediaUri[i].type == Constants.TYPE_URI) {
//                val file = File(mediaUri[i].uri?.path!!)
                val file = File(HooleyMain.activity!!.getFilePathFromContentUri(HooleyMain.activity!!.getUriFrombitmap(HooleyMain.activity!!.handleSamplingAndRotationBitmap(mediaUri[i].uri!!)!!), HooleyMain.activity!!.contentResolver))
                val reqFile = RequestBody.create(MediaType.parse("image/jpeg"), file)
                val fileBody = MultipartBody.Part.createFormData("file", file.name, reqFile)
                mlist.add(fileBody)
            }
        }
        val isUpdate = RequestBody.create(MediaType.parse("text/plain"), update)

        val requestJson = RequestBody.create(MediaType.parse("text/plain"), ApiRequestJson.getInstance().saveEventDataString(mediaUri,
                "",
                eventName,
                evenType,
                eventDetail,
                eventHashs,
                eventStartTime,
                eventEndTime,
                isPublic,
                isPublish,
                optionalCat,
                coHOstList,
                catList,
                geofenceList,
                ageId))
        val call = HooleyApp.apiService.createEvent(mlist, isUpdate, requestJson)
        call.enqueue(object : Callback<SaveEventModel> {
            override fun onResponse(call: Call<SaveEventModel>, response: Response<SaveEventModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                //                Logger.ex("createEvent:" + new Gson().toJson(response.body()!!));
                if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                    callback.onTokenExpire()
                } else
                    callback.success(response.body()!!)
            }

            override fun onFailure(call: Call<SaveEventModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun savePaidEvent(mediaUri: ArrayList<ItemCreateEventWallpaper>, request: UpdateEventRequestModel, isUpdate: String, callback: IWebServiceCallback<SaveEventModel>) {
        val mlist = ArrayList<MultipartBody.Part>()
        for (i in mediaUri.indices) {
            if (mediaUri[i].type == Constants.TYPE_URI) {
//                val file = File(mediaUri[i].uri?.path!!)
                val file = File(HooleyMain.activity!!.getFilePathFromContentUri(HooleyMain.activity!!.getUriFrombitmap(HooleyMain.activity!!.handleSamplingAndRotationBitmap(mediaUri[i].uri!!)!!), HooleyMain.activity!!.contentResolver))
                val reqFile = RequestBody.create(MediaType.parse("image/jpeg"), file)
                val fileBody = MultipartBody.Part.createFormData("file", file.name, reqFile)
                mlist.add(fileBody)
            }
        }

        val requestJson = RequestBody.create(MediaType.parse("text/plain"), Gson().toJson(request))

        val call = HooleyApp.apiService.createEvent(mlist, RequestBody.create(MediaType.parse("text/plain"), isUpdate), requestJson)
        call.enqueue(object : Callback<SaveEventModel> {
            override fun onFailure(call: Call<SaveEventModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }

            override fun onResponse(call: Call<SaveEventModel>, response: Response<SaveEventModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                //                Logger.ex("createEvent:" + new Gson().toJson(response.body()!!));
                if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                    callback.onTokenExpire()
                } else
                    callback.success(response.body()!!)
            }
        })
    }

    /**
     * @param eventId
     * @param msgText
     * @param callback
     */
    fun sendStreamMessage(eventId: String, msgText: String, callback: IWebServiceCallback<GeneralModel>) {
        val call = HooleyApp.apiService.sendTextEventChatStream(SendStreamMessageRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId, msgText))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("sendStreamMessage :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    /**
     * @param eventId
     * @param callback
     */

    fun getMyChatStreamMessages(eventId: String, callback: GenericCallback<GetChatStreamModel>) {
        val call = HooleyApp.apiService.getChatStream(HooleyApp.db.getString(Constants.USER_ID), eventId)
        call.enqueue(object : Callback<GetChatStreamModel> {
            override fun onResponse(call: Call<GetChatStreamModel>, response: Response<GetChatStreamModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getMyChatStreamMessages :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GetChatStreamModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getFeatureEvent(callback: IWebServiceCallback<BookmarkEventModel>) {
        val call = HooleyApp.apiService.getfeatureEvent(HooleyApp.db.getString(Constants.USER_ID), "")
        call.enqueue(object : Callback<BookmarkEventModel> {
            override fun onResponse(call: Call<BookmarkEventModel>, response: Response<BookmarkEventModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getFeatureEvent :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<BookmarkEventModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getBookMarkEvent(callback: IWebServiceCallback<BookmarkEventModel>) {
        val call = HooleyApp.apiService.showEventBookmark(HooleyApp.db.getString(Constants.USER_ID), HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN))
        call.enqueue(object : Callback<BookmarkEventModel> {
            override fun onResponse(call: Call<BookmarkEventModel>, response: Response<BookmarkEventModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getBookMarkEvent :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<BookmarkEventModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getPopularInFriendsEvent(callback: IWebServiceCallback<PopularInFriendsModel>) {
        val call = HooleyApp.apiService.getPopularInFriendsEvent(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<PopularInFriendsModel> {
            override fun onResponse(call: Call<PopularInFriendsModel>, response: Response<PopularInFriendsModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getPopularInFriendsEvent :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<PopularInFriendsModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun seeAllNearByEventListViewFeeds(callback: IWebServiceCallback<NearByEventListFeedModel>) {
        val latitude: String
        val longitude: String
        val personalLatitude = java.lang.Double.toString(HooleyApp.db.getDouble(Constants.PERSONAL_LAT, 0.0))
        val personalLongitude = java.lang.Double.toString(HooleyApp.db.getDouble(Constants.PERSONAL_LANG, 0.0))

        val currentLatitude = java.lang.Double.toString(HooleyApp.db.getDouble(Constants.CURRENT_LAT, 0.0))
        val currentLongitude = java.lang.Double.toString(HooleyApp.db.getDouble(Constants.CURRENT_LANG, 0.0))

        if (TextUtils.isEmpty(personalLatitude) || personalLatitude.equals("0.0", ignoreCase = true) || TextUtils.isEmpty(personalLongitude) || personalLongitude.equals("0.0", ignoreCase = true)) {
            latitude = currentLatitude
            longitude = currentLongitude
        } else {
            latitude = personalLatitude
            longitude = personalLongitude
        }
        val radius = "30"
        val call = HooleyApp.apiService.seeAllNearByEventListViewFeeds(HooleyApp.db.getString(Constants.USER_ID), latitude, longitude, radius, HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN))
        call.enqueue(object : Callback<NearByEventListFeedModel> {
            override fun onResponse(call: Call<NearByEventListFeedModel>, response: Response<NearByEventListFeedModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                //                Logger.ex("seeAllNearByEventListViewFeeds :" + new Gson().toJson(response.body()!!));
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<NearByEventListFeedModel>, t: Throwable) {
                try {
                    if (t is UnknownHostException || t is IOException) {
                        if (t.message.equals("timeout", ignoreCase = true)) {
                            callback.failure(Constants.TIMEOUT)
                        } else
                            callback.failure(Constants.NO_NETWORK)
                    } else {
                        callback.failure(t.message!!)
                    }
                } catch (e: NullPointerException) {

                }

            }
        })
    }

    fun getMyHostEvents(callback: IWebServiceCallback<MyHostEventModel>) {

        val call = HooleyApp.apiService.getMyHostEvents(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<MyHostEventModel> {
            override fun onResponse(call: Call<MyHostEventModel>, response: Response<MyHostEventModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getMyHostEvents :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<MyHostEventModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    try {
                        if (t.message.equals("timeout", ignoreCase = true)) {
                            callback.failure(Constants.TIMEOUT)
                        } else
                            callback.failure(Constants.NO_NETWORK)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getMyAttendEvents(callback: IWebServiceCallback<MyAttendingEventModel>) {

        val call = HooleyApp.apiService.getMyAttendEvents(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<MyAttendingEventModel> {
            override fun onResponse(call: Call<MyAttendingEventModel>, response: Response<MyAttendingEventModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getMyAttendEvents :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<MyAttendingEventModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getMyFollowingEvent(callback: IWebServiceCallback<MyFollowingEventModel>) {
        val call = HooleyApp.apiService.getMyFollowEvents(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<MyFollowingEventModel> {
            override fun onResponse(call: Call<MyFollowingEventModel>, response: Response<MyFollowingEventModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getMyFollowingEvent :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<MyFollowingEventModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })

    }

    fun getMyEventInvite(callback: IWebServiceCallback<MyInviteEventModel>) {
        val call = HooleyApp.apiService.getMyEventInvite(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<MyInviteEventModel> {
            override fun onResponse(call: Call<MyInviteEventModel>, response: Response<MyInviteEventModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getMyEventInvite :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<MyInviteEventModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun searchEvent(searchString: String, radius: Double?, searchDate: String, catList: ArrayList<Category>,
                    isPaid: Boolean?, isFree: Boolean?, callback: IWebServiceCallback<SearchEventModel>) {
        val userId = HooleyApp.db.getString(Constants.USER_ID)
        val latitude = HooleyApp.db.getDouble(Constants.CURRENT_LAT, 0.0)
        val longitude = HooleyApp.db.getDouble(Constants.CURRENT_LANG, 0.0)
        val eventTypes = ArrayList<Int>()
        var ids: Int
        for (i in catList.indices) {
            if (catList[i].isActive) {
                ids = catList[i].id!!
                eventTypes.add(ids)
            }
        }
        val call = HooleyApp.apiService.searchEvent(SearchEventRequestModel(searchString, radius!!, eventTypes, isPaid!!, isFree!!, latitude, longitude, userId, searchDate, HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN)))
        call.enqueue(object : Callback<SearchEventModel> {
            override fun onResponse(call: Call<SearchEventModel>, response: Response<SearchEventModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("SearchEventModel :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<SearchEventModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun addBookMarkSearch(totalMiles: Float, catList: ArrayList<Category>, isPaid: Boolean?, isFree: Boolean?, callback: IWebServiceCallback<GeneralModel>) {
        val eventTypeArrayList = ArrayList<AddSearchBookmarkRequestModel.EventType>()
        for (i in catList.indices) {
            if (catList[i].isActive) {
                eventTypeArrayList.add(AddSearchBookmarkRequestModel.EventType(catList[i].id.toString()))
            }
        }
        val call = HooleyApp.apiService.addBookMarkSearch(AddSearchBookmarkRequestModel(HooleyApp.db.getString(Constants.USER_ID), totalMiles, isPaid, isFree, eventTypeArrayList))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("addBookMarkSearch :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun removeBookMarkSearch(id: String, callback: IWebServiceCallback<GeneralModel>) {
        val call = HooleyApp.apiService.removeBookMarkSearch(RemoveSingleSearchBookMarkRequestModel(id, HooleyApp.db.getString(Constants.USER_ID)))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("removeBookMarkSearch :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun removeAllBookMarkSearch(callback: IWebServiceCallback<GeneralModel>) {
        val call = HooleyApp.apiService.removeAllBookMarkSearch(RemoveAllSearchBookMarkRequestModel(HooleyApp.db.getString(Constants.USER_ID)))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure(response.errorBody()!!.toString())
                    return
                }
                Logger.ex("removeAllBookMarkSearch :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getBookMarkSearch(callback: IWebServiceCallback<BookMarkSearchModel>) {
        val call = HooleyApp.apiService.getBookMarkSearch(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<BookMarkSearchModel> {
            override fun onResponse(call: Call<BookMarkSearchModel>, response: Response<BookMarkSearchModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getBookMarkSearch :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<BookMarkSearchModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param mediaUri
     * @param callback
     */
    fun updateEvent(mediaUri: ArrayList<ItemCreateEventWallpaper>, editEventModel: EditEventModel, eventId: String, isPublish: Boolean, update: String,
                    callback: IWebServiceCallback<GeneralModel>) {
        val mlist = ArrayList<MultipartBody.Part>()
        for (i in mediaUri.indices) {
            if (mediaUri[i].type == Constants.TYPE_URI) {
                val file = File(mediaUri[i].uri?.path!!)
                val reqFile = RequestBody.create(MediaType.parse("image/jpeg"), file)
                val fileBody = MultipartBody.Part.createFormData("file", file.name, reqFile)
                mlist.add(fileBody)
            }
        }
        val isUpdate = RequestBody.create(MediaType.parse("text/plain"), update)

        val requestJson = RequestBody.create(MediaType.parse("text/plain"), ApiRequestJson.getInstance().updateEventString(mediaUri, eventId, editEventModel, isPublish))
        val call = HooleyApp.apiService.updateEvent(mlist, isUpdate, requestJson)
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("updateEvent:" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getEventUpdateAction(eventId: String, updateActionId: Int, callback: IWebServiceCallback<GetUpdateEventActionModel>) {
        val call = HooleyApp.apiService.getEventUpdateAction(HooleyApp.db.getString(Constants.USER_ID), eventId, updateActionId)
        call.enqueue(object : Callback<GetUpdateEventActionModel> {
            override fun onResponse(call: Call<GetUpdateEventActionModel>, response: Response<GetUpdateEventActionModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getEventUpdateAction :" + Gson().toJson(response.body()!!))
                if (response.body()!!.IsSuccessful) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()?.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()?.exception!!)
                }
            }

            override fun onFailure(call: Call<GetUpdateEventActionModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    fun setUpdateEventAction(eventId: String, updateActionId: Int, startTime: String?, endTime: String?, isAlertEmail: Boolean?, isTicketRefund: Boolean?,
                             reasonText: String, callback: IWebServiceCallback<GeneralModel>) {

        if (startTime != null && endTime != null) {
            val call = HooleyApp.apiService.updateEventByActionId(UpdateEventActionRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId, updateActionId, DateUtils.SendDateToDateUpdate(DateUtils.getDateInUtcUpdate(startTime)), DateUtils.SendDateToDateUpdate(DateUtils.getDateInUtcUpdate(endTime)), isAlertEmail, isTicketRefund, reasonText))
            call.enqueue(object : Callback<GeneralModel> {
                override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                    if (response.body() == null) {
                        callback.failure("Server not responding Please try again later")
                        return
                    }
                    Logger.ex("setUpdateEventAction :" + Gson().toJson(response.body()!!))
                    if (response.body()!!.success) {
                        callback.success(response.body()!!)
                    } else {
                        if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                            callback.onTokenExpire()
                        } else
                            callback.failure(response.body()!!.exception!!)
                    }
                }

                override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                    if (t is UnknownHostException || t is IOException) {
                        if (t.message.equals("timeout", ignoreCase = true)) {
                            callback.failure(Constants.TIMEOUT)
                        } else
                            callback.failure(Constants.NO_NETWORK)
                    } else {
                        callback.failure(t.message!!)
                    }
                }
            })

        } else {
            val call = HooleyApp.apiService.updateEventByActionId(UpdateEventActionRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId, updateActionId, null, null, isAlertEmail, isTicketRefund, reasonText))
            call.enqueue(object : Callback<GeneralModel> {
                override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                    if (response.body() == null) {
                        callback.failure("Server not responding Please try again later")
                        return
                    }
                    Logger.ex("setUpdateEventAction :" + Gson().toJson(response.body()!!))
                    if (response.body()!!.success) {
                        callback.success(response.body()!!)
                    } else {
                        if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                            callback.onTokenExpire()
                        } else
                            callback.failure(response.body()!!.exception!!)
                    }
                }

                override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                    if (t is UnknownHostException || t is IOException) {
                        if (t.message.equals("timeout", ignoreCase = true)) {
                            callback.failure(Constants.TIMEOUT)
                        } else
                            callback.failure(Constants.NO_NETWORK)
                    } else {
                        callback.failure(t.message!!)
                    }
                }
            })

        }

    }


    fun getEventCategories(callback: IWebServiceCallback<CreateEventModel>) {
        val call = HooleyApp.apiService.getCreateEventData(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<CreateEventModel> {
            override fun onResponse(call: Call<CreateEventModel>, response: Response<CreateEventModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getEventCategories :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<CreateEventModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })

    }


    fun getPromotionData(eventId: String, callback: IWebServiceCallback<GetEventPromotionModel>) {

        val call = HooleyApp.apiService.getEventPromotionData(HooleyApp.db.getString(Constants.USER_ID), eventId)
        call.enqueue(object : Callback<GetEventPromotionModel> {
            override fun onResponse(call: Call<GetEventPromotionModel>, response: Response<GetEventPromotionModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getPromotionData :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GetEventPromotionModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    fun getUKStateCities(callback: IWebServiceCallback<GetCityStateModel>) {
        val call = HooleyApp.apiService.getAllStatesCities(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<GetCityStateModel> {
            override fun onResponse(call: Call<GetCityStateModel>, response: Response<GetCityStateModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getUKStateCities :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GetCityStateModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })

    }


    fun promoteEvent(eventId: String, amount: Int, startDate: String, endDate: String, minAge: String, maxAge: String,
                     audiences: ArrayList<GetEventPromotionModel.PromoteAudiences>, statesWithCitiesList: ArrayList<GetCityStateModel.StateCity>,
                     stripeToken: String, callback: IWebServiceCallback<GeneralModel>) {

        val mAudiences = ArrayList<PromoteEventRequestModel.PromoteAudience>()
        for (i in audiences.indices) {
            mAudiences.add(PromoteEventRequestModel.PromoteAudience(audiences[i].id, audiences[i].name))
        }

        val mstates = ArrayList<PromoteEventRequestModel.PromoteRegion>()
        for (i in statesWithCitiesList.indices) {
            if (statesWithCitiesList[i].isChecked) {
                val citiesList = ArrayList<PromoteEventRequestModel.PromoteRegionsCity>()
                for (j in statesWithCitiesList[i].citiesList.indices) {
                    if (statesWithCitiesList[i].citiesList[j].isChecked) {
                        citiesList.add(PromoteEventRequestModel.PromoteRegionsCity(statesWithCitiesList[i].citiesList[j].id))
                    }
                }
                mstates.add(PromoteEventRequestModel.PromoteRegion(statesWithCitiesList[i].id, citiesList))
            }
        }

        val model = PromoteEventRequestModel(mstates, mAudiences, amount, DateUtils.getDateInUtc(startDate), DateUtils.getDateInUtc(endDate), eventId, HooleyApp.db.getString(Constants.USER_ID), minAge, maxAge, stripeToken)
        val call = HooleyApp.apiService.promoteEvent(model)
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("promoteEvent :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    /**
     * @param eventId
     * @param callback
     */
    fun addMarkOnWay(eventId: String, callback: IWebServiceCallback<GeneralModel>) {
        val call = HooleyApp.apiService.addMarkOnTheWay(LikeEventRequestModel(eventId, HooleyApp.db.getString(Constants.USER_ID)))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("addMarkOnWay:" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param eventId
     * @param callback
     */
    fun getEventWallpappers(callback: IWebServiceCallback<HooleyGalleryModel>) {
        val call = HooleyApp.apiService.getHooleyGalleryModel(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<HooleyGalleryModel> {
            override fun onResponse(call: Call<HooleyGalleryModel>, response: Response<HooleyGalleryModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getEventWallpappers:" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<HooleyGalleryModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun setEventAlert(eventId: Int, date: String, isAlert: Boolean, callback: GenericCallback<SetAlertModel>) {
        val call = HooleyApp.apiService.setEventAlert(SetEventAlertRequestModel(HooleyApp.db.getString(Constants.USER_ID), eventId, DateUtils.getDateInUtc(date), isAlert))
        call.enqueue(object : Callback<SetAlertModel> {
            override fun onResponse(call: Call<SetAlertModel>, response: Response<SetAlertModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("setTicketAlert " + Gson().toJson(response.body()!!))
                if (response.body()!!.IsSuccessful) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<SetAlertModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun addAnnouncement(mediaUri: Uri?, model: SetEventAnnouncementRequestModel, callback: GenericCallback<GeneralModel>) {
        val mlist = ArrayList<MultipartBody.Part>()
        if (mediaUri != null) {
            val file = File(mediaUri.path!!)
            val reqFile = RequestBody.create(MediaType.parse("image/jpeg"), file)
            val fileBody = MultipartBody.Part.createFormData("files", file.name, reqFile)
            mlist.add(fileBody)
        }
        val requestJson = RequestBody.create(MediaType.parse("text/plain"), Gson().toJson(model))

        val call = HooleyApp.apiService.addEventAnnouncement(mlist, requestJson)
        call.enqueue(object : Callback<GeneralModel> {
            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                callback.failure(t.message!!)
            }

            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                callback.success(response.body()!!)
            }
        })


    }

    fun deleteEventAnnouncement(id: String, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.deleteEventAnnouncement(HooleyApp.db.getString(Constants.USER_ID), id)
        call.enqueue(object : Callback<GeneralModel> {
            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                callback.failure(t.message!!)
            }

            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                callback.success(response.body()!!)
            }

        })

    }


    fun getEventGuestList(eventId: String, type: String, callback: IWebServiceCallback<EventGuestListModel>) {
        val call = HooleyApp.apiService.getEventGuestList(HooleyApp.db.getString(Constants.USER_ID), eventId, type)
        call.enqueue(object : Callback<EventGuestListModel> {
            override fun onResponse(call: Call<EventGuestListModel>, response: Response<EventGuestListModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getEventGuestList :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<EventGuestListModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    fun getEventAnnouncement(eventId: String, callback: IWebServiceCallback<EventAnnouncementModel>) {
        val call = HooleyApp.apiService.getEventAnnouncement(HooleyApp.db.getString(Constants.USER_ID), eventId, HooleyApp.db.getBoolean(Constants.IS_GUEST_LOGIN))
        call.enqueue(object : Callback<EventAnnouncementModel> {
            override fun onResponse(call: Call<EventAnnouncementModel>, response: Response<EventAnnouncementModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("getEventAnnouncement :" + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        callback.onTokenExpire()
                    } else
                        callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<EventAnnouncementModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun updateGeoFence(eventId: String, model: PersonalProfileModel.GeoFence, iWebServiceCallback: IWebServiceCallback<GeneralModel>) {
        var eventGeofenceList = ArrayList<PersonalProfileModel.GeoFence>()
        eventGeofenceList.add(model)
        val call = HooleyApp.apiService.updateEventGeoFence(UpdateGeoFenceRequestModel(eventId, HooleyApp.db.getString(Constants.USER_ID), eventGeofenceList))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        iWebServiceCallback.failure(Constants.TIMEOUT)
                    } else
                        iWebServiceCallback.failure(Constants.NO_NETWORK)
                } else {
                    iWebServiceCallback.failure(t.message!!)
                }
            }

            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    if (response.errorBody() != null)
                        iWebServiceCallback.failure(response.errorBody().toString())
                    else
                        iWebServiceCallback.failure("Server not responding Please try again later")
                    return
                }
                if (response.body()!!.success) {
                    iWebServiceCallback.success(response.body()!!)
                } else {
                    if (response.body()!!.exception.equals("Authentication problem", ignoreCase = true)) {
                        iWebServiceCallback.onTokenExpire()
                    } else
                        iWebServiceCallback.failure(response.body()!!.exception!!)
                }
            }
        })

    }
}
