package com.hooleyapp.hooley.services

import android.net.Uri
import android.util.Log
import com.google.gson.Gson
import com.hooleyapp.hooley.HooleyApp
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.apis.ApiRequestJson
import com.hooleyapp.hooley.interfaces.GenericCallback
import com.hooleyapp.hooley.log.Logger
import com.hooleyapp.hooley.model.*
import com.hooleyapp.hooley.others.Constants
import com.hooleyapp.hooley.others.Util
import com.hooleyapp.hooley.requestmodel.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.net.UnknownHostException
import java.util.*

class ProfileWebService {

    /**
     * @param profilePic
     * @param firstName
     * @param lastName
     * @param aboutMe
     * @param countryCode
     * @param countryISO
     * @param phoneNumber
     * @param homeTown
     * @param email
     * @param website
     * @param genderId
     * @param birthday
     * @param relationShipId
     * @param religionId
     * @param hobbies
     * @param college
     * @param highschool
     * @param collegeMajor
     * @param astrologicalSignId
     * @param policticalViewId
     * @param languageId
     * @param ethinicBackground
     * @param employer
     * @param organization
     * @param occupationId
     * @param cityId
     * @param countryId
     * @param stateId
     * @param callback
     */
    fun updatePersonalProfile(mediaUri: Uri?, profilePic: String, firstName: String, lastName: String, aboutMe: String, countryCode: String, countryISO: String, phoneNumber: String, homeTown: String, email: String, website: String, genderId: String,
                              birthday: String, relationShipId: String, religionId: String, hobbies: String, college: String, highschool: String, collegeMajor: String, astrologicalSignId: String,
                              policticalViewId: String, languageId: String, ethinicBackground: String, employer: String,
                              organization: String, occupationId: String, cityId: String, countryId: String, stateId: String, callback: GenericCallback<GeneralModel>) {


        val mlist = ArrayList<MultipartBody.Part>()
        if (mediaUri != null) {
            val file = File(mediaUri.path!!)
            val reqFile = RequestBody.create(MediaType.parse("image/jpeg"), file)
            val fileBody = MultipartBody.Part.createFormData("files", file.name, reqFile)
            mlist.add(fileBody)
        }
        val requestJson = RequestBody.create(MediaType.parse("text/plain"), ApiRequestJson.getInstance().updatePersonalProfile(
                profilePic, firstName, lastName, aboutMe, countryCode, countryISO, phoneNumber, homeTown, email, website, genderId,
                birthday, relationShipId, religionId, hobbies, college, highschool, collegeMajor, astrologicalSignId, policticalViewId, languageId, ethinicBackground, employer, organization,
                occupationId, cityId, countryId, stateId))


        val call = HooleyApp.apiService.updatePersonalProfile(mlist, requestJson)
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("Update Personal Profile: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })

    }

    /**
     * @param profilePic
     * @param businessName
     * @param aboutBusiness
     * @param countryCode
     * @param countryISO
     * @param businessPhone
     * @param isPhoneVerified
     * @param businessEmail
     * @param businessWebsite
     * @param contactName
     * @param companyTypeId
     * @param industryId
     * @param address
     * @param cityId
     * @param stateId
     * @param countryId
     * @param zipCode
     * @param socialList
     * @param callback
     */
    fun updateBusinessProfile(mediaUri: Uri?, profilePic: String, businessName: String, aboutBusiness: String, countryCode: String, countryISO: String, businessPhone: String, isPhoneVerified: Boolean, businessEmail: String, businessWebsite: String,
                              contactName: String, companyTypeId: String, industryId: String, address: String, cityId: String, stateId: String, countryId: String, zipCode: String, socialList: ArrayList<BusinessProfileModel.SocialMedia>, callback: GenericCallback<GeneralModel>) {


        val mlist = ArrayList<MultipartBody.Part>()
        if (mediaUri != null) {
            val file = File(mediaUri.path!!)
            val reqFile = RequestBody.create(MediaType.parse("image/jpeg"), file)
            val fileBody = MultipartBody.Part.createFormData("files", file.name, reqFile)
            mlist.add(fileBody)
        }

        val requestJson = RequestBody.create(MediaType.parse("text/plain"), ApiRequestJson.getInstance().updateBusinessProfile(
                profilePic, businessName, aboutBusiness, countryCode, countryISO, businessPhone, isPhoneVerified, businessEmail, businessWebsite, contactName, companyTypeId, industryId, address,
                cityId, stateId, countryId, zipCode, socialList))

        val call = HooleyApp.apiService.updateBusinessProfile(mlist, requestJson)
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("Update Business Profile: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })


    }


    /**
     * @param id
     * @param privacyId
     * @param isChecked
     * @param callback
     */
    fun updateUserPrivacy(id: String, privacyId: String, isChecked: Boolean, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.updateUserPrivacy(UpdateUserPrivacyRequestModel(id, HooleyApp.db.getString(Constants.USER_ID), privacyId, isChecked))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("Update User Privacy: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }


    fun updateUserPreferences(privacyId: String, isChecked: Boolean, accessPrivacyId: String, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.updateUserPrivacy(UpdateUserPrivacyRequestModel(HooleyApp.db.getString(Constants.USER_ID), privacyId, isChecked, accessPrivacyId))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("Update User Privacy: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getUserPrivacy(callback: GenericCallback<UserPreferencesModel>) {
        val call = HooleyApp.apiService.getUserPrivacy(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<UserPreferencesModel> {
            override fun onFailure(call: Call<UserPreferencesModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }

            override fun onResponse(call: Call<UserPreferencesModel>, response: Response<UserPreferencesModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("Update User Privacy: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

        })
    }


    fun connectStripe(code: String, callback: GenericCallback<GeneralModel>) {
        val connectStripeRequestModel = ConnectStripeRequestModel(HooleyApp.db.getString(Constants.USER_ID), code)
        val call = HooleyApp.apiService.connectStripe(connectStripeRequestModel)
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("Update User Privacy: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    Log.e("connectStripe", Gson().toJson(response.body()!!))
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                    Util.showToastMessage(HooleyMain.activity, response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })


    }


    /**
     * @param callback
     */
    fun getUserProfile(callback: GenericCallback<GetUserProfilesModel>) {
        val call = HooleyApp.apiService.getUserProfiles(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<GetUserProfilesModel> {
            override fun onResponse(call: Call<GetUserProfilesModel>, response: Response<GetUserProfilesModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("Get User Profile: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GetUserProfilesModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message != null && t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param name
     * @param callback
     */
    fun addUserInterests(name: String, callback: GenericCallback<GeneralModel>) {
        val interestRequestModel = InterestRequestModel(HooleyApp.db.getString(Constants.USER_ID), name)
        val call = HooleyApp.apiService.addUserInerests(interestRequestModel)
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("Add User Interest: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param name
     * @param callback
     */
    fun deleteUserInterest(name: String, callback: GenericCallback<GeneralModel>) {
        val interestRequestModel = InterestRequestModel(HooleyApp.db.getString(Constants.USER_ID), name)
        val call = HooleyApp.apiService.deleteUserInerests(interestRequestModel)
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("Delete User Interest: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param area
     * @param countryId
     * @param stateId
     * @param state
     * @param callback
     */
    fun getCountries(area: String, countryId: String, stateId: String, callback: GenericCallback<CountryStateCitiesModel>) {
        val call = HooleyApp.apiService.getCountryStateCities(HooleyApp.db.getString(Constants.USER_ID), area, countryId, stateId)
        call.enqueue(object : Callback<CountryStateCitiesModel> {
            override fun onResponse(call: Call<CountryStateCitiesModel>, response: Response<CountryStateCitiesModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("Get Countries: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<CountryStateCitiesModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message != null && t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param phoneNumber
     * @param countryCode
     * @param callback
     */
    fun verifyNumber(phoneNumber: String, countryCode: String, msisdn: String, callback: GenericCallback<VerifyPhoneNumberModel>) {

        val verifyNumberRequestModel = VerifyNumberRequestModel(HooleyApp.db.getString(Constants.USER_ID), phoneNumber, countryCode, msisdn)
        val call = HooleyApp.apiService.verifyPhoneNumber(verifyNumberRequestModel)
        call.enqueue(object : Callback<VerifyPhoneNumberModel> {
            override fun onResponse(call: Call<VerifyPhoneNumberModel>, response: Response<VerifyPhoneNumberModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("Verify Number: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<VerifyPhoneNumberModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param messageId
     * @param code
     * @param isBusinessProfile
     * @param callback
     */
    fun verifyPinCode(messageId: String, code: String, isBusinessProfile: Boolean, callback: GenericCallback<GeneralModel>) {
        val verifyCodeRequestModel = VerifyCodeRequestModel(HooleyApp.db.getString(Constants.USER_ID), messageId, code, isBusinessProfile)
        val call = HooleyApp.apiService.verifyCode(verifyCodeRequestModel)
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("verify Pin Code: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun getPersonalGeoFence(callback: GenericCallback<PersonalGeofenceModel>) {
        val call = HooleyApp.apiService.getPersonalGeofence(HooleyApp.db.getString(Constants.USER_ID))
        call.enqueue(object : Callback<PersonalGeofenceModel> {
            override fun onResponse(call: Call<PersonalGeofenceModel>, response: Response<PersonalGeofenceModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("get Personal GeoFence: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<PersonalGeofenceModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun updateUserSocialMedia(socialId: String, socialUrl: String, isConnected: Boolean?, callback: GenericCallback<GeneralModel>) {
        val call = HooleyApp.apiService.updateUserSocialMedia(UpdateUserSocialMediaRequestModel(HooleyApp.db.getString(Constants.USER_ID), socialId, socialUrl, isConnected))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("updateUserSocialMedia: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    fun updatePersonalGeofence(geofenceList: ArrayList<GeoFenceModel>?, callback: GenericCallback<GeneralModel>) {
        val myGeofenceList = ArrayList<UpdatePersonalGeofenceRequestModel.MyGeofence>()
        if (geofenceList != null && geofenceList.size > 0) {
            for (i in geofenceList.indices) {
                myGeofenceList.add(UpdatePersonalGeofenceRequestModel.MyGeofence(geofenceList[i].placeLat, geofenceList[i].placeLong, geofenceList[i].raduis, geofenceList[i].address))
            }
        }
        val call = HooleyApp.apiService.updatePersonalGeofence(UpdatePersonalGeofenceRequestModel(HooleyApp.db.getString(Constants.USER_ID), myGeofenceList))
        call.enqueue(object : Callback<GeneralModel> {
            override fun onResponse(call: Call<GeneralModel>, response: Response<GeneralModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("updatePersonalGeofence: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<GeneralModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })
    }

    /**
     * @param mediaUri
     * @param birthday
     * @param phoneNo
     * @param countryCode
     * @param countryISO
     * @param countryId
     * @param stateId
     * @param cityId
     * @param callback
     */
    fun setupProfile(mediaUri: Uri?, birthday: String, phoneNo: String, countryCode: String, countryISO: String, countryId: String, stateId: String, cityId: String, msisdn: String, gender: Int, callback: GenericCallback<VerifyPhoneNumberModel>) {

        val mlist = ArrayList<MultipartBody.Part>()
        if (mediaUri != null) {
            val file = File(mediaUri.path!!)
            val reqFile = RequestBody.create(MediaType.parse("image/jpeg"), file)
            val fileBody = MultipartBody.Part.createFormData("files", file.name, reqFile)
            mlist.add(fileBody)
        }
        val requestJson = RequestBody.create(MediaType.parse("text/plain"), ApiRequestJson.getInstance().profileSetup(
                birthday, phoneNo, countryCode, countryISO, countryId, stateId, cityId, msisdn, gender))

        val call = HooleyApp.apiService.profileSetup(mlist, requestJson)
        call.enqueue(object : Callback<VerifyPhoneNumberModel> {
            override fun onResponse(call: Call<VerifyPhoneNumberModel>, response: Response<VerifyPhoneNumberModel>) {
                if (response.body() == null) {
                    callback.failure("Server not responding Please try again later")
                    return
                }
                Logger.ex("Update Personal Profile: " + Gson().toJson(response.body()!!))
                if (response.body()!!.success) {
                    callback.success(response.body()!!)
                } else {
                    callback.failure(response.body()!!.exception!!)
                }
            }

            override fun onFailure(call: Call<VerifyPhoneNumberModel>, t: Throwable) {
                if (t is UnknownHostException || t is IOException) {
                    if (t.message.equals("timeout", ignoreCase = true)) {
                        callback.failure(Constants.TIMEOUT)
                    } else
                        callback.failure(Constants.NO_NETWORK)
                } else {
                    callback.failure(t.message!!)
                }
            }
        })

    }

}
