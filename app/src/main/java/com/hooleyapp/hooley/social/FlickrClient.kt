package com.hooleyapp.hooley.social


import android.os.AsyncTask
import android.os.StrictMode
import com.github.scribejava.apis.FlickrApi
import com.github.scribejava.core.builder.ServiceBuilder
import com.github.scribejava.core.model.OAuth1AccessToken
import com.github.scribejava.core.model.OAuth1RequestToken
import com.github.scribejava.core.model.OAuthRequest
import com.github.scribejava.core.model.Verb
import com.github.scribejava.core.oauth.OAuth10aService
import com.hooleyapp.hooley.R
import com.hooleyapp.hooley.activites.HooleyMain
import com.hooleyapp.hooley.log.Logger
import java.io.IOException
import java.util.concurrent.ExecutionException


/**
 * Created by Nauman on 2/20/2018.
 */

class FlickrClient private constructor() {

    companion object {

        private val PROTECTED_RESOURCE_URL = "https://api.flickr.com/services/rest/"
        lateinit var service: OAuth10aService
        lateinit var requestToken: OAuth1RequestToken
        private var authTokenFixed: String? = null
        private var tokenSecret = ""

        var authorizationUrl: String = ""

        fun FlickerAuth(): String {

            var policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
            service = ServiceBuilder(HooleyMain.activity!!.getString(R.string.flickr_consumer_key))
                    .apiSecret(HooleyMain.activity!!.getString(R.string.flickr_consumer_secret))
                    .callback(PROTECTED_RESOURCE_URL)
                    .build(FlickrApi.instance())
            Logger.e("=== Flickr's OAuth Workflow ===")
            Logger.e("Fetching the Request Token...")
            try {
                requestToken = service.requestToken
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            } catch (e: ExecutionException) {
                e.printStackTrace()
            }

            tokenSecret = requestToken.tokenSecret
            Logger.e("Got the Request Token!")
            Logger.e("Now go and authorize ScribeJava here:")
            authorizationUrl = service.getAuthorizationUrl(requestToken)
            Logger.e(authorizationUrl)
            Logger.e("And paste the verifier here")
            return authorizationUrl
        }

        fun logIn(oauthVerifier: String): Boolean {
            //
            // Trade the Request Token and Verfier for the Access Token
            Logger.e("Trading the Request Token for an Access Token...")
            var accessToken: OAuth1AccessToken? = null
            try {
                accessToken = service.getAccessToken(requestToken, oauthVerifier)
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            } catch (e: ExecutionException) {
                e.printStackTrace()
            }

            //        Logger.e("Got the Access Token!");
            //        Logger.e("(The raw response looks like this: " + accessToken.getRawResponse() + "')");

            authTokenFixed = accessToken!!.rawResponse.split("&".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1].split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
            //        accessToken.getRawResponse().
            // Now let's go and ask for a protected resource!
            Logger.e("Now we're going to access a protected resource...")
            val request = OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL)
            request.addQuerystringParameter("method", "flickr.test.login")
            service.signRequest(accessToken, request)
            //        final Response response = service.execute(request);
            Logger.e("Got it! Lets see what we found...")
            //        Logger.e(response.getBody());

            //        shareOnFlikerWithUrl("http://hooley.stagingdesk.com/hooleygallery/download.jpg");
            //        shareOnFlikerWithFile(new File(Uri.parse("file:///storage/emulated/0/Pictures/JPEG_20180221181732_1389159484.jpg").getPath()));
            return !accessToken.isEmpty


        }

    }


    open class RemoteDataTask : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg p0: String?): String? {
            return FlickrClient.FlickerAuth()
        }

    }

}