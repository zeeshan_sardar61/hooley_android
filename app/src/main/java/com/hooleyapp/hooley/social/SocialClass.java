package com.hooleyapp.hooley.social;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.hooleyapp.hooley.HooleyApp;
import com.hooleyapp.hooley.R;
import com.hooleyapp.hooley.activites.DecisionActivity;
import com.hooleyapp.hooley.activites.HooleyAuth;
import com.hooleyapp.hooley.activites.HooleyMain;
import com.hooleyapp.hooley.interfaces.ISocialCallBackListener;
import com.hooleyapp.hooley.log.Logger;
import com.hooleyapp.hooley.others.Constants;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import retrofit2.Call;
//import com.google.android.gms.auth.api.Auth;

/**
 * Created by Nauman on 2/22/2018.
 */

public class SocialClass implements GoogleApiClient.OnConnectionFailedListener {

    public static GoogleApiClient googleApiClient = null;
    public TwitterApiClient twitterApiClient;
    Handler handler = new Handler();
    private CallbackManager callbackManager;
    private TwitterAuthClient twitterAuthClient;
    private ISocialCallBackListener mListener;
    private String userSocialEmail;
    private Boolean isFirstTime = false;

    public static void logoutFaceBook() {
        LoginManager.getInstance().logOut();
    }

    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS);
    }

    /**
     * Initialize FaceBook Client
     */

    public CallbackManager initFacebookSdk() {
        if (callbackManager != null)
            callbackManager = null;
        callbackManager = CallbackManager.Factory.create();
        FacebookSdk.sdkInitialize(DecisionActivity.Companion.getActivity());
        return callbackManager;
    }

    /**
     * Initialize Google plus Client
     */
    public GoogleApiClient initGooglePlus() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .requestScopes(new com.google.android.gms.common.api.Scope(Scopes.PLUS_ME))
                .requestScopes(new com.google.android.gms.common.api.Scope(Scopes.PLUS_LOGIN))
                .build();
        if (googleApiClient == null)
            googleApiClient = new GoogleApiClient.Builder(DecisionActivity.Companion.getActivity())
                    .enableAutoManage(HooleyAuth.Companion.getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .addApi(Plus.API)
                    .build();
        return googleApiClient;
    }

    /**
     * Initialize Twitter Client
     */
    public TwitterAuthClient initTwitter() {
        TwitterAuthConfig authConfig = new TwitterAuthConfig(HooleyMain.Companion.getActivity().getResources().getString(R.string.twitter_comsumer_key),
                HooleyMain.Companion.getActivity().getResources().getString(R.string.twitter_consumer_secret));
        TwitterConfig config = new TwitterConfig.Builder(HooleyMain.Companion.getActivity())
                .twitterAuthConfig(authConfig)
                .debug(true)
                .build();
        Twitter.initialize(config);
        twitterAuthClient = new TwitterAuthClient();
        return twitterAuthClient;
    }

    /**
     * Request to login with twitter
     */

    public void loginWithTwitter() {

        twitterAuthClient.authorize(HooleyMain.Companion.getActivity(), new com.twitter.sdk.android.core.Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                final TwitterSession sessionData = result.data;
                Logger.INSTANCE.d("sessionData==>>" + sessionData);
                twitterApiClient = TwitterCore.getInstance().getApiClient(sessionData);
                HooleyApp.Companion.getDb().putString(Constants.TWITTER_USER_ID, Long.toString(sessionData.getUserId()));
                HooleyApp.Companion.getDb().putString(Constants.TWITTER_USER_NAME, sessionData.getUserName());
                mListener.onCallBackTwitter(true);
            }

            @Override
            public void failure(TwitterException exception) {
                mListener.onCallBackTwitter(false);
                Logger.INSTANCE.d("failure==>> " + exception.getLocalizedMessage());
            }
        });
    }

    public void postToTwitter(final String message) {
        twitterApiClient = TwitterCore.getInstance().getApiClient();
        if (twitterApiClient == null)
            return;
        final StatusesService statusesService = twitterApiClient.getStatusesService();
        final Call<Tweet> update = statusesService.update(message, null, null, null, null, null, null, null, null);
        update.enqueue(new com.twitter.sdk.android.core.Callback<Tweet>() {
            @Override
            public void success(Result<Tweet> result) {
                Logger.INSTANCE.d("success Tweet generated");
            }

            @Override
            public void failure(TwitterException exception) {
                Logger.INSTANCE.d("failure" + exception.getLocalizedMessage());
            }
        });
//
    }

    public void setMListener(ISocialCallBackListener listener) {
        mListener = listener;
    }

    /**
     * Request to login with face book
     */
    public void loginWithFacebook(Activity context) {
        Collection<String> readPermissions = new ArrayList<>();
        readPermissions.add("public_profile");
        readPermissions.add("email");
        readPermissions.add("user_birthday");
        Collection<String> publishPermissions = new ArrayList<>();
        publishPermissions.add("publish_actions");
        if (AccessToken.getCurrentAccessToken() != null)
            LoginManager.getInstance().logOut();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            isFirstTime = true;
                            HooleyApp.Companion.getDb().putString(Constants.SOCIAL_USER_ID, object.getString("id"));
                            HooleyApp.Companion.getDb().putString(Constants.SOCIAL_USER_FIRST_NAME, object.getString("first_name"));
                            HooleyApp.Companion.getDb().putString(Constants.SOCIAL_USER_LAST_NAME, object.getString("last_name"));
                            HooleyApp.Companion.getDb().putString(Constants.SOCIAL_USER_GENDER, object.getString("gender"));
                            HooleyApp.Companion.getDb().putString(Constants.SOCIAL_MEDIA_TYPE, Constants.SOCIAL_FACE_BOOK);
                            HooleyApp.Companion.getDb().putString(Constants.SOCIAL_USER_PROFILE_PIC, "https://graph.facebook.com/" + object.getString("id") + "/picture?type=large");
                            HooleyApp.Companion.getDb().putString(Constants.SOCIAL_USER_EMAIL, object.getString("email"));
//                            Logger.e(" //Email ==> " + object.getString("email") + "//id ==> " + object.getString("id") +
//                                    "//first_name ==> " + object.getString("first_name") + "//last_name ==> " + object.getString("last_name") +
//                                    "//gender ==> " + object.getString("gender"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            HooleyApp.Companion.getDb().putString(Constants.SOCIAL_USER_EMAIL, "");
                        } catch (NullPointerException ex) {
                            ex.printStackTrace();
                            HooleyApp.Companion.getDb().putString(Constants.SOCIAL_USER_EMAIL, "");
                        }
                        HooleyApp.Companion.getDb().putString(Constants.SOCIAL_USER_ID, loginResult.getAccessToken().getUserId());
                        HooleyApp.Companion.getDb().putString(Constants.SOCIAL_ID, loginResult.getAccessToken().getUserId());
                        HooleyApp.Companion.getDb().putString(Constants.SOCIAL_TOKEN, loginResult.getAccessToken().getApplicationId());
                        mListener.onCallBackFaceBook(true);
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email,gender");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Logger.INSTANCE.d("Cancel called");
                mListener.onCallBackFaceBook(false);
            }

            @Override
            public void onError(FacebookException error) {
                Logger.INSTANCE.d("Error called :" + error.toString());
                Logger.INSTANCE.d("Error Message :" + error.getMessage());
                mListener.onCallBackFaceBook(false);
            }
        });
        LoginManager.getInstance().logInWithPublishPermissions(context, publishPermissions);
    }

    public void shareToFB(final String url) {
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(url))
                .build();
        ShareApi.share(content, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    public void loginLinkedIn() {
        LISessionManager.getInstance(HooleyMain.Companion.getActivity())
                .init(HooleyMain.Companion.getActivity(), buildScope(), new AuthListener() {
                    @Override
                    public void onAuthSuccess() {
                        getLinkedInProfileData();
                    }

                    @Override
                    public void onAuthError(LIAuthError error) {
                        mListener.onCallBackLinkedIn(false);
                    }
                }, true);

    }

    private void getLinkedInProfileData() {
        APIHelper apiHelper = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            apiHelper = APIHelper.getInstance(Objects.requireNonNull(HooleyMain.Companion.getActivity()));
        }
        assert apiHelper != null;
        apiHelper.getRequest(HooleyMain.Companion.getActivity(), Constants.GET_LINKEDIN_USER_PROFILE_URL, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse apiResponse) {
                Logger.INSTANCE.d("LinkedInResponse:" + apiResponse.getResponseDataAsJson().toString());
                JSONObject profileObj = apiResponse.getResponseDataAsJson();
                try {
                    HooleyApp.Companion.getDb().putString(Constants.LINKEDIN_USER_ID, profileObj.getString("id"));
                    HooleyApp.Companion.getDb().putString(Constants.LINKEDIN_PROFILE_URL, profileObj.getString("publicProfileUrl"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mListener.onCallBackLinkedIn(true);
            }

            @Override
            public void onApiError(LIApiError liApiError) {
                Logger.INSTANCE.d("LinkedInResponse:" + liApiError.toString());
                mListener.onCallBackLinkedIn(false);
            }
        });
    }


    /**
     * @param result
     */
    public void handleSignInResult(GoogleSignInResult result) {
        Logger.INSTANCE.d("handleSignInResult:" + result);
        if (result == null) {
            /* Login error */
            mListener.onCallBackGooglePlus(false);
        } else {
            /* Login success */
            GoogleSignInAccount acct = result.getSignInAccount();
//            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getActivity());

            Logger.INSTANCE.d(" Login Result Email===>>>>>" + acct.getEmail() + " username===>>>>" + acct.getDisplayName() + "tokenID===>>>>" + acct.getId());
            userSocialEmail = acct.getEmail();
//            String personName = acct.getDisplayName();
            String personGivenName = acct.getGivenName();
            String personFamilyName = acct.getFamilyName();
            Uri personPhoto = acct.getPhotoUrl();
            String imageUri;
            if (personPhoto != null)
                imageUri = "" + personPhoto + "";
            else
                imageUri = "";
            if (personFamilyName.equalsIgnoreCase("null")) {
                personFamilyName = "";
            }
            HooleyApp.Companion.getDb().putString(Constants.SOCIAL_USER_EMAIL, acct.getEmail());
            HooleyApp.Companion.getDb().putString(Constants.SOCIAL_ID, acct.getId());
//            HooleyApp.db.putString(Constants.SOCIAL_TOKEN, acct.getIdToken());
            HooleyApp.Companion.getDb().putString(Constants.SOCIAL_USER_FIRST_NAME, personGivenName);
            HooleyApp.Companion.getDb().putString(Constants.SOCIAL_USER_LAST_NAME, personFamilyName);
            HooleyApp.Companion.getDb().putString(Constants.SOCIAL_USER_PROFILE_PIC, imageUri);
            HooleyApp.Companion.getDb().putString(Constants.SOCIAL_MEDIA_TYPE, Constants.SOCIAL_GOOGLE);
//            if (googleApiClient.hasConnectedApi(Plus.API)) {
//                Person person = Plus.PeopleApi.getCurrentPerson(googleApiClient);
//                if (person != null) {
//                    if (person.getGender() == Person.Gender.MALE) {
//                        gender = "Male";
//                    } else if (person.getGender() == Person.Gender.FEMALE) {
//                        gender = "Female";
//                    } else {
//                        gender = "Other";
//                    }
//                    String personPhoto = person.getImage().getUrl();
//                    personPhoto = personPhoto.substring(0, personPhoto.indexOf("sz=") + 3) + "200";
//                HooleyApp.db.putString(Constants.SOCIAL_USER_FIRST_NAME, person.getName().getGivenName());
//                HooleyApp.db.putString(Constants.SOCIAL_USER_LAST_NAME, person.getName().getFamilyName());
//                HooleyApp.db.putString(Constants.SOCIAL_USER_GENDER, gender);
//                    HooleyApp.db.putString(Constants.SOCIAL_USER_PROFILE_PIC, personPhoto);
//            }
//        }
            mListener.onCallBackGooglePlus(true);
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mListener.onCallBackGooglePlus(false);
    }
}
